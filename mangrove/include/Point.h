/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library
 * 
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * May 2011 (Revised on May 2012)
 *                                                                    
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * Point.h - an ordered set of coordinates of a template class T                                         
 ***********************************************************************************************/

/* Should we include this header file? */
#ifndef POINT_H_

	#define POINT_H_

	#include "Simplex.h"
	#include <vector>
	#include <map>
	#include <set>
	#include <iostream>
	#include <cmath>
	#include <assert.h>
	using namespace std;
	
	/// A tuple of templated values.
	/**
	 * This class describes a tuple of template class T instances, that can be also used as a multi-dimensional array or a point. We require that the template class T, used in this class, must
	 * support:<ul>
	 * <li>the << operator in order to write templated values on an output stream</li>
	 * <li>the >> operator in order to read templated values from an input stream</li>
	 * <li>the = operator in order to assign templated values</li>
	 * <li>the + operator in order to add templated values</li>
	 * <li>the - operator in order to subtract templated values</li>
	 * <li>the * operator in order to multiply templated values</li>
	 * <li>the / operator in order to divide templated values</li>
	 * <li>the == operator in order to compare templated values</li></ul>
	 * \file Point.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	 
	/* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		
		/// This value identifies a clock-wise turn between three tuple of templated values.
		/**
		 * This value identifies a clock-wise turn between three tuple of templated values.
		 * \see mangrove_tds::CW, mangrove_tds::ALIGNED, mangrove_tds::Point, mangronve_tds::turn()
		 */
		#define CCW -1
		
		/// This value identifies a counterclock-wise turn between three tuples of templated values.
		/**
		 * This value identifies a counterclock-wise turn between three tuples of templated values.
		 * \see mangrove_tds::CCW, mangrove_tds::ALIGNED, mangrove_tds::Point, mangronve_tds::turn()
		 */
		#define CW +1
		
		/// This value identifies three aligned tuples of templated values.
		/**
		 * This value identifies three aligned tuples of templated values.
		 * \see mangrove_tds::CCW, mangrove_tds::CW, mangrove_tds::Point, mangronve_tds::turn()
		 */
		#define ALIGNED 0

		/// A tuple of templated values.
		/**
		 * This class represents a tuple of template class T instances, that can be also used as a multi-dimensional array or a point. We require that the template class T, used in this class,
		 * must support:<ul>
	 	 * <li>the << operator in order to write templated values on an output stream</li>
	 	 * <li>the >> operator in order to read templated values from an input stream</li>
		 * <li>the = operator in order to assign templated values</li>
	 	 * <li>the + operator in order to add templated values</li>
	 	 * <li>the - operator in order to subtract templated values</li>
	 	 * <li>the * operator in order to multiply templated values</li>
	 	 * <li>the / operator in order to divide templated values</li>
	 	 * <li>the == operator in order to compare templated values</li></ul>
		 */
		template<class T> class Point
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a not initialized instance of this class: you should provide its initialization before using it by applying the member functions offered by the
			 * mangrove_tds::Point class.
			 */
			inline Point() { this->data.clear(); }
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a new tuple of templated values that describes the origin for the required multidimensional space: all the values in the new tuple are 0 (i.e. the
			 * default value).
			 * \param d the number of templated values in the new tuple
			 * \see mangrove_tds::Point::isNull(), mangrove_tds::Point::updateSize()
			 */
			inline Point(unsigned int d) { this->updateSize(d); }
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a new tuple by using the input templated values, contained in a vector: the input vector will not updated.
			 * \param v the vector of templated values to be copied.
			 */
			inline Point(const vector<T> &v)
			{ 
				/* We create a copy of the input vector. */
				if(v.size()==0) { this->data.clear(); }
				else
				{ 
					/* Now, we copy the state of v */
					this->data.clear();
					this->data.resize(v.size());
					for(unsigned int k=0;k<v.size();k++) this->data[k] = T(v[k]);
				}
			}
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a copy of the input tuple of templated values, by replicating its internal state.
			 * \param p the input tuple of templated values to be copied
			 */
			inline Point(const Point<T>& p)
			{
				/* We create a copy of the templated values contained in 'p'. */
				if(p.data.size()==0) { this->data.clear(); }
				else 
				{ 
					/* Now, we copy the state of p.data */
					this->data.clear();
					this->data.resize( p.data.size());
					for(unsigned int k=0;k<p.data.size();k++) this->data[k] = T(p.data[k]);
				}
			}
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a 2-dimensional tuple of templated values, i.e. it will be composed by two templated values.
			 * \param v1 the first templated value of the new tuple
			 * \param v2 the second templated value of the new tuple
			 * \see mangrove_tds::Point::x(), mangrove_tds::Point::cx(), mangrove_tds::Point::y(), mangrove_tds::Point::cy() 
			 */
			inline Point(T v1,T v2)
			{
				/* We impose that the current tuple must be 2-dimensional */
				this->updateSize(2);
				this->x() = T(v1);
				this->y() = T(v2);
			}
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a 3-dimensional tuple of templated values, i.e. it will be composed by three templated values.
			 * \param v1 the first templated value of the new tuple
			 * \param v2 the second templated value of the new tuple
			 * \param v3 the third templated value of the new tuple
			 * \see mangrove_tds::Point::x(), mangrove_tds::Point::cx(), mangrove_tds::Point::y(), mangrove_tds::Point::cy(), mangrove_tds::Point::z(), mangrove_tds::Point::cz()
			 */
			inline Point(T v1,T v2,T v3)
			{
				/* We impose that the current tuple must be 3-dimensional. */
				this->updateSize(3);
				this->x() = T(v1);
				this->y() = T(v2);
				this->z() = T(v3);
			}
			
			/// This member function removes an instance of this class.
			inline virtual ~Point() { this->data.clear(); }
			
			/// This member function returns the number of templated values in the current tuple.
			/**
			 * This member function returns the number of templated values in the current tuple.
			 * \return the number of templated values in the current tuple
			 * \see mangrove_tds::Point::updateSize()
			 */
			inline unsigned int size() const { return this->data.size(); }

			/// This member function updates the number of templated values in the current tuple.
			/**
			 * This member function updates the number of templated values in the current tuple, by reallocating all the needed space: the old templated values will be lost and overwritten.
			 * \param d the new number of templated values in the current tuple
			 * \see mangrove_tds::Point::size()
			 */
			inline void updateSize(unsigned int d)
			{
				/* First, we delete the old 'data' and then we resize it! */
				this->data.clear();
				if(d!=0) this->data.resize(d);
				for(unsigned int k=0;k<d;k++) this->data[k] = T(0);
			}
			
			/// This member function returns a reference to the templated values in the current tuple.
			/**
			 * This member function returns a reference to the templated values in the current tuple.
			 * \return a reference to the templated values in the current tuple
			 */
			inline vector<T>& getCoordinates() { return this->data; }

			/// This member function returns a constant reference to a templated value in the current tuple.
			/**
			 * This member function returns a constant reference to a templated value in the current tuple. Each templated value is identified by its position in the current tuple of
			 * templated values: if the required position is not valid, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this
			 * case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> with this member function we cannot update the current tuple of templated values. You should
			 * refer to the member functions offered by the mangrove_tds::Point class in order to update the current tuple.
 			 * \param i the position of the required value in the current tuple
			 * \return a constant reference to the required value in the current tuple
			 * \see mangrove_tds::Point::cx(), mangrove_tds::Point::cy(), mangrove_tds::Point::cz()
			 */
			inline const T& at(unsigned int i) const
			{
				/* First, we check the required position and then we return the required component. */
				assert(i < this->data.size());
				return this->data[i];
			}
			
			/// This member function returns a constant reference to the templated value belonging to the x axis in the current tuple.
			/**
			 * This member function returns a constant reference to the templated value belonging to the x axis in the current tuple. Each templated value is identified by its position in the
			 * current tuple of templated values: the templated value belonging to the x axis stays in position 0. If this location is not valid, then this member function will fail if and
			 * only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> with this member
			 * function we cannot update the current tuple of templated values. You should refer to the member functions offered by the mangrove_tds::Point class in order to update the
			 * current tuple.
			 * \return a constant reference to the templated value belonging to the x axis in the current tuple
			 * \see mangrove_tds::Point::at(), mangrove_tds::Point::x()
			 */
			inline const T& cx() const { return this->at(0); }

			/// This member function returns a constant reference to the templated value belonging to the y axis in the current tuple.
			/**
			 * This member function returns a constant reference to the templated value belonging to the y axis in the current tuple. Each templated value is identified by its position in the
			 * current tuple of templated values: the templated value belonging to the y axis stays in position 1. If this location is not valid, then this member function will fail if and
			 * only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> with this member
			 * function we cannot update the current tuple of templated values. You should refer to the member functions
			 * offered by the mangrove_tds::Point class in order to update the current tuple.
			 * \return a constant reference to the templated value belonging to the y axis in the current tuple
			 * \see mangrove_tds::Point::at(), mangrove_tds::Point::y()
			 */
			inline const T& cy() const { return this->at(1); }
			
			/// This member function returns a constant reference to the templated value belonging to the z axis in the current tuple.
			/**
			 * This member function returns a constant reference to the templated value belonging to the z axis in the current tuple. Each templated value is identified by its position in the
			 * current tuple of templated values: the templated value belonging to the z axis stays in position 2. If this location is not valid, then this member function will fail if and
			 * only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> with this member
			 * function we cannot update the current tuple of templated values. You should refer to the member functions offered by the mangrove_tds::Point class in order to update the
			 * current tuple.
			 * \return a constant reference to the templated value belonging to the z axis in the current tuple
			 * \see mangrove_tds::Point::at(), mangrove_tds::Point::z()
			 */
			inline const T& cz() const { return this->at(2); }
			
			/// This member function returns a reference to the templated value belonging to the x axis in the current tuple.
			/**
			 * This member function returns a reference to the templated value belonging to the x axis in the current tuple. Each templated value is identified by its position in the current
			 * tuple of templated values: the templated value belonging to the x axis stays in position 0. If this location is not valid, then this member function will fail if and only if
			 * the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> with this member function
			 * we can also update the current tuple of templated values. You should refer to the member functions offered by the mangrove_tds::Point class in order to access in read-only mode
			 * the templated values in the current tuple.
			 * \return a reference to the templated value belonging to the x axis in the current tuple
			 * \see mangrove_tds::Point::at(), mangrove_tds::Point::cx()
			 */
			inline T& x() { return (*this)[0]; }
			
			/// This member function returns a reference to the templated value belonging to the y axis in the current tuple.
			/**
			 * This member function returns a reference to the templated value belonging to the y axis in the current tuple. Each templated value is identified by its position in the current
			 * tuple of templated values: the templated value belonging to the y axis stays in position 1. If this location is not valid, then this member function will fail if and only if
			 * the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> with this member function
			 * we can also update the current tuple of templated values. You should refer to the member functions offered by the mangrove_tds::Point class in order to access in read-only mode
			 * the templated values in the current tuple.
			 * \return a reference to the templated value belonging to the y axis in the current tuple
			 * \see mangrove_tds::Point::at(), mangrove_tds::Point::cy()
			 */
			inline T& y() { return (*this)[1]; }
			
			/// This member function returns a reference to the templated value belonging to the z axis in the current tuple.
			/**
			 * This member function returns a reference to the templated value belonging to the z axis in the current tuple. Each templated value is identified by its position in the current
			 * tuple of templated values: the templated value belonging to the z axis stays in position 2. If this location is not valid, then this member function will fail if and only if
			 * the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> with this member function
			 * we can also update the current tuple of templated values. You should refer to the member functions offered by the mangrove_tds::Point class in order to access in read-only mode
			 * the templated values in the current tuple.
			 * \return a reference to the templated value belonging to the z axis in the current tuple
			 * \see mangrove_tds::Point::at(), mangrove_tds::Point::cz()
			 */
			inline T& z() { return (*this)[2]; }
			
			/// This member function returns the Euclidean 2-norm of the current tuple of templated values.
			/**
			 * This member function returns the Euclidean 2-norm of the current tuple of templated values. In this member function we assume that the current tuple of templated values can be
			 * considered as an array, thus we can compute its Euclidean 2-norm.<p><b>IMPORTANT:</b> this member function is the basis for the mangrove_tds::Point::norm() member
			 * function. Moreover, we assume that the template class T can be used as input for the standard function <i>sqrt</i> (i.e. computing the square root of a value).
			 * \return the Euclidean 2-norm of the current tuple of templated values
			 * \see mangrove_tds::Point::norm()
			 */
			inline T getLength() { return sqrt( (*this) * (*this)); }
			
			/// This member function returns the Euclidean 2-norm of the current tuple of templated values.
			/**
			 * This member function returns the Euclidean 2-norm of the current tuple of templated values. This member function is an alias for the mangrove_tds::Point::getLength() member
			 * function: in this member function we assume that the current tuple of templated values can be considered as an array, thus we can compute its Euclidean 2-norm.<p>
			 * <b>IMPORTANT:</b> we assume that the template class T can be used as input for the standard function <i>sqrt</i> (i.e. computing the square root of a value).
			 * \return the Euclidean 2-norm of the current tuple of templated values
			 * \see mangrove_tds::Point::getLength()
			 */
			inline T norm() { return this->getLength(); }
			
			/// This member function checks if the current tuple of templated values is composed only by 0 values.
			/**
			 * This member function checks if the current tuple of templated values is composed only by 0 values.
			 * \return <ul><li><i>true</i>, if the current tuple of templated values is composed only by 0 values.</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::Point::getLength(), mangrove_tds::Point::norm()
			 */
			inline bool isNull() { return (this->getLength()==0); }
			
			/// This member function normalizes the current tuple of templated values.
			/**
			 * This member function normalizes the current tuple of templated values. With this member function, we divide each templated value by the the input tuple norm: in this way, we
			 * normalize the current tuple of templated values and its norm will become 1.<p>If the norm of the current tuple of templated values is 0, then this member function will fail if
			 * and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> the template
			 * class T is required to support the / operator, used for dividing templated values.
			 * \see mangrove_tds::Point::getLength(), mangrove_tds::Point::norm()
			 */
			inline void normalize()
			{
				T l;

				/* First, we compute the norm of the current tuple and then we divide each component. */
				l = this->norm();
				(*this)/=l;
			}

			/// This member function computes the oriented normal of a triangle embedded in the tridimensional Euclidean space.
			/**
			 * This member function computes the oriented normal of a triangle embedded in the tridimensional Euclidean space. In this member function we consider tuples of templated values
			 * as vertices of a triangle embedded in the tridimensional Euclidean space: the input triangle orientation is implicitly defined by the three vertices order.<p>If they do not
			 * belong to the tridimensional Euclidean space or they do not form a valid triangle, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is
			 * compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> the required normal will be stored in the current tuple of
			 * templated values and it will be normalized (i.e. its norm will become 1).
			 * \param a the first vertex of the input triangle
			 * \param b the second vertex of the input triangle
			 * \param c the third vertex of the input triangle
			 * \see mangrove_tds::Point::crossproduct(), mangrove_tds::Point::normalize()
			 */
			inline void triangNormal(const Point<T>& a,const Point<T>& b,const Point<T>& c)
			{
				Point<T> p;

				/* The three points must belong to the 3D Euclidean space, then we can compute the required normal. */
				assert(a.size()==3);
				assert(b.size()==3);
				assert(c.size()==3);
				this->updateSize(3);
				p = Point<T>::crossproduct(a-b,a-c);
				p.normalize();
				this->x() = p.cx();
				this->y() = p.cy();
				this->z() = p.cz();
			}
			
			/// This member function writes a debug description of the current tuple of templated values.
			/**
			 * This member function writes a debug description of the current tuple of templated values. In the debug representation, we write information about the current tuple of templated
			 * values.<p>This member function is intended only for debugging purposes: you should apply the writing operator in order to obtain a compact representation of the current tuple
			 * of templated values, suitable for transmitting or receiving objects.<p><b>IMPORTANT:</b> the template class T is required to support the writing operator in order to write
			 * templated values on an output stream.
			 * \param os the output stream on which a debug representation of the current tuple of templated values will be written
			 * \see mangrove_tds::Point::size(), mangrove_tds::Point::at()
			 */
			inline void debug(ostream& os = cout)
			{
				/* We must write the dimension and the templated values of the current tuple */
				os<<"Number of templated values: "<<this->size()<<endl;
				os<<"Templated values: ";
				for(unsigned int k=0;k<this->size();k++) os<<(this->at(k))<<" ";
				os<<endl;
				os.flush();
			}
			
			/// This static member function computes the scalar product among two tuples of templated values.
			/**
			 * This static member function computes the scalar product among two tuples of templated values. In this static member function, we consider two tuples of  templated values as
			 * arrays and then we compute their <i>scalar product</i> (also called <i>dot product</i>).<p>If the two input tuples do not contain the same number of components, then this
			 * static member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.<p><b>IMPORTANT:</b> the template class T is  required to support the + and * operators, used to respectively add and multiply templated values in order to compute the
			 * scalar product among two tuples of templated values.
			 * \param a the first tuple of templated values to be multiplied 
			 * \param b the second tuple of templated values to be multiplied
			 * \return the scalar product of the input tuples
			 * \see mangrove_tds::Point::crossproduct(), mangrove_tds::Point::tripleproduct()
			 */
			inline static T dotproduct(const Point<T>& a,const Point<T>& b) { return (a*b); }
			
			/// This static member function computes the cross product among two tuples of templated values.
			/**
			 * This static member function computes the cross product among two tuples of templated values. In this static member function, we consider two tuples a and b of templated values
			 * as arrays and then we compute their <i>cross product</i>, usually indicated with <i>a x b</i>.<p>The input tuples are required to belong to the tridimensional Euclidean space,
			 * then this static member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a
			 * failed assert.<p><b>IMPORTANT:</b> the template class T is required to support the - and * operators, used to respectively subtract and multiply templated values in order to
			 * compute the cross product among two tuples of templated values.
			 * \param a the first tuple of templated values to be multiplied
			 * \param b the second tuple of templated values to be multiplied
			 * \return the cross product of the input tuples
			 * \see mangrove_tds::Point::dotproduct(), mangrove_tds::Point::tripleproduct()
			 */
			inline static Point<T> crossproduct(const Point<T>& a,const Point<T>& b) { return (a^b); }
			
			/// This static member function computes the triple product among three tuples of templated values.
			/**
			 * This static member function computes the triple product among three tuples of templated values. In this static member function, we consider three tuples of templated values as
			 * arrays and then we compute their <i>triple product</i>: if a,b and c are the input tuples, then we can define their <i>triple product</i> as <i>a * (b x c)</i>.<p>The input
			 * tuples are required to belong to the tridimensional Euclidean space, then this static member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in
			 * debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> the template class T is required to support the +, - and * operators,
			 * used to respectively add, subtract and multiply templated values in order to compute the triple product.
			 * \param a the first tuple of templated values to be multiplied
			 * \param b the second tuple of templated values to be multiplied
			 * \param c the third tuple of templated values to be multiplied
			 * \return the triple product of the input tuples
			 * \see mangrove_tds::Point::dotproduct(), mangrove_tds::Point::crossproduct()
			 */
			inline static T tripleproduct(const Point<T>& a,const Point<T>& b,const Point<T>& c)
			{
				/* First, we check the input dimensions and then we compute the required triple product. */
				assert(a.size()==3);
				assert(b.size()==3);
				assert(c.size()==3);
				return Point<T>::dotproduct(a,Point<T>::crossproduct(b,c));
			}
			
			/// This operator returns a reference to a templated value in the current tuple.
			/**
			 * This operator returns a reference to a templated value in the current tuple. Each templated value is identified by its position in the current tuple of templated values: if
			 * the required position is not valid, then this operator will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.<p><b>IMPORTANT:</b> with this operator we can also update the current tuple of templated values and the template class T is required
			 * to support the assignment operator in order to assign a new templated value in the current tuple.
			 * \param i the position of the required value in the current tuple
			 * \return a reference to the required value in the current tuple
			 * \see mangrove_tds::Point::at(), mangrove_tds::Point::cx(), mangrove_tds::Point::cy(), mangrove_tds::Point::cz()
			 */
			inline T& operator[](unsigned int i)
			{
				/* First, we check the required axis and then we return the required component. */
				assert(i < this->data.size());
				return this->data[i];
			}
			
			/// This operator writes a compact description for the current tuple of templated values.
			/**
			 * This operator writes a compact description for the current tuple of templated values. In this operator, we write a compact representation of a tuple of templated values on an
			 * output stream, suitable for transmitting or receiving objects. Such representation is formatted in accordance with these guidelines:<p><i>[ number of templated values ]
			 * [ list of templated values ]</i><p>This compact representation can be used by the reading operator in order to create a new instance of this class: otherwise, you should apply
			 * the mangrove_tds::Point::debug() member function for debugging purposes.<p><b>IMPORTANT:</b> the template class T is required to support the << operator, used for writing
			 * templated values on an output stream.
			 * \param os the output stream where we can write the compact representation of a tuple of templated values
			 * \param p the tuple of templated values to be written
			 * \return the output stream after having written a compact representation of the input tuple of templated values
			 * \see mangrove_tds::Point::debug(), mangrove_tds::Point::at(), mangrove_tds::Point::size()
			 */
			inline friend ostream& operator<<(ostream& os,const Point<T>& p)
			{
				/* Repetita iuvant - description of the current tuple of templated values:
				 * <number of templated values> <templated values>. */
				os<<p.size();
				for(unsigned int i=0;i<p.size();i++) { os <<" "<<p.at(i); }
				os.flush();
				return os;
			}
			
			/// This operator rebuilds a tuple of templated values, by reading it from an input stream.
			/**
			 * This operator rebuilds a tuple of templated values, by reading it from an input stream. In this operator, we rebuild an instance of this class, by reading a compact
			 * representation of a tuple of templated values from an input stream. Such compact representation is formatted in accordance with these guidelines:<p><i>[ number of templated
			 * values ] [ list of templated values ]</i><p>This compact representation is suitable for transmitting or receving objects and it can be provided by using the writing operator.
			 * Otherwise, you should apply the mangrove_tds::Point::debug() member function for debugging purposes.<p><b>IMPORTANT:</b> the template class T is required to support the >>
			 * operator, used for reading templated values from an input stream.
			 * \param in the input stream from which we can read a compact representation of a tuple of templated values
			 * \param p the tuple of templated values to be rebuilt
			 * \return the input stream after having read a tuple of templated values
			 * \see mangrove_tds::Point::debug(), mangrove_tds::Point::updateSize()
			 */
			inline friend istream& operator>>(istream& in,Point<T> &p)
			{
				unsigned int s;
				T el;

				/* Repetita iuvant - description of the current tuple of templated values:
				 * <number of templated values> <templated values> */
				in>>s;
				p.updateSize(s);
				for(unsigned int i=0;i<p.size();i++)
				{
					in>>el;
					p[i] = el;
				}

				/* If we arrive here, we have read all the templated values. */
				return in;
			}

			/// This operator computes the sum among two tuples of templated values.
			/**
			 * This operator computes the sum among two tuples of templated values. In this operator, we consider two tuples of templated values as arrays and we compute their sum, by adding
			 * components in the same position.<p>If the two input tuples do not contain the same number of components, then this operator will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> the template class T is
			 * required to support the + operator, used for adding templated values.
			 * \param a the first tuple of templated values to be added
			 * \param b the second tuple of templated values to be added
			 * \return the resulting tuple of templated values		
			 * \see mangrove_tds::Point::updateSize()
			 */
			inline friend Point<T> operator+(const Point<T>& a,const Point<T>& b)
			{
				Point<T> temp;

				/* We compute their sum component by component, if their dimension is the same. */
				assert(a.size() == b.size());
				temp.updateSize(a.size());
				for(unsigned int i=0;i<a.size();i++) temp[i] = a.at(i) + b.at(i);
				return temp;
			}
			
			/// This operator computes the sum among two tuples of templated values.
			/**
			 * This operator computes the sum among two tuples of templated values. In this operator, we consider two tuples of templated values as arrays and we compute their difference, by
			 * subtracting components in the same position.<p>If the two input tuples do not contain the same number of components, then this operator will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> the template class T is
			 * required to support the - operator, used for subtracting templated values.
			 * \param a the first tuple of templated values to be subtracted
			 * \param b the second tuple of templated values to be subtracted
			 * \return the resulting tuple of templated values		
			 * \see mangrove_tds::Point::updateSize()
			 */
			inline friend Point<T> operator-(const Point<T>& a,const Point<T>& b)
			{
				Point<T> temp;

				/* We compute their sum component by component, if their dimension is the same. */
				assert(a.size() == b.size());
				temp.updateSize(a.size());
				for(unsigned int i=0;i<a.size();i++) temp[i] = a.at(i) - b.at(i);				
				return temp;
			}
			
			/// This operator computes the scalar product among two tuples of templated values.
			/**
			 * This operator computes the scalar product among two tuples of templated values. In this operator we consider two tuples of templated values as arrays and then we compute their
			 * <i>scalar product</i> (also called <i>dot product</i>).<p>If the two input tuples do not contain the same number of components, then this operator will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> the template class T is 
			 * required to support the + and * operators, used to respectively add and multiply templated values in order to compute the scalar product among two tuples of templated values.
			 * \param a the first tuple of templated values to be multiplied
			 * \param b the second tuple of templated values to be multiplied
			 * \return the scalar product of the input tuples of templated values
			 * \see mangrove_tds::Point::dotproduct
			 */
			inline friend T operator*(const Point<T>& a,const Point<T>& b)
			{
				T el;

				/* We compute their scalar field, if their dimension is the same. */
				assert(a.size() == b.size());
				el = 0;
				for(unsigned int i=0;i<a.size();i++) el = el + ( a.at(i) ) * ( b.at(i));
				return el;
			}
			
			/// This operator translates a tuple of templated values.
			/**
			 * This operator translates a tuple of templated values. In this operator, we consider a tuple of templated values as array and we add a scalar value to each component: we do not
			 * update the input tuple of templated values and we create a new one.<p><b>IMPORTANT:</b> the template class T is required to support the + operator, used for adding
			 * templated values.
			 * \param a the tuple of templated values to be moved
			 * \param s the scalar field to be added
			 * \return the resulting tuple of templated values
			 * \see mangrove_tds::Point::updateSize(), mangrove_tds::Point::at()
			 */
			inline friend Point<T> operator+(const Point<T> &a,const T& s)
			{
				Point<T> temp;

				/* We must add the scalar value 's' to each component of 'a'. */
				temp.updateSize(a.size());
				for(unsigned int i=0;i<a.size();i++) temp[i] = a.at(i) + s;
				return temp;			
			}
			
			/// This operator translates a tuple of templated values.
			/**
			 * This operator translates a tuple of templated values. In this operator, we consider a tuple of templated values as array and we subtract a scalar value from each component: we
			 * do not update the input tuple of templated values and we create a new one.<p><b>IMPORTANT:</b> the template class T is required to support the - operator, used for subtracting
			 * templated values.
			 * \param a the tuple of templated values to be moved
			 * \param s the scalar field to be subtracted
			 * \return the resulting tuple of templated values
 			 * \see mangrove_tds::Point::updateSize(), mangrove_tds::Point::at()
			 */
			inline friend Point<T> operator-(const Point<T> &a,const T& s)
			{
				Point<T> temp;

				/* We must subtract the scalar value 's' from each component of 'a'. */
				temp.updateSize(a.size());
				for(unsigned int i=0;i<a.size();i++) temp[i] = a.at(i) - s;
				return temp;			
			}
			
			/// This operator scales a tuple of templated values, by multiplying a scalar value to each component.
			/**
			 * This operator scales a tuple of templated values, by multiplying a scalar value to each component. In this operator, we consider a tuple of templated values as array and we
			 * multiply a scalar value to each component: we do not update the input tuple of templated values and we create a new one.<p><b>IMPORTANT:</b> the template class T is required to
			 * support the * operator, used for multiplying templated values.
			 * \param a the tuple of templated values to be multiplied
			 * \param s the scalar field to be multiplied
			 * \return the resulting tuple of templated values
			 * \see mangrove_tds::Point::updateSize(), mangrove_tds::Point::at()
			 */
			inline friend Point<T> operator*(const Point<T> &a,const T& s)
			{
				Point<T> temp;

				/* We must multiply the scalar value 's' with each component of 'a'. */
				temp.updateSize(a.size());
				for(unsigned int i=0;i<a.size();i++) temp[i] = a.at(i) * s;
				return temp;
			}
			
			/// This operator scales a tuple of templated values, by dividing each component by a scalar value.
			/**
			 * This operator scales a tuple of templated values, by dividing each component by a scalar value. In this operator, we consider a tuple of templated values as an array and then
			 * we divide each component by a scalar value: we do not update the input tuple of templated values and we create a new one.<p>If the scalar value is 0, then this operator will
			 * fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> the
			 * template class T is required to support the / operator, used for dividing templated values.
			 * \param a the tuple of templated values to be divided
			 * \param s the scalar field to be divided
			 * \return the resulting tuple of templated values
			 * \see mangrove_tds::Point::updateSize(), mangrove_tds::Point::at()
			 */
			inline friend Point<T> operator/(const Point<T> &a,const T& s)
			{
				Point<T> temp;

				/* We must divide each component of 'a' by the scalar value 's', if possible. */
				assert(!(s==0));
				temp.updateSize(a.size());
				for(unsigned int i=0;i<a.size();i++) temp[i] = a.at(i) / s;
				return temp;				
			}
			
			/// This operator translates the current tuple of templated values.
			/**
			 * This operator translates the current tuple of templated values. In this operator, we consider the current tuple of templated values as an array and then we update it, by
			 * adding a scalar value to each component.<p><b>IMPORTANT:</b> the template class T is required to support the + operator in order to add templated values.
			 * \param s the scalar field to be added
			 * \see mangrove_tds::Point::at()
			 */
			inline void operator+=(const T& s) { for(unsigned int i=0;i<this->size();i++) (*this)[i] = this->at(i) + s; }

			/// This operator translates the current tuple of templated values.
			/**
			 * This operator translates the current tuple of templated values. In this operator, we consider the current tuple of templated values as an array and then we update it, by
			 * subtracting a scalar value from each component.<p><b>IMPORTANT:</b> the template class T is required to support the - operator in order to subtract templated values.
			 * \param s the scalar field to be subtracted
			 * \see mangrove_tds::Point::at()
			 */
			inline void operator-=(const T& s) { for(unsigned int i=0;i<this->size();i++) (*this)[i] = this->at(i) - s; }

			/// This operator scales the current tuple of templated values, by multiplying a scalar value to each component.
			/**
			 * This operator scales the current tuple of templated values, by multiplying a scalar value to each component. In this operator, we consider the current tuple of templated
			 * values as an array and then we update it, by multiplying each component by a scalar value.<p><b>IMPORTANT:</b> the template class T is required to support the * operator in
			 * order to multiply templated values.
			 * \param s the scalar field to be multiplied
			 * \see mangrove_tds::Point::at()
			 */
			inline void operator*=(const T& s) { for(unsigned int i=0;i<this->size();i++) (*this)[i] = this->at(i) * s; }

			/// This operator scales the current tuple of templated values, by dividing each component by a scalar value.
			/**
			 * This operator scales the current tuple of templated values, by dividing each component by a scalar value. In this operator, we consider the current tuple of templated values
			 * as an array and then we update it, by dividing each component by a scalar value, different than 0.<p>If the scalar value is 0, then this operator will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> the template class T is
			 * required to support the / operator, used for dividing templated values.
			 * \param s the scalar field to be divided
			 * \see mangrove_tds::Point::at()
			 */
			inline void operator/=(const T& s)
			{
				/* We must divide each component of the current tuple by the scalar value 's', if possible. */
				assert(!(s==0));
				for(unsigned int i=0;i<this->size();i++) (*this)[i] = this->at(i) / s;
			}
			
			/// This operator checks if two tuples of templated values are equal.
			/** 
			 * This operator checks if two tuples of templated values are equal. In this operator, we consider a tuple of templated values as an array and then we say that two tuples of
			 * templated values are equal if and only if their templated values are equal position by position and they have the same number of elements.<p><b>IMPORTANT:</b> the template
			 * class T is required to support the == operator in order to compare templated values.
			 * \param a the first tuple of templated values to be compared
			 * \param b the second tuple of templated values to be compared
			 * \return <ul><li><i>true</i>, if the input tuples of templated values are equal</li>
			 * <li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::Point::at(), mangrove_tds::Point::size()
			 */
			inline friend bool operator==(const Point<T>& a,const Point<T>& b)
			{
				/* Each component must be equal, if they have the same dimension! */
				if(a.size()!=b.size()) return false;
				for(unsigned int i=0;i<a.size();i++) { if(!(a.at(i)==b.at(i))) return false; }
				return true;
			}
			
			/// This operator checks if two tuples of templated values are different.
			/**
			 * This operator checks if two tuples of templated values are different. In this operator, we consider a tuple of templated values as an array and then we say that two tuples of
			 * templated values are equal if and only if their templated values are equal position by position and they have the same number of elements: otherwise they are <i>different</i>.
			 * <p><b>IMPORTANT:</b> the template class T is required to support the == operator in order to compare templated values.
			 * \param a the first tuple of templated values to be compared
			 * \param b the second tuple of templated values to be compared
			 * \return <ul><li><i>true</i>, if the input tuples of templated values are different</li>
			 * <li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::Point::at(), mangrove_tds::Point::size()
			 */
			inline friend bool operator!=(const Point<T>& a, const Point<T>& b)
			{
				/* Each component must be equal, if they have the same dimension! */
				if(a.size()!=b.size()) return true;
				for(unsigned int i=0;i<a.size();i++) { if(!(a.at(i)==b.at(i))) return true; }
				return false;
			}
			
			/// This operator computes the cross product among two tuples of templated values.
			/**
			 * This operator computes the cross product among two tuples of templated values. In this operator we consider two tuples a and b of templated values as arrays and then we compute
			 * their <i>cross product</i>, usually indicated with <i>a x b</i>.<p>The input tuples are required to belong to the tridimensional Euclidean space, <p><b>IMPORTANT:</b> the
			 * template class T is required to support the - and * operators, used to respectively subtract and multiply templated values in order to compute the cross product among two
			 * tuples of templated values.
			 * \param a the first tuple of templated values to be multiplied
			 * \param b the second tuple of templated values to be multiplied
			 * \return the cross product of the input tuples
			 * \see mangrove_tds::Point::dotproduct(), mangrove_tds::Point::crossproduct(), mangrove_tds::Point::tripleproduct() 
			 */
			inline friend Point<T> operator^(const Point<T> &a,const Point<T> &b)
			{
				Point<T> temp;

				/* First, we check the input tuples dimension and then we compute a x b. */
				assert(a.size()==3);
				assert(b.size()==3);
				temp.updateSize(3);
				temp.x() = (a.cy() * b.cz()) - (a.cz() * b.cy());
				temp.y() = (a.cz() * b.cx()) - (a.cx() * b.cz());
				temp.z() = (a.cx() * b.cy()) - (a.cy() * b.cx());
				return temp;
			}
			
			protected:
			
			/// All the templated values in the current tuple.
			vector<T> data;
		};
		
		/// A brief description of a vertex.
		/**
		 * This class describes a brief descriptor of a vertex, associated to a tuple of templated values (see the mangrove_tds::Point class). The template T describes the value for tuples.
		 * Moreover, this vertex can be mapped in positions different from the initial ones: for example, when we try to repair faces in a simplicial complex.
		 * \see mangrove_tds::Point, mangrove_tds::SIMPLEX_ID, mangrove_tds::DataManager
		 */
		template <class T> class VertexDescriptor
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a new instance of this class. A vertex descriptor is composed by a tuple of templated values (see mangrove_tds::Point class). The template T
			 * describes the value for tuples. Moreover, this vertex can be mapped in positions different from the initial ones: for example, when we try to repair faces in a simplicial
			 * complex.<p><b>IMPORTANT:</b> the new descriptor is not initialized.
			 * \see mangrove_tds::Point, mangrove_tds::SIMPLEX_ID, mangrove_tds::DataManager
			 */
			inline VertexDescriptor() { ; }
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a new instance of this class. A vertex descriptor is composed by a tuple of templated values (see mangrove_tds::Point class). The template T
			 * describes the value for tuples. Moreover, this vertex can be mapped in positions different from the initial ones: for example, when we try to repair faces in a simplicial
			 * complex.
			 * \param p the tuple of templated values associated to the current vertex
			 * \param oldid the original position of the current vertex
			 * \see mangrove_tds::Point, mangrove_tds::SIMPLEX_ID, mangrove_tds::DataManager
			 */
			inline VertexDescriptor(const Point<T> &p, SIMPLEX_ID oldid)
			{
				/* We copy the input parameters! */
				this->p=Point<T>(p);
				this->oldid=oldid;
				this->newid=0;
			}
			
			/// This member function destroys an instance of this class.
			inline virtual ~VertexDescriptor() { ; }

			/// This member function returns a reference to the tuple of templated values associated to the current vertex descriptor.
			/**
			 * This member function returns a reference to the tuple of templated values associated to the current vertex descriptor. A vertex descriptor is composed by a tuple of templated
			 * values (see mangrove_tds::Point class). The template T describes the value for tuples. Moreover, this vertex can be mapped in positions different from the initial ones: for
			 * example, when we try to repair faces in a simplicial complex.
			 * \return a reference to the tuple of templated values associated to the current vertex descriptor
			 * \see mangrove_tds::Point, mangrove_tds::SIMPLEX_ID, mangrove_tds::DataManager
			 */
			inline Point<T>& getPoint() { return this->p; }
			
			/// This member function returns a constant reference to the tuple of templated values associated to the current vertex descriptor.
			/**
			 * This member function returns a constant reference to the tuple of templated values associated to the current vertex descriptor. A vertex descriptor is composed by a tuple of
			 * templated values (see mangrove_tds::Point class). The template T describes the value for tuples. Moreover, this vertex can be mapped in positions different from the initial
			 * ones: for example, when we try to repair faces in a simplicial complex.
			 * \return a constant reference to the tuple of templated values associated to the current vertex descriptor
			 * \see mangrove_tds::Point, mangrove_tds::SIMPLEX_ID, mangrove_tds::DataManager
			 */
			inline const Point<T> &getCPoint() const { return this->p; }
			
			/// This member function returns a reference to the old identifier for the current vertex descriptor.
			/**
			 * This member function returns a reference to the old identifier for the current vertex descriptor. A vertex descriptor is composed by a tuple of templated values (see
			 * mangrove_tds::Point class). The template T describes the value for tuples. Moreover, this vertex can be mapped in positions different from the initial ones: for example, when
			 * we try to repair faces in a simplicial complex.
			 * \return a reference to the old identifier for the current vertex descriptor
			 * \see mangrove_tds::Point, mangrove_tds::SIMPLEX_ID, mangrove_tds::DataManager, mangrove_tds::VertexDescriptor::getCOldID()
			 */
			inline SIMPLEX_ID& getOldID() { return this->oldid; }
			
			/// This member function returns a constant reference to the old identifier for the current vertex descriptor.
			/**
			 * This member function returns a constant reference to the old identifier for the current vertex descriptor. A vertex descriptor is composed by a tuple of templated values (see
			 * mangrove_tds::Point class). The template T describes the value for tuples. Moreover, this vertex can be mapped in positions different from the initial ones: for example, when
			 * we try to repair faces in a simplicial complex.
			 * \return a constant reference to the old identifier for the current vertex descriptor
			 * \see mangrove_tds::Point, mangrove_tds::SIMPLEX_ID, mangrove_tds::DataManager, mangrove_tds::VertexDescriptor::getOldID()
			 */
			inline const SIMPLEX_ID& getCOldID() { return this->oldid; }
			
			/// This member function returns a reference to the new identifier for the current vertex descriptor.
			/**
			 * This member function returns a refererence to the new identifier for the current vertex descriptor.<p>A vertex descriptor is composed by a tuple of templated values (see
			 * mangrove_tds::Point class). The template T describes the value for tuples. Moreover, this vertex can be mapped in positions different from the initial ones: for example, when
			 * we try to repair faces in a simplicial complex.
			 * \return a reference to the new identifier for the current vertex descriptor
			 * \see mangrove_tds::Point, mangrove_tds::SIMPLEX_ID, mangrove_tds::DataManager, mangrove_tds::VertexDescriptor::getCNewID()
			 */
			inline SIMPLEX_ID& getNewID() { return this->newid; }
			
			/// This member function returns a constant reference to the new identifier for the current vertex descriptor.
			/**
			 * This member function returns a constant refererence to the new identifier for the current vertex descriptor. A vertex descriptor is composed by a tuple of templated values (see
			 * mangrove_tds::Point class). The template T describes the value for tuples. Moreover, this vertex can be mapped in positions different from the initial ones: for example, when
			 * we try to repair faces in a simplicial complex.
			 * \return a constant reference to the new identifier for the current vertex descriptor
			 * \see mangrove_tds::Point, mangrove_tds::SIMPLEX_ID, mangrove_tds::DataManager, mangrove_tds::VertexDescriptor::getNewID()
			 */
			inline const SIMPLEX_ID& getCNewID() { return this->newid; }
			
			protected:
			
			/// The original identifier for the current vertex descriptor
			/**
			 * \see mangrove_tds::SIMPLEX_ID
			 */
			SIMPLEX_ID oldid;
			
			/// The new identifier for the current vertex descriptor.
			/**
			 * \see mangrove_tds::SIMPLEX_ID
			 */
			SIMPLEX_ID newid;
			
			/// The tuple of templated values associated to the current vertex descriptor.
			/**
			 * \see mangrove_tds::Point
			 */
			Point<T> p;
		};

		/// A comparator for points
		/**
		 * This class describes a comparator for tuples of templated values implemented by the mangrove_tds::Point class.<p>This component is based on the <i>"strictly less than"</i>
		 * comparisons by using the lexicographic order and by giving priority to the number of coordinates for the input points.
		 * \see mangrove_tds::Point
		 */
		template <class T> class ComparePoints
		{
			public:
			
			/// This operator compares two tuples of templated values.
			/**
			 * This operator implements a <i>"strictly less than"</i> comparator for tuples of templated values.<p>This component is based on the <i>"strictly less than"</i> comparisons by
			 * using the lexicographic order and by giving priority to the number of coordinates for the input points.
			 * \param first the first tuple of templated values to be compared
		 	 * \param second the second tuple of templated values to be compared
		 	 * \return <ul><li><i>true</i>, if the first tuple is <i>"strictly less than"</i> the second one</li><li><i>false</i>, otherwise</li></ul>
		 	 * \see mangrove_tds::Point
		 	 */
		 	bool operator() (const Point<T> &first, const Point<T> &second) const
		 	{
		 		/* First, we give priority to the points size and then to the lexicographic ordering over the coordinates! */
		 		if(first.size() < second.size()) return true;
		 		if(first.size() > second.size()) return false;
		 		for(unsigned int i=0;i<first.size();i++)
		 		{
		 			/* Now, we apply the lexicographic order. */
		 			if(first.at(i)<second.at(i)) return true;
		 			if(first.at(i)>second.at(i)) return false;
		 		}
		 		
		 		/* If we arrive here, the first vector is not 'strictly less than' the second one. */
		 		return false;
		 	}
		 	
		 	/// This operator compares two vertices descriptions.
			/**
			 * This operator implements a <i>"strictly less than"</i> comparator for vertices description (see mangrove_tds::VertexDescriptor).<p>This component is based on the
			 * <i>"strictly less than"</i> comparisons by using the lexicographic order and by giving priority to the number of coordinates for the input tuple for each descriptor (see the
			 * mangrove_tds::Point class).
			 * \param first the first vertex descriptor to be compared
			 * \param second the second vertex descriptor to be compared
		 	 * \return <ul><li><i>true</i>, if the first vertex descriptor is <i>"strictly less than"</i> the second one</li><li><i>false</i>, otherwise</li></ul>
		 	 * \see mangrove_tds::Point, mangrove_tds::VertexDescriptor
		 	 */
		 	bool operator() (const VertexDescriptor<T> &first, const VertexDescriptor<T> &second) const
		 	{
		 		if(first.getCPoint().size() < second.getCPoint().size()) return true;
		 		if(first.getCPoint().size() > second.getCPoint().size()) return false;
		 		for(unsigned int i=0;i<first.getCPoint().size();i++)
		 		{
		 			/* Now, we apply the lexicographic order. */
		 			if(first.getCPoint().at(i)<second.getCPoint().at(i)) return true;
		 			if(first.getCPoint().at(i)>second.getCPoint().at(i)) return false;
		 		}
		 		
		 		/* If we arrive here, the first vector is not 'strictly less than' the second one. */
		 		return false;
		 	}
		};
		
		/// An alias for a set of points with double values as coordinates.
		/**
		 * \see mangrove_tds::Point, mangrove_tds::ComparePoints
		 */
		typedef set< Point<double>, ComparePoints<double> > VERTICES_SET;
		
		/// An alias for a mapper among points with double values and simplices identifiers.
		/**
		 * \see mangrove_tds::Point, mangrove_tds::SIMPLEX_ID, mangrove_tds::ComparePoints
		 */
		typedef map< Point<double>, SIMPLEX_ID, ComparePoints<double> > VERTICES_MAPPER;
		
		/// An alias for a mapper among points and field values associated to vertices.
		/**
		 * \see mangrove_tds::Point, mangrove_tds::SIMPLEX_ID, mangrove_tds::ComparePoints
		 */
		typedef map< Point<double>, double, ComparePoints<double> > VERTICES_FIELDS;
		
		/// An alias for a set of vertices descriptions.
		/**
		 * \see mangrove_tds::VertexDescriptor, mangrove_tds::ComparePoints
		 */
		typedef set< VertexDescriptor<double>, ComparePoints<double> > VDESCR_SET;
		
		/// This function computes the rotation sense for a turn in 3D between four points.
		/**
		 * This function computes the rotation sense for a turn in 3D between four points, described through 3 instances of the mangrove_tds::Point class.<p><b>IMPORTANT:</b> we assume that each
		 * point has 3 components. If this condition is not satisfied, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case,
		 * each forbidden operation will result in a failed assert.
		 * \param v0 the first vertex to be considered
		 * \param v1 the second vertex to be considered
		 * \param v2 the third vertex to be considered
		 * \param v3 the fourth vertex to be considered
		 * \return the rotation sense for a turn in 3D between four points, that can be:<ul><li>mangrove_tds::CCW, if the rotation is counter-clockwise</li>
		 * <li>mangrove_tds::CW, if the rotation is clock-wise</li><li>mangrove_tds::ALIGNED, if points are aligned</li></ul>
		 * \see mangrove_tds::CCW, mangrove_tds::CW, mangrove_tds::ALIGNED, mangrove_tds::Point::tripleproduct()
		 */
		template <class T> int turn(Point<T> v0, Point<T> v1, Point<T> v2, Point<T> v3)
		{
			T d;
			
			/* Now, we should compute a triple product! */
			d=Point<T>::tripleproduct(Point<T>(v3-v1),Point<T>(v0-v1),Point<T>(v2-v1));
			if(d<0) return CCW;
			else if(d>0) return CW;
			else return ALIGNED;
		}
	}
	
#endif

