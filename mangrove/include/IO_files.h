/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library                                   
 *                                                                           				    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * January 2011 (Revised on May 2012)
 *                                                                    
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * IO_files.h - cumulative headers for I/O components between simplicial complexes and files.
 ***********************************************************************************************/

/* Should we include this header file? */
#ifndef IO_FILES_H

	#define IO_FILES_H

	#include "off_manager.h"
	#include "abaqus_manager.h"
	#include "gmv_manager.h"
	#include "ts_manager.h"
	using namespace mangrove_tds;
	using namespace std;
	
	/// Components for I/O between simplicial complexes and files.
	/**
	 * The functions defined in this file offers some facilities for choosing and building a component for the effective I/O operations between simplicial complexes and files.
	 * \file IO_files.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	 
	/* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		/// This function checks if a file is supported.
		/**
 		 * We check only the extension in order to understand if a file is supported: we support only files with extensions <i>off</i>, <i>gmv</i>, <i>inp</i>, and <i>ts</i>, i.e. files written
 		 * in <i>OFF</i>, <i>GMV</i>, <i>Abaqus</i> and <i>TS</i> format. Such files are respectively managed by instances of the mangrove_tds::OFFManager,
 		 * mangrove_tds::GMVManager, mangrove_tds::AbaqusManager and mangrove_tds::TSManager classes.
 		 * \param ext the extension of the file to be checked
 		 * \return <ul><li><i>true</i>, if the file to be checked is supported</li><li><i>false</i>, otherwise</li></ul>
 		 * \see mangrove_tds::DataManager, mangrove_tds::IO, mangrove_tds::OFFManager, mangrove_tds::GMVManager, mangrove_tds::AbaqusManager, mangrove_tds::TSManager
 		 */
		bool isSupportedFormat(QString ext)
		{
			/* The supported extensions are gmv, off and inp! */
			if( ext==QString("gmv")) return true;
			if( ext==QString("off")) return true;
			if( ext==QString("inp")) return true;
			if( ext==QString("ts")) return true;
			return false;
		}
	}

#endif

