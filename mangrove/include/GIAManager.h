/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library                                   
 *  		    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * October 2011 (Revised on May 2012)
 *                                                               
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * GIAManager.h - the specialized component for executing I/O operations on the IA* data structure
 ***********************************************************************************************/

/* Should we include this header file? */
#ifndef GIA_MANAGER_H

	#define GIA_MANAGER_H

	#include "GIA.h"
	#include <list>
	#include <map>
	#include <queue>
	#include <algorithm>
	#include <cstdlib>
	using namespace std;
	using namespace mangrove_tds;

	/// The specialized component for executing I/O operations on the <i>IA*</i> data structure.
	/**
	 * The class defined in this file allows to execute I/O operations on the <i>IA*</i> data struture.<p>The <i>IA*</i> data structure, described by the mangrove_tds::GIA class, is a
	 * dimension-independent and incidence-based data structure for representing simplificial complexes with an arbitrary domain. It extends the <i>IA</i> data structure and it is scalable to
	 * manifold complexes, and supports efficient navigation and topological modifications. The <i>IA*</I> data structure encodes only top simplices plus a suitable subset of the adjacency relations,
	 * thus it is a <i>local</i> data structure.<p>The effective I/O operations on the <i>IA*</i> data structure are delegated to a particular instance of the mangrove_tds::DataManager class in order
	 * to support a wide range of I/O formats.
	 * \file GIAManager.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	
	/* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		/// The specialized component for executing I/O operations on the <i>IA*</i> data structure.
		/**
		 * The class defined in this file allows to execute I/O operations on the <i>IA*</i> data struture.<p>The <i>IA*</i> data structure, described by the mangrove_tds::GIA class, is a
		 * dimension-independent data structure for representing simplificial complexes with an arbitrary domain. It extends the <i>IA</i> data structure and it is scalable to manifold complexes,
		 * and supports efficient navigation and topological modifications. The <i>IA*</I> data structure encodes only top simplices plus a suitable subset of the adjacency relations, thus it is
		 * a <i>local</i> data structure.<p>The effective I/O operations on the <i>IA*</i> data structure are delegated to a particular instance of the
		 * mangrove_tds::DataManager class in order to support a wide range of I/O formats.
	 	 * \see mangrove_tds::DataManager, mangrove_tds::IO, mangrove_tds::GIA, mangrove_tds::BaseSimplicialComplex
	 	 */
		class GIAManager : public IO<GIAManager>
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a not initialized instance of this class: you should apply the mangrove_tds::GIAManager::createComplex() member functions in order to initialize it.
			 * \see mangrove_tds::GIAManager::createComplex(), mangrove_tds::GIA, mangrove_tds::BaseSimplicialComplex
			 */
			inline GIAManager() : IO<GIAManager>() { this->ccw = NULL; }
			
			/// This member function destroys an instance of this class.
			/**
			 * This member function destroys an instance of this class. The specialized data structure for the simplicial complex is created in the mangrove_tds::GIAManager::createComplex()
			 * member function, thus you must not manually destroy it: you should apply this destructor or the mangrove_tds::GIAManager::close() member function in order to destroy all the
			 * internal state.
			 * \see mangrove_tds::GIAManager::createComplex(), mangrove_tds::GIAManager::close()
			 */
			inline virtual ~GIAManager() { this->close(); }
			
			/// This member function destroys all the internal state in this component.
			/**
			 * This member function destroys all the internal state in this component. The specialized data structure for the simplicial complex is created in the
			 * mangrove_tds::GIAManager::createComplex() member function, thus you must not manually destroy it: you should apply the destructor or this member function in order to destroy
			 * all the internal state.
			 * \see mangrove_tds::GIAManager::createComplex(), mangrove_tds::GIA, mangrove_tds::BaseSimplicialComplex
			 */
			inline void close()
			{
				/* Now, we reset the current state! */
				if(this->ccw!=NULL) delete this->ccw;
				this->ccw=NULL;
			}
			
			/// This member function returns the simplicial complex created by the current component.
			/**
			 * This member function returns the simplicial complex created in the mangrove_tds::GIAManager::createComplex() member function. The current instance of the <i>IA*</I> data
			 * structure has been created in the mangrove_tds::GIAManager::createComplex() member function, thus you must not manually destroy it. You should apply the destructor or the
			 * mangrove_tds::GIAManager::close() member function in order to destroy all the internal state. Thus, the input pointer must be a C++ pointer to an instance of the
			 * mangrove_tds::GIA class.<p>If such conditions are not satisfied, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In
			 * this case, each forbidden operation will result in a failed assert.
			 * \param cs the simplicial complex created by this component
			 * \see mangrove_tds::GIAManager::close(), mangrove_tds::GIAManager::createComplex(), mangrove_tds::GIA, mangrove_tds::BaseSimplicialComplex
			 */
			template <class D> void getComplex(D** cs)
			{
				D *aux;
				
				/* First, we check if all is ok! */
				assert(this->ccw!=NULL);
				aux = dynamic_cast<D*>(this->ccw);
				assert(aux!=NULL);
				(*cs) = this->ccw;
			}
			
			/// This member function returns a string that describes the data structure created by the current I/O component.
 			/**
 			 * This member function returns a string that describes the data structure created by the current I/O component, and used for encoding the current simplicial complex.
			 * \return a string that describes the data structure created by the current I/O component
 			 * \see mangrove_tds::BaseSimplicialComplex::getDataStructureName()
 			 */
 			inline string getDataStructureName() { return string("Generalized Indexed data structure with Adjacencies (IA*)"); }
 			
 			/// This member function creates a simplicial complex by reading it from a file.
			/**
			 * This member function generates a <i>IA*</I> data structure from a file that contains a possible representation: the most common exchange format for a simplicial complex
			 * consists of a collection of <i>top</i> simplices described by their vertices, but it is not the unique possibility.<p>The <i>IA*</i> data structure, described by the
			 * mangrove_tds::GIA class, is a dimension-independent data structure for representing simplificial complexes with an arbitrary domain. It extends the <i>IA</i> data structure and
			 * it is scalable to manifold complexes, and supports efficient navigation and topological modifications. The <i>IA*</I> data structure encodes only top simplices plus a suitable
			 * subset of the adjacency relations, thus it is a <i>local</i> data structure.<p>In this member function we can customize the required simplicial complex: for instance, we can
			 * attach the Euclidean coordinates and the field value for each vertex in the simplicial complex to be built (if supported in the I/O component). Such information is stored as
			 * local properties (i.e. as subclasses of the mangrove_tds::LocalPropertyHandle class) associated to vertices. You can retrieve them through their names, respectively provided by
			 * the mangrove_tds::DataManager::getPropertyName4Coordinates() and mangrove_tds::DataManager::getPropertyName4FieldValue() member function. Moreover, you can check what 
			 * properties are supported through the mangrove_tds::DataManager::supportsEuclideanCoordinates() and mangrove_tds::DataManager::supportsFieldValues() member functions.<p>If we
			 * cannot complete these operations for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case,
			 * each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> the specialized data structure for the simplicial complex is created in this member function, thus
			 * you must not manually destroy it. You should apply the destructor or mangrove_tds::GIAManager::close() member function in order to destroy all the internal state.
			 * \param loader a component for the effective data reading
	 	 	 * \param fname the path of the input file
			 * \param reqParams this boolean flag identfies if the loading of the auxiliary parameters encoded in the input file is enabled
			 * \param ld this boolean flag indicates if the effective reading of the input data must be performed
			 * \param checksOn this boolean flag indicates if the effective checks on the input data must be performed (valid only if the effective reading must be performed)
			 * \see mangrove_tds::DataManager, mangrove_tds::GIA, mangrove_tds::RawFace, mangrove_tds::RawIncidence, mangrove_tds::RawAdjacency, mangrove_tds::PropertyBase,
			 * mangrove_tds::LocalPropertyHandle, mangrove_tds::BaseSimplicialComplex, mangrove_tds::GIAManager::close(), mangrove_tds::GIAManager::computeAdjacency(),
			 * mangrove_tds::GIAManager::fixGIACoboundary(), mangrove_tds::GIAManager::fixGIAAdjacency()
			 */
			template <class T> void createComplex(DataManager<T> *loader, string fname,bool reqParams,bool ld,bool checksOn)
			{
				bool b;
				double aaa;
				LocalPropertyHandle< Point<double> > *pnt_prop;
				LocalPropertyHandle<double> *fld_prop;
				SimplexIterator it;
				unsigned int k,i,j;
				SimplexPointer cp;
				Point<double> p;
				vector<RawIncidence> incidence;
				vector< vector<RawFace> > raw_faces;
				vector< vector<RawAdjacency> > raw_ad;
				
				/* First, we check if all is ok and then we run the effective construction! */
				assert(loader!=NULL);
				this->close();
				if(ld) loader->loadData(fname,reqParams,false,checksOn);
				b=(loader->supportsEuclideanCoordinates()==true) || (loader->supportsFieldValues()==true);
				if(reqParams) assert(b==true);
				try { this->ccw = new GIA(loader->getDimension()); }
				catch(bad_alloc ba) { this->ccw = NULL; }
				assert(this->ccw!=NULL);
				aaa=0.0;
				pnt_prop=NULL;
				fld_prop=NULL;
				if(reqParams)
				{
					/* What properties must be added? First, we check the Euclidean coordinates */
					if(loader->supportsEuclideanCoordinates())
					{
						pnt_prop=this->ccw->addLocalProperty(0,string(loader->getPropertyName4Coordinates()), Point<double>());
						this->ccw->getPropertyName4Coordinates()=string(loader->getPropertyName4Coordinates());
					}
					
					/* Now, we check the field values! */
					if(loader->supportsFieldValues())
					{
						fld_prop=this->ccw->addLocalProperty(0,string(loader->getPropertyName4FieldValue()),aaa); 
						this->ccw->getPropertyName4FieldValue()=string(loader->getPropertyName4FieldValue());
					}
				}
				
				/* Now, we update 'raw_faces' and 'raw_ad' */
				for(SIMPLEX_TYPE d=0;d<loader->getDimension();d++)
				{
					raw_faces.push_back(vector<RawFace>());
					raw_ad.push_back(vector<RawAdjacency>());
				}
				
				/* Now, we can add all the vertices and their properties! */
				it = loader->getCollection(0).begin();
				k=0;
				while(it!=loader->getCollection(0).end())
				{
					/* Now, we insert the k-vertex in 'ccw' */
					incidence.push_back(RawIncidence());
					this->ccw->addSimplex(cp);
					if(reqParams)
					{
						/* Now, we retrieve the Euclidean coordinates, if supported! */						
						if(loader->supportsEuclideanCoordinates())
						{
							loader->getCoordinates(k,p);
							pnt_prop->set(cp,Point<double>(p));
						}
						
						/* Now, we retrieve the field values, if supported! */
						if(loader->supportsFieldValues())
						{
							loader->getFieldValue(k,aaa);
							fld_prop->set(cp,aaa);
						}
					}
					
					/* Now, we move on the next vertex! */
					k=k+1;
					it++;
				}
				
				/* Now, we can add the other top simplices and initialize auxiliary data structures */
				if(loader->getDimension()==0) return;
				for(SIMPLEX_TYPE d=1;d<=loader->getDimension();d++)
				{
					/* Now, we add all the top d-simplices! */
					for(it=loader->getCollection(d).begin();it!=loader->getCollection(d).end();it++)
					{
						/* Now, we add a new top simplex and then we update its boundary relation! */
						this->ccw->addSimplex(d,cp);
						if(d>=2) raw_ad[d-1].push_back(RawAdjacency(d));
						for(k=0;k<=d;k++)
						{
							i=loader->getCollection(d).simplex(cp.id()).b(k).id();
							this->ccw->simplex(cp).add2Boundary(SimplexPointer(0,i),k);
							if(cp.type()==1) { this->ccw->simplex(SimplexPointer(0,i)).add2Coboundary(SimplexPointer(cp)); }
							else
							{
								incidence[i].addSimplex(cp);
								raw_faces[d-1].push_back(RawFace());
								raw_faces[d-1].back().isTopSimplexChild()=true;
								raw_faces[d-1].back().getIndex()=cp.id();
								raw_faces[d-1].back().getReverseIndex()=k;
								for(j=0;j<k;j++) { raw_faces[d-1].back().getVertices().push_back(loader->getCollection(d).simplex(cp.id()).b(j).id()); }
								for(j=k+1;j<=d;j++) { raw_faces[d-1].back().getVertices().push_back(loader->getCollection(d).simplex(cp.id()).b(j).id()); }
							}
						}
					}
				}
				
				/* If we are managing a 1-dimensional complex, we can exit! */
				if(loader->getDimension()==1)
				{
					raw_faces.clear();
					raw_ad.clear();
					return;
				}
				
				/* Now, we identify the partial co-boundary relation for each vertex and the adjacency relation */
				this->computeAdjacency(raw_faces,raw_ad);
				raw_faces.clear();
				for(it=this->ccw->begin(0);it!=this->ccw->end(0); it++) this->fixGIACoboundary(SimplexPointer(it.getPointer()),incidence,raw_ad);
				this->fixGIAAdjacency(incidence,raw_ad);
				incidence.clear();
				raw_ad.clear();
			}

			/// This member function exports the current simplicial complex in accordance with a certain format.
			/**
			 * This member function exports the current simplicial complex, by writing its representation in accordance with a specific file format.<p>The effective writing is delegated to a
			 * subclass of the mangrove_tds::DataManager class and some further conditions could be checked and verified: for example, the current simplicial complex must support the
			 * extraction of particular auxiliary information (like the Euclidean coordinates and/or scalar field values, described by subclasses of the mangrove_tds::PropertyBase class). If 
			 * such properties are not satisfied, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.
			 * \param writer a component for the effective data writing (we write the data in this member function)
			 * \param fname the path of the output file
			 * \see mangrove_tds::DataManager, mangrove_tds::IO::close(), mangrove_tds::BaseSimplicialComplex, mangrove_tds::PropertyBase
			 */
			template <class T> void writeComplex(DataManager<T> *writer,string fname)
			{
				/* First, we check if all is ok! */
				assert(writer!=NULL);
				writer->writeData(this->ccw,fname);
			}
			
			protected:
			
			/// The simplicial complex created by this component.
			/**
			 * \see mangrove_tds::GIA
			 */
			GIA* ccw;
			
			/// This member function extracts all 0-simplices belonging to a subface of a simplex directly encoded in the current <i>IA*</i> data structure.
			/**
			 * This member function extracts all 0-simplices (namely vertices) belonging to a subface of a simplex directly encoded in the current <i>IA*</i> data structure: the k-th face of a
			 * simplex s is generated by discarding the vertex in position k in the boundary of s (restricted to vertices).
			 * \param cp a pointer to a simplex directly encoded in the current <i>IA*</i> data structure
			 * \param k the position of the required subface of the input simplex
			 * \param face the identifiers of the required subface vertices (their dimension is implicit)
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointer, mangrove_tds::GIAManager::fixGIAAdjacency()
			 */
			inline void findVertices(const SimplexPointer& cp,unsigned int k,vector<SIMPLEX_ID> &face)
			{
				unsigned int j;
				
				face.clear();
				for(j=0;j<k;j++) face.push_back(this->ccw->simplex(cp).bc(j).id());
				for(j=k+1;j<=cp.type();j++) face.push_back(this->ccw->simplex(cp).bc(j).id());
			}
			
			/// This member function checks if at least one simplex directly encoded in the in the current <i>IA*</i> data structure is incident in a face.
			/**
			 * This member function checks if at least one simplex directly encoded in the in the current <i>IA*</i> data structure is incident in a face: we assume to choose such simplex (if
			 * it exists) among an input list of simplices. Moreover, we assume a face is completely described by all vertices belonging to its boundary. In other words, we check if at least
			 * one of the input simplices is incident in all vertices of the input face: the incidence relation for each vertex is described by an instance of the mangrove_tds::RawIncidence
			 * class.
			 * \param face the identifiers of all vertices belonging to the boundary of the required face
			 * \param smps the input list of simplices
			 * \param inc information about the raw incidence for a 0-simplex
			 * \return <ul><li><i>true</i>, if at least one simplex of the input list of simplices is incident in the input face</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointer, mangrove_tds::RawIncidence, mangrove_tds::GIAManager::fixGIAAdjacency(),
			 *  mangrove_tds::GIAManager::isIncident(), mangrove_tds::GIAManager::findVertices()
			 */
			inline bool existIncidentSimplex( vector<SIMPLEX_ID> &face, vector<SimplexPointer> &smps, vector<RawIncidence> &inc)
			{
				/* We can start our research! */
				for(vector<SimplexPointer>::iterator cp=smps.begin();cp!=smps.end();cp++) { if(this->isIncident(SimplexPointer(*cp),face,inc)) return true; }
				return false;
			}
			
			/// This member function checks if a simplex directly encoded in the in the current <i>IA*</i> data structure is incident in a face.
			/**
			 * This member function checks if a simplex directly encoded in the in the current <i>IA*</i> data structure is incident in a face. A face is completely described by all vertices
			 * belonging to its boundary. In other words, we check if the input simplex is incident in all vertices of the input face: the incidence relation for each vertex is described by
			 * an instance of the mangrove_tds::RawIncidence class.
			 * \param cp a pointer to a simplex directly encoded in the current <i>IA*</i> data structure
			 * \param face the identifiers of all vertices belonging to the boundary of the required face
			 * \param inc information about the raw incidence for a 0-simplex
			 * \return <ul><li><i>true</i>, if the input simplex is incident in the input face</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointer, mangrove_tds::RawIncidence, mangrove_tds::GIAManager::fixGIAAdjacency(),
			 * mangrove_tds::GIAManager::existIncidentSimplex(), mangrove_tds::GIAManager::findVertices()
			 */
			inline bool isIncident(const SimplexPointer& cp,vector<SIMPLEX_ID> &face,vector<RawIncidence> &inc)
			{
				vector<SimplexPointer>::iterator t,b;
				
				/* First, we check if 'cp' is incident in all vertices in 'face' */
				for(unsigned int k=1;k<face.size();k++)
				{
					t=inc[ face[k]].getSimplices().begin();
					b=inc[ face[k]].getSimplices().end();
					if( find(t,b,cp)==b) return false;
				}
				
				/* If we arrive here, 'cp' is incident in all vertices in 'face' */
				return true;
			}
			
			/// This member function identifies the adjacency relation in the current <i>IA*</i> data structure.
			/**
			 * This member function identifies the adjacency relation in the current <i>IA*</i> data structure. We restrict the adjacency only to top k-simplices. Adjacency relation is
			 * encoded in an efficient way (see the mangrove_tds::Simplex class): if the shared (k-1)-face is manifold (i.e. it is shared by at most two k-simplices), then the adjacent
			 * simplex is directly encoded, otherwise we encode a pointer to a (k-1)-face and thus we can access its co-boundary. In this way, we obtain a unique list of adjacent simplices,
			 * independently from their number.<p>In this member function, we apply raw information for adjacency between simplices belonging to the current <i>IA*</i> data structure,
			 * generated through the mangrove_tds::GIAManager::computeAdjacency() and encoded through the mangrove_tds::RawAdjacency class. In this way, we can identify what simplices are
			 * adjacent to an other one: the adjacency is organized by each sub-face.<p>Moreover, we apply the raw information about simplices incident in a 0-simplex provided by the
			 * mangrove_tds::RawIncidence class.
			 * \param inc information about the raw incidence for a 0-simplex
			 * \param raw_ad information about the raw adjacency for top simplices
			 * \see mangrove_tds::GIA, mangrove_tds::RawIncidence, mangrove_tds::RawAdjacency, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID,
			 * mangrove_tds::GIAManager::computeAdjacency(), mangrove_tds::GIAManager::createComplex(), mangrove_tds::GIAManager::findVertices(),
			 * mangrove_tds::GIAManager::existIncidentSimplex(), mangrove_tds::GIAManager::isIncident()
			 */
			inline void fixGIAAdjacency(vector<RawIncidence>& inc, vector< vector<RawAdjacency> >& raw_ad)
			{
				unsigned int k,j,lgj,i,a,b;
				SimplexPointer cp;
				vector<SIMPLEX_ID> face;
				vector<SimplexPointer> smps;

				/* We identify adjacency relation starting from top triangles! */
				for(SIMPLEX_TYPE d=2;d<=this->ccw->type();d++)
				{
					/* Are we on maximal simplices? */
					if(d==this->ccw->type())
					{
						/* We are on maximal simplices: now, we iterate on 'raw_ad' */
						for(k=0;k<raw_ad[d-1].size();k++)
						{
							/* Now, we iterate over the faces in the current simplex in 'raw_ad[d-1]' */
							for(j=0;j<=d;j++)
							{
								/* Now, we check how many simplices are incident along the j-th face in the current simplex in 'raw_ad[d-1]' */
								lgj=raw_ad[d-1][k].getSimplices()[j].size();
								if(lgj==0) { ; }
								else if(lgj==1) { this->ccw->simplex(SimplexPointer(d,k)).add2Adjacency(SimplexPointer(raw_ad[d-1][k].getSimplices()[j][0]),j); }
								else
								{
									/* Now, we found a non-manifold connection: first, we check if it is already marked as non-manifold! */
									if(raw_ad[d-1][k].isSingularity(j)==false)
									{
										/* We have a new non-manifold connection! */
										this->ccw->addSimplex(d-1,cp);
										raw_ad[d-1][k].addSingularity(cp,j);
										this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(d,k));
										this->ccw->simplex(SimplexPointer(d,k)).add2Adjacency(SimplexPointer(cp),j);
										for(i=0;i<lgj;i++)
										{
											a=raw_ad[d-1][k].getSimplices()[j][i].id();
											b=raw_ad[d-1][k].getReverseIndices()[j][i];
											raw_ad[d-1][a].addSingularity(cp,b);
											this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(d,a));
										}
									}
									else
									{
										/* We have an old non-manifold connection! */
										raw_ad[d-1][k].getSingularity(j,cp);
										this->ccw->simplex(SimplexPointer(d,k)).add2Adjacency(SimplexPointer(cp),j);
									}
								}
							}
						}
					}
					else
					{
						/* We are on not maximal simplices: now, we iterate over 'raw_ad' */
						for(k=0;k<raw_ad[d-1].size();k++)
						{
							/* Now, we iterate over the faces in the current simplex in 'raw_ad[d-1]' */
							for(j=0;j<=d;j++)
							{
								/* Now, we check how many simplices are incident along the j-th face in the current simplex in 'raw_ad[d-1]' */
								lgj=raw_ad[d-1][k].getSimplices()[j].size();
								if(lgj>=2)
								{
									/* Surely, this adjacency is non-manifold: now, we check if it is already marked as non-manifold! */
									if(raw_ad[d-1][k].isSingularity(j)==false)
									{
										/* We have a new non-manifold connection! */
										this->ccw->addSimplex(d-1,cp);
										raw_ad[d-1][k].addSingularity(cp,j);
										this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(d,k));
										this->ccw->simplex(SimplexPointer(d,k)).add2Adjacency(SimplexPointer(cp),j);
										for(i=0;i<lgj;i++)
										{
											a=raw_ad[d-1][k].getSimplices()[j][i].id();
											b=raw_ad[d-1][k].getReverseIndices()[j][i];
											raw_ad[d-1][a].addSingularity(cp,b);
											this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(d,a));
										}
									}
									else
									{
										/* We have an old non-manifold connection! */
										raw_ad[d-1][k].getSingularity(j,cp);
										this->ccw->simplex(SimplexPointer(d,k)).add2Adjacency(SimplexPointer(cp),j);
									}
								}
								else
								{
									/* This adjacency could be non-manifold: we check if a simplex with dimension greater than d and incident in this face exist! */
									this->findVertices(SimplexPointer(d,k),j,face);
									inc[face[0]].filterSimplices(d+1,smps);
									if(this->existIncidentSimplex(face,smps,inc)==true)
									{
										/* The connection is non-manifold: is it new? */
										if(raw_ad[d-1][k].isSingularity(j)==false)
										{
											/* We have a new non-manifold connection! */
											this->ccw->addSimplex(d-1,cp);
											raw_ad[d-1][k].addSingularity(cp,j);
											this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(d,k));
											this->ccw->simplex(SimplexPointer(d,k)).add2Adjacency(SimplexPointer(cp),j);
											for(i=0;i<lgj;i++)
											{
												a=raw_ad[d-1][k].getSimplices()[j][i].id();
												b=raw_ad[d-1][k].getReverseIndices()[j][i];
												raw_ad[d-1][a].addSingularity(cp,b);
												this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(d,a));
											}
										}
										else
										{
											/* We have an old non-manifold connection! */
											raw_ad[d-1][k].getSingularity(j,cp);
											this->ccw->simplex(SimplexPointer(d,k)).add2Adjacency(SimplexPointer(cp),j);
										}
									}
									else
									{
										/* The connection is manifold! */
										if(lgj==0) { ; }
										else { this->ccw->simplex(SimplexPointer(d,k)).add2Adjacency(SimplexPointer(raw_ad[d-1][k].getSimplices()[j][0]),j); }
									}
								}
							}
						}		
					}
				}
			}
			
			/// This member function identifies all the clusters of simplices in the star of a 0-simplex belonging to the current <i>IA*</i> data structure.
			/**
			 * This member function identifies all the clusters of simplices in the star of a 0-simplex (namely a vertex) belonging to the current <i>IA*</i> data structure. We apply the raw
			 * information for adjacency between simplices belonging to the current <i>IA*</i> data structure, provided by the mangrove_tds::GIAManager::computeAdjacency() member
			 * function.
			 * \param cp a pointer to the required 0-simplex in the current <i>IA*</i> data structure
			 * \param inc the complete coboundary relation for all the vertices in the current <i>IA*</i> data structure (restricted to top simplices)
			 * \param ad the raw information for adjacency between top simplices belonging to the current <i>IA*</i> data structure
			 * \see mangrove_tds::GIA, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle,
			 * mangrove_tds::BaseSimplicialComplex, mangrove_tds::BaseSimplicialComplex::addGlobalProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty(),
			 * mangrove_tds::BaseSimplicialComplex::getPropertyValue(), mangrove_tds::GhostSimplexPointer, mangrove_tds::RawIncidence, mangrove_tds::RawAdjacency,
			 * mangrove_tds::GIAManager::computeAdjacency(), mangrove_tds::GIAManager::fixGIAAdjacency()
			 */
			inline void fixGIACoboundary(const SimplexPointer& cp,vector<RawIncidence> &inc,vector< vector<RawAdjacency> > &ad)
			{
				unsigned int lg;
				GlobalPropertyHandle<bool> *cprop,*asp,*vp;
				bool b;
				vector<SimplexPointer>::iterator it,s1;
				SimplexPointer sigma;

				/* First, we consider the number of top simplices incident in 'cp' */
				lg=inc[cp.id()].getSimplices().size();
				if(lg==0) { return; }
				else if(lg==1) { this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(inc[cp.id()].getSimplices().at(0))); }
				else
				{
					/* There are more than 1 top simplex, we should identify clusters of top simplices */
					cprop=this->ccw->addGlobalProperty(string("candidates"), false);
					asp=this->ccw->addGlobalProperty(string("assigned"),false);
					b=false;
					for(it=inc[cp.id()].getSimplices().begin();it!=inc[cp.id()].getSimplices().end();it++) { cprop->set(SimplexPointer(*it),true); }
					for(it=inc[cp.id()].getSimplices().begin();it!=inc[cp.id()].getSimplices().end();it++)
					{
						/* Now, we look for the cluster incident in 'it' if and only if has not been assigned to an other cluster! */
						if(asp->get(SimplexPointer(*it))==false)
						{
							queue<SimplexPointer> q;
							
							/* A new cluster starts here! */
							b=false;
							vp=this->ccw->addGlobalProperty(string("visited"),false);
							q.push(SimplexPointer(*it));
							while(q.empty()==false)
							{
								/* Now, we extract the first simplex from q and then we proceed on its adjacent! */
								sigma=q.front();
								q.pop();
								if(vp->get(SimplexPointer(sigma))==false)
								{
									/* Now, we can analyze 'sigma' */
									vp->set(SimplexPointer(sigma),true);
									asp->set(SimplexPointer(sigma),true);
									if(b==false)
									{
										this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(sigma));
										b=true;
									}
									
									/* Now, we can analyze the adjacents of 'sigma', incident in 'it' */
									for(unsigned int k=0;k<=sigma.type();k++)
									{
										for(s1=ad[sigma.type()-1][sigma.id()].getSimplices()[k].begin();s1!=ad[sigma.type()-1][sigma.id()].getSimplices()[k].end();s1++)
											{ if(cprop->get(SimplexPointer(*s1))==true) q.push(SimplexPointer(*s1)); }
									}
								}
							}	
							
							/* If we arrive here, we have identified the required cluster! */
							this->ccw->deleteProperty(string("visited"));
							vp=NULL;
						}
					}

					/* If we arrive here, we can remove 'properties' */
					this->ccw->deleteProperty(string("candidates"));
					this->ccw->deleteProperty(string("assigned"));
					if(this->ccw->hasProperty(string("visited"))) this->ccw->deleteProperty(string("visited"));
				}
			}
			
			/// This member function generates raw information for adjacency between simplices belonging to the current <i>IA*</i> data structure.
			/**
			 * This member function generates raw information for adjacency between simplices belonging to the current <i>IA*</i> data structure, encoded in the
			 * mangrove_tds::RawAdjacency class. In this way, we can identify what simplices are adjacent to an other one: the adjacency is organized by each subface.
			 * \param raw_faces the raw description of all the subfaces for the d-simplices
			 * \param raw_ad the required information for the raw adjacency
			 * \see mangrove_tds::GIA, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::RawFace, mangrove_tds::RawAdjacency,
			 * mangrove_tds::GIAManager::createComplex(), mangrove_tds::GIAManager::fixGIAAdjacency(), mangrove_tds::compare_faces(), mangrove_tds::same_faces()
			 */
			inline void computeAdjacency(vector< vector<RawFace> > &raw_faces, vector< vector<RawAdjacency> >& raw_ad)
			{
				vector<unsigned int> cs;
				unsigned int r,c,fi,ri,fi_cs,ri_cs;
				
				/* Now, we must iterate on the slots in 'raw_faces' */
				for(SIMPLEX_TYPE d=1;d<raw_faces.size();d++)
				{
					/* First, we sort 'raw_faces[d]' in order to identify adjacent (d+1)-faces */
					cs.clear();
					sort(raw_faces[d].begin(),raw_faces[d].end(),compare_faces);
					r=0;
					c=1;
					while( (r<raw_faces[d].size()) && (c<raw_faces[d].size()))
					{
						/* Now, we must identify connected components of faces! */
						if(same_faces(raw_faces[d][r],raw_faces[d][c]))
						{
							/* We accumulate replicants! */
							cs.push_back(c);
							c=c+1;
						}
						else
						{
							/* We mus adjust adjancencies! */
							if(cs.size()!=0)
							{
								/* We must store adjacent faces */
								cs.push_back(r);
								for(unsigned int i=0;i<cs.size();i++)
								{
									/* We encode adjacent before i */
									fi=raw_faces[d][cs[i]].getIndex();
									ri=raw_faces[d][cs[i]].getReverseIndex();
									for(unsigned int j=0;j<i;j++)
									{
										fi_cs=raw_faces[d][cs[j]].getIndex();
										ri_cs=raw_faces[d][cs[j]].getReverseIndex();
										raw_ad[d][fi].addSimplex(SimplexPointer(d+1,fi_cs),ri,ri_cs);
									}
									
									/* Now, we encode adjacent after i */
									for(unsigned int j=i+1;j<cs.size();j++)
									{
										fi_cs=raw_faces[d][cs[j]].getIndex();
										ri_cs=raw_faces[d][cs[j]].getReverseIndex();
										raw_ad[d][fi].addSimplex(SimplexPointer(d+1,fi_cs),ri,ri_cs);
									}
								}
							}

							/* We reset auxiliary data structures for continuing...." */
							r=c;
							c=r+1;
							cs.clear();
						}
					}
					
					/* Now, we must complete the construction, finalizing the last cluster! */
					if(r<raw_faces[d].size())
					{
						/* We mus adjust adjancencies, if they exist! */
						if(cs.size()!=0)
						{
							/* We must store adjacent faces */
							cs.push_back(r);
							for(unsigned int i=0;i<cs.size();i++)
							{
								/* We encode adjacent before i */
								fi=raw_faces[d][cs[i]].getIndex();
								ri=raw_faces[d][cs[i]].getReverseIndex();
								for(unsigned int j=0;j<i;j++)
								{
									fi_cs=raw_faces[d][cs[j]].getIndex();
									ri_cs=raw_faces[d][cs[j]].getReverseIndex();
									raw_ad[d][fi].addSimplex(SimplexPointer(d+1,fi_cs),ri,ri_cs);
								}
								
								/* Now, we encode adjacent after i */
								for(unsigned int j=i+1;j<cs.size();j++)
								{
									fi_cs=raw_faces[d][cs[j]].getIndex();
									ri_cs=raw_faces[d][cs[j]].getReverseIndex();
									raw_ad[d][fi].addSimplex(SimplexPointer(d+1,fi_cs),ri,ri_cs);
								}
							}
						}
					}
				}
			}
		};
	}
	
#endif

