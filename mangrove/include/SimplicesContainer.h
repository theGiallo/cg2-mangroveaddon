/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library
 *                                    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * January 2011 (Revised on May 2012)
 *                                                                 
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * SimplicesContainer.h - containers for collections of simplices in a simplicial complex                                             
 ***********************************************************************************************/
 
/* Should we include this header file? */
#ifndef SIMPLICES_CONTAINER_H_

	#define SIMPLICES_CONTAINER_H_

	#include <cstdlib>
	#include <vector>
	#include <assert.h>
	#include <algorithm>
	#include "Simplex.h"
	#include <iostream>
	using namespace std;
	using namespace mangrove_tds;

	/// Containers for collections of simplices in a simplicial complex.
	/**
	 * The classes defined in this file are useful for representing and managing collection of simplices in a simplicial complex. Such collections of simplices are implemented through dynamic
	 * arrays and they support the garbage collector mechanism in order to reuse locations marked as <i>deleted</i>.
	 * \file SimplicesContainer.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	 
	/* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		/* Forward prototypes in order to obtain an incomplete definition. */
		class SimplicesContainer;
		class PropertyBase;
		
		/// An iterator over collections of simplices in a simplicial complex.
		/**
		 * This class describes an iterator over a collection of simplices in a simplicial complex, described by an instance of the mangrove_tds::SimplicesContainer class. This component is
		 * similar to the iterator of the <i><A href="http://www.sgi.com/tech/stl/">STL Library</A></i> and it allows to scan a collection of simplices. Here, the simplices can be enumerated and
		 * implicitly ordered in these collections and thus an iterator can be moved in two different directions, respectively called <i>forward</i> and <i>reverse</i> directions:<p>
		 * <ul><li>an iterator is moved in <i>forward</i> direction if and only if it is moved towards the bottom of a simplices collection;</li><li>an iterator is moved in <i>reverse</i>
		 * direction if and only if it is moved towards the head of a simplices collection.</li></ul><p><b>IMPORTANT:</b> the simplices are visited by this iterator in the same order they
		 * appear in the input collection of simplices.
		 * \see mangrove_tds::SimplicesContainer, mangrove_tds::Simplex, mangrove_tds::SimplexPointer
		 */
		class SimplexIterator
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a not initialized iterator, thus it is mandatory to provide an initialization before using it.<p><b>IMPORTANT:</b> you should not directly invoke
			 * this member function, since you should apply the member functions offered by the mangrove_tds::SimplicesContainer class in order to obtain a valid instance of the
			 * mangrove_tds::SimplexIterator class.
			 * \see mangrove_tds::SimplicesContainer::begin(), mangrove_tds::SimplicesContainer::end(), mangrove_tds::SimplicesContainer::rbegin(),
			 * mangrove_tds::SimplicesContainer::rend()
			 */
			SimplexIterator();
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a deep copy of the input iterator, by replicating its internal state.<p><b>IMPORTANT:</b> you should not directly invoke this member function, since
			 * you should apply the member functions offered by the mangrove_tds::SimplicesContainer class in order to obtain a valid instance of the mangrove_tds::SimplexIterator class.
			 * \param it the iterator to be copied
			 * \see mangrove_tds::SimplicesContainer::begin(), mangrove_tds::SimplicesContainer::end(), mangrove_tds::SimplicesContainer::rbegin(),
			 * mangrove_tds::SimplicesContainer::rend()
			 */
			SimplexIterator(const SimplexIterator &it);
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates an iterator over a collection of simplices: it is possible to create many types of iterators referring to special positions in the  associated
			 * collection of simplices through boolean flags.<p>This member function checks if it is possible to build a valid iterator with the input parameters: if it is not possible, then
			 * this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.<p><b>IMPORTANT:</b> you should not directly invoke this member function, since you should apply the member functions offered by the mangrove_tds::SimplicesContainer
			 * class in order to obtain a valid instance of the mangrove_tds::SimplexIterator class.
			 * \param cc the collection of simplices to be associated to the new instance of this class
			 * \param eofp a boolean flag indicating that the new iterator refers the bottom of the input collection of simplices
			 * \param reofp a boolean flag indicating that the new iterator refers the head of the input collection of simplices
			 * \param index the identifier of the simplex to be referred by the current iterator: it will be considered only if the iterator to be created refers a valid simplex, i.e. a not
			 * deleted simplex and different than the bottom and the head of the input collection, otherwise it will be discarded.
			 * \see mangrove_tds::SimplicesContainer::begin(), mangrove_tds::SimplicesContainer::end(), mangrove_tds::SimplicesContainer::rbegin(),
			 * mangrove_tds::SimplicesContainer::rend()
			 */
			SimplexIterator(SimplicesContainer* cc,bool eofp,bool reofp,SIMPLEX_ID index=0);
			
			/// This member function destroys an instance of this class.
			inline virtual ~SimplexIterator() { this->container = NULL; }
			
			/// This member function returns the collection of simplices associated to the current iterator.
			/**
			 * \return the collection of simplices associated to the current iterator
			 * \see mangrove_tds::SimplicesContainer, mangrove_tds::SimplexIterator::setCollection(), mangrove_tds::SimplexIterator::haveCollection()
			 */
			inline SimplicesContainer * getCollection() const { return this->container; }
			
			/// This member function checks if the current iterator has been associated to a valid collection of simplices.
			/**
			 * \return <ul><li><i>true</i>, if the current iterator has been associated to a valid collection of simplices</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplicesContainer, mangrove_tds::SimplexIterator::getCollection(), mangrove_tds::SimplexIterator::setCollection()
			 */
			inline bool haveCollection() const { return (this->container!=NULL); }
			
			/// This member function updates the collection of simplices associated to the current iterator.
			/**
			 * This member function updates the collection of simplices associated to the current iterator, by checking if the input collection is valid: if the collection to be updated is
			 * not valid, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a
			 * failed assert.
			 * \param container the new collection of simplices to be associated to the current iterator
			 * \see mangrove_tds::SimplicesContainer, mangrove_tds::SimplexIterator::getCollection(), mangrove_tds::SimplexIterator::haveCollection()
			 */
			void setCollection(SimplicesContainer *container);
			
			/// This member function returns a pointer to the simplex referred by the current iterator.
			/**
			 * Usually, a simplex can be referred by an istance of the mangrove_tds::SimplexPointer class and it is identified by its dimension and by an identifier, i.e. its position in a
			 * collection of simplices, described by the mangrove_tds::SimplicesContainer class. This member function returns a pointer for the simplex referred by the current iterator.<p>If
			 * a collection of simplices has not been associated to the current iterator or if it refers to a not valid simplex, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \return a pointer to the simplex referred by the current iterator
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SimplicesContainer, mangrove_tds::SimplexIterator::haveCollection()
		     	 */
			SimplexPointer getPointer() const;
			
			/// This member function writes a debug representation of the current iterator on an output stream.
			/**
		     	 * This member function writes a debug representation of the current iterator, i.e. it writes information about the current iterator and in particular about the simplex of the
			 * associated container referred by the current iterator, if it exists.<p>The debug representation of a valid simplex is provided by the mangrove_tds::Simplex::debug() member
			 * function.<p><b>IMPORTANT:</b> this member function is intended only for debugging purposes. You should apply the writing operator in order to obtain a compact representation of
			 * the current iterator, suitable for transmitting or receiving objects.
			 * \param os the output stream where we can write a debug representation of the current iterator
			 * \see mangrove_tds::SimplicesContainer, mangrove_tds::Simplex::debug(), mangrove_tds::SimplexIterator::haveCollection(), mangrove_tds::SimplexPointer
			 */
			void debug(ostream& os=cout) const;
			
			/// This operator checks if two simplex iterators refer the same simplex.
			/**
			 * This operator checks if two simplex iterators refer the same simplex. Usually, a simplex can be referred by an instance of the mangrove_tds::SimplexPointer class and it is
			 * identified by its dimension and by an identifier, i.e its position in a collection of simplices, described by the mangrove_tds::SimplicesContainer class. Thus, two simplex
			 * iterators refer the same simplex if and only if they are associated to the same collection of simplices and the referred pointers are the same ones, i.e. their dimensions and
			 * types coincide.
			 * \param ci the second simplex iterator to be compared
			 * \return <ul><li><i>true</i>, if the two simplices iterators refer the same simplex</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplexIterator::haveCollection(), mangrove_tds::SimplexIterator::getCollection(), mangrove_tds::SimplicesContainer, mangrove_tds::SimplexPointer
			 */
			bool operator==(const SimplexIterator& ci);
			
			/// This operator checks if two simplex iterators do not refer the same simplex.
			/**
			 * This operator checks if two simplex iterators do not refer the same simplex. Usually, a simplex can be referred by an instance of the mangrove_tds::SimplexPointer class and it
			 * is identified by its dimension and by an identifier, i.e its position in a collection of simplices, described by the mangrove_tds::SimplicesContainer class. Thus, two simplices
			 * iterators refer the same simplex if and only if they are associated to the same collection of simplices and the referred pointers are the same ones, i.e. their dimensions and
			 * types coincide.
			 * \param ci the second simplex iterator to be compared
			 * \return <ul><li><i>true</i>, if the two simplices iterators do not refer the same simplex</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplexIterator::haveCollection(), mangrove_tds::SimplexIterator::getCollection(), mangrove_tds::SimplicesContainer, mangrove_tds::SimplexPointer
			 */
			bool operator!=(const SimplexIterator& ci);
			
			/// This operator returns a reference to the simplex referred by the current iterator.
			/**
			 * This operator returns a reference to the simplex referred by the current iterator. Usually, a simplex can be referred by an instance of the mangrove_tds::SimplexPointer class
			 * and it is identified by its dimension and by an identifier, i.e. its position in a collection of simplices, described by the mangrove_tds::SimplicesContainer class. Moreover,
			 * we say tha a simplex is <i>valid</i> if and only if its location exists and it has not been marked as <i>deleted</i> by the garbage collector mechanism.<p>If the current
			 * iterator has not a collection of simplices associated to it or if the referred simplex is not valid, then this operator will fail if and only if the <i>Mangrove TDS Library</i>
			 * is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \return a reference to the simplex referred by the current iterator
			 * \see mangrove_tds::SimplicesContainer::simplex(), mangrove_tds::SimplexIterator::haveCollection(), mangrove_tds::Simplex, mangrove_tds::SimplexPointer
			 */
			Simplex& operator*();
			
			/// This operator returns a pointer to the simplex referred by the current iterator.
			/**
			 * This operator returns a pointer to the simplex referred by the current iterator. Usually, a simplex can be referred by an instance of the mangrove_tds::SimplexPointer class and
			 * it is identified by its dimension and by an identifier, i.e. its position in a collection of simplices, described by the mangrove_tds::SimplicesContainer class. Moreover, we
			 * say tha a simplex is <i>valid</i> if and only if its location exists and it has not been marked as <i>deleted</i> by the garbage collector mechanism.<p>If the current iterator
			 * has not a collection of simplices associated to it or if the referred simplex is not valid, then this operator will fail if and only if the <i>Mangrove TDS Library</i> is
			 * compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \return a pointer to the simplex referred by the current iterator
			 * \see mangrove_tds::SimplicesContainer::simplex(), mangrove_tds::SimplexIterator::haveCollection(), mangrove_tds::Simplex, mangrove_tds::SimplexPointer
			 */
			Simplex* operator->();
			
			/// This operator moves the current iterator to the next valid simplex in the <i>forward</i> direction.
			/**
			 * This operator moves the current iterator to the next valid simplex in the <i>forward</i> direction. A collection of simplices is described by an instance of the
			 * mangrove_tds::SimplicesContainer class, where the simplices can be implicitly ordered and thus enumerated in the same order they appear. This operator moves the current iterator
			 * to the next valid simplex in the <i>forward</i> direction, i.e. towards the bottom of a simplices collection, if it is possible: we say that a simplex is <i>valid</i> if and
			 * only if its location exists and it has not been marked as <i>deleted</i> by the garbage collector mechanism.<p>If the current iterator has not a collection of simplices
			 * associated to it or if it is not possible to move the current iterator, then this operator will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode.
			 * In this case, each forbidden operation will result in a failed assert.
			 * \return the current iterator referring the next valid simplex along the <i>forward</i> direction
			 * \see mangrove_tds::SimplicesContainer, mangrove_tds::SimplexIterator::haveCollection(), mangrove_tds::Simplex, mangrove_tds::SimplexPointer
			 */
			SimplexIterator operator++();
			
			/// This operator moves the current iterator to the next valid simplex in the <i>forward</i> direction.
			/**
			 * This operator moves the current iterator to the next valid simplex in the <i>forward</i> direction. A collection of simplices is described by an instance of the
			 * mangrove_tds::SimplicesContainer class, where the simplices can be implicitly ordered and thus enumerated in the same order they appear. This operator moves the current
			 * iterator to the next valid simplex in the <i>forward</i> direction, i.e. towards the bottom of a simplices collection, if it is possible: we say that a simplex is <i>valid</i>
			 * if and only if its location exists and it has not been marked as <i>deleted</i> by the garbage collector mechanism.<p>If the current iterator has not a collection of simplices
			 * associated to it or if it is not possible to move the current iterator, then this operator will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode.
			 * In this case, each forbidden operation will result in a failed assert.
			 * \param aux an auxiliary and not useful parameter for imposing the postfix syntax on this operator
	         	 * \return the current iterator referring the next valid simplex along the <i>forward</i> direction
	         	 * \see mangrove_tds::SimplicesContainer, mangrove_tds::SimplexIterator::haveCollection(), mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::idle()
			 */
			SimplexIterator& operator++(int aux);
			
			/// This operator moves the current iterator to the next valid simplex in the <i>reverse</i> direction.
			/**
			 * This operator moves the current iterator to the next valid simplex in the <i>reverse</i> direction. A collection of simplices is described by an instance of the
			 * mangrove_tds::SimplicesContainer class, where the simplices can be implicitly ordered and thus enumerated in the same order they appear. This operator moves the current iterator
			 * to the next valid simplex in the <i>reverse</i> direction, i.e. towards the beginning of a simplices collection, if it is possible: we say that a simplex is <i>valid</i> if and
			 * only if its location exists and it has not been marked as <i>deleted</i> by the garbage collector mechanism.<p>If the current iterator has not a collection of simplices
			 * associated to it or if it is not possible to move the current iterator, then this operator will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode.
			 * In this case, each forbidden operation will result in a failed assert.
			 * \return the current iterator referring the next valid simplex along the <i>reverse</i> direction
			 * \see mangrove_tds::SimplicesContainer, mangrove_tds::SimplexIterator::haveCollection(), mangrove_tds::Simplex, mangrove_tds::SimplexPointer
			 */
			SimplexIterator operator--();
			
			/// This operator moves the current iterator to the next valid simplex in the <i>reverse</i> direction.
			/**
			 * This operator moves the current iterator to the next valid simplex in the <i>reverse</i> direction. A collection of simplices is described by an instance of the
			 * mangrove_tds::SimplicesContainer class, where the simplices can be implicitly ordered and thus enumerated in the same order they appear. This operator moves the current iterator
			 * to the next valid simplex in the <i>reverse</i> direction, i.e. towards the beginning of a simplices collection, if it is possible: we say that a simplex is <i>valid</i> if and
			 * only if its location exists and it has not been marked as <i>deleted</i> by the garbage collector mechanism.<p>If the current iterator has not a collection of simplices
			 * associated to it or if it is not possible to move the current iterator, then this operator will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode.
			 * In this case, each forbidden operation will result in a failed assert.
			 * \param aux an auxiliary and not useful parameter for imposing the postfix syntax on this operator
			 * \return the current iterator referring the next valid simplex along the <i>reverse</i> direction
			 * \see mangrove_tds::SimplicesContainer, mangrove_tds::SimplexIterator::haveCollection(), mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::idle()
			 */
			SimplexIterator& operator--(int aux);
			
			protected:
			
			/// The collection of simplices in a simplicial complex associated to the current iterator.
			/**
			 * \see mangrove_tds::SimplicesContainer
			 */
			SimplicesContainer* container;
			
			/// The identifier of the currently referred simplex.
			/**
			 * This identifier is valid if and only if the iterator does not refer the head or the bottom of a simplices container, i.e. when mangrove_tds::SimplexIterator::eof and
			 * mangrove_tds::SimplexIterator::reof assume <i>false</i> value.
			 * \see mangrove_tds::SIMPLEX_ID
			 */ 
			SIMPLEX_ID p;
			
			/// This boolean flag indicates if the current iterator refers the bottom of the simplices container.
			/**
			 * This flag holds <i>true</i>, if the iterator refers the bottom of the current container, <i>false</i> otherwise.
			 */
			bool eof;
			
			/// This boolean flag indicates if the current iterator refers the head of the simplices container.
			/**
			 * This flag holds <i>true</i>, if the iterator refers the head of the simplices container, <i>false</i> otherwise.
			 */
			bool reof;
		};
		
		/// A collection of simplices that have the same dimension.
		/**
		 * This class describes a collection of simplices with the same dimension, stored in a dynamic vector that supports the garbage collector mechanism: in this way, we can reuse allocated
		 * locations that are not useful for applications. A not useful simplex is marked as <i>deleted</i> and it is not physically destroyed and removed. Thus, it is important to establish when
		 * a simplex is <i>valid</i>: we  say that a simplex is <i>valid</i> if and only if it has not been marked as <i>deleted</i> by the garbage collector. As conseguence, the number of all
		 * the allocated locations could differ from the number of valid locations since some of them could be marked as <i>deleted</i> and thus they cannot be accessed.<p>A simplex
		 * encoded in a similar collection can be also accessed through a pointer (described through the mangrove_tds::SimplexPointer class). A pointer to an existing simplex is composed by the
		 * dimension (called simplex type and described by value mangrove_tds::SIMPLEX_TYPE) and by the position in the corresponding collection of simplices (called simplex identifier and
		 * described by value mangrove_tds::SIMPLEX_ID).<p>All the simplices can be enumerated and implicitly ordered in the same order they appear in the input collection. Thus, the
		 * enumeration can be executed through an iterator (described in the mangrove_tds::SimplexIterator class), that can be moved in two different directions, respectively called
		 * <i>forward</i> and <i>reverse</i> directions:<p><ul><li>an iterator is moved in <i>forward</i> direction if and only if it is moved towards the bottom of a simplices collection;
		 * </li><li>an iterator is moved in <i>reverse</i> direction if and only if it is moved towards the beginning of a simplices collection.</li></ul><p>Moreover, it is possible to associate
		 * a property to each simplex in this collection: a property is an auxiliary information (a normal, a field value, Euclidean coordinates,...) that can be assigned to simplices in a
		 * simplicial complex: a property is described by the mangrove_tds::PropertyBase subclasses.<p><b>IMPORTANT:</b> this class is intended to be used in a transparent way for the user in the
		 * sense that you should not need to directly apply it. In fact, you should apply the member functions offered by the mangrove_tds::BaseSimplicialComplex class in order to access to the
		 * stored simplices and their properties.
		 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::PropertyBase, mangrove_tds::Simplex, mangrove_tds::SimplexIterator, mangrove_tds::SimplexPointer	
		 */
		class SimplicesContainer
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a not initialized collection of simplices: you should provide an initialization before using it by applying the member functions offered by the
			 * mangrove_tds::SimplicesContainer class.
			 * \see mangrove_tds::SimplicesContainer::updateType(), mangrove_tds::SimplicesContainer::newSimplex(), mangrove_tds::SimplicesContainer::addSimplex()
			 */
			inline SimplicesContainer() { this->updateType(0); }
			
			/// This member function creates a new instance of this class.
			/**
		 	 * This member function prepares a new container targeted to contain simplices of a certain dimension: the resulting collection is empty, thus you should add all the simplices by
			 * using the member functions provided by the mangrove_tds::SimplicesContainer class.
			 * \param ctype the new dimension of all the simplices stored in the current collection
			 * \see mangrove_tds::SimplicesContainer::updateType(), mangrove_tds::SimplicesContainer::newSimplex(), mangrove_tds::SimplicesContainer::addSimplex(), 
			 * mangrove_tds::SIMPLEX_TYPE
			 */
			inline SimplicesContainer(SIMPLEX_TYPE ctype) { this->updateType(ctype); }
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a deep copy of the input container, by replicating its internal state.
			 * \param c the collection of simplices to be copied
			 * \see mangrove_tds::SimplicesContainer::updateType(), mangrove_tds::SimplicesContainer::simplexc(), mangrove_tds::Simplex
			 */
			SimplicesContainer(const SimplicesContainer &c);
			
			/// This member function destroys an instance of this class.
			/**
			 * \see mangrove_tds::SimplicesContainer::clearCollection()
			 */
			inline virtual ~SimplicesContainer() { this->clearCollection(); }
			
			/// This member function returns the dimension of all the simplices stored in the current collection.
			/**
			 * This member function returns the dimension of all the simplices stored in the current collection.
			 * \return the dimension of all the simplices stored in the current collection
			 * \see mangrove_tds::SimplicesContainer::updateType()
			 */
			inline SIMPLEX_TYPE type() const { return this->ctype; }
			
			/// This member function reallocates the space needed for storing simplices in the current collection.
			/**
			 * This member function removes all the allocated locations in the current collection, by updating their dimension: the old simplices are physically removed and they cannot be
			 * accessed.
			 * \param ctype the new dimension of all the simplices stored in the current collection
			 * \see mangrove_tds::SimplicesContainer::type(), mangrove_tds::SimplicesContainer::clearCollection(), mangrove_tds::SIMPLEX_TYPE
			 */
			void updateType(SIMPLEX_TYPE ctype);
			
			/// This member function returns the number of all the allocated locations in the current collection of simplices.
			/**
			 * This member function returns the number of all the allocated locations in the current collection and it does not discard the locations marked as <i>deleted</i> by the garbage
			 * collector mechanism in the current collection of simplices.<p><b>IMPORTANT:</b> the number of all the allocated locations could differ from the number of valid locations since
			 * some of them could be marked as <i>deleted</i> and thus they cannot be accessed.
			 * \return the number of all the allocated locations in the current collection of simplices
			 * \see mangrove_tds::SimplicesContainer::isValid(), mangrove_tds::SimplicesContainer::size(), mangrove_tds::SimplicesContainer::deletedSize()
			 */
			inline unsigned long vectorSize() const { return this->simplices.size(); }
			
			/// This member function returns the number of all the valid simplices in the current collection of simplices.
			/**
			 * This member function returns the number of all the valid simplices in the current collection of simplices, by discarding all the locations marked as <i>deleted</i> by the
			 * garbage collector mechanism.<p>We say that a simplex is <i>valid</i> if and only if it has not been marked as <i>deleted</i> by the garbage collector.<p><b>IMPORTANT:</b> the
			 * number of all the allocated locations could differ from the number of valid locations since some of them could be marked as <i>deleted</i> and thus they cannot be accessed.
			 * \return the number of all the valid simplices in the current collection of simplices
			 * \see mangrove_tds::SimplicesContainer::isValid(), mangrove_tds::SimplicesContainer::vectorSize(), mangrove_tds::SimplicesContainer::deletedSize()
			 */
			inline unsigned long size() const { return this->valid_locs; }
			
			/// This member function returns the number of all the simplices marked as deleted in the current collection of simplices.
			/**
			 * This member function returns the number of all the locations marked as <i>deleted</i> by the garbage collector mechanism.<p><b>IMPORTANT:</b> the number of all the
			 * allocated locations could differ from the number of valid locations since some of them could be marked as <i>deleted</i> and thus they cannot be accessed.
			 * \return the number of all the simplices marked as <i>deleted</i> in the current collection of simplices
			 * \see mangrove_tds::SimplicesContainer::isValid(), mangrove_tds::SimplicesContainer::vectorSize(), mangrove_tds::SimplicesContainer::size()
			 */
			inline unsigned long deletedSize() const { return (this->vectorSize() - this->size()); }
			
			/// This member function checks if a simplex location is valid in the current collection of simplices.
			/**
			 * This member function checks if a simplex location is valid in the current collection of simplices. Each simplex can be identified by its location in the
			 * mangrove_tds::SimplicesContainer::simplices array: we say that a simplex is <i>valid</i> if and only if its location exists and it has not been marked as  <i>deleted</i> by the
			 * garbage collector mechanism.
			 * \param i the position of the required simplex in the mangrove_tds::SimplicesContainer::simplices array
			 * \return <ul><li><i>true</i>, if the required simplex is valid</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplicesContainer::size(), mangrove_tds::Simplex::isDeleted(), mangrove_tds::SIMPLEX_ID
			 */
			bool isValid(const SIMPLEX_ID i) const;
			
			/// This member function checks if a simplex location has been marked as <i>deleted</i> by the garbage collector.
			/**
			 * This member function checks if a simplex location has been marked as <i>deleted</i> by the garbage collector. Each simplex can be identified by its location in the
			 * mangrove_tds::SimplicesContainer::simplices array and it could have been marked as <i>deleted</i> by the garbage collector mechanism.
			 * \param i the position of the required simplex in the mangrove_tds::SimplicesContainer::simplices array
			 * \return <ul><li><i>true</i>, if the required simplex has been marked as <i>deleted</i> by the garbage collector</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplicesContainer::deletedSize(), mangrove_tds::SimplicesContainer::deleteSimplex(), mangrove_tds::SIMPLEX_ID
			 */
			bool isDeleted(const SIMPLEX_ID i) const;
			
			/// This member function returns a reference to a simplex in the current collection.
			/**
			 * With this member function we can obtain a reference of a simplex in the current collection of simplices, identified by its location in the
			 * mangrove_tds::SimplicesContainer::simplices array. This simplex is required to be <i>valid</i>: we say that a simplex is <i>valid</i> if and only if its location exists and it
			 * has not been marked as <i>deleted</i> by the garbage collector mechanism.<p>If the required simplex is not valid, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> with this member function, we
			 * can update the required location and in particular we can create a new simplex. Unfortunately, you are not able to invoke the garbage collector mechanism to reuse locations
			 * thus you should not apply this member function in order to create new simplices. You should apply the mangrove_tds::SimplicesContainer::newSimplex() and the
			 * mangrove_tds::SimplicesContainer::addSimplex() member functions in order to add a new simplex in the current collection of simplices.
			 * \param i the position of the required simplex in the current collection of simplices
			 * \return a reference to a simplex in the current collection of simplices
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplicesContainer::isValid(), mangrove_tds::SimplicesContainer::simplexc(), mangrove_tds::SimplicesContainer::newSimplex(),
			 * mangrove_tds::SimplicesContainer::addSimplex()
			 */
			Simplex& simplex(const SIMPLEX_ID i);
			
			/// This member function returns a constant reference to a simplex in the current collection.
			/**
			 * With this member function we can obtain a reference of a simplex in the current collection of simplices, identified by its location in the
			 * mangrove_tds::SimplicesContainer::simplices array. This simplex is required to be <i>valid</i>: we say that a simplex is <i>valid</i> if and only if its location exists and it
			 * has not been marked as <i>deleted</i> by the garbage collector mechanism.<p>If the required simplex is not valid, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> with this member function we
			 * cannot update the required location and we can only access it in read-only mode. You should refer the member functions offered by the mangrove_tds::SimplicesContainer class in
			 * order to update simplices in the current collection.
			 * \param i the position of the required simplex in the current collection of simplices
			 * \return a constant reference to a simplex in the current collection of simplices
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplicesContainer::isValid(), mangrove_tds::SimplicesContainer::simplex(), mangrove_tds::SimplicesContainer::newSimplex(),
			 * mangrove_tds::SimplicesContainer::addSimplex()
			 */
			const Simplex& simplexc(const SIMPLEX_ID i) const;
			
			/// This member function checks if the current collection of simplices is empty.
			/**
			 * This member function checks if the current collection of simplices is empty, in the sense that it checks if the current collection does not contain any valid simplex: a simplex
			 * is <i>valid</i> if and only if it has not been marked as <i>deleted</i> by the garbage collector.
			 * \return <ul><li><i>true</i>, if the current collection does not contain any valid simplex</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplicesContainer::vectorSize(), mangrove_tds::SimplicesContainer::size()
			 */
			bool isEmpty() const;
			
			/// This member function clears the current container, by removing all the simplices.
			/**
			 * This member function clears the current container, by removing all the simplices: as consequence, they cannot be accessed.
			 * \see mangrove_tds::Simplex::clearBoundary(), mangrove_tds::Simplex::clearCoboundary(), mangrove_tds::Simplex::clearAdjacency()
			 */
			void clearCollection();
			
			/// This member function allocates all the required space for a new simplex.
			/**
			 * This member function allocates all the required space for a new simplex in the current collection of simplicess: it applies the garbage collector mechanism, by reusing a
			 * location marked as <i>deleted</i>, if it exists. The new simplex is not initialized: moreover, this member function returns an instance of the mangrove_tds::SimplexPointer
			 * class in order to access the new simplex.<p>This member function does not automatically resize all the properties (i.e. auxiliary information associated to simplices and
			 * described by subclasses of the mangrove_tds::PropertyBase class) associated to simplices in the current collection. If you need to resize auxiliary information, then you should
			 * apply the mangrove_tds::SimplicesContainer::newSimplex() member function.
			 * \param cp a simplex pointer that can be used to access the new allocated simplex
			 * \see mangrove_tds::SimplicesContainer::newSimplex(), mangrove_tds::SimplicesContainer::deleteSimplex(), mangrove_tds::SimplexPointer, mangrove_tds::PropertyBase,
			 * mangrove_tds::Simplex
			 */
			void addSimplex(SimplexPointer &cp);
			
			/// This member function allocates all the required space for a new simplex.
			/**
			 * This member function allocates all the required space for a new simplex in the current collection of simplices: it applies the garbage collector mechanism, by reusing a location
			 * marked as <i>deleted</i>, if it exists. The new simplex is not initialized: moreover, this member function returns an instance of the mangrove_tds::SimplexPointer class in
			 * order to access the new simplex.<p>Moreover, this member function automatically resizes all the properties (i.e. auxiliary information associated to simplices and described by
			 * subclasses of the mangrove_tds::PropertyBase class) associated to simplices in the current collection. If you do not need to resize auxiliary information, then you should apply
			 * the mangrove_tds::SimplicesContainer::addSimplex() member function.
			 * \param properties all the properties associated to the current collection of simplices
			 * \param cp a simplex pointer that can be used to access the new allocated simplex
			 * \see mangrove_tds::SimplicesContainer::addSimplex(), mangrove_tds::SimplicesContainer::deleteSimplex(), mangrove_tds::SimplexPointer, mangrove_tds::PropertyBase,
			 * mangrove_tds::Simplex
			 */
			void newSimplex(vector<PropertyBase *> &properties, SimplexPointer &cp);
			
			/// This member function removes a simplex from the current collection of simplices.
			/**
			 * This member function marks as <i>deleted</i> a simplex from the current collection of simplices: the victim simplex is identified by its location in the
			 *  mangrove_tds::SimplicesContainer::simplices array. This simplex is required to be <i>valid</i>: we say that a simplex is <i>valid</i> if and only if its location exists and it
			 * has not been marked as <i>deleted</i> by the garbage collector mechanism.<p>If the required simplex does not exist or it is not valid, then this member function will fail if
			 * and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> the required
			 * simplex is not physically removed, but it is marked as <i>deleted</i> by the garbage collector mechanism and it cannot be accessed. We can reuse its location by applying the
			 * mangrove_tds::SimplicesContainer::newSimplex() and the mangrove_tds::SimplicesContainer::addSimplex() member functions.
			 * \param i the position of the simplex to be removed in the current collection of simplices
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplicesContainer::isValid(), mangrove_tds::SimplicesContainer::isDeleted(), mangrove_tds::SimplicesContainer::addSimplex(),
			 * mangrove_tds::SimplicesContainer::newSimplex(), mangrove_tds::Simplex::setDeleted(), mangrove_tds::Simplex::isDeleted()
			 */
			void deleteSimplex(const SIMPLEX_ID i);
			
			/// This member function returns an iterator to the first valid simplex in the current collection of simplices.
			/**
			 * This member function returns an iterator to the first valid simplex in the current collection of simplices. All simplices in the current collection can be enumerated and
			 * implicitly ordered in the same order they appear in the input collection: this enumeration can be executed through an iterator (described in the mangrove_tds::SimplexIterator
			 * class), moved only on the valid simplices, i.e. simplices not marked as <i>deleted</i> by the garbage collector mechanism.<p>This member function returns an iterator to the
			 * first valid simplex that we can meet by moving in <i>forward</i> direction, i.e. towards the bottom of the current simplices collection.
			 * \return <ul><li>an iterator to the first valid simplex in the current collection of simplices, if it exists</li><li>a not initialized iterator, otherwise</li></ul>
			 * \see mangrove_tds::SimplicesContainer::end(), mangrove_tds::SimplicesContainer::rbegin(), mangrove_tds::SimplicesContainer::rend(), mangrove_tds::SimplexIterator
			 */
			SimplexIterator begin();
			
			/// This member function returns an iterator to the end of the current collections of simplices.
			/**
			 * This member function returns an iterator to the end of the current collections of simplices. All simplices in the current collection can be enumerated and implicitly ordered in
			 * the same order they appear in the input collection: this enumeration can be executed through an iterator (described in the mangrove_tds::SimplexIterator class), moved only on
			 * the valid simplices, i.e. simplices not marked as <i>deleted</i> by  the garbage collector mechanism.<p>This member function returns an iterator to a particular simplex, that
			 * marks the bottom of the current simplices collection, by moving in <i>forward</i> direction, i.e. towards the bottom of the current simplices collection: alternatively, this
			 * particular simplex marks the beginning of the current collection of simplices by moving in <i>reverse</i> direction, i.e. towards the beginning of the collection.
			 * \return <ul><li>an iterator to the end of the current collection of simplices, if it exists</li><li>a not initialized iterator, otherwise</li></ul>
			 * \see mangrove_tds::SimplicesContainer::begin(), mangrove_tds::SimplicesContainer::rbegin(), mangrove_tds::SimplicesContainer::rend(), mangrove_tds::SimplexIterator
			 */
			SimplexIterator end();

			/// This member function returns an iterator to the last valid simplex in the current collection of simplices.
			/**
			 * This member function returns an iterator to the last valid simplex in the current collection of simplices. All simplices in the current collection can be enumerated and
			 * implicitly ordered in the same order they appear in the input collection: this enumeration can be executed through an iterator (described in the mangrove_tds::SimplexIterator
			 * class), moved only on the valid simplices, i.e. simplices not marked as <i>deleted</i> by the garbage collector mechanism.<p>This member function returns an iterator to the
			 * last valid simplex that we can meet by moving in <i>forward</i> direction, i.e. towards the bottom of the current simplices collection: alternatively, it is the first simplex
			 * that we can meet by moving in <i>reverse</i> direction, i.e. towards the beginning of the collection.
			 * \return <ul><li>an iterator to the last valid simplex in the current collection of simplices, if it exists</li><li>a not initialized iterator, otherwise</li></ul>
			 * \see mangrove_tds::SimplicesContainer::begin(), mangrove_tds::SimplicesContainer::end(), mangrove_tds::SimplicesContainer::rend(), mangrove_tds::SimplexIterator
			 */
			SimplexIterator rbegin();
			
			/// This member function returns an iterator to the beginning of the current collection of simplices.
			/**
			 * This member function returns an iterator to the beginning of the current collection of simplices. All simplices in the current collection can be enumerated and implicitly
			 * ordered in the same order they appear in the input collection: this enumeration can be executed through an iterator (described in the mangrove_tds::SimplexIterator class),
			 * moved only on the valid simplices, i.e. simplices not marked as <i>deleted</i> by the garbage collector mechanism.<p>This member function returns an iterator to a particular
			 * simplex, that marks the beginning of the current simplices collection, by moving in <i>forward</i> direction, i.e. towards the bottom of the current simplices collection:
			 * alternatively, this particular simplex marks the bottom of the current collection of simplices by moving in <i>reverse</i> direction, i.e. towards the beginning of the
			 * collection.
			 * \return <ul><li>an iterator to the beginning of the current collection of simplices, if it exists</li><li>a not initialized iterator, otherwise</li></ul>
			 * \see mangrove_tds::SimplicesContainer::begin(), mangrove_tds::SimplicesContainer::end(), mangrove_tds::SimplicesContainer::rbegin(), mangrove_tds::SimplexIterator
			 */
			SimplexIterator rend();
			
			/// This member function writes a debug representation for all the locations in the current collection.
			/**
			 * This member function writes a debug representation for all the locations in the current collection. In the debug representation provided by this member function, we write
			 * information for all the locations in the current collection. We provide a debug representation for all the locations in the current collection of simplices by using the
			 * mangrove_tds::Simplex::debug() member function.<p><b>IMPORTANT:</b> this member function is intended only for debugging purposes. Otherwise, you should apply the
			 * writing operator in order to obtain a compact representation, suitable for transmitting or receiving objects.
			 * \param os the output stream where we can write a debug representation of the current collection of simplices
			 * \see mangrove_tds::Simplex::debug(), mangrove_tds::SimplicesContainer::vectorSize(), mangrove_tds::SimplicesContainer::deletedSize()
			 */
			void debug(ostream& os = cout) const;
			
			/// This operator writes a compact representation of all the locations in the current collection of simplices.
			/**
			 * In this operator, we write a compact representation of all the locations in the current collection of simplices on a output stream, suitable for transmitting and receiving
			 * objects. Such representation is formatted in accordance with these guidelines:<p><i>[ simplices dimension ]<br>[ number of locations ]<br>[ list of locations ]</i><p>The compact
			 * representation of a location is provided by the writing operator in the mangrove_tds::Simplex class, thus we can easily also describe locations marked as <i>deleted</i>.<p>This
			 * compact representation can be used by the reading operator in order to recreate an instance of the mangrove_tds::SimplicesContainer class: otherwise, you should apply the
			 * mangrove_tds::SimplicesContainer::debug() member function for debugging purposes.
			 * \param os the output stream where we can write the compact representation of a collection of simplices
			 * \param cnt the collection of simplices to be written
			 * \return the output stream after having written the compact description of the input collection of simplices
			 * \see mangrove_tds::SimplicesContainer::debug(), mangrove_tds::Simplex::debug()
			 */
			inline friend ostream& operator<<(ostream& os,const SimplicesContainer &cnt)
			{
				/* Repetita iuvant - description of a collection of simplices:
				 * 
				 * < simplices dimension >
				 * < number of simplices >
				 * < list of simplices > (see operator << for Simplex). */
				os<<cnt.ctype<<endl;
				os<<(cnt.simplices.size())<<endl;
				for(unsigned int i=0;i<cnt.simplices.size();i++) { os<<(cnt.simplices[i]); }
				os.flush();
				return os;
			}
			
			/// This operator rebuilds an instance of this class, by reading a compact representation of a collection of simplices from an input stream.
			/**
			 * In this operator, we rebuild an instance of this class, by reading a compact representation of a collection of simplices from an input stream, suitable for transmitting and
			 * receiving objects. Such representation contains all the locations (included those marked as <i>deleted</i>) and it is formatted in accordance with these guidelines:<p>
			 * <i>[ simplices dimension ]<br>[ number of locations ]<br>[ list of locations ]</i><p>The representation of a simplex can be analyzed by using the reading operator in the
			 * mangrove_tds::Simplex class: in this way, we rebuild all the locations, by rebuilding a snapshot of a collection of simplices.<p>This compact representation can be provided by
			 * the writing operator: otherwise, you should apply the mangrove_tds::SimplicesContainer::debug() member function for debugging purposes.
			 * \param in the input stream from which we can read the representation of a simplices collection
			 * \param cnt the collection of simplices to be rebuilt
			 * \return the input stream after having read the required collection of simplices
			 * \see mangrove_tds::SimplicesContainer::debug(), mangrove_tds::SimplicesContainer::clearCollection(), mangrove_tds::SimplicesContainer::updateType(), mangrove_tds::Simplex
			 */
			inline friend std::istream& operator>>(std::istream& in,SimplicesContainer& cnt)
			{
				SIMPLEX_TYPE aux_dim;
				unsigned int vals;
				Simplex c;

				/* Repetita iuvant - description of a collection of simplices:
				 * 
				 * < simplices dimension >
				 * < number of simplices >
				 * < list of simplices > (see operator >> for Simplex). */
				in>>aux_dim;
				in>>vals;
				cnt.clearCollection(); 
				cnt.updateType(aux_dim);
				cnt.next_loc = vals;
				for(unsigned int k=0;k<vals;k++)
				{
					/* Now, we read the k-th simplex and then we update the internal counters. */
					in>>c;
					cnt.simplices.push_back(Simplex(c));
					if(cnt.simplices[k].isDeleted()) { cnt.next_loc = min(cnt.next_loc,k); }
					else { cnt.valid_locs = cnt.valid_locs+1; }
				}
				
				/* If we arrive here, we have read all the simplices. */
				return in;
			}
			
			protected:
			
			/// The dimension of the simplices stored in the current collection of simplices.
			/**
			 * \see mangrove_tds::SIMPLEX_TYPE
			 */
			SIMPLEX_TYPE ctype;
			
			/// The number of valid locations in the current collection of simplices.
			unsigned int valid_locs;
			
			/// The index of the next free location, i.e. a location not marked as <i>deleted</i> by the garbage collector.
			unsigned int next_loc;
			
			/// The collection of simplices stored in the current collection.
			/**
			 * \see mangrove_tds::Simplex
			 */
			vector<Simplex> simplices;
			
			/// This member function updates the index of the next valid location.
			/**
			 * This member function looks for a valid simplex starting from a certain location: we say that a simplex is <i>valid</i> if and only if its location exists and it has not been
			 * marked as <i>deleted</i> by the garbage collector mechanism, provided by the mangrove_tds::SimplicesContainer class.
			 * \param pos the first index from which our search can start
			 * \see mangrove_tds::SimplicesContainer::isValid(), mangrove_tds::Simplex::isDeleted()
			 */
			void updateNextFreeLocation(unsigned int pos);
		};
	}
	
#endif

