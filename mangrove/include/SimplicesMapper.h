/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library
 * 
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * January 2011	(Revised on May 2012)			    
 *                                                                   
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * SimplicesMapper.h - a component for mapping simplices in a simplicial complex
 ***********************************************************************************************/
 
/* Should we include this header file? */
#ifndef SIMPLICES_MAPPER_H

	#define SIMPLICES_MAPPER_H

	#include "Simplex.h"
	#include "BaseSimplicialComplex.h"
	#include <map>
	#include <vector>
	#include <cstdlib>
	using namespace std;
	using namespace mangrove_tds;
	
	/// Components for mapping simplices belonging to a simplicial complex.
	/**
	 * The class defined in this file are useful for mapping simplices in a simplicial complex, described by the mangrove_tds::BaseSimplicialComplex class. This class is based on the
	 * <A href=http://en.wikipedia.org/wiki/Curiously_recurring_template_pattern>Curiously Recurring Template Pattern</A> in order to implement the static polymorphism.
	 * \file SimplicesMapper.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	
	/* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		/// Components for mapping simplices in a simplicial complex.
		/**
	 	 * This class describes a component used for mapping simplices directly encoded in a data structure for simplicial complexes: it is very useful when it is mandatory to have a continuos set
	 	 * of indices at least in the same class of simplices, for example in order to compute the <i>border matrix</i> for a particular list of simplicess. In particular, it is possible to apply
		 * some updates on a simplicial complex and to remove some simplices from it: as consequence, the set of indices referring simplices in a simplicial complex could not be continuos (at
		 * least in the same class of simplices).<p>A simplicial complex is described by the mangrove_tds::BaseSimplicialComplex class: this class is based on the
	 	 * <A href=http://en.wikipedia.org/wiki/Curiously_recurring_template_pattern>Curiously Recurring Template Pattern</A> in order to implement the static polymorphism. Through the
	 	 * template T, it is possible to provide only the member functions specialized for particular data structures, targeted to encode simplicial complexes. For example, we can consider the
	 	 * <i>Incidence Graph</i> (described by the mangrove_tds::IG class), the <i>Incidence Simplicial</i> data structure (described by the mangrove_tds::IS class), the
	 	 * <i>Simplified Incidence Graph</i> (described by the mangrove_tds::SIG class) and the <i>IA*</i> data structure (described by the mangrove_tds::GIA class). <p>All the simplices
	 	 * can be enumerated and implicitly ordered in the same order they appear. Thus, the enumeration can be executed through an iterator (described by the mangrove_tds::SimplexIterator
	 	 * class), that can be moved in two different directions, respectively called <i>forward</i> and <i>reverse</i> directions:<p><ul><li>an iterator is moved in <i>forward</i>
	 	 * direction if and only if it is moved towards the bottom of a simplices collection;</li><li>an iterator is moved in <i>reverse</i> direction if and only if it is moved towards the
	 	 * beginning of a simplices collection.</li></ul>In this class it is possible to execute a <i>forward</i> mapping and a <i>reverse</i> mapping for all the simplices in the relating
	 	 * simplicial complex. An other possible operation is to have the <i>sparse</i> mapping for some simplices in the related simplicial complex: in this case, only few simplices are mapped
	 	 * in a continuos set of indices in the same order they appear.
	 	 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::IG, mangrove_tds::IS, mangrove_tds::SIG, mangrove_tds::GIA, mangrove_tds::SimplexIterator,
	 	 * mangrove_tds::SimplicesContainer
	 	 */
	 	template <class T> class SimplexPointerMap
	 	{
	 		public:
	 		
	 		/// This member function creates a new instance of this class.
	 		/**
	 		 * This member function creates a not initialized instance of this class: you should update it before using it. First, you should set the reference simplicial complex by applying
			 * the mangrove_tds::SimplexPointerMap::setComplex() member function and then you should apply the member functions offerred by the current class in order to map simplices.<p>A
	 		 * mapping is called:<ul><li><i>forward</i> if and only if it is performed through an iterator moved in <i>forward</i> direction, i.e. if the iterator is moved towards the bottom
			 * of a simplices collection. Such mapping is performed through the mangrove_tds::SimplexPointerMap::forwardMapping() member function.</li><li><i>reverse</i> if and only if it is
	 		 * performed through an iterator moved in <i>reverse</i> direction, i.e. if the iterator is moved towards the beginning of a simplices collection. Such mapping is performed through
	 		 * the mangrove_tds::SimplexPointerMap::reverseMapping() member function.</li><li><i>sparse</i> if and only if few simplices (independently from their dimension) are mapped in
	 		 * a continuos set of indices in the same order they appear. Such mapping is performed through the mangrove_tds::SimplexPointerMap::addSimplex() member function.</li></ul>
	 		 * \see mangrove_tds::SimplexPointerMap::setComplex(), mangrove_tds::SimplexPointerMap::forwardMapping(), mangrove_tds::SimplexPointerMap::reverseMapping(),
	 		 * mangrove_tds::SimplexPointerMap::addSimplex()
	 		 */
	 		inline SimplexPointerMap()
	 		{
	 			/* Dummy initialization! */
	 			this->ccw=NULL;
	 			this->maps.clear();
	 		}

	 		/// This member function creates a new instance of this class.
	 		/**
	 		 * This member function creates a new instance of this class, by also initializing the reference simplicial complex.<p>If the input simplicial complex is not valid or if it has
			 * not been initialized, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will
			 * result in a failed assert.<p>This member function initializes only the simplicial complex associated to the current component: you should apply the member functions offerred by
			 * the current class in order to map simplices.<p>A mapping is called:<ul><li><i>forward</i> if and only if it is performed through an iterator moved in <i>forward</i> direction,
			 * i.e. if the iterator is moved towards the bottom of a simplices collection. Such mapping is performed through the mangrove_tds::SimplexPointerMap::forwardMapping() member
			 * function.</li><li><i>reverse</i> if and only if it is performed through an iterator moved in <i>reverse</i> direction, i.e. if the iterator is moved towards the beginning of a
			 * simplices collection. Such mapping is performed through the mangrove_tds::SimplexPointerMap::reverseMapping() member function.</li><li><i>sparse</i> if and only if few
			 * simplices (independently from their dimension) are mapped in a continuos set of indices in the same order they appear. Such mapping is performed through the
	 		 * mangrove_tds::SimplexPointerMap::addSimplex() member function.</li></ul>
			 * \param ccw the reference simplicial complex
			 * \see mangrove_tds::SimplexPointerMap::forwardMapping(), mangrove_tds::SimplexPointerMap::reverseMapping(), mangrove_tds::SimplexPointerMap::addSimplex()
			 */
			inline SimplexPointerMap(BaseSimplicialComplex<T> *ccw) { this->setComplex(ccw); }
	 		
	 		/// This member function destroys an instance of this class.
	 		/**
	 		 * \see mangrove_tds::SimplexPointerMap::clearMapping()
	 		 */
	 		inline virtual ~SimplexPointerMap() { this->clearMapping(); }
	 		
	 		/// This member function returns the simplicial complex associated to the current component.
	 		/**
	 		 * This member function returns the simplicial complex associated to the current component: if the simplicial complex to be associated to this component is not valid, then this
			 * member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \return the simplicial complex associated to the current component
	 		 * \see mangrove_tds::SimplexPointerMap::setComplex()
	 		 */
	 		inline BaseSimplicialComplex<T> *getComplex()
	 		{
	 			/* First, we check if all is ok */
	 			assert(this->ccw!=NULL);
	 			return this->ccw;
	 		}
	 		
	 		/// This member function updates the simplicial complex associated to the current component.
	 		/**
	 		 * This member function updates the simplicial complex associated to the current component, by reallocating all the needed internal data structures.<p>If the input simplicial
			 * complex to be associated to this component is not valid or if it has not been initialized, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is
			 * compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> this member function initializes only the simplicial complex
			 * associated to the current component, thus you should apply the member functions offerred by the current class in order to map simplices.<p>A mapping is called:
			 * <ul><li><i>forward</i> if and only if it is performed through an iterator moved in <i>forward</i> direction, i.e. if the iterator is moved towards the bottom of a simplices
			 * collection. Such mapping is performed through the  mangrove_tds::SimplexPointerMap::forwardMapping() member function.</li><li><i>reverse</i> if and only if it is performed
			 * through an iterator moved in <i>reverse</i> direction, i.e. if the iterator is moved towards the beginning of a simplices collection. Such mapping is performed through the
	 		 * mangrove_tds::SimplexPointerMap::reverseMapping() member function.</li><li><i>sparse</i> if and only if few simplices (independently from their dimension) are mapped in a
	 		 * continuos set of indices in the same order they appear. Such mapping is performed through the mangrove_tds::SimplexPointerMap::addSimplex() member function.</li></ul>
			 * \param ccw the new reference simplicial complex
			 * \see mangrove_tds::SimplexPointerMap::getComplex(), mangrove_tds::SimplexPointerMap::forwardMapping(), mangrove_tds::SimplexPointerMap::reverseMapping(),
			 * mangrove_tds::SimplexPointerMap::addSimplex()
	 		 */
	 		inline void setComplex(BaseSimplicialComplex<T> *ccw)
	 		{
	 			SIMPLEX_TYPE t;
	 			
	 			/* First, we check the input parameters! */
	 			assert(ccw!=NULL);
	 			t = ccw->type();
	 			this->clearMapping();
	 			this->ccw = ccw;
	 			for(SIMPLEX_TYPE i=0;i<=t;i++) { this->maps.push_back( map<SIMPLEX_ID,SIMPLEX_ID>()); }
	 		}
	 		
	 		/// This member function deallocates all the internal data structures in the current component.
	 		/**
	 		 * This member function deallocates all the internal data structures in the current component: you should invoke the mangrove_tds::SimplexPointerMap::setComplex() member
	 		 * function in order to reuse this component.
	 		 * \see mangrove_tds::SimplexPointerMap::setComplex(), mangrove_tds::SimplexPointerMap::getComplex(), mangrove_tds::SimplexPointerMap::clearMapping()
	 		 */
	 		inline void clearMapping()
	 		{
	 			/* We deallocates all! */
	 			for(unsigned int k=0;k<this->maps.size();k++) { this->maps[k].clear(); }
	 			this->maps.clear();
	 			this->ccw = NULL;
	 		}
	 		 
	 		/// This member function computes the forward mapping for all the simplices in the associated simplicial complex.
	 		/**
	 		 * This member function computes the forward mapping for all the simplices in the associated simplicial complex, formed by some collections of simplices. All the simplices can be
	 		 * enumerated and implicitly ordered in the same order they appear in the input collection and mapped onto a continuos range of mangrove_tds::SIMPLEX_ID values. It is possible to
	 		 * retrieve the mangrove_tds::SIMPLEX_ID value associated to a simplex through the mangrove_tds::SimplexPointerMap::getId() member function.<p>In this member function we
	 		 * perform a <i>forward</i> mapping since it is performed through an iterator moved in <i>forward</i> direction, i.e. if the iterator is moved towards the bottom of a simplices
	 		 * collection.<p>If the reference simplicial complex has not been associated, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in
			 * debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \see mangrove_tds::SimplexPointerMap::reverseMapping(), mangrove_tds::SimplexPointerMap::addSimplex(), mangrove_tds::SimplexPointerMap::getId(),
			 * mangrove_tds::SimplexIterator
			 */
	 		inline void forwardMapping()
	 		{
	 			SimplexIterator it;
	 			SIMPLEX_ID d;
	 			SIMPLEX_TYPE t;
	 			
	 			/* First, we check if all is ok! */
	 			assert(this->ccw!=NULL);
	 			t = this->ccw->type();
	 			for(unsigned int k=0;k<this->maps.size();k++) { this->maps[k].clear(); }
	 			for(SIMPLEX_TYPE i=0;i<=t;i++)
	 			{
	 				/* Now, we enumerate all the simplices in forward direction! */
	 				d = 0;
	 				for( it=this->ccw->begin(i);it!=this->ccw->end(i);it++)
	 				{
	 					this->maps[i][it.getPointer().id()]=d;
	 					d = d+1;
	 				}
	 			}
	 		}
	 		
	 		/// This member function computes the reverse mapping for all the simplices in the associated simplicial complex.
	 		/**
	 		 * This member function computes the reverse mapping for all the simplices in the associated simplicial complex, formed by some collections of simplices. All the simplices can be
	 		 * enumerated and implicitly ordered in the same order they appear in the input collection and mapped onto a continuos range of mangrove_tds::SIMPLEX_ID values. It is possible to
	 		 * retrieve the mangrove_tds::SIMPLEX_ID value associated to a simplex through the mangrove_tds::SimplexPointerMap::getId() member function.<p>In this member function we
	 		 * perform a <i>reverse</i> mapping since it is performed through an iterator moved in <i>reverse</i> direction, i.e. if the iterator is moved towards the beginning of a simplices
	 		 * collection.<p>If the reference simplicial complex has not been associated, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in
			 * debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \see mangrove_tds::SimplexPointerMap::forwardMapping(), mangrove_tds::SimplexPointerMap::addSimplex(), mangrove_tds::SimplexPointerMap::getId(),
			 * mangrove_tds::SimplexIterator
			 */
			inline void reverseMapping()
	 		{
	 			SimplexIterator it;
	 			SIMPLEX_ID d;
	 			SIMPLEX_TYPE t;
	 			
	 			/* First, we check if all is ok! */
	 			assert(this->ccw!=NULL);
	 			t = this->ccw->type();
	 			for(unsigned int k=0;k<this->maps.size();k++) { this->maps[k].clear(); }
	 			for(SIMPLEX_TYPE i=0;i<=t;i++)
	 			{
	 				/* Now, we enumerate all the simplices in reverse direction! */
	 				d = 0;
	 				for( it=this->ccw->rbegin(i);it!=this->ccw->rend(i);it--)
	 				{
	 					this->maps[i][it.getPointer().id()]=d;
	 					d = d+1;
	 				}
	 			}
	 		}
	 		 
	 		/// This member function computes the sparse mapping for a simplex in the associated simplicial complex.
	 		/**
	 		 * This member function maps a single simplex in the reference simplicial complex and in this way we can obtain the <i>sparse</i> mapping. In this case, only few simplices are
	 		 * mapped onto a continuos range of mangrove_tds::SIMPLEX_ID values. It is possible to retrieve the mangrove_tds::SIMPLEX_ID value associated to a simplex through the
	 		 * mangrove_tds::SimplexPointerMap::getId() member function.<p>The input simplex is required to be <i>valid</i> in the reference simplicial complex: we say that a simplex is
	 		 * <i>valid</i> if and only if its location exists and it has not been marked as <i>deleted</i> by the garbage collector mechanism.<p>If the reference simplicial complex has not
	 		 * been associated or if the required simplex does not exist, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this
			 * case, each forbidden operation will result in a failed assert.
			 * \param cp the required simplex to be added
			 * \return the new identifier associated to the input simplex to be added
	 		 * \see mangrove_tds::SimplexPointerMap::forwardMapping(), mangrove_tds::SimplexPointerMap::reverseMapping(), mangrove_tds::SimplexPointerMap::getId()
	 		 */
	 		inline SIMPLEX_ID addSimplex(const SimplexPointer& cp)
	 		{
	 			map<SIMPLEX_ID,SIMPLEX_ID>::iterator it;
	 			SIMPLEX_ID l;
	 			
	 			/* First, we check if all is ok! */
	 			assert(this->ccw!=NULL);
	 			assert(this->ccw->isValid(cp));
	 			it = this->maps[cp.type()].find(cp.id());
	 			if(it!=this->maps[cp.type()].end()) return it->second;
	 			else
	 			{
	 				/* We can the new simplex 'cp' */
	 				l = this->maps[cp.type()].size();
	 				this->maps[cp.type()][cp.id()]=l;
	 				return l;
	 			}
	 		}
	 		
	 		/// This member function returns the value associated to a particular simplex in the reference simplicial complex.
	 		/**
	 		 * This member function returns the value associated to a particular simplex in the reference simplicial complex: this index is unique inside a particular class of simplices. The
			 * input simplex is required to be <i>valid</i> in the reference simplicial complex: we say that a simplex is <i>valid</i> if and only if its location exists and it has not been
			 * marked as <i>deleted</i> by the garbage collector mechanism.<p>If the reference simplicial has not been associated or if the required simplex does not exist, then this member
			 * function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \param cp a pointer to the required simplex in the reference simplicial complex
	 		 * \return the value associated to the required simplex in the reference simplicial complex
	 		 * \see mangrove_tds::SimplexPointerMap::forwardMapping(), mangrove_tds::SimplexPointerMap::reverseMapping(), mangrove_tds::SimplexPointerMap::addSimplex()
	 		 */
	 		inline SIMPLEX_ID getId(const SimplexPointer& cp)
	 		{
	 			map<SIMPLEX_ID,SIMPLEX_ID>::iterator it;
	 			
	 			/* First, we check if all is ok! */
	 			assert(this->ccw!=NULL);
	 			assert(this->ccw->isValid(cp));
	 			it = this->maps[cp.type()].find(cp.id());
	 			assert( it!=this->maps[cp.type()].end());
	 			return it->second;
	 		}
	 		
	 		protected:
	 		
	 		/// The reference simplicial complex for the current component.
	 		/**
	 		 * \see mangrove_tds::BaseSimplicialComplex
	 		 */
	 		BaseSimplicialComplex<T> *ccw;
	 		
	 		/// The mapping for all the required simplices in the reference simplicial complex.
	 		/**
	 		 * The k-th location of this array contains the mapping for all the valid k-simplices in the reference simplicial complex.
	 		 * \see mangrove_tds::SIMPLEX_ID
	 		 */
	 		vector< map<SIMPLEX_ID,SIMPLEX_ID> > maps;
	 	};
	}

#endif

