/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library                                     
 *                                                                           				    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * October 2011 (Revised on May 2012)
 *                                                                    
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * BaseSimplicialComplex.h - the basic services for a data structure encoding a simplicial complex.
 ***********************************************************************************************/
 
/* Should we include this header file? */
#ifndef BASE_SIMPLICIAL_COMPLEX_H_

	#define  BASE_SIMPLICIAL_COMPLEX_H_
	
	#include "SimplicesContainer.h"
	#include "Property.h"
	#include "Simplex.h"
	#include "Miscellanea.h"
	#include <iostream>
	#include <algorithm>
	#include <limits.h>
	#include <map>
	#include <queue>
	#include <iostream>
	#include <fstream>
	using namespace std;
	using namespace mangrove_tds;
	
	/// Basic description for data structures encoding a simplicial complex with an arbitrary domain.
	/**
	 * The class defined in this file is useful for rapidly prototyping a data structure encoding a simplicial complex with an arbitrary domain.<p>This class is based on the	 
	 * <A href=http://en.wikipedia.org/wiki/Curiously_recurring_template_pattern>Curiously Recurring Template Pattern</A> in order to implement the static polymorphism: in this way, we
	 * can reuse some parts and provide only the member functions specialized for specific data structures. In this way, more efficient implementations can be achieved, by discarding all the
	 * virtual member functions. As examples of data structures, we can mention the <i>Incidence Graph</i> (described in the mangrove_tds::IG class), the <i>Incidence Simplicial</i> data
	 * structure (described in the mangrove_tds::IS class), the <i>Simplified Incidence Graph</i> (described in the mangrove_tds::SIG class), the <i>IA*</i> data structure (described in the
	 * mangrove_tds::GIA class), the <i>Triangle-Segment</i> data structure (described in the mangrove::TriangleSegment class) and the <i>Non-Manifold IA</i> data structure (described
	 * in the mangrove_tds::NMIA class).
	 * \file BaseSimplicialComplex.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	 
	/* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		/// Basic description for data structures encoding a simplicial complex with an arbitrary domain.
		/**
		 * This data structure is useful for rapidly prototyping a data structure encoding a simplicial complex with an arbitrary domain.<p>This class is based on the	
		 * <A href=http://en.wikipedia.org/wiki/Curiously_recurring_template_pattern>Curiously Recurring Template Pattern</A> in order to implement the static polymorphism: in this way,
		 * we can reuse some parts and provide only the member functions specialized for specific data structures. In this way, more efficient implementations can be achieved, by discarding all
		 * the virtual member functions. In particular, we can divide data structures in <i>global</i> and <i>local</i>. For us, a <i>global</i> data structure encodes all simplices in the input
		 * simplicial complex, while a <i>local</i> data structure encodes a subset of all simplices in the input simplicial complex. The <i>Incidence Graph</i> (described in the
		 * mangrove_tds::IG class), the <i>Incidence Simplicial</i> data structure (described in the mangrove_tds::IS class) and the <i>Simplified Incidence Graph</i> (described in the
		 * mangrove_tds::SIG class) are global data structures, while the <i>IA*</i> data structure (described in the mangrove_tds::GIA class) is local. In thei first case, all simplices are
		 * encoded and we can access them through an instance of the mangrove_tds::SimplexPointer class. In the second case, only a subset of simplices are not directly encoded and we can
		 * access them through an instance of the mangrove_tds::GhostSimplexPointer class.<p>Through this class (and specific subclasses), we can describe a simplicial complex with an
		 * arbitrary domain and execute operations on it. For istance, it is possible to:<ul><li>extract topological relations for the topological entities;</li><li>apply editing operators and
		 * modify the topology;</li><li>manage auxiliary information (called <i>property</i>) for each simplex. For example, we can attach Euclidean coordinates, field values, normals to each 
		 * simplex. A different set of properties can be associated to simplices of different dimension: a property is described by a particular subclass of the mangrove_tds::PropertyBase class.
		 * </li></ul>
		 * \see mangrove_tds::IG, mangrove_tds::IS, mangrove_tds::SIG, mangrove_tds::GIA, mangrove_tds::SimplicesContainer, mangrove_tds::PropertyBase,
		 * mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, 
		 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplexPointer
		 */
		template <class Derived> class BaseSimplicialComplex
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function prepares the current data structure for encoding a new simplicial complex, allocating all the internal data structures and without any simplices or
			 * properties (i.e. auxiliary information, like Euclidean coordinates and field values, associated to simplices directly encoded in a data structure).<p>Thus, you should apply
			 * the member functions offered by the mangrove_tds::BaseSimplicialComplex class in order to add simplices and properties to the current data structure encoding a new simplicial
			 * complex.
			 * \param t the dimension of the simplicial complex to be encoded in the current data structure
			 * \see mangrove_tds::BaseSimplicialComplex::init(), mangrove_tds::BaseSimplicialComplex::clear(), mangrove_tds::PropertyBase, mangrove_tds::SIMPLEX_TYPE,
			 * mangrove_tds::Face, mangrove_tds::FaceHierarchy, mangrove_tds::GlobalPropertyHandle, mangrove_tds::BaseSimplicialComplex::addGlobalProperty(),
			 * mangrove_tds::LocalPropertyHandle, mangrove_tds::BaseSimplicialComplex::addLocalPropertyHandle(), mangrove_tds::SparsePropertyHandle,
			 * mangrove_tds::BaseSimplicialComplex::addSparseProperty(), mangrove_tds::GhostPropertyHandle, mangrove_tds::BaseSimplicialComplex::addGhostProperty()
			 */
			inline BaseSimplicialComplex(SIMPLEX_TYPE t)
			{
				/* We create space for a simplicial t-complex */			
				this->simplices.clear();
				this->global_properties.clear();
				this->local_properties.clear();
				this->sparse_properties.clear();
				this->ghost_properties.clear();
				this->visited.clear();
				this->prop4coords.clear();
				this->prop4fvalue.clear();
				this->pascal_triangle.clear();
				this->fposes.clear();
				this->init(t);
			}
			
			/// This member function destroys an instance of this class.
			/**
			 * This member function provides a deep deallocation of the current data structure, removing all the simplices directly encoded and all the properties associated to the current
			 * data structure, through the mangrove_tds::BaseSimplicialComplex::clear() member function.
			 * \see mangrove_tds::BaseSimplicialComplex::clear()
			 */
			inline virtual ~BaseSimplicialComplex() { this->clear(); }
			
			/// This member function prepares the current data structure for encoding a new simplicial complex.
			/**
			 * This member function prepares the current data structure for encoding a new simplicial complex, allocating all the internal data structures and without any simplices or
			 * properties (i.e. auxiliary information, like Euclidean coordinates and field values, associated to simplices directly encoded in a data structure).<p>Thus, you should apply
			 * the member functions offered by the mangrove_tds::BaseSimplicialComplex class in order to add simplices and properties to the current data structure encoding a new simplicial
			 * complex.
			 * \param t the dimension of the simplicial complex to be encoded
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::BaseSimplicialComplex::type(), mangrove_tds::PASCAL_TRIANGLE, mangrove_tds::Face,
			 * mangrove_tds::FaceHierarchy, mangrove_tds::BaseSimplicialComplex::createPascalTriangle(), mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::clear(),
			 * mangrove_tds::GlobalPropertyHandle, mangrove_tds::BaseSimplicialComplex::addGlobalProperty(), mangrove_tds::LocalPropertyHandle,
			 * mangrove_tds::BaseSimplicialComplex::addLocalPropertyHandle(), mangrove_tds::SparsePropertyHandle, mangrove_tds::BaseSimplicialComplex::addSparseProperty(),
			 * mangrove_tds::GhostPropertyHandle, mangrove_tds::BaseSimplicialComplex::addGhostProperty() 
			 */
			inline void init(SIMPLEX_TYPE t)
			{
				/* First, we reserve space for the collection of simplices and properties */
				this->simplices.resize(t+1);
				this->visited.resize(t+1);
				this->local_properties.resize(t+1);
				this->createPascalTriangle(t+1);
				for(SIMPLEX_TYPE k=0;k<t+1;k++)
				{
					/* Now, we update the collection of k-simplices and their properties */
					this->simplices[k].updateType(k);
					this->local_properties[k] = vector<PropertyBase*>();
					this->local_properties[k].clear();
					this->visited[k] = vector<SIMPLEX_ID>();
					this->visited[k].clear();
					this->fposes.push_back(FaceHierarchy(k));
				}
			}
			
			/// This member function deallocates all the internal data structures for the current simplicial complex.
			/**
			 * This member function provides a deep deallocation of the current data structure, removing all the simplices directly encoded and all the properties associated to the current
			 * data structure.<p>Thus, it is mandatory to apply the mangrove_tds::BaseSimplicialComplex::init() member function in order to load a new simplicial complex.
			 * \see mangrove_tds::Face, mangrove_tds::FaceHierarchy, mangrove_tds::PASCAL_TRIANGLE, mangrove_tds::PropertyBase, mangrove_tds::SIMPLE_TYPE,
			 * mangrove_tds::BaseSimplicialComplex::init(), mangrove_tds::BaseSimplicialComplex::removeAllProperties(), mangrove_tds::SimplicesContainer::clearCollection()
			 */
			inline void clear()
			{
				/* First, we destroy all the properties and then all the simplices and 'visited' */
				for(unsigned int k=0;k<pascal_triangle.size();k++) pascal_triangle[k].clear();
				pascal_triangle.clear();
				this->removeAllProperties();
				for(unsigned int k=0;k<this->simplices.size();k++)
				{
					this->simplices[k].clearCollection();
					this->visited[k].clear();
					this->fposes[k].clear();
				}

				/* Now, we have removed all the k-simplices and 'visited[k]', thus we can remove the vectors! */
				this->simplices.clear();
				this->visited.clear();
				this->prop4coords.clear();
				this->prop4fvalue.clear();
				this->fposes.clear();
			}
			
			/// This member function returns a string that describes the data structure used for encoding the current simplicial complex.
 			/**
 			 * This member function returns a string that describes the data structure used for encoding the current simplicial complex.
 			 * \return a string that describes the current data structure
 			 * \see mangrove_tds::BaseSimplicialComplex::debug()
 			 */
 			inline string getDataStructureName() { return static_cast<Derived*>(this)->getDataStructureName(); }
 			
 			/// This member function checks if the current data structure encodes all simplices in the simplicial complex to be represented.
 			/**
 			 * This member function checks if the current data structure encodes all simplices in the current simplicial complex: in this, case it is called <i>global</i> data structure.
			 * Alternatively, a data structure can encode a subset of all simplices, for instance all top simplices: in this case, it is called <i>local</i> data structure.
 			 * \return <ul><li><i>true</i>, if the current data structure encodes all simplices</li><li><i>false</i>, otherwise</li></ul>
 			 * \see mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::isTop(), mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer
 			 */
 			inline bool encodeAllSimplices() { return static_cast<Derived*>(this)->encodeAllSimplices(); }
 			
 			/// This member function computes the storage cost of the current data structure.
			/**
			 * This member function computes the storage cost of the current data structure, expressed as number of pointers to the simplices directly encoded in the current data structure.
			 * In other words, we compute the number of instances of the mangrove_tds::SimplexPointer class required for encoding the current data structure.
			 * \return the storage cost of the current data structure
			 * \see mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::SimplexIterator, mangrove_tds::BaseSimplicialComplex::type(), mangrove_tds::SIMPLEX_TYPE
			 */
 			inline unsigned int getStorageCost() { return static_cast<Derived*>(this)->getStorageCost(); }
 			
			/// This member function returns the dimension of the simplicial complex described by the current data structure.
			/**
			 * This member function returns the dimension of the simplicial complex described by the current data structure: the dimension of the current simplicial complex is well defined
			 * only if we have not invoked the mangrove_tds::BaseSimplicialComplex::clear() member function and thus if and only if the current simplicial complex has been initialized.<p>If
			 * the current simplicial complex has not been initialized, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this
			 * case, each forbidden operation will result in a failed assert.
		 	 * \return the dimension of the simplicial complex described by the current data structure.
		 	 * \see mangrove_tds::BaseSimplicialComplex::init(), mangrove_tds::BaseSimplicialComplex::clear(), mangrove_tds::SimplicesContainer, mangrove_tds::Simplex,
		 	 * mangrove_tds::SimplicesContainer, mangrove_tds::SIMPLEX_TYPE
			 */
			inline SIMPLEX_TYPE type() const
			{
				/* We return the dimension of the current simplicial complex. */
				assert(this->simplices.size()>0);
				return (this->simplices.size()-1); 
			}
			
			/// This member function checks if a simplex is a valid simplex directly encoded in the current data structure.
		 	/**
		 	 * This member function checks if a simplex is a valid simplex directly encoded in the current data structure: we say that a simplex is <i>valid</i> if and only if its location
			 * exists and it has not been marked as <i>deleted</i> by the garbage collector mechanism.<p>Each simplex directly encoded in the current data structure is identified by an
			 * instance of the mangrove_tds::SimplexPointer class and thus by its type and by its identifier.
		 	 * \param cp a pointer to the required simplex
		 	 * \return <ul><li><i>true</i>, if the input simplex is a valid simplex directly encoded in the current data structure</li><li><i>false</i>, otherwise</li></ul>
		 	 * \see mangrove_tds::SimplicesContainer::isValid(), mangrove_tds::SimplexPointer, mangrove_tds::Simplex
		 	 */
		 	inline bool isValid(const SimplexPointer &cp) const
		 	{
		 		/* We check if the required simplex is directly encoded! */
		 		if(cp.type()>this->type()) return false;
		 		else if(cp.id()<this->simplices[cp.type()].size()) return this->simplices[cp.type()].isValid(cp.id());
		 		else return false;
		 	}
		 	
		 	/// This member function returns the number of all the allocated locations for storing simplices in the current data structure.
		 	/**
		 	 * This member function returns the number of all the allocated locations for storing simplices in the current data structure: in this member function, we do not discard locations
		 	 * marked as <i>deleted</i> by the garbage collector mechanism.<p>If we cannot complete this operation for any reason, then this member function will fail if and only if the
		 	 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
		 	 * \param i the dimension of the required simplices directly encoded in the current data structure
		 	 * \return the number of all the allocated locations for storing simplices in the current data structure
		 	 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplicesContainer::vectorSize(), mangrove_tds::BaseSimplicialComplex::isValid()
		 	 */
		 	inline unsigned int vectorSize(SIMPLEX_TYPE i) const 
			{
				/* First, we check if the required class of simplices is valid! */
				assert(i<=this->type());
				return this->simplices[i].vectorSize();
			}
			
			/// This member function returns the number of valid simplices directly stored in the current data structure.
			/**
			 * This member function returns the number of valid simplices directly stored in the current data structure. We say that a simplex is <i>valid</i> if and only if its location
			 * exists and it has not been marked as <i>deleted</i> by the garbage collector mechanism.<p>If we cannot complete this operation, then this member function will fail if and only
			 * if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param i the dimension of the required simplices directly encoded in the current data structure
			 * \return the number of valid simplices directly stored in the current data structure
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplicesContainer::size(), mangrove_tds::Simplex::isDeleted()
			 */
			inline unsigned int size(SIMPLEX_TYPE i) const
			{
				/* First, we check if the required class of simplices is valid! */
				assert(i<=this->type());
				return this->simplices[i].size();
			}
			
			/// This member function returns the number of locations for storing simplices marked as deleted in the current data structure.
			/**
			 * This member function returns the number of locations for storing simplices marked as deleted in the current data structure: in this component and in particular in the
			 * mangrove_tds::SimplicesContainer class, a location is not physically destroyed, but it is marked as <i>deleted</i> and it cannot be accessed until we reuse it.<p>If we cannot
			 * complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation
			 * will result in a failed assert.
			 * \param i the dimension of the required simplices directly encoded in the current data structure
			 * \return the number of locations for storing simplices marked as deleted in the current data structure
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplicesContainer::deletedSize(), mangrove_tds::Simplex::isDeleted()
			 */
			inline unsigned int deletedSize(SIMPLEX_TYPE i) const
			{
				/* First, we check if the required class of simplices is valid! */
				assert(i<=this->type());
				return this->simplices[i].deletedSize();
			}
			
			/// This member function returns a reference to a simplex directly encoded in the current data structure.
		 	/**
		 	 * This member function returns a reference to a simplex directly encoded in the current data structure: the required simplex must be valid. We say that a simplex is <i>valid</i>
			 * if and only if its location exists and it has not been marked as <i>deleted</i> by the garbage collector mechanism.<p>Each simplex directly encoded in the current data
			 * structure is identified by an instance of the mangrove_tds::SimplexPointer class and thus by its type and by its identifier. If we cannot complete this operation, then this
			 * member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
		 	 * \param cp a pointer to the required simplex
		 	 * \return a reference to a simplex directly encoded in the current data structure
		 	 * \see mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::BaseSimplicialComplex::simplexc(),
		 	 * mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplicesContainer::simplex(), mangrove_tds::BaseSimplicial::ghostSimplex()
		 	 */
		 	inline Simplex& simplex(const SimplexPointer &cp)
			{
				/* First, we check if the required simplex exists! */
				assert(cp.type()<=this->type());
				return this->simplices[cp.type()].simplex(cp.id());
			}
			
			/// This member function returns a constant reference to a simplex directly encoded in the current data structure.
		 	/**
		 	 * This member function returns a constant reference to a simplex directly encoded in the current data structure: the required simplex must be valid. We say that a simplex is
		 	 * <i>valid</i> if and only if its location exists and it has not been marked as <i>deleted</i> by the garbage collector mechanism.<p>Each simplex directly encoded in the current
		 	 * data structure is identified by an instance of the mangrove_tds::SimplexPointer class and thus by its type and by its identifier. If we cannot complete this operation, then
			 * this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
		 	 * \param cp a pointer to the required simplex
		 	 * \return a constant reference to a simplex directly encoded in the current data structure
		 	 * \see mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::BaseSimplicialComplex::simplex(),
		 	 * mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplicesContainer::simplexc(), mangrove_tds::BaseSimplicial::ghostSimplex()
		 	 */
			inline const Simplex& simplexc(const SimplexPointer &cp) const
			{
				/* First, we check if the required simplex exists! */
				assert(cp.type()<=this->type());
				return this->simplices[cp.type()].simplexc(cp.id());
			}
			
			/// This member function returns a description of a ghost simplex in the current data structure.
			/**
			 * This member function returns a description of a ghost simplex in the current data structure, i.e. a simplex not directly encoded in a data structure: such simplex can be
			 * accessed through an instance of the mangrove_tds::GhostSimplexPointer class.<p>A pointer to a simplex not directly encoded in data structure (namely a <i>ghost simplex</i>) is
			 * composed by a tuple <i>(tp,ti,cp,ci)</i>, describing a ghost simplex as the <i>ci</i>-th face of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension <i>tp</i>.
			 * Such simplices exist only inside <i>local</i> data structures, i.e. data structures not encoding all simplices: for istance, the <i>IA*</i> (described by the mangrove_tds::GIA
			 * class), the <i>Non-Manifold IA</i> (described by the mangrove_tds::NMIA class), and the <i>Triangle-Segment</i> (described by the mangrove_tds::TriangleSegment class) are
			 * examples of <i>local</i> data structures.<p>If we cannot complete this operation for any reason, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param gsp a pointer to the required ghost simplex
			 * \param ghost a description for the required ghost simplex
			 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GIA,
			 * mangrove_tds::TriangleSegment, mangrove_tds::NMIA, mangrove_tds::PASCAL_TRIANGLE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(),
			 * mangrove_tds::BaseSimplicialComplex::simplex(), mangrove_tds::BaseSimplicialComplex::simplexc(), mangrove_tds::BaseSimplicialComplex::isValid()
			 */
			inline void ghostSimplex(const GhostSimplexPointer &gsp,GhostSimplex *ghost)
			{
				unsigned int t,ct,ci,k;
				vector<unsigned int> indices;

				/* First, we can check if all is ok! */
				assert(ghost!=NULL);
				assert(this->encodeAllSimplices()==false);
				assert(this->isValid(SimplexPointer(gsp.getParentType(),gsp.getParentId())));
				assert(this->isTop(SimplexPointer(gsp.getParentType(),gsp.getParentId())));
				ghost->clear();
				t=gsp.getParentType();
				ct=gsp.getChildType();
				ci=gsp.getChildId();
				if(gsp.isEncoded()==false)
				{
					/* We must have a valid child of a valid top simplex! */
					assert(ct<t);
					assert(ci<this->pascal_triangle[t+1][ct+1]);
				}
				
				/* Now, we identify vertices positions to be considered */
				if(t==0) { ghost->getBoundary().push_back(SimplexPointer(0,gsp.getParentId())); }
				else
				{
					indices=vector<unsigned int>(this->fposes[t].getHierarchy()[ct][ci].getCVertices());
					for(k=0;k<indices.size();k++) { ghost->getBoundary().push_back(SimplexPointer(this->simplex(SimplexPointer(t,gsp.getParentId())).b(indices[k]))); }
				}

				/* Now, we can update  the pointer for the 'ghost' simplex! */
				ghost->getGhostSimplexPointer().setParentType(t);
				ghost->getGhostSimplexPointer().setParentId(gsp.getParentId());
				ghost->getGhostSimplexPointer().setChildType(ct);
				ghost->getGhostSimplexPointer().setChildId(ci);
			}
			
			/// This member function retrieves a subset of ghost simplices in the current data structure.
			/**
			 * This member function retrieves a subset of ghost simplices, identified by their dimension, in the current data structure, i.e. simplices not directly encoded in the current
			 * data structure, assumed to be <i>local</i>. For istance, the <i>IA*</i> (described by the mangrove_tds::GIA class), the <i>Non-Manifold IA</i> (described by the
			 * mangrove_tds::NMIA class) and the <i>Triangle-Segment</i> (described by the mangrove_tds::TriangleSegment class) are examples of <i>local</i> data structures.<p>Each ghost
			 * simplex is described through an instance of the mangrove_tds::GhostSimplex class and it can be referred through a ghost simplex pointer, described by an instance of the
			 * mangrove_tds::GhostSimplexPointer class.<p><b>IMPORTANT:</b> the required ghost simplices are encoded through an instance of the mangrove_tds::GS_COMPLEX, because
			 * each ghost simplex can be referred through many ghost simplices pointers.<p>If we cannot complete this operation for any reason, then this member function will fail if and
			 * only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param d the dimension of the required ghost simplices
			 * \param gs the required ghost simplices
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GS_COMPLEX, mangrove_tds::SimplexIterator, mangrove_tds::Simplex, mangrove_tds::GhostSimplex,
			 * mangrove_tds::GhostSimplexPointer, mangrove_tds::GIA, mangrove_tds::TriangleSegment, mangrove_tds::NMIA
			 */
			inline void retrieveAllSimplices(SIMPLEX_TYPE d,GS_COMPLEX &gs)
			{
				SimplexIterator it;
				SIMPLEX_TYPE k;
				GhostSimplex s;
				list<GhostSimplexPointer> bnd;
				list<GhostSimplexPointer>::iterator bnd_it;
				SimplexPointer f;
				unsigned int pos;

				/* First, we must understand what simplices we must consider! */
				assert(this->encodeAllSimplices()==false);
				gs.clear();
				if(d==0)
				{
					/* We require all vertices! */
					for(it=this->begin(d);it!=this->end(d);it++)
					{
						/* We check if the current vertex is top */
						if(this->isTop(it.getPointer()))
						{
							this->ghostSimplex(GhostSimplexPointer(it.getPointer().type(),it.getPointer().id()),&s);
							gs.insert(GhostSimplex(s));
						}
						else
						{
							f=SimplexPointer(this->simplex(it.getPointer()).cc(0));
							this->simplex(f).positionInBoundary(it.getPointer(),pos);
							this->ghostSimplex(GhostSimplexPointer(f.type(),f.id(),0,pos),&s);
							gs.insert(GhostSimplex(s));
						}
					}
				}
				else
				{
					/* We require all d-simplices (not vertices): we start from top d-simplices */
					for(it=this->begin(d);it!=this->end(d);it++)
					{
						/* We require only top simplices */
						if(this->isTop(it.getPointer()))
						{
							this->ghostSimplex(GhostSimplexPointer(it.getPointer().type(),it.getPointer().id()),&s);
							gs.insert(GhostSimplex(s));
						}
					}
					
					/* Do we require maximal simplices? */
					if(d!=this->type())
					{
						/* Now, we visit the other top simplices */
						for(k=d+1;k<=this->type();k++)
						{
							/* Now, we extract all the d-subfaces of the k-simplices */
							for(it=this->begin(k);it!=this->end(k);it++)
							{
								/* We require only top simplices */
								if(this->isTop(it.getPointer()))
								{
									this->boundaryk(GhostSimplexPointer(it.getPointer().type(),it.getPointer().id()),d,&bnd);
									for(bnd_it=bnd.begin();bnd_it!=bnd.end();bnd_it++)
									{
										this->ghostSimplex(GhostSimplexPointer(*bnd_it),&s);
										gs.insert(GhostSimplex(s));
									}
								}
							}
						}
					}
				}
			}
			
			/// This member function returns an iterator to the first valid simplex of a certain dimension directly encoded in the current data structure.
			/**
			 * This member function returns an iterator to the first valid simplex of a certain dimension directly encoded in the current data structure. All the k-simplices in the
			 * simplicial complex described by the current data structure are stored in a collection of simplices, described by the mangrove_tds::SimplicesContainer class and identified by
			 * its dimension k, i.e. its position in the mangrove_tds::BaseSimplicialComplex::simplices member field. All the simplices in the required collection can be enumerated and
			 * implicitly ordered as they appear in the required collection: this enumeration can be executed through an iterator (described in the mangrove_tds::SimplexIterator class),
			 * moved only on the valid simplices, i.e. simplices not marked as <i>deleted</i> by the garbage collector mechanism.<p>This member function returns an iterator to the first
			 * valid simplex that we can meet by moving in <i>forward</i> direction, i.e. towards the bottom of the required collection of simplices. Alternatively, such simplex is the last
			 * simplex that we can meet by moving in <i>reverse</i> direction, i.e. towards the beginning of the required collection of simplices.<p>If we cannot complete this operation for
			 * any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in
			 * a failed assert.
			 * \param i the dimension of the required class of simplices directly encoded in the current simplicial complex
			 * \return <ul><li>an iterator to the first valid simplex in the current collection of simplices, if it exists</li><li>a not initialized iterator, otherwise</li></ul>
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexIterator, mangrove_tds::SimplicesContainer, mangrove_tds::SimplexPointer,
			 * mangrove_tds::BaseSimplicialComplex::end(), mangrove_tds::BaseSimplicialComplex::rbegin(), mangrove_tds::BaseSimplicialComplex::rend(),
			 * mangrove_tds::BaseSimplicialComplex::type()
			 */
			inline SimplexIterator begin(SIMPLEX_TYPE i)
		 	{
		 		/* First, we check if the required class of simplices is valid */
		 		assert(i<= this->type());
		 		return this->simplices[i].begin();
		 	}
		 	
		 	/// This member function returns an iterator to the bottom of a collection of simplices directly encoded in the current data structure.
		 	/**
		 	 * This member function returns an iterator to the bottom of a collection of simplices directly encoded in the current data structure. All the k-simplices in the simplicial
			 * complex described by the current data structure are stored in a collection of simplices, described by the mangrove_tds::SimplicesContainer class and identified by its
			 * dimension k, i.e. its position in the mangrove_tds::BaseSimplicialComplex::simplices member field. All the simplices in the required collection can be enumerated and
			 * implicitly ordered as they appear in the required collection: this enumeration can be executed through an iterator (described in the mangrove_tds::SimplexIterator class),
			 * moved only on the valid simplices, i.e. simplices not marked as <i>deleted</i> by the garbage collector mechanism.<p>This member function returns an iterator to a particular
			 * simplex, that marks the bottom of the required collection of simplices by moving in <i>forward</i> direction, i.e. towards the bottom of the required collection:
			 * alternatively, this particular simplex marks the beginning of the required collection of simplices by moving in <i>reverse</i> direction, i.e. towards the beginning of the
			 * collection.<p>If we cannot complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug
			 * mode. In this case, each forbidden operation will result in a failed assert.
		 	 * \param i the dimension of the required class of simplices in the current simplicial complex
		 	 * \return an iterator to the bottom of the required collection of simplices directly encoded in the current data structure
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexIterator, mangrove_tds::SimplicesContainer, mangrove_tds::SimplexPointer,
			 * mangrove_tds::BaseSimplicialComplex::begin(), mangrove_tds::BaseSimplicialComplex::rbegin(), mangrove_tds::BaseSimplicialComplex::rend(),
			 * mangrove_tds::BaseSimplicialComplex::type()
		 	 */
		 	inline SimplexIterator end(SIMPLEX_TYPE i)
		 	{
		 		/* Firtst, we check if the required class of simplices is valid! */
		 		assert(i<=this->type());
		 		return this->simplices[i].end();
		 	}
		 	
		 	/// This member function returns an iterator to the last valid simplex of a certain dimension directly encoded in the current data structure.
		 	/**
		 	 * This member function returns an iterator to the last valid simplex of a certain dimension directly encoded in the current data structure. All the k-simplices in the simplicial
			 * complex described by the current data structure are stored in a collection of simplices, described by the mangrove_tds::SimplicesContainer class and identified by its
			 * dimension k, i.e. its position in the mangrove_tds::BaseSimplicialComplex::simplices member field. All the simplices in the required collection can be enumerated and
			 * implicitly ordered as they appear in the required collection: this enumeration can be executed through an iterator (described in the mangrove_tds::SimplexIterator class),
			 * moved only on the valid simplices, i.e. simplices not marked as <i>deleted</i> by the garbage collector mechanism.<p>This member function returns an iterator to the last valid
			 * simplex that we can meet by moving in <i>forward</i> direction, i.e. towards the bottom of the required collection of simplices. Alternatively, such simplex is the first
			 * simplex that we can meet by moving in <i>reverse</i> direction, i.e. towards the beginning of the required collection of simplices.<p>If we cannot complete this operation for
			 * any reason,  then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result
			 * in a failed assert.
			 * \param i the dimension of the required class of simplices directly encoded in the current simplicial complex
			 * \return <ul><li>an iterator to the last valid simplex in the current collection of simplices, if it exists</li><li>a not initialized iterator, otherwise</li></ul>
		 	 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexIterator, mangrove_tds::SimplicesContainer, mangrove_tds::SimplexPointer,
		 	 * mangrove_tds::BaseSimplicialComplex::begin(), mangrove_tds::BaseSimplicialComplex::end(), mangrove_tds::BaseSimplicialComplex::rend(),
		 	 * mangrove_tds::BaseSimplicialComplex::type()
		 	 */			 
		 	inline SimplexIterator rbegin(SIMPLEX_TYPE i)
		 	{
		 		/* First, we check if the required class of simplices is valid! */
		 		assert(i<=this->type());
		 		return this->simplices[i].rbegin();
		 	}
		 	
		 	/// This member function returns an iterator to the beginning of a collection of simplices directly encoded in the current data structure.
		 	/**
		 	 * This member function returns an iterator to the beginning of a collection of simplices directly encoded in the current data structure. All the k-simplices in the simplicial
			 * complex described by the current data structure are stored in a collection of simplices, described by the mangrove_tds::SimplicesContainer class and identified by its
			 * dimension k, i.e. its position in the mangrove_tds::BaseSimplicialComplex::simplices member field. All the simplices in the required collection can be enumerated and
			 * implicitly ordered as they appear in the required collection: this enumeration can be executed through an iterator (described in the mangrove_tds::SimplexIterator class),
			 * moved only on the valid simplices, i.e. simplices not marked as <i>deleted</i> by the garbage collector mechanism.<p>This member function returns an iterator to a particular
			 * simplex, that marks the beginning of the required collection of simplices by moving in <i>forward</i> direction, i.e. towards the bottom of the required collection:
			 * alternatively, this particular simplex marks the bottom of the required collection of simplices by moving in <i>reverse</i> direction, i.e. towards the beginning of the
			 * collection.<p>If we cannot complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug
			 * mode. In this case, each forbidden operation will result in a failed assert.
		 	 * \param i the dimension of the required class of simplices in the current simplicial complex
		 	 * \return an iterator to the beginning of the required collection of simplices directly encoded in the current data structure
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexIterator, mangrove_tds::SimplicesContainer, mangrove_tds::SimplexPointer,
			 * mangrove_tds::BaseSimplicialComplex::begin(), mangrove_tds::BaseSimplicialComplex::end(), mangrove_tds::BaseSimplicialComplex::rbegin(),
			 * mangrove_tds::BaseSimplicialComplex::type()
		 	 */
		 	inline SimplexIterator rend(SIMPLEX_TYPE i)
		 	{
		 		/* Firtst, we check if the required class of simplices is valid! */
		 		assert(i<=this->type());
		 		return this->simplices[i].rend();
		 	}
		 	
		 	/// This member function creates a new location for storing a vertex to be directly encoded in the current data structure.
		 	/**
		 	 * This member function creates a new location for storing a 0-simplex (namely a vertex) to be directly encoded in the current data structure: we do not provide any information
			 * about its coboundary and adjacency relation. At the mean time, we also resize all the properties (i.e. auxiliary information, like Euclidean coordinates and field values),
			 * associated to simplices directly encoded in the current data structure.<p>If we cannot complete this operation for any reason, then this member function will fail if and only
			 * if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param cp a pointer that can be used to access the new vertex
		 	 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::PropertyBase,
		 	 * mangrove_tds::BaseSimplicialComplex::getLocalPropertiesNumber(), mangrove_tds::BaseSimplicialComplex::getGlobalPropertiesNumber(),
		 	 * mangrove_tds::SimplicesContainer::newSimplex(), mangrove_tds::SimplicesContainer::addSimplex(), mangrove_tds::BaseSimplicialComplex::isValid()
		 	 */
		 	inline void addSimplex(SimplexPointer &cp)
			{
				SimplexPointer aux;
				
				/* We must create a vertex */
				this->addSimplex(0,aux);
				cp.setType(aux.type());
				cp.setId(aux.id());
			}
		 	
		 	/// This member function creates a new location for a simplex to be directly encoded in the current data structure.
		 	/**
		 	 * This member function creates a new location for a simplex to be directly encoded in the current data structure: we provide only the dimension of the new simplex and thus we
			 * have not any information about its boundary, coboundary and adjacency relation. At the mean time, we also resize all the properties (i.e. auxiliary information, like Euclidean
			 * coordinates and field values), associated to simplices directly encoded in the current data structure.<p>If we cannot complete this operation for any reason, then this member
			 * function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param ct the dimension of the location for the new simplex to be directly encoded in the current data structure
			 * \param cp a pointer that can be used to access the new simplex
		 	 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::getLocalPropertiesNumber(),
		 	 * mangrove_tds::BaseSimplicialComplex::getGlobalPropertiesNumber(), mangrove_tds::SimplicesContainer::newSimplex(), mangrove_tds::SimplicesContainer::addSimplex(),
		 	 * mangrove_tds::BaseSimplicialComplex::sortBoundary(), mangrove_tds::BaseSimplicialComplex::isValid()
		 	 */
		 	inline void addSimplex(SIMPLEX_TYPE ct, SimplexPointer& cp)
			{
				unsigned int loc,glb,lct;
				SimplexPointer aux;
				vector<PropertyBase*>::iterator pb;
				
				/* First, we check if we can add a similar simplex in the current simplicial complex */
				assert(ct<=this->type());
				loc = this->getLocalPropertiesNumber(ct);
				glb = this->getGlobalPropertiesNumber();
				if((loc+glb)==0) { this->simplices[ct].addSimplex(aux); }
				else if((loc!=0) && (glb==0)) { this->simplices[ct].newSimplex(this->local_properties[ct],aux); }
				else if( (glb!=0) && (loc==0)) { this->simplices[ct].newSimplex(this->global_properties,aux); }
				else
				{
					/* We create a new simplex and resize all the properties! */
					this->simplices[ct].newSimplex(this->global_properties,aux);
					lct = this->simplices[ct].size();
					for(pb=this->local_properties[ct].begin();pb!=this->local_properties[ct].end();pb++) { (*pb)->resize(lct); }
				}
				
				/* Now, we copy 'aux' in 'cp' */
				cp.setType(aux.type());
				cp.setId(aux.id());
			}
			
			/// This member function creates a new location for storing an edge to be directly encoded in the current data structure.
			/**
			 * This member function creates a new location for storing a 1-simplex (namely an edge) to be directly encoded in the current data structure: we do not provide any information
			 * about its coboundary and adjacency relation. At the mean time, we also resize all the properties (i.e. auxiliary information, like Euclidean coordinates and field values),
			 * associated to simplices directly encoded in the current data structure.<p>An edge is described by its two endpoints, i.e. by its two vertices: they are required to be valid
			 * 0-simplices directly encoded in the current data structure. We say that a simplex is <i>valid</i> if and only if its location exists and it has not been marked as
			 * <i>deleted</i> by the garbage collector mechanism.<p>If we cannot complete this operation for any reason, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode.
			 * In this case, each forbidden operation will result in a failed assert.
			 * \param a a pointer to the first endpoint of the new edge to be added
			 * \param b a pointer to the second endpoint of the new edge to be added
		 	 * \param cp a pointer that can be used to access the new edge
		 	 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::getLocalPropertiesNumber(),
		 	 * mangrove_tds::BaseSimplicialComplex::getGlobalPropertiesNumber(), mangrove_tds::SimplicesContainer::newSimplex(), mangrove_tds::SimplicesContainer::addSimplex(),
		 	 * mangrove_tds::BaseSimplicialComplex::sortBoundary(), mangrove_tds::BaseSimplicialComplex::isValid()
			 */
			inline void addSimplex(const SimplexPointer& a,const SimplexPointer& b, SimplexPointer& cp)
			{
				SimplexPointer aux;
				vector<SimplexPointer> v;
				
				/* Now, we compose the boundary and then we proceed */
				v.resize(2);
				v[0]=SimplexPointer(a);
				v[1]=SimplexPointer(b);
				this->addSimplex(v,aux);		
				cp.setType(aux.type());
				cp.setId(aux.id());
			}
			
			/// This member function creates a new location for storing a triangle to be directly encoded in the current data structure.
			/**
			 * This member function creates a new location for storing a 2-simplex (namely a triangle) to be directly encoded in the current data structure: we do not provide any information
			 * about its coboundary and adjacency relation. At the mean time, we also resize all the properties (i.e. auxiliary information, like Euclidean coordinates and field values),
			 * associated to simplices directly encoded in the current data structure.<p>A triangle is described by three simplices belonging to its boundary, required to be valid simplices
			 * in the current data structure. We say that a simplex is <i>valid</i> if and only if its location exists and it has not been marked as <i>deleted</i> by the garbage collector
			 * mechanism.<p>If we cannot complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode.
			 * In this case, each forbidden operation will result in a failed assert.
			 * \param a a pointer to a simplex belonging to the boundary of the new triangle to be added
			 * \param b a pointer to a simplex belonging to the boundary of the new triangle to be added
			 * \param c a pointer to a simplex belonging to the boundary of the new triangle to be added
			 * \param cp a pointer that can be used to access the new triangle
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::getLocalPropertiesNumber(),
			 * mangrove_tds::BaseSimplicialComplex::getGlobalPropertiesNumber(), mangrove_tds::SimplicesContainer::newSimplex(), mangrove_tds::SimplicesContainer::addSimplex(),
			 * mangrove_tds::BaseSimplicialComplex::sortBoundary(), mangrove_tds::BaseSimplicialComplex::isValid()
			 */
			inline void addSimplex(const SimplexPointer& a,const SimplexPointer&b, const SimplexPointer&c, SimplexPointer& cp)
			{
				SimplexPointer aux;
				vector<SimplexPointer> v;
				
				/* Now, we compose the boundary and then we proceed */
				v.resize(3);
				v[0]=SimplexPointer(a);
				v[1]=SimplexPointer(b);
				v[2]=SimplexPointer(c);
				this->addSimplex(v,aux);
				cp.setType(aux.type());
				cp.setId(aux.id());
			}

			/// This member function creates a new location for storing a tetrahedron to be directly encoded in the current data structure.
			/**
			 * This member function creates a new location for storing a 3-simplex (namely a tetrahedron) to be directly encoded in the current data structure: we do not provide any
			 * information about its coboundary and adjacency relation. At the mean time, we also resize all the properties (i.e. auxiliary information, like Euclidean coordinates and field
			 * values), associated to simplices directly encoded in the current data structure.<p>A tetrahedron is described by four simplices belonging to its boundary, required to be valid
			 * simplices in the current data structure. We say that a simplex is <i>valid</i> if and only if its location exists and it has not been marked as <i>deleted</i> by the garbage
			 * collector mechanism.<p>If we cannot complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in
			 * debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param a a pointer to a simplex belonging to the boundary of the new tetrahedron to be added
			 * \param b a pointer to a simplex belonging to the boundary of the new tetrahedron to be added
			 * \param c a pointer to a simplex belonging to the boundary of the new tetrahedron to be added
			 * \param d a pointer to a simplex belonging to the boundary of the new tetrahedron to be added
			 * \param cp a pointer that can be used to access the new tetrahedron
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::getLocalPropertiesNumber(),
			 * mangrove_tds::BaseSimplicialComplex::getGlobalPropertiesNumber(), mangrove_tds::SimplicesContainer::newSimplex(), mangrove_tds::SimplicesContainer::addSimplex(),
			 * mangrove_tds::BaseSimplicialComplex::sortBoundary(), mangrove_tds::BaseSimplicialComplex::isValid()
			 */
			inline void addSimplex(const SimplexPointer& a,const SimplexPointer& b,const SimplexPointer& c,const SimplexPointer& d,SimplexPointer& cp)
			{
				SimplexPointer aux;
				vector<SimplexPointer> v;
				
				/* Now, we compose the boundary and then we proceed */
				v.resize(4);
				v[0]=SimplexPointer(a);
				v[1]=SimplexPointer(b);
				v[2]=SimplexPointer(c);
				v[3]=SimplexPointer(d);
				this->addSimplex(v,aux);
				cp.setType(aux.type());
				cp.setId(aux.id());
			}

		 	/// This member function creates a new location for a simplex to be directly encoded in the current data structure.
			/**
			 * This member function creates a new location for a simplex to be directly encoded in the current data structure: we do not provide any information about its coboundary and
			 * adjacency relation. At the mean time, we also resize all the properties (i.e. auxiliary information, like Euclidean coordinates and field values), associated to simplices
			 * directly encoded in the current data structure.<p>Such simplex is described by some simplices belonging to its boundary, required to be valid in the current data structure. We
			 * say that a simplex is <i>valid</i> if and only if its location exists and it has not been marked as <i>deleted</i> by the garbage collector mechanism.<p>If we cannot complete
			 * this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.
			 * \param v the pointers to simplices belonging to the boundary of the new simplex to be added
			 * \param cp a pointer that can be used to access the new simplex
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::getLocalPropertiesNumber(),
			 * mangrove_tds::BaseSimplicialComplex::getGlobalPropertiesNumber(), mangrove_tds::SimplicesContainer::newSimplex(), mangrove_tds::SimplicesContainer::addSimplex(),
			 * mangrove_tds::BaseSimplicialComplex::sortBoundary(), mangrove_tds::BaseSimplicialComplex::isValid()
		 	 */
		 	inline void addSimplex(vector<SimplexPointer> &v, SimplexPointer& cp)
			{
				SimplexPointer aux;
				SIMPLEX_TYPE t,vt;
				vector<SimplexPointer>::iterator it;
				
				/* First, we must understand the number of elements in the boundary! */
				if(v.size()==0) { this->addSimplex(aux); }
				else
				{
					/* Now, we check if the boundary is valid */
					t = this->type();
					assert(v.size()>=2);
					vt = v.size()-1;
					assert(t>=vt);
					for(it =v.begin();it!=v.end();it++)
					{
						/* We must check if the current boundary simplex size is 'vt-1' and if its valid. */
						assert( this->isValid(*it));
						assert( (*it).type()<=vt-1);
					}
					
					/* If we arrive here, the boundary is valid and then we can create a new simplex, by updating its boundary (and ordering)l */
					this->addSimplex(vt,aux);
					for(SIMPLEX_TYPE k=0;k<v.size();k++) { this->simplex(aux).b(k).setId(v.at(k).id()); }
					for(SIMPLEX_TYPE k=0;k<v.size();k++) { this->simplex(aux).b(k).setType(v.at(k).type()); }
					this->sortBoundary(aux);
				}
				
				/* If we arrive here, we can update 'cp' and exit! */
				cp.setType(aux.type());
				cp.setId(aux.id());
			}

		 	/// This member function checks if a simplex directly encoded in the current data structure is top.
		 	/**
		 	 * This member function checks if a simplex directly encoded in the current data structure is top: we say that a simplex is a <i>top simplex</i> if and only if its coboundary is
			 * empty. Such simplex is required to be <i>valid</i>: we say that a simplex is <i>valid</i> if and only if its location exists and it has not been marked as <i>deleted</i> by
			 * the garbage collector mechanism.<p>Each simplex directly encoded in the current data structure is identified by an instance of the mangrove_tds::SimplexPointer class and thus
			 * by its type and by its identifier. If we cannot complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is
			 * compiled in debug mode. In this case, each forbidden operation will result in a failed assert.	
			 * \param cp a pointer to a simplex directly encoded in the current data structure
		 	 * \return <ul><li><i>true</i>, if the input simplex is top</li><li><i>false</i>, otherwise</li></ul>
		 	 * \see mangrove_tds::BaseSimplicialComplex::simplexc(), mangrove_tds::Simplex, mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::SIMPLEX_TYPE,
		 	 * mangrove_tds::SIMPLEX_ID, mangrove_tds::BaseSimplicialComplex::getTopSimplicesNumber(), mangrove_tds::BaseSimplicialComplex::isValid()
			 */
		 	inline bool isTop(const SimplexPointer &cp) const { return this->simplexc(cp).c().empty(); }
		 	
		 	/// This member function returns the number of top simplices directly encoded in the current data structure.
		 	/**
		 	 * This member function returns the number of top simplices directly encoded in the current data structure: we say that a simplex is a <i>top simplex</i> if and only if its
			 * coboundary is empty. Such simplices are required to be <i>valid</i>: we say that a simplex is <i>valid</i> if and only if its location exists and it has not been marked as
			 * <i>deleted</i> by the garbage collector mechanism.<p>Each simplex directly encoded in the current data structure is identified by an instance of the
			 * mangrove_tds::SimplexPointer class and thus by its type and by its identifier.
		 	 * \param d the dimension of the required top simplices directly encoded in the current data structure
		 	 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexIterator, mangrove_tds::BaseSimplicialComplex::isTop(), mangrove_tds::SimplexPointer,
		 	 * mangrove_tds::BaseSimplicialComplex::begin(), mangrove_tds::BaseSimplicialComplex::end()
		 	 */
		 	inline unsigned int getTopSimplicesNumber(SIMPLEX_TYPE d)
			{
				unsigned int nt;
				SimplexIterator it;
				
				/* Now, we extract the number of top simplices! */
				nt=0;
				for(it=this->begin(d);it!=end(d);it++) { if(this->isTop(SimplexPointer(it.getPointer()))) nt=nt+1; }
				return nt;
			}
			
			/// This member function retrieves the top simplices directly encoded in the current data structure.
			/**
			 * This member function retrieves the top simplices directly encoded in the current data structure: we say that a simplex is a <i>top simplex</i> if and only if its coboundary is
			 * empty. Such simplices are required to be <i>valid</i>: we say that a simplex is <i>valid</i> if and only if its location exists and it has not been marked as <i>deleted</i> by
			 * the garbage collector mechanism.<p>Each simplex directly encoded in the current data structure is identified by an instance of the mangrove_tds::SimplexPointer class and thus
			 * by its type and by its identifier.
			 * \param d the dimension of the required top simplices directly encoded in the current data structure
			 * \param l the required top simplices directly encoded in the current data structure
			 * \return the number of the required top simplices directly encoded in the current data structure
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexIterator, mangrove_tds::BaseSimplicialComplex::isTop(), mangrove_tds::SimplexPointer,
			 * mangrove_tds::BaseSimplicialComplex::begin(), mangrove_tds::BaseSimplicialComplex::end()
			 */
			inline unsigned int getTopSimplices(SIMPLEX_TYPE d,list<SimplexPointer> &l)
			{
				/* Now, we extract all the top simplices! */
				l.clear();
				for(SimplexIterator it=this->begin(d);it!=end(d);it++) { if(this->isTop(SimplexPointer(it.getPointer()))) l.push_back(SimplexPointer(it.getPointer())); }
				return l.size();
			}
			
			/// This member function retrieves the top simplices directly encoded in the current data structure.
			/**
			 * This member function retrieves the top simplices directly encoded in the current data structure: we say that a simplex is a <i>top simplex</i> if and only if its coboundary is
			 * empty. Such simplices are required to be <i>valid</i>: we say that a simplex is <i>valid</i> if and only if its location exists and it has not been marked as <i>deleted</i> by
			 * the garbage collector mechanism.<p>Each simplex directly encoded in the current data structure is identified by an instance of the mangrove_tds::SimplexPointer class and thus
			 * by its type and by its identifier.
			 * \param l the required top simplices directly encoded in the current data structure
			 * \return the number of the required top simplices directly encoded in the current data structure
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexIterator, mangrove_tds::BaseSimplicialComplex::isTop(), mangrove_tds::SimplexPointer,
			 * mangrove_tds::BaseSimplicialComplex::begin(), mangrove_tds::BaseSimplicialComplex::end()
			 */
			inline unsigned int getTopSimplices(list<SimplexPointer> &l)
			{
				/* Now, we extract all the top simplices! */
				l.clear();
				for(SIMPLEX_TYPE d=0;d<=this->type();d++)
				{
					/* Now, we enumerate all the d-simplices and look for the top ones! */
					for(SimplexIterator it=this->begin(d);it!=end(d);it++) { if(this->isTop(SimplexPointer(it.getPointer()))) l.push_back(SimplexPointer(it.getPointer())); }
				}
				
				/* If we arrive here, we can return the number of top simplices! */
				return l.size();
			}
			
			/// This member function identifies all simplices belonging to the boundary of a simplex directly encoded in the current data structure.
 			/**
 			 * This member function identifies all simplices belonging to the boundary of a simplex directly encoded in the current data structure.<p>The reference simplex and the boundary
 			 * simplices are required to be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector
 			 * mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function
 			 * only with a <i>global</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is
			 * compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary
			 * properties (i.e. an instance of the mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what properties are used, because usually they are
			 * removed before starting a new computation. In any case, we remove them at the end of this member function.
	 		 * \param cp the reference simplex directly encoded in the current data structure
	 		 * \param ll a list containing all simplices belonging to the boundary of a simplex directly encoded in the current data structure.
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase,
	 		 * mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isValid()
	 		 */
	 		inline void boundary(const SimplexPointer& cp,list<SimplexPointer>* ll) { static_cast<Derived*>(this)->boundary(SimplexPointer(cp),ll); }
	 		
	 		/// This member function identifies all simplices belonging to the boundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the boundary of a simplex not directly encoded in the current data structure.<p>The reference simplex and the
			 * boundary simplices can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can
			 * invoke this member function only with a <i>local</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the
	 		 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we
	 		 * could apply some temporary properties (i.e., an instance of the mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what properties are used
	 		 * because usually they are removed before starting a new computation. In any case, we remove them at the end of this member function.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
	 		 * \param ll a list containing all simplices belonging to the boundary of a simplex not directly encoded in the current data structure.
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase,
	 		 * mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isValid()
	 		 */
	 		inline void boundary(const GhostSimplexPointer& cp,list<GhostSimplexPointer>* ll) { static_cast<Derived*>(this)->boundary(GhostSimplexPointer(cp),ll); }

			/// This member function identifies all simplices of a certain dimension belonging to the boundary of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices of a certain dimension belonging to the boundary of a simplex directly encoded in the current data structure: this member function
	 		 * filters boundary simplices against their dimension.<p>The reference simplex and the boundary simplices are required to be <i>valid</i>, i.e. directly stored in the current data
	 		 * structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class
	 		 * and thus by their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>If we cannot complete this operation, then
	 		 * this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary properties (i.e. an instance of the mangrove_tds::PropertyBase class) for marking simplices.
			 * You should be careful about what properties are used because usually they are removed before starting a new computation. In any case, we remove them at the end of this member
	 		 * function.
 			 * \param cp the reference simplex directly encoded in the current data structure
			 * \param k the dimension of the required simplices belonging to the input simplex boundary
			 * \param ll a list of the required simplices belonging to the input simplex boundary
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase,
			 * mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isValid() 
			 */
	 		inline void boundaryk(const SimplexPointer& cp,SIMPLEX_TYPE k,list<SimplexPointer> *ll) { static_cast<Derived*>(this)->boundaryk(SimplexPointer(cp),k,ll); }

	 		/// This member function identifies all simplices of a certain dimension belonging to the boundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices of a certain dimension belonging to the boundary of a simplex not directly encoded in the current data structure: this member
			 * function filters boundary simplices against their dimension.<p>The reference simplex and the boundary simplices can also be not directly encoded in the current data structure
			 * and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>If we
			 * cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary properties (i.e. an instance of the
			 * mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what properties are used because usually they are removed before starting a new
			 * computation. In any case, we remove them at the end of this member function.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
			 * \param k the dimension of the required simplices belonging to the input simplex boundary
			 * \param ll a list of the required simplices belonging to the input simplex boundary
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase,
	 		 * mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isValid() 
			 */
	 		inline void boundaryk(const GhostSimplexPointer& cp,SIMPLEX_TYPE k,list<GhostSimplexPointer> *ll)
	 		{ static_cast<Derived*>(this)->boundaryk(GhostSimplexPointer(cp),k,ll); }
	 		
	 		/// This member function identifies all simplices belonging to the coboundary of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the coboundary of a simplex directly encoded in the current data structure.<p>The reference simplex and the star
	 		 * simplices are required to be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector
	 		 * mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function
	 		 * only with a <i>global</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is
			 * compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary
			 * properties (i.e. an instance of the mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what properties are used because usually they are
			 * removed before starting a new computation. In any case, we remove them at the end of this member function.
 			 * \param cp the reference simplex directly encoded in the current data structure
			 * \param ll a list of the required simplices belonging to the input simplex coboundary
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::GhostSimplexPointer,
			 * mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::isValid()
			 */
	 		inline void coboundary(const SimplexPointer& cp,list<SimplexPointer> *ll) { static_cast<Derived*>(this)->coboundary(SimplexPointer(cp),ll); }
	 		
	 		/// This member function identifies all simplices belonging to the coboundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the coboundary of a simplex not directly encoded in the current data structure.<p>The reference simplex and the star
	 		 * simplices can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke
			 * this member function only with a <i>local</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the
	 		 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we
	 		 * could apply some temporary properties (i.e. an instance of the mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what properties are used
	 		 * because usually they are removed before starting a new computation. In any case, we remove them at the end of this member function.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
	 		 * \param ll a list of the required simplices belonging to the input simplex coboundary
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::GhostSimplexPointer,
			 * mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::isValid()
			 */
	 		inline void coboundary(const GhostSimplexPointer& cp,list<GhostSimplexPointer> *ll) { static_cast<Derived*>(this)->coboundary(GhostSimplexPointer(cp),ll); }
	 		
	 		/// This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex directly encoded in the current data structure: in other words,
			 * this member function filters coboundary simplices against their dimension.<p>The reference simplex and the coboundary simplices are required to be <i>valid</i>, i.e. directly
			 * stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They are identified by instances of the
	 		 * mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>If we
	 		 * cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary properties (i.e. an instance of the
			 * mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what properties are used because usually they are removed before starting a new
			 * computation. In any case, we remove them at the end of this member function.
 			 * \param cp the reference simplex directly encoded in the current data structure
			 * \param k the dimension of the required simplices belonging to the input simplex coboundary
			 * \param ll a list of the required simplices belonging to the input simplex coboundary
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::GhostSimplexPointer,
			 * mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::isValid()
			 */
	 		inline void coboundaryk(const SimplexPointer& cp,SIMPLEX_TYPE k,list<SimplexPointer> *ll) { static_cast<Derived*>(this)->coboundaryk(SimplexPointer(cp),k,ll); }
	 		
	 		/// This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex not directly encoded in the current data structure: in other
			 * words, this member function filters coboundary simplices against their dimension.<p>The reference simplex and the coboundary simplices can also be not directly encoded in the
			 * current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member function only with a <i>local</i> data
			 * structure.<p>If we cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case,
			 * each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary properties (i.e. an instance of the
	 		 * mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what properties are used because usually they are removed before starting a new
	 		 * computation. In any case, we remove them at the end of this member function.
 			 * \param cp the reference simplex not directly encoded in the current data structure
			 * \param k the dimension of the required simplices belonging to the input simplex coboundary
			 * \param ll a list of the required simplices belonging to the input simplex coboundary
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::GhostSimplexPointer,
			 * mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::isValid()
			 */
	 		inline void coboundaryk(const GhostSimplexPointer& cp, SIMPLEX_TYPE k, list<GhostSimplexPointer> *ll) { static_cast<Derived*>(this)->coboundaryk(GhostSimplexPointer(cp),k,ll); }
	 		
	 		/// This member function identifies all simplices of a certain dimension belonging to the star of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices of a certain dimension belonging to the star of a simplex directly encoded in the current data structure: in other words, this
			 * member function returns all simplices incident in the reference simplex.<p>The reference simplex and the star simplices are required to be <i>valid</i>, i.e. directly stored
			 * in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They are identified by instances of the
			 * mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>If we
			 * cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary properties (i.e. an instance of the
			 * mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what properties are used because usually they are removed before starting a new
			 * computation. In any case, we remove them at the end of this member function.
 			 * \param cp the reference simplex directly encoded in the current data structure
			 * \param ll a list of the required simplices belonging to the input simplex star
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::GhostSimplexPointer,
			 * mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::isValid()
			 */
	 		inline void star(const SimplexPointer &cp, list<SimplexPointer> *ll) { this->coboundary(SimplexPointer(cp),ll); }
	 		
	 		/// This member function identifies all simplices belonging to the coboundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the star of a simplex not directly encoded in the current data structure: in other words, this member function
			 * returns all simplices incident in the reference simplex.<p>The reference simplex and the star simplices can also be not directly encoded in the current data structure and they
			 * are identified by
	 		 * the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>If we cannot complete this operation,
	 		 * then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary properties (i.e. an instance of the mangrove_tds::PropertyBase class) for marking simplices.
			 * You should be careful about what properties are used because usually they are removed before starting a new computation. In any case, we remove them at the end of this member
	 		 * function.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
	 		 * \param ll a list of the required simplices belonging to the input simplex star
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::GhostSimplexPointer,
			 * mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::isValid()
			 */
	 		inline void star(const GhostSimplexPointer &cp, list<GhostSimplexPointer> *ll) { this->coboundary(GhostSimplexPointer(cp),ll); }
	 		
	 		/// This member function identifies all simplices adjacent to a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices adjacent to a simplex directly encoded in the current data structure: two 0-simplices are <i>adjacent</i> if they are connected
			 * by a common edge, while two k-simplices (with k>0) are <i>adjacent</i> if they share a simplex of dimension k-1.<p>The reference simplex and the adjacent simplices are
			 * required to be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They
			 * are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function only with a
			 * <i>global</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug
			 * mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary properties (i.e. an
			 * instance of the mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what properties are used because usually they are removed before starting
			 * a new computation. In any case, we remove them at the end of this member function.
	 		 * \param cp the reference simplex directly encoded in the current data structure
			 * \param ll a list of the required simplices belonging to the input simplex adjacency
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::GhostSimplexPointer,
			 * mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::isValid()
	 		 */
	 		inline void adjacency(const SimplexPointer &cp,list<SimplexPointer> *ll) { static_cast<Derived*>(this)->adjacency(SimplexPointer(cp),ll); }
	 		
	 		/// This member function identifies all simplices adjacent to a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices adjacent to a simplex not directly encoded in the current data structure: two 0-simplices are <i>adjacent</i> if they are
			 * connected by a common edge, while two k-simplices (with k>0) are <i>adjacent</i> if they share a simplex of dimension k-1.<p>The reference simplex and the adjacent simplices
			 * can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member
			 * function only with a <i>local</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is
			 * compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary
			 * properties (i.e. an instance of the mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what properties are used because usually they are
			 * removed before starting a new computation. In any case, we remove them at the end of this member function.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
			 * \param ll a list of the required simplices belonging to the input simplex adjacency and not directly encoded in the current data structure
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::GhostSimplexPointer,
	 		 * mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::isValid()
	 		 */
	 		inline void adjacency(const GhostSimplexPointer &cp,list<GhostSimplexPointer> *ll) { static_cast<Derived*>(this)->adjacency(GhostSimplexPointer(cp),ll); }
	 		
	 		/// This member function identifies all simplices belonging to the link of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the link of a simplex directly encoded in the current data structure: let <i>c</i> a simplex in the current
			 * simplicial complex, then the <i>link</i> of <i>c</i> is the set of all the subsimplices of the simplices in the star of <i>c</i> that are not incident in <i>c</i>.<p>The
			 * reference simplex and the required simplices must be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by
			 * the garbage collector mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can
			 * invoke this member function only with a <i>global</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we
			 * could apply some temporary properties (i.e. an instance of the mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what properties are used
			 * because usually they are removed before starting a new computation. In any case, we remove them at the end of this member function.
	 		 * \param cp the reference simplex directly encoded in the current data structure
	 		 * \param ll a list of the required simplices belonging to the input simplex link
	 		 * \see  mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplexPointer, mangrove_tds::BaseSimplicialComplex::type(), mangrove_tds::BaseSimplicialComplex::isValid(),
	 		 * mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::PropertyBase
	 		 */
	 		inline void link(const SimplexPointer& cp,list<SimplexPointer> *ll) { static_cast<Derived*>(this)->link(SimplexPointer(cp),ll); }

	 		/// This member function identifies all simplices belonging to the link of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the link of a simplex not directly encoded in the current data structure: let <i>c</i> a simplex in the current
			 * simplicial complex, then the <i>link</i> of <i>c</i> is the set of all the subsimplices of the simplices in the star of <i>c</i> that are not incident in <i>c</i>.<p>The
			 * reference simplex and the required simplices can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer
			 * class. Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>If we cannot complete this operation, then this member function will fail if
			 * and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this
			 * member function, we could apply some temporary properties (i.e. an instance of the mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what
			 * properties are used because usually they are removed before starting a new computation. In any case, we remove them at the end of this member function.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
	 		 * \param ll a list of the required simplices belonging to the input simplex link and not directly encoded in the current data structure
	 		 * \see mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::SIMPLEX_TYPE,
	 		 * mangrove_tds::SIMPLEX_ID, mangrove_tds::GS_COMPLEX, mangrove_tds::PropertyBase
	 		 */
	 		inline void link(const GhostSimplexPointer& cp,list<GhostSimplexPointer> *ll) { static_cast<Derived*>(this)->link(GhostSimplexPointer(cp),ll); }
	 		
	 		/// This member function returns the number of components in the link of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function returns the number of components in the link of a simplex directly encoded in the current data structure: let <i>c</i> a simplex in the current simplicial
	 		 * complex, then the <i>link</i> of <i>c</i> is the set of all the subsimplices of the simplices in the star of <i>c</i> that are not incident in <i>c</i>.<p>The reference simplex
	 		 * and the required simplices must be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage
			 * collector mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this
			 * member function only with a <i>global</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function we
			 * use at least a global property (i.e. an instance of the mangrove_tds::GlobalPropertyHandle class) with boolean values, called <i>visited</i>, and a local property (i.e. an
			 * instance of the mangrove_tds::LocalPropertyHandle class) for vertices with boolean values, called <i>refVertex</i>. We could also add other properties in accordance with the
			 * specific data structure to be defined. We remove such properties, if they already belong to the current simplicial complex. In any case, we remove them at the end of this
			 * member function.
	 		 * \param cc the reference simplex directly encoded in the current data structure
	 		 * \param lk a list of the required simplices belonging to the input simplex link
	 		 * \return the number of components in the link of a simplex directly encoded in the current data structure
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplexPointer, mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::LocalPropertyHandle,
	 		 * mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isValid()
	 		 */
	 		inline unsigned int getLinkComponentsNumber(const SimplexPointer &cc,list<SimplexPointer> *lk)
	 		{ return static_cast<Derived*>(this)->getLinkComponentsNumber(SimplexPointer(cc),lk); }
			
			/// This member function returns the number of components in the link of a simplex not directly encoded in the current data structure.
			/**
			 * This member function returns the number of components in the link of a simplex not directly encoded in the current data structure: let <i>c</i> a simplex in the current 
			 * simplicial complex, then the <i>link</i> of <i>c</i> is the set of all the subsimplices of the simplices in the star of <i>c</i> that are not incident in <i>c</i>.<p>The
			 * reference simplex and the required simplices can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer
			 * class. Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>If we cannot complete this operation, then this member function will fail if
			 * and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \param cc the reference simplex not directly encoded in the current data structure
	 		 * \param lk a list of the required simplices belonging to the input simplex link
	 		 * \return the number of components in the link of a simplex not directly encoded in the current data structure
	 		 * \see mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::SIMPLEX_TYPE,
	 		 * mangrove_tds::SIMPLEX_ID, mangrove_tds::GS_COMPLEX, mangrove_tds::PropertyBase
			 */
			inline unsigned int getLinkComponentsNumber(const GhostSimplexPointer &cc,list<GhostSimplexPointer> *lk)
			{ return static_cast<Derived*>(this)->getLinkComponentsNumber(GhostSimplexPointer(cc),lk); }
			
			/// This member function checks if the shape is regular.
			/**
			 * @authors: Gianluca Alloisio, Nikolas De Giorgis
			 */
			inline bool isRegular(void)
			{
				/* We start from "d-1"-simplexes because the function
				 * could be less expensive if there are top simplexes of high dimension */
				for (SIMPLEX_TYPE d=this->type()-1 ; d>=0 ; d--)
				{
					SimplexIterator it;
					for(it=this->begin(d);it!=end(d);it++)
					{
						if(this->isTop(SimplexPointer(it.getPointer())))
						{
							return false;
						}
					}
					/* SIMPLEX_TYPE is unsigned int, so we must break or we'll get stuck in the for loop */
					if (d==0) break;
				}
				return true;
			}
			
			/// This member function checks if the shape is pseudo-manifold.
			/**
			 * @authors: Gianluca Alloisio, Nikolas De Giorgis
			 */
			inline bool isPseudoManifold()
			{
			    SIMPLEX_TYPE d=this->type()-1;
			    SimplexIterator it;
			    list<GhostSimplexPointer> l_gs;
			    GS_COMPLEX lgs;
			    GS_COMPLEX::iterator lgs_it;
			    list<SimplexPointer> ls;
			    if (!this->isKConnected(d))
			      return false;
			    if (this->encodeAllSimplices()==false)
			    {
			       this->retrieveAllSimplices(d,lgs);
			       for(lgs_it=lgs.begin();lgs_it!=lgs.end();lgs_it++)
				{
				      this->star(lgs_it->getCGhostSimplexPointer(),&l_gs);
				      if (l_gs.size()>3)
					return false;
				}
			    }
			    else
			    {
				for(it=this->begin(d);it!=this->end(d);it++)
				{
				    this->star(it.getPointer(),&ls);
				    if (ls.size()>3)
				      return false;
				}
			    }
			    return true;
			}
			
			/// This member function checks if the shape is k-connected.
			/**
			 * @authors: Gianluca Alloisio, Nikolas De Giorgis
			 */
			inline bool isKConnected(SIMPLEX_TYPE k)
			{
				assert(k<this->type());
				int v = 0; 
				SimplexPointer sp,tsp,ttsp;
				GhostSimplexPointer gsp,tgsp,ttgsp;
				list<GhostSimplexPointer> lg, vlg;
				list<SimplexPointer> l,vl;
				string prop = "ikc_v";
				/* We just retrieve the first k+1 simplex, then we start visiting the dual graph (implicity encoded) */
				if(this->encodeAllSimplices()==false)
				{
					if (this->findGhostProperty(prop)==NULL)
					{
						addGhostProperty(prop, false);
						
					}
					
					GS_COMPLEX gs;
					GS_COMPLEX::iterator gs_it;
					this->retrieveAllSimplices(k+1,gs);
					gsp=GhostSimplexPointer(gs.begin()->getCGhostSimplexPointer());
					
					vlg.push_back(gsp);
					this->setPropertyValue<bool>(prop, gsp, true);
					while (!vlg.empty())
					{
						tgsp = vlg.front();
						vlg.pop_front();
						v++;
						this->adjacency(tgsp,&lg);
						/* We visit all the k+1 simplexes adjacent that are not already visited */
						for (list<GhostSimplexPointer>::iterator it = lg.begin() ; it!=lg.end() ; it++)
						{
							bool nothing;
							
							ttgsp=*it;
							if (this->getPropertyValue<bool>(prop, ttgsp, nothing)==false)
							{
								this->setPropertyValue<bool>(prop, ttgsp, true);
								vlg.push_back(ttgsp);
							}
						}
					}
					this->deleteProperty(prop);
					/* If we have visited all the k+1 simplexes, than the graph has one
					 * connected component and the shape is k-connected */
					return (v==gs.size());
				} else
				{
					this->unmarkAllVisited();
					sp=(SimplexPointer(this->begin(k+1).getPointer()));
					vl.push_back(sp);
					this->visit(sp,true);
					while(!vl.empty())
					{
					  tsp=vl.front();
					  vl.pop_front();
					  v++;
					  this->adjacency(tsp,&l);
					  /* We visit all the k+1 simplexes adjacent that are not already visited */
					  for(list<SimplexPointer>::iterator it=l.begin();it!=l.end(); it++)
					  {
					    ttsp=*it;
					    if (!this->isVisited(ttsp))
					    {
					      vl.push_back(ttsp);
					      this->visit(ttsp,true);
					    }
					  }
					  
					}
					this->unmarkAllVisited();
					/* If we have visited all the k+1 simplexes, than the graph has one
					 * connected component and the shape is k-connected */
					return (v==this->size(k+1));
				}
			}
			
			/// This member function checks if the shape is manifold.
			/**
			 * @authors: Gianluca Alloisio, Nikolas De Giorgis
			 */
			inline bool isManifoldShape(void)
			{
				SIMPLEX_TYPE t;
				SimplexPointer cc;
				SimplexIterator it;
				GS_COMPLEX gs;
				GS_COMPLEX::iterator gs_it;
				
				t=min(SIMPLEX_TYPE(1),static_cast<Derived*>(this)->type());
				for(SIMPLEX_TYPE d=0;d<=t;d++)
				{
					/* First, we check if all is ok and then we check which type of data structure we are using! */
					if(this->encodeAllSimplices()==true)
					{
						/* We are using a global data structure! */
						for(it=static_cast<Derived*>(this)->begin(d);it!=static_cast<Derived*>(this)->end(d);it++)
						{
							cc=SimplexPointer(it.getPointer());
							if(static_cast<Derived*>(this)->isManifold(SimplexPointer(cc))==false) return false;
						}
					}
					else
					{
						/* We are using a local data structure */
						/* We must retrieve all the d-simplices (ghost) */
						this->retrieveAllSimplices(d,gs);
						for(gs_it=gs.begin();gs_it!=gs.end();gs_it++)
						{
							if(static_cast<Derived*>(this)->isManifold(GhostSimplexPointer(gs_it->getCGhostSimplexPointer()))==false) return false;
						}
					}
				}
				return true;
			}
			
			/// This member function checks if a simplex directly encoded in the current data structure is manifold.
			/**
			 * This member function checks if a simplex directly encoded in the current data structure is manifold: we say that a simplex is <i>manifold</i> if and only if its neighborhood is
			 * homeomorphic to a connected component of a sphere, otherwise it is called <i>non-manifold</i>.<p>The reference simplex must be <i>valid</i>, i.e. directly stored in the
			 * current data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. It is identified by instances of the
			 * mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>If we
			 * cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary properties (i.e. an instance of the
			 * mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what properties are used because usually they are removed before starting a new
			 * computation. In any case, we remove them at the end of this member function.
			 * \param cp the reference simplex directly encoded in the current data structure
			 * \return <ul><li><i>true</i>, if the required simplex is manifold</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplexPointer, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(),
			 * mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::isValid()
			 */
			inline bool isManifold(const SimplexPointer& cp) { return static_cast<Derived*>(this)->isManifold( SimplexPointer(cp)); }
			
			/// This member function checks if a simplex not directly encoded in the current data structure is manifold.
			/**
			 * This member function checks if a simplex not directly encoded in the current data structure is manifold: we say that a simplex is <i>manifold</i> if and only if its
			 * neighborhood is homeomorphic to a connected component of a sphere, otherwise it is called <i>non-manifold</i>.<p>The reference simplex and the required simplices can also be
			 * not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member function
			 * only with a <i>local</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled
			 * in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary properties
			 * (i.e. an instance of the mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what properties are used because usually they are removed before
			 * starting a new computation. In any  case, we remove them at the end of this member function.
			 * \param cp the reference simplex not directly encoded in the current data structure
			 * \return <ul><li><i>true</i>, if the required simplex is manifold</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GS_COMPLEX,
			 * mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::isValid()
			 */
			inline bool isManifold(const GhostSimplexPointer& cp) { return static_cast<Derived*>(this)->isManifold(GhostSimplexPointer(cp)); }
			
			/// This member function extracts all non-manifold simplices of a certain dimension directly encoded in the current data structure.
			/**
			 * This member function returns the number of all non-manifold simplices of a certain dimension directly encoded in the current data structure: a simplex is <i>manifold</i> if and
			 * only if its neighborhood is homeomorphic to a connected component of a sphere, otherwise it is called <i>non-manifold</i>.<p>The required simplices must be <i>valid</i>, i.e.
			 * directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They are identified by instances of the
			 * mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>If we
			 * cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary properties (i.e. an instance of the
			 * mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what properties are used because usually they are removed before starting a new
			 * computation. In any case, we remove them at the end of this member function.
			 * \param d the dimension of the required simplices
			 * \param sings all non-manifold simplices of a certain dimension directly encoded in the current data structure
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexIterator, mangrove_tds::SimplexPointer, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::begin(),
			 * mangrove_tds::BaseSimplicialComplex::end(), mangrove_tds::BaseSimplicialComplex::encodeAllSimplices()
			 */
			inline unsigned int getNonManifoldSimplices(SIMPLEX_TYPE d,list<SimplexPointer> &sings)
			{
				SimplexIterator it;
				SimplexPointer cc;
				
				/* First, we check if all is ok! */
				assert(d<=this->type());
				assert(this->encodeAllSimplices()==true);
				sings.clear();
				if(d<=1)
				{
					for(it=this->begin(d);it!=this->end(d);it++)
					{
						cc=SimplexPointer(it.getPointer());
						if(this->isManifold(SimplexPointer(cc))==false) sings.push_back(SimplexPointer(cc));
					}
				}
				
				/* If we arrive here, we can return the number of singularities! */
				return sings.size();
			}

 			/// This member function extracts all non-manifold simplices of a certain dimension not directly encoded in the current data structure.
			/**
			 * This member function returns the number of all non-manifold simplices of a certain dimension not directly encoded in the current data structure: a simplex is <i>manifold</i>
			 * if and only if its neighborhood is homeomorphic to a connected component of a sphere, otherwise it is called <i>non-manifold</i>.<p>The reference simplex and the required
			 * simplices can also be not directly encoded in the current data structure and they are identified by the complete this operation, then this member function will fail if and
			 * only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member
			 * function, we could apply some temporary properties (i.e. an instance of the mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what
			 * properties are used because usually they are removed before starting a new computation. In any case, we remove them at the end of this member function.			 
			 * \param d the dimension of the required simplices
			 * \param sings all non-manifold simplices of a certain dimension not directly encoded in the current data structure
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplex, mangrove_tds::GS_COMPLEX, mangrove_tds::GhostSimplexPointer, mangrove_tds::PropertyBase,
			 * mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::retrieveAllSimplices()
			 */
			inline unsigned int getNonManifoldSimplices(SIMPLEX_TYPE d,list<GhostSimplexPointer> &sings)
			{
				GS_COMPLEX gs;
				GhostSimplexPointer aux;
				GS_COMPLEX::iterator gs_it;

				/* First, we check if all is ok */
				assert(d<=this->type());
				assert(this->encodeAllSimplices()==false);
				sings.clear();
				if(d<=1)
				{
					/* We must retrieve all the d-simplices (ghost) */
						this->retrieveAllSimplices(d,gs);
						for(gs_it=gs.begin();gs_it!=gs.end();gs_it++) 
						{
							aux=GhostSimplexPointer(gs_it->getCGhostSimplexPointer());
							if(this->isManifold(GhostSimplexPointer(aux))==false) sings.push_back(GhostSimplexPointer(GhostSimplexPointer(aux)));
						}
				}
				
				/* If we arrive here, we can return the number of elements in 'sings' */
				return sings.size();
			}
			
			/// This member function extracts all non-manifold simplices directly encoded in the current data structure.
			/**
			 * This member function extracts all non-manifold simplices directly encoded in the current data structure: a simplex is <i>manifold</i> if and only if its neighborhood is
			 * homeomorphic to a connected component of a sphere, otherwise it is called <i>non-manifold</i>.<p>The required simplices must be <i>valid</i>, i.e. directly stored in the
			 * current data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They are identified by instances of the
			 * mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>If we
			 * cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary properties (i.e. an instance of the
			 * mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what properties are used because usually they are removed before starting a new
			 * computation. In any case, we remove them at the end of this member function.
			 * \param sings all non-manifold simplices directly encoded in the current data structure
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexIterator, mangrove_tds::SimplexPointer, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::begin(),
			 * mangrove_tds::BaseSimplicialComplex::end(), mangrove_tds::BaseSimplicialComplex::encodeAllSimplices()
			 */
			inline void getNonManifoldSimplices(list<SimplexPointer> &sings)
			{
				SIMPLEX_TYPE t;
				SimplexIterator it;
				SimplexPointer cc;
				
				/* First, we must check the maximum limit of simplices to be checked! */
				assert(this->encodeAllSimplices()==true);
				t=min(SIMPLEX_TYPE(1),this->type());
				sings.clear();
				for(SIMPLEX_TYPE d=0;d<=t;d++)
				{
					/* Now, we extract all the non-manifold singularities of dimension d */
					for(it=this->begin(d);it!=this->end(d);it++)
					{
						cc=SimplexPointer(it.getPointer());
						if(this->isManifold(SimplexPointer(cc))==false) sings.push_back(SimplexPointer(cc));
					}
				}
			}
			
			/// This member function extracts all non-manifold simplices in the current data structure.
			/**
			 * This member function extracts all non-manifold simplices in the current data structure: a simplex is <i>manifold</i> if and only if its neighborhood is homeomorphic to a
			 * connected component of a sphere, otherwise it is called <i>non-manifold</i>.<p>The reference simplex and the required simplices can also be not directly encoded in the current
			 * data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member function only with a <i>local</i> data
			 * structure.<p>If we cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case,
			 * each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary properties (i.e. an instance of the
			 * mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what properties are used because usually they are removed before starting a new
			 * computation. In any case, we remove them at the end of this member function.
			 * \param sings all non-manifold simplices in the current data structure
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplex, mangrove_tds::GS_COMPLEX, mangrove_tds::GhostSimplexPointer, mangrove_tds::PropertyBase,
			 * mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::retrieveAllSimplices()
			 */
			 inline void getNonManifoldSimplices(list<GhostSimplexPointer> &sings)
			 {
			 	SIMPLEX_TYPE t;
				GS_COMPLEX gs;
				GS_COMPLEX::iterator gs_it;
				GhostSimplexPointer aux;

			 	/* First, we must check the maximum limit of simplices to be checked! */
				assert(this->encodeAllSimplices()==false);
				t=min(SIMPLEX_TYPE(1),this->type());
				sings.clear();
				for(SIMPLEX_TYPE d=0;d<=t;d++)
				{
					/* Now, we extract all the non-manifold singularities of dimension d */
					gs.clear();
					this->retrieveAllSimplices(d,gs);
					for(gs_it=gs.begin();gs_it!=gs.end();gs_it++) 
					{
						aux=GhostSimplexPointer(gs_it->getCGhostSimplexPointer());
						if(this->isManifold(GhostSimplexPointer(aux))==false) sings.push_back(GhostSimplexPointer(GhostSimplexPointer(aux)));
					}
				}
			}
			
			/// This member function returns the number of all non-manifold simplices in the current data structure.
			/**
			 * This member function returns the number of all non-manifold simplices in the current data structure: a simplex is <i>manifold</i> if and only if its neighborhood is
			 * homeomorphic to a connected component of a sphere, otherwise it is called <i>non-manifold</i>.<p>If we cannot complete this operation, then this member function will fail if
			 * and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this
			 * member function, we could apply some temporary properties (i.e. an instance of the mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what
			 * properties are used because usually they are removed before starting a new computation. In any case, we remove them at the end of this member function.
			 * \return the number of all non-manifold simplices directly encoded in the current data structure
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase,
			 * mangrove_tds::BaseSimplicialComplex::encodeAllSimplices()
			 */
			inline unsigned int getNonManifoldSimplicesNumber()
			{
				unsigned int sings;
				SIMPLEX_TYPE t;
				
				/* Now, we look for all the non-manifold simplices! */
				sings=0;
				t=min(SIMPLEX_TYPE(1),this->type());
				for(SIMPLEX_TYPE d=0;d<=t;d++) { sings=sings+this->getNonManifoldSimplicesNumber(d); }
				return sings;
			}
			
			/// This member function returns the number of all non-manifold simplices of a certain dimension in the current data structure.
			/**
			 * This member function returns the number of all non-manifold simplices of a certain dimension in the current data structure: a simplex is <i>manifold</i> if and only if its
			 * neighborhood is homeomorphic to a connected component of a sphere, otherwise it is called <i>non-manifold</i>.<p>If we cannot complete this operation, then this member
			 * function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p>
			 * <b>IMPORTANT:</b> in this member function, we could apply some temporary properties (i.e. an instance of the mangrove_tds::PropertyBase class) for marking simplices. You
			 * should be careful about what properties are used because usually they are removed before starting a new computation. In any case, we remove them at the end of this member
			 * function.
			 * \param d the dimension of the required simplices
			 * \return the number of all non-manifold simplices of a certain dimension directly encoded in the current data structure
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexIterator, mangrove_tds::SimplexPointer, mangrove_tds::PropertyBase, mangrove_tds::GhostSimplex,
			 * mangrove_tds::GS_COMPLEX, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::GhostSimplexPointer,
			 * mangrove_tds::BaseSimplicialComplex::retrieveAllSimplices()
			 */
			inline unsigned int getNonManifoldSimplicesNumber(SIMPLEX_TYPE d)
			{
				unsigned int n;
				SimplexIterator it;
				SimplexPointer cc;
				GS_COMPLEX gs;
				GS_COMPLEX::iterator gs_it;
				
				/* First, we check if all is ok and then we check which type of data structure we are using! */
				assert(d<=this->type());
				if(this->encodeAllSimplices()==true)
				{
					/* We are using a global data structure! */
					n=0;
					if(d<=1)
					{
						for(it=this->begin(d);it!=this->end(d);it++)
						{
							cc=SimplexPointer(it.getPointer());
							if(this->isManifold(SimplexPointer(cc))==false) n=n+1;
						}
					}
				}
				else
				{
					/* We are using a local data structure */
					n=0;
					if(d<=1)
					{
						/* We must retrieve all the d-simplices (ghost) */
						this->retrieveAllSimplices(d,gs);
						for(gs_it=gs.begin();gs_it!=gs.end();gs_it++) { if(this->isManifold(GhostSimplexPointer(gs_it->getCGhostSimplexPointer()))==false) n=n+1; }
					}
				}
				
				/* If we arrive here, we can return the number of singularities! */
				return n;
			}
			
			/// This member function returns the number of non-manifold edges in the boundary of a simplex directly encoded in the current data structure.
			/**
			 * This member function returns the number of non-manifold edges in the boundary of a simplex directly encoded in the current data structure: an edge is <i>manifold</i> if and
			 * only if its neighborhood is homeomorphic to a connected component of a sphere, otherwise it is called <i>non-manifold</i>.<p>The reference simplex must be <i>valid</i>, i.e.
			 * directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. It is identified by an instance of the
			 * mangrove_tds::SimplexPointer class and thus by its type and its identifier. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>If we
			 * cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary properties (i.e. an instance of the
			 * mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what properties are used because usually they are removed before starting a new
			 * computation. In any case, we remove them at the end of this member function.
			 * \param cp a pointer to the reference simplex directly encoded in the current data structure
			 * \return the number of non-manifold edges in the boundary of a simplex directly encoded in the current data structure
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices()
			 */
			inline unsigned int getNMEdgesNumber(const SimplexPointer &cp)
 			{
 				list<SimplexPointer> l;
 				list<SimplexPointer>::iterator it;
 				unsigned int lg;
 				
 				/* First, we extract all the boundary edges for 'cp' */
 				assert(this->encodeAllSimplices()==true);
 				lg=0;
 				this->boundaryk(SimplexPointer(cp),1,&l);
 				for(it=l.begin();it!=l.end();it++) { if(this->isManifold(SimplexPointer(*it))==false) lg=lg+1; }
 				return lg;
 			}
 			
 			/// This member function returns the number of non-manifold edges in the boundary of a simplex not directly encoded in the current data structure.
 			/**
 			 * This member function returns the number of non-manifold edges in the boundary of a simplex not directly encoded in the current data structure: an edge is <i>manifold</i> if and
 			 * only if its neighborhood is homeomorphic to a connected component of a sphere, otherwise it is called <i>non-manifold</i>.<p>The reference simplex and the required simplices
 			 * can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member
 			 * function only with a <i>local</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is
 			 * compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary
 			 * properties (i.e. an instance of the mangrove_tds::PropertyBase class) for marking simplices. You should be careful about what properties are used because usually they are
	 		 * removed before starting a new computation. In any case, we remove them at the end of this member function.
			 * \param cp a pointer to the reference simplex not directly encoded in the current data structure
			 * \return the number of non-manifold edges in the boundary of a simplex directly encoded in the current data structure
 			 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices()
 			 */
 			inline unsigned int getNMEdgesNumber(const GhostSimplexPointer &cp)
 			{
 				list<GhostSimplexPointer> l;
 				list<GhostSimplexPointer>::iterator it;
				unsigned int lg;

 				/* First, we extract all the boundary edges for 'cp' */
 				assert(this->encodeAllSimplices()==false);
 				lg=0;
 				this->boundaryk(GhostSimplexPointer(cp),1,&l);
 				for(it=l.begin();it!=l.end();it++) { if(this->isManifold(GhostSimplexPointer(*it))==false) lg=lg+1; }
 				return lg;
 			}
 			
 			/// This member function returns the number of simplices different from wire edges incident in a 0-simplex.
 			/**
 			 * This member function returns the number of simplices different from wire edges incident in a 0-simplex (namely a vertex) directly encoded in the current data structure: for
			 * us, a <i>wire edge</i> is a top 1-simplex.<p>The reference simplex is required to be <i>valid</i>, i.e. a real location that has not been marked as <i>deleted</i> by the
			 * garbage collector mechanism. It is identified by an instance of the mangrove_tds::SimplexPointer class and thus by its type and by its identifier: if we cannot complete this
			 * operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation
			 * will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, the reference simplex and the required simplices are directly encoded in the specific data
			 * structure and the total coboundary relation is not computed.
 			 * \param cp a pointer to the reference 0-simplex directly encoded in the current data structure
 			 * \return the number of simplices different from wire edges incident in the input 0-simplex
 			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::COMPLEX, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(),
 			 * mangrove_tds::BaseSimplicialComplex::simplexc(), mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::BaseSimplicialComplex::isTop(),
 			 * mangrove_tds::BaseSimplicialComplex::isValid()
 			 */
 			inline unsigned int skipWireEdges(const SimplexPointer &cp)
			{
				unsigned int lg,c;
				SimplexPointer sk;
				
				/* First of all, we check if 'cp' is a vertex! */	
				assert(cp.type()==0);
				assert(this->isValid(cp));
				c=0;
				lg=this->simplexc(SimplexPointer(cp)).getCoboundarySize();
				for(unsigned int k=0;k<lg;k++)
				{
					sk=SimplexPointer(this->simplexc(SimplexPointer(cp)).cc(k));
					if(sk.type()!=1) c=c+1;
					if( (sk.type()==1) && (this->isTop(SimplexPointer(sk))==false)) c=c+1;
				}
				
				/* If we arrive here, we can return the result! */			
				return c;
			}
			
			/// This member function returns the number of simplices of a certain dimension incident in a simplex.
			/**
			 * This member function returns the number of simplices of a certain dimension incident in a simplex directly encoded in the current data structure.<p>The reference simplex is
			 * required to be <i>valid</i>, i.e. a real location that has not been marked as <i>deleted</i> by the garbage collector mechanism. It is identified by an instance of the
			 * mangrove_tds::SimplexPointer class and thus by its type and by its identifier: if we cannot complete this operation for any reason, then this member function will fail if and
			 * only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member
			 * function, the reference simplex and the required simplices are directly encoded in the specific data structure and the total coboundary relation is not computed.
			 * \param cp a pointer to the reference simplex directly encoded in the current data structure
			 * \param d the dimension of the required simplices
			 * \return the number of simplices of a certain dimension incident in the input simplex
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(),
			 * mangrove_tds::BaseSimplicialComplex::simplexc(), mangrove_tds::BaseSimplicialComplex::isTop(), mangrove_tds::BaseSimplicialComplex::isValid()
			 */
			unsigned int findIncidentSimplicesNumber(const SimplexPointer &cp,SIMPLEX_TYPE d)
			{
				unsigned int lg,c;
				SimplexPointer sk;
				
				/* First, we check if cp is valid and if it can have a d-component incident on it! */
				assert(this->isValid(cp));
				assert(d>cp.type());
				c=0;
				lg=this->simplexc(SimplexPointer(cp)).getCoboundarySize();
				for(unsigned int k=0;k<lg;k++)
				{
					sk=SimplexPointer(this->simplexc(SimplexPointer(cp)).cc(k));
					if(sk.type()==d) c=c+1;
				}
				
				/* If we arrive here, we can return the result! */
				return c;
			}
			
			/// This member function returns the number of simplices different from dangling faces incident in a 1-simplex.
			/**
			 * This member function returns the number of simplices different from dangling faces incident in a 1-simplex (namely an edge) directly encoded in the current data structure: for
			 * us, a <i>dangling face</i> is a top 2-simplex.<p>The reference simplex is required to be <i>valid</i>, i.e. a real location that has not been marked as <i>deleted</i> by the
			 * garbage collector mechanism. It is identified by an instance of the mangrove_tds::SimplexPointer class and thus by its type and by its identifier: if we cannot complete this
			 * operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation
			 * will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, the reference simplex and the required simplices are directly encoded in the specific data
			 * structure and the total coboundary relation is not computed.
			 * \param cp a pointer to the reference 1-simplex directly encoded in the current data structure
			 * \return the number of simplices different from dangling faces incident in the input 1-simplex
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::simplexc(),
			 * mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::isTop(), mangrove_tds::BaseSimplicialComplex::isValid()
			 */
			inline unsigned int skipDanglingFaces(const SimplexPointer &cp)
			{
				unsigned int lg,c;
				SimplexPointer sk;
				
				/* First of all, we check if 'cp' is an edge! */	
				assert(cp.type()==1);
				assert(this->isValid(cp));
				c=0;
				lg=this->simplexc(SimplexPointer(cp)).getCoboundarySize();
				for(unsigned int k=0;k<lg;k++)
				{
					sk=SimplexPointer(this->simplexc(SimplexPointer(cp)).cc(k));
					if(sk.type()!=2) c=c+1;
					if( (sk.type()==2) && (this->isTop(SimplexPointer(sk))==false)) c=c+1;
				}
				
				/* If we arrive here, we can return the result! */				
				return c;
			}

			/// This member function provides an analysis of the co-boundary relation directly encoded in the current data structure.
			/**
			 * This member function provides an analysis of the co-boundary relation directly encoded in the current data structure.<p><b>IMPORTANT:</b> the statistics are computed only on
			 * non-top simplices of the required dimension (i.e. simplices where their co-boundary relation is not empty).
			 * \param d the dimension of the required simplices
			 * \param min_lg the minimum number of simplices directly encoded in the required co-boundary relation
			 * \param max_lg the maximum number of simplices directly encoded in the required co-boundary relation
			 * \param avg_lg the average number of simplices directly encoded in the required co-boundary relation
			 * \param tot_lg the total number of simplices directly encoded in the required co-boundary relation
			 * \param os the output stream where we can write the distribution of the simplices directly encoded in the required co-boundary relation
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::SimplexIterator, mangrove_tds::BaseSimplicialComplex::isTop(),
			 * mangrove_tds::computeAverage()
			 */
			inline void analyzeCoboundaryRelation(SIMPLEX_TYPE d,unsigned int& min_lg,unsigned int& max_lg,double &avg_lg,unsigned int &tot_lg,ofstream &os=cout)
			{
				SimplexIterator it;
				unsigned int amin,amax,atot,nt;

				/* We should loop on d-simplices! */
				amin=UINT_MAX;
				amax=0;
				nt=0;
				atot=0;
				for(it=this->begin(d);it!=this->end(d);it++)
				{
					if(it->c().empty()==false)
					{
						os<<(it->c().size())<<endl;
						os.flush();
						amin=min(amin,(unsigned int)it->c().size());
						amax=max(amax,(unsigned int)it->c().size());
						atot=atot+it->c().size();
						nt=nt+1;
					}
				}

				/* If we arrive here, we finished! */
				if(nt==0)
				{
					min_lg=0;
					max_lg=0;
					avg_lg=0.0;
					tot_lg=0;
				}
				else
				{
					min_lg=amin;
					max_lg=amax;
					avg_lg=computeAverage(atot,nt);
					tot_lg=atot;
				}
			}

			/// This member function checks if a simplex directly encoded in the current data structure belongs to the shape boundary.
	 		/**
	 		 * This member function checks if a simplex directly encoded in the current data structure belongs to the shape boundary. In other words, we check if a simplex is useful for
			 * describing the external surface enclosing the input object to be represented through the current data structure.<p>The reference simplex is required to be <i>valid</i>, i.e.
			 * directly stored in the current data structure and related to a location not marked as <i>deleted</i> by the garbage collector mechanism. It is identified by instances of the
			 * mangrove_tds::SimplexPointer class and thus by its type and identifier. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>If we
	 		 * cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary properties (i.e. an instance of the
			 * mangrove_tds::PropertyBase class) for computing the input simplex star. You should be careful about what properties are used because usually they are removed before starting a
			 * new computation. In any case, we remove them at the end of this member function.
	 		 * \param cp a pointer to the reference simplex
	 		 * \return <ul><li><i>true</i>, if the input simplex belongs to the shape boundary</li><li><i>false</i>, otherwise</li></ul>
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::Simplex, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(),
	 		 * mangrove_tds::BaseSimplicialComplex::isTop()
	 		 */
	 		inline bool isBorderSimplex(const SimplexPointer& cp)
	 		{
	 			list<SimplexPointer> l;

	 			/* Now, we check if all is ok! */
	 			assert(this->encodeAllSimplices()==true);
	 			if(this->isTop(cp)) return true;
	 			else if(cp.type()!=this->type()-1) { return false; }
	 			else
	 			{
	 				/* If we arrive here, then 'cp.type()=this->type()-1' */
	 				if(this->simplex(cp).getCoboundarySize()>1) return false;
	 				else
	 				{
	 					this->coboundary(cp,&l);
	 					if(l.size()<=1) return true;
	 					else return false;
	 				}
	 			}
	 		}
	 		
	 		/// This member function checks if a simplex directly not encoded in the current data structure belongs to the shape boundary.
	 		/**
	 		 * This member function checks if a simplex not directly encoded in the current data structure belongs to the shape boundary. In other words, we check if a simplex is useful for
	 		 * describing the external surface enclosing the input object to be represented through the current data structure.<p>The reference simplex can also be not directly encoded in the
	 		 * current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member function only with a <i>local</i> data
	 		 * structure.<p>If we cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case,
			 * each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary properties (i.e. an instance of the
	 		 * mangrove_tds::PropertyBase class) for computing the input simplex star. You should be careful about what properties are used because usually they are removed before starting a
	 		 * new computation. In any case, we remove them at the end of this member function.
	 		 * \param cp a pointer to the reference ghost simplex
	 		 * \return <ul><li><i>true</i>, if the input ghost simplex belongs to the shape boundary</li><li><i>false</i>, otherwise</li></ul>
	 		 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplex, mangrove_tds::Simplex,
	 		 * mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::ghostSimplex()
	 		 */
	 		inline bool isBorderSimplex(const GhostSimplexPointer& cp)
	 		{
	 			unsigned int t,ct,ci;
				list<GhostSimplexPointer> l;

	 			/* First, we check if all is ok! */
	 			assert(this->encodeAllSimplices()==true);
	 			assert(this->isValid(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			assert(this->isTop(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			t=cp.getParentType();
				ct=cp.getChildType();
				ci=cp.getChildId();
				if(cp.isEncoded()==false)
				{
					/* We must have a valid child of a valid top simplex! */
					assert(ct<t);
					assert(ci<this->pascal_triangle[t+1][ct+1]);
					if(ct!=this->type()-1) { return false; }
					else
					{
						this->coboundary(cp,&l);
						if(l.size()<=1) return true;
						else return false;
					}
				}
				else { return true; }
	 		}
	 		
	 		/// This member function checks if any simplex is adjacent along an edge.
	 		/**
	 		 * This member function checks if any simplex is adjacent along an edge in the current simplicial complex. In other words, we check if the star of an edge is composed by only one 
	 		 * triangle.<p>The reference edge is required to be <i>valid</i>, i.e. directly stored in the current data structure and related to a location not marked as <i>deleted</i> by the
	 		 * garbage collector mechanism. It is identified by instances of the mangrove_tds::SimplexPointer class and thus by its type and identifier. Consequently, we can invoke this
			 * member function only with a <i>global</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation  will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we
			 * could apply some temporary properties (i.e. an instance of the mangrove_tds::PropertyBase class) for computing the input simplex star. You should be careful about what
			 * properties are used because usually they are removed before starting a new computation. In any case, we remove them at the end of this member function.
	 		 * \param cp a pointer to the reference edge
	 		 * \return <ul><li><i>true</i>, if the star of an edge is composed by only one triangle</li><li><i>false</i>, otherwise</ul>
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isTop()
	 		 */
	 		bool nothingAdjacentAlong(const SimplexPointer& cp)
	 		{
	 			list<SimplexPointer> cob;
	 			
	 			/* First, we check if all is ok! */
	 			assert(this->encodeAllSimplices()==true);
	 			assert(cp.type()==1);
	 			if(cp.type()!=1) return false;
	 			if(this->isTop(cp)) return false;
	 			this->star(SimplexPointer(cp),&cob);
	 			if(cob.size()!=1) return false;
	 			else return (cob.back().type()==2);
	 		}
	 		
	 		/// This member function checks if any simplex is adjacent along an edge.
	 		/**
	 		 * This member function checks if any simplex is adjacent along an edge in the current simplicial complex. In other words, we check if the star of an edge is composed by only one 
	 		 * triangle.<p><p>The reference simplex can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class.
	 		 * Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and
			 * only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member
	 		 * function, we could apply some temporary properties (i.e. an instance of the mangrove_tds::PropertyBase class) for computing the input simplex star. You should be careful about
	 		 * what properties are used because usually they are removed before starting a new computation. In any case, we remove them at the end of this member function.
	 		 * \param cp a pointer to the reference edge
	 		 * \return <ul><li><i>true</i>, if the star of an edge is composed by only one triangle</li><li><i>false</i>, otherwise</ul>
	 		 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::Simplex, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isTop()
	 		 */
	 		bool nothingAdjacentAlong(const GhostSimplexPointer& cp)
	 		{
	 			list<GhostSimplexPointer> cob;
	 			unsigned int t,ct,ci;
	 							
	 			/* First, we check if all is ok! */
	 			assert(this->encodeAllSimplices()==true);
	 			assert(this->isValid(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			assert(this->isTop(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			t=cp.getParentType();
				ct=cp.getChildType();
				ci=cp.getChildId();
				if(cp.isEncoded()==false)
				{
					/* We must have a valid child of a valid top simplex! */
					assert(ct<t);
					assert(ci<this->pascal_triangle[t+1][ct+1]);
					this->star(GhostSimplexPointer(cp),&cob);
					if(cob.size()!=1) return false;
					else { return (cob.back().getChildType()==2); }
				}
				else { return false; }
	 		}

	 		/// This member function identifies clusters in the simplicial complex represented by the current data structure.
			/**
			 * This member function looks for clusters in the simplicial complex represented by the current data structure: for us, a k-cluster is formed by k-simplices that are
			 * (k-1)-connected.<p>We assume all k-simplices were <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the
			 * garbage collector mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke
			 * this member function only with a <i>global</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in 	debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we
			 * could apply some temporary properties (i.e. an instance of the mangrove_tds::PropertyBase class) for marking simplices. In particular, we use the local property (i.e. an
			 * instance of the mangrove_tds::LocalPropertyHandle) with boolean values, called <i>assigned2cluster</i>, for marking d-simplices. You should be careful about what properties
			 * are used, because usually they are removed before starting a new computation. In any case, we remove them at the end of this member function.
			 * \param d the dimension of the required clusters
			 * \param top this boolean flag allows to ask clusters formed by only top simplices
			 * \param clusters simplices forming the required clusters
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::PropertyBase,
			 * mangrove_tds::LocalPropertyHandle, mangrove_tds::BaseSimplicialComplex::addLocalProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty()
			 */
			inline void getClusters(SIMPLEX_TYPE d,bool top,vector< vector<SimplexPointer> >& clusters)
			{
				LocalPropertyHandle<bool> *a2c;
				
				/* First, we check if all is ok and then proceed! */
				assert(d<=this->type());
				assert(this->encodeAllSimplices()==true);
				destroy(clusters);
				if(this->hasProperty(string("assigned2cluster"))) this->deleteProperty(string("assigned2cluster"));
				a2c=this->addLocalProperty(d,string("assigned2cluster"),false);
				if(top) this->findTopClusters(d,clusters,a2c);
				else this->findNonTopClusters(d,clusters,a2c);
				this->deleteProperty(string("assigned2cluster"));
			}
			
			/// This member function identifies clusters in the simplicial complex represented by the current data structure.
			/**
			 * This member function looks for clusters in the simplicial complex represented by the current data structure: for us, a k-cluster is formed by k-simplices that are
			 * (k-1)-connected.<p>We assume all clusters are formed by <i>ghost</i> simplices, i.e. not directly stored in the current data structure. They are identified by instances of the 
			 * mangrove_tds::GhostSimplexPointer class and thus we can invoke this member function only with a <i>local</i> data structure.<p>If we cannot complete this operation, then this
			 * member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p>
			 * <b>IMPORTANT:</b> in this member function, we could apply some temporary properties (i.e. an instance of the mangrove_tds::PropertyBase class) for marking simplices. In
			 * particular, we use the ghost property (i.e. an instance of the mangrove_tds::GhostPropertyHandle) with boolean values, called <i>assigned2cluster</i>, for marking d-simplices.
			 * You should be careful about what properties are used, because usually they are removed before starting a new computation. In any case, we remove them at the end of this member
			 * function.
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::PropertyBase,
			 * mangrove_tds::GhostPropertyHandle, mangrove_tds::BaseSimplicialComplex::addGhostProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty(),
			 * mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer
			 */
			inline void getClusters(SIMPLEX_TYPE d,bool top,vector< vector<GhostSimplexPointer> >& clusters)
			{
				GhostPropertyHandle<bool> *a2c;
				
				/* First, we check if all is ok and then proceed! */
				assert(d<=this->type());
				assert(this->encodeAllSimplices()==false);
				destroy(clusters);
				if(this->hasProperty(string("assigned2cluster"))) this->deleteProperty(string("assigned2cluster"));
				a2c=this->addGhostProperty(string("assigned2cluster"),false);
				if(top) this->findTopClusters(d,clusters,a2c);
				else this->findNonTopClusters(d,clusters,a2c);
				this->deleteProperty(string("assigned2cluster"));
			}

			/// This member function returns a reference to the name of the local property associated to all the vertices for storing Euclidean coordinates.
			/**
			 * This member function returns a reference to the name of the local property associated to all the vertices for storing Euclidean coordinates. Euclidean coordinates (if
			 * supported) are stored as a local property (described by the mangrove_tds::LocalPropertyHandle class) and they are associated to each vertex in the current data structure. For
			 * their encoding, we use instances of the mangrove_tds::Point class and <i>double floating</i> values.
			 * \return a reference to the name of the local property associated to all the vertices for storing Euclidean coordinates
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::Point, mangrove_tds::Simplex, mangrove_tds::GhostSimplex,
			 * mangrove_tds::BaseSimplicialComplex::supportsEuclideanCoordinates()
			 */
			inline string& getPropertyName4Coordinates() { return this->prop4coords; }
			
			/// This member function checks if the current data structure supports the extraction of Euclidean coordinates for each vertex.
			/**
			 * This member function checks if the current data structure supports the extraction of Euclidean coordinates for each vertex.<p>Euclidean coordinates (if supported) are stored
			 * as a local property (described by the mangrove_tds::LocalPropertyHandle class) and they are associated to each vertex in the current data structure. For their encoding, we use
			 * instances of the mangrove_tds::Point class and <i>double floating</i> values. The name of the involved property is returned by the
			 * mangrove_tds::BaseSimplicialComplex::getPropertyName4Coordinates() member function.
			 * \return <ul><li><i>true</i>, if the current data structure supports the extraction of Euclidean coordinates</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::Point, mangrove_tds::BaseSimplicialComplex::findLocalProperty(),
			 * mangrove_tds::BaseSimplicialComplex::getPropertyName4Coordinates()
			 */
			inline bool supportsEuclideanCoordinates()
			{
				/* First, we check if all is ok! */
				if(this->prop4coords.empty()) return false;
				else return (this->findLocalProperty(0,string(this->prop4coords))!=NULL);
			}
			
			/// This member function returns a reference to the name of the local property associated to all the vertices for storing field values.
			/**
			 * This member function returns a reference to the name of the local property associated to all the vertices for storing field values. Field values (if supported) are stored as a
			 * local property (described by the mangrove_tds::LocalPropertyHandle class) and they are associated to each vertex in the current data structure. For their encoding, we use
			 * <i>double floating</i> values.
			 * \return a reference to the name of the local property associated to all the vertices for storing field values
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::BaseSimplicialComplex::findLocalProperty(),
			 * mangrove_tds::BaseSimplicialComplex::supportsFieldValues()
			 */
			inline string& getPropertyName4FieldValue() { return this->prop4fvalue; }
			
			/// This member function checks if the current data structure supports the extraction of field values for each vertex.
			/**
			 * This member function checks if the current data structure supports the extraction of field values for each vertex.<p> Field values (if supported) are stored as a local property
			 * (described by the mangrove_tds::LocalPropertyHandle class) and they are associated to each vertex in the current data structure. For their encoding, we use
			 * <i>double floating</i> values. The name of the involved property is returned by the mangrove_tds::BaseSimplicialComplex::getPropertyName4FieldValue() member function.
			 * \return <ul><li><i>true</i>, if the current simplicial complex supports the extraction of field values</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::BaseSimplicialComplex::findLocalProperty(),
			 * mangrove_tds::BaseSimplicialComplex::getPropertyName4FieldValue()
			 */
			inline bool supportsFieldValues()
			{
				/* First, we check if all is ok! */
				if(this->prop4fvalue.empty()) return false;
				else return (this->findLocalProperty(0,string(this->prop4fvalue))!=NULL);
			}
			
			/// This member function creates a new local property in the current data structure.
			/**
			 * This member function creates a new local property in the current data structure: we say that a property (i.e. auxiliary information like Euclidean coordinates and field
			 * values) is <i>local</i> if and only if it is binded to a particular class of simplices (identified by their dimension) directly encoded in the current data structure. Each
			 * property is identified by its unique name, while the input templated value is mandatory for identifying its type.<p>If we cannot create the required property for any reason,
			 * then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.
	 		 * \param i the dimension of the simplices related to the new property to be created
	 		 * \param name the unique name to be used for the new property to be created
	 		 * \param orig the default template value for the property to be created
	 		 * \return a component for managing the new local property
	 		 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::BaseSimplicialComplex::findProperty(),
	 		 * mangrove_tds::BaseSimplicialComplex::deleteProperty(), mangrove_tds::BaseSimplicialComplex::removeAllProperties(),
	 		 * mangrove_tds::BaseSimplicialComplex::getLocalPropertiesNumber(), mangrove_tds::BaseSimplicialComplex::debugLocalProperties()
			 */
			template <class T> inline LocalPropertyHandle<T> * addLocalProperty(SIMPLEX_TYPE i,string name,T orig)
			{
				PropertyBase* p;
				LocalPropertyHandle<T> *aux;

				/* First, we look for a property with 'name' and then we can proceed! */
				assert(i<=this->type());
				p = NULL;
				p = this->findProperty(name);
				assert(p==NULL);
				aux=NULL;
				try { aux = new LocalPropertyHandle<T>(i,name,orig); }
				catch(bad_alloc ba) { aux=NULL; }
				assert(aux!=NULL);
				this->local_properties[i].push_back(aux);
				this->local_properties[i].back()->resize(this->simplices[i].size());
				return aux;
			}
			
			/// This member function creates a new global property in the current data structure.
			/**
			 * This member function creates a new global property in the current data structure: we say that a property (i.e. auxiliary information like Euclidean coordinates and field
			 * values) is <i>global</i> if and only if it is binded to all the simplices in the current data structure. Each property is identified by its unique name, while the input
			 * templated value is mandatory for identifying its type.<p>If we cannot create the required property for any reason, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in  debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \param name the unique name to be used for the new property to be created
	 		 * \param orig the default template value for the property to be created
	 		 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::BaseSimplicialComplex::findProperty(),
	 		 * mangrove_tds::BaseSimplicialComplex::deleteProperty(), mangrove_tds::BaseSimplicialComplex::removeAllProperties(),
	 		 * mangrove_tds::BaseSimplicialComplex::getGlobalPropertiesNumber(), mangrove_tds::BaseSimplicialComplex::debugGlobalProperties()
			 */
			template <class T> inline GlobalPropertyHandle<T> * addGlobalProperty(string name,T orig)
			{
				PropertyBase *p;
				SIMPLEX_TYPE t;
				GlobalPropertyHandle<T> *aux;

				/* First, we look for a property named 'name' and then we can proceed */
				t = this->type();
				p = NULL;
				p = this->findProperty(name);
				assert(p==NULL);
				aux=NULL;
				try { aux = new GlobalPropertyHandle<T>(t,name,orig); }
				catch(bad_alloc ba) { aux=NULL; }
				assert(aux!=NULL);
				this->global_properties.push_back(aux);
				for(SIMPLEX_TYPE i=0;i<=t;i++) { this->global_properties.back()->resize(i,this->simplices[i].size()); }
				return aux;
			}

			/// This member function creates a new sparse property in the current data structure.
			/**
			 * This member function creates a new sparse property in the current data structure: we say that a property (i.e. auxiliary information like Euclidean coordinates and field
			 * values) is <i>sparse</i> if and only if it is associated to a sparse set of simplices directly encoded in the current data structure without particular rules. Each property is
			 * identified by its unique name, while the input templated value is mandatory for identifying its type.<p>If we cannot create the required property for any reason, then this
			 * member function will fail if  and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param name the unique name to be used for the new property to be created
	 		 * \param orig the default template value for the property to be created
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::PropertyBase, mangrove_tds::SparsePropertyHandle, mangrove_tds::BaseSimplicialComplex::findProperty(),
			 * mangrove_tds::BaseSimplicialComplex::deleteProperty(), mangrove_tds::BaseSimplicialComplex::removeAllProperties(),
			 * mangrove_tds::BaseSimplicialComplex::getSparsePropertiesNumber(), mangrove_tds::BaseSimplicialComplex::debugSparseProperties()
			 */
			template <class T> inline SparsePropertyHandle<T> *addSparseProperty(string name,T orig)
			{
				PropertyBase *p;
				SIMPLEX_TYPE t;
				SparsePropertyHandle<T> *aux;
				
				/* First, we look for a property named 'name' and then we can proceed */
				t = this->type();
				p = NULL;
				p = this->findProperty(name);
				assert(p==NULL);
				aux=NULL;
				try { aux = new SparsePropertyHandle<T>(t,name,orig); }
				catch(bad_alloc ba) { aux=NULL; }
				assert(aux!=NULL);
				this->sparse_properties.push_back(aux);
				return aux;
			}
			
			/// This member function creates a new ghost property in the current data structure.
			/**
			 * This member function creates a new ghost property in the current data structure: we say that a property (i.e. auxiliary information like Euclidean coordinates and field
			 * values) is <i>ghost</i> if and only if it is binded to some simplices not directly encoded in the current data structure, assumed to be <i>local</i>. Such properties can be
			 * added only with a <i>local</i> data structure. Each property is identified by its unique name, while the input templated value is mandatory for identifying its type.<p>If we
			 * cannot create the required property for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case,
			 * each forbidden operation will result in a failed assert.
	 		 * \param name the unique name to be used for the new property to be created
			 * \param orig the default template value for the property to be created
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::PropertyBase, mangrove_tds::GhostPropertyHandle, mangrove_tds::BaseSimplicialComplex::findProperty(),
	 		 * mangrove_tds::BaseSimplicialComplex::deleteProperty(), mangrove_tds::BaseSimplicialComplex::removeAllProperties(),
	 		 * mangrove_tds::BaseSimplicialComplex::getGhostPropertiesNumber(), mangrove_tds::BaseSimplicialComplex::debugGhostProperties()
			 */
			template <class T> inline GhostPropertyHandle<T> *addGhostProperty(string name,T orig)
			{
				PropertyBase *p;
				SIMPLEX_TYPE t;
				GhostPropertyHandle<T> *aux;
				
				/* First, we look for a property named 'name' and then we can proceed */
				t = this->type();
				assert(this->encodeAllSimplices()==false);
				if(this->encodeAllSimplices()==true) return NULL;
				p = NULL;
				p = this->findProperty(name);
				assert(p==NULL);
				aux=NULL;
				try { aux = new GhostPropertyHandle<T>(t,name,orig); }
				catch(bad_alloc ba) { aux=NULL; }
				assert(aux!=NULL);
				this->ghost_properties.push_back(aux);
				return aux;
			}

			/// This member function checks if a property has been associated to the current data structure.
			/**
			 * This member function checks if a property, identified by its unique name, has been associated to the current data structure. A property (i.e. a subclass of the
			 * mangrove_tds::PropertyBase class) consists of auxiliary information (like Euclidean cooordinates and the field values) associated to a simplex in the current data structure
			 * and it is identified by its unique name.
			 * \param name the unique name of the property to be checked
			 * \return <ul><li><i>true</i>, if the required property has been associated to a simplex</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle,
			 * mangrove_tds::BaseSimplicialComplex::findProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty(), mangrove_tds::BaseSimplicialComplex::removeAllProperties()
			 */
			inline bool hasProperty(string name) { return (this->findProperty(name)!=NULL); }
			
			/// This member function returns a reference to the value of a property associated to a simplex directly encoded in the current data structure.
			/**
			 * This member function returns a reference to the value of a property associated to a simplex directly encoded in the current data structure. A property (i.e. a subclass of the
			 * mangrove_tds::PropertyBase class) consists of auxiliary information (like Euclidean cooordinates and the field values) associated to each simplex directly encoded in the
			 * current data structure.<p>In this member function, we do not know which type of property we require. Each property is identified by its unique name, while the input templated
			 * value is mandatory for identifying its type. The input simplex must be directly encoded in the current data structure and it is required to be <i>valid</i>, i.e. a real
			 * location that has not been marked as <i>deleted</i> by the garbage collector mechanism. Thus, we cannot access a <i>ghost</i> property through this member function, because
			 * such property requires a ghost simplex, i.e. not directly encoded in the current data structure.<p>If we cannot complete this operation for any reason, then this member
			 * function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param name the unique name of the property to be analyzed
			 * \param cp a pointer to the simplex to be analyzed
			 * \param def a constant templated value for identifying the type of the required property (not used or modified in this member function)
			 * \return a reference to the value of a property associated to a simplex directly encoded in the current data structure
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle,
			 * mangrove_tds::GhostPropertyHandle, mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::deleteProperty(),
			 * mangrove_tds::BaseSimplicialComplex::removeAllProperties(), mangrove_tds::BaseSimplicialComplex::isValid()
			 */
			template <class T> inline T& getPropertyValue(string name, const SimplexPointer& cp,T& def)
			{
				PropertyBase *p;
				GlobalPropertyHandle<T> *glob_aux;
				LocalPropertyHandle<T> *loc_aux;
				SparsePropertyHandle<T> *sparse_aux;
				
				/* First, we must check if the input parameters are valid */
				idle(def);
				assert( this->isValid(cp));
				p = NULL;
				p = this->findProperty(name);
				assert(p!=NULL);
				assert(p->isGhost()==false);
				if(p->isLocal())
				{
					/* We must get a value from a local property */
					assert(p->getDimension()==cp.type());
					loc_aux = NULL;
					loc_aux = dynamic_cast< LocalPropertyHandle<T> *>(p);
					assert(loc_aux!=NULL);
					return loc_aux->get(SimplexPointer(cp));
				}
				else if(p->isSparse())
				{
					/* We must get a value from a sparse property! */
					sparse_aux = NULL;
					sparse_aux = dynamic_cast<SparsePropertyHandle<T>*>(p);
					assert(sparse_aux!=NULL);
					return sparse_aux->get(SimplexPointer(cp));
				}
				else
				{
					/* We must get a value from a global property */
					glob_aux = NULL;
					glob_aux = dynamic_cast< GlobalPropertyHandle<T> *>(p);
					assert(glob_aux!=NULL);
					return glob_aux->get(SimplexPointer(cp));
				}
			}

			/// This member function updates the value of a property associated to a simplex directly encoded in the current data structure.
			/**
			 * This member function updates the value of a property associated to a simplex directly encoded in the current data structure. A property (i.e. a subclass of the
			 * mangrove_tds::PropertyBase class) consists of auxiliary information (like Euclidean cooordinates and the field values) associated to each simplex directly encoded in the
			 * current data structure.<p>In this member function, we do not know which type of property we require. Each property is identified by its unique name, while the input templated
			 * value is mandatory for identifying its type. The input simplex must be directly encoded in the current data structure and it is required to be <i>valid</i>, i.e. a real
			 * location that has not been marked as <i>deleted</i> by the garbage collector mechanism. Thus, we cannot access a <i>ghost</i> property through this member function, because
			 * such property requires a ghost simplex, i.e. not directly encoded in the current data structure.<p>If we cannot complete this operation for any reason, then this member
			 * function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param name the unique name of the property to be analyzed
			 * \param cp a pointer to the simplex to be analyzed
			 * \param value the new value to be associated with the input simplex
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle,
			 * mangrove_tds::GhostPropertyHandle, mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::deleteProperty(),
			 * mangrove_tds::BaseSimplicialComplex::removeAllProperties(), mangrove_tds::BaseSimplicialComplex::isValid()
			 */
			template <class T> inline void setPropertyValue(string name, const SimplexPointer& cp, T value)
			{
				PropertyBase *p;
				GlobalPropertyHandle<T> *glob_aux;
				LocalPropertyHandle<T> *loc_aux;
				SparsePropertyHandle<T> *sparse_aux;
				
				/* First, we must check if the input parameters are valid */
				assert( this->isValid(cp));
				p = NULL;
				p = this->findProperty(name);
				assert(p!=NULL);
				assert(p->isGhost()==false);
				if(p->isLocal())
				{
					/* We must get a value from a local property */
					assert(p->getDimension()==cp.type());
					loc_aux=NULL;
					loc_aux=dynamic_cast< LocalPropertyHandle<T> *>(p);
					assert(loc_aux!=NULL);
					loc_aux->set(SimplexPointer(cp), value);
				}
				else if(p->isSparse())
				{
					/* We must update a value of a sparse property! */
					sparse_aux=NULL;
					sparse_aux=dynamic_cast< SparsePropertyHandle<T> *>(p);
					assert(sparse_aux!=NULL);
					sparse_aux->set( SimplexPointer(cp),value);
				}
				else
				{
					/* We must update a value of a global property */
					glob_aux=NULL;
					glob_aux=dynamic_cast< GlobalPropertyHandle<T> *>(p);
					assert(glob_aux!=NULL);
					glob_aux->set( SimplexPointer(cp), value);
				}
			}

			/// This member function returns a reference to the value of a property associated a ghost simplex in the current data structure.
			/**
			 * This member function returns a reference to the value of a property associated a ghost simplex in the current data structure, i.e. the value of a <i>ghost</i> property,
			 * described by the mangrove_tds::GhostPropertyHandle class. It consists of auxiliary information (like Euclidean cooordinates and the field values) associated to a ghost
			 * simplex, i.e. a simplex not directly encoded in the current data structure.  Each property is identified by its unique name, while the input templated value is mandatory for
			 * identifying its type. Thus, we can apply this member function only with <i>local</i> data structures, since they encode only a subset of simplices (e.g. only the top
			 * ones).<p>If we cannot complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In
			 * this case, each forbidden operation will result in a failed assert.
			 * \param name the unique name of the property to be analyzed
			 * \param cp a pointer to the ghost simplex to be analyzed
			 * \param def a constant templated value for identifying the type of the required property (not used or modified in this member function)
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle,
			 * mangrove_tds::GhostPropertyHandle, mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::deleteProperty(),
			 * mangrove_tds::BaseSimplicialComplex::removeAllProperties(), mangrove_tds::BaseSimplicialComplex::isValid()
			 */
			template <class T> inline T& getPropertyValue(string name, const GhostSimplexPointer& cp, T& def)
			{
				PropertyBase *p;
				GhostPropertyHandle<T> *ghost_aux;
				GhostSimplex s;
				
				/* First, we must check if the input parameters are valid */
				idle(def);
				s.clear();
				this->ghostSimplex(GhostSimplexPointer(cp),&s);
				p = NULL;
				p = this->findProperty(name);
				assert(p!=NULL);
				assert(p->isGhost()==true);
				ghost_aux=dynamic_cast< GhostPropertyHandle<T> * >(p);
				assert(ghost_aux!=NULL);
				return ghost_aux->get(GhostSimplex(s));
			}
			
			/// This member function updates the value of a property associated to a ghost simplex in the current data structure.
			/**
			 * This member function updates the value of a property associated to a ghost simplex in the current data structure, i.e. the value of a <i>ghost</i> property, described by the
			 * mangrove_tds::GhostPropertyHandle class. It consists of auxiliary information (like Euclidean cooordinates and the field values) associated to a ghost simplex, i.e. a simplex
			 * not directly encoded in the current data structure. Each property is identified by its unique name, while the input templated value is mandatory for identifying its type.
			 * Thus, we can apply this member function only with <i>local</i> data structures, since they encode only a subset of simplices (e.g. only the top ones).<p>If we cannot complete
			 * this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.
			 * \param name the unique name of the property to be analyzed
			 * \param cp a pointer to the simplex to be analyzed
			 * \param value the new value to be associated with the input simplex
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle,
			 * mangrove_tds::GhostPropertyHandle, mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::deleteProperty(),
			 * mangrove_tds::BaseSimplicialComplex::removeAllProperties(), mangrove_tds::BaseSimplicialComplex::isValid()
			 */
			template <class T> inline void setPropertyValue(string name, const GhostSimplexPointer& cp, T value)
			{
				PropertyBase *p;
				GhostPropertyHandle<T> *ghost_aux;
				GhostSimplex s;
				
				/* First, we must check if the input parameters are valid */
				s.clear();
				this->ghostSimplex(GhostSimplexPointer(cp),&s);
				p = NULL;
				p = this->findProperty(name);
				assert(p!=NULL);
				assert(p->isGhost()==true);
				ghost_aux=dynamic_cast< GhostPropertyHandle<T> * >(p);
				assert(ghost_aux!=NULL);
				ghost_aux->set(GhostSimplex(s),value);
			}

			/// This member function removes all properties associated to the current data structure.
			/**
			 * This member function removes all properties associated to the current data structure. A property (i.e. a subclass of the mangrove_tds::PropertyBase class) consists of auxiliary
			 * information (like Euclidean cooordinates and the field values) associated to a simplex in the current data structure.<p>If we cannot complete this operation for any reason,
			 * then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle,
			 * mangrove_tds::GhostPropertyHandle, mangrove_tds::BaseSimplicialComplex::findProperty(), mangrove_tds::BaseSimplicialComplex::addLocalProperty(),
			 * mangrove_tds::BaseSimplicialComplex::addGlobalProperty(), mangrove_tds::BaseSimplicialComplex::addSparseProperty(),
			 * mangrove_tds::BaseSimplicialComplex::addGhostProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty()
			 */
			inline void removeAllProperties()
			{
				SIMPLEX_TYPE t;
				PropertyBase *aux;

				/* The current simplicial complex has been initialized: we can start to destroy the global properties */
				t = this->type();
				for(unsigned int k=0;k<this->global_properties.size();k++)
				{
					aux = this->global_properties[k];
					delete aux;
					this->global_properties[k] = NULL;
				}
				
				/* Now, we can destroy the local properties */
				for(unsigned int k=0;k<this->local_properties.size();k++)
				{
					/* We clear all the local properties for the k-simplices */
					for(unsigned int j=0;j<this->local_properties[k].size();j++) 
					{
						aux = this->local_properties[k].at(j);
						delete aux;
						(this->local_properties[k])[j] = NULL;
					}
						
					/* We clear the set of local properties for the k-simplices */
					this->local_properties[k].clear();
				}
				
				/* Now, we remove the sparse properties! */
				for(unsigned int k=0;k<this->sparse_properties.size();k++)
				{
					aux = this->sparse_properties[k];
					delete aux;
					this->sparse_properties[k] = NULL;
				}
				
				/* Now, we remove the ghost properties! */
				for(unsigned int k=0;k<this->ghost_properties.size();k++)
				{
					aux = this->ghost_properties[k];
					delete aux;
					this->ghost_properties[k] = NULL;
				}

				/* Now, we can destroy the vectors for properties */
				this->sparse_properties.clear();
				this->local_properties.clear();
				this->global_properties.clear();
				this->ghost_properties.clear();
				this->prop4fvalue.clear();
				this->prop4coords.clear();
			}
			
			/// This member function removes a property associated to the current data structure.
			/**
			 * This member function removes a property associated to the current data structure. A property (i.e. a subclass of the mangrove_tds::PropertyBase class) consists of auxiliary
			 * information (like Euclidean cooordinates and the field values) associated to a simplex in the current data structure and it is identified by its unique name.<p>If we cannot
			 * complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each
			 * forbidden operation will result in a failed assert.
			 * \param name the name of the property to be removed
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, 
			 * mangrove_tds::GhostPropertyHandle, mangrove_tds::BaseSimplicialComplex::findProperty(), mangrove_tds::BaseSimplicialComplex::addLocalProperty(),
			 * mangrove_tds::BaseSimplicialComplex::addGlobalProperty(), mangrove_tds::BaseSimplicialComplex::addSparseProperty(),
			 * mangrove_tds::BaseSimplicialComplex::addGhostProperty(), mangrove_tds::BaseSimplicialComplex::removeAllProperties()
			 */
			inline void deleteProperty(string name)
			{
				SIMPLEX_TYPE t,i;
				PropertyBase *p;
				vector<PropertyBase *>::iterator it;

				/* First, we look for the victim property and then we can proceed */
				t = this->type();
				p = NULL;
				p = this->findProperty(name);
				assert(p!=NULL);
				if(p->isLocal()) 
				{
					/* The victim property is local, thus we can remove it from 'local_properties' */
					i = p->getDimension();
					it = find(this->local_properties[i].begin(),this->local_properties[i].end(),p);
					assert(it!=this->local_properties[i].end());
					this->local_properties[i].erase(it);
					if(name.compare(this->prop4coords)==0) { this->prop4coords.clear(); }
					if(name.compare(this->prop4fvalue)==0) { this->prop4fvalue.clear(); }
				}
				else if(p->isSparse())
				{
					/* The victim property is sparse, thus we can remove it from 'sparse_properties' */
					it = find(this->sparse_properties.begin(),this->sparse_properties.end(),p);
					assert(it!=this->sparse_properties.end());
					this->sparse_properties.erase(it);
				}
				else if(p->isGhost())
				{
					/* The victim property is ghost, thus we can remove it from 'ghost_properties' */
					it = find(this->ghost_properties.begin(),this->ghost_properties.end(),p);
					assert(it!=this->ghost_properties.end());
					this->ghost_properties.erase(it);
				}
				else
				{
					/* The victim property is global, thus we can remove it from 'global_properties' */
					it = find( this->global_properties.begin(), this->global_properties.end(), p);
					assert(it!=this->global_properties.end());
					this->global_properties.erase(it);
				}

				/* If we arrive here, we can destroy 'p' and exit */
				delete p;
				p = NULL;
			}
			
			/// This member function returns the number of local properties in the current data structure.
			/**
			 * This member function returns the number of local properties in the current data structure: we say that a property (i.e. auxiliary information like Euclidean coordinates and
			 * field values) is <i>local</i> if and only if it is binded to a particular class of simplices (identified by their dimension) directly encoded in the current data
			 * structure.<p>If we cannot complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode.
			 * In this case, each forbidden operation will result in a failed assert.
	 		 * \param i the dimension of the simplices related to the local property to be analyzed
	 		 * \return the number of local properties in the current data structure.
	 		 * \see mangrove_tds::PropertyBase, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::LocalPropertyHandle, mangrove_tds::BaseSimplicialComplex::addLocalProperty(),
	 		 * mangrove_tds::BaseSimplicialComplex::deleteProperty(), mangrove_tds::BaseSimplicialComplex::removeAllProperties(),
	 		 * mangrove_tds::BaseSimplicialComplex::debugLocalProperties()
	 		 */		 
	 		inline unsigned int getLocalPropertiesNumber(SIMPLEX_TYPE i) const
			{
				/* First, we check if the required class of simplices exists! */
				assert( i<=this->type());
				return this->local_properties[i].size();
			}
			
			/// This member function returns the number of global properties in the current data structure.
			/**
			 * This member function returns the number of global properties in the current data structure: we say that a property (i.e. auxiliary information like Euclidean coordinates and
			 * field values) is <i>global</i> if and only if it is binded to all the simplices directly encoded in the current data structure.<p>If we cannot complete this operation for any
			 * reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a
			 * failed assert.
	 		 * \return the number of global properties in the current data structure.
	 		 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::BaseSimplicialComplex::addGlobalProperty(),
	 		 * mangrove_tds::BaseSimplicialComplex::deleteProperty(), mangrove_tds::BaseSimplicialComplex::removeAllProperties(),
	 		 * mangrove_tds::BaseSimplicialComplex::debugGlobalProperties()
	 		 */
			inline unsigned int getGlobalPropertiesNumber() const
			{
				SIMPLEX_TYPE t;
				
				/* First, we extract the type of the current simplicial complex (for assert) and then we can proceed */
				t = this->type();
				return this->global_properties.size(); 
			}
			
			/// This member function returns the number of sparse properties in the current data structure.
	 		/**
	 		 * This member function returns the number of sparse properties in the current data structure: we say that a property (i.e. auxiliary information like Euclidean coordinates and
			 * field values) is <i>sparse</i> if and only if it is associated to a sparse set of simplices directly encoded in the current data structure without particular rules.<p>If we
			 * cannot complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each
			 * forbidden operation will result in a failed assert.	 
			 * \return the number of sparse properties in the current data structure
	 		 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::PropertyBase, mangrove_tds::SparsePropertyHandle, mangrove_tds::BaseSimplicialComplex::addSparseProperty(),
	 		 * mangrove_tds::BaseSimplicialComplex::deleteProperty(), mangrove_tds::BaseSimplicialComplex::removeAllProperties(),
	 		 * mangrove_tds::BaseSimplicialComplex::debugSparseProperties()
	 		 */ 
	 		inline unsigned int getSparsePropertiesNumber() const
			{
				SIMPLEX_TYPE t;

				/* First, we extract the type of the current simplicial complex (for assert) and then we can proceed */
				t = this->type();
				return this->sparse_properties.size(); 
			}
			
			/// This member function returns the number of ghost properties in the current data structure.
			/**
			 * This member function returns the number of ghost properties in the current data structure: we say that a property (i.e. auxiliary information like Euclidean coordinates and
			 * field values) is <i>ghost</i> if and only if it is associated to a subset of simplices not directly encoded in the input data structure.<p>If we cannot complete this operation
			 * for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode.
	 		 * In this case, each forbidden operation will result in a failed assert.	 
			 * \return the number of ghost properties in the current data structure
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::PropertyBase, mangrove_tds::GhostPropertyHandle, mangrove_tds::BaseSimplicialComplex::addGhostProperty(),
			 * mangrove_tds::BaseSimplicialComplex::deleteProperty(), mangrove_tds::BaseSimplicialComplex::removeAllProperties(),
			 * mangrove_tds::BaseSimplicialComplex::debugGhostProperties()
			 */
			inline unsigned int getGhostPropertiesNumber() const
			{
				SIMPLEX_TYPE t;
				
				/* First, we extract the type of the current simplicial complex (for assert) and then we can proceed */
				t = this->type();
				return this->ghost_properties.size(); 
			}
			
			/// This member function provides a debug representation of local properties associated to the current data structure.
			/**
			 * This member function provides a debug representation of local properties associated to the current data structure. We say that a property (i.e. auxiliary information like
			 * Euclidean coordinates and field values) is <i>local</i> if and only if it is binded to a particular class of simplices (identified by their dimension) directly encoded in the
			 * current data structure.<p>If we cannot complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled
			 * in debug mode. In this case, each forbidden operation will result in a failed assert.	
			 * \param i the dimension of the simplices related to the local property to be analyzed
			 * \param os the output stream where we must write the debug representation of all the local properties
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::PropertyBase::debug(), mangrove_tds::BaseSimplicialComplex::addLocalProperty(), 
			 * mangrove_tds::BaseSimplicialComplex::deleteProperty(), mangrove_tds::BaseSimplicialComplex::removeAllProperties(),
			 * mangrove_tds::BaseSimplicialComplex::getLocalPropertiesNumber()
			 */
			inline void debugLocalProperties(SIMPLEX_TYPE i,ostream& os = cout)
	 		{
	 			vector<PropertyBase*>::iterator it;
	 			
	 			/* First, we must check if all is ok */
	 			assert(i<=this->type());
	 			os<<"There are "<<this->local_properties[i].size()<<" local properties for the ";
	 			os<<this->simplices[i].size()<<" simplices of dimension "<<i<<endl<<endl;
	 			for(it=this->local_properties[i].begin();it!=this->local_properties[i].end();it++) 
	 			{
	 				/* Now, we write the current local property */
	 				(*it)->debug(os);
	 				os<<endl;
	 			}
	 			
	 			/* If we arrive here, we have finished! */
	 			os.flush();
	 		}
	 		
	 		/// This member function provides a debug representation of global properties associated to the current data structure.
			/**
			 * This member function provides a debug representation of global properties associated to the current data structure. We say that a property (i.e. auxiliary information like
			 * Euclidean coordinates and field values) is <i>global</i> if and only if it is binded to all the simplices directly encoded in the current data structure.<p>If we cannot
			 * complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each
			 * forbidden operation will result in a failed assert.
			 * \param os the output stream where we must write the debug representation of all the global properties
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::PropertyBase::debug(), mangrove_tds::GlobalPropertyHandle, mangrove_tds::BaseSimplicialComplex::addGlobalProperty(),
			 * mangrove_tds::BaseSimplicialComplex::deleteProperty(), mangrove_tds::BaseSimplicialComplex::removeAllProperties(),
			 * mangrove_tds::BaseSimplicialComplex::getGlobalPropertiesNumber()
			 */
			inline void debugGlobalProperties(ostream& os = cout)
	 		{
	 			SIMPLEX_TYPE t;
	 			vector<PropertyBase*>::iterator it;
	 			
	 			/* First, we check if the current simplicial complex is initialized */
	 			t = this->type();
	 			os<<"There are "<<this->global_properties.size()<<" global properties"<<endl<<endl;
	 			for(it=this->global_properties.begin();it!=this->global_properties.end();it++)
	 			{
	 				/* Now, we write the current global property */
	 				(*it)->debug(os);
	 				os<<endl;
	 			}
	 			
	 			/* If we arrive here, we have finished! */
	 			os.flush();
	 		}
	 		
	 		/// This member function provides a debug representation of sparse properties associated to the current data structure.
	 		/**
	 		 * This member function provides a debug representation of sparse properties associated to the current data structure. We say that a property (i.e. auxiliary information like
			 * Euclidean coordinates and field values) is <i>sparse</i> if and only if it is associated to a sparse set of simplices directly encoded in the current data structure without
			 * particular rules.<p>If we cannot complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in
			 * debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param os the output stream where we must write the debug representation of all the sparse properties
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::PropertyBase::debug(), mangrove_tds::SparsePropertyHandle, mangrove_tds::BaseSimplicialComplex::addSparseProperty(),
			 * mangrove_tds::BaseSimplicialComplex::deleteProperty(),  mangrove_tds::BaseSimplicialComplex::removeAllProperties(),
			 * mangrove_tds::BaseSimplicialComplex::getSparsePropertiesNumber()
	 		 */
			inline void debugSparseProperties(ostream& os = cout)
	 		{
	 			SIMPLEX_TYPE t;
	 			vector<PropertyBase*>::iterator it;
	 			
	 			/* First, we check if the current simplicial complex is initialized */
	 			t = this->type();
	 			os<<"There are "<<this->sparse_properties.size()<<" sparse properties"<<endl<<endl;
	 			for(it=this->sparse_properties.begin();it!=this->sparse_properties.end();it++)
	 			{
	 				/* Now, we write the current sparse property */
	 				(*it)->debug(os);
	 				os<<endl;
	 			}
	 			
	 			/* If we arrive here, we have finished! */
	 			os.flush();
	 		}
	 		
	 		/// This member function provides a debug representation of ghost properties associated to the current data structure.
	 		/**
	 		 * This member function provides a debug representation of ghost properties associated to the current data structure. We say that a property (i.e. auxiliary information like
			 * Euclidean coordinates and field values) is <i>ghost</i> if and only if it is associated to a subset of simplices not directly encoded in the input data structure.<p>If we
			 * cannot complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each
			 * forbidden operation will result in a failed assert.
	 		 * \param os the output stream where we must write the debug representation of all the ghost properties
	 		 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::PropertyBase::debug(), mangrove_tds::GhostPropertyHandle, mangrove_tds::BaseSimplicialComplex::addGhostProperty(),
	 		 * mangrove_tds::BaseSimplicialComplex::deleteProperty(), mangrove_tds::BaseSimplicialComplex::removeAllProperties(),
	 		 * mangrove_tds::BaseSimplicialComplex::getGhostPropertiesNumber()
			 */
	 		inline void debugGhostProperties(ostream& os = cout)
	 		{
	 			SIMPLEX_TYPE t;
	 			vector<PropertyBase*>::iterator it;
	 			
	 			/* First, we check if the current simplicial complex is initialized */
	 			t = this->type();
	 			os<<"There are "<<this->ghost_properties.size()<<" ghost properties"<<endl<<endl;
	 			for(it=this->ghost_properties.begin();it!=this->ghost_properties.end();it++)
	 			{
	 				/* Now, we write the current ghost property */
	 				(*it)->debug(os);
	 				os<<endl;
	 			}
	 			
	 			/* If we arrive here, we have finished! */
	 			os.flush();
	 		}

			/// This member function provides a representation of several clusters in the simplicial complex represented by the current data structure.
			/**
			 * This member function provides a representation of several clusters in the simplicial complex represented by the current data structure. Such representation can be used with the
			 * <i><A href=http://www.disi.unige.it/person/PanozzoD/mc/>Graph-Complex Viewer</A></i>, a tool for visualizing a decomposition of a simplicial complex and its decomposition
			 * graph. You should refer the official documentation of this tool for complete guidelines to be satisfied. In this member function, we create only a node for each cluster,
			 * without arcs.<p>This tool can visualize d-dimensional simplicial complexes with d=1,2,3 embedded in the Euclidean tridimensional space and it is not tailored for a particular
			 * decomposition. Thus, we also require Euclidean coordinates, stored in a local property (i.e. an instance of the mangrove_tds::LocalPropertyHandle class), associated to all the
			 * vertices for storing Euclidean coordinates. The name of the involved property is returned by the mangrove_tds::BaseSimplicialComplex::getPropertyName4Coordinates() member
			 * function. For its encoding, we apply instances of the mangrove_tds::Point class and <i>double floating</i> values.<p>We assume all clusters are formed by <i>ghost</i>
			 * simplices, i.e. not directly stored in the current data structure. They are identified by instances of the mangrove_tds::GhostSimplexPointer class and thus we can invoke this
			 * member function only with a <i>local</i> data structure.<p><b>IMPORTANT:</b> if we cannot complete this operation, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param clusters a description of the clusters to be written
			 * \param fname the path for the output file
			 * \param graph_name the name of the output graph
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_ID, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::PropertyBase, mangrove_tds::Point,
			 * mangrove_tds::LocalPropertyHandle, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isValid(),
			 * mangrove_tds::BaseSimplicialComplex::getPropertyName4Coordinates(), mangrove_tds::BaseSimplicialComplex::supportsEuclideanCoordinates()
			 */
			inline void componentsForViewer( vector< vector<GhostSimplexPointer> > &clusters, string fname, string graph_name)
			{
				ofstream os;
				vector< vector<SIMPLEX_ID> > inds;
				vector< Point<double> > points;
				vector< Point<double> > bars;
				double xp,yp,zp;
				unsigned int lg,aux;

				/* First, we check if all is ok! */
				assert(this->encodeAllSimplices()==false);
				assert(this->supportsEuclideanCoordinates());
				assert(this->type()<=3);
				assert(clusters.size()!=0);
				for(SIMPLEX_TYPE d=0;d<=3;d++) { inds.push_back( vector<SIMPLEX_ID>()); }
				os.open(fname.c_str());
				assert(os.is_open());	
				os<<clusters.size()<<endl;
				for(unsigned int k=0;k<clusters.size();k++)
				{
					/* Now, we write the description of the k-th clusters! */
					os<<string(string("component")+fromQt2Cplustring(QString::number(k)))<<endl;
					os.flush();
					this->expandCluster(clusters[k],points,inds);
					lg=points.size();
					os<<lg<<endl;
					xp=0.0;
					yp=0.0;
					zp=0.0;
					for(unsigned int j=0;j<lg;j++)
					{
						os<<points[j].x()<<" "<<points[j].y()<<" "<<points[j].z()<<endl;
						os.flush();
						xp=xp+points[j].x();
						yp=yp+points[j].y();
						zp=zp+points[j].z();
					}
				
					/* Now, we add the gravity center and the simplices! */
					bars.push_back(Point<double>(3));
					bars.back().x() = (xp / lg);
					bars.back().y() = (yp / lg);
					bars.back().z() = (zp / lg);
					if( (inds[1].size()==0) && (inds[2].size()==0) && (inds[3].size()==0))
					{
						/* We have a component composed only by vertices! */
						os<<points.size()<<endl;
						for(unsigned z=0;z<points.size();z++) os<<z<<" ";
						os<<endl;
						os<<"0"<<endl;
						os<<"0"<<endl;
						os<<"0"<<endl;
					}
					else
					{
						/* Now, we can write elements: no vertices, we start from edges! */
						os<<"0"<<endl;
						lg=inds[1].size()/2;
						os<<lg<<endl;
						aux=0;
						for(unsigned int z=0;z<lg;z++)
						{
							os<<inds[1].at(aux)<<" "<<inds[1].at(aux+1)<<endl;
							aux=aux+2;
						}

						/* Now, we analyze triangles */
						lg=inds[2].size()/3;
						os<<lg<<endl;
						aux=0;
						for(unsigned int z=0;z<lg;z++)
						{
							os<<inds[2].at(aux)<<" "<<inds[2].at(aux+1)<<" "<<inds[2].at(aux+2)<<endl;
							aux=aux+3;
						}
					
						/* Now, we analyze tetrahedra! */
						lg=inds[3].size()/4;
						os<<lg<<endl;
						aux=0;
						for(unsigned int z=0;z<lg;z++)
						{
							os<<inds[3].at(aux)<<" "<<inds[3].at(aux+1)<<" "<<inds[3].at(aux+2)<<" "<<inds[3].at(aux+3)<<endl;
							aux=aux+4;
						}
					}
				}

				/* If we arrive here, we can complete this description! */
				os<<"1"<<endl;
				os<<graph_name<<endl;
				os<<clusters.size()<<endl;
				for(unsigned int k=0;k<clusters.size();k++)
				{
					os<<string(string("component")+fromQt2Cplustring(QString::number(k)))<<" ";
					os<<bars[k].x()<<" "<<bars[k].y()<<" "<<bars[k].z()<<" "<<k<<endl;
				}
			
				/* If we arrive here, we finished our analysis */
				os<<"0"<<endl;
				os.close();
			}

			/// This member function provides a representation of several clusters in the simplicial complex represented by the current data structure.
			/**
			 * This member function provides a representation of several clusters in the simplicial complex represented by the current data structure. Such representation can be used with the
			 * <i><A href=http://www.disi.unige.it/person/PanozzoD/mc/>Graph-Complex Viewer</A></i>, a tool for visualizing a decomposition of a simplicial complex and its decomposition
			 * graph. You should refer the official documentation of this tool for complete guidelines to be satisfied. In this member function, we create only a node for each cluster,
			 * without arcs.<p>This tool can visualize d-dimensional simplicial complexes with d=1,2,3 embedded in the Euclidean tridimensional space and it is not tailored for a particular
			 * decomposition. Thus, we also require Euclidean coordinates, stored in a local property (i.e. an instance of the mangrove_tds::LocalPropertyHandle class), associated to all the
			 * vertices for storing Euclidean coordinates. The name of the involved property is returned by the mangrove_tds::BaseSimplicialComplex::getPropertyName4Coordinates() member
			 * function. For its encoding, we apply instances of the mangrove_tds::Point class and <i>double floating</i> values.<p>We assume all clusters are formed by <i>valid</i>
			 * simplices, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They are identified by
			 * instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data
			 * structure.<p><b>IMPORTANT:</b> if we cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug
			 * mode. In this case, each forbidden operation will result in a failed assert.
			 * \param clusters a description of the clusters to be written
			 * \param fname the path for the output file
			 * \param graph_name the name of the output graph
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_ID, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::PropertyBase, mangrove_tds::Point,
			 * mangrove_tds::LocalPropertyHandle, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isValid(),
			 * mangrove_tds::BaseSimplicialComplex::getPropertyName4Coordinates(), mangrove_tds::BaseSimplicialComplex::supportsEuclideanCoordinates()
			 */
			inline void componentsForViewer( vector< vector<SimplexPointer> > &clusters, string fname, string graph_name)
			{
				vector< vector<SIMPLEX_ID> > inds;
				ofstream os;
				vector< Point<double> > points;
				vector< Point<double> > bars;
				unsigned int lg,aux;
				double xp,yp,zp;
			
				/* First, we check if all is ok! */
				assert(this->encodeAllSimplices());
				assert(this->supportsEuclideanCoordinates());
				assert(this->type()<=3);
				assert(clusters.size()!=0);
				for(SIMPLEX_TYPE d=0;d<=3;d++) { inds.push_back( vector<SIMPLEX_ID>()); }
				os.open(fname.c_str());
				assert(os.is_open());	
				os<<clusters.size()<<endl;
				for(unsigned int k=0;k<clusters.size();k++)
				{
					/* Now, we write the description of the k-th clusters! */
					os<<string(string("component")+fromQt2Cplustring(QString::number(k)))<<endl;
					os.flush();
					this->expandCluster(clusters[k],points,inds);
					lg=points.size();
					os<<lg<<endl;
					xp=0.0;
					yp=0.0;
					zp=0.0;
					for(unsigned int j=0;j<lg;j++)
					{
						os<<points[j].x()<<" "<<points[j].y()<<" "<<points[j].z()<<endl;
						os.flush();
						xp=xp+points[j].x();
						yp=yp+points[j].y();
						zp=zp+points[j].z();
					}
				
					/* Now, we add the gravity center and the simplices! */
					bars.push_back(Point<double>(3));
					bars.back().x() = (xp / lg);
					bars.back().y() = (yp / lg);
					bars.back().z() = (zp / lg);
					if( (inds[1].size()==0) && (inds[2].size()==0) && (inds[3].size()==0))
					{
						/* We have a component composed only by vertices! */
						os<<points.size()<<endl;
						for(unsigned z=0;z<points.size();z++) os<<z<<" ";
						os<<endl;
						os<<"0"<<endl;
						os<<"0"<<endl;
						os<<"0"<<endl;
					}
					else
					{
						/* Now, we can write elements: no vertices, we start from edges! */
						os<<"0"<<endl;
						lg=inds[1].size()/2;
						os<<lg<<endl;
						aux=0;
						for(unsigned int z=0;z<lg;z++)
						{
							os<<inds[1].at(aux)<<" "<<inds[1].at(aux+1)<<endl;
							aux=aux+2;
						}

						/* Now, we analyze triangles */
						lg=inds[2].size()/3;
						os<<lg<<endl;
						aux=0;
						for(unsigned int z=0;z<lg;z++)
						{
							os<<inds[2].at(aux)<<" "<<inds[2].at(aux+1)<<" "<<inds[2].at(aux+2)<<endl;
							aux=aux+3;
						}
					
						/* Now, we analyze tetrahedra! */
						lg=inds[3].size()/4;
						os<<lg<<endl;
						aux=0;
						for(unsigned int z=0;z<lg;z++)
						{
							os<<inds[3].at(aux)<<" "<<inds[3].at(aux+1)<<" "<<inds[3].at(aux+2)<<" "<<inds[3].at(aux+3)<<endl;
							aux=aux+4;
						}
					}
				}

				/* If we arrive here, we can complete this description! */
				os<<"1"<<endl;
				os<<graph_name<<endl;
				os<<clusters.size()<<endl;
				for(unsigned int k=0;k<clusters.size();k++)
				{
					os<<string(string("component")+fromQt2Cplustring(QString::number(k)))<<" ";
					os<<bars[k].x()<<" "<<bars[k].y()<<" "<<bars[k].z()<<" "<<k<<endl;
				}
			
				/* If we arrive here, we finished our analysis */
				os<<"0"<<endl;
				os.close();
			}
			
			/// This member function provides a representation of the simplicial complex represented by the current data structure.
			/**
			 * This member function provides a representation of the simplicial complex represented by the current data structure. Such representation can be used with the
			 * <i><A href=http://www.disi.unige.it/person/PanozzoD/mc/>Graph-Complex Viewer</A></i>, a tool for visualizing a decomposition of a simplicial complex and its decomposition
			 * graph. You should refer the official documentation of this tool for complete guidelines to be satisfied. In this member function, we create only a node for the whole model,
			 * without arcs.<p>This tool can visualize d-dimensional simplicial complexes with d=1,2,3 embedded in the Euclidean tridimensional space and it is not tailored for a particular
			 * decomposition. Thus, we also require Euclidean coordinates, stored in a local property (i.e. an instance of the mangrove_tds::LocalPropertyHandle class), associated to all the
			 * vertices for storing Euclidean coordinates. The name of the involved property is returned by the mangrove_tds::BaseSimplicialComplex::getPropertyName4Coordinates() member
			 * function. For its encoding, we apply instances of the mangrove_tds::Point class and <i>double floating</i> values.<p><b>IMPORTANT:</b> if we cannot complete this operation,
			 * then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.
			 * \param fname the path for the output file
			 * \param graph_name the name of the output graph
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_ID, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::PropertyBase, mangrove_tds::Point,
			 * mangrove_tds::LocalPropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(),
			 * mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::BaseSimplicialComplex::getPropertyName4Coordinates(),
			 * mangrove_tds::BaseSimplicialComplex::supportsEuclideanCoordinates()
			 */
			inline void write4Viewer(string fname, string graph_name)
			{
				list<SimplexPointer> tops;
				vector< vector<SimplexPointer> > globs;
				vector< vector<GhostSimplexPointer> > locs;

				/* First, we check if all is ok! */
				assert(this->supportsEuclideanCoordinates());
				assert(this->type()<=3);
				this->getTopSimplices(tops);
				if(this->encodeAllSimplices())
				{
					/* The current data structure is global */
					globs.push_back(vector<SimplexPointer>());
					for(list<SimplexPointer>::iterator it=tops.begin();it!=tops.end();it++) globs.back().push_back(SimplexPointer(*it));
					this->componentsForViewer(globs,fname,graph_name);
				}
				else
				{
					/* The current data structure is local */
					locs.push_back(vector<GhostSimplexPointer>());
					for(list<SimplexPointer>::iterator it=tops.begin();it!=tops.end();it++) locs.back().push_back(GhostSimplexPointer(it->type(),it->id()));
					this->componentsForViewer(locs,fname,graph_name);
				}
			}
			
			/// This member function provides a debug representation of the current data structure.
	 		/**
	 		 * This member function provides a debug representation of the current data structure on an output stream, by providing some information about its simplices and its properties,
			 * i.e. auxiliary information associated to the current data structure (if required).<p>If we cannot complete this operation, then this member function will fail if and only if
			 * the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \param prop this boolean flag allows the writing of the debug representation for all properties
			 * \param os the output stream where we can write a debug representation of the current data structure
	 		 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplicesContainer::debug(), mangrove_tds::BaseSimplicialComplex::getDataStructureName(),
	 		 * mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicalComplex::getStorageCost(), mangrove_tds::Simplex::debug(),
	 		 * mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::debugLocalProperties(), mangrove_tds::BaseSimplicialComplex::debugGlobalProperties(),
	 		 * mangrove_tds::BaseSimplicialComplex::debugSparseProperties(), mangrove_tds::BaseSimplicialComplex::debugGhostProperties() 
	 		 */
	 		inline void debug(bool prop,ostream& os = cout)
			{
				SIMPLEX_TYPE t;
				
				/* We write a debug representation of the current simplicial complex. */
				t = this->type();
				os<<endl<<"This is a simplicial "<<t<<"-complex encoded through the "<<this->getDataStructureName()<<" data structure"<<endl<<endl;
				os<<"\tThe "<<this->getDataStructureName()<<" data structure ";
				if(this->encodeAllSimplices()) os<<"encodes all simplices"<<endl<<endl;
				else os<<"does not encode all simplices"<<endl<<endl;
				cout<<"\tStorage cost: "<<this->getStorageCost()<<endl<<endl;
				os<<"\tName of the local property for Euclidean coordinates: "<<string(this->prop4coords)<<endl<<endl;
				os<<"\tName of the local property for field value: "<<string(this->prop4fvalue)<<endl<<endl;
				for(SIMPLEX_TYPE i=0;i<=t;i++)
				{
					os<<"COLLECTION of "<<i<<"-SIMPLICES"<<endl;
					os<<"=============================="<<endl<<endl;
					this->simplices[i].debug();
					if(prop) this->debugLocalProperties(i,os);
				}
				
				/* Now, we write information about the other properties */
				if(prop)
				{
					this->debugGlobalProperties(os);
					this->debugSparseProperties(os);
					if(this->encodeAllSimplices()==false) this->debugGhostProperties(os);
					os.flush();
				}
			}
			
			/// This member function checks if we can always use the input pointer for accessing simplices.
			/**
			 * This member function checks if we can always use the input pointer for accessing simplices.<p>In this case, we can always use instances of the
			 * mangrove_tds::SimplexPointer class only with <i>global</i> data structures, i.e., that encode all simplices.
			 * \param cp a fake parameter for specializing this member function
			 * \return <ul><li><i>true</i>, if the current data structure is global</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplexPointer, mangrove_tds::idle()
			 */
			inline bool canBeAlwaysUsed(const SimplexPointer& cp)
			{
				/* We can always use 'SimplexPointer' only with global data structures! */
				idle(cp);
				return (this->encodeAllSimplices()==true);
			}
			
			/// This member function checks if we can always use the input pointer for accessing simplices.
			/**
			 * This member function checks if we can always use the input pointer for accessing simplices.<p>In this case, we can always use instances of the
			 *  mangrove_tds::GhostSimplexPointer class only with <i>local</i> data structures, i.e., that encode only a subset of all simplices.
			 * \param gp a fake parameter for specializing this member function
			 * \return <ul><li><i>true</i>, if the current data structure is local</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplexPointer, mangrove_tds::idle()
			 */
			inline bool canBeAlwaysUsed(const GhostSimplexPointer& gp)
			{
				/* We can always use 'GhostSimplexPointer' only with local data structures! */
				idle(gp);
				return (this->encodeAllSimplices()==false);
			}

			protected:
			
			/// All simplices directly encoded in the current data structure, ordered by dimension.
			/**
			 * All simplices directly encoded in the current data structure are ordered by dimension and stored in a collection: the collection in the k-th location stores all the
			 * k-simplices in the current data structure.
			 * \see mangrove_tds::SimplicesContainer
			 */
			vector<SimplicesContainer> simplices;
			
			/// These auxiliary data structures are useful for visiting simplices directly encoded in the current data structure.
			/**
			 * The k-th location of this vector contains all the indices of the k-simplices that have been marked as <i>visited</i> in order to extract topological relations.
			 * \see mangrove_tds::SIMPLEX_ID
			 */
			vector< vector<SIMPLEX_ID> > visited;
			
			/// The name of the local property associated to all the vertices for storing Euclidean coordinates.
			/**
			 * Euclidean coordinates (if supported) are stored as a local property (described by the mangrove_tds::LocalPropertyHandle class) and they are associated to each vertex. They are
			 * described as instance of the mangrove_tds::Point class.
			 * \see mangrove_tds::LocalPropertyHandle, mangrove_tds::Point, mangrove_tds::Simplex, mangrove_tds::SimplexPointer
			 */
			string prop4coords;
			
			/// The name of the local property associated to all the vertices for storing field values.
			/**
			 * Field values (if supported) are stored as a local property (described by the mangrove_tds::LocalPropertyHandle class) and they are associated to each vertex. They are
			 * described a <i>double floating point</i> value.
			 * \see mangrove_tds::LocalPropertyHandle, mangrove_tds::Simplex, mangrove_tds::SimplexPointer
			 */
			string prop4fvalue;
			
			/// All the local properties associated to simplices directly encoded in the current data structure, ordered by dimension.
			/**
			 * We say that a property is <i>local</i> if and only if it is binded only for a particular class of simplices, identified by their dimension. The local properties are ordered by
			 * dimension and we store local properties associated to k-simplices in the k-th location of this vector. Each of them is described by an instance of the
			 * mangrove_tds::LocalPropertyHandle class.
			 * \see mangrove_tds::LocalPropertyHandle, mangrove_tds::PropertyBase
			 */
			vector < vector<PropertyBase *> > local_properties;
			
			/// All the global properties associated to simplices directly encoded in the current data structure.
			/**
			 * We say that a property is <i>global</i> if and only if it is binded for all the simplices directly encoded in the current data structure. Each of them is described by an
			 * instance of the mangrove_tds::GlobalPropertyHandle class.
			 * \see mangrove_tds::GlobalPropertyHandle, mangrove_tds::PropertyBase
			 */
			vector<PropertyBase *> global_properties;
			
			/// All the sparse properties associated to simplices directly encoded in the current data structure.
			/**
			 * We say that a property is <i>sparse</i> if and only if it is associated to a sparse set of simplices without particular rules. Each of them is described by an instance of the
			 * mangrove_tds::SparsePropertyHandle class.
			 * \see mangrove_tds::SparsePropertyHandle, mangrove_tds::PropertyBase
		     	 */
			vector<PropertyBase *> sparse_properties;
			
			/// All the ghost properties associated to simplices not directly encoded in the current data structure.
			/**
			 * We say that a property is <i>ghost</i> if and only if it is associated to a set of simplices not directly encoded in the current data structure. Such properties are valid only
			 * if used with a <i>local</i> data structure, that encodes only a subset of simplices, i.e. only the top ones. Each of them is described by an instance of the
			 * mangrove_tds::GhostPropertyHandle class.
			 * \see mangrove_tds::GhostPropertyHandle, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices()
			 */
			vector<PropertyBase *> ghost_properties;
			
			/// The Pascal's Triangle to be used for generating subfaces.
			/**
		 	 * In mathematics, Pascal's triangle is a triangular array, whose elements are the binomial coefficients.The Pascal's Triangle has usually d+1 rows, where d is the <i>order</i>
			 * of such triangle. If we are encoding a simplicial k-complex, then the order of this Pascal's Triangle is d=k+1.
		 	 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::type(), mangrove_tds::BaseSimplicialComplex::init(), mangrove_tds::PASCAL_TRIANGLE
		 	 */
		 	PASCAL_TRIANGLE pascal_triangle;
			
			/// The positions of subfaces of a simplex, expressed in terms of vertices identifiers.
			/**
			 * The positions of subfaces of a simplex, expressed in terms of vertices identifiers (i.e. instances of the mangrove_tds::Face class), organized into a hierarchy of faces.
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::Face, mangrove_tds::FaceHierarchy
			 */
			vector<FaceHierarchy> fposes;

			/// This member function creates a new instance of this class.
			inline BaseSimplicialComplex() { ; }
			
			/// This member function creates the Pascal's Triangle of a certain order.
			/**
			 * This member function creates the Pascal's Triangle of a certain order to be used for checking the subfaces generation.<p>In mathematics, Pascal's triangle is a triangular
			 * array, whose elements are the binomial coefficients.The Pascal's Triangle has usually d+1 rows, where d is the <i>order</i> of such triangle. If we are encoding a simplicial
			 * k-complex, then the order of this Pascal's Triangle is d=k+1.
			 * \param n the order of the new Pascal's Triangle to be built
		 	 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::type(), mangrove_tds::BaseSimplicialComplex::init(), mangrove_tds::PASCAL_TRIANGLE
		 	 */
			inline void createPascalTriangle(SIMPLEX_TYPE n)
			{
				/* First, we clear 'pascal_triangle' and then we compute the required triangle!*/
				for(unsigned int k=0;k<pascal_triangle.size();k++) pascal_triangle[k].clear();
				pascal_triangle.clear();
				pascal_triangle.push_back(vector<unsigned int>());
				pascal_triangle.back().push_back(1);
				if(n==0) return;
				pascal_triangle.push_back(vector<unsigned int>());
				pascal_triangle.back().push_back(1);
				pascal_triangle.back().push_back(1);
				if(n==1) return;
				for(unsigned int k=2;k<=n;k++)
				{
					/* We add a new line! */
					pascal_triangle.push_back(vector<unsigned int>());
					pascal_triangle.back().push_back(1);
					for(unsigned int j=1;j<k;j++) { pascal_triangle.back().push_back(pascal_triangle[k-1][j]+pascal_triangle[k-1][j-1]); }
					pascal_triangle.back().push_back(1);
				}
			}
			
			/// This member function sorts the boundary of a simplex directly encoded in the current data structure.
			/**
			 * This member function sorts the boundary of a simplex directly encoded in the current data structure. As result, the boundary of the input simplex is stored in order to extract
			 * chain-complexes from it.
			 * \param cp a pointer to the simplex (to be ordered) directly encoded in the current data structure
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::Simplex, mangrove_tds::COMPLEX,
			 * mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::boundaryVertices()
			 */
			inline void sortBoundary(const SimplexPointer &cp)
			{
				SimplexPointer aux_cp;
				set<SIMPLEX_ID>::iterator v;
				set<SIMPLEX_ID> vcp;
				vector<SIMPLEX_ID> cbp;
				vector< vector<SIMPLEX_ID> > projs;
				vector< set<SIMPLEX_ID> > bs;
				vector<SimplexPointer> aux;
				bool stop;
				unsigned int j;
				COMPLEX com;
				COMPLEX::iterator com_it;

				/* First, we extract the 'cp' boundary and then we must consider if the current data structure is global! */
				if(cp.type()==0) return;
				aux_cp.setType(cp.type());
				aux_cp.setId(cp.id());
				if(this->encodeAllSimplices()==false)
				{
					/* If we arrive here, the current data structure is local (we assume a soup of simplices). */
					for(unsigned int k=0;k<=cp.type();k++) { com.insert(this->simplex(aux_cp).b(k)); }
					com_it=com.begin();
					for(unsigned int k=0;k<=cp.type();k++,com_it++)
					{
						this->simplex(aux_cp).b(k).setType(0);
						this->simplex(aux_cp).b(k).setId(com_it->id());
					}
				}
				else
				{
					/* If we arrive here, the current data structure is global! */
					this->boundaryVertices(aux_cp,vcp);
					if(cp.type()==1)
					{
						v=vcp.begin();
						for(unsigned int k=0;k<=cp.type();k++,v++)
						{
							this->simplex(aux_cp).b(k).setType(0);
							this->simplex(aux_cp).b(k).setId(*v);						
						}
						
						/* If we arrive here, we can clear auxiliary data structures! */
						vcp.clear();	
						return; 
					}
					else
					{
						/* We haven't a basic case (i.e. an ege), thus we must compute all projections! */
						for(unsigned int k=0;k<=cp.type();k++) cbp.push_back(this->simplex(cp).b(k).id());
						for(unsigned int k=0;k<cbp.size();k++)
						{
							projs.push_back( vector<SIMPLEX_ID>());
							findProjection(k,vcp,projs[k]);
							bs.push_back(set<SIMPLEX_ID>());
							aux.push_back(SimplexPointer(cp.type()-1,cbp[k]));
							this->boundaryVertices(aux[k],bs[k]);
						}
						
						/* Now, we must maps the projections on the real simplices pointers */
						for(unsigned int k=0;k<projs.size();k++)
						{
							/* Now, we must find the correct mapping for the k-th projection */
							stop = false;
							j=0;
							while(stop==false)
							{
								if(sameValues(bs[j], projs[k])) { stop = true; }
								else { j=j+1; }
							}

							/* Now, we can update the boundary with the new mapping */
							this->simplex(aux_cp).b(k).setId(cbp[j]);
							this->simplex(aux_cp).b(k).setType(cp.type()-1);
						}
						
						/* We clear all the auxiliary data structures and exit! */
						for(unsigned int k=0;k<projs.size();k++) { projs[k].clear(); }
						for(unsigned int k=0;k<bs.size();k++) { bs.clear(); }
						bs.clear();
						projs.clear();
						cbp.clear();
						vcp.clear();
						aux.clear();	
					}
				}
			}
			
			/// This member function extracts all the vertices on the boundary of a simplex directly encoded in the current data structure.
			/**
			 * This member function extracts all the 0-simplices (namely vertices) on the boundary of a simplex directly encoded in the current data structure. The resulting set of vertices
			 * is ordered against their identifiers.<p><b>IMPORTANT:</b> we assume the current data structure is <i>global</i>, i.e. it encodes all simplices.
			 * \param cp a pointer for the required simplex directly encoded in the current data structure
			 * \param ob the ordered set of vertices on the required simplex boundary
			 * \see mangrove_tds::BaseSimplicialComplex::sortBoundary()
			 */
			inline void boundaryVertices(const SimplexPointer &cp, set<SIMPLEX_ID> &ob)
			{
				/* First, we must check if we are in the basic situation (we have an edge or a vertex) or not! */
				if(cp.type()==0) return;
				else if(cp.type()==1)
				{
					ob.insert( this->simplex(cp).bc(0).id());
					ob.insert(this->simplex(cp).bc(1).id());
				}
				else { for(unsigned int k=0;k<=cp.type();k++) this->boundaryVertices(SimplexPointer(this->simplex(cp).bc(k)),ob); }
			}
			
			/// This member function updates the visiting status of a simplex in the current data structure.
			/**
			 * This member function updates the visiting status of a simplex directly encoded in the current data structure: this operation allows to reduce the number of computations, for
			 * example when we are extracting a topological relation or for other types of computations.<p>The reference simplex is required to be <i>valid</i>, i.e. a real location that has
			 * not been marked as <i>deleted</i> by the garbage collector mechanism, and it is is identified by an instance of the mangrove_tds::SimplexPointer class and thus by its type and
			 * by its identifier. If we cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this
			 * case, each forbidden operation will result in a failed assert.
			 * \param cp a pointer to the required simplex
			 * \param flag a boolean flag that identifies the new visiting state of the required simplex
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplicesContainer, mangrove_tds::BaseSimplicialComplex::isValid(),
			 * mangrove_tds::BaseSimplicialComplex::isVisited(), mangrove_tds::BaseSimplicialComplex::unmarkAllVisited(), mangrove_tds::Simplex::setVisited()
			 */
			inline void visit(const SimplexPointer& cp, bool flag = true)
			{
				bool cf;
				vector<SIMPLEX_ID>::iterator it;

				/* First, we check if the current pointer 'cp' is good for us! */
				assert( this->isValid(cp));
				cf = this->simplices[cp.type()].simplex(cp.id()).isVisited();
				if(cf==flag) { return; }
				else if( (cf==true) && (flag==false))
				{
					/* We must unvisit cp */
				    it = find( this->visited[cp.type()].begin(), this->visited[cp.type()].end(), cp.id()); 
				    assert(it!=this->visited[cp.type()].end());
					this->visited[cp.type()].erase(it);
					this->simplices[cp.type()].simplex(cp.id()).setVisited(false);
				}
				else
				{
					/* We must visit cp */
					this->visited[cp.type()].push_back(cp.id());
					this->simplices[cp.type()].simplex(cp.id()).setVisited(true);
				}
			}
			
			/// This member function checks if a simplex has been marked as visited in the current data structure.
			/**
			 * This member function checks if a simplex has been marked as visited in the current data structure: this operation allows to reduce the number of computations, for example when
			 * we are extracting a topological relation or for other types of computations.<p>We assume the reference simplex is <i>valid</i> (i.e. a real location that has not been marked as
			 * <i>deleted</i> by the garbage collector mechanism) and it is identified by an instance of the mangrove_tds::SimplexPointer class and thus by its type and by its identifier. In
			 * other words, it must be directly encoded in the current data structure.
			 * \param cp a pointer to the simplex to be checked
			 * \return <ul><li><i>true</i>, if the required simplex has been marked as visited</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::SimplicesContainer, mangrove_tds::BaseSimplicialComplex::visit(),
			 * mangrove_tds::BaseSimplicialComplex::unmarkAllVisited(), mangrove_tds::Simplex::isVisited(), mangrove_tds::BaseSimplicialComplex::isValid()
			 */
			inline bool isVisited(const SimplexPointer& cp) { return this->simplices[cp.type()].simplex(cp.id()).isVisited(); }
			
			/// This member function unmarks all the simplices marked as visited in the current data structure.
			/**
			 * This member function unmarks all the simplices that have been marked as visited in the current data structure complex: this operation allows to reduce the number of
			 * computations, for example when we are extracting a topological relation or for other types of computations and it must be invoked at the end of the required computations.
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplicesContainer, mangrove_tds::BaseSimplicialComplex::visit(), mangrove_tds::Simplex::setVisited(),
			 * mangrove_tds::BaseSimplicialComplex::isVisited(), mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::BaseSimplicialComplex::type()
			 */
			inline void unmarkAllVisited()
			{
				SIMPLEX_TYPE t;
				vector<SIMPLEX_ID>::iterator it;
				
				/* We must unmark all the visited simplices! */
				t = this->type();
				for(SIMPLEX_TYPE k=0;k<=t;k++)
				{
					/* Now, we unmark all the k-simplices! */
					for(it = this->visited[k].begin(); it!=this->visited[k].end(); it++) { this->simplices[k].simplex((*it)).setVisited(false); }
					this->visited[k].clear();
				}
			}
			
			/// This member function looks for a local property identified by its name.
			/**
			 * This member function looks for a local property identified by its unique name in the current data structure: the local properties are associated to a particular class of
			 * simplices (identified by their dimension) directly encoded in the current data structure. Each of them is described by an instance of the mangrove_tds::LocalPropertyHandle
			 * class.
			 * \param i the dimension of simplices directly encoded in the current data structure to be analyzed
			 * \param name the name of the local property to be found
			 * \return <ul><li>a pointer to the required local property, if it exists</li><li>a <i>NULL</i> value, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::findProperty()
			 */
			inline PropertyBase * findLocalProperty(SIMPLEX_TYPE i, string name)
			{
				vector<PropertyBase*>::iterator it;
				
				/* We must look for the local properties associated to i-simplices. */
				for(it=this->local_properties[i].begin();it!=this->local_properties[i].end();it++) { if(name.compare((*it)->getName())==0) return (*it); }
				return NULL;
			}
			
			/// This member function looks for a global property identified by its name.
			/**
			 * This member function looks for a global property identified by its unique name in the current data structure: the global properties are associated to all the simplices
			 * directly encoded in the current data structure. Each of them is described by an instance of the mangrove_tds::GlobalPropertyHandle class.
			 * \param name the name of the global property to be found
			 * \return <ul><li>a pointer to the required global property, if it exists</li><li>a <i>NULL</i> value, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::findProperty()
			 */
			inline PropertyBase *findGlobalProperty(string name)
			{
				vector<PropertyBase*>::iterator it;
				
				/* We must look for the global properties */
				for(it=this->global_properties.begin();it!=this->global_properties.end();it++) { if(name.compare((*it)->getName())==0) return (*it); }
				return NULL;
			}
			
			/// This member function looks for a sparse property identified by its name.
			/**
			 * This member function looks for a sparse property identified by its unique name in the current data structure: the sparse properties are associated to a subset of the simplices
			 * directly encoded in the current data structure, choosen without a particular rule. Each of them is described by an instance of the mangrove_tds::SparsePropertyHandle class.
			 * \param name the name of the sparse property to be found
			 * \return <ul><li>a pointer to the required sparse property, if it exists</li><li>a <i>NULL</i> value, otherwise</li></ul>
		     	 * \see mangrove_tds::PropertyBase, mangrove_tds::SparsePropertyHandle, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::findProperty()
			 */
			inline PropertyBase *findSparseProperty(string name)
			{
			      vector<PropertyBase *>::iterator it;

			      /* We must look for the sparse properties! */
			      for(it=this->sparse_properties.begin();it!=this->sparse_properties.end();it++) { if(name.compare((*it)->getName())==0) return (*it); }
			      return NULL;
			}
			
			/// This member function looks for a ghost property identified by its name.
			/**
			 * This member function looks for a ghost property identified by its unique name in the current data structure: the ghost properties are associated to a subset of the simplices
			 * not directly encoded in the current data structure, assumed to be <i>local</i>. Each of them is described by an instance of the mangrove_tds::GhostPropertyHandle class.
			 * \param name the name of the ghost property to be found
			 * \return <ul><li>a pointer to the required ghost property, if it exists</li><li>a <i>NULL</i> value, otherwise</li></ul>
		     	 * \see mangrove_tds::PropertyBase, mangrove_tds::GhostPropertyHandle, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::findProperty()
			 */
			inline PropertyBase *findGhostProperty(string name)
			{
				vector<PropertyBase *>::iterator it;
				
				/* We must look for the ghost properties */
				for(it=this->ghost_properties.begin();it!=this->ghost_properties.end();it++) { if(name.compare((*it)->getName())==0) return (*it); }
				return NULL;
			}
			
			/// This member function looks for a property identified by its name.
			/**
			 * This member function looks for a property identified by its unique name in the current data structure: a property (i.e. a subclass of the mangrove_tds::PropertyBase class)
			 * consists of auxiliary information (like Euclidean cooordinates and the field values) associated to each simplex directly encoded in the current data structure.
			 * \param name the unique name of the required property
			 * \return <ul><li>a pointer to the required property, if it exists</li><li>a <i>NULL</i> value, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle,
			 * mangrove_tds::GhostPropertyHandle, mangrove_tds::BaseSimplicialComplex::findLocalProperty(), mangrove_tds::BaseSimplicialComplex::findGlobalProperty(),
			 * mangrove_tds::BaseSimplicialComplex::findSparseProperty(), mangrove_tds::BaseSimplicialComplex::findGhostProperty()
			 */
			inline PropertyBase *findProperty(string name)
			{
				PropertyBase *p;
				
				/* First, we look among the global properties and then we look among the sparse and the local ones. */
				p = NULL;
				p = this->findGlobalProperty(name);
				if(p!=NULL) return p;
				p = NULL;
				p = this->findSparseProperty(name);
				if(p!=NULL) return p;
				for(SIMPLEX_TYPE t=0;t<this->local_properties.size();t++)
				{
					/* Now, we check if there is a local property with 'name' as name */
					p = NULL;
					p = this->findLocalProperty(t,name);
					if(p!=NULL) return p;
				}
				
				/* Now, we look for the ghost properties, if the current data structure is 'local' */
				if(this->encodeAllSimplices()==false)
				{
					/* The current data structure is NULL */
					p = NULL;
					p = this->findGhostProperty(name);
					if(p!=NULL) return p;
				}
				
				/* If we arrive here, the required property does not exist! */
				return NULL;
			}
			
			/// This member function checks if a simplex has an intersection with a reference simplex.
			/**
			 * This member function checks if a simplex has an intersection with a reference d-simplex: in other words, we check if the input simplex has at least one simplex in common with a
			 * reference d-simplex and thus if the reference d-simplex belongs to the boundary of the input simplex. We assume to have a local property with boolean values, called
			 * <i>refSimplex</i> in order to identify the reference simplex: such property must be associated to the d-simplices. Moreover, all the involved simplices are valid simplices
			 * directly encoded in the current data structure, required to be <i>global</i>.
			 * \param a the input simplex to be checked
			 * \param d the dimension of the reference simplex
			 * \param rsp the component for accessing the <i>refSimplex</i> local property
			 * \return <ul><li><i>true</i>, if the input simplex has an intersection with a reference simplex</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::LocalPropertyHandle, mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE
			 */
			inline bool intersect(const SimplexPointer &a, SIMPLEX_TYPE d,LocalPropertyHandle<bool> *rsp)
			{
				list<SimplexPointer> l;
				list<SimplexPointer>::iterator it;
				
				/* We must understand the type of the input simplex 'a' */
				if(a.type()<d) { return false; }
				else if(a.type()==d) return rsp->get(SimplexPointer(a));
				else
				{
					/* Now, we extract all d-simplices in the boundary of 'a' */
					this->boundaryk(SimplexPointer(a),d,&l);
					for(it=l.begin();it!=l.end();it++) if(rsp->get(SimplexPointer(*it))) return true;
					return false;
				}
			}

			/// This member function checks if a simplex has an intersection with a reference vertex.
			/**
			 * This member function checks if a simplex has an intersection with a reference 0-simplex (namely a vertex): in other words, we check if the input simplex has at least one
			 * vertex in common with a reference simplex. We assume to have a local property with boolean values, called <i>refVertex</i>, in order to identify the reference vertices.
			 * Moreover, all the involved simplices are valid simplices directly encoded in the current data structure, required to be <i>global</i>.
			 * \param a the input simplex to be checked
			 * \param rvp the component for accessing the <i>refVertex</i> local property
			 * \return <ul><li><i>true</i>, if the input simplex has an intersection with a reference simplex</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::LocalPropertyHandle, mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE
			 */
			inline bool intersect(const SimplexPointer &a,LocalPropertyHandle<bool> *rvp)
			{
				list<SimplexPointer> l;
				list<SimplexPointer>::iterator it;
				
				/* First, we check if the input simplex is a vertex! */
				if(a.type()==0) return rvp->get(SimplexPointer(a));
				else if(a.type()==1)
				{
					if(rvp->get(SimplexPointer(this->simplex(a).bc(0)))) return true;
					if(rvp->get(SimplexPointer(this->simplex(a).bc(1)))) return true;
					return false;
				}
				else
				{
					/* Now, we extract all the vertices of 'a' */
					this->boundaryk(SimplexPointer(a),0,&l);
					for(it=l.begin();it!=l.end();it++) if(rvp->get(SimplexPointer(*it))) return true;
					return false;
				}
			}
			
			/// This member function retrieves clusters in the simplicial complex represented by the current data structure.
			/**
			 * This member function retrieves clusters in the simplicial complex represented by the current data structure: for us, a k-cluster is formed by k-simplices that are
			 * (k-1)-connected.<p>We assume all k-simplices were <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the
			 * garbage collector mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke
			 * this member function only with a <i>global</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we
			 * could apply some temporary properties (i.e. an instance of the mangrove_tds::PropertyBase class) for marking simplices. We assume to have the <i>assigned2cluster</i> local
			 * property (i.e. an instance of the mangrove_tds::LocalPropertyHandle class). You should be careful about what properties are used, because usually they are removed before
			 * starting a new computation. In any case, we remove them at the end of this member function.
			 * \param d the dimension of the required clusters
			 * \param clusters simplices forming the required clusters
			 * \param a2c a component for accessing the local property <i>assigned2cluster</i>
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::PropertyBase,
			 * mangrove_tds::LocalPropertyHandle, mangrove_tds::BaseSimplicialComplex::addLocalProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty(),
			 * mangrove_tds::BaseSimplicialComplex::isTop()
			 */
			inline void findNonTopClusters(SIMPLEX_TYPE d,vector< vector<SimplexPointer> >& clusters, LocalPropertyHandle<bool> *a2c)
			{
				SimplexPointer ni;
				list<SimplexPointer> l;
				list<SimplexPointer>::iterator cp;
				
				/* Now, we iterate over the d-simplices and we look for d-clusters! */
				for(SimplexIterator it=this->begin(d);it!=this->end(d);it++)
	 			{
	 				/* Now, we check if the current d-simplex is assigned! */
	 				if(a2c->get(SimplexPointer(it.getPointer()))==false)
	 				{
	 					queue<SimplexPointer> q;

	 					/* Now, we visit a new cluster! */
	 					clusters.push_back(vector<SimplexPointer>());
	 					q.push(SimplexPointer(it.getPointer()));
	 					while(q.empty()==false)
	 					{
	 						/* Now, we extract the first element from q */
							ni=q.front();
							q.pop();
							if(a2c->get(SimplexPointer(ni))==false)
							{
								clusters.back().push_back(SimplexPointer(ni));
								a2c->set(SimplexPointer(ni),true);
								this->adjacency(SimplexPointer(ni),&l);
								for(cp=l.begin();cp!=l.end();cp++) { q.push(SimplexPointer(*cp)); }
							}
	 					}
	 				}
	 			}
			}
			
			/// This member function retrieves clusters formed only by top simplices in the simplicial complex represented by the current data structure.
			/**
			 * This member function retrieves clusters formed only by top simplices in the simplicial complex represented by the current data structure: for us, a k-cluster is formed by
			 * k-simplices that are (k-1)-connected.<p>We assume all k-simplices were <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as
			 * <i>deleted</i> by the garbage collector mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers.
			 * Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and
			 * only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member
			 * function, we could apply some temporary properties (i.e. an instance of the mangrove_tds::PropertyBase class) for marking simplices. Surely, we assume to have the
			 * <i>assigned2cluster</i> local property (i.e. an instance of the mangrove_tds::LocalPropertyHandle class). You should be careful about what properties are used, because usually
			 * they are removed before starting a new computation. In any case, we remove them at the end of this member function.
			 * \param d the dimension of the required clusters
			 * \param clusters simplices forming the required clusters
			 * \param a2c a component for accessing the local property <i>assigned2cluster</i>
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::PropertyBase,
			 * mangrove_tds::LocalPropertyHandle, mangrove_tds::BaseSimplicialComplex::addLocalProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty()
			 */
			inline void findTopClusters(SIMPLEX_TYPE d,vector< vector<SimplexPointer> >& clusters, LocalPropertyHandle<bool> *a2c)
			{
				list<SimplexPointer> tops,l;
				SimplexPointer ni;

				/* Now, we identify all the top d-simplices */
				this->getTopSimplices(d,tops);
				for(list<SimplexPointer>::iterator it=tops.begin();it!=tops.end();it++)
				{
					/* Now, we check if the current top d-simplex is assigned! */
					if(a2c->get(SimplexPointer(*it))==false)
	 				{
	 					queue<SimplexPointer> q;
	 					
	 					/* Now, we visit a new cluster! */
	 					q.push(SimplexPointer(*it));
	 					clusters.push_back(vector<SimplexPointer>());
	 					while(q.empty()==false)
	 					{
	 						/* Now, we extract the first element from q */
							ni=q.front();
							q.pop();
							if(a2c->get(SimplexPointer(ni))==false)
							{
								clusters.back().push_back(SimplexPointer(ni));
								a2c->set(SimplexPointer(ni),true);
								this->adjacency(SimplexPointer(ni),&l);
								for(list<SimplexPointer>::iterator cp=l.begin();cp!=l.end();cp++)
								{
									/* Now, we check if the adjacent simplex has been assigned to a cluster! */
									if(a2c->get(SimplexPointer(*cp))==false) { if(this->isTop(SimplexPointer(*cp))) q.push(SimplexPointer(*cp)); }
								}
							}
	 					}
	 				}
				}
			}
			
			/// This member function retrieves clusters formed only by top simplices in the simplicial complex represented by the current data structure.
			/**
			 * This member function retrieves clusters formed only by top simplices in the simplicial complex represented by the current data structure: for us, a k-cluster is formed by
			 * k-simplices that are (k-1)-connected.<p>We assume all clusters are formed by <i>ghost</i> simplices, i.e. not directly stored in the current data structure. They are
			 * identified by instances of the mangrove_tds::GhostSimplexPointer class and thus we can invoke this member function only with a <i>local</i> data structure.<p>If we cannot
			 * complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation
			 * will result in a failed assert.<p><b>IMPORTANT:</b> in this member function, we could apply some temporary properties (i.e. an instance of the mangrove_tds::PropertyBase
			 * class) for marking simplices. In particular, we use the ghost property (i.e. an instance of the mangrove_tds::GhostPropertyHandle) with boolean values, called
			 * <i>assigned2cluster</i>, for marking d-simplices. You should be careful about what properties are used, because usually they are removed before starting a new computation. In
			 * any case, we remove them at the end of this member function.
			 * \param d the dimension of the required clusters
			 * \param clusters simplices forming the required clusters
			 * \param a2c a component for accessing the ghost property <i>assigned2cluster</i>
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::PropertyBase,
			 * mangrove_tds::GhostPropertyHandle, mangrove_tds::BaseSimplicialComplex::addGhostProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty(),
			 * mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer
			 */
			inline void findTopClusters(SIMPLEX_TYPE d,vector< vector<GhostSimplexPointer> >& clusters, GhostPropertyHandle<bool> *a2c)
			{
				list<SimplexPointer> tops;
				SimplexPointer ni;
				list<GhostSimplexPointer> l;
				GhostSimplex s;
				bool *b;

				/* Now, we identify all the top d-simplices */
				this->getTopSimplices(d,tops);
				for(list<SimplexPointer>::iterator it=tops.begin();it!=tops.end();it++)
				{
					/* Now, we check if the current top d-simplex is assigned! */
					this->ghostSimplex(GhostSimplexPointer(it->type(),it->id()),&s);
					if(a2c->get(s)==false)
	 				{
	 					queue<SimplexPointer> q;
	 					
	 					/* Now, we visit a new cluster! */
	 					q.push(SimplexPointer(*it));
	 					clusters.push_back(vector<GhostSimplexPointer>());
	 					while(q.empty()==false)
	 					{
	 						/* Now, we extract the first element from q */
							ni=q.front();
							q.pop();
							this->ghostSimplex(GhostSimplexPointer(ni.type(),ni.id()),&s);
							a2c->get(s,&b);
							if((*b)==false)
							{
								(*b)=true;
	 							clusters.back().push_back(GhostSimplexPointer(ni.type(),ni.id()));
								this->adjacency(GhostSimplexPointer(ni.type(),ni.id()),&l);
	 							for(list<GhostSimplexPointer>::iterator cp=l.begin();cp!=l.end();cp++)
									{ if(cp->isEncoded()) q.push(SimplexPointer(cp->getParentType(),cp->getParentId())); }
							}
	 					}
	 				}
				}
			}
 
 			/// This member function retrieves clusters in the simplicial complex represented by the current data structure.
			/**
			 * This member function retrieves clusters in the simplicial complex represented by the current data structuree: for us, a k-cluster is formed by k-simplices that are
			 * (k-1)-connected.<p>We assume all clusters are formed by <i>ghost</i> simplices, i.e. not directly stored in the current data structure. They are identified by instances of the
			 * mangrove_tds::GhostSimplexPointer class and thus we can invoke this member function only with a <i>local</i> data structure.<p>If we cannot complete this operation, then this
			 * member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p>
			 * <b>IMPORTANT:</b> in this member function, we could apply some temporary properties (i.e. an instance of the mangrove_tds::PropertyBase class) for marking simplices. In
			 * particular, we use the ghost property (i.e. an instance of the mangrove_tds::GhostPropertyHandle) with boolean values, called <i>assigned2cluster</i>, for marking d-simplices.
			 * You should be careful about what properties are used, because usually they are removed before starting a new computation. In any case, we remove them at the end of this member
			 * function.
			 * \param d the dimension of the required clusters
			 * \param clusters simplices forming the required clusters
			 * \param a2c a component for accessing the ghost property <i>assigned2cluster</i>
			 */
			inline void findNonTopClusters(SIMPLEX_TYPE d,vector< vector<GhostSimplexPointer> >& clusters, GhostPropertyHandle<bool> *a2c)
			{
				GS_COMPLEX gs;
				GS_COMPLEX::iterator it;
				GhostSimplexPointer ni;
				GhostSimplex s;
				bool *b;
				list<GhostSimplexPointer> l;

				/* First,we retrieve all d-simplices and then we explore them! */
				this->retrieveAllSimplices(d,gs);
				for(it=gs.begin();it!=gs.end();it++)
  				{
  					/* Now, we check if the current d-simplex is assigned! */
  					if(a2c->get(GhostSimplex(*it))==false)
  					{
  						queue<GhostSimplexPointer> q;
  						
  						/* Now, we visit a new cluster! */
	 					q.push(GhostSimplexPointer(it->getCGhostSimplexPointer()));
	 					clusters.push_back(vector<GhostSimplexPointer>());
	 					while(q.empty()==false)
	 					{
	 						/* Now, we extract the first element from q */
	 						ni=q.front();
							q.pop();
							this->ghostSimplex(GhostSimplexPointer(ni),&s);
							a2c->get(s,&b);
							if((*b)==false)
							{
								(*b)=true;
								clusters.back().push_back(GhostSimplexPointer(ni));
								this->adjacency(GhostSimplexPointer(ni),&l);
								for(list<GhostSimplexPointer>::iterator cp=l.begin();cp!=l.end();cp++) q.push(GhostSimplexPointer(*cp));
							}
	 					}
  					}
  				}
			}
			
			/// This member function expands the content of a cluster in the current simplicial complex.
			/**
			 * This member function expands the content of a cluster in the current simplicial complex, retrieving an indexed version. A cluster if formed by a list of simplices, thus we
			 * extract a local list of its vertices and then we describe each simplex as a list of indices over the local list of vertices. Euclidean coordinates are required and the input
			 * simplicial complex must be a 3-dimensional one, at most.<p>Through such information, we can provide a representation of a cluster, suitable to be used with the
			 * <i><A href=http://www.disi.unige.it/person/PanozzoD/mc/>Graph-Complex Viewer</A></i>, a tool for visualizing a decomposition of a simplicial complex and its decomposition
			 * graph. You should refer the official documentation of this tool for complete guidelines to be satisfied.<p><b>IMPORTANT:</b> we assume all clusters are formed by <i>ghost</i> 
			 * simplices, i.e. not directly stored in the current data structure. They are identified by instances of the mangrove_tds::GhostSimplexPointer class and thus we can invoke this
			 * member function only with a <i>local</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \param clust all ghost simplices describing the input cluster
	 		 * \param pts all the points in the input cluster
	 		 * \param inds the indexed version of simplices covering the input cluster
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::Point, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::GhostSimplexPointer
			 */
			inline void expandCluster(vector<GhostSimplexPointer> &clust, vector< Point<double> >&pts, vector< vector<SIMPLEX_ID> > &inds)
			{
				vector< vector<SIMPLEX_ID> > simplexes;
				GhostSimplex s;
				unsigned int k;
				map<SIMPLEX_ID,SIMPLEX_ID> vett;
				double x,y,z;
				Point<double> p;
				set<SIMPLEX_ID> vs;
				SIMPLEX_ID vind;

				/* First, we check if all is ok! */
				assert(clust.size()!=0);
				pts.clear();
				for(SIMPLEX_TYPE d=0;d<=3;d++) inds[d].clear();
				for(SIMPLEX_TYPE d=0;d<=3;d++) simplexes.push_back( vector<SIMPLEX_ID>() );
				for(vector<GhostSimplexPointer>::iterator cp=clust.begin();cp!=clust.end();cp++)
				{
					/* Now, we analyze the current simplex 'cp' in the cluster! */
					this->ghostSimplex(GhostSimplexPointer(*cp),&s);
					if(cp->getChildType()==0) vs.insert(s.getCBoundary()[0].id());
					else
					{
						/* The current simplex 'cp' is not a vertex! */
						for(k=0;k<=cp->getChildType();k++)
						{
							vs.insert(s.getCBoundary()[k].id());
							simplexes[cp->getChildType()].push_back(s.getCBoundary()[k].id());
						}				
					}
				}
				
				/* If we arrive here, we can retrieve information about the vertices! */
				vind=0;
				for(set<SIMPLEX_ID>::iterator it=vs.begin();it!=vs.end();it++)
				{
					x=this->getPropertyValue( string(this->prop4coords),SimplexPointer(0,(*it)),p).x();
					y=this->getPropertyValue( string(this->prop4coords),SimplexPointer(0,(*it)),p).y();
					z=this->getPropertyValue( string(this->prop4coords),SimplexPointer(0,(*it)),p).z();
					pts.push_back(Point<double>(x,y,z));
					vett[ (*it) ] = vind;
					vind = vind+1;
				}
			
				/* If we arrive here, we can retrieve local (new) indices for simplices descriptions */
				for(SIMPLEX_TYPE d=0;d<=3;d++)
					{ for(vector<SIMPLEX_ID>::iterator it=simplexes[d].begin();it!=simplexes[d].end();it++) inds[d].push_back(vett[(*it)]); }
			}

			/// This member function expands the content of a cluster in the current simplicial complex.
			/**
	 		 * This member function expands the content of a cluster in the current simplicial complex, retrieving an indexed version. A cluster if formed by a list of simplices, thus we
			 * extract a local list of its vertices and then we describe each simplex as a list of indices over the local list of vertices. Euclidean coordinates are required and the input
			 * simplicial complex must be a 3-dimensional one, at most.<p>Through such information, we can provide a representation of a cluster, suitable to be used with the
	 		 * <i><A href=http://www.disi.unige.it/person/PanozzoD/mc/>Graph-Complex Viewer</A></i>, a tool for visualizing a decomposition of a simplicial complex and its decomposition
	 		 * graph. You should refer the official documentation of this tool for complete guidelines to be satisfied.<p><b>IMPORTANT:</b> we assume all clusters are formed by <i>valid</i>
	 		 * simplices, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They are identified by
	 		 * instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data
	 		 * structure.<p>If we cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case,
			 * each forbidden operation will result in a failed assert.
	 		 * \param clust all simplices describing the input cluster
	 		 * \param pts all the points in the input cluster
	 		 * \param inds the indexed version of simplices covering the input cluster
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::Point, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID
	 		 */
			inline void expandCluster(vector<SimplexPointer> &clust, vector< Point<double> > &pts,vector< vector<SIMPLEX_ID> > &inds)
			{
				double x,y,z;
				SIMPLEX_ID vind;
				Point<double> p;
				vector< vector<SIMPLEX_ID> > simplexes;
				set<SIMPLEX_ID> vs;
				map<SIMPLEX_ID,SIMPLEX_ID> vett;
				list<SimplexPointer> bk;

				/* First, we check if all is ok! */
				assert(clust.size()!=0);
				pts.clear();
				for(SIMPLEX_TYPE d=0;d<=3;d++) inds[d].clear();
				for(SIMPLEX_TYPE d=0;d<=3;d++) simplexes.push_back( vector<SIMPLEX_ID>() );
				for(vector<SimplexPointer>::iterator cp=clust.begin();cp!=clust.end();cp++)
				{
					/* Now, we analyze the current cell 'cp' in the cluster! */
					if(cp->type()==0) vs.insert(cp->id());
					else
					{
						/* The current cell 'cp' is not a vertex! */
						this->boundaryk(SimplexPointer(*cp),0,&bk);
						for(list<SimplexPointer>::iterator current=bk.begin();current!=bk.end();current++)
						{
							vs.insert(current->id());
							simplexes[cp->type()].push_back(current->id());
						}
					}
				}
				
				/* If we arrive here, we can retrieve information about the vertices! */
				vind=0;
				for(set<SIMPLEX_ID>::iterator it=vs.begin();it!=vs.end();it++)
				{
					x=this->getPropertyValue( string(this->prop4coords),SimplexPointer(0,(*it)),p).x();
					y=this->getPropertyValue( string(this->prop4coords),SimplexPointer(0,(*it)),p).y();
					z=this->getPropertyValue( string(this->prop4coords),SimplexPointer(0,(*it)),p).z();
					pts.push_back(Point<double>(x,y,z));
					vett[ (*it) ] = vind;
					vind = vind+1;
				}
			
				/* If we arrive here, we can retrieve local (new) indices for cells descriptions */
				for(SIMPLEX_TYPE d=0;d<=3;d++)
					{ for(vector<SIMPLEX_ID>::iterator it=simplexes[d].begin();it!=simplexes[d].end();it++) inds[d].push_back(vett[(*it)]); }
			}
		};
		
		/// This function extracts all the top simplices from a list of pointers to simplices directly encoded in a data structure.
		/**
		 * This function extracts all the top simplices from a list of pointers to simplices directly encoded in a data structure: such simplices are encoded and accessible
		 * through an instance of the mangrove_tds::SimplexPointer class.
		 * \param src the input list of simplex pointers
		 * \param dst the required top simplices
		 * \param  ccw the reference simplicial complex
		 * \see mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex, mangrove_tds::BaseSimplicialComplex::isTop(), mangrove_tds::GhostSimplexPointer,
		 * mangrove_tds::RawFace, mangrove_tds::Simplex
	 	 */
		template <class T> void extractTopSimplices( list<SimplexPointer> &src, vector<SimplexPointer> &dst, BaseSimplicialComplex<T> *ccw)
	 	{
	 		SIMPLEX_TYPE t;
	 		list<SimplexPointer>::iterator it;
	 		
	 		/* First, we check if all is ok! */
	 		assert(ccw!=NULL);
	 		t = ccw->type();
	 		dst.clear();
	 		for(it=src.begin();it!=src.end();it++) { if(ccw->isTop(SimplexPointer(*it))) dst.push_back(SimplexPointer(*it)); }
	 	}
	 	
	 	/// This function checks if two lists of ghost simplices contain the same elements.
		/**
		 * This function checks if two lists of ghost simplices contain the same elements.
		 * \param la the first list of ghost simplices to be compared
		 * \param a the data structure related to the first list of ghost simplices to be compared
		 * \param lb the second list of ghost simplices to be compared
		 * \param b the data structure related to the second list of ghost simplices to be compared
		 * \return <ul><li><i>true</i>, if the input list of ghost simplices are the same</li><li><i>false</i>, otherwise</li></ul>
		 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::BaseSimplicialComplex
		 */
		template <class U,class V> bool same(list<GhostSimplexPointer> &la,BaseSimplicialComplex<U> *a,list<GhostSimplexPointer> &lb,BaseSimplicialComplex<V> *b)
		{
			GS_COMPLEX sa,sb;
			GhostSimplex s;
			GS_COMPLEX::iterator sa_it,sb_it;

			/* First, we check if all is ok, and then we convert 'la' in a set of ghost simplices */
			assert(a!=NULL);
			assert(b!=NULL);
			assert(a->encodeAllSimplices()==false);
			assert(b->encodeAllSimplices()==false);
			for(list<GhostSimplexPointer>::iterator it=la.begin();it!=la.end();it++)
			{
				a->ghostSimplex(GhostSimplexPointer(*it),&s);
				sa.insert(GhostSimplex(s));
			}
			
			/* Now, we convert 'lb' in a set of ghost simplices */
			for(list<GhostSimplexPointer>::iterator it=lb.begin();it!=lb.end();it++)
			{
				b->ghostSimplex(GhostSimplexPointer(*it),&s);
				sb.insert(GhostSimplex(s));
			}

			/* We need the same number of elements */
			if(sa.size()!=sb.size()) return false;
			else
			{
				/* Now, we check the elements in 'sa' and 'sb' */
				sa_it=sa.begin();
				for(sb_it=sb.begin();sb_it!=sb.end();sb_it++,sa_it++) { if(sa_it->theSame(GhostSimplex(*sb_it))==false) return false; }
				return true;
			}
		}
		
		/// This member function retrieves the number of simplices in the star of a vertex in a local data structure.
		/**
		 * This member function retrieves the number of simplices in the star of a vertex in a local data structure without any optimization. Recall that only a subset of simplices is directly
		 * encoded in a <i>local</i> data structure<p>In this case, we visit all simplices in the input data structure, identifying which simplices are incident in the input data structure.
		 * \param cp a pointer to the input vertex
		 * \param ccw the local data structure to be used
		 * \param ll a list containing the required ghost simplices
		 * \return the number of simplices in the star of a vertex in the input data structure
		 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::SimplexPointer, mangrove_tds::GS_COMPLEX, mangrove_tds::GhostSimplex
		 */
		template <class T> unsigned int starFromScratch(const SimplexPointer& cp, BaseSimplicialComplex<T> *ccw,list<GhostSimplexPointer> *ll)
		{
			unsigned int n;
			GS_COMPLEX gs;
			GS_COMPLEX::iterator it;

			/* First, we check is all is ok */
			assert(ccw!=NULL);
			assert(ccw->encodeAllSimplices()==false);
			assert(cp.type()==0);
			assert(ll!=NULL);
			n=0;
			ll->clear();
			for(SIMPLEX_TYPE d=1;d<=ccw->type();d++)
			{
				ccw->retrieveAllSimplices(d,gs);
				for(it=gs.begin();it!=gs.end();it++)
				{
					if(it->isIncident(SimplexPointer(cp))==true)
					{
						n=n+1;
						ll->push_back(GhostSimplexPointer(it->getCGhostSimplexPointer()));
					}
				}
			}
			
			/* Now, we have finished! */
			return n;
		}
		
		/// This member function retrieves the number of simplices of a certain dimension in the star of a vertex in a local data structure.
		/**
		 * This member function retrieves the number of simplices of a certain dimension in the star of a vertex in a local data structure without any optimization. Recall that only a subset of
		 * simplices is directly encoded in a <i>local</i> data structure<p>In this case, we visit all simplices in the input data structure, identifying which simplices are incident in the
		 * input data structure.
		 * \param cp a pointer to the input vertex
		 * \param ccw the local data structure to be used
		 * \param k the dimension of the required simplices
		 * \param ll a list containing the required ghost simplices
		 * \return the number of simplices in the star of a vertex in the input data structure
		 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::SimplexPointer, mangrove_tds::GS_COMPLEX, mangrove_tds::GhostSimplex, mangrove_tds::SIMPLEX_TYPE
		 */
		template <class T> unsigned int starFromScratch(const SimplexPointer& cp, BaseSimplicialComplex<T> *ccw,SIMPLEX_TYPE k,list<GhostSimplexPointer> *ll)
		{
			unsigned int n;
			GS_COMPLEX gs;
			GS_COMPLEX::iterator it;

			/* First, we check is all is ok */
			assert(ccw!=NULL);
			assert(ccw->encodeAllSimplices()==false);
			assert(cp.type()==0);
			assert(ll!=NULL);
			n=0;
			ll->clear();
			ccw->retrieveAllSimplices(k,gs);
			for(it=gs.begin();it!=gs.end();it++)
			{
				if(it->isIncident(SimplexPointer(cp))==true)
				{
					n=n+1;
					ll->push_back(GhostSimplexPointer(it->getCGhostSimplexPointer()));
				}
			}
			
			/* Now, we have finished! */
			return n;
		}
	}
	 
#endif

