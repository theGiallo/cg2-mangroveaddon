/*********************************************************************************************************************
 * This source code belongs to the Mangrove TDS Library                                     
 *  		    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * October 2011 (Revised on May 2012)
 *                                                               
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * SIGManager.h - the specialized component for executing I/O operations on the Simplified Incidence Graph
 ********************************************************************************************************************/

/* Should we include this header file? */
#ifndef SIG_MANAGER_H

	#define SIG_MANAGER_H

	#include "SIG.h"
	#include "Miscellanea.h"
	#include "IGManager.h"
	#include <list>
	#include <map>
	#include <queue>
	#include <algorithm>
	#include <cstdlib>
	using namespace std;
	using namespace mangrove_tds;
	
	/// The specialized component for executing I/O operations on the <i>Simplified Incidence Graph</i>.
	/**
	 * The class defined in this file allows to execute I/O operations on the <i>Simplified Incidence Graph</i>.<p>The <i>Simplified Incidence Graph</i>, described by the
	 * mangrove_tds::SIG class, is a dimension-independent and incidence-based data structure for representing simplificial complexes with an arbitrary domain. This data structure encodes all
	 * simplices in a simplicial complex, thus it is a <i>global</i> data structure. For each k-simplex, it encodes:<ul><li>all the k+1 simplices of dimension k-1 belonging to its boundary;</li>
	 * <li>one top j-simplex (with j>k) for each j-cluster in its star.</li></ul><p>The effective I/O operations on the <i>Simplified Incidence Graph</i> are delegated to a particular instance of the
	 * mangrove_tds::DataManager class in order to support a wide range of I/O formats.
	 * \file SIGManager.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	
	/* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		/// The specialized component for executing I/O operations on the <i>Simplified Incidence Graph</i>.
		/**
	 	 * The class defined in this file allows to execute I/O operations on the <i>Simplified Incidence Graph</i>.<p>The <i>Simplified Incidence Graph</i>, described by the
	 	 * mangrove_tds::SIG class, is a dimension-independent data structure for representing simplificial complexes with an arbitrary domain. This data structure encodes all simplices in a
	 	 * simplicial complex, thus it is a <i>global</i> data structure. For each k-simplex, it encodes:<ul><li>all the k+1 simplices of dimension k-1 belonging to its boundary;</li><li>one
	 	 * top j-simplex (with j>k) for each j-cluster in its star.</li></ul><p>The effective I/O operations on the <i>Simplified Incidence Graph</i> are delegated to a particular instance of the
	 	 * mangrove_tds::DataManager class in order to support a wide range of I/O formats.
	 	 * \see mangrove_tds::DataManager, mangrove_tds::IO, mangrove_tds::SIG, mangrove_tds::IG, mangrove_tds::IGManager, mangrove_tds::BaseSimplicialComplex
	 	 */
	 	class SIGManager : public IO<SIGManager>
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a not initialized instance of this class: you should apply the mangrove_tds::SIGManager::createComplex() or
			 * mangrove_tds::SIGManager::createFromIG() member functions in order to initialize it.
			 * \see mangrove_tds::SIGManager::createComplex(), mangrove_tds::SIGManager::createFromIG(), mangrove_tds::SIG
			 */
			inline SIGManager() : IO<SIGManager>() { this->ccw = NULL; }
			
			/// This member function destroys an instance of this class.
			/**
			 * The specialized data structure (i.e. the <i>Simplified Incidence Graph</i>, namely a new instance of the mangrove_tds::SIG class) for the simplicial complex is created in the
			 * mangrove_tds::SIGManager::createComplex() or in the mangrove_tds::SIGManager::createFromIG() member functions, thus you must not manually destroy it: you should apply this
			 * destructor or the mangrove_tds::SIGManager::close() member function in order to destroy all the internal state.
			 * \see mangrove_tds::SIGManager::createComplex(), mangrove_tds::SIGManager::createFromIG(), mangrove_tds::SIGManager::close(), mangrove_tds::SIG
			 */
			inline virtual ~SIGManager() { this->close(); }
			
			/// This member function destroys all the internal state in this component.
			/**
			 * This member function destroys all the internal state in this component. The specialized data structure  (i.e. the <i>Simplified Incidence Graph</i>, namely a new instance of the
			 * mangrove_tds::SIG class) for the simplicial complex is created in the mangrove_tds::SIGManager::createComplex() or in the mangrove_tds::SIGManager::createFromIG() member
			 * functions, thus you must not manually destroy it: you should apply the destructor or this member function
			 * in order to destroy all the internal state.
			 * \see mangrove_tds::SIGManager::createComplex(), mangrove_tds::SIGManager::createFromIG(), mangrove_tds::SIG
			 */
			inline void close()
			{
				/* Now, we reset the current state! */
				if(this->ccw!=NULL) delete this->ccw;
				this->ccw=NULL;
			}
			
			/// This member function returns the simplicial complex created by the current component.
			/**
			 * This member function returns the specialized data structure used for encoding the simplicial complex managed in the current component. The specialized data structure (i.e. the
			 * <i>Simplified Incidence Graph</i>, namely a new instance of the mangrove_tds::SIG class) for the simplicial complex is created in the
			 * mangrove_tds::SIGManager::createComplex() or in the mangrove_tds::SIGManager::createFromIG() member functions, thus you must not manually destroy it. You should apply the
			 * destructor or the mangrove_tds::SIGManager::close() member function in order to destroy all the internal state. Thus, the input pointer must be a C++ pointer to an instance of
			 * the mangrove_tds::SIG class.<p>If such conditions are not satisfied, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug
			 * mode. In this case, each forbidden operation will result in a failed assert.
			 * \param cs the simplicial complex created by this component
			 * \see mangrove_tds::SIGManager::close(), mangrove_tds::SIGManager::createComplex(), mangrove_tds::SIGManager::createFromIG(), mangrove_tds::SIG
			 */
			template <class D> void getComplex(D** cs)
			{
				D *aux;
				
				/* First, we check if all is ok! */
				assert(this->ccw!=NULL);
				aux = dynamic_cast<D*>(this->ccw);
				assert(aux!=NULL);
				(*cs) = this->ccw;
			}
			
			/// This member function returns a string that describes the data structure created by the current I/O component.
 			/**
 			 * This member function returns a string that describes the data structure created by the current I/O component, and used for encoding the current simplicial complex.
			 * \return a string that describes the data structure created by the current I/O component
 			 * \see mangrove_tds::BaseSimplicialComplex::getDataStructureName()
 			 */
 			inline string getDataStructureName() { return string("Simplified Incidence Graph (SIG)"); }
 			
 			/// This member function creates a simplicial complex by reading it from a file.
			/**
			 * This member function generates a <i>Simplified Incidence Graph</i> from a file that contains a possible representation: the most common exchange format for a simplicial
			 * complex consists of a collection of <i>top</i> simplices described by their vertices, but it is not the unique possibility.<p>The <i>Simplified Incidence Graph</i>, described
			 * by the mangrove_tds::SIG class, is a dimension-independent data structure for representing simplificial complexes with an arbitrary domain. This data structure encodes all the
			 * simplices in a simplicial complex, thus it is a <i>global</i> data structure. For each k-simplex, it encodes:<ul><li>all the k+1 simplices of dimension k-1 belonging to its
			 * boundary;</li><li>one top j-simplex (with j>k) for each j-cluster in its star.</li></ul>The effective I/O operations on the <i>Simplified Incidence Graph</i> are delegated to a
			 * particular instance of the mangrove_tds::DataManager class in order to support a wide range of I/O formats.<p>Consequently, if the input contains a soup of top simplices, then
			 * we need to generate all simplices and then to establish all the topological relations among them.<p>In this member function we can customize the required simplicial complex:
			 * for instance, we can attach the Euclidean coordinates and the field value for each vertex in the simplicial complex to be built (if supported in the I/O component). Such
			 * information is stored as local properties (i.e. as subclasses of the mangrove_tds::LocalPropertyHandle class) associated to vertices. You can retrieve them through their names,
			 * respectively provided by the mangrove_tds::DataManager::getPropertyName4Coordinates() and mangrove_tds::DataManager::getPropertyName4FieldValue() member function. Moreover, you
			 * can check what properties are supported through the mangrove_tds::DataManager::supportsEuclideanCoordinates() and mangrove_tds::DataManager::supportsFieldValues() member
			 * functions.<p>If we cannot complete these operations for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug
			 * mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> the specialized data structure for the simplicial complex is created in this
			 * member function or in the mangrove_tds::SIGManager::createFromIG() member funciton , thus you must not manually destroy it. You should apply the destructor or
			 * mangrove_tds::SIGManager::close() member function in order to destroy all the internal state.
			 * \param loader a component for the effective data reading
	 	 	 * \param fname the path of the input file
			 * \param reqParams this boolean flag identfies if the loading of the auxiliary parameters encoded in the input file is enabled
			 * \param ld this boolean flag indicates if the effective reading of the input data must be performed
			 * \param checksOn this boolean flag indicates if the effective checks on the input data must be performed (valid only if the effective reading must be performed)
			 * \see mangrove_tds::DataManager, mangrove_tds::SIGManager::close(), mangrove_tds::SIGManager::createFromIG(), mangrove_tds::BaseSimplicialComplex, mangrove_tds::IG,
			 * mangrove_tds::SIG, mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::RawFace, mangrove_tds::RawIncidence, mangrove_tds::RawAdjacency,
			 * mangrove_tds::SIGManager::addEdge(), mangrove_tds::SIGManager::getUnique(), mangrove_tds::SIGManager::computeAdjacency()
			 */
			template <class T> void createComplex(DataManager<T> *loader, string fname,bool reqParams,bool ld,bool checksOn)
			{
				bool b;
				LocalPropertyHandle<double> *fvals_prop;
				LocalPropertyHandle< Point<double> > *pnts_prop;
				double aaa;
				unsigned int k,uik,rik,ik;
				SimplexIterator it;
				Point<double> p;
				SimplexPointer cp;
				vector< vector<RawIncidence> > inc;
				vector< vector<RawFace> > raw_faces;
				vector< vector<RawAdjacency> > raw_ad;
				list<SimplexPointer> bnd;
				list<SimplexPointer>::iterator bnd_it;
				SIMPLEX_ID cip;
				
				/* First, we check if all is ok and then we run the effective construction! */
				assert(loader!=NULL);
				this->close();
				if(ld) loader->loadData(fname,reqParams,true,checksOn);
				b=(loader->supportsEuclideanCoordinates()==true) || (loader->supportsFieldValues()==true);
				if(reqParams) assert(b==true);
				try { this->ccw = new SIG(loader->getDimension()); }
				catch(bad_alloc ba) { this->ccw = NULL; }
				assert(this->ccw!=NULL);
				aaa=0.0;
				pnts_prop=NULL;
				fvals_prop=NULL;
				if(reqParams)
				{
					/* What properties must be added? First, we check the Euclidean coordinates */
					if(loader->supportsEuclideanCoordinates())
					{
						pnts_prop=this->ccw->addLocalProperty(0,string(loader->getPropertyName4Coordinates()), Point<double>());
						this->ccw->getPropertyName4Coordinates()=string(loader->getPropertyName4Coordinates());
					}
					
					/* Now, we check the field values! */
					if(loader->supportsFieldValues())
					{
						fvals_prop=this->ccw->addLocalProperty(0,string(loader->getPropertyName4FieldValue()),aaa); 
						this->ccw->getPropertyName4FieldValue()=string(loader->getPropertyName4FieldValue());
					}
				}
				
				/* Now, we can add all the vertices and their properties! */
				for(SIMPLEX_TYPE d=0;d<=loader->getDimension();d++) inc.push_back(vector<RawIncidence>());
				for(SIMPLEX_TYPE d=0;d<loader->getDimension();d++) raw_faces.push_back(vector<RawFace>());
				for(SIMPLEX_TYPE d=0;d<loader->getDimension();d++) raw_ad.push_back(vector<RawAdjacency>());
				it = loader->getCollection(0).begin();
				k=0;
				while(it!=loader->getCollection(0).end())
				{
					/* Now, we insert the k-th vertex in 'ccw' */
					this->ccw->addSimplex(cp);
					inc[0].push_back(RawIncidence());
					if(reqParams)
					{
						/* Now, we retrieve the Euclidean coordinates, if supported! */						
						if(loader->supportsEuclideanCoordinates())
						{
							loader->getCoordinates(k,p);
							pnts_prop->set(SimplexPointer(cp),Point<double>(p));
						}
						
						/* Now, we retrieve the field values, if supported! */
						if(loader->supportsFieldValues())
						{
							loader->getFieldValue(k,aaa);
							fvals_prop->set(SimplexPointer(cp),aaa);
						}
					}
					
					/* Now, we move on the next vertex! */
					k=k+1;
					it++;
				}
				
				/* Now, we must eliminate and create the subsimplices of the top simplices. Then, we must understand how connecting the top simplices! */
				if(loader->getDimension()==0) return;
				for(SIMPLEX_TYPE d=1;d<loader->getRawFaces().size();d++) { this->getUnique(d,loader,inc,raw_ad); }
				for(it=loader->getCollection(1).begin(); it!=loader->getCollection(1).end();it++)
				{
					cip=this->addEdge((*it).bc(0).id(),(*it).bc(1).id());
					inc[1].push_back(RawIncidence());
					inc[0][(*it).bc(0).id()].addSimplex(SimplexPointer(1,cip));
					inc[0][(*it).bc(1).id()].addSimplex(SimplexPointer(1,cip));
				}
				
				/* Now, we proceed on the other top simplices! */
				for(SIMPLEX_TYPE d=loader->getDimension();d>=2;d--)
				{
					/* First, we check if there are any top simplices of dimension d */
					if( loader->getCollection(d).size()!=0)
					{
						/* If we arrive here, there are certainly simplices of dimension d-1 that are on these simplices boundary! */
						for(k=0;k<loader->getRawFaces()[d-1].size();k++)
						{
							/* Now, we check if the k-th simplex is a child of a top simplex! */
							if(loader->getRawFaces()[d-1].at(k).isTopSimplexChild())
							{
								uik = loader->getRawFaces()[d-1].at(k).getUniqueIndex();
								ik = loader->getRawFaces()[d-1].at(k).getIndex();
								rik = loader->getRawFaces()[d-1].at(k).getReverseIndex();
								loader->getCollection(d).simplex(ik).b(rik).setId(uik);
								loader->getCollection(d).simplex(ik).b(rik).setType(d-1);								
							}
						}
						
						/* Now, we add all the top simplices of dimension d to ccw */
						for(it=loader->getCollection(d).begin();it!=loader->getCollection(d).end();it++)
						{
							/* Now, we create a new simplex with its boundary! */
							this->ccw->addSimplex(d,cp);
							for(unsigned int l=0;l<=d;l++) 
							{
								this->ccw->simplex(cp).b(l).setId((*it).b(l).id());
								this->ccw->simplex(cp).b(l).setType((*it).b(l).type());
								raw_faces[d-1].push_back(RawFace());
								raw_faces[d-1].back().isTopSimplexChild()=true;
								raw_faces[d-1].back().getIndex()=cp.id();
								raw_faces[d-1].back().getReverseIndex()=l;
								raw_faces[d-1].back().getOriginalPosition()=raw_faces[d-1].size()-1;
								raw_faces[d-1].back().getSharedFace()=SimplexPointer((*it).b(l));
								raw_faces[d-1].back().getUniqueIndex()=(*it).b(l).id();
							}
							
							/* Now, we extract the boundary of the new top simplex 'cp' */
							raw_ad[d-1].push_back(RawAdjacency(d));
							inc[d].push_back(RawIncidence());
							this->ccw->boundary(SimplexPointer(cp),&bnd);
							for(bnd_it=bnd.begin();bnd_it!=bnd.end();bnd_it++) { inc[bnd_it->type()][bnd_it->id()].addSimplex(SimplexPointer(cp)); }
						}
					}
				}
				
				/* Now, we identify the partial co-boundary relation! */
				this->computeAdjacency(raw_faces,raw_ad);
				raw_faces.clear();
				for(SIMPLEX_TYPE d=0;d<this->ccw->type();d++)
					{ for(it=this->ccw->begin(d);it!=this->ccw->end(d);it++) this->computeCoboundary(inc,raw_ad,SimplexPointer(it.getPointer())); }
			}

			/// This member function exports the current simplicial complex in accordance with a certain format.
			/**
			 * This member function exports the current simplicial complex, by writing its representation in accordance with a specific file format.<p>The effective writing is delegated to a
			 * subclass of the mangrove_tds::DataManager class and some further conditions could be checked and verified: for example, the current simplicial complex must support the
			 * extraction of particular auxiliary information (like the Euclidean coordinates and/or scalar field values, described by subclasses of the mangrove_tds::PropertyBase class). If
			 * such properties are not satisfied, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.
			 * \param writer a component for the effective data writing (we write the data in this member function)
			 * \param fname the path of the output file
			 * \see mangrove_tds::DataManager, mangrove_tds::IO::close(), mangrove_tds::BaseSimplicialComplex, mangrove_tds::PropertyBase
			 */
			template <class T> void writeComplex(DataManager<T> *writer,string fname)
			{
				/* First, we check if all is ok! */
				assert(writer!=NULL);
				writer->writeData(this->ccw,fname); 
			}
			
			/// This member function creates a simplicial complex by starting from an <i>Incidence Graph</i>.
			/**
			 * This member function allows to generate an <i>Incidence Simplicial</i> data structure for encoding a simplicial complex by starting from an instance of the
			 * <i>Incidence Graph</i> (described in the mangrove_tds::IG class). First, we copy all the boundaries of the simplices (see the mangrove_tds::SIGManager::copySimplices() member
			 * function) and then we modify the complete coboundary relation in order to obtain the partial coboundary relation for each simplex (see the
			 * mangrove_tds::SIGManager::computeCoboundary() member function).<p>In this member function we can customize the required simplicial complex: for instance, we can attach
			 * the Euclidean coordinates and the field value for each vertex in the simplicial complex to be built (if supported). Such information is stored as instance of local properties
			 * (i.e. as subclasses of the mangrove_tds::LocalPropertyHandle class) associated to vertices. You can retrieve them through their names, respectively provided by the
			 * mangrove_tds::IG::getPropertyName4Coordinates() and mangrove_tds::IG::getPropertyName4FieldValue() member functions. Moreover, you can check what properties are
			 * supported through the mangrove_tds::IG::supportsEuclideanCoordinates() and mangrove_tds::IG::supportsFieldValues() member functions.<p><b>IMPORTANT:</b> the
			 * specialized data structure for the simplicial complex is created in this member function, thus you must not manually destroy it. You should apply the destructor or
			 * mangrove_tds::IO::close() member function in order to destroy all the internal state.<p>Moreover, in this member function, we use two global properties with boolean values,
			 * called <i>assigned</i> and <i>candidates</i>, for identifying clusters of simplices in the star of simplices. We remove such properties, if they already belong to the input
			 * <i>Incidence Graph</i>. In any case, we remove them at the end of computation.<p>If we cannot complete this operation for any reason, then this member function will fail if and
			 * only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param ig the input <i>Incidence Graph</i> to be analyzed
			 * \param reqParams this boolean flag identfies if the loading of the auxiliary parameters encoded in the input <i>Incidence Graph</i> is enabled
			 * \see mangrove_tds::SIGManager::close(), mangrove_tds::SIGManager::createComplex(), mangrove_tds::SIGManager::copySimplices(), mangrove_tds::SIG, mangrove_tds::IG,
			 * mangrove_tds::SIGManager::computeCoboundary(), mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex, mangrove_tds::GlobalPropertyHandle,
			 * mangrove_tds::LocalPropertyHandle, mangrove_tds::IG::getPropertyName4Coordinates(), mangrove_tds::IG::getPropertyName4FieldValue(),
			 * mangrove_tds::IG::supportsEuclideanCoordinates(), mangrove_tds::IG::supportsFieldValues()
			 */
			inline void createFromIG(IG *ig,bool reqParams)
			{
				bool b;

				/* First, we check if all is ok! */
				assert(ig!=NULL);
				this->close();
				b=(ig->supportsEuclideanCoordinates()==true) || (ig->supportsFieldValues()==true);
				if(reqParams) assert(b==true);
				try { this->ccw = new SIG(ig->type()); }
				catch(bad_alloc ba) { this->ccw = NULL; }
				assert(this->ccw!=NULL);
				this->copySimplices(ig,reqParams);
				this->computeCoboundary(ig);
			}
			
			protected:
			
			/// The simplicial complex created by this component.
			/**
			 * \see mangrove_tds::SIG
			 */
			SIG* ccw;
			
			/// This member functions add a new edge to the current simplicial complex.
			/**
			 * This member functions add a new edge to the current data structure: an edge is completely defined by its two vertices, that we assume already stored in the current data
			 * structure.
			 * \param v0 the identifier of the first vertex for the new edge to be added to the current simplicial complex
			 * \param v1 the identifier of the second vertex for the new edge to be added to the current simplicial complex
			 * \return the identifier of the new edge added to the current simplicial complex
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SIGManager::getUnique(), mangrove_tds::SimplexPointer, mangrove_tds::SIG, mangrove_tds::BaseSimplicialComplex
			 */
			inline SIMPLEX_ID addEdge(SIMPLEX_ID v0,SIMPLEX_ID v1)
			{
				SimplexPointer cp;
			
				/* First, we add a new edge and then return its new identifier! */
				this->ccw->addSimplex(SimplexPointer(0,v0),SimplexPointer(0,v1),cp);
				return cp.id();
			}
			
			/// This member function copies all the simplices from an <i>Incidence Graph</i> to the current <i>Simplified Incidence Graph</i>.
			/**
			 * This member function copies all the simplices from an <i>Incidence Graph</i> (described through an instance of the mangrove_tds::IG class) to the current
			 * <i>Simplified Incidence Graph</i> (described through an instance of the mangrove_tds::SIG class). It is mandatory to copy only the simplices and their boundaries, while the
			 * partial coboundary relation is computed by the mangrove_tds::SIGManager::computeCoboundary() starting from the input <i>Incidence Graph</i>.<p>If it is required, we can also
			 * copy auxiliary information (Euclidean coordinates and field values) associated to the input <i>Incidence Graph</i>: such properties are reachable through their names, i.e. the
			 * same names in the input <i>Incidence Graph</i>. Their names are respectively provided by the mangrove_tds::BaseSimplicialComplex::getPropertyName4Coordinates() and
			 * mangrove_tds::BaseSimplicialComplex::getPropertyName4FieldValue() member functions.
			 * \param ig the input <i>Incidence Graph</i> to be copied
 			 * \param reqParams this boolean flag identfies if the loading of the auxiliary parameters encoded in the input Incidence Graph is enabled
 			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::Point, mangrove_tds::SimplexIterator, mangrove_tds::Simplex,
 			 * mangrove_tds::SimplexIterator, mangrove_tds::SIGManager::computeCoboundary(), mangrove_tds::IG, mangrove_tds::SIG,
 			 * mangrove_tds::BaseSimplicialComplex::getPropertyName4Coordinates(), mangrove_tds::BaseSimplicialComplex::getPropertyName4FieldValue()
			 */
			inline void copySimplices(IG *ig,bool reqParams)
			{
				LocalPropertyHandle< Point<double> > *pnt_prop;
				LocalPropertyHandle<double> *fld_prop;
				double aaa,f;
				SimplexIterator it;
				SimplexPointer cp;
				Point<double> p,a;
				
				/* First, we add all the local properties, if required, for storing Euclidean coordinates and field values! */
				aaa=0.0;
				pnt_prop=NULL;
				fld_prop=NULL;
				if(reqParams)
				{
					/* Now, we must check if the Euclidean coordinates are supported! */
					if(ig->supportsEuclideanCoordinates()==true)
					{
						pnt_prop=this->ccw->addLocalProperty(0,string(ig->getPropertyName4Coordinates()), Point<double>());
						this->ccw->getPropertyName4Coordinates()=string(ig->getPropertyName4Coordinates());
					}
					
					/* Now, we must check if the field values are supported! */
					if(ig->supportsFieldValues()==true)
					{
						fld_prop=this->ccw->addLocalProperty(0,string(ig->getPropertyName4FieldValue()),aaa); 
						this->ccw->getPropertyName4FieldValue()=string(ig->getPropertyName4FieldValue());
					}
				}
				
				/* Now, we copy all vertices! */
				for(it=ig->begin(0);it!=ig->end(0); it++)
				{
					/* Now, we create a new vertex and then we add auxiliary information! */
					this->ccw->addSimplex(cp);
					if(reqParams)
					{
						/* Now, we must check if the Euclidean coordinates are supported! */
						if(ig->supportsEuclideanCoordinates()==true)
						{
							a=ig->getPropertyValue(string(this->ccw->getPropertyName4Coordinates()),SimplexPointer(it.getPointer()),p);
							pnt_prop->set(SimplexPointer(it.getPointer()),Point<double>(a));
						}
					
						/* Now, we must check if the field values are supported! */
						if(ig->supportsFieldValues()==true)
						{
							f=ig->getPropertyValue(string(this->ccw->getPropertyName4FieldValue()),SimplexPointer(it.getPointer()),aaa);
							fld_prop->set(SimplexPointer(it.getPointer()),f);
						}
					}
				}
				
				/* Now, we can copy the other simplices */
				if(ig->type()==0) { return; }
				for(unsigned int k=1;k<=ig->type();k++)
				{
					/* Now, we copy all the k-simplices! */
					for(it=ig->begin(k);it!=ig->end(k);it++)
					{
						/* Now, we add a new k-simplex and then we copy its boundary! */
						this->ccw->addSimplex(k,cp);
						for(unsigned int i=0;i<k+1;i++)
						{
							this->ccw->simplex(cp).b(i).setType(ig->simplex(SimplexPointer(it.getPointer())).bc(i).type());
							this->ccw->simplex(cp).b(i).setId(ig->simplex(SimplexPointer(it.getPointer())).bc(i).id());
						}
					}
				}
			}
			
			/// This member function computes the partial coboundary relation for each simplex in the current <i>Simplified Incidence Graph</i>.
			/**
			 * This member function computes the partial coboundary relation for each simplex in the current <i>Simplified Incidence Graph</i> data structure: we start our analysis from the
			 * corresponding <i>Incidence Graph</i> and we try to identify all the clusters of simplices in the simplex star through the mangrove_tds::SIGManager::identifyClusters() member
			 * function.<p><b>IMPORTANT:</b> in this member function, we use two global properties with boolean values, called <i>assigned</i> and <i>candidates</i>, for identifying
			 * clusters of simplices in the star of simplices. We remove such properties, if they already belong to the input <i>Incidence Graph</i>. In any case, we remove them at the end of
			 * computation.
			 * \param ig the <i>Incidence Graph</i> corresponding to the <i>Simplified Incidence Graph</i> to be created
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIGManager::identifyClusters(), mangrove_tds::IG, mangrove_tds::SIG, mangrove_tds::BaseSimplicialComplex,
			 * mangrove_tds::Simplex, mangrove_tds::BaseSimplicialComplex::begin(), mangrove_tds::BaseSimplicialComplex::end()
			 */
			inline void computeCoboundary(IG *ig)
			{
				SimplexIterator it;
				vector<SimplexPointer> cps;
				vector<SimplexPointer>::iterator cp;
				
				/* First, we loop over the dimensions in the current 'ig' */
				if(ig->type()==0) { return; }
				for(SIMPLEX_TYPE d=0;d<=ig->type()-1;d++)
				{
					/* Now, we iterate over all the d-simplices! */
					for(it=ig->begin(d);it!=ig->end(d);it++)
					{
						/* First, we check if the current simplex is top */
						this->ccw->simplex(it.getPointer()).clearCoboundary();
						if(ig->isTop(SimplexPointer(it.getPointer()))==false)
						{
							/* Now, we must extract the connected components of its star! */
							this->identifyClusters( ig, SimplexPointer(it.getPointer()), cps);
							for(cp=cps.begin();cp!=cps.end();cp++) { this->ccw->simplex(it.getPointer()).add2Coboundary(SimplexPointer(*cp)); }
						}
					}
				}
				
				/* If we arrive here, we have finished! */
				cps.clear();
			}
			
			/// This member function computes the partial coboundary relation for a simplex in the current <i>Simplified Incidence Graph</i>.
			/**
			 * This member function computes the partial coboundary relation for a simplex in the current <i>Simplified Incidence Graph</i> data structure: we need the auxiliary information
			 * about the incidence and adjacency relation, restricted only to the top simplices, respectively described by the mangrove_tds::RawIncidence and mangrove_tds::RawAdjacency class.
			 * \param inc auxiliary information about the incidence relation
			 * \param ad auxiliary information about the adjacency relation
			 * \param cp a pointer to the input simplex
			 * \see mangrove_tds::RawIncidence, mangrove_tds::RawAdjacency, mangrove_tds::SimplexPointer, mangrove_tds::Simplex
			 */
			inline void computeCoboundary(vector< vector<RawIncidence> > &inc, vector< vector<RawAdjacency> > &ad, const SimplexPointer &cp)
			{
				unsigned int lg;
				GlobalPropertyHandle<bool> *cprop,*asp,*vp;
				vector<SimplexPointer>::iterator it,s1;
				bool b;
				SimplexPointer sigma;

				/* First, we consider the number of top simplices incident in 'cp' */
				lg=inc[cp.type()][cp.id()].getSimplices().size();
				if(lg==0) { return; }
				else if(lg==1) { this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(inc[cp.type()][cp.id()].getSimplices().at(0))); }
				else
				{
					/* There are more than 1 top simplex, we should identify clusters of top simplices */
					cprop=this->ccw->addGlobalProperty(string("candidates"), false);
					asp=this->ccw->addGlobalProperty(string("assigned"),false);
					b=false;
					for(it=inc[cp.type()][cp.id()].getSimplices().begin();it!=inc[cp.type()][cp.id()].getSimplices().end();it++) { cprop->set(SimplexPointer(*it),true); }		
					for(it=inc[cp.type()][cp.id()].getSimplices().begin();it!=inc[cp.type()][cp.id()].getSimplices().end();it++)
					{
						/* Now, we look for the cluster incident in 'it' if and only if has not been assigned to an other cluster! */
						if(asp->get(SimplexPointer(*it))==false)
						{
							/* Now, we check the dimension of 'it' */
							b=false;
							if(it->type()==1)
							{
								this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(*it));
								asp->set(SimplexPointer(*it),true);
							}
							else if(it->type()==cp.type()+1)
							{
								/* We have found a valid simplex! */
								asp->set(SimplexPointer(*it),true);
								this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(*it));
							}
							else
							{
								queue<SimplexPointer> q;

								/* If we arrive here, we must visit the cluster incident in 'it' */
								vp=this->ccw->addGlobalProperty(string("visited"),false);
								q.push(SimplexPointer(*it));
								while(q.empty()==false)
								{
									/* Now, we extract the first simplex from q and then we proceed on its adjacent! */
									sigma=q.front();
									q.pop();
									if(vp->get(SimplexPointer(sigma))==false)
									{
										/* Now, we can analyze 'sigma' */
										vp->set(SimplexPointer(sigma),true);
										asp->set(SimplexPointer(sigma),true);
										if(b==false)
										{
											this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(sigma));
											b=true;
										}
										
										/* Now, we can analyze the adjacents of 'sigma', incident in 'it' */
										for(unsigned int k=0;k<=sigma.type();k++)
										{
											for(s1=ad[sigma.type()-1][sigma.id()].getSimplices()[k].begin();s1!=ad[sigma.type()-1][sigma.id()].getSimplices()[k].end();s1++)
											{ if(cprop->get(SimplexPointer(*s1))==true) q.push(SimplexPointer(*s1)); }
										}
									}
								}
								
								/* If we arrive here, we have identified the required cluster! */
								this->ccw->deleteProperty(string("visited"));
								vp=NULL;
							}
						}
					}

					/* If we arrive here, we can remove 'properties' */
					this->ccw->deleteProperty(string("candidates"));
					this->ccw->deleteProperty(string("assigned"));
					if(this->ccw->hasProperty(string("visited"))) this->ccw->deleteProperty(string("visited"));
				}
			}
			
			/// This member function identifies all the clusters of simplices in the star of a simplex belonging to an <i>Incidence Graph</i>.
			/**
			 * This member function identifies all the clusters of simplices in the star of a simplex belonging to an <i>Incidence Graph</i>.<p><b>IMPORTANT:</b> in this member function, we
			 * use the <i>candidates</i>, <i>assigned</i> and <i>visited</i> global properties (i.e. instances of the mangrove_tds::GlobalPropertyHandle class) for identifying clusters of
			 * simplices in the star of each simplex. We remove such properties, if they already belong to the input <i>Incidence Graph</i>. In any case, we remove them at the end of
			 * computation.
			 * \param ig the <i>Incidence Graph</i> corresponding to the <i>Simplified Incidence Graph</i> to be built
			 * \param cc a pointer to the required simplex
			 * \param cps all the required simplices, one for each connected component
			 * \see mangrove_tds::IG, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle,
			 * mangrove_tds::BaseSimplicialComplex, mangrove_tds::BaseSimplicialComplex::addGlobalProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty(),
			 * mangrove_tds::BaseSimplicialComplex::getPropertyValue()
			 */
			inline void identifyClusters(IG *ig,const SimplexPointer &cc,vector<SimplexPointer> &cps)
			{
				GlobalPropertyHandle<bool> *cprop,*asp,*vp;
				list<SimplexPointer> ccl;
				SimplexPointer sigma,sigma1,sigma2;
				unsigned int cbnd_lg;
				vector<SimplexPointer>::iterator current;
				vector<SimplexPointer> candidates,cluster;

				/* First, we clear 'cps' and then we can identify all the top simplices incident in 'cc' */
				cps.clear();
				ig->coboundary(SimplexPointer(cc),&ccl);
				extractTopSimplices(ccl,candidates,ig);
				if(ig->hasProperty(string("candidates"))) ig->deleteProperty(string("candidates"));
			 	if(ig->hasProperty(string("assigned"))) ig->deleteProperty(string("assigned"));
			 	if(ig->hasProperty(string("visited"))) ig->deleteProperty(string("visited"));
				if(candidates.size()==0) { return; }
				if(candidates.size()==1) { cps.push_back(SimplexPointer(candidates[0])); }
				else
				{
					/* Now, we must identify all the clusters incident in cc */
					cprop=ig->addGlobalProperty(string("candidates"), false);
					asp=ig->addGlobalProperty(string("assigned"),false);
					for(current=candidates.begin();current!=candidates.end();current++) { cprop->set(SimplexPointer(*current),true); }
					for(current=candidates.begin();current!=candidates.end();current++)
					{
						/* Now, we look for the cluster incident in 'current' if and only if has not been assigned to an other cluster! */
						if(asp->get(SimplexPointer(*current))==false)
						{
							/* Now, we check the dimension of the 'current' simplex */
							if(current->type()==cc.type()+1)
							{
								/* We have found a valid simplex! */
								asp->set(SimplexPointer(*current),true);
								cps.push_back(SimplexPointer(*current));
							}
							else
							{
								queue<SimplexPointer> q;

								/* If we arrive here, we must visit the cluster incident in 'current' */
								vp=ig->addGlobalProperty(string("visited"),false);
								cluster.clear();
								q.push(SimplexPointer(*current));
								while(q.empty()==false)
								{
									/* Now, we extract the first simplex from q and then we proceed on its adjacent! */
									sigma=q.front();
									q.pop();
									if(vp->get(SimplexPointer(sigma))==false)
									{
										/* Now, we can proceed on the 'sigma' adjacents */
										vp->set(SimplexPointer(sigma),true);
										asp->set(SimplexPointer(sigma),true);
										cluster.push_back(SimplexPointer(sigma));
										for(SIMPLEX_TYPE k=0;k<sigma.type()+1;k++)
										{
											/* Now, we extract the coboundary of sigma1 */
											sigma1=SimplexPointer(ig->simplex(sigma).bc(k));
											cbnd_lg=ig->simplex(sigma1).getCoboundarySize();
											for(unsigned int l=0;l<cbnd_lg;l++)
											{
												/* Now, we have found an adjacent one of 'sigma' and now we check if it is incident in 'cc'*/
												sigma2=SimplexPointer(ig->simplex(sigma1).cc(l));
												if(cprop->get(SimplexPointer(sigma2))==true) { q.push(SimplexPointer(sigma2)); }
											}
										}
									}
								}

								/* If we arrive here, we have identified the required cluster! */	
								if(cluster.size()!=0) { cps.push_back(SimplexPointer(cluster[0])); }
								ig->deleteProperty(string("visited"));
								vp=NULL;
							}
						}
					}

					/* If we arrive here, we can remove 'properties' */
					ig->deleteProperty(string("candidates"));
					ig->deleteProperty(string("assigned"));
					if(ig->hasProperty(string("visited"))) ig->deleteProperty(string("visited"));
				}
			}
			
			/// This member function generates all the unique simplices in the current simplicial complex.
			/**
			 * This member function generates all the unique simplices in the current simplicial complex: we start our analysis from the raw subsimplices of all the top simplices in the I/O
			 * component. We assume to retrieve all top simplices through the mangrove_tds::DataManager::getCollection() member function and to retrieve all the raw simplices (described
			 * through the mangrove_tds::RawFace class) through the mangrove_tds::DataManager::getRawFaces() member function.<p>In this algorithm, we generate all the unique simplices,
			 * by sorting all the raw simplices: in this way, we can also rebuild the boundary hierarchy between them.
			 * \param d the dimension of the unique simplices to be generated
			 * \param loader the component that contains the raw and top simplices describing the input simplicial complex
			 * \param inc the auxiliary data structure for storing coboundary relation, restricted to top simplices
			 * \param raw_ad the auxiliary data structure for storing adjacency relation, restricted to top simplices
			 * \see mangrove_tds::SIGManager::addEdge(), mangrove_tds::SIMPLEX_TYPE, mangrove_tds::RawFace, mangrove_tds::DataManager::getCollection(),
			 * mangrove_tds::SimplicesContainer, mangrove_tds::DataManager::getRawFaces(), mangrove_tds::Simplex, mangrove_tds::RawIncidence, mangrove_tds::RawAdjacency
			 */
			template <class T> void getUnique(SIMPLEX_TYPE d,DataManager<T> *loader,vector< vector<RawIncidence> > &inc,vector< vector<RawAdjacency> > &raw_ad)
			{
				vector<RawFace> auxd;
				unsigned int r,c;
				SIMPLEX_ID v0,v1,indr,t;
				vector<unsigned int> cs;
				vector<SimplexPointer> f;
				SimplexPointer cp;
				
				/* First, we copy all the raw d-simplices in order to mantain the correspondence among them */
				for(unsigned int k=0;k<loader->getRawFaces()[d].size();k++)
				{
					loader->getRawFaces()[d][k].getOriginalPosition() = k;
					auxd.push_back(RawFace(loader->getRawFaces()[d].at(k)));	
				}
				
				/* Now, we sort the copy 'auxd' in order to remove duplicates (replicants). */				
				sort( auxd.begin(),auxd.end(),compare_faces);
				r=0;
				c=1;
				while( (r<auxd.size()) && (c<auxd.size()))
				{
					/* Now, we must identify connected components of faces! */
					if(same_faces(auxd[r],auxd[c]))
					{
						/* We accumulate replicants! */
						cs.push_back(c);
						c=c+1;
					}
					else
					{
						/* Now, we must finalize the identification of a new simplex */
						if(d==1)
						{
							/* Now, we must add an edge! */
							v0 = auxd[r].getVertices().at(0);
							v1 = auxd[r].getVertices().at(1);
							indr = this->addEdge(v0,v1);
							inc[d].push_back(RawIncidence());
						}
						else
						{
							/* Now, we must rebuild the boundary of the new simplex to be added */
							f.clear();
							for(unsigned int j=0;j<=d;j++)
							{
								t=auxd[r].getBoundary()[j];
								f.push_back( SimplexPointer(d-1, loader->getRawFaces()[d-1][t].getUniqueIndex()));
							}
							
							/* Now, we add the new simplex. */
							this->ccw->addSimplex(f,cp);
							indr = cp.id();
							inc[d].push_back(RawIncidence());
							raw_ad[d-1].push_back(RawAdjacency(d));
						}
						
						/* Now, we must complete the construction of the new simplex and then we prepare the creation of a new cluster of elements! */
						loader->getRawFaces()[d][auxd[r].getOriginalPosition()].getUniqueIndex() = indr;
						auxd[r].getUniqueIndex() = indr;
						for(unsigned int l=0;l<cs.size();l++) { loader->getRawFaces()[d][auxd[cs[l]].getOriginalPosition()].getUniqueIndex() = indr; }
						r=c;
						c=r+1;
						cs.clear();
					}
				}
				
				/* Now, we must complete the construction, finalizing the last cluster! */
				if(r<auxd.size())
				{
					/* Now, we must finalize the identification of a new simplex! */
					if(d==1)
					{
						/* Now, we must add an edge! */
						v0 = auxd[r].getVertices().at(0);
						v1 = auxd[r].getVertices().at(1);
						indr = this->addEdge(v0,v1);
						inc[d].push_back(RawIncidence());
					}
					else
					{
						/* Now, we must rebuild the boundary of the new simplex to be added */
						f.clear();
						for(unsigned int j=0;j<=d;j++)
						{
							t=auxd[r].getBoundary()[j];
							f.push_back( SimplexPointer(d-1, loader->getRawFaces()[d-1][t].getUniqueIndex()));
						}
							
						/* Now, we add the new simplex. */
						this->ccw->addSimplex(f,cp);
						indr = cp.id();
						inc[d].push_back(RawIncidence());
						raw_ad[d-1].push_back(RawAdjacency(d));
					}
					
					/* Now, we must complete the construction of the new simplex! */
					auxd[r].getUniqueIndex() = indr;
					loader->getRawFaces()[d][auxd[r].getOriginalPosition()].getUniqueIndex() = indr;
					for(unsigned int l=0;l<cs.size();l++)
					{
						auxd[cs[l]].getUniqueIndex() = indr;
						loader->getRawFaces()[d][auxd[cs[l]].getOriginalPosition()].getUniqueIndex() = indr;
					}
				}
			}
			
			/// This member function retrieves the adjacency relation, restricted only to top simplices.
			/**
			 * This member function retrieves the adjacency relation, restricted only to top simplices.
			 * \param raw_faces the auxiliary information for describing intermediate faces shared by top simplices
			 * \param raw_ad the auxiliary information for describing the adjacency information restricted only to top simplices
			 * \see mangrove_tds::RawFace, mangrove_tds::RawAdjacency, mangrove_tds::compare_shared_faces(), mangrove_tds::same_shared_faces(), mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, 
			 */
			inline void computeAdjacency(vector< vector<RawFace> > & raw_faces,vector< vector<RawAdjacency> > & raw_ad)
			{
				unsigned int r,c,fi,ri,fi_cs,ri_cs;
				vector<unsigned int> cs;

				/* Now, we must iterate on the slots in 'raw_faces' */
				for(SIMPLEX_TYPE d=1;d<raw_faces.size();d++)
				{
					/* First, we sort 'raw_faces[d]' in order to identify adjacent (d+1)-faces */
					cs.clear();
					sort(raw_faces[d].begin(),raw_faces[d].end(),compare_shared_faces);
					r=0;
					c=1;
					while( (r<raw_faces[d].size()) && (c<raw_faces[d].size()))
					{
						/* Now, we must identify connected components of faces! */
						if(same_shared_faces(raw_faces[d][r],raw_faces[d][c]))
						{
							/* We accumulate replicants! */
							cs.push_back(c);
							c=c+1;
						}
						else
						{
							/* We mus adjust adjancencies! */
							if(cs.size()!=0)
							{
								/* We must store adjacent faces */
								cs.push_back(r);
								for(unsigned int i=0;i<cs.size();i++)
								{
									/* We encode adjacent before i */
									fi=raw_faces[d][cs[i]].getIndex();
									ri=raw_faces[d][cs[i]].getReverseIndex();
									for(unsigned int j=0;j<i;j++)
									{
										fi_cs=raw_faces[d][cs[j]].getIndex();
										ri_cs=raw_faces[d][cs[j]].getReverseIndex();
										raw_ad[d][fi].addSimplex(SimplexPointer(d+1,fi_cs),ri,ri_cs);
									}
									
									/* Now, we encode adjacent after i */
									for(unsigned int j=i+1;j<cs.size();j++)
									{
										fi_cs=raw_faces[d][cs[j]].getIndex();
										ri_cs=raw_faces[d][cs[j]].getReverseIndex();
										raw_ad[d][fi].addSimplex(SimplexPointer(d+1,fi_cs),ri,ri_cs);
									}
								}
							}
							
							/* We reset auxiliary data structures for continuing...." */
							r=c;
							c=r+1;
							cs.clear();
						}	
					}
					
					/* Now, we must complete the construction, finalizing the last cluster! */
					if(r<raw_faces[d].size())
					{
						/* We mus adjust adjancencies! */
						if(cs.size()!=0)
						{
							/* We must store adjacent faces */
							cs.push_back(r);
							for(unsigned int i=0;i<cs.size();i++)
							{
								/* We encode adjacent before i */
								fi=raw_faces[d][cs[i]].getIndex();
								ri=raw_faces[d][cs[i]].getReverseIndex();
								for(unsigned int j=0;j<i;j++)
								{
									fi_cs=raw_faces[d][cs[j]].getIndex();
									ri_cs=raw_faces[d][cs[j]].getReverseIndex();
									raw_ad[d][fi].addSimplex(SimplexPointer(d+1,fi_cs),ri,ri_cs);
								}
									
								/* Now, we encode adjacent after i */
								for(unsigned int j=i+1;j<cs.size();j++)
								{
									fi_cs=raw_faces[d][cs[j]].getIndex();
									ri_cs=raw_faces[d][cs[j]].getReverseIndex();
									raw_ad[d][fi].addSimplex(SimplexPointer(d+1,fi_cs),ri,ri_cs);
								}
							}
						}
					}
				}
			}			
		};
	}

#endif

