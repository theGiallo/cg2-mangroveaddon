/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library                                
 *                                                                           				    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * September 2011 (Revised on May 2012)
 *                                                                 
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * NMIA.h -  implementation of the Non-Manifold IA (NMIA) data structure
 ***********************************************************************************************/

/* Should we include this header file? */
#ifndef NMIA_H

	#define NMIA_H

	#include "BaseSimplicialComplex.h"
	#include "Miscellanea.h"
	#include <algorithm>
	#include <assert.h>
	#include <cstdlib>
	#include <queue>
	#include <stack>
	using namespace mangrove_tds;
	using namespace std;
	
	/// Implementation of the <i>Non-Manifold IA</i> data structure.
	/**
	 * The class defined in this file implements the <i>Non-Manifold IA</i> data structure, described by the mangrove_tds::NMIIA class, is a dimension-specific data structure for representing
	 * simplificial 3-complexes with an arbitrary domain. It extends the <i>IA</i> data structure and it is scalable to manifold complexes, and supports efficient navigation and topological
	 * modifications. The <i>Non-Manifold IA</I> data structure encodes only top simplices plus a suitable subset of the adjacency relations, thus it is a <i>local</i> data structure.<p>This
	 * class must be used in conjunction with the mangrove_tds::BaseSimplicialComplex class in accordance with the
	 * <A href=http://en.wikipedia.org/wiki/Curiously_recurring_template_pattern>Curiously Recurring Template Pattern</A> rules in order to implement the static polymorphism: in this way
	 * we can reuse some parts of the mangrove_tds::BaseSimplicialComplex class and we can achieve more efficient implementations, by discarding virtual member functions.
	 * \file NMIA.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	 
	 /* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		/// Implementation of the <i>Non-Manifold IA</i> data structure.
		/**
	 	 * The class defined in this file implements the <i>Non-Manifold IA</i> data structure, described by the mangrove_tds::NMIIA class, is a dimension-specific data structure for
	 	 * representing simplificial 3-complexes with an arbitrary domain. It extends the <i>IA</i> data structure and it is scalable to manifold complexes, and supports efficient navigation and
	 	 * topological modifications. The <i>Non-Manifold IA</I> data structure encodes only top simplices plus a suitable subset of the adjacency relations, thus it is a <i>local</i> data
	 	 * structure.<p>This class must be used in conjunction with the mangrove_tds::BaseSimplicialComplex class in accordance with the
	 	 * <A href=http://en.wikipedia.org/wiki/Curiously_recurring_template_pattern>Curiously Recurring Template Pattern</A> rules in order to implement the static polymorphism: in this
	 	 * way we can reuse some parts of the mangrove_tds::BaseSimplicialComplex class and we can achieve more efficient implementations, by discarding virtual member functions.
	 	 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplexPointer, mangrove_tds::IG, mangrove_tds::IS, mangrove_tds::SIG,
	 	 * mangrove_tds::GIA, mangrove_tds::TriangleSegment, mangrove_tds::GhostPropertyHandle
		 */
		class NMIA : public BaseSimplicialComplex<NMIA>
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates all the internal data structures required for encoding a simplicial complex through a <i>Non-Manifold IA</i>. In this member function, we allocate
			 * all the internal data structures without any simplices or properties, i.e. auxiliary information, like Euclidean coordinates and field values, associated to simplices directly
			 * encoded in the <i>Non-Manifold IA</i> data structure. Thus, you should apply the member functions offered by the mangrove_tds::NMIA class in order to add simplices and
			 * properties to the current data structure encoding a new simplicial complex.
			 * \param t the dimension of the simplicial complex to be encoded in the current data structure
			 * \see mangrove_tds::BaseSimplicialComplex::init(), mangrove_tds::BaseSimplicialComplex::clear(), mangrove_tds::PropertyBase, mangrove_tds::SIMPLEX_TYPE,
			 * mangrove_tds::BaseSimplicialComplex::addGlobalProperty(), mangrove_tds::BaseSimplicialComplex::addLocalProperty(),
			 * mangrove_tds::BaseSimplicialComplex::addSparseProperty(), mangrove_tds::BaseSimplicialComplex::addGhostProperty()
			 */
			inline NMIA(SIMPLEX_TYPE t) : BaseSimplicialComplex<NMIA>(t) { assert(t==3); }
			
			/// This member function destroys an instance of this class.
			inline virtual ~NMIA() { ; }
			
			/// This member function returns a string that describes the current data structure.
			/**
 			 * This member function returns a string that describes the current data structure, usually its name.
 			 * \return a string that describes the current data structure
 			 * \see mangrove_tds::BaseSimplicialComplex::debug()
 			 */
 			inline string getDataStructureName() { return string("Non-Manifold IA (NMIA)"); }
 			
 			/// This member function checks if the current data structure encodes all simplices in the simplicial complex to be represented.
 			/**
 			 * This member function checks if the current data structure encodes all simplices in the current simplicial complex: in this, case it is called <i>global</i> data structure.
			 * Alternatively, a data structure can encode a subset of all simplices, for instance all top simplices: in this case, it is called <i>local</i> data structure.<p>The
			 * <i>Non-Manifold IA</i> data structure encodes only top simplices and it is a <i>local</i> data structure.
 			 * \return <ul><li><i>true</i>, if the current data structure encodes all simplices</li><li><i>false</i>, otherwise</li></ul>
 			 * \see mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::isTop(), mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
 			 */
 			inline bool encodeAllSimplices() { return false; }
 			
 			/// This member function computes the storage cost of the current data structure.
			/**
			 * This member function computes the storage cost of the current data structure, expressed as number of pointers to the simplices directly encoded in the current data 		
			 * structure. In other words, we compute the number of instances of the mangrove_tds::SimplexPointer class required for encoding the current data structure.
			 * \return the storage cost of the current data structure
			 * \see mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::SimplexIterator, mangrove_tds::BaseSimplicialComplex::type(), mangrove_tds::SIMPLEX_TYPE
			 */
			inline unsigned int getStorageCost()
 			{
 				unsigned int c;
 				SIMPLEX_TYPE t,d;
 				SimplexIterator it;

 				/* We must iterate on all the simplices in the current data structure! */
				c=0;
				t=this->type();
				for(d=0;d<=t;d++)
				{
					/* Now, we iterate over all the d-simplices! */
					for(it=this->begin(d);it!=this->end(d);it++)
					{
						/* Now, we consider the type of the current d-simplex! */
						if(it->type()==0) { c=c+it->getCoboundarySize(); }
						else if(this->isTop(SimplexPointer(it.getPointer())))
						{
							/* Now, we must understand which type of top simplex we have */
							if(it->type()==3) { c=c+8; }
							else if(it->type()==2) { c=c+6; }
							else if(it->type()==1) { c=c+2; }
							else { ; }
						}
						else { c=c+2; }
					}
				}
				
				/* If we arrive here, we can return the storage cost! */
				return c;
 			}
						
 			/// This member function identifies all simplices belonging to the boundary of a simplex directly encoded in the current data structure.
 			/**
 			 * This member function identifies all simplices belonging to the boundary of a simplex directly encoded in the current data structure.<p>The reference simplex and the boundary
 			 * simplices are required to be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector
 			 * mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function
 			 * only with a <i>global</i> data structure.<p>The <i>Non-Manifold IA</i> data structure is a <i>local</i> data structure, thus we cannot apply this member function for
 			 * computing the boundary of a simplex: you should apply instances of the mangrove_tds::GhostSimplexPointer class for accessing descriptions of simplices not directly encoded in 
			 * the current data structure. Consequently, this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.
	 		 * \param cp the reference simplex directly encoded in the current data structure
	 		 * \param ll a list containing all simplices belonging to the boundary of a simplex directly encoded in the current data structure.
	 		 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::PropertyBase
	 		 */
	 		inline void boundary(const SimplexPointer& cp, list<SimplexPointer>* ll)
	 		{
	 			/* You cannot apply this member function - idle avoids compiler warnings! */
	 			idle(ll);
	 			idle(cp);
	 			assert(this->encodeAllSimplices()==true);
	 		}
	 		
	 		/// This member function identifies all simplices of a certain dimension belonging to the boundary of a simplex directly encoded in the current data structure.
 			/**
 			 * This member function identifies all simplices of a certain dimension belonging to the boundary of a simplex directly encoded in the current data structure: this member function
 			 * filters boundary simplices against their dimension.<p>The reference simplex and the boundary simplices are required to be <i>valid</i>, i.e. directly stored in the current data
 			 * structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class
 			 * and thus by their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>The <i>Non-Manifold IA</i> data structure
 			 * is a <i>local</i> data structure, thus we cannot apply this member function for computing the boundary of a simplex: you should apply instances of the
 			 * mangrove_tds::GhostSimplexPointer class for accessing descriptions of simplices not directly encoded in the current data structure. Consequently, this member function will
			 * fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
 			 * \param cp the reference simplex directly encoded in the current data structure
 			 * \param k the dimension of the required simplices belonging to the input simplex boundary
	 		 * \param ll a list containing all simplices belonging to the boundary of a simplex directly encoded in the current data structure
			 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
			 * mangrove_tds::PropertyBase
	 		 */
	 		inline void boundaryk(const SimplexPointer& cp,SIMPLEX_TYPE k,list<SimplexPointer> *ll)
	 		{
	 			/* You cannot apply this member function - idle avoids compiler warnings! */
	 			idle(ll);
	 			idle(cp);
	 			idle(k);
	 			assert(this->encodeAllSimplices()==true);
	 		}
 			
 			/// This member function identifies all simplices belonging to the boundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the boundary of a simplex not directly encoded in the current data structure.<p>The reference simplex and the
			 * boundary simplices can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can
			 * invoke this member function only with a <i>local</i> data structure.<p>The <i>Non-Manifold IA</i> data structure is a <i>local</i> data structure.<p><b>IMPORTANT:</b>if we
			 * cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.
 			 * \param cp the reference simplex not directly encoded in the current data structure
	 		 * \param ll a list containing all simplices belonging to the boundary of a simplex not directly encoded in the current data structure.
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::Simplex,
	 		 * mangrove_tds::COMPLEX
	 		 */
	 		inline void boundary(const GhostSimplexPointer& cp, list<GhostSimplexPointer>* ll)
	 		{
	 			stack<SimplexPointer> s;
				COMPLEX c;
				SimplexPointer curr;
				vector<SIMPLEX_ID> fs;
				unsigned int t,ct,ci,i,j,lg;

	 			/* First, we check if all is ok and then if 'cp' is a real ghost simplex! */
	 			assert(ll!=NULL);
	 			assert(this->isValid(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			assert(this->isTop(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			ll->clear();
	 			t=cp.getParentType();
				ct=cp.getChildType();
				ci=cp.getChildId();
				if(cp.isEncoded()==false)
				{
					/* The parent is a valid top: we must have a valid child of a valid top simplex! */
					assert(ct<t);
					assert(ci<this->pascal_triangle[t+1][ct+1]);
					if(ct!=0)
					{
						/* Now, we can extract the boundary of a not encoded simplex (not vertex) */
						s.push(SimplexPointer(ct,ci));
						while(s.empty()==false)
						{
							curr=s.top();
							s.pop();
							if(curr.type()!=0)
							{
								fs=vector<SIMPLEX_ID>(this->fposes[t].getHierarchy()[curr.type()][curr.id()].getCBoundary());
								for(unsigned int z=0;z<fs.size();z++)
								{
									lg=c.size();
									c.insert(SimplexPointer(curr.type()-1,fs[z]));
									if(c.size()!=lg)
									{
										s.push(SimplexPointer(curr.type()-1,fs[z]));
										ll->push_back(GhostSimplexPointer(t,cp.getParentId(),curr.type()-1,fs[z]));	
									}
								}
							}
						}
					}
				}
				else if(ct!=0) { for(j=0;j<t;j++) for(i=0;i<pascal_triangle[t+1][j+1];i++) ll->push_back(GhostSimplexPointer(t,cp.getParentId(),j,i)); }
	 		}
	 		
	 		/// This member function identifies all simplices of a certain dimension belonging to the boundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices of a certain dimension belonging to the boundary of a simplex not directly encoded in the current data structure: this member
			 * function filters boundary simplices against their dimension.<p>The reference simplex and the boundary simplices can also be not directly encoded in the current data structure
			 * and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>The
	 		 * <i>Non-Manifold IA</i> data structure is a <i>local</i> data structure.<p><b>IMPORTANT:</b>if we cannot complete this operation, then this member function will fail if and
	 		 * only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
			 * \param k the dimension of the required simplices belonging to the input simplex boundary
			 * \param ll a list of the required simplices belonging to the input simplex boundary
	 		 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isValid()
			 */
	 		inline void boundaryk(const GhostSimplexPointer& cp,SIMPLEX_TYPE k,list<GhostSimplexPointer> *ll)
	 		{
				unsigned int t,ct,ci,i,lg;
				vector<SIMPLEX_ID> fs;
				SimplexPointer curr;
				stack<SimplexPointer> s;
				COMPLEX c;

	 			/* First, we check if all is ok and then if 'cp' is a real ghost simplex! */
	 			assert(ll!=NULL);
	 			assert(this->isValid(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			assert(this->isTop(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			ll->clear();
	 			t=cp.getParentType();
				ct=cp.getChildType();
				ci=cp.getChildId();
				if(cp.isEncoded()==false)
				{
					/* The parent is a valid top: we must have a valid child of a valid top simplex! */
					assert(ct<t);
					assert(ci<this->pascal_triangle[t+1][ct+1]);
					assert(k<ct);
					if(ct!=0)
					{
						/* Now, we can extract the boundary of a not encoded simplex (not vertex) */
						s.push(SimplexPointer(ct,ci));
						while(s.empty()==false)
						{
							curr=s.top();
							s.pop();
							if(curr.type()>k)
							{
								fs=vector<SIMPLEX_ID>(this->fposes[t].getHierarchy()[curr.type()][curr.id()].getCBoundary());
								for(unsigned int z=0;z<fs.size();z++)
								{
									lg=c.size();
									c.insert(SimplexPointer(curr.type()-1,fs[z]));
									if(c.size()!=lg) { s.push(SimplexPointer(curr.type()-1,fs[z])); }
								}
							}
							else if(curr.type()==k) { ll->push_back(GhostSimplexPointer(t,cp.getParentId(),k,curr.id())); }
						}
					}
				}
				else if(ct!=0) { for(i=0;i<pascal_triangle[t+1][k+1];i++) ll->push_back(GhostSimplexPointer(t,cp.getParentId(),k,i)); }
	 		}
	 		
	 		/// This member function identifies all simplices belonging to the coboundary of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the coboundary of a simplex directly encoded in the current data structure.<p>The reference simplex and the star
	 		 * simplices are required to be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector
	 		 * mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function
	 		 * only with a <i>global</i> data structure.<p>The <i>Non-Manifold IA</i> data structure is a <i>local</i> data structure, thus we cannot apply this member function for
	 		 * computing the coboundary of a simplex: you should apply instances of the mangrove_tds::GhostSimplexPointer class for accessing descriptions of simplices not directly encoded in
	 		 * the current data structure. Consequently, this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
	 		 * operation will result in a failed assert.
 			 * \param cp the reference simplex directly encoded in the current data structure
			 * \param ll a list of the required simplices belonging to the input simplex coboundary
			 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
			 * mangrove_tds::PropertyBase
			 */
			inline void coboundary(const SimplexPointer& cp,list<SimplexPointer> *ll)
	 		{
	 			/* You cannot apply this member function - idle avoids compiler warnings! */
	 			idle(ll);
	 			idle(cp);
	 			assert(this->encodeAllSimplices()==true);
	 		}

	 		/// This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex directly encoded in the current data structure: this member 
			 * function filters coboundary simplices against their dimension.<p>The reference simplex and the star simplices are required to be <i>valid</i>, i.e. directly stored in the
			 * current data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They are identified by instances of the
			 * mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>The
			 * <i>Non-Manifold IA</i> data structure is a <i>local</i> data structure, thus we cannot apply this member function for computing the coboundary of a simplex: you should apply
			 * instances of the mangrove_tds::GhostSimplexPointer class for accessing descriptions of simplices not directly encoded in the current data structure. Consequently, this member
			 * function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
 			 * \param cp the reference simplex directly encoded in the current data structure
 			 * \param k the dimension of the required simplices belonging to the input simplex coboundary
			 * \param ll a list of the required simplices belonging to the input simplex coboundary
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::GhostSimplex,
			 * mangrove_tds::GhostSimplexPointer, mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::LocalPropertyHandle,
			 * mangrove_tds::BaseSimplicialComplex::addGlobalProperty(), mangrove_tds::BaseSimplicialComplex::addLocalProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty()
			 */
	 		inline void coboundaryk(const SimplexPointer& cp,SIMPLEX_TYPE k,list<SimplexPointer> *ll)
	 		{
	 			/* You cannot apply this member function - idle avoids compiler warnings! */
	 			idle(ll);
	 			idle(cp);
	 			idle(k);
	 			assert(this->encodeAllSimplices()==true);
	 		}
 		
	 		/// This member function identifies all simplices belonging to the coboundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the coboundary of a simplex not directly encoded in the current data structure.<p>The reference simplex and the star
			 * simplices can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke
			 * this member function only with a <i>local</i> data structure.<p>The <i>Non-Manifold IA</i> data structure is a <i>local</i> data structure.<p>If we cannot complete this
			 * operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in
			 * a failed assert.<p><b>IMPORTANT:</b> when you try to apply this member function, you should be careful about the use of auxiliary information (i.e. subclasses of the
			 * mangrove_tds::PropertyBase class) associated to the current simplicial complex. In other words, in this member function, we use a ghost property, called <i>cobs</i> and
			 * described by an instance of the mangrove_tds::GhostPropertyHandle, for marking ghost simplices. We remove this property, if it already belongs to the current simplicial
			 * complex. In any case, we remove it at the end of this member function.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
	 		 * \param ll a list of the required simplices belonging to the input simplex coboundary
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::PropertyBase,
	 		 * mangrove_tds::GhostPropertyHandle, mangrove_tds::NMIA::retrieveTopTriangles()
	 		 */
	 		inline void coboundary(const GhostSimplexPointer& cp,list<GhostSimplexPointer> *ll)
	 		{
	 			GhostPropertyHandle<bool> *gprop;
				SimplexPointer v,e,v0,v1,current,start;
				unsigned int t,ct,ci,lg,f0,f1,pos;
				GhostSimplexPointer curr;
				GhostSimplex s;
				bool *b;

				/* First, we check if all is ok! */
	 			assert(ll!=NULL);
	 			assert(this->isValid(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			assert(this->isTop(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			ll->clear();
	 			t=cp.getParentType();
				ct=cp.getChildType();
				ci=cp.getChildId();
				if(cp.isEncoded()==false)
				{
					/* We must have a valid child of a valid top simplex */
					assert(ct<t);
					assert(ci<this->pascal_triangle[t+1][ct+1]);
					if(this->hasProperty(string("cobs"))) this->deleteProperty(string("cobs"));
					gprop=this->addGhostProperty(string("cobs"),false);
					this->ghostSimplex(cp,&s);
					if(ct==0)
					{
						/* Now, we are managing the co-booundary of a vertex! */
						v=SimplexPointer(s.getCBoundary().at(0));
						lg=this->simplex(v).getCoboundarySize();
						for(unsigned int j=0;j<lg;j++)
						{
							/* Now, we take the representative simplex of the 'current' cluster, and understand which type of cluster we have */
							if(this->hasProperty(string("cobs"))) this->deleteProperty(string("cobs"));
							gprop=this->addGhostProperty(string("cobs"),false);
							current=SimplexPointer(this->simplex(v).c(j));
							if(current.type()==1) { ll->push_back(GhostSimplexPointer(1,current.id())); }
							else if(current.type()==2) { this->visitCluster2D(v.id(),SimplexPointer(current),gprop,ll); }
							else { this->visitCluster3D(v.id(),SimplexPointer(current),gprop,ll); }
							this->deleteProperty(string("cobs"));
						}
					}
					else if(ct==1)
					{
						/* Now, we are managing an edge */
						e=SimplexPointer(0,0);
						v0=SimplexPointer(s.getCBoundary().at(0));
						v1=SimplexPointer(s.getCBoundary().at(1));
						if(t==2)
						{
							/* It is a child of a top triangle: does it exist? */
							if(this->simplex(SimplexPointer(t,cp.getParentId())).hasAdjacency(ci)==false)
							{
								ll->push_back(GhostSimplexPointer(t,cp.getParentId()));
								return;
							}
							else
							{
								/* Is it a manifold adjacency? */
								e=SimplexPointer(this->simplex(SimplexPointer(t,cp.getParentId())).a(ci));
								if(e.type()==2)
								{
									ll->push_back(GhostSimplexPointer(t,cp.getParentId()));
									ll->push_back(GhostSimplexPointer(e.type(),e.id()));
									ll->sort(compare_ghost_simplices_pointers);
									return;
								}
							}
						}
						else
						{
							/* It is a child of a tetrahedron */
							if(this->simplex(SimplexPointer(t,cp.getParentId())).isSetUp(1,ci)==true)
							{
								/* The connection is not manifold! */
								this->simplex(SimplexPointer(t,cp.getParentId())).getAuxiliaryBoundarySimplex(1,ci,e);
							}
							else
							{
								/* The connection is manifold */
								f0=this->fposes[3].getHierarchy()[1].at(ci).getCCoboundary().at(0);
								f1=this->fposes[3].getHierarchy()[1].at(ci).getCCoboundary().at(1);
								if(this->simplex(SimplexPointer(t,cp.getParentId())).hasAdjacency(f0)==true) { ; }
								else if(this->simplex(SimplexPointer(t,cp.getParentId())).hasAdjacency(f1)==true) { ; }
								else
								{
									ll->push_back(GhostSimplexPointer(t,cp.getParentId()));
									ll->push_back(GhostSimplexPointer(t,cp.getParentId(),2,f0));
									ll->push_back(GhostSimplexPointer(t,cp.getParentId(),2,f1));
									ll->sort(compare_ghost_simplices_pointers);
									return;	
								}
							}
						}
						
						/* If we arrive here, we can continue! */
						if(e.type()==1)
						{
							/* The current edge is non-manifold, so we must visit clusters incident in the current edge */
							this->ghostSimplex(GhostSimplexPointer(cp),&s);
							start=SimplexPointer(this->simplex(e).cc(1));
							pos=getFace(s,start);
	 						current=SimplexPointer(start);
							do
	 						{
	 							/* We proceed on the next cluster incident at the edge 'cp' */
								if(current.type()==2)
								{
									e=SimplexPointer(this->simplex(SimplexPointer(current)).a(pos));
									ll->push_back(GhostSimplexPointer(current.type(),current.id()));
								}
								else
								{
									queue<SimplexPointer> q;
									
									/* The 'current' simplex is a tetrahedron, so we must visit this cluster! */
									this->simplex(SimplexPointer(current)).getAuxiliaryBoundarySimplex(1,pos,e);
									q.push(SimplexPointer(current.type(),current.id()));
									while(q.empty()==false)
									{
										v=q.front();
										q.pop();
										this->ghostSimplex(GhostSimplexPointer(v.type(),v.id()),&s);
										b=NULL;
										gprop->get(s,&b);
										if((*b)==false)
										{
											/* The tetrahedron 'v' is new, and then we proceed only on triangles incident in 'cp' */
											(*b)=true;
											ll->push_back(GhostSimplexPointer(v.type(),v.id()));
											this->simplex(SimplexPointer(v)).positionInBoundary(SimplexPointer(v0),f0);
											this->simplex(SimplexPointer(v)).positionInBoundary(SimplexPointer(v1),f1);
											for(unsigned int j=0;j<4;j++)
											{
												if((j!=f0) && (j!=f1))
												{
													/* We have to consider the adjacency along the j-th triangle! */
													if(this->simplex(SimplexPointer(v)).hasAdjacency(j)) { q.push(SimplexPointer(this->simplex(SimplexPointer(v)).a(j))); }
													this->ghostSimplex(GhostSimplexPointer(v.type(),v.id(),2,j),&s);		
													b=NULL;
													gprop->get(s,&b);
													if( (*b)==false)
													{
														ll->push_back(GhostSimplexPointer(v.type(),v.id(),2,j));
														(*b)=true;
													}
												}
											}
										}
									}
								}
								
								/* Now, we can proceed on the next cluster */
								this->ghostSimplex(GhostSimplexPointer(current.type(),current.id(),1,pos),&s);
								current=SimplexPointer(this->simplex(e).cc(1));
	 							pos=getFace(s,current);
	 						}
	 						while((current.id()!=start.id()) || (current.type()!=start.type()));
	 					}
						else
						{
							queue<GhostSimplexPointer> q;
							
							/* The current edge is manifold: there are only tetrahera */
							q.push(GhostSimplexPointer(cp.getParentType(),cp.getParentId()));
							while(q.empty()==false)
							{
								curr=q.front();
								q.pop();
								this->ghostSimplex(GhostSimplexPointer(curr),&s);
								b=NULL;
								gprop->get(s,&b);
								if( (*b)==false)
								{	
									/* The ghost simplex pointer 'curr' is new, and then we proceed only on triangles incident in 'cp' */
									(*b)=true;
									ll->push_back(GhostSimplexPointer(curr));
									this->simplex(SimplexPointer(curr.getParentType(),curr.getParentId())).positionInBoundary(SimplexPointer(v0),f0);
									this->simplex(SimplexPointer(curr.getParentType(),curr.getParentId())).positionInBoundary(SimplexPointer(v1),f1);
									for(unsigned int j=0;j<4;j++)
									{
										if((j!=f0) && (j!=f1))
										{
											/* We have to consider the adjacency along the j-th triangle! */
											if(this->simplex(SimplexPointer(curr.getParentType(),curr.getParentId())).hasAdjacency(j)==true)
												{ q.push(GhostSimplexPointer(3,this->simplex(SimplexPointer(curr.getParentType(),curr.getParentId())).a(j).id())); }
											
											/* Now, we add the k-th triangle! */
											this->ghostSimplex(GhostSimplexPointer(curr.getParentType(),curr.getParentId(),2,j),&s);
											b=NULL;
											gprop->get(s,&b);
											if( (*b)==false)
											{
												ll->push_back(GhostSimplexPointer(curr.getParentType(),curr.getParentId(),2,j));
												(*b)=true;
											}
										}
									}
								}
							}
						}
					}
					else
					{
						/* Here, we have a triangle that it is not top: we have a shape embedded in 3D, thus we can immediately retrieve the involved tetrahedra */
						v=SimplexPointer(t,cp.getParentId());
						ll->push_back(GhostSimplexPointer(t,cp.getParentId()));
						if(this->simplex(SimplexPointer(v)).hasAdjacency(ci)) ll->push_back(GhostSimplexPointer(3,this->simplex(v).a(ci).id()));
					}
					
					/* If we arrive here, we can sort the required list of simplices! */
					if(this->hasProperty(string("cobs"))) this->deleteProperty(string("cobs"));
					ll->sort(compare_ghost_simplices_pointers);
				}
	 		}
	 		
	 		/// This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex not directly encoded in the current data structure: this member
	 		 * function filters coboundary simplices against their dimension.<p>The reference simplex and the star simplices can also be not directly encoded in the current data structure and
	 		 * they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>The
	 		 * <i>Non-Manifold IA</i> data structure is a <i>local</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the
	 		 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> when you try to apply this
	 		 * member function, you should be careful about the use of auxiliary information (i.e. subclasses of the mangrove_tds::PropertyBase class) associated to the current simplicial 
			 * complex. In other words, in this member function, we use a ghost property, called <i>cobs</i> and described by an instance of the mangrove_tds::GhostPropertyHandle, for
			 * marking ghost simplices. We remove this property, if it already belongs to the current simplicial complex. In any case, we remove it at the end of this member function.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
			 * \param k the dimension of the required simplices belonging to the input simplex coboundary
			 * \param ll a list of the required simplices belonging to the input simplex coboundary
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase,
	 		 * mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isValid()
			 */
	 		inline void coboundaryk(const GhostSimplexPointer& cp,SIMPLEX_TYPE k,list<GhostSimplexPointer> *ll)
	 		{
	 			unsigned int t,ct,ci,f0,f1,pos,lg;
	 			GhostSimplex s;
	 			GhostPropertyHandle<bool> *gprop;
	 			SimplexPointer v,e,v0,v1,current,start;
				bool *b;
				GhostSimplexPointer curr;

	 			/* First, we check if all is ok! */
	 			assert(ll!=NULL);
	 			assert(this->isValid(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			assert(this->isTop(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			ll->clear();
	 			t=cp.getParentType();
				ct=cp.getChildType();
				ci=cp.getChildId();
				if(cp.isEncoded()==false)
				{
					/* We must have a valid child of a valid top simplex */
					assert(ct<t);
					assert(ci<this->pascal_triangle[t+1][ct+1]);
					assert(k<=this->type());
					assert(k>ct);
					if(this->hasProperty(string("cobs"))) this->deleteProperty(string("cobs"));
					gprop=this->addGhostProperty(string("cobs"),false);
					this->ghostSimplex(cp,&s);
					if(ct==0)
					{
						/* Now, we are managing the co-booundary of a vertex! */
						v=SimplexPointer(s.getCBoundary().at(0));
						lg=this->simplex(v).getCoboundarySize();
						for(unsigned int j=0;j<lg;j++)
						{
							/* Now, we take the representative simplex of the 'current' cluster, and understand which type of cluster we have */
							if(this->hasProperty(string("cobs"))) this->deleteProperty(string("cobs"));
							gprop=this->addGhostProperty(string("cobs"),false);
							current=SimplexPointer(this->simplex(v).c(j));
							if(current.type()==1) { if(k==1) ll->push_back(GhostSimplexPointer(1,current.id())); }
							else if(current.type()==2) { if(k<=2) this->visitCluster2D(v.id(),k,SimplexPointer(current),gprop,ll); }
							else { this->visitCluster3D(v.id(),k,SimplexPointer(current),gprop,ll); }
							this->deleteProperty(string("cobs"));
						}
					}
					else if(ct==1)
					{
						/* Now, we are managing an edge */
						e=SimplexPointer(0,0);
						v0=SimplexPointer(s.getCBoundary().at(0));
						v1=SimplexPointer(s.getCBoundary().at(1));
						if(t==2)
						{
							/* It is a child of a top triangle: does it exist? */
							if(this->simplex(SimplexPointer(t,cp.getParentId())).hasAdjacency(ci)==false)
							{
								if(k==2) ll->push_back(GhostSimplexPointer(t,cp.getParentId()));
								return;
							}
							else
							{
								/* Is it a manifold adjacency? */
								e=SimplexPointer(this->simplex(SimplexPointer(t,cp.getParentId())).a(ci));
								if(e.type()==2)
								{
									if(k==2) ll->push_back(GhostSimplexPointer(t,cp.getParentId()));
									if(k==2) ll->push_back(GhostSimplexPointer(e.type(),e.id()));
									ll->sort(compare_ghost_simplices_pointers);
									return;
								}
							}
						}
						else
						{
							/* It is a child of a tetrahedron */
							if(this->simplex(SimplexPointer(t,cp.getParentId())).isSetUp(1,ci)==true)
							{
								/* The connection is not manifold! */
								this->simplex(SimplexPointer(t,cp.getParentId())).getAuxiliaryBoundarySimplex(1,ci,e);
							}
							else
							{
								/* The connection is manifold */
								f0=this->fposes[3].getHierarchy()[1].at(ci).getCCoboundary().at(0);
								f1=this->fposes[3].getHierarchy()[1].at(ci).getCCoboundary().at(1);
								if(this->simplex(SimplexPointer(t,cp.getParentId())).hasAdjacency(f0)==true) { ; }
								else if(this->simplex(SimplexPointer(t,cp.getParentId())).hasAdjacency(f1)==true) { ; }
								else
								{
									if(k==3) ll->push_back(GhostSimplexPointer(t,cp.getParentId()));
									if(k==2) ll->push_back(GhostSimplexPointer(t,cp.getParentId(),2,f0));
									if(k==2) ll->push_back(GhostSimplexPointer(t,cp.getParentId(),2,f1));
									ll->sort(compare_ghost_simplices_pointers);
									return;	
								}
							}
						}
						
						/* If we arrive here, we can continue! */
						if(e.type()==1)
						{
							/* The current edge is non-manifold, so we must visit clusters incident in the current edge */
							this->ghostSimplex(GhostSimplexPointer(cp),&s);
							start=SimplexPointer(this->simplex(e).cc(1));
							pos=getFace(s,start);
	 						current=SimplexPointer(start);
							do
	 						{
	 							/* We proceed on the next cluster incident at the edge 'cp' */
								if(current.type()==2)
								{
									e=SimplexPointer(this->simplex(SimplexPointer(current)).a(pos));
									if(k==2) ll->push_back(GhostSimplexPointer(current.type(),current.id()));
								}
								else
								{
									queue<SimplexPointer> q;
									
									/* The 'current' simplex is a tetrahedron, so we must visit this cluster! */
									this->simplex(SimplexPointer(current)).getAuxiliaryBoundarySimplex(1,pos,e);
									q.push(SimplexPointer(current.type(),current.id()));
									while(q.empty()==false)
									{
										v=q.front();
										q.pop();
										this->ghostSimplex(GhostSimplexPointer(v.type(),v.id()),&s);
										b=NULL;
										gprop->get(s,&b);
										if((*b)==false)
										{
											/* The tetrahedron 'v' is new, and then we proceed only on triangles incident in 'cp' */
											(*b)=true;
											if(k==3) ll->push_back(GhostSimplexPointer(v.type(),v.id()));
											this->simplex(SimplexPointer(v)).positionInBoundary(SimplexPointer(v0),f0);
											this->simplex(SimplexPointer(v)).positionInBoundary(SimplexPointer(v1),f1);
											for(unsigned int j=0;j<4;j++)
											{
												if((j!=f0) && (j!=f1))
												{
													/* We have to consider the adjacency along the j-th triangle! */
													if(this->simplex(SimplexPointer(v)).hasAdjacency(j)) { q.push(SimplexPointer(this->simplex(SimplexPointer(v)).a(j))); }
													this->ghostSimplex(GhostSimplexPointer(v.type(),v.id(),2,j),&s);		
													b=NULL;
													gprop->get(s,&b);
													if( (*b)==false)
													{
														if(k==2) ll->push_back(GhostSimplexPointer(v.type(),v.id(),2,j));
														(*b)=true;
													}
												}
											}
										}
									}
								}

								/* Now, we can proceed on the next cluster */
								this->ghostSimplex(GhostSimplexPointer(current.type(),current.id(),1,pos),&s);
								current=SimplexPointer(this->simplex(e).cc(1));
	 							pos=getFace(s,current);
	 						}
	 						while((current.id()!=start.id()) || (current.type()!=start.type()));	 							
						}
						else
						{
							queue<GhostSimplexPointer> q;
							
							/* The current edge is manifold: there are only tetrahera */
							q.push(GhostSimplexPointer(cp.getParentType(),cp.getParentId()));
							while(q.empty()==false)
							{
								curr=q.front();
								q.pop();
								this->ghostSimplex(GhostSimplexPointer(curr),&s);
								b=NULL;
								gprop->get(s,&b);
								if( (*b)==false)
								{	
									/* The ghost simplex pointer 'curr' is new, and then we proceed only on triangles incident in 'cp' */
									(*b)=true;
									if(k==3) ll->push_back(GhostSimplexPointer(curr));
									this->simplex(SimplexPointer(curr.getParentType(),curr.getParentId())).positionInBoundary(SimplexPointer(v0),f0);
									this->simplex(SimplexPointer(curr.getParentType(),curr.getParentId())).positionInBoundary(SimplexPointer(v1),f1);
									for(unsigned int j=0;j<4;j++)
									{
										if((j!=f0) && (j!=f1))
										{
											/* We have to consider the adjacency along the j-th triangle! */
											if(this->simplex(SimplexPointer(curr.getParentType(),curr.getParentId())).hasAdjacency(j)==true)
												{ q.push(GhostSimplexPointer(3,this->simplex(SimplexPointer(curr.getParentType(),curr.getParentId())).a(j).id())); }
											
											/* Now, we add the j-th triangle! */
											this->ghostSimplex(GhostSimplexPointer(curr.getParentType(),curr.getParentId(),2,j),&s);
											b=NULL;
											gprop->get(s,&b);
											if( (*b)==false)
											{
												if(k==2) ll->push_back(GhostSimplexPointer(curr.getParentType(),curr.getParentId(),2,j));
												(*b)=true;
											}	
										}
									}
								}
							}
						}
					}
					else
					{
						/* Here, we have a triangle that it is not top: we have a shape embedded in 3D, thus we can immediately retrieve the involved tetrahedra */
						v=SimplexPointer(t,cp.getParentId());
						ll->push_back(GhostSimplexPointer(t,cp.getParentId()));
						if(this->simplex(SimplexPointer(v)).hasAdjacency(ci)) ll->push_back(GhostSimplexPointer(3,this->simplex(v).a(ci).id()));
					}
					
					/* If we arrive here, we can sort the required list of simplices! */
					if(this->hasProperty(string("cobs"))) this->deleteProperty(string("cobs"));
					ll->sort(compare_ghost_simplices_pointers);
				}
	 		}
	 		
	 		/// This member function identifies all simplices adjacent to a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices adjacent to a simplex directly encoded in the current data structure: two 0-simplices are <i>adjacent</i> if they are connected 
			 * by a common edge, while two k-simplices (with k>0) are <i>adjacent</i> if they share a simplex of dimension k-1.<p>The reference simplex and the adjacent simplices are
			 * required to be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They
			 * are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function only with a
			 * <i>global</i> data structure.<p>The <i>Non-Manifold IA</i> data structure is a <i>local</i> data structure, thus we cannot apply this member function for computing the
			 * adjacency of a simplex: you should apply instances of the mangrove_tds::GhostSimplexPointer class for accessing descriptions of simplices not directly encoded in the current
			 * data structure. Consequently, this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation
			 * will result in a failed assert.
	 		 * \param cp the reference simplex directly encoded in the current data structure
			 * \param ll a list of the required simplices belonging to the input simplex adjacency
			 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
			 * mangrove_tds::PropertyBase
			 */
			inline void adjacency(const SimplexPointer &cp,list<SimplexPointer> *ll)
			{
				/* You cannot apply this member function - idle avoids compiler warnings! */
	 			idle(ll);
	 			idle(cp);
	 			assert(this->encodeAllSimplices()==true);
			}

			/// This member function identifies all simplices adjacent to a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices adjacent to a simplex not directly encoded in the current data structure: two 0-simplices are <i>adjacent</i> if they are
			 * connected by a common edge, while two k-simplices (with k>0) are <i>adjacent</i> if they share a simplex of dimension k-1.<p>The reference simplex and the adjacent simplices
			 * can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member
			 * function only with a <i>local</i> data structure.<p>The <i>Non-Manifold IA</i> data structure is a <i>local</i> data structure.<p>If we cannot complete this operation, then
			 * this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.<p><b>IMPORTANT:</b> when you try to apply this member function, you should be careful about the use of auxiliary information (i.e. subclasses of the
			 * mangrove_tds::PropertyBase class) associated to the current simplicial complex. In other words, in this member function, we use a ghost property, called <i>cobs</i> and
			 * described by an instance of the mangrove_tds::GhostPropertyHandle, for marking ghost simplices. We remove this property, if it already belongs to the current simplicial
			 * complex. In any case, we remove it at the end of this member function.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
			 * \param ll a list of the required simplices belonging to the input simplex adjacency and not directly encoded in the current data structure
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase
	 		 */
	 		inline void adjacency(const GhostSimplexPointer &cp,list<GhostSimplexPointer> *ll)
	 		{
	 			unsigned int t,ct,ci,lg;
	 			GhostSimplex s,s1;
				SimplexPointer v,aux,current;
				GhostPropertyHandle<bool> *gprop;
				list<GhostSimplexPointer> l0,l1,l2,b;
				list<GhostSimplexPointer>::iterator it,itb;

	 			/* First, we check if all is ok! */
	 			assert(ll!=NULL);
	 			assert(this->isValid(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			assert(this->isTop(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			ll->clear();
	 			t=cp.getParentType();
				ct=cp.getChildType();
				ci=cp.getChildId();
				if(cp.isEncoded()==false)
				{
					/* The input simplex is a real ghost simplex: we must have a valid child of a valid top simplex! */
					assert(ct<t);
					assert(ci<this->pascal_triangle[t+1][ct+1]);
					this->ghostSimplex(cp,&s);
					if(ct==0)
					{
						/* Now, we are managing the co-booundary of a vertex! */
						v=SimplexPointer(s.getCBoundary().at(0));
						lg=this->simplex(v).getCoboundarySize();
						for(unsigned int j=0;j<lg;j++)
						{
							/* Now, we take the representative simplex of the 'current' cluster, and understand which type of cluster we have */
							if(this->hasProperty(string("cobs"))) this->deleteProperty(string("cobs"));
							gprop=this->addGhostProperty(string("cobs"),false);
							current=SimplexPointer(this->simplex(v).c(j));
							if(current.type()==1) 
							{
								if(this->simplex(current).bc(0).id()==v.id()) ll->push_back(GhostSimplexPointer(1,current.id(),0,1));
								if(this->simplex(current).bc(1).id()==v.id()) ll->push_back(GhostSimplexPointer(1,current.id(),0,0));
							}
							else if(current.type()==2) { this->adjacencyCluster2D(v.id(),SimplexPointer(current),gprop,ll); }
							else { this->adjacencyCluster3D(v.id(),SimplexPointer(current),gprop,ll); }
							this->deleteProperty(string("cobs"));
						}
					}
					else if(ct==1)
					{
						/* Now, we must compute the adjacency of an edge (not top)! */
						this->boundaryk(GhostSimplexPointer(cp),0,&b);
						this->coboundaryk(GhostSimplexPointer(b.front()),1,&l0);
						this->coboundaryk(GhostSimplexPointer(b.back()),1,&l1);
						for(it=l0.begin();it!=l0.end();it++)
						{
							this->ghostSimplex(GhostSimplexPointer(*it),&s1);
							if(s.theSame(s1)==false) ll->push_back(GhostSimplexPointer(*it));
						}

						/* Now, we complete the extraction! */
						for(it=l1.begin();it!=l1.end();it++)
						{
							this->ghostSimplex(GhostSimplexPointer(*it),&s1);
							if(s.theSame(s1)==false) ll->push_back(GhostSimplexPointer(*it));
						}
					}
					else if(ct==2)
					{
						/* Now, we must compute the adjacency of a triangle (not top) */
						this->boundaryk(GhostSimplexPointer(cp),1,&b);
						for(itb=b.begin();itb!=b.end();itb++)
						{
							this->coboundaryk(GhostSimplexPointer(*itb),2,&l0);
							for(it=l0.begin();it!=l0.end();it++)
							{
								this->ghostSimplex(GhostSimplexPointer(*it),&s1);
								if(s.theSame(s1)==false) ll->push_back(GhostSimplexPointer(*it));
							}
						}
					}
				}
				else
				{
					/* The input simplex is a fake ghost simplex, i.e. it is a top simplex! Is it maximal or not? */
					this->ghostSimplex(cp,&s);
					if(ct==3)
					{
						/* Now, we must compute the adjacency of a tetrahedron, directly encoded */
						for(unsigned int j=0;j<4;j++)
						{
							if(this->simplex(SimplexPointer(3,cp.getParentId())).hasAdjacency(j))
							{
								aux=this->simplex(SimplexPointer(3,cp.getParentId())).a(j);
								ll->push_back(GhostSimplexPointer(3,aux.id()));
							}
						}
					}
					else if(ct==2)
					{
						/* Now, we must compute the adjacency of a top face: we analyze faces incident at its edges! */
						this->coboundaryk(GhostSimplexPointer(cp.getParentType(),cp.getParentId(),1,0),2,&l0);
						this->coboundaryk(GhostSimplexPointer(cp.getParentType(),cp.getParentId(),1,1),2,&l1);
						this->coboundaryk(GhostSimplexPointer(cp.getParentType(),cp.getParentId(),1,2),2,&l2);
						for(it=l0.begin();it!=l0.end();it++)
						{
							this->ghostSimplex(GhostSimplexPointer(*it),&s1);
							if(s.theSame(s1)==false) ll->push_back(GhostSimplexPointer(*it));
						}
						
						/* Now, we analyze adjacency along the second edge! */
						for(it=l1.begin();it!=l1.end();it++)
						{
							this->ghostSimplex(GhostSimplexPointer(*it),&s1);
							if(s.theSame(s1)==false) ll->push_back(GhostSimplexPointer(*it));
						}
						
						/* Now, we analyze adjacency along the third edge! */
						for(it=l2.begin();it!=l2.end();it++)
						{
							this->ghostSimplex(GhostSimplexPointer(*it),&s1);
							if(s.theSame(s1)==false) ll->push_back(GhostSimplexPointer(*it));
						}
					}
					else if(ct==1)
					{
						/* Now, we must compute the adjacency of a wire edge! */
						this->coboundaryk(GhostSimplexPointer(cp.getParentType(),cp.getParentId(),0,0),1,&l0);
						this->coboundaryk(GhostSimplexPointer(cp.getParentType(),cp.getParentId(),0,1),1,&l1);
						for(it=l0.begin();it!=l0.end();it++)
						{
							this->ghostSimplex(GhostSimplexPointer(*it),&s1);
							if(s.theSame(s1)==false) ll->push_back(GhostSimplexPointer(*it));
						}

						/* Now, we complete the extraction! */
						for(it=l1.begin();it!=l1.end();it++)
						{
							this->ghostSimplex(GhostSimplexPointer(*it),&s1);
							if(s.theSame(s1)==false) ll->push_back(GhostSimplexPointer(*it));
						}
					}
				}
				
				/* If we arrive here, we can sort the required list of simplices! */
				ll->sort(compare_ghost_simplices_pointers);
	 		}
	 		
	 		/// This member function identifies all simplices belonging to the link of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the link of a simplex directly encoded in the current data structure: let <i>c</i> a simplex in the current
			 * simplicial complex, then the <i>link</i> of <i>c</i> is the set of all the subsimplices of the simplices in the star of <i>c</i> that are not incident to <i>c</i>.<p>The
			 * reference simplex and the required simplices must be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by
			 * the garbage collector mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can
			 * invoke this member function only with a <i>global</i> data structure.<p>The <i>Non-Manifold IA</i> data structure is a <i>local</i> data structure, thus we cannot apply this
			 * member function for computing the adjacency of a simplex: you should apply instances of the mangrove_tds::GhostSimplexPointer class for accessing descriptions of simplices not
			 * directly encoded in the current data structure. Consequently, this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this
			 * case, each forbidden operation will result in a failed assert.
	 		 * \param cp the reference simplex directly encoded in the current data structure
	 		 * \param ll a list of the required simplices belonging to the input simplex link
	 		 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplexPointer, mangrove_tds::BaseSimplicialComplex::type(), mangrove_tds::PropertyBase
	 		 */
	 		inline void link(const SimplexPointer& cp,list<SimplexPointer> *ll)
	 		{
	 			/* Dummy statements for avoiding warnings! */
	 			idle(cp);
	 			idle(ll);
	 			assert(this->encodeAllSimplices()==true);
	 		}
	 		
	 		/// This member function identifies all simplices belonging to the link of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the link of a simplex not directly encoded in the current data structure: let <i>c</i> a simplex in the current
			 * simplicial complex, then the <i>link</i> of <i>c</i> is the set of all the subsimplices of the simplices in the star of <i>c</i> that are not incident to <i>c</i>.<p>The
			 * reference simplex and the required simplices can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer
			 * class.Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>The <i>Non-Manifold IA</i> data structure is a <i>local</i> data
			 * structure.<p>If we cannot complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode.
			 * In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b>when you try to apply this member function, you should be careful about the use of
			 * auxiliary information (i.e. subclasses of the mangrove_tds::PropertyBase class) associated to the current simplicial complex. In other words, in this member function, we use a
			 * ghost property, called <i>lnk</i> and described by an instance of the mangrove_tds::GhostPropertyHandle, for marking ghost simplices. We remove this property, if it already
			 * belongs to the current simplicial complex. In any case, we remove it at the end of this member function.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
	 		 * \param ll a list of the required simplices belonging to the input simplex link and not directly encoded in the current data structure
	 		 * \see mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::SIMPLEX_TYPE,
	 		 * mangrove_tds::SIMPLEX_ID, mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle
	 		 */
	 		inline void link(const GhostSimplexPointer& cp,list<GhostSimplexPointer> *ll)
	 		{
	 			unsigned int t,ct,ci,lg,pos,f0,f1;
	 			GhostSimplex s,s1,s2,s3;
				SimplexPointer v,current,e,v0,v1,start;
				GhostPropertyHandle<bool> *gprop;
				bool *b;

	 			/* First, we check if all is ok! */
	 			assert(ll!=NULL);
	 			assert(this->isValid(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			assert(this->isTop(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			ll->clear();
	 			t=cp.getParentType();
				ct=cp.getChildType();
				ci=cp.getChildId();
				if(cp.isEncoded()==false)
				{
					/* We must have a valid child of a valid top simplex! */
					assert(ct<t);
					assert(ci<this->pascal_triangle[t+1][ct+1]);
					this->ghostSimplex(cp,&s);
					if(this->hasProperty(string("lnk"))) this->deleteProperty(string("lnk"));
					gprop=this->addGhostProperty(string("lnk"),false);
					if(ct==0)
					{
						/* Now, we are managing the link of a vertex! */
						v=SimplexPointer(s.getBoundary()[0]);
						lg=this->simplex(v).getCoboundarySize();
						for(unsigned int j=0;j<lg;j++)
						{
							/* Now, we take the representative simplex of the 'current' cluster, and understand which type of cluster we have */
							if(this->hasProperty(string("lnk"))) this->deleteProperty(string("lnk"));
							gprop=this->addGhostProperty(string("lnk"),false);
							current=SimplexPointer(this->simplex(v).c(j));
							if(current.type()==1)
							{
								if(this->simplex(current).bc(0).id()==v.id()) ll->push_back(GhostSimplexPointer(current.type(),current.id(),0,1));
								else ll->push_back(GhostSimplexPointer(current.type(),current.id(),0,0));
							}
							else if(current.type()==2) { this->linkVSCluster2D(v.id(),SimplexPointer(current),gprop,ll); }
							else { this->linkVSCluster3D(v.id(),SimplexPointer(current),gprop,ll); }
							this->deleteProperty(string("lnk"));
						}
					}
					else if(ct==1)
					{
						/* The ghost simplex is an edge! */
						e=SimplexPointer(0,0);
						v0=SimplexPointer(s.getCBoundary().at(0));
						v1=SimplexPointer(s.getCBoundary().at(1));
						if(t==2)
						{
							/* It is a child of a top triangle: how many vertices we must add? */
							ll->push_back(GhostSimplexPointer(t,cp.getParentId(),0,ci));
							this->ghostSimplex(GhostSimplexPointer(t,cp.getParentId(),0,ci),&s1);
							gprop->get(s1,&b);
							(*b)=true;
							if(this->simplex(SimplexPointer(t,cp.getParentId())).hasAdjacency(ci)==false) { return; }
							else
							{
								/* Is it a manifold adjacency? */
								e=SimplexPointer(this->simplex(SimplexPointer(t,cp.getParentId())).a(ci));
								if(e.type()==2)
								{
									/* Yes, it is a manifold adjacency with an other top triangle! */
									pos=this->getFace(s,e);
									ll->push_back(GhostSimplexPointer(e.type(),e.id(),0,pos));
									ll->sort(compare_ghost_simplices_pointers);
									return;
								}
							}
						}
						else
						{
							/* It is a child of a tetrahedron */
							if(this->simplex(SimplexPointer(t,cp.getParentId())).isSetUp(1,ci)==true)
								{ this->simplex(SimplexPointer(t,cp.getParentId())).getAuxiliaryBoundarySimplex(1,ci,e); }
							else
							{
								/* The connection is manifold */
								f0=this->fposes[3].getHierarchy()[1].at(ci).getCCoboundary().at(0);
								f1=this->fposes[3].getHierarchy()[1].at(ci).getCCoboundary().at(1);
								if(this->simplex(SimplexPointer(t,cp.getParentId())).hasAdjacency(f0)==true) { ; }
								else if(this->simplex(SimplexPointer(t,cp.getParentId())).hasAdjacency(f1)==true) { ; }
								else
								{
									this->ghostSimplex(GhostSimplexPointer(t,cp.getParentId()),&s1);
									s2.clear();
									for(unsigned int j=0;j<4;j++)
									{
										if(s1.getCBoundary().at(j).id()==v0.id()) { ; }
										else if(s1.getCBoundary().at(j).id()==v1.id()) { ; }
										else
										{
											s2.getBoundary().push_back(SimplexPointer(s1.getCBoundary().at(j)));
											ll->push_back(GhostSimplexPointer(t,cp.getParentId(),0,j));
										} 
									}
									
									/* Here, we can add the edge opposite to 'cp' */
									ll->push_back(GhostSimplexPointer(t,cp.getParentId(),1,this->getFace(s2,SimplexPointer(t,cp.getParentId()))));
									ll->sort(compare_ghost_simplices_pointers);
									return;
								}
							}
						}
						
						/* If we arrive here, we can continue! */
						if(e.type()==1)
						{
							/* The current edge is non-manifold, so we must visit clusters incident in the current edge */
							this->ghostSimplex(GhostSimplexPointer(cp),&s);
							start=SimplexPointer(this->simplex(e).cc(1));
							pos=getFace(s,start);
	 						current=SimplexPointer(start);
	 						do
	 						{
	 							/* We proceed on the next cluster incident at the edge 'cp' */
								if(current.type()==2)
								{
									/* The 'current' cluster is a top triangle! */
									e=SimplexPointer(this->simplex(SimplexPointer(current)).a(pos));
									this->ghostSimplex(GhostSimplexPointer(current.type(),current.id(),0,pos),&s1);
									b=NULL;
									gprop->get(s1,&b);
									if((*b)==false)
									{
										(*b)=true;
										ll->push_back(GhostSimplexPointer(current.type(),current.id(),0,pos));
									}
								}
								else
								{
									queue<SimplexPointer> q;
									
									/* The 'current' simplex is a tetrahedron, so we must visit this cluster! */
									this->simplex(SimplexPointer(current)).getAuxiliaryBoundarySimplex(1,pos,e);
									q.push(SimplexPointer(current));
									while(q.empty()==false)
									{
										/* Now, we extract the tetrahedron 'v' and then we check if it is new! */
										v=q.front();
										q.pop();
										this->ghostSimplex(GhostSimplexPointer(v.type(),v.id()),&s1);
										b=NULL;
										gprop->get(s1,&b);
										if((*b)==false)
										{
											/* The tetrahedron 'v' is new, thus we can visit 'v' */
											(*b)=true;
											this->simplex(SimplexPointer(v)).positionInBoundary(SimplexPointer(v0),f0);
											this->simplex(SimplexPointer(v)).positionInBoundary(SimplexPointer(v1),f1);
											for(unsigned int j=0;j<4;j++)
											{
												/* We have to consider the adjacency along the j-th triangle! */
												if((j!=f0) && (j!=f1))
													{ if(this->simplex(SimplexPointer(v)).hasAdjacency(j)) q.push(SimplexPointer(this->simplex(SimplexPointer(v)).a(j))); }
											}
											
											/* Now, we consider the contribution of 'v' to the link of 'cp' */
											s2.clear();
											for(unsigned int j=0;j<4;j++)
											{
												if(s1.getCBoundary().at(j).id()==v0.id()) { ; }
												else if(s1.getCBoundary().at(j).id()==v1.id()) { ; }
												else
												{
													s2.getBoundary().push_back(SimplexPointer(s1.getCBoundary().at(j)));
													s3.clear();
													s3.getBoundary().push_back(SimplexPointer(s1.getCBoundary().at(j)));
													b=NULL;
													gprop->get(s3,&b);
													if( (*b)==false)
													{
														(*b)=true;
														ll->push_back(GhostSimplexPointer(v.type(),v.id(),0,this->getFace(s3,SimplexPointer(v))));
													}
												}
											}
											
											/* Finally, we add the opposite edge to 'cp' */
											b=NULL;
											gprop->get(s2,&b);
											if( (*b)==false)
											{
												(*b)=true;
												ll->push_back(GhostSimplexPointer(v.type(),v.id(),1,this->getFace(s2,SimplexPointer(v))));
											}
										}
									}
								}

	 							/* Now, we can proceed on the next cluster */
								this->ghostSimplex(GhostSimplexPointer(current.type(),current.id(),1,pos),&s);
								current=SimplexPointer(this->simplex(e).cc(1));
	 							pos=getFace(s,current);
	 						}
	 						while((current.id()!=start.id()) || (current.type()!=start.type()));
						}
						else
						{
							queue<SimplexPointer> q;
							
							/* The current edge is manifold: there are only tetrahera */
							q.push(SimplexPointer(cp.getParentType(),cp.getParentId()));
							while(q.empty()==false)
							{
								current=q.front();
								q.pop();
								this->ghostSimplex(GhostSimplexPointer(current.type(),current.id()),&s1);
								b=NULL;
								gprop->get(s1,&b);
								if( (*b)==false)
								{	
									/* The ghost simplex pointer 'curr' is new, and then we proceed only on triangles incident in 'cp' */
									(*b)=true;
									this->simplex(SimplexPointer(current)).positionInBoundary(SimplexPointer(v0),f0);
									this->simplex(SimplexPointer(current)).positionInBoundary(SimplexPointer(v1),f1);
									for(unsigned int j=0;j<4;j++)
									{
										if((j!=f0) && (j!=f1))
										{
											/* We have to consider the adjacency along the j-th triangle! */
											if(this->simplex(SimplexPointer(current)).hasAdjacency(j)) { q.push(SimplexPointer(this->simplex(SimplexPointer(current)).a(j))); }
										}
									}
									
									/* Now, we consider the contribution of 'current' to the link of 'cp' */
									s2.clear();
									for(unsigned int j=0;j<4;j++)
									{
										if(s1.getCBoundary().at(j).id()==v0.id()) { ; }
										else if(s1.getCBoundary().at(j).id()==v1.id()) { ; }
										else
										{
											s2.getBoundary().push_back(SimplexPointer(s1.getCBoundary().at(j)));
											s3.clear();
											s3.getBoundary().push_back(SimplexPointer(s1.getCBoundary().at(j)));
											b=NULL;
											gprop->get(s3,&b);
											if( (*b)==false)
											{
												(*b)=true;
												ll->push_back(GhostSimplexPointer(current.type(),current.id(),0,j));
											}
										} 
									}
									
									/* Finally, we add the opposite edge to 'cp' */
									b=NULL;
									gprop->get(s2,&b);
									if( (*b)==false)
									{
										(*b)=true;
										ll->push_back(GhostSimplexPointer(current.type(),current.id(),1,this->getFace(s2,SimplexPointer(current))));
									}	
								}
							}
						}
					}
					else if(ct==2)
					{
						/* The ghost simplex is a face, shared by two tetrahedra (at most) */
						ll->push_back(GhostSimplexPointer(t,cp.getParentId(),0,ci));
						v=SimplexPointer(t,cp.getParentId());
						if(this->simplex(SimplexPointer(t,cp.getParentId())).hasAdjacency(ci))
						{
							v=SimplexPointer(this->simplex(SimplexPointer(t,cp.getParentId())).a(ci));
							ll->push_back(GhostSimplexPointer(v.type(),v.id(),0,this->getFace(s,SimplexPointer(v))));
						}
					}
					
					/* If we arrive here, we can sort the required list of simplices! */
					if(this->hasProperty(string("lnk"))) this->deleteProperty(string("lnk"));
					ll->sort(compare_ghost_simplices_pointers);
				}
	 		}
	 		
	 		/// This member function returns the number of components in the link of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function returns the number of components in the link of a simplex directly encoded in the current data structure: let <i>c</i> a simplex in the current simplicial
	 		 * complex, then the <i>link</i> of <i>c</i> is the set of all the subsimplices of the simplices in the star of <i>c</i> that are not incident to <i>c</i>.<p>The reference simplex
	 		 * and the required simplices must be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage
			 * collector mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this
			 * member function only with a <i>global</i> data structure.<p>The <i>Non-Manifold IA</i> data structure is a <i>local</i> data structure, thus we cannot apply this member
			 * function for computing the number of components in the link of a simplex: you should apply instances of the mangrove_tds::GhostSimplexPointer class for accessing descriptions
			 * of simplices not directly encoded in the current data structure. Consequently, this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in
			 * debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \param cc the reference simplex directly encoded in the current data structure
	 		 * \param lk a list of the required simplices belonging to the input simplex link
	 		 * \return the number of components in the link of a simplex directly encoded in the current data structure
	 		 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase
	 		 */
	 		inline unsigned int getLinkComponentsNumber(const SimplexPointer &cc,list<SimplexPointer> *lk)
			{
				/* You cannot apply this member function - idle avoids compiler warnings! */
	 			idle(lk);
	 			idle(cc);
	 			assert(this->encodeAllSimplices()==true);
	 			return 0;
			}
			
			/// This member function returns the number of components in the link of a simplex not directly encoded in the current data structure.
			/**
			 * This member function returns the number of components in the link of a simplex not directly encoded in the current data structure: let <i>c</i> a simplex in the current
			 * simplicial complex, then the <i>link</i> of <i>c</i> is the set of all the subsimplices of the simplices in the star of <i>c</i> that are not incident in <i>c</i>.<p>The
			 * reference simplex and the required simplices can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer
			 * class. Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>The <i>Non-Manifold IA</i> data structure is a <i>local</i> data
			 * structure.<p>If we cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case,
			 * each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> when you try to apply this member function, you should be careful about the use of auxiliary
			 * information (i.e. subclasses of the mangrove_tds::PropertyBase class) associated to the current simplicial complex. We remove such properties, if they already
			 * belong to the current simplicial complex. In any case, we remove them at the end of this member function.
	 		 * \param cc the reference simplex not directly encoded in the current data structure
	 		 * \param lk a list of the required simplices belonging to the input simplex link
	 		 * \return the number of components in the link of a simplex not directly encoded in the current data structure
	 		 * \see mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::SIMPLEX_TYPE,
	 		 * mangrove_tds::SIMPLEX_ID, mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle
			 */
			inline unsigned int getLinkComponentsNumber(const GhostSimplexPointer &cc,list<GhostSimplexPointer> *lk)
			{
				unsigned int n,t,ct,ci;
				GhostSimplex s;
				SimplexPointer v;

				/* First, we check if all is ok! */
				assert(lk!=NULL);
				assert(this->isValid(SimplexPointer(cc.getParentType(),cc.getParentId())));
	 			assert(this->isTop(SimplexPointer(cc.getParentType(),cc.getParentId())));
	 			lk->clear();
	 			n=0;
	 			t=cc.getParentType();
				ct=cc.getChildType();
				ci=cc.getChildId();
				if(cc.isEncoded()==false)
				{
					/* We have an internal simplex to be analyzed: first, we check if the input simplex is a vertex! */
					assert(ct<t);
					assert(ci<this->pascal_triangle[t+1][ct+1]);
					this->link(GhostSimplexPointer(cc),lk);
					if(ct==0)
					{
						this->ghostSimplex(GhostSimplexPointer(cc),&s);
						v=SimplexPointer(s.getCBoundary()[0]);
						n=this->simplex(SimplexPointer(v)).getCoboundarySize();
					}
					else if(ct==1)
					{
						/* An edge can be shared by a list of edge-based clusters! */
						if(t==2)
						{
							/* The input edge is child of a top triangle */
							if(this->simplex(SimplexPointer(t,cc.getParentId())).hasAdjacency(ci)==false) { return 1; }
							else
							{
								v=SimplexPointer(this->simplex(SimplexPointer(t,cc.getParentId())).a(ci));
								if(v.type()==2) return 2;
								else return this->retrieveClustersNumber(SimplexPointer(t,cc.getParentId()),ci);
							}
						}
						else
						{
							/* The input edge is a child of a tetrahedron! */
							if(this->simplex(SimplexPointer(t,cc.getParentId())).isSetUp(1,ci)==false) { return 1; }
							else return this->retrieveClustersNumber(SimplexPointer(t,cc.getParentId()),ci);
						}
					}
					else
					{
						/* Here, we have a triangle that it is not top: we have a shape embedded in 3D, thus we can immediately retrieve the involved tetrahedra */
						v=SimplexPointer(t,cc.getParentId());
						n=n+1;
						if(this->simplex(SimplexPointer(v)).hasAdjacency(ci)) n=n+1;
					}

					/* If we arrive here, we can return 'n' */
					return n;
				}
				else { return 0; }
			}
			
			/// This member function checks if a simplex directly encoded in the current data structure is manifold.
			/**
			 * This member function checks if a simplex directly encoded in the current data structure is manifold: we say that a simplex is <i>manifold</i> if and only if its neighborhood is
			 * homeomorphic to a connected component of a sphere, otherwise it is called <i>non-manifold</i>.<p>The reference simplex must be <i>valid</i>, i.e. directly stored in the
			 * current data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. It is identified by instances of the
			 * mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>The
			 * <i>Non-Manifold IA</i> data structure is a <i>local</i> data structure, thus we cannot apply this member function for checking if a simplex is manifold: you should apply
			 * instances of the mangrove_tds::GhostSimplexPointer class for accessing descriptions of simplices not directly encoded in the current data structure. Consequently, this member
			 * function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param cp the reference simplex directly encoded in the current data structure
			 * \return <ul><li><i>true</i>, if the required simplex is manifold</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
			 * mangrove_tds::PropertyBase
			 */
			inline bool isManifold(const SimplexPointer& cp)
	 		{
	 			/* You cannot apply this member function - idle avoids compiler warnings! */
	 			idle(cp);
	 			assert(this->encodeAllSimplices()==true);
	 			return false;
	 		}
	 		
	 		/// This member function checks if a simplex not directly encoded in the current data structure is manifold.
			/**
			 * This member function checks if a simplex not directly encoded in the current data structure is manifold: we say that a simplex is <i>manifold</i> if and only if its
			 * neighborhood is homeomorphic to a connected component of a sphere, otherwise it is called <i>non-manifold</i>.<p>The reference simplex and the required simplices can also be
			 * not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member function
			 * only with a <i>local</i> data structure.<p>The <i>Non-Manifold IA</i> data structure is a <i>local</i> data structure.<p>If we cannot complete this operation, then this member
			 * function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.<p><b>IMPORTANT:</b> when you try to apply this member function, you should be careful about the use of auxiliary information (i.e. subclasses of the
			 * mangrove_tds::PropertyBase class) associated to the current simplicial complex. In other words, in this member function, we use a ghost property, called <i>cobs</i> and
			 * described by an instance of the mangrove_tds::GhostPropertyHandle, for marking ghost simplices. We remove this property, if it already belongs to the current simplicial
			 * complex. In any case, we remove it at the end of this member function.
			 * \param cp the reference simplex not directly encoded in the current data structure
			 * \return <ul><li><i>true</i>, if the required simplex is manifold</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase,
			 * mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::LocalPropertyHandle
	 		 */
	 		inline bool isManifold(const GhostSimplexPointer& cp)
			{
				unsigned int t,ct,ci,n;
				GhostSimplex s;
				SimplexPointer c;
				list<GhostSimplexPointer> cob;
				list<GhostSimplexPointer>::iterator it;

				/* First, we check if all is ok! */
				assert(this->isValid(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			assert(this->isTop(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			t=cp.getParentType();
				ct=cp.getChildType();
				ci=cp.getChildId();
				if(cp.isEncoded()==false)
				{
					/* We must have a valid child of a valid top simplex! */
					assert(ct<t);
					assert(ci<this->pascal_triangle[t+1][ct+1]);
					switch(ct)
					{
						case 0:
						
							/* The current ghost simplex is a vertex! */
							this->ghostSimplex(cp,&s);
							c=SimplexPointer(0,s.getBoundary().at(0).id());
							n=this->simplex(SimplexPointer(c)).getCoboundarySize();
							if(n>2) { return false; }
							else if(n==2)
							{
								/* If the two connected components are 1-dimensional, then this vertex is manifold! */
								if(this->simplex(SimplexPointer(c)).cc(0).type()!=1) return false;
								if(this->simplex(SimplexPointer(c)).cc(1).type()!=1) return false;
								return true;
							}
							else if(n==1)
							{
								/* First, we check if the unique cluster is 1-dimensional! */
								if(this->simplex(SimplexPointer(c)).cc(0).type()==1) return true;
								else
								{
									/* Now, we must check if all the incident edges are manifold! */
									this->coboundaryk(GhostSimplexPointer(cp),1,&cob);
									for(it=cob.begin();it!=cob.end();it++) { if(this->isManifold(GhostSimplexPointer(*it))==false) return false; }
									return true;
								}
							}
							else { return true; }

						case 1:
						
							/* The current ghost simplex is an edge: it directly marked in the NMIA, if it is manifold */
							if(t==2) { return this->simplex(SimplexPointer(t,cp.getParentId())).isManifoldAdjacency(ci); }
							else { return (this->simplex(SimplexPointer(t,cp.getParentId())).isSetUp(1,ci)==false); }
							
						default:
						
							/* In the default case, we have a manifold simplex */
							return true;
					};
				}
				else { return true; }
			}

			protected:
			
			/// This member function returns the position of a ghost simplex in the boundary of a top simplex.
	 		/**
	 		 * This member function returns the position of a ghost simplex in the boundary of a top simplex: the position is local against the list of subfaces of the same dimension for the
			 * input top simplex.
	 		 * \param s a raw description of the input ghost simplex
	 		 * \param cc a pointer to the input top simplex
	 		 * \return the position of the input ghost simplex in the boundary of the input top simplex.
	 		 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::SIMPLEX_ID
	 		 */
	 		inline unsigned int getFace(const GhostSimplex &s, const SimplexPointer &cc)
	 		{
	 			unsigned int k,p;
	 			vector<SIMPLEX_ID> pos;
	 			
	 			/* First, we express the input ghost simplex as canonical vertices (against cc) */
	 			for(k=0;k<s.getCBoundary().size();k++)
	 			{
	 				this->simplex(SimplexPointer(cc)).positionInBoundary(SimplexPointer(s.getCBoundary().at(k)),p);
	 				pos.push_back(p);
	 			}

	 			/* Now, we check the input face between the subfaces of 'cc', plus a dummy return! */
	 			for(k=0;k<this->fposes[cc.type()].getHierarchy()[s.getCBoundary().size()-1].size();k++) 
	 				{ if(pos==this->fposes[cc.type()].getHierarchy()[s.getCBoundary().size()-1].at(k).getCVertices()) return k; }
	 			return 0;
	 		}
	 		
	 		/// This member function returns all top triangles incident along a non-manifold edge directly encoded in the current data structure.
	 		/**
	 		 * This member function returns all top triangles incident along a non-manifold edge directly encoded in the current data structure. Here, we encode only a pointer to the previous
	 		 * cluster and the next cluster surrounding a non-manifold edge in a clockwise orientation. Thus, we must retrieve the complete list of top triangles incident at a non-manifold
			 * edge.
	 		 * \param cp a pointer to the input top triangle, to whom the input non-manifold edge belongs
	 		 * \param k the position of the input non-manifold edge in the input top triangle
	 		 * \param q a queue where we can store the required top triangles
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::NMIA::getFace(), mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer 
	 		 */
	 		inline void retrieveTopTriangles(const SimplexPointer& cp, unsigned int k, queue<SimplexPointer> *q)
	 		{
	 			unsigned int pos;
	 			SimplexPointer curr,e;
	 			GhostSimplex s;

	 			/* First, we start from the initial simplex and then we move on the adjacent clusters (rebuild the list of adjacent clusters) */
	 			pos=k;
	 			curr=SimplexPointer(cp);
	 			do
	 			{
	 				/* We proceed on the next cluster! */
	 				e=SimplexPointer(this->simplex(curr).a(pos));
	 				this->ghostSimplex(GhostSimplexPointer(curr.type(),curr.id(),1,pos),&s);	 				
	 				curr=SimplexPointer(this->simplex(e).cc(1));
	 				pos=getFace(s,curr);
	 				if(curr.id()!=cp.id()) { q->push(SimplexPointer(curr)); }
	 			}
	 			while(curr.id()!=cp.id());
	 		}
	 		
	 		/// This member function retrieves all clusters incident at a non-manifold edge directly encoded in the current data structure.
	 		/**
	 		 * This member function retrieves all clusters incident at a non-manifold edge directly encoded in the current data structure. Here, we encode only a pointer to the previous
			 * cluster and the next cluster surrounding a non-manifold edge in a clockwise orientation.
	 		 * \param cp a pointer to the input top simplex, to whom the input non-manifold edge belongs
	 		 * \param k the position of the input non-manifold edge in the input top simplex
	 		 * \param c the set of all clusters
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::NMIA::getFace(), mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::COMPLEX
	 		 */
	 		inline void retrieveClusters(const SimplexPointer& cp,unsigned int k,COMPLEX &c)
	 		{
	 			GhostSimplex s;
				SimplexPointer e,start,curr;
				unsigned int pos;

	 			/* First, we must consider the starting cluster! */
	 			this->ghostSimplex(GhostSimplexPointer(cp.type(),cp.id(),1,k),&s);
	 			if(cp.type()==2) e=SimplexPointer(this->simplex(SimplexPointer(cp)).a(k));
	 			else this->simplex(SimplexPointer(cp)).getAuxiliaryBoundarySimplex(1,k,e);
	 			start=SimplexPointer(this->simplex(e).cc(1));
				pos=getFace(s,start);
	 			curr=SimplexPointer(start);
	 			do
	 			{
	 				c.insert(SimplexPointer(curr));
	 				if(curr.type()==2) e=SimplexPointer(this->simplex(SimplexPointer(curr)).a(pos));
	 				else this->simplex(SimplexPointer(curr)).getAuxiliaryBoundarySimplex(1,pos,e);				
	 				curr=SimplexPointer(this->simplex(e).cc(1));
	 				pos=getFace(s,curr);
	 			}
	 			while((curr.id()!=start.id()) || (curr.type()!=start.type()));
	 		}
	 		
	 		/// This member function retrieves the number of all clusters at a non-manifold edge directly encoded in the current data structure.
	 		/**
	 		 * This member function retrieves the number of all clusters at a non-manifold edge directly encoded in the current data structure. Here, we encode only a pointer to the previous
	 		 * cluster and the next cluster surrounding a non-manifold edge in a clockwise orientation.
	 		 * \param cp a pointer to the input top simplex, to whom the input non-manifold edge belongs
	 		 * \param k the position of the input non-manifold edge in the input top simplex
	 		 * \return the number of all clusters at a non-manifold edge directly encoded in the current data structure
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::NMIA::getFace(), mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::COMPLEX
	 		 */
	 		inline unsigned int retrieveClustersNumber(const SimplexPointer& cp,unsigned int k)
	 		{
	 			GhostSimplex s;
				SimplexPointer e,start,curr;
				unsigned int pos,n;
				
				/* First, we must consider the starting cluster! */
	 			this->ghostSimplex(GhostSimplexPointer(cp.type(),cp.id(),1,k),&s);
	 			if(cp.type()==2) e=SimplexPointer(this->simplex(SimplexPointer(cp)).a(k));
	 			else this->simplex(SimplexPointer(cp)).getAuxiliaryBoundarySimplex(1,k,e);
	 			start=SimplexPointer(this->simplex(e).cc(1));
				pos=getFace(s,start);
	 			curr=SimplexPointer(start);
	 			n=0;
	 			do
	 			{
	 				n=n+1;
	 				if(curr.type()==2) e=SimplexPointer(this->simplex(SimplexPointer(curr)).a(pos));
	 				else this->simplex(SimplexPointer(curr)).getAuxiliaryBoundarySimplex(1,pos,e);				
	 				curr=SimplexPointer(this->simplex(e).cc(1));
	 				pos=getFace(s,curr);
	 			}
	 			while((curr.id()!=start.id()) || (curr.type()!=start.type()));
	 			return n;
	 		}

	 		/// This member function performs a visit of an edge-based cluster formed only by top triangles incident at a vertex.
	 		/**
	 		 * This member function performs a visit of an edge-based cluster formed only by top triangles incident at a vertex.
	 		 * \param v the identifier of the required vertex
	 		 * \param current a pointer to the representative top triangle for the cluster to be analyzed
	 		 * \param gprop a property for marking top triangles visited during this process
	 		 * \param ll a list which contains all simplices visited during this process
	 		 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointer, mangrove_tds::GhostPropertyHandle, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
	 		 */
	 		inline void visitCluster2D(SIMPLEX_ID v,const SimplexPointer& current,GhostPropertyHandle<bool> *gprop,list<GhostSimplexPointer> *ll)
	 		{
	 			queue<SimplexPointer> q;
				SimplexPointer aaa,e;
				bool *b;
				GhostSimplex s;
				unsigned int pos,f0,f1;

	 			/* The 'current' cluster is formed only by top triangles! */
	 			q.push(SimplexPointer(current));
	 			while(q.empty()==false)
				{
					aaa=q.front();
					q.pop();
					this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id()),&s);
					b=NULL;
					gprop->get(s,&b);
					if((*b)==false)
					{
						/* The simplex 'aaa' is new, now we add its edges incident at 'v' and then we proceed our visit on faces incident at 'v' */
						(*b)=true;
						ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id()));
						this->simplex(SimplexPointer(aaa)).positionInBoundary(SimplexPointer(0,v),pos);
						f0=this->fposes[2].getHierarchy()[0].at(pos).getCCoboundary().at(0);
						this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),1,f0),&s);
						b=NULL;
						gprop->get(s,&b);
						if((*b)==false)
						{
							(*b)=true;
							ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),1,f0));
							if(this->simplex(SimplexPointer(aaa)).hasAdjacency(f0)==true)
							{
								e=SimplexPointer(this->simplex(SimplexPointer(aaa)).a(f0));
								if(e.type()!=1) { q.push(SimplexPointer(e.type(),e.id())); }
								else { this->retrieveTopTriangles(SimplexPointer(aaa),f0,&q); }
							}
						}
						
						/* Now, we analyze the adjacency along the second edge incident at 'v' */
						f1=this->fposes[2].getHierarchy()[0].at(pos).getCCoboundary().at(1);
						this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),1,f1),&s);
						b=NULL;
						gprop->get(s,&b);
						if((*b)==false)
						{
							(*b)=true;
							ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),1,f1));
							if(this->simplex(SimplexPointer(aaa)).hasAdjacency(f1)==true)
							{
								e=SimplexPointer(this->simplex(SimplexPointer(aaa)).a(f1));
								if(e.type()!=1) { q.push(SimplexPointer(e.type(),e.id())); }
								else { this->retrieveTopTriangles(SimplexPointer(aaa),f1,&q); }
							}
						}
					}
				}
	 		}
	 		
	 		/// This member function performs a visit of an edge-based cluster formed only by top triangles incident at a vertex.
	 		/**
	 		 * This member function performs a visit of an edge-based cluster formed only by top triangles incident at a vertex, selecting only a subset of simplices.
	 		 * \param v the identifier of the required vertex
	 		 * \param k the dimension of the required simplices
	 		 * \param current a pointer to the representative top triangle for the cluster to be analyzed
	 		 * \param gprop a property for marking top triangles visited during this process
	 		 * \param ll a list which contains all simplices visited during this process
	 		 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::GhostPropertyHandle, mangrove_tds::GhostSimplexPointer,
	 		 * mangrove_tds::GhostSimplex
	 		 */
	 		inline void visitCluster2D(SIMPLEX_ID v,SIMPLEX_TYPE k,const SimplexPointer& current,GhostPropertyHandle<bool> *gprop,list<GhostSimplexPointer> *ll)
	 		{
	 			queue<SimplexPointer> q;
	 			SimplexPointer aaa,e;
				bool *b;
				GhostSimplex s;
				unsigned int pos,f0,f1;

	 			/* The 'current' cluster is formed only by top triangles! */
	 			q.push(SimplexPointer(current));
	 			while(q.empty()==false)
				{
					aaa=q.front();
					q.pop();
					this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id()),&s);
					b=NULL;
					gprop->get(s,&b);
					if((*b)==false)
					{
						/* The simplex 'aaa' is new, now we add its edges incident at 'v' and then we proceed our visit on faces incident at 'v' */
						(*b)=true;
						if(k==aaa.type()) ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id()));
						this->simplex(SimplexPointer(aaa)).positionInBoundary(SimplexPointer(0,v),pos);
						f0=this->fposes[2].getHierarchy()[0].at(pos).getCCoboundary().at(0);
						this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),1,f0),&s);
						b=NULL;
						gprop->get(s,&b);
						if((*b)==false)
						{
							(*b)=true;
							if(k==1) ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),1,f0));
							if(this->simplex(SimplexPointer(aaa)).hasAdjacency(f0)==true)
							{
								e=SimplexPointer(this->simplex(SimplexPointer(aaa)).a(f0));
								if(e.type()!=1) { q.push(SimplexPointer(e.type(),e.id())); }
								else { this->retrieveTopTriangles(SimplexPointer(aaa),f0,&q); }
							}
						}
						
						/* Now, we analyze the adjacency along the second edge incident at 'v' */
						f1=this->fposes[2].getHierarchy()[0].at(pos).getCCoboundary().at(1);
						this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),1,f1),&s);
						b=NULL;
						gprop->get(s,&b);
						if((*b)==false)
						{
							(*b)=true;
							if(k==1) ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),1,f1));
							if(this->simplex(SimplexPointer(aaa)).hasAdjacency(f1)==true)
							{
								e=SimplexPointer(this->simplex(SimplexPointer(aaa)).a(f1));
								if(e.type()!=1) { q.push(SimplexPointer(e.type(),e.id())); }
								else { this->retrieveTopTriangles(SimplexPointer(aaa),f1,&q); }
							}
						}
					}
				}
	 		}

	 		/// This member function performs a visit of an edge-based cluster represented by a top triangle.
	 		/**
	 		 * This member function performs a visit of an edge-based cluster represented by a top triangle and incident at a vertex.
	 		 * \param v the identifier of the required vertex
	 		 * \param current a pointer to the representative top triangle for the cluster to be analyzed
	 		 * \param gprop a property for marking simplices visited during this process
	 		 * \param ll a list which contains all simplices visited during this process
	 		 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointer, mangrove_tds::GhostPropertyHandle, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::COMPLEX
	 		 */
	 		inline void visitTopTriangle(SIMPLEX_ID v,const SimplexPointer& current,GhostPropertyHandle<bool> *gprop,list<GhostSimplexPointer> *ll)
	 		{
	 			queue<SimplexPointer> q;
	 			GhostSimplex s;
	 			bool *b;
	 			SimplexPointer aaa,e;
				COMPLEX c;
				unsigned int pos,f0,f1;

	 			/* First, we check if this cluster has been visited! */
	 			this->ghostSimplex(GhostSimplexPointer(current.type(),current.id()),&s);
	 			b=NULL;
	 			gprop->get(s,&b);
	 			if((*b)==false)
				{
					/* The 'current' cluster has not been visited! */
					q.push(SimplexPointer(current));
					while(q.empty()==false)
					{
						aaa=q.front();
						q.pop();
						if(this->isVisited(SimplexPointer(aaa))==false)
						{
							/* Now, we are visiting an other top 2-simplex! */
							this->visit(SimplexPointer(aaa),true);
							this->simplex(SimplexPointer(aaa)).positionInBoundary(SimplexPointer(0,v),pos);
							this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id()),&s);
							b=NULL;
	 						gprop->get(s,&b);
	 						if((*b)==false)
							{
								(*b)=true;
								ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id()));
							}
							
							/* Now, we visit the first edge of 'aaa' incident at 'v' */	
							f0=this->fposes[2].getHierarchy()[0].at(pos).getCCoboundary().at(0);
							this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),1,f0),&s);
							b=NULL;
							gprop->get(s,&b);
							if((*b)==false)
							{
								(*b)=true;
								ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),1,f0));
								if(this->simplex(SimplexPointer(aaa)).hasAdjacency(f0)==true)
								{
									e=SimplexPointer(this->simplex(SimplexPointer(aaa)).a(f0));
									if(e.type()!=1) { q.push(SimplexPointer(e.type(),e.id())); }
									else { this->retrieveClusters(SimplexPointer(aaa),f0,c); }
								}
							}
							
							/* Now, we visit the second edge of 'aaa' incident at 'v' */	
							f1=this->fposes[2].getHierarchy()[0].at(pos).getCCoboundary().at(1);
							this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),1,f1),&s);
							b=NULL;
							gprop->get(s,&b);
							if((*b)==false)
							{
								(*b)=true;
								ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),1,f1));
								if(this->simplex(SimplexPointer(aaa)).hasAdjacency(f1)==true)
								{
									e=SimplexPointer(this->simplex(SimplexPointer(aaa)).a(f1));
									if(e.type()!=1) { q.push(SimplexPointer(e.type(),e.id())); }
									else { this->retrieveClusters(SimplexPointer(aaa),f1,c); }
								}
							}
						}
					}
					
					/* If we arrive here, we have visited all simplices in the current cluster! */
					this->unmarkAllVisited();
					for(COMPLEX::iterator it=c.begin();it!=c.end();it++)
					{
						/* Now, we discard the 'current' cluster */
						if((it->type()!=current.type()) || (it->id()!=current.id()))
						{
							/* Is its representative simplex a tetrahedron or a top simplex? */
							if(it->type()==3) { this->visitCluster3D(v,SimplexPointer(*it),gprop,ll); }
							else { this->visitTopTriangle(v,SimplexPointer(*it),gprop,ll); }
						}
					}
				}
	 		}
	 		
	 		/// This member function performs a visit of an edge-based cluster represented by a top triangle.
	 		/**
	 		 * This member function performs a visit of an edge-based cluster represented by a top triangle and incident at a vertex, selecting a subset of simplices.
	 		 * \param v the identifier of the required vertex
	 		 * \param k the dimension of the required simplices
	 		 * \param current a pointer to the representative top triangle for the cluster to be analyzed
	 		 * \param gprop a property for marking simplices visited during this process
	 		 * \param ll a list which contains all simplices visited during this process
	 		 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::GhostPropertyHandle, mangrove_tds::GhostSimplexPointer,
	 		 * mangrove_tds::GhostSimplex, mangrove_tds::COMPLEX
	 		 */
	 		inline void visitTopTriangle(SIMPLEX_ID v,SIMPLEX_TYPE k,const SimplexPointer& current,GhostPropertyHandle<bool> *gprop,list<GhostSimplexPointer> *ll)
	 		{
	 			queue<SimplexPointer> q;
	 			GhostSimplex s;
	 			bool *b;
	 			SimplexPointer aaa,e;
				COMPLEX c;
				unsigned int pos,f0,f1;
				
				/* First, we check if this cluster has been visited! */
	 			this->ghostSimplex(GhostSimplexPointer(current.type(),current.id()),&s);
	 			b=NULL;
	 			gprop->get(s,&b);
	 			if((*b)==false)
				{
					/* The 'current' cluster has not been visited! */
					q.push(SimplexPointer(current));
					while(q.empty()==false)
					{
						aaa=q.front();
						q.pop();
						if(this->isVisited(SimplexPointer(aaa))==false)
						{
							/* Now, we are visiting an other top 2-simplex! */
							this->visit(SimplexPointer(aaa),true);
							this->simplex(SimplexPointer(aaa)).positionInBoundary(SimplexPointer(0,v),pos);
							this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id()),&s);
							b=NULL;
	 						gprop->get(s,&b);
	 						if((*b)==false)
							{
								(*b)=true;
								if(k==aaa.type()) ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id()));
							}	
							
							/* Now, we visit the first edge of 'aaa' incident at 'v' */	
							f0=this->fposes[2].getHierarchy()[0].at(pos).getCCoboundary().at(0);
							this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),1,f0),&s);
							b=NULL;
							gprop->get(s,&b);
							if((*b)==false)
							{
								(*b)=true;
								if(k==1) ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),1,f0));
								if(this->simplex(SimplexPointer(aaa)).hasAdjacency(f0)==true)
								{
									e=SimplexPointer(this->simplex(SimplexPointer(aaa)).a(f0));
									if(e.type()!=1) { q.push(SimplexPointer(e.type(),e.id())); }
									else { this->retrieveClusters(SimplexPointer(aaa),f0,c); }
								}
							}
							
							/* Now, we visit the second edge of 'aaa' incident at 'v' */	
							f1=this->fposes[2].getHierarchy()[0].at(pos).getCCoboundary().at(1);
							this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),1,f1),&s);
							b=NULL;
							gprop->get(s,&b);
							if((*b)==false)
							{
								(*b)=true;
								if(k==1) ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),1,f1));
								if(this->simplex(SimplexPointer(aaa)).hasAdjacency(f1)==true)
								{
									e=SimplexPointer(this->simplex(SimplexPointer(aaa)).a(f1));
									if(e.type()!=1) { q.push(SimplexPointer(e.type(),e.id())); }
									else { this->retrieveClusters(SimplexPointer(aaa),f1,c); }
								}
							}
						}
					}
					
					/* If we arrive here, we have visited all simplices in the current cluster! */
					this->unmarkAllVisited();
					for(COMPLEX::iterator it=c.begin();it!=c.end();it++)
					{
						/* Now, we discard the 'current' cluster */
						if((it->type()!=current.type()) || (it->id()!=current.id()))
						{
							/* Is its representative simplex a tetrahedron or a top simplex? */
							if(it->type()==3) { this->visitCluster3D(v,k,SimplexPointer(*it),gprop,ll); }
							else { this->visitTopTriangle(v,k,SimplexPointer(*it),gprop,ll); }
						}
					}
				}
	 		}
	
	 		/// This member function performs a visit of an edge-based cluster represented by a tetrahedron.
	 		/**
	 		 * This member function performs a visit of an edge-based cluster represented by a tetrahedron and incident at a vertex.<p><b>IMPORTANT:</b> such a cluster can be formed by
	 		 * parts of different dimensionalities.
	 		 * \param v the identifier of the required vertex
	 		 * \param current a pointer to the representative tetrahedron for the cluster to be analyzed
	 		 * \param gprop a property for marking simplices visited during this process
	 		 * \param ll a list which contains all simplices visited during this process
	 		 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointer, mangrove_tds::GhostPropertyHandle, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::COMPLEX
	 		 */
	 		inline void visitCluster3D(SIMPLEX_ID v,const SimplexPointer& current,GhostPropertyHandle<bool> *gprop,list<GhostSimplexPointer> *ll)
	 		{
	 			GhostSimplex s;
	 			bool *b;
	 			queue<SimplexPointer> q;
	 			SimplexPointer aaa;
				unsigned int pos,idc;
				COMPLEX c;

	 			/* First, we check if this cluster has been visited! */
	 			this->ghostSimplex(GhostSimplexPointer(current.type(),current.id()),&s);
	 			b=NULL;
	 			gprop->get(s,&b);
	 			if((*b)==false)
				{
					/* We must visit the 'current' cluster! */
					q.push(SimplexPointer(current));
					while(q.empty()==false)
					{
						aaa=q.front();
						q.pop();
						if(this->isVisited(SimplexPointer(aaa))==false)
						{
							/* Now, we are visiting an other tetraedron! */
							this->visit(SimplexPointer(aaa),true);
							this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id()),&s);
							b=NULL;
	 						gprop->get(s,&b);
	 						if((*b)==false)
							{
								(*b)=true;
								ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id()));
							}
							
							/* Now, we proceed on faces adjacent at 'v' */
							this->simplex(SimplexPointer(aaa)).positionInBoundary(SimplexPointer(0,v),pos);
							for(unsigned int j=0;j<4;j++)
							{
								/* Is it the j-th face to be considered? */
								if(j!=pos)
								{
									/* Does exist an adjacency along the j-th face of 'aaa'? */
									if(this->simplex(SimplexPointer(aaa)).hasAdjacency(j)) { q.push(SimplexPointer(this->simplex(SimplexPointer(aaa)).a(j))); }
									this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),2,j),&s);
									b=NULL;
	 								gprop->get(s,&b);
	 								if((*b)==false)
									{
										(*b)=true;
										ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),2,j));
									}
								}
							}
							
							/* Now, we analyze the edges incident at 'v' */
							for(unsigned int z=0;z<3;z++)
							{
								/* Now, we consider the z-th edge incident at 'v' */
								idc=this->fposes[3].getHierarchy()[0].at(pos).getCCoboundary().at(z);
								if(this->simplex(SimplexPointer(aaa)).isSetUp(1,idc)==true) { this->retrieveClusters(SimplexPointer(aaa),idc,c); }
								this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),1,idc),&s);
								b=NULL;
								gprop->get(s,&b);
								if((*b)==false)
								{
									(*b)=true;
									ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),1,idc));
								}
							}
						}
					}
					
					/* If we arrive here, we have visited all simplices in the current cluster! */
					this->unmarkAllVisited();
					for(COMPLEX::iterator it=c.begin();it!=c.end();it++)
					{
						/* Now, we discard the 'current' cluster */
						if((it->type()!=current.type()) || (it->id()!=current.id()))
						{
							/* Is its representative simplex a tetrahedron or a top simplex? */
							if(it->type()==3) { this->visitCluster3D(v,SimplexPointer(*it),gprop,ll); }
							else { this->visitTopTriangle(v,SimplexPointer(*it),gprop,ll); }
						}
					}
				}		
	 		}
	 		
	 		/// This member function performs a visit of an edge-based cluster represented by a tetrahedron.
	 		/**
	 		 * This member function performs a visit of an edge-based cluster represented by a tetrahedron and incident at a vertex, selecting a subset of simplices.<p><b>IMPORTANT:</b>
	 		 * such a cluster can be formed by parts of different dimensionalities.
	 		 * \param v the identifier of the required vertex
	 		 * \param k the dimension of the required simplices
	 		 * \param current a pointer to the representative tetrahedron for the cluster to be analyzed
	 		 * \param gprop a property for marking simplices visited during this process
	 		 * \param ll a list which contains all simplices visited during this process
	 		 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointer, mangrove_tds::GhostPropertyHandle, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::COMPLEX, mangrove_tds::SIMPLEX_TYPE
	 		 */
	 		inline void visitCluster3D(SIMPLEX_ID v,SIMPLEX_TYPE k,const SimplexPointer& current,GhostPropertyHandle<bool> *gprop,list<GhostSimplexPointer> *ll)
	 		{
	 			GhostSimplex s;
	 			bool *b;
	 			queue<SimplexPointer> q;
	 			SimplexPointer aaa;
				unsigned int pos,idc;
				COMPLEX c;
				
				/* First, we check if this cluster has been visited! */
	 			this->ghostSimplex(GhostSimplexPointer(current.type(),current.id()),&s);
	 			b=NULL;
	 			gprop->get(s,&b);
	 			if((*b)==false)
				{
					/* We must visit the 'current' cluster! */
					q.push(SimplexPointer(current));
					while(q.empty()==false)
					{
						aaa=q.front();
						q.pop();
						if(this->isVisited(SimplexPointer(aaa))==false)
						{
							/* Now, we are visiting an other tetraedron! */
							this->visit(SimplexPointer(aaa),true);
							this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id()),&s);
							b=NULL;
	 						gprop->get(s,&b);
	 						if((*b)==false)
							{
								(*b)=true;
								if(k==aaa.type()) ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id()));
							}
					
							/* Now, we proceed on faces adjacent at 'v' */
							this->simplex(SimplexPointer(aaa)).positionInBoundary(SimplexPointer(0,v),pos);
							for(unsigned int j=0;j<4;j++)
							{
								/* Is it the j-th face to be considered? */
								if(j!=pos)
								{
									/* Does exist an adjacency along the j-th face of 'aaa'? */
									if(this->simplex(SimplexPointer(aaa)).hasAdjacency(j)) { q.push(SimplexPointer(this->simplex(SimplexPointer(aaa)).a(j))); }
									this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),2,j),&s);
									b=NULL;
	 								gprop->get(s,&b);
	 								if((*b)==false)
									{
										(*b)=true;
										if(k==2) ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),2,j));
									}
								}
							}
							
							/* Now, we analyze the edges incident at 'v' */
							for(unsigned int z=0;z<3;z++)
							{
								/* Now, we consider the z-th edge incident at 'v' */
								idc=this->fposes[3].getHierarchy()[0].at(pos).getCCoboundary().at(z);
								if(this->simplex(SimplexPointer(aaa)).isSetUp(1,idc)==true) { this->retrieveClusters(SimplexPointer(aaa),idc,c); }
								this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),1,idc),&s);
								b=NULL;
								gprop->get(s,&b);
								if((*b)==false)
								{
									(*b)=true;
									if(k==1) ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),1,idc));
								}
							}
						}
					}
					
					/* If we arrive here, we have visited all simplices in the current cluster! */
					this->unmarkAllVisited();
					for(COMPLEX::iterator it=c.begin();it!=c.end();it++)
					{
						/* Now, we discard the 'current' cluster */
						if((it->type()!=current.type()) || (it->id()!=current.id()))
						{
							/* Is its representative simplex a tetrahedron or a top simplex? */
							if(it->type()==3) { this->visitCluster3D(v,k,SimplexPointer(*it),gprop,ll); }
							else { this->visitTopTriangle(v,k,SimplexPointer(*it),gprop,ll); }
						}
					}
				}
	 		}
	 		
	 		/// This member function retrieves the contribution of a cluster represented by a top triangle to the link of a vertex.
	 		/**
	 		 * This member function retrieves the contribution of a cluster represented by a top triangle to the link of a vertex.
	 		 * \param v the identifier of the required vertex
	 		 * \param current a pointer to the representative top triangle for the cluster to be analyzed
	 		 * \param gprop a property for marking simplices visited during this process
	 		 * \param ll a list which contains all simplices visited during this process
	 		 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointer, mangrove_tds::GhostPropertyHandle, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::COMPLEX 
	 		 */
	 		inline void linkVSCluster2D(SIMPLEX_ID v,const SimplexPointer& current,GhostPropertyHandle<bool> *gprop,list<GhostSimplexPointer> *ll)
	 		{
	 			SimplexPointer aaa,e;
	 			queue<SimplexPointer> q;
	 			bool *b;
				GhostSimplex s;
				unsigned int pos,f0,f1;

	 			/* The 'current' cluster is formed only by top triangles! */
	 			q.push(SimplexPointer(current));
	 			while(q.empty()==false)
				{
					aaa=q.front();
					q.pop();
					this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id()),&s);
					this->simplex(SimplexPointer(aaa)).positionInBoundary(SimplexPointer(0,v),pos);
					b=NULL;
					gprop->get(s,&b);
					if((*b)==false)
					{
						/* The simplex 'aaa' is new, now we add the edge opposite at 'v' and then we proceed our visit on faces incident at 'v' */
						(*b)=true;
						f0=this->fposes[2].getHierarchy()[0].at(pos).getCCoboundary().at(0);
						if(this->simplex(SimplexPointer(aaa)).hasAdjacency(f0)==true)
						{
							e=SimplexPointer(this->simplex(SimplexPointer(aaa)).a(f0));
							if(e.type()!=1) { q.push(SimplexPointer(e.type(),e.id())); }
							else { this->retrieveTopTriangles(SimplexPointer(aaa),f0,&q); }
						}
						
						/* Now, we analyze the adjacency along the second edge incident at 'v' */
						f1=this->fposes[2].getHierarchy()[0].at(pos).getCCoboundary().at(1);
						if(this->simplex(SimplexPointer(aaa)).hasAdjacency(f1)==true)
						{
							e=SimplexPointer(this->simplex(SimplexPointer(aaa)).a(f1));
							if(e.type()!=1) { q.push(SimplexPointer(e.type(),e.id())); }
							else { this->retrieveTopTriangles(SimplexPointer(aaa),f1,&q); }
						}
						
						/* Now, we add the first vertex 'f0' */
						this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),0,f0),&s);
						b=NULL;
	 					gprop->get(s,&b);
	 					if((*b)==false)
	 					{
	 						(*b)=true;
	 						ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),0,f0));
	 					}
							
						/* Now, we add the second vertex 'f1' */
						this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),0,f1),&s);
						b=NULL;
	 					gprop->get(s,&b);
	 					if((*b)==false)
	 					{
	 						(*b)=true;
	 						ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),0,f1));
	 					}
	 					
	 					/* Now, we add the edge 'pos' */
	 					this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),1,pos),&s);
						b=NULL;
	 					gprop->get(s,&b);
	 					if((*b)==false)
	 					{
	 						(*b)=true;
	 						ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),1,pos));
	 					}
					}
				}
	 		}

	 		/// This member function retrieves all vertices adjacent to a vertex belonging to an edge-based cluster represented by a top triangle.
	 		/**
	 		 * This member function retrieves all vertices adjacent to a vertex belonging to an edge-based cluster represented by a top triangle.
	 		 * \param v the identifier of the required vertex
	 		 * \param current a pointer to the representative top triangle for the cluster to be analyzed
	 		 * \param gprop a property for marking simplices visited during this process
	 		 * \param ll a list which contains all simplices visited during this process
	 		 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointer, mangrove_tds::GhostPropertyHandle, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::COMPLEX
	 		 */
	 		inline void adjacencyCluster2D(SIMPLEX_ID v,const SimplexPointer& current,GhostPropertyHandle<bool> *gprop,list<GhostSimplexPointer> *ll)
	 		{
	 			queue<SimplexPointer> q;
				SimplexPointer aaa,e;
				bool *b;
				GhostSimplex s;
				unsigned int pos,f0,f1;
				
				/* The 'current' cluster is formed only by top triangles! */
	 			q.push(SimplexPointer(current));
	 			while(q.empty()==false)
				{
					aaa=q.front();
					q.pop();
					this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id()),&s);
					b=NULL;
					gprop->get(s,&b);
					if((*b)==false)
					{
						/* The simplex 'aaa' is new, now we add its edges incident at 'v' and then we proceed our visit on faces incident at 'v' */
						(*b)=true;
						this->simplex(SimplexPointer(aaa)).positionInBoundary(SimplexPointer(0,v),pos);
						f0=this->fposes[2].getHierarchy()[0].at(pos).getCCoboundary().at(0);
						if(this->simplex(SimplexPointer(aaa)).hasAdjacency(f0)==true)
						{
							e=SimplexPointer(this->simplex(SimplexPointer(aaa)).a(f0));
							if(e.type()!=1) { q.push(SimplexPointer(e.type(),e.id())); }
							else { this->retrieveTopTriangles(SimplexPointer(aaa),f0,&q); }
						}
						
						/* Now, we analyze the adjacency along the second edge incident at 'v' */
						f1=this->fposes[2].getHierarchy()[0].at(pos).getCCoboundary().at(1);
						if(this->simplex(SimplexPointer(aaa)).hasAdjacency(f1)==true)
						{
							e=SimplexPointer(this->simplex(SimplexPointer(aaa)).a(f1));
							if(e.type()!=1) { q.push(SimplexPointer(e.type(),e.id())); }
							else { this->retrieveTopTriangles(SimplexPointer(aaa),f1,&q); }
						}
						
						/* Now, we add the first vertex 'f0' */
						this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),0,f0),&s);
						b=NULL;
	 					gprop->get(s,&b);
	 					if((*b)==false)
	 					{
	 						(*b)=true;
	 						ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),0,f0));
	 					}
							
						/* Now, we add the second vertex 'f1' */
						this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),0,f1),&s);
						b=NULL;
	 					gprop->get(s,&b);
	 					if((*b)==false)
	 					{
	 						(*b)=true;
	 						ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),0,f1));
	 					}
					}
				}
	 		}
	 		
	 		/// This member function retrieves the contribution of a cluster represented by a tetrahedron to the link of a vertex.
	 		/**
	 		 * This member function retrieves the contribution of a cluster represented by a tetrahedron to the link of a vertex.
	 		 * \param v the identifier of the required vertex
	 		 * \param current a pointer to the representative tetrahedron for the cluster to be analyzed
	 		 * \param gprop a property for marking simplices visited during this process
	 		 * \param ll a list which contains all simplices visited during this process
	 		 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointer, mangrove_tds::GhostPropertyHandle, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::COMPLEX 
	 		 */
	 		inline void linkVSCluster3D(SIMPLEX_ID v,const SimplexPointer& current,GhostPropertyHandle<bool> *gprop,list<GhostSimplexPointer> *ll)
	 		{
	 			GhostSimplex s;
	 			bool *b;
	 			queue<SimplexPointer> q;
	 			SimplexPointer aaa;
				COMPLEX c;
				unsigned int pos,idc;
				list<GhostSimplexPointer> bnd;

	 			/* First, we check if this cluster has been visited! */
	 			this->ghostSimplex(GhostSimplexPointer(current.type(),current.id()),&s);
	 			b=NULL;
	 			gprop->get(s,&b);
	 			if((*b)==false)
				{
					/* We must visit the 'current' cluster! */
					q.push(SimplexPointer(current));
					while(q.empty()==false)
					{
						aaa=q.front();
						q.pop();
						if(this->isVisited(SimplexPointer(aaa))==false)
						{
							/* Now, we are visiting an other tetraedron! */
							this->visit(SimplexPointer(aaa),true);
							this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id()),&s);
							b=NULL;
	 						gprop->get(s,&b);
	 						if((*b)==false) { (*b)=true; }
							this->simplex(SimplexPointer(aaa)).positionInBoundary(SimplexPointer(0,v),pos);
							for(unsigned int j=0;j<4;j++)
							{
								/* Is it the j-th face to be considered? */
								if(j!=pos)
								{
									if(this->simplex(SimplexPointer(aaa)).hasAdjacency(j)) q.push(SimplexPointer(this->simplex(SimplexPointer(aaa)).a(j)));
									b=NULL;
	 								this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),0,j),&s);
	 								gprop->get(s,&b);
	 								if((*b)==false)
	 								{
	 									(*b)=true;
	 									ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),0,j));
	 								}
								}
								else
								{
									/* We should add the face 'pos' to the required link */
									b=NULL;
									this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),2,pos),&s);
	 								gprop->get(s,&b);
	 								if((*b)==false)
	 								{
	 									(*b)=true;
	 									ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),2,pos));
	 								}
	 								
	 								/* Now, we consider edges on the boundary of the 'pos' face */
	 								this->boundaryk(GhostSimplexPointer(aaa.type(),aaa.id(),2,pos),1,&bnd);
	 								for(list<GhostSimplexPointer>::iterator it=bnd.begin();it!=bnd.end();it++)
	 								{
	 									b=NULL;
	 									this->ghostSimplex(GhostSimplexPointer(*it),&s);
	 									gprop->get(s,&b);
	 									if((*b)==false)
	 									{
	 										(*b)=true;
	 										ll->push_back(GhostSimplexPointer(*it));
	 									}
	 								}
								}
							}
							
							/* Now, we analyze the edges incident at 'v' */
							for(unsigned int z=0;z<3;z++)
							{
								/* Now, we consider the z-th edge incident at 'v' */
								idc=this->fposes[3].getHierarchy()[0].at(pos).getCCoboundary().at(z);
								if(this->simplex(SimplexPointer(aaa)).isSetUp(1,idc)==true) { this->retrieveClusters(SimplexPointer(aaa),idc,c); }
							}
						}
					}
					
					/* If we arrive here, we have visited all simplices in the current cluster! */
					this->unmarkAllVisited();
					for(COMPLEX::iterator it=c.begin();it!=c.end();it++)
					{
						/* Now, we discard the 'current' cluster */
						if((it->type()!=current.type()) || (it->id()!=current.id()))
						{
							/* Is its representative simplex a tetrahedron or a top simplex? */
							if(it->type()==3) { this->linkVSCluster3D(v,SimplexPointer(*it),gprop,ll); }
							else { this->linkVSTopTriangle(v,SimplexPointer(*it),gprop,ll); }
						}
					}
				}
	 		}
	 		
	 		/// This member function retrieves all vertices adjacent to a vertex belonging to an edge-based cluster represented by a tetrahedron.
	 		/**
	 		 * This member function retrieves all vertices adjacent to a vertex belonging to an edge-based cluster represented by a tetrahedron.<p><b>IMPORTANT:</b> such a cluster can be
	 		 * formed by parts of different dimensionalities.
	 		 * \param v the identifier of the required vertex
	 		 * \param current a pointer to the representative tetrahedron for the cluster to be analyzed
	 		 * \param gprop a property for marking simplices visited during this process
	 		 * \param ll a list which contains all simplices visited during this process
	 		 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointer, mangrove_tds::GhostPropertyHandle, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::COMPLEX
	 		 */
	 		inline void adjacencyCluster3D(SIMPLEX_ID v,const SimplexPointer& current,GhostPropertyHandle<bool> *gprop,list<GhostSimplexPointer> *ll)
	 		{
	 			GhostSimplex s;
	 			bool *b;
	 			queue<SimplexPointer> q;
	 			SimplexPointer aaa;
				unsigned int pos,idc;
				COMPLEX c;
				
	 			/* First, we check if this cluster has been visited! */
	 			this->ghostSimplex(GhostSimplexPointer(current.type(),current.id()),&s);
	 			b=NULL;
	 			gprop->get(s,&b);
	 			if((*b)==false)
				{
					/* We must visit the 'current' cluster! */
					q.push(SimplexPointer(current));
					while(q.empty()==false)
					{
						aaa=q.front();
						q.pop();
						if(this->isVisited(SimplexPointer(aaa))==false)
						{
							/* Now, we are visiting an other tetraedron! */
							this->visit(SimplexPointer(aaa),true);
							this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id()),&s);
							b=NULL;
	 						gprop->get(s,&b);
	 						if((*b)==false) { (*b)=true; }
	 						this->simplex(SimplexPointer(aaa)).positionInBoundary(SimplexPointer(0,v),pos);
	 						for(unsigned int j=0;j<4;j++)
							{
								/* Is it the j-th face to be considered? */
								if(j!=pos)
								{
									/* Does exist an adjacency along the j-th face of 'aaa'? */
									if(this->simplex(SimplexPointer(aaa)).hasAdjacency(j)) { q.push(SimplexPointer(this->simplex(SimplexPointer(aaa)).a(j))); }
	 								b=NULL;
	 								this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),0,j),&s);
	 								gprop->get(s,&b);
	 								if((*b)==false)
	 								{
	 									(*b)=true;
	 									ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),0,j));
	 								}
								}
							}
							
							/* Now, we analyze the edges incident at 'v' */
							for(unsigned int z=0;z<3;z++)
							{
								/* Now, we consider the z-th edge incident at 'v' */
								idc=this->fposes[3].getHierarchy()[0].at(pos).getCCoboundary().at(z);
								if(this->simplex(SimplexPointer(aaa)).isSetUp(1,idc)==true) { this->retrieveClusters(SimplexPointer(aaa),idc,c); }
							}
						}
					}
					
					/* If we arrive here, we have visited all simplices in the current cluster! */
					this->unmarkAllVisited();
					for(COMPLEX::iterator it=c.begin();it!=c.end();it++)
					{
						/* Now, we discard the 'current' cluster */
						if((it->type()!=current.type()) || (it->id()!=current.id()))
						{
							/* Is its representative simplex a tetrahedron or a top simplex? */
							if(it->type()==3) { this->adjacencyCluster3D(v,SimplexPointer(*it),gprop,ll); }
							else { this->adjacencyTopTriangle(v,SimplexPointer(*it),gprop,ll); }
						}
					}
				}
	 		}

	 		/// This member function retrieves all vertices adjacent to a vertex belonging to an edge-based cluster represented by a top triangle.
	 		/**
	 		 * This member function retrieves all vertices adjacent to a vertex belonging to an edge-based cluster represented by a top triangle.
	 		 * \param v the identifier of the required vertex
	 		 * \param current a pointer to the representative top triangle for the cluster to be analyzed
	 		 * \param gprop a property for marking simplices visited during this process
	 		 * \param ll a list which contains all simplices visited during this process
	 		 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointer, mangrove_tds::GhostPropertyHandle, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::COMPLEX
	 		 */
	 		inline void adjacencyTopTriangle(SIMPLEX_ID v,const SimplexPointer& current,GhostPropertyHandle<bool> *gprop,list<GhostSimplexPointer> *ll)
	 		{
	 			queue<SimplexPointer> q;
	 			bool *b;
	 			GhostSimplex s;
	 			COMPLEX c;
	 			SimplexPointer aaa,e;
	 			unsigned int pos,f0,f1;

	 			/* First, we check if this cluster has been visited! */
	 			this->ghostSimplex(GhostSimplexPointer(current.type(),current.id()),&s);
	 			b=NULL;
	 			gprop->get(s,&b);
	 			if((*b)==false)
				{
					/* The 'current' cluster has not been visited! */
					q.push(SimplexPointer(current));
					while(q.empty()==false)
					{
						aaa=q.front();
						q.pop();
						if(this->isVisited(SimplexPointer(aaa))==false)
						{
							/* Now, we are visiting an other top 2-simplex! */
							this->visit(SimplexPointer(aaa),true);
							this->simplex(SimplexPointer(aaa)).positionInBoundary(SimplexPointer(0,v),pos);
							this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id()),&s);
							b=NULL;
	 						gprop->get(s,&b);
	 						if((*b)==false) { (*b)=true; }
	 						f0=this->fposes[2].getHierarchy()[0].at(pos).getCCoboundary().at(0);
	 						if(this->simplex(SimplexPointer(aaa)).hasAdjacency(f0)==true)
							{
								e=SimplexPointer(this->simplex(SimplexPointer(aaa)).a(f0));
								if(e.type()!=1) { q.push(SimplexPointer(e.type(),e.id())); }
								else { this->retrieveClusters(SimplexPointer(aaa),f0,c); }
							}
							
							/* Now, we visit the second edge of 'aaa' incident at 'v' */
							f1=this->fposes[2].getHierarchy()[0].at(pos).getCCoboundary().at(1);
							if(this->simplex(SimplexPointer(aaa)).hasAdjacency(f1)==true)
							{
								e=SimplexPointer(this->simplex(SimplexPointer(aaa)).a(f1));
								if(e.type()!=1) { q.push(SimplexPointer(e.type(),e.id())); }
								else { this->retrieveClusters(SimplexPointer(aaa),f1,c); }
							}
							
							/* Now, we add the first vertex 'f0' */
							this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),0,f0),&s);
							b=NULL;
	 						gprop->get(s,&b);
	 						if((*b)==false)
	 						{
	 							(*b)=true;
	 							ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),0,f0));
	 						}
							
							/* Now, we add the second vertex 'f1' */
							this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),0,f1),&s);
							b=NULL;
	 						gprop->get(s,&b);
	 						if((*b)==false)
	 						{
	 							(*b)=true;
	 							ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),0,f1));
	 						}	
						}
					}
					
					/* If we arrive here, we have visited all simplices in the current cluster! */
					this->unmarkAllVisited();
					for(COMPLEX::iterator it=c.begin();it!=c.end();it++)
					{
						/* Now, we discard the 'current' cluster */
						if((it->type()!=current.type()) || (it->id()!=current.id()))
						{
							/* Is its representative simplex a tetrahedron or a top simplex? */
							if(it->type()==3) { this->adjacencyCluster3D(v,SimplexPointer(*it),gprop,ll); }
							else { this->adjacencyTopTriangle(v,SimplexPointer(*it),gprop,ll); }
						}
					}
				}
	 		}
	 		
	 		/// This member function retrieves the contribution of a cluster represented by a top triangle to the link of a vertex.
	 		/**
	 		 * This member function retrieves the contribution of a cluster represented by a top triangle to the link of a vertex.
	 		 * \param v the identifier of the required vertex
	 		 * \param current a pointer to the top triangle
	 		 * \param gprop a property for marking simplices visited during this process
	 		 * \param ll a list which contains all simplices visited during this process
	 		 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointer, mangrove_tds::GhostPropertyHandle, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::COMPLEX 
	 		 */
			inline void linkVSTopTriangle(SIMPLEX_ID v,const SimplexPointer& current,GhostPropertyHandle<bool> *gprop,list<GhostSimplexPointer> *ll)
	 		{
	 			unsigned int pos,f0,f1;
	 			SimplexPointer aaa,e;
	 			queue<SimplexPointer> q;
	 			GhostSimplex s;
	 			bool *b;
	 			COMPLEX c;
	 			
	 			/* First, we check if this top triangle has been visited! */
	 			this->ghostSimplex(GhostSimplexPointer(current.type(),current.id()),&s);
	 			b=NULL;
	 			gprop->get(s,&b);
	 			if((*b)==false)
				{
					/* The 'current' top triangle has not been visited! */
					q.push(SimplexPointer(current));
					while(q.empty()==false)
					{
						aaa=q.front();
						q.pop();
						if(this->isVisited(SimplexPointer(aaa))==false)
						{
							/* Now, we are visiting an other simplex! */
							this->visit(SimplexPointer(aaa),true);
							this->simplex(SimplexPointer(aaa)).positionInBoundary(SimplexPointer(0,v),pos);
							this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id()),&s);
							b=NULL;
	 						gprop->get(s,&b);
	 						if((*b)==false) { (*b)=true; }
	 						f0=this->fposes[2].getHierarchy()[0].at(pos).getCCoboundary().at(0);
	 						if(this->simplex(SimplexPointer(aaa)).hasAdjacency(f0)==true)
							{
								e=SimplexPointer(this->simplex(SimplexPointer(aaa)).a(f0));
								if(e.type()!=1) { q.push(SimplexPointer(e.type(),e.id())); }
								else { this->retrieveClusters(SimplexPointer(aaa),f0,c); }
							}
							
							/* Now, we visit the second edge of 'aaa' incident at 'v' */
							f1=this->fposes[2].getHierarchy()[0].at(pos).getCCoboundary().at(1);
							if(this->simplex(SimplexPointer(aaa)).hasAdjacency(f1)==true)
							{
								e=SimplexPointer(this->simplex(SimplexPointer(aaa)).a(f1));
								if(e.type()!=1) { q.push(SimplexPointer(e.type(),e.id())); }
								else { this->retrieveClusters(SimplexPointer(aaa),f1,c); }
							}
							
							/* Now, we add the first vertex 'f0' */
							this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),0,f0),&s);
							b=NULL;
	 						gprop->get(s,&b);
	 						if((*b)==false)
	 						{
	 							(*b)=true;
	 							ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),0,f0));
	 						}
							
							/* Now, we add the second vertex 'f1' */
							this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),0,f1),&s);
							b=NULL;
	 						gprop->get(s,&b);
	 						if((*b)==false)
	 						{
	 							(*b)=true;
	 							ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),0,f1));
	 						}
	 					
	 						/* Now, we add the edge 'pos' */
	 						this->ghostSimplex(GhostSimplexPointer(aaa.type(),aaa.id(),1,pos),&s);
							b=NULL;
	 						gprop->get(s,&b);
	 						if((*b)==false)
	 						{
	 							(*b)=true;
	 							ll->push_back(GhostSimplexPointer(aaa.type(),aaa.id(),1,pos));
	 						}
						}
					}
					
					/* If we arrive here, we have visited all simplices in the current cluster! */
					this->unmarkAllVisited();
					for(COMPLEX::iterator it=c.begin();it!=c.end();it++)
					{
						/* Now, we discard the 'current' cluster */
						if((it->type()!=current.type()) || (it->id()!=current.id()))
						{
							/* Is its representative simplex a tetrahedron or a top simplex? */
							if(it->type()==3) { this->linkVSCluster3D(v,SimplexPointer(*it),gprop,ll); }
							else { this->linkVSTopTriangle(v,SimplexPointer(*it),gprop,ll); }
						}
					}
				}
	 		}
		};
	}
	
#endif

