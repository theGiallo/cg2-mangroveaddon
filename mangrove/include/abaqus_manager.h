/******************************************************************************************************
 *  This source code belongs to the Mangrove TDS Library
 *
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * November 2011 (Revised on May 2012)
 *
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * abaqus_manager.h - component for I/O on the Abaqus files
 *****************************************************************************************************/
 
/* Should we include this header file? */
 #ifndef ABAQUS_MANAGER_H

	#define ABAQUS_MANAGER_H

	#include "IO.h"
	#include "Miscellanea.h"
	#include "SimplicesMapper.h"
	#include "Simplex.h"
	#include "Point.h"
	#include <climits>
	#include <cfloat>
	#include <iomanip>
	using namespace mangrove_tds;
	using namespace std;
	
	/// Component for I/O between simplicial complexes and <i>Abaqus</i> files.
	/**
	 * The class defined in this file describes a component for I/O between simplicial complexes and files designed for the
	 * <A href=http://www.simulia.com/products/unified_fea.html><i>Abaqus Product Suite</i></A>, a commercial software package for finite element analysis marketed under the
	 * <A href=http://www.simulia.com><i>Simulia</i></A> brand.<p>This suite is quite complex, thus this component offers a limited support for such files: you should refer the official
	 * documentation at <A href=http://www.simulia.com><i>Simulia</i></A>.<p>In this class we extract the main information stored in the input file and then we use this component for creating a new
	 * simplicial complex. Moreover, we can write a representation of a simplicial complex formatted in accordance with the <i>Abaqus</i> format. In this component only the Euclidean coordinates are
	 * supported, while we do not store any field value for each vertex.
	 * \file abaqus_manager.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	
	/* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		/// A manager for the content of a file written in the <i>Abaqus</i> format.
		/**
		 * This class describes a component for I/O between simplicial 3-complexes embedded in the Euclidean space and files designed for the
		 * <A href=http://www.simulia.com/products/unified_fea.html><i>Abaqus Product Suite</i></A>, a commercial software package for finite element analysis marketed under the
		 * <A href=http://www.simulia.com><i>Simulia</i></A> brand.<p>This suite is quite complex, thus this component offers a limited support for such files: you should refer the official
		 * documentation at <A href=http://www.simulia.com><i>Simulia</i></A>.<p>In this class we extract the main
		 * information stored in the input file and then we use this component for creating a new simplicial complex. Moreover, we can write a representation of a simplicial
		 * 3-complex formatted in accordance with the <i>Abaqus</i> format. In this component only the Euclidean coordinates are supported, while we do not store any
		 * field value for each vertex.
		 * \see mangrove_tds::DataManager, mangrove_tds::IO, mangrove_tds::MeshComponent
		 */
		class AbaqusManager : public DataManager<AbaqusManager>
		{
			public:
			
			/// This member function creates an instance of this class.
			/**
			 * This member function creates a subclass of the mangrove_tds::DataManager class, optimized for managing simplicial 3-complexes embedded in the Euclidean space and files
			 * designed for the <A href=http://www.simulia.com/products/unified_fea.html><i>Abaqus Product Suite</i></A>, a commercial software package for finite element analysis
			 * marketed under the <A href=http://www.simulia.com><i>Simulia</i></A> brand.<p>This suite is quite complex, thus this component offers a limited support for such files: you
			 * should refer the official documentation at <A href=http://www.simulia.com><i>Simulia</i></A>.<p><b>IMPORTANT:</b> the new instance created by this member function is
			 * not initialized, consequently we must load new data through the mangrove_tds::AbaqusManager::loadData() member function.
			 * \see mangrove_tds::DataManager, mangrove_tds::IO, mangrove_tds::Point, mangrove_tds::AbaqusManager::loadData(), mangrove_tds::MeshComponent
			 */
			inline AbaqusManager() : DataManager<AbaqusManager>() 
			{
				/* We reset the coordinates! */
				this->xs.clear();
				this->ys.clear();
				this->zs.clear();
			}
			
			/// This member function destroys an instance of this class.
			/**
			 * This member function destroys an instance of this class.
			 * \see mangrove_tds::AbaqusManager::resetState()
			 */
			inline virtual ~AbaqusManager() { this->resetState(); }
			
			/// This member function allows to customize the property name containing the Euclidean coordinates in the simplicial complex to be built.
			/**
			 * This member function allows to customize the property name containing the Euclidean coordinates for all vertices in the simplicial complex to be built.<p>In this component,
			 * only the Euclidean coordinates are supported, while we do not store any field value for each vertex: the string to be used for the Euclidean coordinates is <i>coordinates</i>.
			 * \return the property name containing the Euclidean coordinates in the simplicial complex to be built
			 * \see mangrove_tds::PropertyBase, mangrove_tds::Point, mangrove_tds::AbaqusManager::supportsEuclideanCoordinates(), mangrove_tds::AbaqusManager::getCoordinates()
			 */
			inline string getPropertyName4Coordinates() { return string("coordinates"); }
			
			/// This member function checks if the loading of the Euclidean coordinates is supported and enabled in the current component.
			/**
			 * This member function checks if the loading of the Euclidean coordinates is supported by the current component. In this component only the Euclidean coordinates are supported,
			 * while we do not store any field value for each vertex.
			 * \return <ul><li><i>true</i>, if the current component supports the loading of the Euclidean coordinates for all the vertices.</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::Point, mangrove_tds::PropertyBase, mangrove_tds::AbaqusManager::getPropertyName4Coordinates(), mangrove_tds::AbaqusManager::getCoordinates()
			 */
			inline bool supportsEuclideanCoordinates() 
			{
				/* In this component we could load the Euclidean coordinates, now we must check if we have the same number of elements in xs, ys, zs! */
				if(this->simplices.size()==0) return false;
				if(this->xs.size()!=simplices[0].vectorSize()) return false;
				if(this->ys.size()!=simplices[0].vectorSize()) return false;
				if(this->zs.size()!=simplices[0].vectorSize()) return false;
				return true;
			}
			
			/// This member function returns the Euclidean coordinates associated to a vertex in the simplicial complex to be built.
			/**
			 * This member function returns the Euclidean coordinates associated to a vertex in the simplicial complex to be built.<p>If we cannot complete this operation for any reason, then
			 * this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param i the identifier of the required vertex
			 * \param p the Euclidean coordinates of the required vertex
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::Point, mangrove_tds::PropertyBase, mangrove_tds::Simplex, mangrove_tds::AbaqusManager::getPropertyName4Coordinates(),
			 * mangrove_tds::AbaqusManager::supportsEuclideanCoordinates()
			 */
			inline void getCoordinates(SIMPLEX_ID i,Point<double> & p)
			{
				/* First, we check if all is ok and then we extract the i-th point from 'coordinates' */
				assert(this->supportsEuclideanCoordinates());
				assert(i<this->xs.size());
				p.updateSize(3);
				p.x()=this->xs[i];
				p.y()=this->ys[i];
				p.z()=this->zs[i];
			}
			
			/// This member function allows to customize the property name containing the field values in the simplicial complex to be built.
			/**
			 * This member function allows to customize the property name containing the field values for all vertices in the simplicial complex to be built.<p>In this component, only the
			 * Euclidean coordinates are supported, while we do not store any field value for each vertex. Consequently, this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \return the property name containing the field values in the simplicial complex to be built
			 * \see mangrove_tds::PropertyBase, mangrove_tds::AbaqusManager::supportsFieldValues(), mangrove_tds::AbaqusManager::getFieldValue()
			 */
			inline string getPropertyName4FieldValue()
			{
				/* Field values are not supported! */
				assert(false);
				return string(); 
			}
			
			/// This member function checks if the loading of the field values is supported by the current component.
			/**
			 * This member function checks if the loading of the field values is supported by the current component: in this component only the Euclidean coordinates are supported, while we
			 * do not store any field value for each vertex.
			 * \return <ul><li><i>true</i>, if the current component supports the loading of the field values for all the vertices.</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::AbaqusManager::getPropertyName4FieldValue(), mangrove_tds::AbaqusManager::getFieldValue()
			 */
			inline bool supportsFieldValues() { return false; }
			
			/// This member function returns the field value associated to a vertex in the current simplicial complex.
			/**
			 * This member function returns the field value associated to a vertex in the simplicial complex to be built. In this component only the Euclidean coordinates are supported,
			 * while we do not store any field value for each vertex. Consequently, this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode.
			 * In this case, each forbidden operation will result in a failed assert.
			 * \param i the identifier of the required vertex
			 * \param f the field value of the required vertex
			 * \see mangrove_tds::AbaqusManager::getPropertyName4FieldValue(), mangrove_tds::AbaqusManager::supportsFieldValues(), mangrove_tds::SIMPLEX_ID, mangrove_tds::idle()
			 */
			inline void getFieldValue(SIMPLEX_ID i, double &f)
			{
				/* Dummy statetements for avoiding warnings! */
				idle(i);
				idle(f);
				assert(false);
			}
			
			/// This member function parses the content of a file written in <i>Abaqus</i> format.
			/**
			 * This member function parses the content of a file written in the <i>Abaqus</i> format: it is designed for the
			 * <A href=http://www.simulia.com/products/unified_fea.html><i>Abaqus Product Suite</i></A>, a commercial software package for finite element analysis marketed under the
			 * <A href=http://www.simulia.com><i>Simulia</i></A> brand.<p>This suite is quite complex, thus this component offers a limited support for such files: you should refer the
			 * official documentation at <A href=http://www.simulia.com><i>Simulia</i></A>.<p>If the input file does not exist or its content does not satisfy the <i>Abaqus</i> format
			 * guidelines, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in
			 * a failed assert.<p>In this component, only the Euclidean coordinates are supported, while we do not store any field value for each vertex. The string to be used for the
			 * Euclidean coordinates is <i>coordinates</i>, returned by the mangrove_tds::AbaqusManager::getPropertyName4Coordinates() member function: you can extract coordinates for a
			 * vertex through the mangrove_tds::AbaqusManager::getCoordinates() member function.
			 * \param fname the path of the input file
			 * \param reqParams this boolean flag identfies if the loading of the auxiliary parameters is required
		 	 * \param gd this boolean flag indicates if the effective generation of raw simplices must be performed
		 	 * \param checksOn this boolean flag indicates if the effective checks on the input data must be performed
			 * \see mangrove_tds::Simplex, mangrove_tds::SimplicesContainer, mangrove_tds::MeshComponent, mangrove_tds::SimplexPointer, mangrove_tds::AbaqusManager::resetState(),
			 * mangrove_tds::AbaqusManager::getCoordinates(), mangrove_tds::AbaqusManager::getPropertyName4Coordinates(), mangrove_tds::AbaqusManager::mergeComponents()
			 */
			inline void loadData(string fname,bool reqParams,bool gd,bool checksOn)
			{
				bool read_node,end_node;
				double x,y,z;
				string aux,line,kw,comma;
				vector<unsigned int> ids;
				unsigned int curr,ne,ind;
				
				/* First, we reset the current state and then we open the input file!. */
				this->resetState();
				this->in.open(fname.c_str());
				assert(this->in.is_open());
				this->simplices.push_back(SimplicesContainer(0));
				curr=0;
				ne=0;
				read_node=false;
				end_node=false;
				while(this->in.eof()==false)
				{
					/* First, we must understand how we can proceed our reading! */
					this->in>>aux;
					if(QString(aux.c_str()).startsWith("**")) { getline(this->in,line); }
					else if(QString(aux.c_str()).startsWith("*"))
					{
						/* Now, we extract the real keyword and then we understand if it is interesting for us! */
						kw=aux.substr(1,aux.size()-1);
						if(kw.compare(string("Part,"))==0)
						{ 
							/* We are introducing a new component of the current object */
							this->components.push_back(MeshComponent());
							getline(this->in,line);
						}
						else if(kw.compare(string("End"))==0)
						{
							/* We must read the next keyword in order to understand what we are closing! */
							this->in>>kw;
							if( kw.compare(string("Instance"))==0) { curr=curr+1; }
						}
						else if(kw.compare(string("Instance,"))==0) { getline(this->in,line); }
						else if(kw.compare(string("Node"))==0)
						{
							/* The next lines describe all the vertices! */
							read_node = true;
							end_node = false;
						}
						else if(kw.compare( string("Element,"))==0)
						{
							/* We have found the 'Element' keyword and now we read the elements type! */
							this->in>>aux;
							if(aux.compare("type=B31")==0) ne=2;
							else if(aux.compare("type=S3")==0) ne=3;
							else if(aux.compare("type=C3D4")==0) ne=4;
							else assert(false);
							assert(read_node);
							end_node = true;						
						}
						else { getline(this->in,line); }
					}
					else
					{
						/* Now, we must understand if we must read a node or an element (i.e. an edge, a triangle or a tetrahedron). */
						if( read_node && (!end_node))
						{
							/* Now, we must read a vertex description - code, x coordinate, y coordinate, z coordinate (we have already read the code) */
							this->in>>setprecision(9)>>x;
							this->in>>comma;
							this->in>>setprecision(9)>>y;
							this->in>>comma;
							this->in>>setprecision(9)>>z;
							this->components[curr].addPoint(x,y,z);
						}
						else
						{
							/* Now, we must read an element description! */
							ids.clear();
							ids.resize(ne);
							for(unsigned int k=0;k<ne;k++)
							{
								this->in>>ind;
								if(k!=ne-1) this->in>>comma;
								ids[k] = ind-1;
							}
							
							/* If we arrive here, we store the indices of the current element! */
							this->components[curr].addSimplex(ids);
						}
					}
				}

				/* If we arrive here, we have finished the effective reading from the input file! */
				this->in.close();
				if(checksOn) { this->mergeComponents(reqParams,gd); }
				else { this->copyComponents(reqParams,gd); }
			}

			/// This member function exports a simplicial complex in a file written in the <i>Abaqus</i> format.
			/**
			 * This member function exports a simplicial 3-complex, by writing its representation in accordance with the <i>Abaqus</i> format: it is designed for the
			 * <A href=http://www.simulia.com/products/unified_fea.html><i>Abaqus Product Suite</i></A>, a commercial software package for finite element analysis marketed under the
			 * <A href=http://www.simulia.com><i>Simulia</i></A> brand.<p>This suite is quite complex, thus this component offers a limited support for such files: you should refer the
			 * official documentation at <A href=http://www.simulia.com><i>Simulia</i></A>.<p>We can manage only a simplicial 3-complex composed by vertices, lines, triangles and
			 * tetrahedra. The output <i>Abaqus</i> file will be composed by an unique component with collections of elements with different dimensions. We write only the valid simplices,
			 * i.e. simplices with real locations and not marked as <i>deleted</i> by the garbage collector mechanism.<p>If we cannot complete this operation, then this member function will
			 * fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> we
			 * assume Euclidean coordinates in the input simplicial complex are stored in a local property named as the string returned by the
			 * mangrove_tds::AbaqusManager::getPropertyName4Coordinates() member function.
			 * \param ccw the input simplicial complex to be written
			 * \param fname the path of the output file
			 * \see mangrove_tds::SimplexPointerMap, mangrove_tds::BaseSimplicialComplex, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::LocalPropertyHandle,
			 * mangrove_tds::AbaqusManager::getPropertyName4Coordinates(), mangrove_tds::AbaqusManager::supportsEuclideanCoordinates(), mangrove_tds::AbaqusManager::writeGlobalComplex(),
			 * mangrove_tds::AbaqusManager::writeLocalComplex()
			 */
			template <class T> void writeData(BaseSimplicialComplex<T> *ccw,string fname)
			{
				/* First, we check the input parameters! */
				assert(ccw!=NULL);
				assert(ccw->type()<=3);
				if(ccw->encodeAllSimplices()==true) this->writeGlobalComplex(ccw,fname);
				else this->writeLocalComplex(ccw,fname);
			}
			
			/// This member function exports a subcomplex in a file written in the <i>Abaqus</i> format.
			/**
			 * This member function exports a subcomplex, by writing its representation in accordance with the <i>Abaqus</i> format: it is designed for the
			 * <A href=http://www.simulia.com/products/unified_fea.html><i>Abaqus Product Suite</i></A>, a commercial software package for finite element analysis marketed under the
			 * <A href=http://www.simulia.com><i>Simulia</i></A> brand.<p>This suite is quite complex, thus this component offers a limited support for such files: you should refer the
			 * official documentation at <A href=http://www.simulia.com><i>Simulia</i></A>.<p>We can manage only a simplicial 3-complex composed by vertices, lines, triangles and
			 * tetrahedra. The output <i>Abaqus</i> file will be composed by an unique component with collections of elements with different dimensions.<p>Here, we consider a subset of
			 * simplices directly encoded in the input data structure, that must be valid simplices, i.e. simplices with real locations and not marked as <i>deleted</i> by the garbage
			 * collector mechanism.<p>We assume Euclidean coordinates in the input simplicial complex are stored in a local property named as the string returned by the
			 * mangrove_tds::AbaqusManager::getPropertyName4Coordinates() member function.<p><b>IMPORTANT:</b> if we cannot complete this operation for any reason, then this member function
			 * will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param tops the simplices to be considered top in the new subcomplex to be written
			 * \param ccw the reference simplicial complex
			 * \param fname the path of the output file
			 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::SimplexPointer, mangrove_tds::LocalPropertyHandle, mangrove_tds::RawFace, mangrove_tds::Point,
			 * mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointerMap, mangrove_tds::AbaqusManager::getPropertyName4Coordinates(),
			 * mangrove_tds::AbaqusManager::supportsEuclideanCoordinates()
			 */
			template <class T> void writeSubcomplex( vector<SimplexPointer> &tops, BaseSimplicialComplex<T> *ccw, string fname)
			{
				string ec_name;
				vector< vector<RawFace> > simplexes;
				set<SIMPLEX_ID> vs;
				SIMPLEX_ID vind;
				Point<double> p;
				map<SIMPLEX_ID,SIMPLEX_ID> vett;
				list<SimplexPointer> bk;
				list<GhostSimplexPointer> bk_loc;
				GhostSimplex s;
				
				/* We must write the input simplicial complex as an unique Abaqus component. */ 
				assert(ccw!=NULL);
				assert(ccw->type()<=3);
				ec_name=string(this->getPropertyName4Coordinates());
				assert(ccw->hasProperty(string(ec_name)));
				assert(tops.size()!=0);
				if(this->out.is_open()) this->out.close();
				this->out.open(fname.c_str());
				assert(this->out.is_open());
				for(SIMPLEX_ID k=0;k<=ccw->type();k++) { simplexes.push_back(vector<RawFace>()); }
				this->out<<"*Heading"<<endl;
				this->out<<"** Job name: nmmodel-1 Model name: Model-1"<<endl;
				this->out<<"*Preprint, echo=NO, model=NO, history=NO, contact=NO"<<endl;
				this->out<<"**"<<endl;
				this->out<<"** PARTS"<<endl;
				this->out<<"**"<<endl;
				this->out<<"*Part, name=Part-1"<<endl;
				this->out<<"*End Part"<<endl;
				this->out<<"**"<<endl;
				this->out<<"**"<<endl;
				this->out<<"** ASSEMBLY"<<endl;
				this->out<<"**"<<endl;
				this->out<<"*Assembly, name=Assembly"<<endl;
				this->out<<"**"<<endl;
				this->out<<"*Instance, name=Part-1-1, part=Part-1"<<endl;
				this->out<<"*Node"<<endl;
				this->out.flush();
				for(vector<SimplexPointer>::iterator cp=tops.begin();cp!=tops.end();cp++)
				{
					/* Now, we consider if the current data structure is global or local! */
					if(ccw->encodeAllSimplices()==true)
					{
						/* Now, we extract the vertices of the current new top simplex (from a global data structure) */
						if(cp->type()!=0)
						{
							/* Now, we extract all the vertices of the current top simplex! */
							ccw->boundaryk( SimplexPointer(*cp),0,&bk);
							simplexes[cp->type()].push_back( RawFace());
							for(list<SimplexPointer>::iterator current=bk.begin();current!=bk.end();current++)
							{
								vs.insert(current->id());
								simplexes[cp->type()].back().getVertices().push_back(current->id());
							}
						}
					}
					else
					{
						/* Now, we extract the vertices of the current new top simplex (from a local data structure) */
						if(cp->type()!=0)
						{
							/* Now, we extract all the vertices of the current top simplex! */
							ccw->boundaryk(GhostSimplexPointer(cp->type(),cp->id()),0,&bk_loc);
							simplexes[cp->type()].push_back( RawFace());
							for(list<GhostSimplexPointer>::iterator current=bk_loc.begin();current!=bk_loc.end();current++)
							{
								ccw->ghostSimplex(GhostSimplexPointer(*current),&s);
								vs.insert(s.getCBoundary().at(0).id());
								simplexes[cp->type()].back().getVertices().push_back(s.getCBoundary().at(0).id());
							}
						} 
					}	
				}
				
				/* If we arrive here, we can write information about the vertices! */
				vind = 1;
				for(set<SIMPLEX_ID>::iterator it=vs.begin();it!=vs.end();it++)
				{
					/* Now, we write the Euclidean coordinates of the current vertex!  */
					this->out<<vind<<", ";
					this->out<<setprecision(9)<<ccw->getPropertyValue( string(ec_name),SimplexPointer(0,(*it)),p).x()<<", ";
					this->out<<setprecision(9)<<ccw->getPropertyValue( string(ec_name),SimplexPointer(0,(*it)),p).y()<<", ";
					this->out<<setprecision(9)<<ccw->getPropertyValue( string(ec_name),SimplexPointer(0,(*it)),p).z()<<endl;
					this->out.flush();
					vett[ (*it) ] = vind;
					vind = vind+1;
				}
				
				/* If we arrive here, we can write the simplices description! */
				vind=1;
				for(unsigned int k=0;k<simplexes.size();k++)
				{
					/* Now, we check if the current class of simplices is empty! */
					if(simplexes[k].size()!=0)
					{
						/* Now, we write information about the current class of simplices since it is not empty! */
						this->out<<"*Element, type=";
						if(k==1) this->out<<"B31"<<endl;
						if(k==2) this->out<<"S3"<<endl;
						if(k==3) this->out<<"C3D4"<<endl;
						for(unsigned int i=0;i<simplexes[k].size();i++)
						{
							/* We write the unique identifier and the vertices identifiers for the i-th simplex. */
							this->out<<vind;
							for(vector<SIMPLEX_ID>::iterator it=simplexes[k].at(i).getVertices().begin();it!=simplexes[k].at(i).getVertices().end();it++)
							{
								/* Now, we write the current simplex! */
								this->out<<", ";
								this->out<<vett[(*it)];
							}
							
							/* We finalize the writing for the i-th simplex! */
							this->out<<endl;
							this->out.flush();
							vind=vind+1;
						}
					}
				}
				
				/* If we arrive here, we have finished! */
				this->out<<"*End Instance"<<endl;
				this->out<<"**"<<endl;
				this->out<<"*End Assembly"<<endl;
				this->out.close();
				simplexes.clear();
				bk.clear();
				bk_loc.clear();
				vs.clear();
				vett.clear();
			}

			/// This member function exports a subcomplex in a file written in the <i>Abaqus</i> format.
			/**
			 * This member function exports a subcomplex, by writing its representation in accordance with the <i>Abaqus</i> format: it is designed for the
			 * <A href=http://www.simulia.com/products/unified_fea.html><i>Abaqus Product Suite</i></A>, a commercial software package for finite element analysis marketed under the
			 * <A href=http://www.simulia.com><i>Simulia</i></A> brand.<p>This suite is quite complex, thus this component offers a limited support for such files: you should refer the
			 * official documentation at <A href=http://www.simulia.com><i>Simulia</i></A>.<p>We can manage only a simplicial 3-complex composed by vertices, lines, triangles and
			 * tetrahedra. The output <i>Abaqus</i> file will be composed by an unique component with collections of elements with different dimensions.p>Here, we receive as input a list of
			 * ghost simplices, i.e. simplices not directly encoded in the input data structure, that must be a <i>local</i> mangrove, i.e. encodes only a subset of simplices.<p>We assume
			 * Euclidean coordinates in the input simplicial complex are stored in a local property named as the string returned by the
			 * mangrove_tds::AbaqusManager::getPropertyName4Coordinates() member function.<p><b>IMPORTANT:</b> if we cannot complete this operation for any reason, then this member function
			 * will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param tops the simplices to be considered top in the new subcomplex to be written
			 * \param ccw the reference simplicial complex
			 * \param fname the path of the output file
			 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::LocalPropertyHandle, mangrove_tds::RawFace, mangrove_tds::Point,
			 * mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointerMap, mangrove_tds::AbaqusManager::getPropertyName4Coordinates(),
			 * mangrove_tds::AbaqusManager::supportsEuclideanCoordinates()
			 */
			template <class T> void writeSubcomplex( vector<GhostSimplexPointer> &tops, BaseSimplicialComplex<T> *ccw, string fname)
			{
				string ec_name;
				vector< vector<RawFace> > simplexes;
				list<GhostSimplexPointer> bk_loc;
				GhostSimplex s;
				set<SIMPLEX_ID> vs;
				SIMPLEX_ID vind;
				Point<double> p;
				map<SIMPLEX_ID,SIMPLEX_ID> vett;
				
				/* We must write the input simplicial complex as an unique Abaqus component. */ 
				assert(ccw!=NULL);
				assert(ccw->type()<=3);
				assert(ccw->encodeAllSimplices()==false);
				ec_name=string(this->getPropertyName4Coordinates());
				assert(ccw->hasProperty(string(ec_name)));
				assert(tops.size()!=0);
				if(this->out.is_open()) this->out.close();
				this->out.open(fname.c_str());
				assert(this->out.is_open());
				for(SIMPLEX_ID k=0;k<=ccw->type();k++) { simplexes.push_back(vector<RawFace>()); }
				this->out<<"*Heading"<<endl;
				this->out<<"** Job name: nmmodel-1 Model name: Model-1"<<endl;
				this->out<<"*Preprint, echo=NO, model=NO, history=NO, contact=NO"<<endl;
				this->out<<"**"<<endl;
				this->out<<"** PARTS"<<endl;
				this->out<<"**"<<endl;
				this->out<<"*Part, name=Part-1"<<endl;
				this->out<<"*End Part"<<endl;
				this->out<<"**"<<endl;
				this->out<<"**"<<endl;
				this->out<<"** ASSEMBLY"<<endl;
				this->out<<"**"<<endl;
				this->out<<"*Assembly, name=Assembly"<<endl;
				this->out<<"**"<<endl;
				this->out<<"*Instance, name=Part-1-1, part=Part-1"<<endl;
				this->out<<"*Node"<<endl;
				this->out.flush();
				for(vector<GhostSimplexPointer>::iterator cp=tops.begin();cp!=tops.end();cp++)
				{
					/* Now, we extract the vertices of the current new ghost simplex (from a local data structure) */
					if(cp->type()!=0)
					{
						/* Now, we extract all the vertices of the current ghost simplex! */
						ccw->boundaryk(GhostSimplexPointer(*cp),0,&bk_loc);
						simplexes[cp->type()].push_back( RawFace());
						for(list<GhostSimplexPointer>::iterator current=bk_loc.begin();current!=bk_loc.end();current++)
						{
							ccw->ghostSimplex(GhostSimplexPointer(*current),&s);
							vs.insert(s.getCBoundary().at(0).id());
							simplexes[cp->type()].back().getVertices().push_back(s.getCBoundary().at(0).id());
						}
					}
				}
				
				/* If we arrive here, we can write information about the vertices! */
				vind = 1;
				for(set<SIMPLEX_ID>::iterator it=vs.begin();it!=vs.end();it++)
				{
					/* Now, we write the Euclidean coordinates of the current vertex!  */
					this->out<<vind<<", ";
					this->out<<setprecision(9)<<ccw->getPropertyValue( string(ec_name),SimplexPointer(0,(*it)),p).x()<<", ";
					this->out<<setprecision(9)<<ccw->getPropertyValue( string(ec_name),SimplexPointer(0,(*it)),p).y()<<", ";
					this->out<<setprecision(9)<<ccw->getPropertyValue( string(ec_name),SimplexPointer(0,(*it)),p).z()<<endl;
					this->out.flush();
					vett[ (*it) ] = vind;
					vind = vind+1;
				}
				
				/* If we arrive here, we can write the simplices description! */
				vind=1;
				for(unsigned int k=0;k<simplexes.size();k++)
				{
					/* Now, we check if the current class of simplices is empty! */
					if(simplexes[k].size()!=0)
					{
						/* Now, we write information about the current class of simplices since it is not empty! */
						this->out<<"*Element, type=";
						if(k==1) this->out<<"B31"<<endl;
						if(k==2) this->out<<"S3"<<endl;
						if(k==3) this->out<<"C3D4"<<endl;
						for(unsigned int i=0;i<simplexes[k].size();i++)
						{
							/* We write the unique identifier and the vertices identifiers for the i-th simplex. */
							this->out<<vind;
							for(vector<SIMPLEX_ID>::iterator it=simplexes[k].at(i).getVertices().begin();it!=simplexes[k].at(i).getVertices().end();it++)
							{
								/* Now, we write the current simplex! */
								this->out<<", ";
								this->out<<vett[(*it)];
							}
							
							/* We finalize the writing for the i-th simplex! */
							this->out<<endl;
							this->out.flush();
							vind=vind+1;
						}
					}
				}
				
				/* If we arrive here, we have finished! */
				this->out<<"*End Instance"<<endl;
				this->out<<"**"<<endl;
				this->out<<"*End Assembly"<<endl;
				this->out.close();
				simplexes.clear();
				bk_loc.clear();
				vs.clear();
				vett.clear();	
			}

			protected:
			
			/// The x-value coordinates for each vertex.
			vector<double> xs;
			
			/// The y-value coordinates for each vertex.
			vector<double> ys;
			
			/// The z-value coordinates for each vertex.
			vector<double> zs;
			
			/// This member function clears the internal state.
			/**
			 * This member function clears the internal state, by deallocating its member fields.
			 * \see mangrove_tds::RawFace, mangrove_tds::SimplicesContainer, mangrove_tds::MeshComponent
			 */
			inline void resetState()
			{
				/* We deallocate the internal state of the current instance */
				this->xs.clear();
				this->ys.clear();
				this->zs.clear();
				if(this->in.is_open()) this->in.close();
				if(this->out.is_open()) this->out.close();
				for(unsigned int k=0;k<this->raw_faces.size();k++) { this->raw_faces[k].clear(); }
				this->raw_faces.clear();
				this->components.clear();
				for(unsigned int k=0;k<this->simplices.size();k++) { this->simplices[k].clearCollection(); }
				this->simplices.clear();
			}
			
			/// This member function copies the content of components in the current simplicial complex.
			/**
			 * This member function copies the content of components in the current simplicial complex into a unique component: each component is described through an instance of the
			 * mangrove_tds::MeshComponent. In this way, we can generate an unique set of raw simplices (described by the mangrove_tds::RawFace class): this operation is mandatory for
			 * creating a new simplicial complex, described by a subclass of the mangrove_tds::BaseSimplicialComplex class.
			 * \param reqCoords this boolean flag identfies if the Euclidean coordinates (the unique auxiliary parameters in this component) loading is required
			 * \param gd this boolean flag indicates if the effective generation of raw simplices must be performed
			 * \see mangrove_tds::RawFace, mangrove_tds::BaseSimplicialComplex, mangrove_tds::MeshComponent, mangrove_tds::DataManager::generateRawSimplices(), 
			 * mangrove_tds::DataManager::loadData()
			 */
			inline void copyComponents(bool reqCoords,bool gd)
			{
				SimplexPointer cp;
				map< SimplexPointer,SIMPLEX_ID, CompareSimplex> mapper;
				vector<unsigned int> orig,aux;
				unsigned int ne,n;
				list<SIMPLEX_ID> dest;
				list<SIMPLEX_ID>::iterator it;
				SIMPLEX_ID idt;				

				/* First, we map each vertex in every component! */
				for(unsigned int k=0;k<this->components.size();k++) 
				{
					/* We add all the vertices in the k-th component in order to create a unique set of vertices! */
					for(unsigned int j=0;j<this->components[k].getPoints().size();j++)
					{
						this->simplices[0].addSimplex(cp);
						mapper[SimplexPointer(k,j)]=cp.id();
						if(reqCoords)
						{
							this->xs.push_back(this->components[k].getPoints().at(j).cx());
							this->ys.push_back(this->components[k].getPoints().at(j).cy());
							this->zs.push_back(this->components[k].getPoints().at(j).cz());
						}
					}
				}
				
				/* Now, we create a unique list of top simplices, starting from the list of simplices in each component! */
				for(unsigned int k=0;k<this->components.size();k++) 
				{
					/* We analyze the list of simplices  in the current component! */
					for(unsigned int j=0;j<this->components[k].getSimplices().size();j++)
					{
						/* Now, we extract the current simplex, and we must map it over the global list of vertices */
						orig=vector<unsigned int>( this->components[k].getSimplices().at(j));
						dest.clear();
						for(unsigned int i=0;i<orig.size();i++)
						{
							idt=mapper[SimplexPointer(k,orig[i])];
							dest.push_back(idt);
						}

						/* Now, we sort 'dest', and try to add this new face! */
						dest.sort();
						ne=dest.size();
						if(ne>=2)
						{
							/* In this case, we have at least two vertices and thus we can add a new top simplex! */
							if(ne-1>=this->simplices.size())
							{
								/* Now, we must create a new container! */
								n = this->simplices.size();
								this->simplices.resize(ne);
								for(unsigned int z=n;z<ne;z++) this->simplices[z].updateType(z);
							}
							
							/* Now, we can create the new simplex and then we add the boundary vertices! */
							this->simplices[ne-1].addSimplex(cp);
							it=dest.begin();
							for(unsigned int z=0;z<dest.size();z++,it++) this->simplices[ne-1].simplex(cp.id()).add2Boundary(SimplexPointer(0,*it),z);
							if( (ne>2) && (gd==true))
							{
								aux.clear();
								for(it=dest.begin();it!=dest.end();it++) aux.push_back(*it);
								this->generateRawSimplices(aux,this->simplices[ne-1].size()-1,true);
							}
						}
					}
				}
			}
			
			/// This member function merges components in the current simplicial complex.
			/**
			 * This member function merges components in the current simplicial complex into a unique component: each component is described through an instance of the
			 * mangrove_tds::MeshComponent. In this way, we can generate an unique set of raw simplices (described by the mangrove_tds::RawFace class): this operation is mandatory for
			 * creating a new simplicial complex, described by a subclass of the mangrove_tds::BaseSimplicialComplex class.
			 * \param reqCoords this boolean flag identfies if the Euclidean coordinates (the unique auxiliary parameters in this component) loading is required
			 * \param gd this boolean flag indicates if the effective generation of raw simplices must be performed
			 * \see mangrove_tds::RawFace, mangrove_tds::BaseSimplicialComplex, mangrove_tds::MeshComponent, mangrove_tds::DataManager::generateRawSimplices(), 
			 * mangrove_tds::DataManager::loadData()
			 */
			inline void mergeComponents(bool reqCoords,bool gd)
			{
				list<unsigned int>::iterator it;
				list<unsigned int> dest;
				VERTICES_SET vs;
				SimplexPointer cp;
				VERTICES_MAPPER mapper;
				unsigned int ne,n;
				vector<unsigned int> orig,aux;
				
				/* First, we must build a unique set of vertices starting from all the components! */
				for(unsigned int k=0;k<this->components.size();k++) { for(unsigned int j=0;j<this->components[k].getPoints().size();j++) vs.insert( this->components[k].getPoints().at(j)); }
				for(VERTICES_SET::iterator it=vs.begin();it!=vs.end();it++)
				{
					/* Now, we add all the vertices and we store their coordinates! */
					this->simplices[0].addSimplex(cp);
					mapper[ (*it) ] = cp.id();
					if(reqCoords)
					{
						this->xs.push_back(it->cx());
						this->ys.push_back(it->cy());
						this->zs.push_back(it->cz());
					}
				}
				
				/* Now, we must build a unique set of simplices: first, we map them into the global list of vertices! */
				for(unsigned int k=0;k<this->components.size();k++)
				{
					/* Now, we must map all the simplices in the k-th component over the global list of vertices */
					for(unsigned int j=0;j<this->components[k].getSimplices().size();j++)
					{
						/* Now, we extract the current simplex and we must map it over the global list of vertices */
						orig.clear();
						dest.clear();
						orig = vector<unsigned int>( this->components[k].getSimplices().at(j));
						for(unsigned int i=0;i<orig.size();i++) { dest.push_back(mapper[this->components[k].getPoints().at(orig[i])]); }
						dest.sort();
						dest.unique();
						ne = dest.size();
						if(ne>=2)
						{
							/* In this case, we have at least two vertices and thus we can add a new top simplex */
							if(ne-1>=this->simplices.size())
							{
								/* Now, we must create a new container! */
								n = this->simplices.size();
								this->simplices.resize(ne);
								for(unsigned int z=n;z<ne;z++) this->simplices[z].updateType(z);
							}
							
							/* Now, we can create the new simplex and then we add the boundary vertices! */
							this->simplices[ne-1].addSimplex(cp);
							it=dest.begin();
							for(unsigned int z=0;z<dest.size();z++,it++) this->simplices[ne-1].simplex(cp.id()).add2Boundary(SimplexPointer(0,*it),z);
							if( (ne>2) && (gd==true))
							{
								aux.clear();
								for(it=dest.begin();it!=dest.end();it++) aux.push_back(*it);
								this->generateRawSimplices(aux,this->simplices[ne-1].size()-1,true);
							}
						}
					}
				}
			}
			
			/// This member function exports a simplicial complex in a file written in the <i>Abaqus</i> format.
			/**
			 * This member function exports a simplicial 3-complex described by a data structure encoding all simplices (namely a <i>global</i> data structure). This member function writes a
			 * representation of the input simplicial 3-complex in accordance with the <i>Abaqus</i> format: it is designed for the
			 * <A href=http://www.simulia.com/products/unified_fea.html><i>Abaqus Product Suite</i></A>, a commercial software package for finite element analysis marketed under the
			 * <A href=http://www.simulia.com><i>Simulia</i></A> brand.
			 * \param ccw the input global simplicial 3-complex to be represented
			 * \param fname the path of the output file
			 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::SimplexPointerMap, mangrove_tds::Point, mangrove_tds::SimplexPointer, mangrove_tds::SimplexIterator
			 */
			template <class T> void writeGlobalComplex(BaseSimplicialComplex<T>*ccw,string fname)
			{
				SimplexPointerMap<T> map;
				unsigned int vind;
				Point<double> p;
				list<SimplexPointer> tops1,tops2,tops3,bk;
				SimplexIterator it;
				string ec_name;

				/* First, we check if the parameters are ok */
				ec_name=string(this->getPropertyName4Coordinates());
				assert(ccw->hasProperty(string(ec_name)));
				map.setComplex(ccw);
				map.forwardMapping();
				if(this->out.is_open()) this->out.close();
				this->out.open(fname.c_str());
				assert(this->out.is_open());
				this->out<<"*Heading"<<endl;
				this->out<<"** Job name: nmmodel-1 Model name: Model-1"<<endl;
				this->out<<"*Preprint, echo=NO, model=NO, history=NO, contact=NO"<<endl;
				this->out<<"**"<<endl;
				this->out<<"** PARTS"<<endl;
				this->out<<"**"<<endl;
				this->out<<"*Part, name=Part-1"<<endl;
				this->out<<"*End Part"<<endl;
				this->out<<"**"<<endl;
				this->out<<"**"<<endl;
				this->out<<"** ASSEMBLY"<<endl;
				this->out<<"**"<<endl;
				this->out<<"*Assembly, name=Assembly"<<endl;
				this->out<<"**"<<endl;
				this->out<<"*Instance, name=Part-1-1, part=Part-1"<<endl;
				this->out<<"*Node"<<endl;
				this->out.flush();
				vind=1;
				for(it=ccw->begin(0);it!=ccw->end(0);it++)
				{
					/* Now, we extract the current vertex and its coordinates! */
					this->out<<vind<<", ";
					this->out<<setprecision(9)<<ccw->getPropertyValue( string(ec_name),SimplexPointer(it.getPointer()),p).x()<<", ";
					this->out<<setprecision(9)<<ccw->getPropertyValue( string(ec_name),SimplexPointer(it.getPointer()),p).y()<<", ";
					this->out<<setprecision(9)<<ccw->getPropertyValue( string(ec_name),SimplexPointer(it.getPointer()),p).z()<<endl;
					this->out.flush();
					vind=vind+1;
				}
				
				/* Now, we extract all top simplices */
				tops1.clear();
				tops2.clear();
				tops3.clear();
				if(ccw->type()>=1) ccw->getTopSimplices(1,tops1); 
				if(ccw->type()>=2) ccw->getTopSimplices(2,tops2);
				if(ccw->type()==3) ccw->getTopSimplices(3,tops3);
				vind=1;
				if(tops1.size()!=0)
				{
					/* We have some wire-edges! */
					this->out<<"*Element, type=B31"<<endl;
					for(list<SimplexPointer>::iterator cp=tops1.begin();cp!=tops1.end();cp++)
					{
						/* Now, we extract its boundary! */
						this->out<<vind;
						ccw->boundaryk(SimplexPointer(*cp),0,&bk);
						for(list<SimplexPointer>::iterator current=bk.begin();current!=bk.end();current++) { this->out<<", "<<(map.getId(SimplexPointer(*current))+1); }
						this->out<<endl;
						this->out.flush();
						vind=vind+1;
					}
				}
				
				/* Now, we write the top 2-simplices, if they exist! */
				if(tops2.size()!=0)
				{
					/* We have some dangling faces! */				
					this->out<<"*Element, type=S3"<<endl;
					for(list<SimplexPointer>::iterator cp=tops2.begin();cp!=tops2.end();cp++)
					{
						/* Now, we extract its boundary! */
						this->out<<vind;
						ccw->boundaryk(SimplexPointer(*cp),0,&bk);
						for(list<SimplexPointer>::iterator current=bk.begin();current!=bk.end();current++) { this->out<<", "<<(map.getId(SimplexPointer(*current))+1); }
						this->out<<endl;
						this->out.flush();
						vind=vind+1;
					}
				}
				
				/* Now, we write the top 3-simplices, if they exist! */
				if(tops3.size()!=0)
				{
					/* We have tetrahedra! */
					this->out<<"*Element, type=C3D4"<<endl;
					this->out.flush();
					for(list<SimplexPointer>::iterator cp=tops3.begin();cp!=tops3.end();cp++)
					{
						/* Now, we extract its boundary! */
						this->out<<vind;
						ccw->boundaryk( SimplexPointer(*cp),0,&bk);
						for(list<SimplexPointer>::iterator current=bk.begin();current!=bk.end();current++) { this->out<<", "<<(map.getId(SimplexPointer(*current))+1); }
						this->out<<endl;
						this->out.flush();
						vind=vind+1;
					}
				}
				
				/* If we arrive here, we have finished! */
				this->out<<"*End Instance"<<endl;
				this->out<<"**"<<endl;
				this->out<<"*End Assembly"<<endl;
				this->out.close();
				tops1.clear();
				tops2.clear();
				tops3.clear();
				bk.clear();
			}
			
			/// This member function exports a simplicial complex in a file written in the <i>Abaqus</i> format.
			/**
			 * This member function exports a simplicial 3-complex described by a data structure encoding only a subset of all simplices (namely a <i>local</i> data structure). This member
			 * function writes a representation of the input simplicial 3-complex in accordance with the <i>Abaqus</i> format: it is designed for the
			 * <A href=http://www.simulia.com/products/unified_fea.html><i>Abaqus Product Suite</i></A>, a commercial software package for finite element analysis marketed under the
			 * <A href=http://www.simulia.com><i>Simulia</i></A> brand.
			 * \param ccw the input local simplicial 3-complex to be represented
			 * \param fname the path of the output file
			 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::SimplexPointerMap, mangrove_tds::Point, mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplexIterator
			 */
			template <class T> void writeLocalComplex(BaseSimplicialComplex<T> *ccw,string fname)
			{
				SimplexIterator it;
				GhostSimplex s;
				unsigned int k;
				string ec_name;
				unsigned int vind;
				Point<double> p;
				SimplexPointerMap<T> map;
				list<SimplexPointer> tops1,tops2,tops3;

				/* First, we check if the parameters are ok */
				ec_name=string(this->getPropertyName4Coordinates());
				assert(ccw->hasProperty(string(ec_name)));
				map.setComplex(ccw);
				map.forwardMapping();
				if(this->out.is_open()) this->out.close();
				this->out.open(fname.c_str());
				assert(this->out.is_open());
				this->out<<"*Heading"<<endl;
				this->out<<"** Job name: nmmodel-1 Model name: Model-1"<<endl;
				this->out<<"*Preprint, echo=NO, model=NO, history=NO, contact=NO"<<endl;
				this->out<<"**"<<endl;
				this->out<<"** PARTS"<<endl;
				this->out<<"**"<<endl;
				this->out<<"*Part, name=Part-1"<<endl;
				this->out<<"*End Part"<<endl;
				this->out<<"**"<<endl;
				this->out<<"**"<<endl;
				this->out<<"** ASSEMBLY"<<endl;
				this->out<<"**"<<endl;
				this->out<<"*Assembly, name=Assembly"<<endl;
				this->out<<"**"<<endl;
				this->out<<"*Instance, name=Part-1-1, part=Part-1"<<endl;
				this->out<<"*Node"<<endl;
				this->out.flush();
				vind=1;
				for(it=ccw->begin(0);it!=ccw->end(0);it++)
				{
					/* Now, we extract the current vertex and its coordinates! */
					this->out<<vind<<", ";
					this->out<<setprecision(9)<<ccw->getPropertyValue( string(ec_name),SimplexPointer(it.getPointer()),p).x()<<", ";
					this->out<<setprecision(9)<<ccw->getPropertyValue( string(ec_name),SimplexPointer(it.getPointer()),p).y()<<", ";
					this->out<<setprecision(9)<<ccw->getPropertyValue( string(ec_name),SimplexPointer(it.getPointer()),p).z()<<endl;
					this->out.flush();
					vind=vind+1;
				}
				
				/* Now, we extract all top simplices */
				tops1.clear();
				tops2.clear();
				tops3.clear();
				if(ccw->type()>=1) ccw->getTopSimplices(1,tops1); 
				if(ccw->type()>=2) ccw->getTopSimplices(2,tops2);
				if(ccw->type()==3) ccw->getTopSimplices(3,tops3);
				vind=1;
				if(tops1.size()!=0)
				{
					/* We have some wire-edges! */
					this->out<<"*Element, type=B31"<<endl;
					for(list<SimplexPointer>::iterator cp=tops1.begin();cp!=tops1.end();cp++)
					{
						/* Now, we extract its boundary! */
						this->out<<vind;
						ccw->ghostSimplex(GhostSimplexPointer(cp->type(),cp->id()),&s);
						for(k=0;k<=cp->type();k++) { this->out<<", "<<(map.getId(SimplexPointer(s.getBoundary()[k]))+1); }
						this->out<<endl;
						this->out.flush();
						vind=vind+1;
					}
				}
				
				/* Now, we write the top 2-simplices, if they exist! */
				if(tops2.size()!=0)
				{
					/* We have some dangling faces! */				
					this->out<<"*Element, type=S3"<<endl;
					for(list<SimplexPointer>::iterator cp=tops2.begin();cp!=tops2.end();cp++)
					{
						/* Now, we extract its boundary! */
						this->out<<vind;
						ccw->ghostSimplex(GhostSimplexPointer(cp->type(),cp->id()),&s);
						for(k=0;k<=cp->type();k++) { this->out<<", "<<(map.getId(SimplexPointer(s.getBoundary()[k]))+1); }
						this->out<<endl;
						this->out.flush();
						vind=vind+1;
					}
				}
				
				/* Now, we write the top 3-simplices, if they exist! */
				if(tops3.size()!=0)
				{
					/* We have tetrahedra! */
					this->out<<"*Element, type=C3D4"<<endl;
					for(list<SimplexPointer>::iterator cp=tops3.begin();cp!=tops3.end();cp++)
					{
						/* Now, we extract its boundary! */
						this->out<<vind;
						ccw->ghostSimplex(GhostSimplexPointer(cp->type(),cp->id()),&s);
						for(k=0;k<=cp->type();k++) { this->out<<", "<<(map.getId(SimplexPointer(s.getBoundary()[k]))+1); }
						this->out<<endl;
						this->out.flush();
						vind=vind+1;
					}
				}
				
				/* If we arrive here, we have finished! */
				this->out<<"*End Instance"<<endl;
				this->out<<"**"<<endl;
				this->out<<"*End Assembly"<<endl;
				this->out.close();
				tops1.clear();
				tops2.clear();
				tops3.clear();
			}
		};
	}

#endif

