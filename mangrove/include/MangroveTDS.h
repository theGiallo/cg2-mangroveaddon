/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library                                   
 *                                                                           				    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * July 2011 (Revised on May 2012)
 *                                                                    
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * MangroveTDS.h - cumulative header for all data structures (mangroves) in this library
 ***********************************************************************************************/
 
/* Should we include this header file? */
#ifndef MANGROVE_TDS_H

	#define MANGROVE_TDS_H
	
	#include "BaseSimplicialComplex.h"
	#include "IGManager.h"
	#include "ISManager.h"
	#include "SIGManager.h"
	#include "GIAManager.h"
	#include "NMIAManager.h"
	#include "TriangleSegmentManager.h"
	using namespace mangrove_tds;
	
	/// All data structures encoded in this library
	/**
	 * This is a cumulative header for including all data structures encoded in this library, described through templated instances of the mangrove_tds::BaseSimplicialComplex class. We can classify
	 * data structures as <i>global</i> and <i>local</i> data structures. Namely, a <i>global</i> data structure encodes all simplices, while a <i>local</i> data structure encodes only a subset of 
	 * simplices (usually only top simplices). At the moment, we are able to use the following global data structures:<ul><li>the <i>Incidence Graph</i>, see mangrove_tds::IG class;</li><li>the
	 * <i>Incidence Simplicial</i> data structure, see mangrove_tds::IS class;</li><li>the <i>Simplified Incidence Graph</i>, see mangrove_tds::SIG class.</li></ul>Again, we are able to use the
	 * following local data structures:<ul><li>the <i>IA* data structure</i>, see mangrove_tds::GIA class;</li><li>the <i>Triangle-Segment</i> data structure (only for 2-dimensional shapes), see
	 * mangrove_tds::TriangleSegment class</li><li>the <i>Non-Manifold IA</i> data structure (only for
	 * 3-dimensional shapes), see mangrove_tds::NMIA class.</li></ul>
	 * \file MangroveTDS.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	
#endif

