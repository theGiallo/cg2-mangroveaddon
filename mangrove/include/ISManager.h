/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library                            
 *                                                                           				    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * October 2011 (Revised on May 2012)
 *                                                                
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * ISManager.h - the specialized component for executing I/O operations on the Incidence Simplicial.
 ***********************************************************************************************/

/* Should we include this header file? */
#ifndef IS_MANAGER_H

	#define IS_MANAGER_H

	#include "IS.h"
	#include "IO.h"
	#include "IGManager.h"
	#include "Miscellanea.h"
	#include <list>
	#include <map>
	#include <queue>
	#include <algorithm>
	#include <cstdlib>
	using namespace std;
	using namespace mangrove_tds;

	/// The specialized component for executing I/O operations on the <i>Incidence Simplicial</i> data structure.
	/**
	 * The class defined in this file allows to execute I/O operations on the <i>Incidence Simplicial</i> data structure.<p>The <i>Incidence Simplicial</i> data structure, described by the
	 * mangrove_tds::IS class, is a dimension-independent and incidence-based data structure for representing simplicial complexes with an arbitrary domain and it encodes all the simplices in a
	 * simplicial complex, thus it is a <i>global</i> data structure. For each k-simplex it encodes:<ul><li>all the k+1 simplices of dimension k-1 belonging to its boundary;</li><li>one simplex of
	 * dimension k+1 for each connected component in its link.</li></ul>The effective I/O operations on the input <i>Incidence Simplicial</i> data structure are delegated to a particular instance of
	 * the mangrove_tds::DataManager class in order to support a wide range of I/O formats.
	 * \file ISManager.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	 
	/* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		/// The specialized component for executing I/O operations on the <i>Incidence Simplicial</i> data structure.
		/**
		 * This class allows to execute I/O operations on the <i>Incidence Simplicial</i> data structure.<p>The <i>Incidence Simplicial</i> data structure, described by the mangrove_tds::IS
		 * class, is a dimension-independent and incidence-based data structure for representing simplicial complexes with an arbitrary domain and it encodes all the simplices in a simplicial
		 * complex, thus it is a <i>global</i> data structure. For each k-simplex it encodes:<ul><li>all the k+1 simplices of dimension k-1 belonging to its boundary;</li><li>one simplex of
		 * dimension k+1 for each connected component in its link.</li></ul>The effective I/O operations on the <i>Incidence Simplicial</i> data structure are delegated to a particular instance
		 * of the mangrove_tds::DataManager class in order to support a wide range of I/O formats.<p><b>IMPORTANT:</b> in this class we build an <i>Incidence Simplicial</i> data structure, by
		 * starting from the corresponding instance of the <i>Incidence Graph</i> (described in the mangrove_tds::IG class) and then we update it in order to obtain the required
		 * <i>Incidence Simplicial</i> data structure.
		 * \see mangrove_tds::DataManager, mangrove_tds::IO, mangrove_tds::IS, mangrove_tds::IG, mangrove_tds::IGManager, mangrove_tds::BaseSimplicialComplex
		 */
		class ISManager : public IO<ISManager>
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a not initialized instance of this class: you should apply the mangrove_tds::ISManager::createComplex() or mangrove_tds::ISManager::createFromIG()
			 * member functions in order to initialize it.
			 * \see mangrove_tds::ISManager::createComplex(), mangrove_tds::ISManager::createFromIG(), mangrove_tds::IS, mangrove_tds::BaseSimplicialComplex
			 */
			inline ISManager() : IO<ISManager>() { this->ccw = NULL; }
			
			/// This member function destroys an instance of this class.
			/**
			 * This member function destroys an instance of this class. The specialized data structure for the simplicial complex is created in the mangrove_tds::ISManager::createComplex() or
			 * mangrove_tds::ISManager::createFromIG() member functions, thus you must not manually destroy it: you should apply this destructor or the mangrove_tds::ISManager::close()
			 * member function in order to destroy all the internal state.
			 * \see mangrove_tds::ISManager::createComplex(), mangrove_tds::ISManager::createFromIG(), mangrove_tds::IS, mangrove_tds::BaseSimplicialComplex,
			 * mangrove_tds::ISManager::close()
			 */
			inline virtual ~ISManager() { this->close(); }
			
			/// This member function destroys all the internal state in this component.
			/**
			 * This member function destroys all the internal state in this component. The specialized data structure for the simplicial complex is created in the
			 * mangrove_tds::ISManager::createComplex() or mangrove_tds::ISManager::createFromIG() member functions, thus you must not manually destroy it: you should apply the
			 * destructor or this member function in order to destroy all the internal state.
			 * \see mangrove_tds::ISManager::createComplex(), mangrove_tds::ISManager::createFromIG(), mangrove_tds::IS, mangrove_tds::BaseSimplicialComplex
			 */
			inline void close()
			{
				/* Now, we reset the current state! */
				if(this->ccw!=NULL) delete this->ccw;
				this->ccw=NULL;
			}
			
			/// This member function returns the simplicial complex created by the current component.
			/**
			 * This member function returns the simplicial complex created in the mangrove_tds::ISManager::createComplex() member function. The current instance of the
			 * <i>Incidence Simplicial</i> data structure has been created in the mangrove_tds::ISManager::createComplex() or mangrove_tds::ISManager::createFromIG() member functions,
			 * thus you must not manually destroy it. You should apply the destructor or the mangrove_tds::ISManager::close() member function in order to destroy all the internal state. Thus,
			 * the input pointer must be a C++ pointer to an instance of the mangrove_tds::IS class.<p>If such conditions are not satisfied, then this member function will fail if and only if
			 * the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param cs the simplicial complex created by this component
			 * \see mangrove_tds::ISManager::createComplex(), mangrove_tds::ISManager::createFromIG(), mangrove_tds::IS, mangrove_tds::BaseSimplicialComplex,
			 * mangrove_tds::ISManager::close()
			 */
			template <class D> void getComplex(D** cs)
			{
				D *aux;
				
				/* First, we check if all is ok! */
				assert(this->ccw!=NULL);
				aux = dynamic_cast<D*>(this->ccw);
				assert(aux!=NULL);
				(*cs) = this->ccw;
			}
			
			/// This member function returns a string that describes the data structure created by the current I/O component.
 			/**
 			 * This member function returns a string that describes the data structure created by the current I/O component, and used for encoding the current simplicial complex.
			 * \return a string that describes the data structure created by the current I/O component
 			 * \see mangrove_tds::BaseSimplicialComplex::getDataStructureName()
 			 */
 			inline string getDataStructureName() { return string("Incidence Simplicial (IS)"); }
 			
 			/// This member function creates a simplicial complex by reading it from a file.
			/**
			 * This member function generates an <i>Incidence Simplicial</i> data structure from a file that contains a possible representation: the most common exchange format for a
			 * simplicial complex consists of a collection of <i>top</i> simplices described by their vertices, but it is not the unique possibility.<p>The <i>Incidence Simplicial</i> data
			 * structure, described by the mangrove_tds::IS class, is a dimension-independent data structure for representing simplicial complexes with an arbitrary domain and it encodes all
			 * the simplices in a simplicial complex. Thus, it is a <i>global</i> data structure. For each k-simplex it encodes:<ul><li>all the k+1 simplices of dimension k-1 belonging to its
			 * boundary;</li><li>one simplex of dimension k+1 for each connected component in its link.</li></ul>The effective I/O operations on the <i>Incidence Simplicial</i> data structure
			 * are delegated to a particular instance of the mangrove_tds::DataManager class in order to support a wide range of I/O formats.<p>Consequently, if the input contains a soup of
			 * top simplices, then we need to generate all the simplices and then to establish all the topological relations among them.<p>In this member function we can customize the
			 * required simplicial complex: for instance, we can attach the Euclidean coordinates and the field value for each vertex in the simplicial complex to be built (if supported in
			 * the I/O component). Such information is stored as local properties (i.e. as subclasses of the mangrove_tds::LocalPropertyHandle class) associated to vertices. You can retrieve
			 * them through their names, respectively provided by the mangrove_tds::DataManager::getPropertyName4Coordinates() and mangrove_tds::DataManager::getPropertyName4FieldValue()
			 * member function. Moreover, you can check what properties are supported through the mangrove_tds::DataManager::supportsEuclideanCoordinates() and
			 * mangrove_tds::DataManager::supportsFieldValues() member functions.<p>If we cannot complete these operations for any reason, then this member function will fail if and only if
			 * the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> the specialized data
			 * structure for the simplicial complex is created in this member function or in the mangrove_tds::ISManager::createFromIG() member function, thus you must not manually destroy it.
			 * You should apply the destructor or mangrove_tds::ISManager::close() member function in order to destroy all the internal state.
			 * \param loader a component for the effective data reading
	 	 	 * \param fname the path of the input file
			 * \param reqParams this boolean flag identfies if the loading of the auxiliary parameters encoded in the input file is enabled
			 * \param ld this boolean flag indicates if the effective reading of the input data must be performed
			 * \param checksOn this boolean flag indicates if the effective checks on the input data must be performed (valid only if the effective reading must be performed)
			 * \see mangrove_tds::DataManager, mangrove_tds::ISManager::close(), mangrove_tds::ISManager::createFromIG(), mangrove_tds::BaseSimplicialComplex, mangrove_tds::IG,
			 * mangrove_tds::IS, mangrove_tds::ISManager::createIncidence(), mangrove_tds::RawIncidence, mangrove_tds::LocalPropertyHandle, mangrove_tds::RawFace,
			 * mangrove_tds::ISManager::retrieveCoboundary(), mangrove_tds::ISManager::retrieveComponents(), mangrove_tds::ISManager::addEdge(), mangrove_tds::ISManager::getUnique()
			 */
			template <class T> void createComplex(DataManager<T> *loader, string fname,bool reqParams,bool ld,bool checksOn)
			{
				bool b;
				LocalPropertyHandle<double> *fvals_prop;
				LocalPropertyHandle< Point<double> > *pnts_prop;
				double aaa;
				unsigned int k,uik,rik,ik;
				SimplexIterator it;
				Point<double> p;
				SimplexPointer cp;
				vector< vector<RawIncidence> > inc;
				
				/* First, we check if all is ok and then we run the effective construction! */
				assert(loader!=NULL);
				this->close();
				if(ld) loader->loadData(fname,reqParams,true,checksOn);
				b=(loader->supportsEuclideanCoordinates()==true) || (loader->supportsFieldValues()==true);
				if(reqParams) assert(b==true);
				try { this->ccw = new IS(loader->getDimension()); }
				catch(bad_alloc ba) { this->ccw = NULL; }
				assert(this->ccw!=NULL);
				aaa=0.0;
				pnts_prop=NULL;
				fvals_prop=NULL;
				if(reqParams)
				{
					/* What properties must be added? First, we check the Euclidean coordinates */
					if(loader->supportsEuclideanCoordinates())
					{
						pnts_prop=this->ccw->addLocalProperty(0,string(loader->getPropertyName4Coordinates()), Point<double>());
						this->ccw->getPropertyName4Coordinates()=string(loader->getPropertyName4Coordinates());
					}
					
					/* Now, we check the field values! */
					if(loader->supportsFieldValues())
					{
						fvals_prop=this->ccw->addLocalProperty(0,string(loader->getPropertyName4FieldValue()),aaa); 
						this->ccw->getPropertyName4FieldValue()=string(loader->getPropertyName4FieldValue());
					}
				}
				
				/* Now, we can add all the vertices and their properties! */
				it = loader->getCollection(0).begin();
				k=0;
				while(it!=loader->getCollection(0).end())
				{
					/* Now, we insert the k-th vertex in 'ccw' */
					this->ccw->addSimplex(cp);
					if(reqParams)
					{
						/* Now, we retrieve the Euclidean coordinates, if supported! */						
						if(loader->supportsEuclideanCoordinates())
						{
							loader->getCoordinates(k,p);
							pnts_prop->set(SimplexPointer(cp),Point<double>(p));
						}
						
						/* Now, we retrieve the field values, if supported! */
						if(loader->supportsFieldValues())
						{
							loader->getFieldValue(k,aaa);
							fvals_prop->set(SimplexPointer(cp),aaa);
						}
					}
					
					/* Now, we move on the next vertex! */
					k=k+1;
					it++;
				}
				
				/* Now, we must eliminate and create the subsimplices of the top simplices. Then, we must understand how connecting the top simplices! */
				if(loader->getDimension()==0) return;
				for(SIMPLEX_TYPE d=1;d<loader->getRawFaces().size();d++) { this->getUnique(d,loader); }
				for(it=loader->getCollection(1).begin(); it!=loader->getCollection(1).end();it++) { this->addEdge((*it).bc(0).id(),(*it).bc(1).id()); }
				for(SIMPLEX_TYPE d=loader->getDimension();d>=2;d--)
				{
					/* First, we check if there are any top simplices of dimension d */
					if( loader->getCollection(d).size()!=0)
					{
						/* If we arrive here, there are certainly simplices of dimension d-1 that are on these simplices boundary! */
						for(k=0;k<loader->getRawFaces()[d-1].size();k++)
						{
							/* Now, we check if the k-th simplex is a child of a top simplex! */
							if(loader->getRawFaces()[d-1].at(k).isTopSimplexChild())
							{
								uik = loader->getRawFaces()[d-1].at(k).getUniqueIndex();
								ik = loader->getRawFaces()[d-1].at(k).getIndex();
								rik = loader->getRawFaces()[d-1].at(k).getReverseIndex();
								loader->getCollection(d).simplex(ik).b(rik).setId(uik);
								loader->getCollection(d).simplex(ik).b(rik).setType(d-1);								
							}
						}
						
						/* Now, we add all the top simplices of dimension d to ccw */
						for(it=loader->getCollection(d).begin();it!=loader->getCollection(d).end();it++)
						{
							/* Now, we create a new simplex with its boundary! */
							this->ccw->addSimplex(d,cp);
							for(unsigned int l=0;l<=d;l++) 
							{ 
								this->ccw->simplex(cp).b(l).setId((*it).b(l).id());
								this->ccw->simplex(cp).b(l).setType((*it).b(l).type());
							}
						}
					}
				}
				
				/* Now, we must update the co-boundary relation! */
				this->createIncidence(inc);
				this->retrieveCoboundary(&inc);
			}
 			
 			/// This member function exports the current simplicial complex in accordance with a certain format.
			/**
			 * This member function exports the current simplicial complex, by writing its representation in accordance with a specific file format.<p>The effective writing is delegated to a
			 * subclass of the mangrove_tds::DataManager class and some further conditions could be checked and verified: for example, the current simplicial complex must support the
			 * extraction of particular auxiliary information (like the Euclidean coordinates and/or scalar field values, described by subclasses of the mangrove_tds::PropertyBase class). If
			 * such properties are not satisfied, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.
			 * \param writer a component for the effective data writing (we write the data in this member function)
			 * \param fname the path of the output file
			 * \see mangrove_tds::ISManager::createComplex(), mangrove_tds::ISManager::createFromIG(), mangrove_tds::IS, mangrove_tds::BaseSimplicialComplex, mangrove_tds::ISManager::close()
			 */
			template <class T> void writeComplex(DataManager<T> *writer,string fname)
			{
				/* First, we check if all is ok! */
				assert(writer!=NULL);
				writer->writeData(this->ccw,fname); 
			}
			
			/// This member function creates a simplicial complex by starting from an <i>Incidence Graph</i>.
			/**
			 * This member function allows to generate an <i>Incidence Simplicial</i> data structure for encoding a simplicial complex by starting from an instance of the
			 * <i>Incidence Graph</i> (described in the mangrove_tds::IG class). First, we copy all the boundaries of the simplices (see the mangrove_tds::ISManager::copySimplices() member
			 * function) and then we modify the complete coboundary relation in order to obtain the partial coboundary relation for each simplex (see the
			 * mangrove_tds::ISManager::computeCoboundary() member function).<p>In this member function we can customize the required simplicial complex: for instance, we can attach the
			 * Euclidean coordinates and the field value for each vertex in the simplicial complex to be built (if supported). Such information is stored as instance of local properties (i.e.
			 * as subclasses of the mangrove_tds::LocalPropertyHandle class) associated to vertices. You can retrieve them through their names, respectively provided by the
			 * mangrove_tds::IG::getPropertyName4Coordinates() and mangrove_tds::IG::getPropertyName4FieldValue() member functions. Moreover, you can check what properties are supported
			 * through the mangrove_tds::IG::supportsEuclideanCoordinates() and mangrove_tds::IG::supportsFieldValues() member functions.<p><b>IMPORTANT:</b> the specialized data structure
			 * for the simplicial complex is created in this member function, thus you must not manually destroy it. You should apply the destructor or mangrove_tds::IO::close() member
			 * function in order to destroy all the internal state.<p>Moreover, in this member function, we use two global properties with boolean values, called <i>visited</i> and
			 * <i>inStar</i>, for identifying connected components in the link of simplices. We remove such properties, if they already belong to the output <i>Incidence Graph</i>. In any
			 * case, we remove them at the end of computation.<p>If we cannot complete this operation for any reason, then this member function will fail if and only if the
			 *<i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param ig the input <i>Incidence Graph</i> to be analyzed
			 * \param reqParams this boolean flag identfies if the loading of the auxiliary parameters encoded in the input <i>Incidence Graph</i> is enabled
			 * \see mangrove_tds::ISManager::close(), mangrove_tds::ISManager::createComplex(), mangrove_tds::ISManager::copySimplices(), mangrove_tds::ISManager::computeCoboundary(),
			 * mangrove_tds::IS, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex, mangrove_tds::GlobalPropertyHandle, mangrove_tds::IG, mangrove_tds::LocalPropertyHandle,
			 * mangrove_tds::IG::getPropertyName4Coordinates(), mangrove_tds::IG::getPropertyName4FieldValue() mangrove_tds::IG::supportsEuclideanCoordinates(),
			 * mangrove_tds::IG::supportsFieldValues()
			 */
			inline void createFromIG(IG *ig,bool reqParams)
			{
				bool b;

				/* First, we check if all is ok! */
				assert(ig!=NULL);
				this->close();
				b=(ig->supportsEuclideanCoordinates()==true) || (ig->supportsFieldValues()==true);
				if(reqParams) assert(b==true);
				try { this->ccw = new IS(ig->type()); }
				catch(bad_alloc ba) { this->ccw = NULL; }
				assert(this->ccw!=NULL);
				this->copySimplices(ig,reqParams);
				this->computeCoboundary(ig);
			}
			
			protected:
			
			/// The simplicial complex created by this component.
			/**
			 * \see mangrove_tds::IS
			 */
			IS* ccw;
			
			/// This member functions add a new edge to the current simplicial complex.
			/**
			 * This member functions add a new edge to the current data structure: an edge is completely defined by its two vertices, that we assume already stored in the current data
			 * structure.
			 * \param v0 the identifier of the first vertex for the new edge to be added to the current simplicial complex
			 * \param v1 the identifier of the second vertex for the new edge to be added to the current simplicial complex
			 * \return the identifier of the new edge added to the current simplicial complex
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::ISManager::getUnique(), mangrove_tds::SimplexPointer, mangrove_tds::IS, mangrove_tds::BaseSimplicialComplex
			 */
			inline SIMPLEX_ID addEdge(SIMPLEX_ID v0,SIMPLEX_ID v1)
			{
				SimplexPointer cp;
			
				/* First, we add a new edge and then return its new identifier! */
				this->ccw->addSimplex(SimplexPointer(0,v0),SimplexPointer(0,v1),cp);
				return cp.id();
			}
			
			/// This member function retrieves the complete incidence relation for the input simplicial complex.
			/**
			 * This member function retrieves the complete incidence relation for the input simplicial complex: it is useful when we try to construct a new instance of the
			 * <i>Incidence Simplicial</i> data structure from scratch. Basically this member function is similar to the mangrove_tds::IG::buildCoboundaryFromBoundary() member function. In
			 * other words, we rebuilds the coboundary relations for all simplices directly stored in the current data structure, starting from the boundary relations (already encoded) in the
			 * current data structure. Retrieved coboundary is encoded in an instance of the mangrove_tds::RawIncidence class.<p><b>IMPORTANT:</b> we assume to use this member function only
			 * for building a new instance of the <i>Incidence Simplicial</i> data structure from scratch.
			 * \param inc the auxiliary data structure for storing coboundary relation
			 * \see mangrove_tds::RawIncidence, mangrove_tds::IG::buildCoboundaryFromBoundary(), mangrove_tds::SimplexPointer
			 */
			inline void createIncidence(vector< vector<RawIncidence> > &inc)
			{
				SimplexIterator it;
				unsigned int lg,j;

				/* First, we create locations in 'inc' */
				inc.push_back(vector<RawIncidence>(this->ccw->size(0),RawIncidence()));
				for(SIMPLEX_TYPE d=1;d<=this->ccw->type();d++)
				{
					/* Now, we visit all the simplices in the d-th collection of simplices, i.e. the d-simplices! */
					inc.push_back(vector<RawIncidence>(this->ccw->size(d),RawIncidence()));
					for(it=this->ccw->begin(d);it!=this->ccw->end(d);it++)
					{
						/* Now, we retrieve the coboundary */
						lg = it->b().size();
						for(j=0;j<lg;j++) { inc[it->b(j).type()][it->b(j).id()].addSimplex(SimplexPointer(it.getPointer())); }
					}
				}
			}
			
			/// This member function copies all the simplices from an <i>Incidence Graph</i> to the current <i>Incidence Simplicial</i> data structure.
			/**
			 * This member function copies all the simplices from an <i>Incidence Graph</i> (described through an instance of the mangrove_tds::IG class) to the current
			 * <i>Incidence Simplicial</i> data structure (described through an instance of the mangrove_tds::IS class). It is mandatory to copy only the simplices and their boundaries, while
			 * the partial coboundary relation is computed by the mangrove_tds::ISManager::computeCoboundary() starting from the input <i>Incidence Graph</i>.<p>If it is required, we can also
			 * copy auxiliary information (Euclidean coordinates and field values) associated to the input <i>Incidence Graph</i>: such properties are reachable through their names, i.e. the
			 * same names in the input <i>Incidence Graph</i>. Their names are respectively provided by the mangrove_tds::BaseSimplicialComplex::getPropertyName4Coordinates() and
			 * mangrove_tds::BaseSimplicialComplex::getPropertyName4FieldValue() member functions.
			 * \param ig the input <i>Incidence Graph</i> to be copied
 			 * \param reqParams this boolean flag identfies if the loading of the auxiliary parameters encoded in the input Incidence Graph is enabled
 			 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::ISManager::computeCoboundary(), mangrove_tds::IG, mangrove_tds::IS,
 			 * mangrove_tds::BaseSimplicialComplex::getPropertyName4Coordinates(), mangrove_tds::BaseSimplicialComplex::getPropertyName4FieldValue()
			 */
			inline void copySimplices(IG *ig,bool reqParams)
			{
				SimplexIterator it;
				SimplexPointer cp;
				double aaa,f;
				Point<double> p,a;
				LocalPropertyHandle<double> *fv_prop;
				LocalPropertyHandle< Point<double> > *pnt_prop;

				/* First, we add all the local properties, if required, for storing Euclidean coordinates and field values! */
				aaa=0.0;
				fv_prop=NULL;
				pnt_prop=NULL;
				if(reqParams)
				{
					/* Now, we must check if the Euclidean coordinates are supported! */
					if(ig->supportsEuclideanCoordinates()==true)
					{
						pnt_prop=this->ccw->addLocalProperty(0,string(ig->getPropertyName4Coordinates()), Point<double>());
						this->ccw->getPropertyName4Coordinates()=string(ig->getPropertyName4Coordinates());
					}
					
					/* Now, we must check if the field values are supported! */
					if(ig->supportsFieldValues()==true)
					{
						fv_prop=this->ccw->addLocalProperty(0,string(ig->getPropertyName4FieldValue()),aaa); 
						this->ccw->getPropertyName4FieldValue()=string(ig->getPropertyName4FieldValue());
					}
				}
				
				/* Now, we copy all vertices! */
				for(it=ig->begin(0);it!=ig->end(0); it++)
				{
					/* Now, we create a new vertex and then we add auxiliary information! */
					this->ccw->addSimplex(cp);
					if(reqParams)
					{
						/* Now, we must check if the Euclidean coordinates are supported! */
						if(ig->supportsEuclideanCoordinates()==true)
						{
							a=ig->getPropertyValue(string(this->ccw->getPropertyName4Coordinates()),SimplexPointer(it.getPointer()),p);
							pnt_prop->set(SimplexPointer(it.getPointer()),Point<double>(a));
						}
					
						/* Now, we must check if the field values are supported! */
						if(ig->supportsFieldValues()==true)
						{
							f=ig->getPropertyValue(string(this->ccw->getPropertyName4FieldValue()),SimplexPointer(it.getPointer()),aaa);
							fv_prop->set(SimplexPointer(it.getPointer()),f);
						}
					}
				}
				
				/* Now, we can copy the other simplices */
				if(ig->type()==0) { return; }
				for(unsigned int k=1;k<=ig->type();k++)
				{
					/* Now, we copy all the k-simplices! */
					for(it=ig->begin(k);it!=ig->end(k);it++)
					{
						/* Now, we add a new k-simplex and then we copy its boundary! */
						this->ccw->addSimplex(k,cp);
						for(unsigned int i=0;i<k+1;i++)
						{
							this->ccw->simplex(cp).b(i).setType(ig->simplex(SimplexPointer(it.getPointer())).bc(i).type());
							this->ccw->simplex(cp).b(i).setId(ig->simplex(SimplexPointer(it.getPointer())).bc(i).id());
						}
					}
				}
			}
			
			/// This member function computes the partial coboundary relation for each simplex in the current <i>Incidence Simplicial</i> data structure.
			/**
			 * This member function computes the partial coboundary relation for each simplex in the current <i>Incidence Simplicial</i> data structure: we start our analysis from the
			 * corresponding <i>Incidence Graph</i> and we try to identify all the connected components in the star of a simplex (alternatively, in the link of a simplex) through the
			 * mangrove_tds::ISManager::identifyComponents() member function. We remark that an <i>Incidence Simplicial</i> data structure must store one simplex of dimension k+1 for each
			 * connected component in the star of a k-simplex.<p><b>IMPORTANT:</b> in this member function, we use two global properties with boolean values, called <i>visited</i> and
			 * <i>inStar</i>, for identifying connected components in the link of simplices. We remove such properties, if they already belong to the input <i>Incidence Graph</i>. In any 
			 * case, we remove them at the end of computation.
			 * \param ig the <i>Incidence Graph</i> corresponding to the <i>Incidence Simplicial</i> to be created
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::ISManager::identifyComponents(), mangrove_tds::IG, mangrove_tds::IS,
			 * mangrove_tds::BaseSimplicialComplex
			 */
			inline void computeCoboundary(IG *ig)
			{
				vector<SimplexPointer> cps;
				vector<SimplexPointer>::iterator cp;
				SimplexIterator it;
				unsigned n;
				
				/* First, we loop over the dimensions in the current 'ig' */
				if(ig->type()==0) { return; }
				for(SIMPLEX_TYPE d=0;d<=ig->type()-1;d++)
				{
					/* Now, we iterate over all the d-simplices! */
					for(it=ig->begin(d);it!=ig->end(d);it++)
					{
						/* First, we check if the current simplex is top or if it has only one simplex in its coboundary in 'ig' */
						n=ig->simplex(it.getPointer()).getCoboundarySize();
						if(n==0) { this->ccw->simplex(it.getPointer()).clearCoboundary(); }
						else
						{
							/* Now, we must extract the connected components of its star! */
							this->identifyComponents( ig, SimplexPointer(it.getPointer()), cps);
							for(cp=cps.begin();cp!=cps.end();cp++) { this->ccw->simplex(it.getPointer()).add2Coboundary(SimplexPointer(*cp)); }
						}
					}
				}
				
				/* If we arrive here, we have finished! */
				cps.clear();
			}
			
			/// This member function identifies all the connected components in the star of a simplex in the corresponding <i>Incidence Graph</i>.
			/**
			 * This member function identifies all the connected components in the star of a simplex in the corresponding <i>Incidence Graph</i>, by returning a partial coboundary relation for
			 * each component. We remark that an <i>Incidence Simplicial</i> data structure must store one simplex of dimension k+1 for each connected component in the star of a k-simplex
			 * (alternatively, in the link of a simplex).<p><b>IMPORTANT:</b> in this member function, we use two global properties with boolean values, called <i>visited</i> and
			 * <i>inStar</i>, for identifying connected components in the link of simplices. We remove such properties, if they already belong to the input <i>Incidence Graph</i>. In any case,
			 * we remove them at the end of computation.
			 * \param ig the <i>Incidence Graph</i> corresponding to the <i>Incidence Simplicial</i> data structure to be built
		     	 * \param cc a pointer to the required simplex
			 * \param cps all the required simplices, one for each connected component
			 * \see mangrove_tds::IG, mangrove_tds::SimplexPointer, mangrove_tds::IS, mangrove_tds::BaseSimplicialComplex, mangrove_tds::PropertyBase,
			 * mangrove_tds::GlobalPropertyHandle, mangrove_tds::ISManager::retrieveComponents()
			 */
			inline void identifyComponents(IG *ig,const SimplexPointer &cc,vector<SimplexPointer> &cps)
			{
				GlobalPropertyHandle<bool> *isp,*vp;
				vector<SimplexPointer> str;
				SimplexPointer current;
				list<SimplexPointer>::iterator cp,temp;
				vector< vector<SimplexPointer> > aux;
				list<SimplexPointer> ccl,b,c;

				/* First, we clear components and then we compute the required coboundary! */
				cps.clear();
				ig->star(cc,&ccl);
				if(ig->hasProperty(string("visited"))) ig->deleteProperty(string("visited"));
			 	if(ig->hasProperty(string("inStar"))) ig->deleteProperty(string("inStar"));
			 	isp=ig->addGlobalProperty( string("inStar"),false);
			 	vp=ig->addGlobalProperty( string("visited"),false);
			 	for(cp=ccl.begin();cp!=ccl.end();cp++) { isp->set(SimplexPointer(*cp),true); }
				for(cp=ccl.begin();cp!=ccl.end();cp++)
			 	{
			 		/* Now, we check if the current simplex 'cp' has been marked, otherwise a new connected component can start */
			 		if(vp->get(SimplexPointer(*cp))==false)
			 		{
			 			queue<SimplexPointer> q;
			 			
			 			/* The analysis of a new connected component can start! */
			 			aux.push_back(vector<SimplexPointer>());
			 			q.push(SimplexPointer(*cp));
			 			while(!q.empty())
						{
							current = q.front();
							q.pop();
							if(vp->get(SimplexPointer(current))==false)
							{
								/* Now, we visit 'current' and then we proceed over its adjacents! */
								vp->set(SimplexPointer(current),true);
								aux.back().push_back(SimplexPointer(current));
								ig->boundary(current,&b);
								for(temp=b.begin();temp!=b.end();temp++) { if(isp->get(SimplexPointer(*temp))==true) q.push(*temp); }
								ig->star(current,&c);
								for(temp=c.begin();temp!=c.end();temp++) { if(isp->get(SimplexPointer(*temp))==true) q.push(*temp); }
							}
						}
			 		}
			 	}

			 	/* Now, we must extract all the simplices of dimension cc.type()+1 */
				for(unsigned int k=0;k<aux.size();k++)
				{
					/* Now, we extract all the simplices of dimension cc.type()+1 from aux[k] and then we take the first one! */
					extractSimplices(cc.type()+1,aux[k],str);
					cps.push_back( SimplexPointer(str[0]));
				}

				/* If we arrive here, we have finished and then we can free memory! */
				ig->deleteProperty(string("visited"));
				ig->deleteProperty(string("inStar")); 
				destroy(aux);
			}
			
			/// This member function generates all the unique simplices in the current simplicial complex.
			/**
			 * This member function generates all the unique simplices in the current simplicial complex: we start our analysis from the raw subsimplices of all the top simplices in the I/O
			 * component. We assume to retrieve all top simplices through the mangrove_tds::DataManager::getCollection() member function and to retrieve all the raw simplices (described
			 * through the mangrove_tds::RawFace class) through the mangrove_tds::DataManager::getRawFaces() member function.<p>In this algorithm, we generate all the unique simplices,
			 * by sorting all the raw simplices: in this way, we can also rebuild the boundary hierarchy between them.
			 * \param d the dimension of the unique simplices to be generated
			 * \param loader the component that contains the raw and top simplices describing the input simplicial complex
			 * \see mangrove_tds::ISManager::addEdge(), mangrove_tds::SIMPLEX_TYPE, mangrove_tds::RawFace, mangrove_tds::DataManager::getCollection(),
			 * mangrove_tds::SimplicesContainer, mangrove_tds::DataManager::getRawFaces(), mangrove_tds::Simplex
			 */
			template <class T> void getUnique(SIMPLEX_TYPE d,DataManager<T> *loader)
			{
				vector<RawFace> auxd;
				unsigned int r,c;
				SIMPLEX_ID v0,v1,indr,t;
				vector<unsigned int> cs;
				vector<SimplexPointer> f;
				SimplexPointer cp;
				
				/* First, we copy all the raw d-simplices in order to mantain the correspondence among them */
				for(unsigned int k=0;k<loader->getRawFaces()[d].size();k++)
				{
					loader->getRawFaces()[d][k].getOriginalPosition() = k;
					auxd.push_back(RawFace(loader->getRawFaces()[d].at(k)));	
				}
				
				/* Now, we sort the copy 'auxd' in order to remove duplicates (replicants). */				
				sort( auxd.begin(),auxd.end(),compare_faces);
				r=0;
				c=1;
				while( (r<auxd.size()) && (c<auxd.size()))
				{
					/* Now, we must identify connected components of faces! */
					if(same_faces(auxd[r],auxd[c]))
					{
						/* We accumulate replicants! */
						cs.push_back(c);
						c=c+1;
					}
					else
					{
						/* Now, we must finalize the identification of a new simplex */
						if(d==1)
						{
							/* Now, we must add an edge! */
							v0 = auxd[r].getVertices().at(0);
							v1 = auxd[r].getVertices().at(1);
							indr = this->addEdge(v0,v1);
						}
						else
						{
							/* Now, we must rebuild the boundary of the new simplex to be added */
							f.clear();
							for(unsigned int j=0;j<=d;j++)
							{
								t=auxd[r].getBoundary()[j];
								f.push_back( SimplexPointer(d-1, loader->getRawFaces()[d-1][t].getUniqueIndex()));
							}
							
							/* Now, we add the new simplex. */
							this->ccw->addSimplex(f,cp);
							indr = cp.id();
						}
						
						/* Now, we must complete the construction of the new simplex and then we prepare the creation of a new cluster of elements! */
						loader->getRawFaces()[d][auxd[r].getOriginalPosition()].getUniqueIndex() = indr;
						auxd[r].getUniqueIndex() = indr;
						for(unsigned int l=0;l<cs.size();l++) { loader->getRawFaces()[d][auxd[cs[l]].getOriginalPosition()].getUniqueIndex() = indr; }
						r=c;
						c=r+1;
						cs.clear();
					}
				}
				
				/* Now, we must complete the construction, finalizing the last cluster! */
				if(r<auxd.size())
				{
					/* Now, we must finalize the identification of a new simplex! */
					if(d==1)
					{
						/* Now, we must add an edge! */
						v0 = auxd[r].getVertices().at(0);
						v1 = auxd[r].getVertices().at(1);
						indr = this->addEdge(v0,v1);
					}
					else
					{
						/* Now, we must rebuild the boundary of the new simplex to be added */
						f.clear();
						for(unsigned int j=0;j<=d;j++)
						{
							t=auxd[r].getBoundary()[j];
							f.push_back( SimplexPointer(d-1, loader->getRawFaces()[d-1][t].getUniqueIndex()));
						}
							
						/* Now, we add the new simplex. */
						this->ccw->addSimplex(f,cp);
						indr = cp.id();
					}
					
					/* Now, we must complete the construction of the new simplex! */
					auxd[r].getUniqueIndex() = indr;
					loader->getRawFaces()[d][auxd[r].getOriginalPosition()].getUniqueIndex() = indr;
					for(unsigned int l=0;l<cs.size();l++)
					{
						auxd[cs[l]].getUniqueIndex() = indr;
						loader->getRawFaces()[d][auxd[cs[l]].getOriginalPosition()].getUniqueIndex() = indr;
					}
				}
			}
			
			/// This member function computes the partial coboundary relation for each simplex in the current <i>Incidence Simplicial</i> data structure.
			/**
			 * This member function computes the partial coboundary relation for each simplex in the current <i>Incidence Simplicial</i> data structure: we start our analysis from the
			 * corresponding incidence relation, described through an instance of the mangrove_tds::RawIncidence class, and we try to identify all the connected components in the star of a
			 * simplex (alternatively, in the link of a simplex) through the mangrove_tds::ISManager::retrieveComponents() member function. We remark that an <i>Incidence Simplicial</i> data
			 * structure must store one simplex of dimension k+1 for each connected component in the star of a k-simplex.<p><b>IMPORTANT:</b> we assume to use this member function only
			 * for building a new instance of the <i>Incidence Simplicial</i> data structure from scratch.
			 * \param inc the auxiliary data structure for storing coboundary relation
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::ISManager::retrieveComponents(), mangrove_tds::IG, mangrove_tds::IS,
			 * mangrove_tds::BaseSimplicialComplex, mangrove_tds::GlobalPropertyHandle, mangrove_tds::ISManager::retrieveComponents()
			 */
			inline void retrieveCoboundary(vector< vector<RawIncidence> > *inc)
			{
				SimplexIterator it;
				unsigned int n;
				SIMPLEX_TYPE tid;
				SIMPLEX_ID nid;
				vector<SimplexPointer>::iterator cp;
				GlobalPropertyHandle<bool> *rsp;

				/* First, we loop over the dimensions in the current 'ccw' */
				if(this->ccw->type()==0) { return; }
				rsp=this->ccw->addGlobalProperty(string("refSimplex"),false);
				for(SIMPLEX_TYPE d=0;d<this->ccw->type();d++)
				{
					/* Now, we iterate over all the d-simplices! */
					for(it=this->ccw->begin(d);it!=this->ccw->end(d);it++)
					{
						/* First, we check if the current simplex is top or if it has only one simplex in its coboundary in 'ig' */
						tid=it.getPointer().type();
						nid=it.getPointer().id();
						n=(*inc)[tid][nid].getSimplices().size();
						if(n==0) { this->ccw->simplex(it.getPointer()).clearCoboundary(); }
						else if(n==1) { this->ccw->simplex(it.getPointer()).add2Coboundary(SimplexPointer((*inc)[tid][nid].getSimplices()[0])); }
						else { this->retrieveComponents(inc,SimplexPointer(it.getPointer()),rsp); }
					}
				}
				
				/* Now, we remove the property 'refSimplex' */
				this->ccw->deleteProperty(string("refSimplex"));
			}
			
			/// This member function checks if a simplex has an intersection with a reference simplex.
			/**
			 * This member function checks if a simplex has an intersection with a reference d-simplex in the current <i>Incidence Simplicial</i> data structure: in other words, we check if
			 * the input simplex has at least one simplex in common with a reference d-simplex and thus if the reference d-simplex belongs to the boundary of the input simplex. We assume to
			 * have a local property with boolean values, called <i>refSimplex</i> in order to identify the reference simplex: such property must be associated to d-simplices.
			 * \param a the input simplex to be checked
			 * \param d the dimension of the reference simplex
			 * \param rsp the component for checking the intersection with a reference simplex
			 * \return <ul><li><i>true</i>, if the input simplex has an intersection with a reference simplex</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::GlobalPropertyHandle, mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE
			 */
			inline bool intersect(const SimplexPointer &a, SIMPLEX_TYPE d,GlobalPropertyHandle<bool> *rsp)
			{
				list<SimplexPointer> l;
				list<SimplexPointer>::iterator it;
				unsigned int k;
				
				/* We must understand the type of the input simplex 'a' */
				if(a.type()==0)
				{
					if(d==0) { return rsp->get(SimplexPointer(a)); }
					else { return false; }
				}
				else if(d==a.type()-1)
				{
					/* Required simplices are directly encoded in 'a' */
					for(k=0;k<=a.type();k++) { if(rsp->get(SimplexPointer(this->ccw->simplex(SimplexPointer(a)).b(k)))) return true; }
					return false;
				}
				else if(a.type()==d) return rsp->get(SimplexPointer(a));
				else if(a.type()<d) { return false; }
				else
				{
					/* Now, we must retrieve them through 'boundaryk' */
					this->ccw->boundaryk(SimplexPointer(a),d,&l);
					for(it=l.begin();it!=l.end();it++) if(rsp->get(SimplexPointer(*it))) return true;
					return false;
				}
			}
			
			/// This member function identifies all the connected components in the star of a simplex in the current <i>Incidence Simplicial</i> data structure.
			/**
			 * This member function identifies all the connected components in the star of a simplex in the corresponding <i>Incidence Simplicial</i> data structure, by returning a partial
			 * coboundary relation for each component. We remark that an <i>Incidence Simplicial</i> data structure must store one simplex of dimension k+1 for each connected component in
			 * the star of a k-simplex (alternatively, in the link of a simplex).<p><b>IMPORTANT:</b> we assume to use this member function only for building a new instance of the
			 * <i>Incidence Simplicial</i> data structure from scratch.
			 * \param inc the auxiliary data structure for storing coboundary relation 
		     	 * \param cc a pointer to the required simplex
			 * \param rsp the component for checking the intersection with a reference simplex
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::IS, mangrove_tds::GlobalPropertyHandle, mangrove_tds::RawIncidence,
			 * mangrove_tds::ISManager::selectFromStar(), mangrove_tds::ISManager::selectFromBoundary()
			 */
			inline void retrieveComponents(vector< vector<RawIncidence> > *inc,const SimplexPointer &cc,GlobalPropertyHandle<bool> *rsp)
			{
				GlobalPropertyHandle<bool> *vp;
				vector<SimplexPointer>::iterator cp,temp1;
				SimplexPointer current,temp;

				/* First, we prepare properties! */
				vp=this->ccw->addGlobalProperty(string("visited"),false);
				rsp->set(SimplexPointer(cc),true);
				for(cp=(*inc)[cc.type()][cc.id()].getSimplices().begin();cp!=(*inc)[cc.type()][cc.id()].getSimplices().end();cp++)
				{
					/* Now, we check if the current simplex 'cp' has been marked, otherwise a new connected component can start */
			 		if(vp->get(SimplexPointer(*cp))==false)
			 		{
			 			queue<SimplexPointer> q;
			 			
			 			/* The analysis of a new connected component can start! */
						this->ccw->simplex(SimplexPointer(cc)).add2Coboundary(SimplexPointer(*cp));
			 			q.push(SimplexPointer(*cp));
			 			while(!q.empty())
						{
							current = q.front();
							q.pop();
							if(vp->get(SimplexPointer(current))==false)
							{
								/* We can proceed on 'current': first, we analyze its boundary and then its co-boundary! */
								vp->set(SimplexPointer(current),true);
								this->selectFromBoundary(SimplexPointer(current),rsp,cc.type(),q);
								this->selectFromStar(SimplexPointer(current),rsp,q,inc,cc.type());
							}
						}
			 		}
				}

				/* If we arrive here, we have finished our research! */
				this->ccw->deleteProperty(string("visited"));
				rsp->set(SimplexPointer(cc),false);
			}
			
			/// This member function retrieves simplices incident in a reference simplex belonging to the boundary of an other simplex.
			/**
			 * This member function retrieves simplices incident in a reference simplex belonging to the boundary of an other simplex in the current <i>Incidence Simplicial</i> data structure.
			 * We remark that an <i>Incidence Simplicial</i> data structure must store one simplex of dimension k+1 for each connected component in the star of a k-simplex (alternatively, in
			 * the link of a simplex).<p><b>IMPORTANT:</b> we assume to use this member function only for building a new instance of the <i>Incidence Simplicial</i> data structure from
			 * scratch.
			 * \param current the input from which we extract its boundary
			 * \param isp the component for checking the intersection with a reference simplex
			 * \param ct the dimension of the reference simplex
			 * \param q the queue where we store the required simplices
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GlobalPropertyHandle, mangrove_tds::IS, mangrove_tds::RawIncidence,
			 * mangrove_tds::ISManager::selectFromStar(), mangrove_tds::ISManager::retrieveComponents()
			 */
			inline void selectFromBoundary(const SimplexPointer &current,GlobalPropertyHandle<bool> *isp,SIMPLEX_TYPE ct,queue<SimplexPointer> &q)
			{
				queue<SimplexPointer> q1;
				SimplexPointer temp,aux;

				/* Now, we select simplices on the 'current' boundary, which are also incident in 'cc' */
				if(current.type()>ct+1)
				{
					/* We visit the boundary simplices */
					q1.push(SimplexPointer(current));
					while(!q1.empty())
					{
						temp=q1.front();
						q1.pop();
						if( (temp.type()>1) && (temp.type()-1>ct))
						{
							/* We forward on boundary simplices */
							for(unsigned int k=0;k<=temp.type();k++)
							{
								aux=this->ccw->simplex(temp).b(k);
								if(this->intersect(SimplexPointer(current),ct,isp)==true)
								{
									/* We proceed only on boundary simplices incident in 'cc' */
									q.push(SimplexPointer(aux));
									q1.push(SimplexPointer(aux));
								}
							}
						}
					}	
				}
			}
			
			/// This member function retrieves all simplices in the star of a simplex in the current data structure incident into a reference simplex.
			/**
			 * This member function retrieves all simplices in the star of a simplex in the corresponding <i>Incidence Simplicial</i> data structure, incident into a reference simplex. We
			 * remark that an <i>Incidence Simplicial</i> data structure must store one simplex of dimension k+1 for each connected component in the star of a k-simplex (alternatively, in the
			 * link of a simplex).<p><b>IMPORTANT:</b> we assume to use this member function only for building a new instance of the <i>Incidence Simplicial</i> data structure from scratch.
			 * \param c a pointer to the required simplex
			 * \param isp the component for checking the intersection with a reference simplex
			 * \param q1 the queue where we store the required simplices
			 * \param inc the auxiliary data structure for storing coboundary relation
			 * \param d the dimension of the reference simplex		 
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GlobalPropertyHandle, mangrove_tds::IS, mangrove_tds::RawIncidence,
			 * mangrove_tds::ISManager::selectFromCoboundary(), mangrove_tds::ISManager::retrieveComponents()
			 */
			inline void selectFromStar(const SimplexPointer &c, GlobalPropertyHandle<bool> *isp,queue<SimplexPointer> &q1,vector< vector<RawIncidence> > *inc,SIMPLEX_TYPE d)
			{
				queue<SimplexPointer> q;
				SimplexPointer current;
				vector<SimplexPointer>::iterator cp;
				GlobalPropertyHandle<bool> *vp;

				/* We can start our research! */
				q.push(SimplexPointer(c));
				vp=this->ccw->addGlobalProperty( string("visited_star"),false);
				while(!q.empty())
				{
					current = q.front();
					q.pop();
					if((current.type()>c.type()) && (vp->get(SimplexPointer(current))==false))
					{
						vp->set(SimplexPointer(current),true);
						if(this->intersect(SimplexPointer(current),d,isp)) q1.push(SimplexPointer(current));
					}
					
					/* If we arrive here, we can proceed on the incident simplices */
					for(cp=(*inc)[current.type()][current.id()].getSimplices().begin();cp!=(*inc)[current.type()][current.id()].getSimplices().end();cp++)
					{ q.push(SimplexPointer(*cp)); }
				}

				/* If we arrive here, we remove the 'visited' property! */
				this->ccw->deleteProperty(string("visited_star"));
			}
		};
	}
	
#endif

