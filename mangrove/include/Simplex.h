/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library                                   
 *                                                                           				    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * June 2011 (Revised on May 2012)
 *                                                                    
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * Simplex.h - representations of simplices contained in a simplicial complex                                                    
 ***********************************************************************************************/
 
/* Should we include this header file? */
#ifndef SIMPLEX_H_

	#define SIMPLEX_H_
	
	#include <vector>
	#include <list>
	#include <assert.h>
	#include <iostream>
	#include <algorithm>
	#include <map>
	using namespace std;
	
	/// Representations of simplices contained in a simplicial complex.
	/**
	 * The classes defined in this file are useful for representing and accessing a simplex in a data structure for representing a simplicial complex.
	 * \file Simplex.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */

	/* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		/// An alias used in order to define the simplex type, i.e. its dimension.
		typedef unsigned int SIMPLEX_TYPE;

		/// An alias used in order to define the simplex identifier, i.e. its position in a simplices collection.
		typedef unsigned int SIMPLEX_ID;
		
		/// A pointer to a simplex directly encoded in a data structure.
		/**
		 * This class describes a pointer to a a simplex directly encoded in a data structure, described through an instance of the mangrove_tds::BaseSimplicialComplex class. If the data
		 * structure encodes all simplices, then we call it as <i>global</i> data structure, otherwise it is called <i>local</i> data structure. Examples of global data structures are the
		 * <i>Incidence Graph</i> (described by the mangrove_tds::IG class), the <i>Incidence Simplicial</i> (described by the mangrove_tds::IS class) data structure and the
		 * <i>Simplified Incidence Graph</i> (described by the mangrove_tds::SIG class). The <i>IA*</i> (described by the mangrove_tds::GIA class) is a local data structure, because it
		 * encodes only top simplices.<p>You can use an instance of the mangrove_tds::SimplexPointer only for accessing a simplex directly encoded in a global data structure. Otherwise, you
		 * cannot use this pointer, but you must apply the pointer for a generic simplex, described through the mangrove_tds::GhostSimplexPointer class.<p>A pointer to an existing simplex is
		 * composed by the dimension (called <i>simplex type</i> and described by the mangrove_tds::SIMPLEX_TYPE type) and by the position in the corresponding collection of simplices
		 * (called <i>simplex identifier</i> and described by the mangrove_tds::SIMPLEX_ID type).<p>This class is used to extract all the topological relations and to access properties (i.e.
		 * auxiliary information like Euclidean coordinates and field values associated to simplices, described by instances of the mangrove_tds::PropertyBase class) for a simplex directly
		 * encoded in a data structure.
		 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::IG, mangrove_tds::IS, mangrove_tds::SIG, mangrove_tds::GIA, mangrove_tds::PropertyBase,
		 * mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle,
		 * mangrove_tds::GhostSimplexPointer
		 */
		class SimplexPointer
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a not initialized pointer to a simplex: you should initialize it by using the mangrove_tds::SimplexPointer::setType() and the
			 * mangrove_tds::SimplexPointer::setId() member functions.
			 * \see mangrove_tds::SimplexPointer::setType(), mangrove_tds::SimplexPointer::setId(), mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID
			 */
			SimplexPointer();
			
			/// This member function creates a new instance of this class.
			/**
			 * In this member function, we assign dimension and identifier for the simplex to be referred by the new pointer: we assume that this simplex is <i>valid</i> in the related data
			 * structure.<p><b>IMPORTANT:</b>we say that a simplex is <i>valid</i> in a data structure if and only if its location exists and it has not been marked as <i>deleted</i> by the
			 * garbage collector mechanism, like in the mangrove_tds::SimplicesContainer class. 
			 * \param typep the dimension of the simplex to be referred by the new simplex pointer
			 * \param idp the identifier of the simplex to be referred by the new simplex pointer
			 * \see mangrove_tds::SimplexPointer::setType(), mangrove_tds::SimplexPointer::setId(), mangrove_tds::SimplicesContainer, mangrove_tds::SIMPLEX_TYPE,
			 * mangrove_tds::SIMPLEX_ID
			 */
			SimplexPointer(SIMPLEX_TYPE typep,SIMPLEX_ID idp);
			
			/// This member function creates a new instance of this class.
			/**
			 * In this member function, we create a copy of the input simplex pointer, by replicating its internal state.
			 * \param cp the simplex pointer to be copied
			 * \see mangrove_tds::SimplexPointer::setType(), mangrove_tds::SimplexPointer::setId(), mangrove_tds::SimplexPointer::id(), mangrove_tds::SimplexPointer::type(),
			 * mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID
			 */
			SimplexPointer(const SimplexPointer& cp);
			
			/// This member function destroys an instance of this class.
			inline virtual ~SimplexPointer() { ; }
			
			/// This member function returns the dimension of the simplex referred by the current pointer.
			/**
			 * This member function returns the dimension of the simplex referred by the current pointer: you can also update the dimension of such simplex through the
			 * mangrove_tds::SimplexPointer::setType() member function.
			 * \return the dimension of the simplex referred by the current pointer
			 * \see mangrove_tds::SimplexPointer::setType(), mangrove_tds::SIMPLEX_TYPE
			 */
			inline SIMPLEX_TYPE type() const { return this->_type; }
			
			/// This member function updates the dimension of the simplex referred by the current pointer.
			/**
			 * This member function updates the dimension of the simplex referred by the current pointer: you can also obtain the dimension of such simplex through the 
			 * mangrove_tds::SimplexPointer::type() member function.
			 * \param typep the dimension of the simplex to be referred by the current pointer
			 * \see mangrove_tds::SimplexPointer::type(), mangrove_tds::SIMPLEX_TYPE
			 */
			inline void setType(SIMPLEX_TYPE typep) { this->_type = typep; }
			
			/// This member function returns the identifier of the simplex referred by the current pointer.
			/**
			 * This member function returns the identifier of the simplex referred by the current pointer: you can also update the identifier of such simplex through the
			 * mangrove_tds::SimplexPointer::setId() member function.
			 * \return the identifier of the simplex referred by the current pointer
			 * \see mangrove_tds::SimplexPointer::setId(), mangrove_tds::SIMPLEX_ID
			 */
			inline SIMPLEX_ID id() const { return this->_id; }
			
			/// This member function updates the identifier of the simplex referred by the current pointer.
			/**
			 * This member function updates the identifier of the simplex referred by the current pointer: you can also obtain the identifier of such simplex through the
			 * mangrove_tds::SimplexPointer::id() member function.
		     	 * \param idp the identifier of the simplex to be referred by the current simplex pointer
		     	 * \see mangrove_tds::SimplexPointer::id(), mangrove_tds::SIMPLEX_ID
			 */
			inline void setId(SIMPLEX_ID idp) { this->_id = idp; }
			
			/// This member function writes a debug representation of the current simplex pointer on an output stream.
			/**
			 * In the debug representation, we write dimension and identifier of the simplex referred by the current simplex pointer.<p><b>IMPORTANT:</b> this member function is intended
			 * only for debugging purposes, otherwise you should apply the writing operator in order to obtain a compact representation of a simplex pointer.
			 * \param os the output stream where we can write the debug representation of a simplex pointer
			 * \see mangrove_tds::SimplexPointer::id(), mangrove_tds::SimplexPointer::type(), mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID
			 */
			void debug(ostream& os = cout) const;
			
			/// This operator writes a compact representation of a simplex pointer on a output stream.
			/**
			 * In this operator, we write a compact representation of a simplex pointer, suitable for transmitting and receiving objects. Such representation is formatted in accordance with 
			 * these guidelines:<p><i>[ type ] [ identifier ]</i><p><b>IMPORTANT:</b> this operator is not intended for debugging purposes. Otherwise, you should apply the
			 * mangrove_tds::SimplexPointer::debug() member function.
			 * \param os the output stream where we can write the compact representation of a simplex pointer
			 * \param p the simplex pointer to be written
			 * \return the output stream after having written the input simplex pointer description
			 * \see mangrove_tds::SimplexPointer::debug(), mangrove_tds::SimplexPointer::id(), mangrove_tds::SimplexPointer::type(), mangrove_tds::SIMPLEX_TYPE,
			 * mangrove_tds::SIMPLEX_ID
			 */
			inline friend ostream& operator<<(ostream& os, const SimplexPointer& p)
			{
				/* Repetita iuvant - simplex pointer representation: [ type ] [ identifier ] */
				os<<p.type()<<" "<<p.id()<<endl;
				os.flush();
				return os;
			}
			
			/// This operator rebuilds a simplex pointer by reading its compact representation from an input stream.
			/**
			 * In this operator, we rebuild a new instance of a simplex pointer by reading its compact representation from an input stream. Such representation is formatted in accordance with
			 * these guidelines:<p><i>[ type ] [ identifier ]</i><p><b>IMPORTANT:</b> this operator is not intended for debugging purposes. Otherwise, you should apply the
			 * mangrove_tds::SimplexPointer::debug() member function.
			 * \param in the input stream from which we can rebuild a simplex pointer
			 * \param p the simplex pointer to be rebuilt
			 * \return the input stream after having read the input simplex pointer description
			 * \see mangrove_tds::SimplexPointer::debug(), mangrove_tds::SimplexPointer::setId(), mangrove_tds::SimplexPointer::setType(), mangrove_tds::SIMPLEX_TYPE,
			 * mangrove_tds::SIMPLEX_ID
			 */
			inline friend istream& operator>>(istream& in,SimplexPointer& p)
			{
				SIMPLEX_TYPE t;
				SIMPLEX_ID i;
				
				/* Repetita iuvant - simplex pointer representation: [ type ] [ identifier ] */
				in>>t;
				in>>i;
				p.setType(t);
				p.setId(i);
				return in;
			}
			
			/// This member function checks if two simplex pointers are equal.
			/**
			 * In this member function, two simplex pointers are equal if and only if they refer the same simplex: in other words, the referred simplices dimension and the identifier must be
			 * the same ones.
			 * \param a the simplex pointer to be compared with the current one
			 * \return <ul><li><i>true</i>, if the current simplex pointer and the input one refer the same simplex</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplexPointer::id(), mangrove_tds::SimplexPointer::type()
			 */
			bool theSame(const SimplexPointer& a) const;
			
			/// This operator checks if two simplex pointers are equal.
			/**
			 * In this operator, two simplex pointers are equal if and only if they refer the same simplex: in other words, the referred simplices dimension and the identifier must be the
			 * same ones.
			 * \param a the simplex pointer to be compared with the current one
			 * \return <ul><li><i>true</i>, if the current simplex pointer and the input one refer the same simplex</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplexPointer::id(), mangrove_tds::SimplexPointer::type(), mangrove_tds::SimplexPointer::theSame() 
			 */
			bool operator==(const SimplexPointer& a);
			
			/// This operator compares two simplex pointers.
			/**
			 * In this operator, we compare two simplex pointers: the first pointer is the current one, while this operator receives the second one as input. For us, the first pointer is
			 * lesser than the second one by giving priority to the referred simplices dimension, in accordance with the lexicographic order.
			 * \return <ul><li><i>true</i>, if the current pointer is lesser than the other</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplexPointer::id(), mangrove_tds::SimplexPointer::type()
			 */
			bool operator<(const SimplexPointer& a);
			
			protected:
			
			/// The dimension of the referred simplex.
			/**
			 * \see mangrove_tds::SIMPLEX_TYPE
			 */
			SIMPLEX_TYPE _type;

			/// The identifier of the referred simplex.
			/**
			 * \see mangrove_tds::SIMPLEX_ID
			 */
			SIMPLEX_ID _id;
		};
		
		/// A compact representation of a simplex directly encoded in a data structure.
		/**
		 * This class contains a compact representation of a simplex directly encoded in a data structure for describing generic shapes in arbitrary dimensions: in this way, we can encode
		 * different data structures by using a common framework, provided and described through the mangrove_tds::BaseSimplicialComplex class.<p>For each simplex of dimension k we
		 * encode:<ul><li>k+1 simplices of dimension j<k belonging to its boundary relation: we also remark the boundary of a 0-simplex is empty;</li><li>some simplices that belong to its
		 * coboundary relation;</li><li>some simplices for describing all simplices sharing a (k-1)-face with the current simplex.</li></ul>Subsets of simplices directly encoded in topological
		 * relations depends from the specific data structure: in particular, the adjacency relation can be encoded in an efficient way: if the shared (k-1)-face is manifold, then the adjacent
		 * simplex is directly encoded, otherwise we encode a pointer to a (k-1)-face and thus we can access its coboundary. In this way, we obtain a unique list of adjacent simplices,
		 * independently from their number.<p>In order to increase the expressive power of a data structure we can also store the <i>auxiliary boundary</i> for a k-simplex, with k>1. It is
		 * formed by auxiliary simplices, different from the canonical boundary, that belong to the boundary of the current simplex. For instance, if we store the vertices of a triangle as
		 * canonical boundary, then its auxiliary boundary is formed by at least one of its edges. This feature is very useful in those data structures, like the <i>Non-manifold IA</i>
		 * (mangrove_tds::NMIA class), where we store non-manifold edge belonging to the boundary of a top simplex.<p>A boolean flag is also present, but it is internally used by the visiting
		 * algorithms, while a boolean flag that represents the <i>deleted</i> status for the current simplex is used by the garbage collection mechanism implemented in the
		 * mangrove_tds::SimplicesContainer class.<p>Finally, we can use an instance of the mangrove_tds::SimplexPointer class in order to refer a simplex directly encoded in a data structure:
		 * in some special cases, we can apply an instance of the mangrove_tds::GhostSimplexPointer class for accessing a simplex not directly encoded in a data structure.
		 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::PropertyBase, mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplexPointer, mangrove_tds::SimplicesContainer,
		 * mangrove_tds::NMIA
		 */
		class Simplex
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function does not initialize the new simplex, thus you should initialize this new instance before using it through the member functions provided by the
			 * mangrove_tds::Simplex class.
			 * \see mangrove_tds::Simplex::add2Boundary(), mangrove_tds::Simplex::add2Coboundary(), mangrove_tds::Simplex::add2Adjacency(), mangrove_tds::Simplex::setType(),
			 * mangrove_tds::Simplex::add2AuxiliaryBoundary()
			 */
			Simplex();
			
			/// This member function creates a new instance of this class.
			/**
			 * In this member function, we create a new simplex of a certain dimension by allocating all the needed space for the boundary relation, but we do not initialize it. We remark a
			 * k-simplex has a boundary composed by k+1 simplices of dimension j<k, except for a 0-simplex since a 0-simplex has an empty boundary.<p>We also create empty coboundary and
			 * adjacency relations, thus you should refer member functions offered by the mangrove_tds::Simplex class in order to initialize topological relations for the new simplex.
			 * \param type the dimension of the new simplex
			 * \see mangrove_tds::Simplex::add2Boundary(), mangrove_tds::Simplex::add2Coboundary(), mangrove_tds::Simplex::add2Adjacency(), mangrove_tds::Simplex::setType(),
			 * mangrove_tds::Simplex::add2AuxiliaryBoundary(), mangrove_tds::SIMPLEX_TYPE
			 */
			Simplex(SIMPLEX_TYPE type);
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a deep copy of the input simplex, by replicating all the internal state fields.
			 * \param c the simplex to be copied
			 * \see mangrove_tds::Simplex::setDeleted(), mangrove_tds::Simplex::setVisited(), mangrove_tds::Simplex::isDeleted(), mangrove_tds::Simplex::isVisited(),
			 * mangrove_tds::Simplex::bc(), mangrove_tds::Simplex::cc(), mangrove_tds::Simplex::ac(), mangrove_tds::Simplex::clearBoundary(), mangrove_tds::Simplex::clearCoboundary(),
			 * mangrove_tds::Simplex::clearAdjacency()
			 */
			Simplex(const Simplex& c);
			
			/// This member function destroys an instance of this class.
			/**
			 * This member function destroys an instance of this class, deallocating its internal state.
			 * \see mangrove_tds::Simplex::clearBoundary(), mangrove_tds::Simplex::clearCoboundary(), mangrove_tds::Simplex::clearAdjacency(),
			 * mangrove_tds::Simplex::clearAuxiliaryBoundary()
			 */
			virtual ~Simplex();
			
			/// This member function checks if the current simplex is marked as <i>deleted</i> in the garbage collector.
			/**
			 * This member function checks if the current simplex is marked as <i>deleted</i> in the garbage collector inside a collection of simplices: for example, a similar mechanism is
			 * provided by the mangrove_tds::SimplicesContainer class.
			 * \return <ul><li><i>true</i>, if the current simplex is marked as <i>deleted</i> for the garbage collector</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::Simplex::setDeleted(), mangrove_tds::SimplicesContainer, mangrove_tds::BaseSimplicialComplex
			 */
			inline bool isDeleted() const { return this->deleted; }
			
			/// This member function updates the deletion state of the current simplex for the garbage collector.
			/**
			 * This member function updates the deletion state of the current simplex for the garbage collector inside a collection of simplices: for example, a similar mechanism is provided
			 * by the mangrove_tds::SimplicesContainer class.<p>The current simplex can be marked with a boolean flag: if it is <i>true</i>, then the current simplex has been deleted,
			 * otherwise it has not been deleted.
			 * \param b the boolean flag for marking the current simplex for the garbage collector
			 * \see mangrove_tds::Simplex::isDeleted(), mangrove_tds::SimplicesContainer, mangrove_tds::BaseSimplicialComplex
			 */
			inline void setDeleted(bool b) { this->deleted = b; }
			
			/// This member function checks if the current simplex has been visited in order to extract topological relations.
			/**
			 * This member function checks if the current simplex has been visited in order to extract topological relations in the mangrove_tds::BaseSimplicialComplex class.
			 * \return <ul><li><i>true</i>, if the current simplex has been visited in order to extract topological relations</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::Simplex::setVisited(), mangrove_tds::BaseSimplicialComplex
			 */
			inline bool isVisited() const { return this->visited; }
			
			/// This member function updates the visiting status of the current simplex in order to extract topological relations.
			/**
			 * This member function updates the visiting status of the current simplex in order to extract topological relations in the mangrove_tds::BaseSimplicialComplex class. The current
			 * simplex can be marked as <i>visited</i> with a boolean flag: if flag is <i>true</i>, then the current simplex has been visited, otherwise it has not been visited.
			 * \param b the boolean flag for marking the current simplex as visited
			 * \see mangrove_tds::Simplex::isVisited(), mangrove_tds::BaseSimplicialComplex
			 */
			inline void setVisited(bool b) { this->visited = b; }
			 
			/// This member function returns the dimension of the current simplex.
			/**
			 * This member function returns the dimension of the current simplex, i.e. the minimum number of simplices describing its boundary relation. The boundary of a k-simplex is
			 * composed by k+1 simplices of dimension j<k: we also remark that a 0-simplex has an empty boundary.
			 * \return the dimension of the current simplex
			 * \see mangrove_tds::Simplex::setType(), mangrove_tds::SIMPLEX_TYPE, mangrove_tds::Simplex::getBoundarySize()
			 */
			SIMPLEX_TYPE type() const;
			
			/// This member function updates the dimension of the current simplex.
			/**
			 * This member function updates the dimension of the current simplex, i.e. the minimum number of simplices describing its boundary relation. The boundary of a k-simplex is
			 * composed by k+1 simplices of dimension j<k: we also remark that a 0-simplex has an empty boundary.<p><b>IMPORTANT:</b> in this member function, we reallocate all the neeeded
			 * space for the boundary relation (including the auxiliary boundary) without initialize it and we also clear the other topological relations for the current simplex. Thus, you
			 * should refer the member functions offered by the mangrove_tds::Simplex class in order to initialize the topological relations of the current simplex.  
			 * \param t the new dimension of the current simplex
			 * \see mangrove_tds::Simplex::clearBoundary(), mangrove_tds::Simplex::clearCoboundary(), mangrove_tds::Simplex::clearAdjacency(), mangrove_tds::Simplex::type(),
			 * mangrove_tds::SIMPLEX_TYPE, mangrove_tds::Simplex::add2Boundary(), mangrove_tds::Simplex::add2Coboundary(), mangrove_tds::Simplex::add2Adjacency(),
			 * mangrove_tds::Simplex::add2AuxiliaryBoundary(), mangrove_tds::Simplex::supportsAuxiliaryBoundary()
			 */
			void setType(SIMPLEX_TYPE t);
			
			/// This member function checks if the current simplex supports the auxiliary boundary.
			/**
			 * This member function checks if the current simplex supports the auxiliary boundary. It is formed by auxiliary simplices, different from the canonical boundary, that belong to
			 * the boundary of the current simplex. For instance, if we store the vertices of a triangle as canonical boundary, then its auxiliary boundary is formed by at least one of its
			 * edges. This feature is very useful in those data structures, like the <i>Non-manifold IA</i> (mangrove_tds::NMIA class), where we store non-manifold edge belonging to the
			 * boundary of a top simplex.
			 * \return <ul><li><i>true</i>, if the current simplex supports the auxiliary boundary</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointer, mangrove_tds::NMIA, mangrove_tds::Simplex::type(),
			 * mangrove_tds::Simplex::add2AuxliaryBoundarySimplex(), mangrove_tds::Simplex::getAuxiliaryBoundarySimplex(), mangrove_tds::Simplex::removeAuxiliaryBoundarySimplex()
			 */
			 inline bool supportsAuxiliaryBoundary() const { return (this->type()>1); }
			 
			/// This member function returns the number of simplices belonging to the current simplex boundary.
			/**
			 * This member function returns the number of simplices belonging to the current simplex boundary: the boundary of a k-simplex is composed by k+1 simplices of dimension j<k,
			 * except for a 0-simplex since a 0-simplex has an empty boundary.
			 * \return the number of simplices belonging to the current simplex boundary
			 * \see mangrove_tds::Simplex::setType(), mangrove_tds::Simplex::type(), mangrove_tds::Simplex::clearBoundary(), mangrove_tds::Simplex::bc(),
			 * mangrove_tds::Simplex::add2Boundary(), mangrove_tds::Simplex::removeFromBoundary(), mangrove_tds::Simplex::positionInBoundary()
			 */
			inline unsigned int getBoundarySize() const { return this->boundary.size(); }
			
			/// This member function returns the number of simplices belonging to the current simplex coboundary.
			/**
			 * This member function returns the number of simplices belonging to the current simplex coboundary: the coboundary of a k-simplex is composed by some simplices of dimension j>k,
			 * incident in the current one.
			 * \return the number of simplices belonging to the current simplex coboundary
			 * \see mangrove_tds::Simplex::clearCoboundary(), mangrove_tds::Simplex::cc(), mangrove_tds::Simplex::add2Coboundary(), mangrove_tds::Simplex::removeFromCoboundary(),
			 * mangrove_tds::Simplex::positionInCoboundary()
			 */
			inline unsigned int getCoboundarySize() const { return this->coboundary.size(); }
			
			/// This member function returns the number of simplices belonging to the auxiliary boundary of the current simplex.
			/**
			 * This member function returns the number of simplices belonging to the auxiliary boundary of the current simplex. It is formed by auxiliary simplices, different from the
			 * canonical boundary, that belong to the boundary of the current simplex. For instance, if we store the vertices of a triangle as canonical boundary, then its auxiliary boundary
			 * is formed by at least one of its edges. This feature is very useful in those data structures, like the <i>Non-manifold IA</i> (mangrove_tds::NMIA class), where we store
			 * non-manifold edges belonging to the boundary of a top simplex.<p><b>IMPORTANT:</b> if we cannot complete this operation for any reason, then this member function will fail if
			 * and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \return the number of simplices belonging to the auxiliary boundary of the current simplex
			 * \see mangrove_tds::SIMPLEX_ID, mangrrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::Simplex::supportsAuxiliaryBoundary(),
			 * mangrove_tds::Simplex::add2AuxliaryBoundarySimplex(), mangrove_tds::Simplex::getAuxiliaryBoundarySimplex(), mangrove_tds::Simplex::removeAuxiliaryBoundarySimplex()
			 */
			unsigned int auxiliaryBoundarySize() const;
			
			/// This member function returns the number of simplices of a certain dimension belonging to the auxiliary boundary of the current simplex.
			/**
			 * This member function returns the number of simplices of a certain dimension belonging to the auxiliary boundary of the current simplex. It is formed by auxiliary simplices,
			 * different from the canonical boundary, that belong to the boundary of the current simplex. For instance, if we store the vertices of a triangle as canonical boundary, then its
			 * auxiliary boundary is formed by at least one of its edges. This feature is very useful in those data structures, like the <i>Non-manifold IA</i> (mangrove_tds::NMIA class),
			 * where we store non-manifold edges belonging to the boundary of a top simplex.<p><b>IMPORTANT:</b> if we cannot complete this operation for any reason, then this member
			 * function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param t the dimension of the required simplices in the auxiliary boundary of the current simplex
			 * \return the number of simplices of a certain dimension belonging to the auxiliary boundary of the current simplex
			 * \see mangrove_tds::SIMPLEX_ID, mangrrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::Simplex::supportsAuxiliaryBoundary(),
			 * mangrove_tds::Simplex::add2AuxliaryBoundarySimplex(), mangrove_tds::Simplex::getAuxiliaryBoundarySimplex(), mangrove_tds::Simplex::removeAuxiliaryBoundarySimplex() 
			 */
			unsigned int auxiliaryBoundarySize(SIMPLEX_TYPE t) const;
			
			/// This member function checks if a simplex is encoded in the auxiliary boundary of the current simplex.
			/**
			 * This member function checks if a simplex is encoded in the auxiliary boundary of the current simplex. It is formed by auxiliary simplices, different from the canonical
			 * boundary, that belong to the boundary of the current simplex. For instance, if we store the vertices of a triangle as canonical boundary, then its auxiliary boundary is formed
			 * by at least one of its edges. This feature is very useful in those data structures, like the <i>Non-manifold IA</i> (mangrove_tds::NMIA class), where we store non-manifold
			 * edges belonging to the boundary of a top simplex.<p><b>IMPORTANT:</b> if we cannot complete this operation for any reason, then this member function will fail if and only if
			 * the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param cp a pointer to the required simplex to be analyzed
			 * \return <ul><li><i>true</i>, if the input simplex belongs to the auxiliary boundary of the current simplex</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SIMPLEX_ID, mangrrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::Simplex::supportsAuxiliaryBoundary(),
			 * mangrove_tds::Simplex::add2AuxliaryBoundarySimplex(), mangrove_tds::Simplex::getAuxiliaryBoundarySimplex(), mangrove_tds::Simplex::removeAuxiliaryBoundarySimplex()
			 */
			bool isAuxiliary(const SimplexPointer &cp) const;

			/// This member function deallocates the space reserved for the current simplex boundary.
			/**
			 * This member function deallocates all the space reserved for the current simplex boundary: the boundary of a k-simplex is composed by k+1 simplices of dimension j<k, except for
			 * a 0-simplex since a 0-simplex has an empty boundary.
			 * \see mangrove_tds::Simplex::setType(), mangrove_tds::Simplex::type(), mangrove_tds::Simplex::getBoundarySize(), mangrove_tds::Simplex::bc(),
			 * mangrove_tds::Simplex::add2Boundary(), mangrove_tds::Simplex::removeFromBoundary(), mangrove_tds::Simplex::positionInBoundary()
			 */
			inline void clearBoundary() { this->boundary.clear(); }
			
			/// This member function deallocates the space reserved for the current simplex coboundary.
			/**
			 * This member function deallocates all the space reserved for the current simplex coboundary: the coboundary of a k-simplex is composed by some simplices of dimension j>k,
			 * incident in the current one.
			 * \see mangrove_tds::Simplex::getCoboundarySize(), mangrove_tds::Simplex::cc(), mangrove_tds::Simplex::add2Coboundary(), mangrove_tds::Simplex::removeFromCoboundary(),
			 * mangrove_tds::Simplex::positionInCoboundary()
			 */
			inline void clearCoboundary() { this->coboundary.clear(); }
			
			/// This member function deallocates the space reserved for the current simplex adjacency.
			/**
			 * This member function deallocates all the space reserved for the current simplex adjacency: it is composed by some simplices for describing all simplices sharing a (k-1)-face
			 * with the current simplex. It can be encoded in an efficient way: if the shared (k-1)-face is manifold, then the adjacent simplex is directly encoded, otherwise we encode a
			 * pointer to a (k-1)-face and thus we can access its coboundary. In this way, we obtain a unique list of adjacent simplices, independently from their number.
			 * \see mangrove_tds::Simplex::ac(), mangrove_tds::Simplex::add2Adjacency(), mangrove_tds::Simplex::removeFromAdjacency(), mangrove_tds::Simplex::hasAdjacency(),
			 * mangrove_tds::Simplex::isManifoldAdjacency(), mangrove_tds::Simplex::positionInAdjacency()
			 */
			void clearAdjacency();
			
			/// This member function deallocates the space reserved for auxiliary boundary of the current simplex.
			/**
			 * This member function deallocates the space reserved for auxiliary boundary of the current simplex. It is formed by auxiliary simplices, different from the canonical boundary,
			 * that belong to the boundary of the current simplex. For instance, if we store the vertices of a triangle as canonical boundary, then its auxiliary boundary is formed by at
			 * least one of its edges. This feature is very useful in those data structures, like the <i>Non-manifold IA</i> (mangrove_tds::NMIA class), where we store non-manifold edges
			 * belonging to the boundary of a top simplex.
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointer, mangrove_tds::Simplex::supportsAuxiliaryBoundary(), mangrove_tds::Simplex::add2AuxliaryBoundarySimplex(),
			 * mangrove_tds::Simplex::getAuxiliaryBoundarySimplex(), mangrove_tds::Simplex::removeAuxiliaryBoundarySimplex()
			 */
			void clearAuxiliaryBoundary();
			
			/// This member function returns constant pointers to all simplices belonging to the current simplex boundary.
			/**
			 * With this member function, we can obtain constant pointers to all simplices belonging to the current simplex boundary: the boundary of a k-simplex is composed by k+1 simplices
			 * of dimension j<k. We also remark that a 0-simplex has an empty boundary.<p><b>IMPORTANT:</b> we cannot update pointers to the boundary simplices and we can only read them
			 * by using this member function. You should refer the member functions offered by the mangrove_tds::Simplex class in order to update the boundary relation of the current simplex.
			 * \return constant pointers to all simplices belonging to the current simplex boundary
			 * \see mangrove_tds::Simplex::setType(), mangrove_tds::Simplex::type(), mangrove_tds::Simplex::getBoundarySize(), mangrove_tds::Simplex::clearBoundary(),
			 * mangrove_tds::Simplex::bc(), mangrove_tds::Simplex::add2Boundary(), mangrove_tds::Simplex::removeFromBoundary(), mangrove_tds::Simplex::positionInBoundary(),
			 * mangrove_tds::SimplexPointer
			 */
			inline const vector<SimplexPointer>& b() const { return this->boundary; }
			
			/// This member function returns constant pointers to all simplices belonging to the current simplex coboundary.
			/**
			 * With this member function, we can obtain constant pointers to all simplices belonging to the current simplex coboundary: the coboundary of a k-simplex is composed by some
			 * simplices of dimension j>k, incident in the current one.<p><b>IMPORTANT:</b> we cannot update the pointers to the coboundary simplices and we can only read them by using
			 * this member function. You should refer the member functions offered by the mangrove_tds::Simplex class in order to update the coboundary relation of the current simplex.
			 * \return constant pointers to all simplices belonging to the current simplex coboundary
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex::getCoboundarySize(), mangrove_tds::Simplex::clearCoboundary(), mangrove_tds::Simplex::cc(), 
			 * mangrove_tds::Simplex::add2Coboundary(), mangrove_tds::Simplex::removeFromCoboundary(), mangrove_tds::Simplex::positionInCoboundary()
			 */
			inline const vector<SimplexPointer>& c() const { return this->coboundary; }
			
			/// This member function returns constant pointers to all simplices belonging to the current simplex adjacency.
			/**
			 * With this member function, we can obtain constant pointers to all simplices belonging to the current simplex adjacency: it is composed by some simplices for describing all
			 * simplices sharing a (k-1)-face with the current simplex. It can be encoded in an efficient way: if the shared (k-1)-face is manifold, then the adjacent simplex is directly
			 * encoded, otherwise we encode a pointer to a (k-1)-face and thus we can access its coboundary. In this way, we obtain a unique list of adjacent simplices, independently from
			 * their number.<p><b>IMPORTANT:</b> we cannot update the pointers to the adjacency simplices and we can only read them by using this member function. You should refer the member
			 * functions offered by the mangrove_tds::Simplex class in order to update the adjacency relation of the current simplex.
			 * \return constant pointers to all simplices describing the current simplex adjacency
			 * \see mangrove_tds::Simplex::ac(), mangrove_tds::Simplex::add2Adjacency(), mangrove_tds::Simplex::removeFromAdjacency(), mangrove_tds::Simplex::clearAdjacency(),
			 * mangrove_tds::Simplex::hasAdjacency(), mangrove_tds::Simplex::isManifoldAdjacency(), mangrove_tds::Simplex::positionInAdjacency()
			 */
			inline const vector< vector<SimplexPointer> >& a() const { return this->adjacency; }
			
			/// This member function returns a reference to a simplex belonging to the current simplex boundary.
			/**
			 * With this member function, we can obtain a reference to a simplex belonging to the current simplex boundary: a k-simplex has a boundary composed by k+1 simplices of dimension
			 * j<k, except for a 0-simplex since a 0-simplex has an empty boundary.<p>The required pointer is identified by its position in the mangrove_tds::Simplex::boundary vector: if the
			 * required position is not valid, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.
			 * \param elem the position of the required simplex in the mangrove_tds::Simplex::boundary array
			 * \return a reference to a simplex belonging to the current simplex boundary
			 * \see mangrove_tds::Simplex::setType(), mangrove_tds::Simplex::type(), mangrove_tds::Simplex::getBoundarySize(), mangrove_tds::Simplex::clearBoundary(),
			 * mangrove_tds::Simplex::bc(), mangrove_tds::Simplex::add2Boundary(), mangrove_tds::Simplex::removeFromBoundary(), mangrove_tds::Simplex::positionInBoundary(),
			 * mangrove_tds::SimplexPointer
			 */
			SimplexPointer& b(unsigned int elem);
			
			/// This member function returns a reference to a simplex belonging to the current simplex coboundary.
			/**
			 * With this member function, we can obtain a reference to a simplex belonging to the current simplex coboundary: a k-simplex has a coboundary composed by some simplices of
			 * dimension j>k, incident in the current one.<p>The required reference is identified by its position in the mangrove_tds::Simplex::coboundary vector: if the required position is
			 * not valid, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in
			 * a failed assert.
			 * \param elem the position of the required reference in the mangrove_tds::Simplex::coboundary vector
			 * \return a reference to a simplex belonging to the current simplex coboundary
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex::getCoboundarySize(), mangrove_tds::Simplex::clearCoboundary(), mangrove_tds::Simplex::cc(),
			 * mangrove_tds::Simplex::add2Coboundary(), mangrove_tds::Simplex::removeFromCoboundary(), mangrove_tds::Simplex::positionInCoboundary()
			 */
			SimplexPointer& c(unsigned int elem);
			
			/// This member function returns a reference to a simplex belonging to the current simplex adjacency.
			/**
			 * With this member function, we can obtain a reference to a simplex belonging to the current simplex adjacency: it is composed by some simplices for describing all simplices
			 * sharing a (k-1)-face with the current simplex. It can be encoded in an efficient way: if the shared (k-1)-face is manifold (i.e. it is shared by at most two k-simplices), then
			 * the adjacent simplex is directly encoded, otherwise we encode a pointer to a (k-1)-face and thus we can access its coboundary. In this way, we obtain a unique list of adjacent
			 * simplices, independently from their number.<p>The required reference is identified by its position in the mangrove_tds::Simplex::adjacency vector: if we cannot complete this
			 * operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation
			 * will result in a failed assert.
			 * \param elem the position of the required reference in the mangrove_tds::Simplex::adjacency vector
			 * \return a reference to a simplex belonging to the current simplex adjacency
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex::ac(), mangrove_tds::Simplex::add2Adjacency(), mangrove_tds::Simplex::removeFromAdjacency(),
			 * mangrove_tds::Simplex::clearAdjacency(), mangrove_tds::Simplex::hasAdjacency(), mangrove_tds::Simplex::isManifoldAdjacency(), mangrove_tds::Simplex::positionInAdjacency()
			 */
			SimplexPointer& a(unsigned int elem);
			
			/// This member function retrieves a simplex belonging to the auxiliary boundary of the current simplex.
			/**
			 * This member function retrieves a simplex belonging to the auxiliary boundary of the current simplex. It is formed by auxiliary simplices, different from the canonical
			 * boundary, that belong to the boundary of the current simplex. For instance, if we store the vertices of a triangle as canonical boundary, then its auxiliary boundary is formed
			 * by at least one of its edges. This feature is very useful in those data structures, like the <i>Non-manifold IA</i> (mangrove_tds::NMIA class), where we store non-manifold
			 * edges belonging to the boundary of a top simplex.<p><b>IMPORTANT:</b> if this operation cannot be completed, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param ct the type of the simplex to be checked
			 * \param ci the identifier of the simplex as child of the current simplex (similar to an instance of the mangrove_tds::GhostSimplexPointer class)
			 * \param cp the output pointer to the new simplex to be retrieved from the auxiliary boundary in the current simplex
			 * \see mangrove_tds::NMIA, mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_ID, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer,
			 * mangrove_tds::Simplex::supportsAuxiliaryBoundary(), mangrove_tds::Simplex::clearAuxiliaryBoundary(), mangrove_tds::Simplex::add2AuxiliaryBoundary(),
			 * mangrove_tds::Simplex::removeAuxiliaryBoundarySimplex()
			 */
			void getAuxiliaryBoundarySimplex(SIMPLEX_TYPE ct,SIMPLEX_ID ci,SimplexPointer &cp);

			/// This membert function checks if a location of the auxliary boundary has been set up.
			/**
			 * This membert function checks if a location of the auxliary boundary has been set up. The auxiliary boundary is formed by auxiliary simplices, different from the canonical
			 * boundary, that belong to the boundary of the current simplex. For instance, if we store the vertices of a triangle as canonical boundary, then its auxiliary boundary is formed
			 * by at least one of its edges. This feature is very useful in those data structures, like the <i>Non-manifold IA</i> (mangrove_tds::NMIA class), where we store non-manifold
			 * edges belonging to the boundary of a top simplex.<p><b>IMPORTANT:</b> if this operation cannot be completed, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param ct the type of the simplex to be checked
			 * \param ci the identifier of the simplex as child of the current simplex (similar to an instance of the mangrove_tds::GhostSimplexPointer class)
			 * \return <ul><li><i>true</i>, if the required location has been set up</li><li><i>false</i>, otherwise</ul>
			 * \see mangrove_tds::NMIA, mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_ID, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer,
			 * mangrove_tds::Simplex::supportsAuxiliaryBoundary(), mangrove_tds::Simplex::clearAuxiliaryBoundary(), mangrove_tds::Simplex::add2AuxiliaryBoundary(),
			 * mangrove_tds::Simplex::removeAuxiliaryBoundarySimplex()
			 */
			bool isSetUp(SIMPLEX_TYPE ct,SIMPLEX_ID ci);

			/// This member function returns a constant reference to a simplex belonging to the current simplex boundary.
			/**
			 * With this member function, we can obtain a constant reference to a simplex belonging to the current simplex boundary: a k-simplex has a boundary composed by k+1 simplices of
			 * dimension j<k, except for a 0-simplex since a 0-simplex has an empty boundary.<p>The required constant reference is identified by its position in the
			 * mangrove_tds::Simplex::boundary vector: if the required position is not valid, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in
			 * debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> we cannot update the required reference and we can only read it by using 
			 * this member function. You should refer the member functions offered by the mangrove_tds::Simplex class in order to update the boundary relation of the current simplex.
			 * \param elem the position of the required constant reference in the mangrove_tds::Simplex::boundary vector
			 * \return a constant reference to a simplex belonging to the current simplex boundary
			 * \see mangrove_tds::Simplex::setType(), mangrove_tds::Simplex::type(), mangrove_tds::Simplex::getBoundarySize(), mangrove_tds::Simplex::clearBoundary(),
			 * mangrove_tds::Simplex::add2Boundary(), mangrove_tds::Simplex::removeFromBoundary(), mangrove_tds::Simplex::positionInBoundary(), mangrove_tds::SimplexPointer
			 */
			const SimplexPointer& bc(unsigned int elem) const;
			
			/// This member function returns a constant reference to a simplex belonging to the current simplex coboundary.
			/**
			 * With this member function, we can obtain a constant reference to a simplex belonging to the current simplex coboundary: a k-simplex has a coboundary composed by some simplices
			 * of dimension j>k, incident in the current one.<p>The required constant reference is identified by its position in the mangrove_tds::Simplex::coboundary vector: if the required
			 * position is not valid, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation
			 * will result in a failed assert.<p><b>IMPORTANT:</b> we cannot update the required reference and we can only read it by using this member function. You should refer the member
			 * functions offered by the mangrove_tds::Simplex class in order to update the coboundary relation of the current simplex.
			 * \param elem the position of the required constant reference in the mangrove_tds::Simplex::coboundary vector
			 * \return a constant reference to a simplex belonging to the current simplex coboundary
			 * \see mangrove_tds::Simplex::getCoboundarySize(), mangrove_tds::Simplex::clearCoboundary(), mangrove_tds::Simplex::add2Coboundary(),
			 * mangrove_tds::Simplex::removeFromCoboundary(), mangrove_tds::Simplex::positionInCoboundary(), mangrove_tds::SimplexPointer
			 */			
			const SimplexPointer& cc(unsigned int elem) const;
			
			/// This member function returns a constant reference to a simplex belonging to the current simplex adjacency.
			/**
			 * With this member function, we can obtain a constant reference to a simplex belonging to the current simplex adjacency: it is composed by some simplices for describing all
			 * simplices sharing a (k-1)-face with the current simplex. It can be encoded in an efficient way: if the shared (k-1)-face is manifold, then the adjacent simplex is directly
			 * encoded, otherwise we encode a pointer to a (k-1)-face and thus we can access its coboundary. In this way, we obtain a unique list of adjacent simplices, independently from
			 * their number.<p>The required constant reference is identified by its position in the mangrove_tds::Simplex::adjacency vector: if we cannot complete this operation for any
			 * reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a
			 * failed assert.
			 * \param elem the position of the required constant reference in the mangrove_tds::Simplex::adjacency vector
			 * \return a constant reference to a simplex belonging to the current simplex adjacency
			 * \see mangrove_tds::Simplex::add2Adjacency(), mangrove_tds::Simplex::removeFromAdjacency(), mangrove_tds::Simplex::clearAdjacency(),
			 * mangrove_tds::Simplex::hasAdjacency(), mangrove_tds::Simplex::isManifoldAdjacency(), mangrove_tds::Simplex::positionInAdjacency()
			 */
			const SimplexPointer& ac(unsigned int elem) const;
			
			/// This member function adds a pointer to a new simplex in the current simplex boundary.
			/**
			 * With this member function, we can add a pointer to a new simplex in the current simplex boundary relation: a k-simplex has a boundary composed by k+1 simplices of dimension 
			 * j<k, except for a 0-simplex since a 0-simplex has an empty boundary.<p>The simplex to be added is identified by its pointer and thus we can check if its dimension satisfies
			 * the boundary condition and if the required position exists. If it does not satisfy the boundary condition or if the required position is not valid, then this member function
			 * will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param cp a pointer to the new simplex to be added in the current simplex boundary
			 * \param elem the position of the new simplex in the mangrove_tds::Simplex::boundary vector
			 * \see mangrove_tds::Simplex::setType(), mangrove_tds::Simplex::type(), mangrove_tds::Simplex::getBoundarySize(), mangrove_tds::Simplex::clearBoundary(),
			 * mangrove_tds::Simplex::bc(), mangrove_tds::Simplex::removeFromBoundary(), mangrove_tds::Simplex::positionInBoundary(), mangrove_tds::SimplexPointer
			 */
			void add2Boundary(const SimplexPointer& cp,unsigned int elem);
			
			/// This member function adds a new simplex to the coboundary relation of the current simplex.
			/**
			 * With this member function, we can add a new simplex to the coboundary relation of the current simplex: a k-simplex has a coboundary composed by some simplices of dimension
			 * j>k, incident in the current one.<p>The simplex to be added is identified by its pointer and thus we can check if its dimension satisfies the coboundary condition. If it does
			 * not satisfy the coboundary condition, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.
			 * \param cp a pointer to the new simplex to be added to the current simplex coboundary
			 * \see mangrove_tds::Simplex::getCoboundarySize(), mangrove_tds::Simplex::clearCoboundary(), mangrove_tds::Simplex::cc(), mangrove_tds::Simplex::removeFromCoboundary(),
			 * mangrove_tds::Simplex::positionInCoboundary(), mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::Simplex::type()
			 */
			void add2Coboundary(const SimplexPointer& cp);
			
			/// This member function adds a new simplex to the adjacency relation of the current simplex.
			/**
			 * With this member function, we can add a pointer to a new simplex in the current simplex adjacency relation: it is composed by some simplices for describing all simplices
			 * sharing a (k-1)-face with the current simplex. It can be encoded in an efficient way: if the shared (k-1)-face is manifold, then the adjacent simplex is directly encoded,
			 * otherwise we encode a pointer to a (k-1)-face and thus we can access its coboundary.<p>The shared (k-1)-face is identified by their position in the
			 * mangrove_tds::Simplex::adjacency vector: moreover, the simplex to be added is identified by its pointer and thus we can check if its dimension satisfies the adjacency
			 * condition. If it does not satisfy the adjacency condition or if the required position is not valid, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param cp a pointer to the new simplex to be added to the current simplex adjacency
			 * \param elem the position of the (k-1)-face shared between the new simplex and the current one
			 * \see mangrove_tds::Simplex::ac(), mangrove_tds::Simplex::removeFromAdjacency(), mangrove_tds::Simplex::clearAdjacency(), mangrove_tds::Simplex::hasAdjacency(),
			 * mangrove_tds::Simplex::isManifoldAdjacency(), mangrove_tds::Simplex::positionInAdjacency()
			 */
			void add2Adjacency(const SimplexPointer& cp,unsigned int elem);

			/// This member function adds a new simplex to the auxiliary boundary of the current simplex.
			/**
			 * This member function adds a new simplex to the auxiliary boundary of the current simplex. It is formed by auxiliary simplices, different from the canonical boundary, that
			 * belong to the boundary of the current simplex. For instance, if we store the vertices of a triangle as canonical boundary, then its auxiliary boundary is formed by at least
			 * one of its edges. This feature is very useful in those data structures, like the <i>Non-manifold IA</i> (mangrove_tds::NMIA class), where we store non-manifold edge belonging
			 * to the boundary of a top simplex.<p><b>IMPORTANT:</b> if this operation cannot be completed, then this member function will fail if and only if the <i>Mangrove TDS Library</i>
			 * is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param cp a pointer to the new simplex to be added to the auxiliary boundary in the current simplex
			 * \param ci the identifier of the new simplex as child of the current simplex (similar to an instance of the mangrove_tds::GhostSimplexPointer class)
			 * \see mangrove_tds::NMIA, mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer, mangrove_tds::Simplex::supportsAuxiliaryBoundary(),
			 * mangrove_tds::Simplex::clearAuxiliaryBoundary(), mangrove_tds::Simplex::getAuxiliaryBoundarySimplex(), mangrove_tds::Simplex::removeAuxiliaryBoundarySimplex()
			 */
			void add2AuxiliaryBoundary(const SimplexPointer& cp,SIMPLEX_ID ci);

			/// This member function removes a simplex from the current simplex boundary.
			/**
			 * With this member function, we can remove a simplex from the current simplex boundary relation: a k-simplex has a boundary composed by k+1 simplices of dimension j<k, except
			 * for a 0-simplex since a 0-simplex has an empty boundary.<p>The simplex to be removed is identified by its pointer and thus we can check if its dimension satisfies boundary
			 * condition and if it belongs to the current simplex boundary. If it does not satisfy boundary condition or if it does not belong to the current boundary relation, then this
			 * member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.<p><b>IMPORTANT</b>: in this member function, we remove the input simplex from the current simplex boundary and then we destroy the current coboundary and adjacency
			 * relations. We also destroy the auxiliary boundary. You should rebuild them in accordance with the particular data structure you are using.
			 * \param cp a pointer to the simplex to be removed from the current simplex boundary
			 * \see mangrove_tds::Simplex::setType(), mangrove_tds::Simplex::type(), mangrove_tds::Simplex::getBoundarySize(), mangrove_tds::Simplex::clearBoundary(),
			 * mangrove_tds::Simplex::bc(), mangrove_tds::Simplex::add2Boundary(), mangrove_tds::Simplex::positionInBoundary(), mangrove_tds::SimplexPointer
			 */
			void removeFromBoundary(const SimplexPointer& cp);
			
			/// This member function removes a simplex from the current simplex coboundary.
			/**
			 * With this member function, we can remove a simplex from the current simplex coboundary relation: a k-simplex has a coboundary composed by some simplices of dimension j>k,
			 * incident in the current one.<p>The simplex to be removed is identified by its pointer, thus we can check if its dimension satisfies coboundary condition and if it belongs to
			 * the current simplex coboundary. If it does not satisfy the coboundary condition or it does not belong to the current coboundary relation, then this member function will fail
			 * if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param cp a pointer to the simplex to be removed from the current simplex coboundary
			 * \see mangrove_tds::Simplex::getCoboundarySize(), mangrove_tds::Simplex::clearCoboundary(), mangrove_tds::Simplex::cc(), mangrove_tds::Simplex::add2Coboundary(),
			 * mangrove_tds::Simplex::positionInCoboundary(), mangrove_tds::SimplexPointer
			 */
			void removeFromCoboundary(const SimplexPointer& cp);
			
			/// This member function removes a simplex from the current simplex adjacency.
			/**
			 * With this member function, we can remove a simplex from the current simplex adjacency relation: it is composed by some simplices for describing all simplices sharing a
			 * (k-1)-face with the current simplex. It can be encoded in an efficient way: if the shared (k-1)-face is manifold, then the adjacent simplex is directly encoded, otherwise we
			 * encode a pointer to a (k-1)-face and thus we can access its coboundary.<p>The simplex to be removed is identified by its pointer, thus we can check if its dimension satisfies
			 * the adjacency condition and if it belongs to the current simplex adjacency. If it does not satisfy the adjacency condition or it does not belong to the current adjacency
			 * relation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a
			 * failed assert.
			 * \param cp a pointer to the simplex to be removed from the current simplex adjacency
			 * \see mangrove_tds::Simplex::ac(), mangrove_tds::Simplex::add2Adjacency(), mangrove_tds::Simplex::clearAdjacency(), mangrove_tds::Simplex::hasAdjacency(),
			 * mangrove_tds::Simplex::isManifoldAdjacency(), mangrove_tds::Simplex::positionInAdjacency()
			 */
			void removeFromAdjacency(const SimplexPointer& cp);
			
			/// This member function removes a simplex from the auxiliary boundary.
			/**
			 * This member function removes a simplex from the auxiliary boundary. It is formed by auxiliary simplices, different from the canonical boundary, that belong to the boundary of 
			 * the current simplex. For instance, if we store the vertices of a triangle as canonical boundary, then its auxiliary boundary is formed by at least one of its edges. This
			 * feature is very useful in those data structures, like the <i>Non-manifold IA</i> (mangrove_tds::NMIA class), where we store non-manifold edges belonging to the boundary of a
			 * top simplex.<p><b>IMPORTANT:</b> if this operation cannot be completed, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug
			 * mode. In this case, each forbidden operation will result in a failed assert.
			 * \param ct the dimension of the simplex to be removed from the auxiliary boundary in the current simplex
			 * \param ci the identifier of the simplex to be removed as child of the current simplex (similar to an instance of the mangrove_tds::GhostSimplexPointer class)
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointer, mangrove_tds::NMIA, mangrove_tds::GhostSimplexPointer,
			 * mangrove_tds::Simplex::supportsAuxiliaryBoundary(), mangrove_tds::Simplex::clearAuxiliaryBoundary(), mangrove_tds::Simplex::getAuxiliaryBoundarySimplex(),
			 * mangrove_tds::Simplex::add2AuxiliaryBoundary()
			 */
			void removeAuxiliaryBoundarySimplex(SIMPLEX_TYPE ct, SIMPLEX_ID ci);
			
			/// This member function checks if the current simplex has a simplex adjacent along a particular subface.
			/**
			 * This member function checks if the current k-simplex has a k-simplex adjacent along a particular subface: it is composed by some simplices for describing all simplices sharing
			 * a (k-1)-face with the current simplex. It can be encoded in an efficient way: if the shared (k-1)-face is manifold, then the adjacent simplex is directly encoded, otherwise we
			 * encode a pointer to a (k-1)-face and thus we can access its coboundary.<p>If we cannot complete this operation for any reason, then this member function will fail if and only
			 * if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.		 
			 * \param elem the position of the (k-1)-face to be shared in the mangrove_tds::Simplex::adjacency vector
			 * \return <ul><li><i>true</i>, if the current k-simplex has a k-simplex adjacent along the required (k-1)-face</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::Simplex::ac(), mangrove_tds::Simplex::add2Adjacency(), mangrove_tds::Simplex::removeFromAdjacency(), mangrove_tds::Simplex::clearAdjacency(),
			 * mangrove_tds::Simplex::isManifoldAdjacency(), mangrove_tds::Simplex::positionInAdjacency()
			 */
			bool hasAdjacency(unsigned int elem) const;

			/// This member function checks if the current simplex has a manifold adjacency along a particular subface.
			/**
			 * This member function checks if the current k-simplex has a manifold adjacency along a particular subface: it is composed by some simplices for describing all simplices sharing
			 * a (k-1)-face with the current simplex. It can be encoded in an efficient way: if the shared (k-1)-face is manifold, then the adjacent simplex is directly encoded, otherwise we
			 * encode a pointer to a (k-1)-face and thus we can access its coboundary.<p>If we cannot complete this operation for any reason, then this member function will fail if and only
			 * if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.	
			 * \param elem the position of the (k-1)-face to be shared in the mangrove_tds::Simplex::adjacency vector
			 * \return <ul><li><i>true</i>, if the current k-simplex has a manifold adjacency along a particular subface</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::Simplex::ac(), mangrove_tds::Simplex::add2Adjacency(), mangrove_tds::Simplex::removeFromAdjacency(), mangrove_tds::Simplex::clearAdjacency(),
			 * mangrove_tds::Simplex::hasAdjacency(), mangrove_tds::Simplex::positionInAdjacency()
			 */
			bool isManifoldAdjacency(unsigned int elem) const;
			
			/// This member function checks if a simplex belongs to the current simplex boundary.
			/**
			 * This member function checks if a simplex belongs to the current simplex boundary: a k-simplex has a boundary composed by k+1 simplices of dimension j<k, except for a 0-simplex
			 * since a 0-simplex has an empty boundary.
			 * \param cp a pointer to the required simplex
			 * \param pos the position of the required simplex in the boundary relation (it is valid only if the input simplex belongs to the boundary)
			 * \return <ul><li><i>true</i>, if the input simplex is stored in the current simplex boundary</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex::getBoundarySize(), mangrove_tds::Simplex::clearBoundary(), mangrove_tds::Simplex::bc(),
			 * mangrove_tds::Simplex::add2Boundary(), mangrove_tds::Simplex::removeFromBoundary()
			 */
			bool positionInBoundary(const SimplexPointer& cp,unsigned int &pos);
			
			/// This member function checks if a simplex belongs to the current simplex coboundary.
			/**
			 * This member function checks if a simplex belongs to the current simplex coboundary: a k-simplex has a coboundary composed by some simplices of dimension j>k, incident in the
			 * current one.
			 * \param cp a pointer to the required simplex
			 * \param pos the position of the required simplex in the coboundary relation (it is valid only if the input simplex belongs to the coboundary)
			 * \return <ul><li><i>true</i>, if the input simplex is stored in the current simplex coboundary</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex::getCoboundarySize(), mangrove_tds::Simplex::clearCoboundary(), mangrove_tds::Simplex::cc(),
			 * mangrove_tds::Simplex::add2Coboundary(), mangrove_tds::Simplex::removeFromCoboundary()
			 */
			bool positionInCoboundary(const SimplexPointer& cp,unsigned int &pos);
			
			/// This member function checks if a simplex belongs to the current simplex adjacency.
			/**
			 * This member function checks if a simplex belongs to the current simplex adjacency: it is composed by some simplices for describing all simplices sharing a (k-1)-face with the
			 * current simplex. It can be encoded in an efficient way: if the shared (k-1)-face is manifold, then the adjacent simplex is directly encoded, otherwise we encode a pointer to a
			 * (k-1)-face and thus we can access its coboundary.
			 * \param cp a pointer to the required simplex
			 * \param pos the index of the (k-1)-face shared between the input simplex and the current one (it is valid only if the input simplex belongs to the adjacency)
			 * \return <ul><li><i>true</i>, if the input simplex is stored in the current simplex adjacency</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex::ac(), mangrove_tds::Simplex::add2Adjacency(), mangrove_tds::Simplex::removeFromAdjacency(),
			 * mangrove_tds::Simplex::clearAdjacency(), mangrove_tds::Simplex::hasAdjacency(), mangrove_tds::Simplex::isManifoldAdjacency()
			 */
			bool positionInAdjacency(const SimplexPointer& cp,unsigned int &pos);
			
			/// This member function writes a debug representation of the current simplex on a output stream.
			/**
			 * In the debug representation, we write information about the topological relations of the current simplex. Moreover, we provide information about the garbage and visiting
			 * status.<p><b>IMPORTANT:</b> this member function is intended only for debugging purposes. Otherwise, you should apply the writing operator in order to obtain a compact
			 * representation of a simplex, suitable for transmitting or receiving objects.
			 * \param os the output stream where we can write a debug representation of the current simplex
			 * \see mangrove_tds::SimplicesContainer::debug(), mangrove_tds::Simplex::getBoundarySize(), mangrove_tds::Simplex::getCoboundarySize(),
			 * mangrove_tds::Simplex::hasAdjacency(), mangrove_tds::Simplex::bc(), mangrove_tds::Simplex::cc(), mangrove_tds::Simplex::ac()
			 */
			void debug(ostream& os = cout) const;
			
			/// This operator writes a compact representation of a simplex on a output stream.
			/**
			 * In this operator, we write a compact representation of a k-simplex on a output stream, formatted in accordance with these guidelines:<p><i>[ deleted status ]<br>
			 * [ number of boundary simplices ] [ pointers to the boundary simplices ]<br>[ number of coboundary simplices ] [ pointers to the coboundary simplices ]<br>for each (k-1)-face f:
			 *  [ number of adjacent simplices along f ] [ pointers to the adjacent simplices along f ]</i><br>for each dimension d:<i>[ number of d-simplices in the auxiliary boundary ]
			 * [ local identifiers of such simplices] [ global identifiers of such simplices ]</i><p>In other words, this representation is a snapshot of the current state of a k-simplex and
			 * it is suitable for transmitting or receiving objects. Moreover, it can be used by the reading operator in order to recreate a new instance of this class: otherwise, you should
			 * apply the mangrove_tds::Simplex::debug() member function for debugging purposes.
			 * \param os the output stream where we can write the compact representation of a simplex
			 * \param c the simplex to be written
			 * \return the output stream after having written the input simplex description
			 * \see mangrove_tds::Simplex::debug(), mangrove_tds::Simplex::bc(), mangrove_tds::Simplex::cc(), mangrove_tds::Simplex::ac(), mangrove_tds::Simplex::getBoundarySize(),
			 * mangrove_tds::Simplex::getCoboundarySize(), mangrove_tds::Simplex::hasAdjacency()
			 */
			friend ostream& operator<<(ostream& os,const Simplex& c)
			{
				map<SIMPLEX_ID,SIMPLEX_ID>::const_iterator it;
				
				/* Repetita iuvant - representation of a simplex:
		 		 *
		 	 	 * [ deleted status ]
		 		 * [ number of boundary simplices ] [ pointers to the boundary simplices ]
		 		 * [ number of coboundary simplices ] [ pointers to the coboundary simplices ]
		 		 * for each (k-1)-face [ number of adjacent simplices ] [ pointers to the adjacent simplices ]
		 		 * for each dimension d<k: [ number of d-simplices in the auxiliary boundary ] [ local identifiers of such simplices] [ global identifiers of such simplices ] */
		 		os<<c.deleted<<endl;
				os<<c.getBoundarySize()<<endl;
				for(unsigned int k=0;k<c.getBoundarySize();k++) os<<c.bc(k);
				os<<c.getCoboundarySize()<<endl;
				for(unsigned int k=0;k<c.getCoboundarySize();k++) os<<c.cc(k);
				for(unsigned int k=0;k<c.adjacency.size();k++)
				{
					if(c.hasAdjacency(k)==false) os<<"0"<<endl;
					else os<<"1 "<<c.ac(k);
				}
				
				/* Now, we write auxiliary boundary! */
				for(unsigned int k=0;k<c.aux_boundary.size();k++)
				{
					os<<c.aux_boundary[k].size();
					for(it=c.aux_boundary[k].begin();it!=c.aux_boundary[k].end();it++) os<<" "<<it->first<<" "<<it->second;
					os<<endl;
					os.flush();
				}
				
				/* If we arrive here, we finished! */
				return os;
			}
			
			/// This operator rebuilds an instance of this class, by reading a compact representation of a simplex from an input stream.
			/**
			 * In this operator, we rebuild an instance of this class, by reading a compact representation of a simplex from an input stream. Such representation is formatted in accordance
			 * with these guidelines:<p><i>[ deleted status ]<br>[ number of boundary simplices ] [ pointers to the boundary simplices ]<br>[ number of coboundary simplices ]
			 * [ pointers to the coboundary simplices ]<br>for each (k-1)-face f: [ number of adjacent simplices along f ] [ pointers to the adjacent simplices along f ]</i><br>for each
			 * dimension d: <i>[ number of d-simplices in the auxiliary boundary ] [ local identifiers of such simplices] [ global identifiers of such simplices ]</i><p>In other words, this
			 * representation is a snapshot of the current state of a simplex and it is suitable for transmitting or receiving objects. Moreover, it can be provided by using the writing
			 * operator: otherwise, you should apply the mangrove_tds::Simplex::debug() member function for debugging purposes.
			 * \param in the input stream from which we can read the representation of a simplex
			 * \param c the simplex to be rebuilt
			 * \return the input stream after having read the input simplex description
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex::setDeleted(), mangrove_tds::Simplex::setType(), mangrove_tds::Simplex::add2Boundary(),
			 * mangrove_tds::Simplex::add2Coboundary(), mangrove_tds::Simplex::add2Adjacency(), mangrove_tds::Simplex::add2AuxiliaryBoundary()
			 */
			friend istream& operator>>(istream& in,Simplex& c)
			{
				bool aux;
				unsigned int lg,g,l;
				SimplexPointer cp;

				/* Repetita iuvant - representation of a simplex:
		 		 *
		 	 	 * [ deleted status ]
		 		 * [ number of boundary simplices ] [ pointers to the boundary simplices ]
		 		 * [ number of coboundary simplices ] [ pointers to the coboundary simplices ]
		 		 * for each (k-1)-face [ number of adjacent simplices ] [ pointers to the adjacent simplices ]
		 		 * for each dimension d<k: [ number of d-simplices in the auxiliary boundary ] [ local identifiers of such simplices] [ global identifiers of such simplices ] */
		 		in>>aux;
		 		if(aux)
				{
					/* We are reading a deleted simplex, thus its topological relations are empty */
					c.setDeleted(true);
					c.setType(0);
					in>>lg;
					in>>lg;
				}
				else
				{
					/* If we arrive here, we are reading the boundary! */
					c.setDeleted(false);
					in>>lg;
					if(lg!=0) c.setType(lg-1);
					else c.setType(0);
					for(unsigned int k=0;k<lg;k++)
					{
						in>>cp;
						c.add2Boundary(SimplexPointer(cp),k);
					}
				
					/* If we arrive here, we can read informations about the coboundary */
					in>>lg;
					for(unsigned int k=0;k<lg;k++)
					{
						in>>cp;
						c.add2Coboundary(SimplexPointer(cp));
					}
					
					/* If we arrive here, we can read information about the adjacency! */
					for(unsigned int k=0;k<=c.type();k++)
					{
						in>>lg;
						if(lg!=0)
						{
							in>>cp;
							c.add2Adjacency(SimplexPointer(cp),k);
						}
					}
					
					/* If we arrive here, we can information about auxiliary boundary! */
					for(unsigned int k=0;k<c.type();k++)
					{
						in>>lg;
						for(unsigned int j=0;j<lg;j++)
						{
							in>>l;
							in>>g;
							c.aux_boundary[k][l]=g;
						}
					}	
				}
				
				/* If we arrive here, we can finalize this member function! */
				c.setVisited(false);
				return in;
			}
			
			protected:
			
			/// This boolean flag is internally used for garbage collector.
			/**
			 * \see mangrove_tds::SimplicesContainer
			 */
			bool deleted;
			
			/// This boolean flag is used by the internal algorithms in order to extract topological relations.
			/**
			 * \see mangrove_tds::BaseSimplicialComplex
			 */
			bool visited;
			
			/// The pointers for the simplices in the current simplex boundary relation.
			/**
			 * \see mangrove_tds::SimplexPointer
			 */
			vector<SimplexPointer> boundary;
			
			/// The pointers for the simplices in the current simplex coboundary relation.
			/**
			 * \see mangrove_tds::SimplexPointer
			 */
			vector<SimplexPointer> coboundary;
			
			/// The pointers for the simplices in the current simplex adjacency relation.
			/**
			 * \see mangrove_tds::SimplexPointer
			 */
			vector< vector<SimplexPointer> > adjacency;
			
			/// The pointers for the simplices in the auxiliary boundary of the current simplex, organized by dimension.
			/**
			 * Simplices on the auxiliary boundary of the current simplex are organized by dimension. The key value of each map is the identifier of the simplices, seen as child of the
			 * current simplex (similar to mangrove_tds::GhostSimplexPointer), while data are the identifiers of the simplices in the input data structure.
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_ID, mangrove_tds::SIMPLEX_TYPE
			 */
			vector< map<SIMPLEX_ID,SIMPLEX_ID> > aux_boundary;
		};

		/// A pointer to a simplex not directly encoded in a data structure.
		/**
		 * This class describes a pointer to a simplex not directly encoded in a data structure, described through an instance of the mangrove_tds::BaseSimplicialComplex class. If the data
		 * structure encodes all simplices, then we call it as <i>global</i> data structure, otherwise it is called <i>local</i> data structure. Examples of global data structures are the
		 * <i>Incidence Graph</i> (described by the mangrove_tds::IG class), the <i>Incidence Simplicial</i> (described by the mangrove_tds::IS class) data structure and the 
		 * <i>Simplified Incidence Graph</i> (described by the mangrove_tds::SIG class). The <i>IA*</i> (described by the mangrove_tds::GIA class) is a local data structure, because it
		 * encodes only top simplices.<p>A pointer to a simplex not directly encoded in data structure (namely a <i>ghost simplex</i>) is composed by a tuple <i>(tp,ti,cp,ci)</i>, describing a
		 * ghost simplex as the <i>ci</i>-th face of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension <i>tp</i>.<p><b>IMPORTANT:</b> we can also use an instance of
		 * this class for describing a directly encoded simplex, providing a common interface for accessing simplices.	 
		 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::IG, mangrove_tds::IS, mangrove_tds::SIG, mangrove_tds::GIA, mangrove_tds::PropertyBase,
		 * mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle,
		 * mangrove_tds::SimplexPointer
		 */
		class GhostSimplexPointer
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a not initialized pointer to a ghost simplex: you should initialize it before use this new instance for accessing ghost simplices.
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::setParentType(),
			 * mangrove_tds::GhostSimplexPointer::setParentId(), mangrove_tds::GhostSimplexPointer::setChildType(), mangrove_tds::GhostSimplexPointer::setChildId()
			 */
			GhostSimplexPointer();
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a new instance of this class, describing a simplex directly encoded in the current data structure: it is usually a top simplex, like in the
			 * mangrove_tds::GIA data structure. In this case, we should specify only the parent top simplex: you can check this situation through the
			 * mangrove_tds::GhostSimplexPointer::isEncoded() member function.
			 * \param pt the type of the top simplex, parent of this ghost simplex
			 * \param pi the identifier of the top simplex, parent of this ghost simplex
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::setParentType(),
			 * mangrove_tds::GhostSimplexPointer::setParentId(), mangrove_tds::GhostSimplexPointer::setChildType(), mangrove_tds::GhostSimplexPointer::setChildId(),
			 * mangrove_tds::GhostSimplexPointer::isEncoded(), mangrove_tds::GIA
			 */
			GhostSimplexPointer(SIMPLEX_TYPE pt,SIMPLEX_ID pi);
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a new instance of this class: we identify the current ghost simplex as a a tuple <i>(tp,ti,cp,ci)</i>, describing a ghost simplex as the
			 * <i>ci</i>-th face of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension <i>tp</i>.
			 * \param pt the type of the top simplex, parent of this ghost simplex
			 * \param pi the identifier of the top simplex, parent of this ghost simplex
			 * \param ct the type of the current ghost simplex
			 * \param ci the identifier of the current ghost simplex
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::setParentType(),
			 * mangrove_tds::GhostSimplexPointer::setParentId(), mangrove_tds::GhostSimplexPointer::setChildType(), mangrove_tds::GhostSimplexPointer::setChildId(),
			 * mangrove_tds::GhostSimplexPointer::isEncoded()
			 */
			GhostSimplexPointer(SIMPLEX_TYPE pt,SIMPLEX_ID pi,SIMPLEX_TYPE ct,SIMPLEX_ID ci);

			/// This member function creates a new instance of this class.
			/**
			 * In this member function, we create a copy of the input pointer to a ghost simplex, by replicating its internal state.
			 * \param g the pointer to a ghost simplex to be copied
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::setParentType(),
			 * mangrove_tds::GhostSimplexPointer::setParentId(), mangrove_tds::GhostSimplexPointer::setChildType(), mangrove_tds::GhostSimplexPointer::setChildId(),
			 * mangrove_tds::GhostSimplexPointer::isEncoded()
			 */
			GhostSimplexPointer(const GhostSimplexPointer &g);
			
			/// This member function destroys an instance of this class.
			inline virtual ~GhostSimplexPointer() { ; }
			
			/// This member function returns the type of the top simplex, parent of this ghost simplex.
			/**
			 * This member function returns the type of the top simplex, parent of this ghost simplex: we identify the current ghost simplex as a a tuple <i>(tp,ti,cp,ci)</i>, describing a
			 * ghost simplex as the <i>ci</i>-th face of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension<i>tp</i>. Consequently, this member function returns <i>tp</i>.
			 * \return the type of the top simplex, parent of this ghost simplex
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::setParentType(), mangrove_tds::GhostSimplexPointer::getParentId(),
			 * mangrove_tds::GhostSimplexPointer::getChildType(), mangrove_tds::GhostSimplexPointer::getChildId(), mangrove_tds::GhostSimplexPointer::isEncoded()
			 */
			inline SIMPLEX_TYPE getParentType() const { return this->pt; }

			/// This member function updates the type of the top simplex, parent of this ghost simplex.
			/**
			 * This member function updates the type of the top simplex, parent of this ghost simplex: we identify the current ghost simplex as a a tuple <i>(tp,ti,cp,ci)</i>, describing a
			 * ghost simplex as the <i>ci</i>-th face of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension<i>tp</i>. Consequently, this member function updates <i>tp</i>.
			 * \param pt the new type of the top simplex, parent of this ghost simplex
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::getParentType(),
			 * mangrove_tds::GhostSimplexPointer::setParentId(), mangrove_tds::GhostSimplexPointer::setChildType(), mangrove_tds::GhostSimplexPointer::setChildId(),
			 * mangrove_tds::GhostSimplexPointer::isEncoded()
			 */
			inline void setParentType(SIMPLEX_TYPE pt) { this->pt=pt; }
			
			/// This member function returns the identifier of the top simplex, parent of this ghost simplex.
			/**
			 * This member function returns the identifier of the top simplex, parent of this ghost simplex: we identify the current ghost simplex as a a tuple <i>(tp,ti,cp,ci)</i>,
			 * describing a ghost simplex as the <i>ci</i>-th face of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension <i>tp</i>. Consequently, this member function returns
			 * <i>ti</i>.
			 * \return the identifier of the top simplex, parent of this ghost simplex
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::setParentId(), mangrove_tds::GhostSimplexPointer::getParentType(),
			 * mangrove_tds::GhostSimplexPointer::getChildType(), mangrove_tds::GhostSimplexPointer::getChildId(), mangrove_tds::GhostSimplexPointer::isEncoded()
			 */
			inline SIMPLEX_ID getParentId() const { return this->pi; }

			/// This member function updates the identifier of the top simplex, parent of this ghost simplex.
			/**
			 * This member function updates the identifier of the top simplex, parent of this ghost simplex: we identify the current ghost simplex as a a tuple <i>(tp,ti,cp,ci)</i>,
			 * describing a ghost simplex as the <i>ci</i>-th face of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension <i>tp</i>. Consequently, this member function updates
			 * <i>ti</i>.
			 * \param pi the new identifier of the top simplex, parent of this ghost simplex
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::getParentId(), mangrove_tds::GhostSimplexPointer::setParentType(),
			 * mangrove_tds::GhostSimplexPointer::setChildType(), mangrove_tds::GhostSimplexPointer::setChildId(), mangrove_tds::GhostSimplexPointer::isEncoded()
			 */
			inline void setParentId(SIMPLEX_ID pi) { this->pi=pi; }

			/// This member function returns the type of the current ghost simplex.
			/**
			 * This member function returns the type of the current ghost simplex: we identify the current ghost simplex as a a tuple <i>(tp,ti,cp,ci)</i>, describing a ghost simplex as the
			 * <i>ci</i>-th face of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension <i>tp</i>. Consequently, this member function returns <i>ct</i>.
			 * \return the type of the current ghost simplex
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::getParentType(), mangrove_tds::GhostSimplexPointer::getParentId(),
			 * mangrove_tds::GhostSimplexPointer::getChildId(), mangrove_tds::GhostSimplexPointer::isEncoded()
			 */
			inline SIMPLEX_TYPE getChildType() const { return this->ct; }
			
			/// This member function returns the type of the current ghost simplex.
			/**
			 * This member function returns the type of the current ghost simplex: we identify the current ghost simplex as a a tuple <i>(tp,ti,cp,ci)</i>, describing a ghost simplex as the
			 * <i>ci</i>-th face of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension <i>tp</i>. Consequently, this member function returns <i>ct</i>.<p>
			 * <b>IMPORTANT:</b> basically, it is an alias for the mangrove_tds::GhostSimplexPointer::getChildType(), and it is useful for creating a common interface for accessing simplices.
			 * \return the type of the current ghost simplex
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::getParentType(),
			 * mangrove_tds::GhostSimplexPointer::getParentId(), mangrove_tds::GhostSimplexPointer::getChildId(), mangrove_tds::GhostSimplexPointer::getChildType(),
			 * mangrove_tds::GhostSimplexPointer::isEncoded()
			 */
			inline SIMPLEX_TYPE type() const { return this->getChildType(); }
			
			/// This member function updates the type of the current ghost simplex.
			/**
			 * This member function updates the type of the current ghost simplex: we identify the current ghost simplex as a a tuple <i>(tp,ti,cp,ci)</i>, describing a ghost simplex as the
			 * <i>ci</i>-th face of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension <i>tp</i>. Consequently, this member function updates <i>ct</i>.
			 * \param ct the new type of the current ghost simplex
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::getChildType(), mangrove_tds::GhostSimplexPointer::setParentType(),
			 * mangrove_tds::GhostSimplexPointer::setParentId(), mangrove_tds::GhostSimplexPointer::setChildId(), mangrove_tds::GhostSimplexPointer::isEncoded() 
			 */
			inline void setChildType(SIMPLEX_TYPE ct) { this->ct=ct; }
			
			/// This member function returns the identifier of the current ghost simplex.
			/**
			 * This member function returns the identifier of the current ghost simplex: we identify the current ghost simplex as a a tuple <i>(tp,ti,cp,ci)</i>, describing a ghost simplex
			 * as the <i>ci</i>-th face of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension <i>tp</i>. Consequently, this member function returns <i>ci</i>.
			 * \return the identifier of the current ghost simplex
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::getParentType(), mangrove_tds::GhostSimplexPointer::getParentId(),
			 * mangrove_tds::GhostSimplexPointer::getChildType(), mangrove_tds::GhostSimplexPointer::isEncoded()
			 */
			inline SIMPLEX_ID getChildId() const { return this->ci; }

			/// This member function updates the identifier of the current ghost simplex.
			/**
			 * This member function updates the identifier of the current ghost simplex: we identify the current ghost simplex as a a tuple <i>(tp,ti,cp,ci)</i>, describing a ghost simplex
			 * as the <i>ci</i>-th face of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension <i>tp</i>. Consequently, this member function <i>ci</i>.
			 * \param ci the new identifier of the current ghost simplex
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::getChildId(), mangrove_tds::GhostSimplexPointer::setParentType(),
			 * mangrove_tds::GhostSimplexPointer::setParentId(), mangrove_tds::GhostSimplexPointer::setChildType(), mangrove_tds::GhostSimplexPointer::isEncoded()
			 */
			inline void setChildId(SIMPLEX_ID ci) { this->ci=ci; }

			/// This member function checks if the current pointer refers an encoded simplex.
			/**
			 * This member function checks if the current pointer refers a simplex directly encoded in the current data structure: it is usually a top simplex, like in the mangrove_tds::GIA
			 * data structure. In this case, we should specify only the parent top simplex.
			 * \return <ul><li><i>true</i>, if the current pointer refers a top simplex</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::getParentType(), mangrove_tds::GhostSimplexPointer::getParentId(),
			 * mangrove_tds::GhostSimplexPointer::getChildType(), mangrove_tds::GhostSimplexPointer::getChildId(), mangrove_tds::GIA
			 */
			bool isEncoded() const;

			/// This member function writes a debug representation of the current ghost simplex pointer on an output stream.
			/**
			 * In the debug representation, we write a debug representation of the ghost simplex referred by the current simplex pointer on an output stream.<p><b>IMPORTANT:</b> this
			 * member function is intended only for debugging purposes, otherwise you should apply the writing operator in order to obtain a compact representation of a ghost simplex pointer.
			 * \param os the output stream where we can write the debug representation of the current ghost simplex pointer
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::isEncoded(), mangrove_tds::GhostSimplexPointer::getParentType(),
			 * mangrove_tds::GhostSimplexPointer::getParentId(), mangrove_tds::GhostSimplexPointer::getChildType(), mangrove_tds::GhostSimplexPointer::getChildId()
			 */
			void debug(ostream& os = cout) const;
			
			/// This operator writes a compact representation of a ghost simplex pointer on an output stream.
			/**
			 * In this operator, we write a compact representation of a ghost simplex pointer on an output stream, suitable for transmitting and receiving objects. A ghost simplex can be
			 * described as a tuple <i>(tp,ti,cp,ci)</i>, i.e. as the <i>ci</i>-th face of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension <i>tp</i>. Consequently, a
			 * representation of a ghost simplex is formatted in accordance with these guidelines:<p><i>[ tp ti cp ci ]</i><p><b>IMPORTANT:</b> this operator is not intended for debugging
			 * purposes. Otherwise, you should apply the mangrove_tds::GhostSimplexPointer::debug() member function.
			 * \param os the output stream where we can write the compact representation of a ghost simplex pointer
			 * \param g the ghost simplex pointer to be written
			 * \return the output stream after having produced the input ghost simplex pointer description
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::isEncoded(), mangrove_tds::GhostSimplexPointer::debug(),
			 * mangrove_tds::GhostSimplexPointer::getParentType(), mangrove_tds::GhostSimplexPointer::getParentId(), mangrove_tds::GhostSimplexPointer::getChildType(),
			 * mangrove_tds::GhostSimplexPointer::getChildId()
			 */
			inline friend ostream& operator<<(ostream& os, const GhostSimplexPointer& g)
			{
				/* Repetita iuvant - simplex pointer representation: [ pt pi ct ci ] */
				os<<g.getParentType()<<" "<<g.getParentId()<<" "<<g.getChildType()<<" "<<g.getChildId()<<endl;
				os.flush();
				return os;
			}
			
			/// This operator rebuilds a ghost simplex pointer by reading its compact representation from an input stream.
			/**
			 * In this operator, we rebuild a new instance of a ghost simplex pointer by reading its compact representation from an input stream, suitable for transmitting and receiving
			 * objects. A ghost simplex can be described as a tuple <i>(tp,ti,cp,ci)</i>, i.e. as the <i>ci</i>-th face of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension
			 * <i>tp</i>. Consequently, a representation of a ghost simplex is formatted in accordance with these guidelines:<p><i>[ tp ti cp ci ]</i><p><b>IMPORTANT:</b> this operator is not
			 * intended for debugging purposes. Otherwise, you should apply the mangrove_tds::GhostSimplexPointer::debug() member function.
			 * \param in the input stream from which we can read the compact representation of a ghost simplex pointer
			 * \param g the ghost simplex pointer to be rebuilt
			 * \return the input stream after having rebuilt a ghost simplex pointer
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::isEncoded(), mangrove_tds::GhostSimplexPointer::debug()
			 */
			inline friend istream& operator>>(istream& in,GhostSimplexPointer& g)
			{
				SIMPLEX_TYPE t1,t2;
				SIMPLEX_ID i1,i2;

				/* Repetita iuvant - simplex pointer representation: [ pt pi ct ci ] */
				in>>t1;
				in>>i1;
				in>>t2;
				in>>i2;
				g.setParentType(t1);
				g.setParentId(i1);
				g.setChildType(t2);
				g.setChildId(i2);
				return in;
			}
			
			/// This member function checks if two ghost simplex pointers are equal.
			/**
			 * This member function checks if two ghost simplex pointers are equal: two ghost simplex pointers are equal if they refer the same child of the same top simplex, thus all
			 * indices must be the same.<p><b>IMPORTANT:</b> two ghost simplices can be shared by two or more top simplices and they can be accessed through different pointers, i.e.
			 * referring different children of different top simplices.
			 * \param g the ghost simplex pointer to compared with the current one
			 * \return <ul><li><i>true</i>, if the current ghost simplex pointer and the input one refer the same child of the same top simplex</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::isEncoded()
			 */
			bool theSame(const GhostSimplexPointer& g) const;
			
			/// This operator checks if two ghost simplex pointers are equal.
			/**
			 * This operator checks if two ghost simplex pointers are equal: two ghost simplex pointers are equal if they refer the same child of the same top simplex, thus all indices must
			 * be the same.<p><b>IMPORTANT:</b> two ghost simplices can be shared by two or more top simplices and they can be accessed through different pointers, i.e. referring different
			 * children of different top simplices.
			 * \param g the ghost simplex pointer to compared with the current one
			 * \return <ul><li><i>true</i>, if the current ghost simplex pointer and the input one refer the same child of the same top simplex</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::isEncoded()
			 */
			bool operator==(const GhostSimplexPointer& g);
			
			/// This operator compares two ghost simplex pointers.
			/**
			 * In this operator, we compare two ghost simplex pointers: the first pointer is the current one, while this operator receives the second one as input. For us, the first ghost
			 * simplex pointer is lesser than the second one by giving priority to the dimension of the referred top simplex, in accordance with the lexicographic order.<p><b>IMPORTANT:</b>
			 * two ghost simplices can be shared by two or more top simplices and they can be accessed through different pointers, i.e. referring different children of different top
			 * simplices.
			 * \param g the ghost simplex pointer to compared with the current one
			 * \return <ul><li><i>true</i>, if the current pointer is lesser than the other</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer::isEncoded()
			 */
			bool operator<(const GhostSimplexPointer& g);

			protected:
			
			/// The type of the top simplex, parent of this ghost simplex.
			/**
			 * \see mangrove_tds::SIMPLEX_TYPE
			 */
			SIMPLEX_TYPE pt;
			
			/// The identifier of the top simplex, parent of this ghost simplex.
			/**
			 * \see mangrove_tds::SIMPLEX_ID
			 */
			SIMPLEX_ID pi;
			
			/// The type of the current ghost simplex.
			/**
			 * \see mangrove_tds:::SIMPLEX_TYPE
			 */
			SIMPLEX_TYPE ct;

			/// The identifier of the current ghost simplex.
			/**
			 * \see mangrove_tds::SIMPLEX_ID
			 */
			SIMPLEX_ID ci;
		};
		
		/// A brief description of a ghost simplex.
		/**
		 * This class provides a brief description of a ghost simplex, i.e. a simplex not directly encoded in a data structure.<p>If a data structure encodes all simplices, then we call it as
		 * <i>global</i> data structure, otherwise it is called <i>local</i> data structure. Examples of global data structures are the <i>Incidence Graph</i> (described by the
		 * mangrove_tds::IG class), the <i>Incidence Simplicial</i> (described by the mangrove_tds::IS class) data structure and the <i>Simplified Incidence Graph</i> (described by the
		 * mangrove_tds::SIG class). The <i>IA*</i> (described by the mangrove_tds::GIA class) is a local data structure, because it encodes only top simplices.<p>A similar simplex can be
		 * accessed through an instance of the mangrove_tds::GhostSimplexPointer class: it is composed by a tuple <i>(tp,ti,cp,ci)</i>, describing a ghost simplex as the <i>ci</i>-th face of
		 * dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension <i>tp</i>. We can also use an instance of this class for describing a directly encoded simplex, providing a common
		 * interface for accessing simplices.
		 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::IG, mangrove_tds::IS, mangrove_tds::SIG, mangrove_tds::GIA
		 */
		class GhostSimplex
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a brief description of a ghost simplex, i.e. a simplex not directly encoded in a data structure.<p>If a data structure encodes all simplices, then
			 * we call it as <i>global</i> data structure, otherwise it is called <i>local</i> data structure. Examples of global data structures are the <i>Incidence Graph</i> (described by
			 * the mangrove_tds::IG class), the <i>Incidence Simplicial</i> (described by the mangrove_tds::IS class) data structure and the <i>Simplified Incidence Graph</i> (described by
			 * the mangrove_tds::SIG class). The <i>IA*</i> (described by the mangrove_tds::GIA class) is a local data structure, because it encodes only top simplices.<p>A similar simplex
			 * can be accessed through an instance of the mangrove_tds::GhostSimplexPointer class: it is composed by a tuple <i>(tp,ti,cp,ci)</i>, describing a ghost simplex as the
			 * <i>ci</i>-th face of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension <i>tp</i>. We can also use an instance of this class for describing a directly encoded
			 * simplex, providing a common interface for accessing simplices.<p><b>IMPORTANT:</b> the new description is not initialized, then you must provide its initialization before
			 * using it.
			 * \see mangrove_tds::GhostSimplex::getGhostSimplexPointer(), mangrove_tds::GhostSimplex::getBoundary(), mangrove_tds::GhostSimplexPointer, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::IG, mangrove_tds::IS, mangrove_tds::SIG, mangrove_tds::GIA 
			 */
			GhostSimplex();
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a copy of a brief description of a ghost simplex, i.e. a simplex not directly encoded in a data structure.<p>If a data structure encodes all
			 * simplices, then we call it as <i>global</i> data structure, otherwise it is called <i>local</i> data structure. Examples of global data structures are the
			 * <i>Incidence Graph</i> (described by the mangrove_tds::IG class), the <i>Incidence Simplicial</i> (described by the mangrove_tds::IS class) data structure and the
			 * <i>Simplified Incidence Graph</i> (described by the mangrove_tds::SIG class). The <i>IA*</i> (described by the mangrove_tds::GIA class) is a local data structure, because it
			 * encodes only top simplices.<p>A similar simplex can be accessed through an instance of the mangrove_tds::GhostSimplexPointer class: it is composed by a tuple
			 * <i>(tp,ti,cp,ci)</i>, describing a ghost simplex as the <i>ci</i>-th face of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension <i>tp</i>. We can also use an
			 * instance of this class for describing a directly encoded simplex, providing a common interface for accessing simplices.
		 	 * \param s the input ghost simplex to be copied
		 	 * \see mangrove_tds::GhostSimplex::getGhostSimplexPointer(), mangrove_tds::GhostSimplex::getBoundary(), mangrove_tds::GhostSimplexPointer, mangrove_tds::Simplex,
		 	 * mangrove_tds::SimplexPointer, mangrove_tds::IG, mangrove_tds::IS, mangrove_tds::SIG, mangrove_tds::GIA
			 */
			GhostSimplex(const GhostSimplex& s);
			
			/// This member function destroys an instance of this class.
			/**
			 * This member function destroys an instance of this class.
			 * \see mangrove_tds::GhostSimplex::clear()
			 */
			virtual ~GhostSimplex();
			
			/// This member function clears the internal state of the current ghost simplex.
			/**
			 * This member function clears the internal state of the current ghost simplex in order to be reused.<p><b>IMPORTANT:</b> you must reinitialize this ghost simplex through the
			 * mangrove_tds::GhostSimplex::getGhostSimplexPointer() and mangrove_tds::GhostSimplex::getBoundary() member functions.
			 * \see mangrove_tds::GhostSimplex::getGhostSimplexPointer(), mangrove_tds::GhostSimplex::getBoundary()
			 */
			void clear();
			
			/// This member function returns a reference to the simplex pointer to the current ghost simplex.
			/**
			 * This member function returns a reference to the simplex pointer to the current ghost simplex.<p>A similar simplex can be accessed through an instance of the
			 * mangrove_tds::GhostSimplexPointer class: it is composed by a tuple <i>(tp,ti,cp,ci)</i>, describing a ghost simplex as the <i>ci</i>-th face of dimension <i>ct</i> of the
			 * <i>ti</i>-th top simplex of dimension <i>tp</i>. We can also use an instance of this class for describing a directly encoded simplex, providing a common interface for accessing
			 * simplices.<p><b>IMPORTANT:</b> you can modify the referred pointer through this member function.
			 * \return a reference to the simplex pointer to the current ghost simplex
			 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex::getCGhostSimplexPointer(), mangrove_tds::SimplexPointer, mangrove_tds::Simplex
			 */
			GhostSimplexPointer& getGhostSimplexPointer();
			
			/// This member function returns a constant reference to the simplex pointer to the current ghost simplex.
			/**
			 * This member function returns a reference to the simplex pointer to the current ghost simplex.<p>A similar simplex can be accessed through an instance of the
			 * mangrove_tds::GhostSimplexPointer class: it is composed by a tuple <i>(tp,ti,cp,ci)</i>, describing a ghost simplex as the <i>ci</i>-th face of dimension <i>ct</i> of the
			 * <i>ti</i>-th top simplex of dimension <i>tp</i>. We can also use an instance of this class for describing a directly encoded simplex, providing a common interface for accessing
			 * simplices.
			 * \return a constant reference to the simplex pointer to the current ghost simplex
			 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplex::getGhostSimplexPointer()
			 */
			const GhostSimplexPointer& getCGhostSimplexPointer() const;
			
			/// This member function returns a reference to the 0-simplices belonging to the current ghost simplex boundary.
			/**
			 * This member function returns a reference to the 0-simplices (namely, <i>vertices</i>) belonging to the current ghost simplex boundary.<p>A similar simplex can be accessed
			 * through an instance of the mangrove_tds::GhostSimplexPointer class: it is composed by a tuple <i>(tp,ti,cp,ci)</i>, describing a ghost simplex simplex as the <i>ci</i>-th face
			 * of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension <i>tp</i>. We can also use an instance of this class for describing a directly encoded simplex, providing a
			 * common interface for accessing simplices.<p><b>IMPORTANT:</b> you can modify boundary vertices through this member function.
			 * \return a reference to the 0-simplices belonging to the current ghost simplex boundary
			 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplex::getCBoundary()
			 */
			vector<SimplexPointer>& getBoundary();
			
			/// This member function returns a constant reference to the 0-simplices belonging to the current ghost simplex boundary.
			/**
			 * This member function returns a constant reference to the 0-simplices (namely, <i>vertices</i>) belonging to the current ghost simplex boundary.<p>A similar simplex can be
			 * accessed through an instance of the mangrove_tds::GhostSimplexPointer class: it is composed by a tuple <i>(tp,ti,cp,ci)</i>, describing a ghost simplex as the <i>ci</i>-th
			 * face of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension <i>tp</i>. We can also use an instance of this class for describing a directly encoded simplex,
			 * providing a common interface for accessing simplices.
			 * \return a constant reference to the 0-simplices belonging to the current ghost simplex boundary
			 * \see mangrove_tds::GhostSimplexPointer
			 */
			const vector<SimplexPointer>& getCBoundary() const;
			
			/// This member function checks if the current ghost simplex is incident in a vertex.
			/**
			 * This member function checks if the current ghost simplex is incident in a vertex, i.e. if a vertex belongs to its boundary, formed by some vertices.<p>If we cannot complete
			 * this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.
			 * \param cp a pointer to the required vertex
			 * \return <ul><li><i>true</i>, if the input vertex belongs to the current ghost simplex</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplexPointer, mangrove_tds::Simplex
			 */
			bool isIncident(const SimplexPointer& cp) const;
			
			/// This member function returns the position of a vertex in the boundary of the current ghost simplex.
			/**
			 * This member function returns the position of a vertex in the boundary of the current ghost simplex, i.e. it finds the position of the input vertex in its boundary, formed by
			 * some vertices.<p>If we cannot complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug
			 * mode. In this case, each forbidden operation will result in a failed assert.
			 * \param cp a pointer to the required vertex
			 * \param pos the position of the required vertex in the boundary relation (it is valid only if the input vertex belongs to the boundary)
			 * \return <ul><li><i>true</i>, if the input vertex is stored in the current ghost simplex boundary</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplexPointer, mangrove_tds::Simplex
			 */
			bool getPositionOf(const SimplexPointer& cp,unsigned int &pos);

			/// This member function checks if two current ghost simplices are the same simplex.
			/**
			 * This member function checks if two current ghost simplices are the same simplex, i.e. they are described by the same list of vertices.<p><b>IMPORTANT:</b> we cannot check if
			 * they have the same pointer, because a ghost simplex can be accessed through different ghost simplex pointers, i.e. instances of the mangrove_tds::GhostSimplexPointer class. We
			 * must check if boundary vertices are the same.
			 * \param s the second ghost simplex to be compared with the current one
			 * \return <ul><li><i>true</i>, if the input ghost simplex is described by the same of vertices as the current one</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplexPointer, mangrove_tds::Simplex
			 */
			bool theSame(const GhostSimplex& s) const;

			/// This member function writes a debug representation of the current ghost simplex on a output stream.
			/**
			 * This member function writes a debug representation of the current ghost simplex on a output stream.
			 * \param os the output stream where we can write a debug representation of the current ghost simplex
			 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplexPointer, mangrove_tds::Simplex
			 */
			void debug(ostream& os = cout) const;

			/// This operator writes a compact representation of a ghost simplex on an output stream.
			/**
			 * This operator writes a compact representation of a ghost simplex on an output stream, suitable for transmitting and receiving objects. A ghost simplex is identified as a tuple
			 * <i>(tp,ti,cp,ci)</i>, i.e. as the <i>ci</i>-th face of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension <i>tp</i>. Alternatively, it can be considered as
			 * formed by its list of 0-simplices. As a consequence, a representation of a ghost simplex is formatted with these guidelines:<p><i>[ tp ti cp ci ] [ vertices ]</i><p>
			 * <b>IMPORTANT:</b> this operator is not intended for debugging purposes. Otherwise, you should apply the mangrove_tds::GhostSimplex::debug() member function.
			 * \param os the output stream where we can write the compact representation of a ghost simplex pointer
			 * \param g the ghost simplex to be written
			 * \return the output stream after having produced the input ghost simplex description
			 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplex::debug()
			 */
			inline friend ostream& operator<<(ostream& os, const GhostSimplex& g)
			{
				/* We write the representation of the related ghost simplex pointer and the ghost 0-simplices */
				os<<g.gsp;
				for(unsigned int k=0;k<g.bnd.size();k++) { os<<g.bnd[k].id()<<" "; }
				os<<endl;
				os.flush();
				return os;
			}

			/// This operator rebuilds a compact representation of a ghost simplex on an output stream.
			/**
			 * This operator rebuilds a compact representation of a ghost simplex from an input stream, suitable for transmitting and receiving objects. A ghost simplex is identified as a
			 * tuple <i>(tp,ti,cp,ci)</i>, i.e. as the <i>ci</i>-th face of dimension <i>ct</i> of the <i>ti</i>-th top simplex of dimension <i>tp</i>. Alternatively, it can be considered as
			 * formed by its list of 0-simplices. As a consequence, a representation of a ghost simplex is formatted with these guidelines:<p><i>[ tp ti cp ci ] [ vertices ]</i>
			 * \param in the input stream from which we can read the compact representation of the ghost simplex pointer to be rebuilt
			 * \param g the ghost simplex to be read
			 * \return the input stream from after having read the ghost simplex description
			 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplex::debug()
			 */
			inline friend istream& operator>>(istream& in,GhostSimplex& g)
			{
				SIMPLEX_ID n;

				/* Now, we read the ghost simplex pointer related to the current ghost simplex */
				in>>g.gsp;
				g.bnd.clear();
				for(unsigned int k=0;k<=g.gsp.getChildType();k++)
				{
					in>>n;
					g.bnd.push_back(SimplexPointer(0,n));
				}

				/* If we arrive here, we have finished! */
				return in;
			}

			protected:
			
			/// A pointer to the current ghost simplex.
			/**
			 * \see mangrove_tds::GhostSimplexPointer
			 */
			GhostSimplexPointer gsp;
			
			/// Ghost 0-simplices belonging to the current ghost simplex boundary.
			/**
			 * \see mangrove_tds::SimplexPointer
			 */
			vector<SimplexPointer> bnd;
		};
	}

#endif

