/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library                                   
 *                                                                           				    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * March 2011 (Revised on May 2012)
 *                                                                    
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * Property.h - All the properties associated to simplices in a simplicial complex
 ***********************************************************************************************/
 
/* Should we include this header file? */
#ifndef PROPERTY_H_

	#define PROPERTY_H_
	
	#include <cstdlib>
	#include <assert.h>
	#include <iostream>
	#include <string>
	#include <deque>
	#include <map>
	#include <utility>
	#include "Simplex.h"
	#include "Miscellanea.h"
	using namespace std;
	using namespace mangrove_tds;

	/// Components for associating properties to simplices in a simplicial complex.
	/**
	 * The classes defined in this file are useful for representing and managing properties, i.e. auxiliary information (like normals, field values and so on) that can be associated to simplices in
	 * a simplicial complex, described by the mangrove_tds::BaseSimplicialComplex class.
	 * \file Property.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */

	/* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{	
		/// A handle for a generic property that can be assigned to simplices in a simplicial complex.
		/**
		 * This class describes a handle for a generic property (a normal, a field value, Euclidean coordinates,...) that can be assigned to simplices in a simplicial complex, described by the
		 * mangrove_tds::BaseSimplicialComplex class.<p>A property is identified by a unique name and it can be:<ul><li><i>global</i>, i.e. a property available for all the simplices in the
		 * current simplicial complex, described by the mangrove_tds::GlobalPropertyHandle class;</li><li><i>local</i>, i.e. a property binded only for a particular class of simplices in the
		 * current simplicial complex, described by the mangrove_tds::LocalPropertyHandle class</li><li><i>sparse</i>, i.e. a property associated to some simplices without a particular rule,
		 * described by the mangrove_tds::SparsePropertyHandle class.</li><li><i>ghost</i>, i.e. a property associated to a subset of simplices not directly encoded in the input data
		 * structure, described by the mangrove_tds::GhostPropertyHandle class.</ul>The <i>global</i>, <i>local</i> and <i>sparse</i> properties can be used with every type of data
		 * structures, while the <i>ghost</i> properties only with the <i>local</i> data structures, that encode only a subset of simplices (for instance, only top ones).<p>
		 * <b>IMPORTANT:</b> you should not directly use this class, since you should manage properties through the member functions offered by the mangrove_tds::BaseSimplicialComplex
		 * class.
		 * \see mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
		 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::BaseSimplicialComplex
		 */
		class PropertyBase
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function provides a rough initialization of the new instance of this class.<p><b>IMPORTANT:</b> you should not directly apply this member function, since you
			 * should manage properties through the member functions offered by the mangrove_tds::BaseSimplicialComplex class.
			 * \see mangrove_tds::BaseSimplicialComplex
			 */
			inline PropertyBase() { this->name.clear(); }
			
			/// This member function createas a new instance of this class.
			/**
			 * This member function creates a new instance of this class, by updating its name: it is the unique identifier for a property.<p><b>IMPORTANT:</b> you should not directly apply
			 * this member function, since you should manage properties through the member functions offered by the mangrove_tds::BaseSimplicialComplex class.
			 * \param name the new name of the current property
			 * \see mangrove_tds::PropertyBase::setName(), mangrove_tds::BaseSimplicialComplex
			 */
			inline PropertyBase(string name) { this->setName(name); }
			
			/// This member function destroys an instance of this class.
			inline virtual ~PropertyBase() { this->name.clear(); }
			
			/// This member function returns the name of the current property.
			/**
			 * This member function returns the name of the current property: it is the unique identifier of the current property.
			 * \return the name of the current property
			 * \see mangrove_tds::PropertyBase::setName()
			 */
			inline string getName() const { return string(this->name); }
			
			/// This member function updates the name of the current property.
			/**
			 * This member function updates the name of the current property: it is the unique identifier of the current property.
			 * \param name the new name of the current property
			 * \see mangrove_tds::PropertyBase::getName()
			 */
			inline void setName(string name) { this->name = string(name); }
			
			/// This member function checks if the current property is local.
			/**
			 * This member function checks if the current property is local. We say that a property is <i>local</i> if and only if it is binded only for a particular class of simplices in
			 * the current simplicial complex: a similar property is described by the mangrove_tds::LocalPropertyHandle class.<p><b>IMPORTANT:</b> this member function is pure virtual in the
			 * current class, thus all the subclasses of the mangrove_tds::PropertyBase class must provide a valid implementation for this member function.
			 * \return <ul><li><i>true</i>, if the current property is local</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle,
			 * mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer
			 */
			virtual bool isLocal() const =0;
			
			/// This member function checks if the current property is sparse.
			/**
			 * This member function checks if the current property is sparse. We say that a property is <i>sparse</i> if and only if it is binded to some simplices without a particular rule:
			 * a similar property is described by the mangrove_tds::SparsePropertyHandle class.<p><b>IMPORTANT:</b> this member function is pure virtual in the current class, thus all the
			 * subclasses of the mangrove_tds::PropertyBase class must provide a valid implementation for this member function.
			 * \return <ul><li><i>true</i>, if the current property is sparse</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle,
			 * mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer
			 */
			virtual bool isSparse() const =0;
			
			/// This member function checks if the current property is ghost.
			/**
			 * This member function checks if the current property is ghost. We say that a property is <i>ghost</i> if and only if it is binded to a subset of simplices not directly encoded 
			 * in the input data structure, described by the mangrove_tds::GhostPropertyHandle class.<p><b>IMPORTANT:</b> this member function is pure virtual in the current class, thus all
			 * the subclasses of the mangrove_tds::PropertyBase class must provide a valid implementation for this member function.
			 * \return <ul><li><i>true</i>, if the current property is ghost</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle,
			 * mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer
			 */
			virtual bool isGhost() const=0;
			
			/// This member function returns the dimension of simplices to whom the current property is associated.
			/**
			 * This member function returns the dimension of simplices to whom the current property is associated.<p><b>IMPORTANT:</b> this member function is pure virtual in the current
			 * class, thus all the subclasses of the mangrove_tds::PropertyBase class must provide a valid implementation for this member function.
			 * \return the dimension of simplices to whom the current property is associated
			 * \see mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle,
			 * mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE,
			 */
			virtual SIMPLEX_TYPE getDimension() const =0;
			
			/// This member function is used in order to remove all the values of the current property.
		 	/**
		 	 * This member function is used in order to remove all the values of the current property.<p><b>IMPORTANT:</b> this member function is pure virtual in the current class, thus all
		 	 * the subclasses of the mangrove_tds::PropertyBase class must provide a valid implementation for this member function.
		 	 * \see mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle,
			 * mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer
		 	 */
		 	virtual void clearValues()=0;
		 	
		 	/// This member function provides a debug representation of the current property on an output stream.
			/**
			 * This member function provides a debug representation of the current property on an output stream: such representation is intended only for debugging purposes.<p>
			 * <b>IMPORTANT:</b> this member function is pure virtual in the current class, thus all the subclasses of the mangrove_tds::PropertyBase class must provide a valid implementation
			 * for this member function.
			 * \param os the output stream where we can write a debug representation of the current property
			 * \see mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle,
			 * mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer
			 */
			virtual void debug(ostream &os = cout) const =0;
			
			/// This member function returns the number of values in the current property.
			/**
			 * This member function returns the number of values in the current property. This class describes a handle for a generic property (a normal, a field value, Euclidean
			 * coordinates,...) that can be assigned to simplices in a simplicial complex, described by the mangrove_tds::BaseSimplicialComplex class. A property is composed by a set of
			 * values, one for each simplex in the reference collection of simplices: this member function returns the number of these values, i.e. the number of simplices associated to the
			 * current property.<p>It is important to state that the reference collection of simplices could implement a garbage collector mechanism (as in the
			 * mangrove_tds::SimplicesContainer class), thus you should synchronize the number of property values with the number of all the simplices, including the deleted ones.<p>In the
			 * subclasses of the mangrove_tds::PropertyBase class some further conditions could be checked and verified: if they are not satisfied, then this member function will fail if and
			 * only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> this member
			 * function is pure virtual in the current class, thus all the subclasses of the mangrove_tds::PropertyBase class must provide a valid implementation for this member function.
			 * \return the number of values in the current property
			 * \see mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle,
			 * mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplicesContainer,
			 * mangrove_tds::BaseSimplicialComplex
			 */
			virtual unsigned int size() const =0;
			
			/// This member function returns the number of values for a particular class of simplices associated to the current property.
			/**
			 * This member function returns the number of values for a particular class of simplices associated to the current property. This class describes a handle for a generic property
			 * (a normal, a field value, Euclidean coordinates,...) that can be assigned to simplices in a simplicial complex, described by the mangrove_tds::BaseSimplicialComplex class. A
			 * property is composed by a set of values, one for each simplex in the reference collection of simplices: this member function returns the number of values for a particular
			 * class of simplices in the current property, identified by their dimension.<p>It is important to state that the reference collection of simplices could implement a garbage
			 * collector mechanism (as in the mangrove_tds::SimplicesContainer class), thus you should synchronize the number of property values with the number of all the simplices,
			 * including the deleted ones.<p>In the subclasses of the mangrove_tds::PropertyBase class some further conditions could be checked and verified: if they are not satisfied, then
			 * this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.<p><b>IMPORTANT:</b> this member function is pure virtual in the current class, thus all the subclasses of the mangrove_tds::PropertyBase class must provide a valid
			 * implementation for this member function.
			 * \param i the dimension of the required class of simplices
			 * \return the number of values for simplices of a particular dimension in the current property.
			 * \see mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle,
			 * mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplicesContainer,
			 * mangrove_tds::BaseSimplicialComplex, mangrove_tds::SIMPLEX_TYPE
			 */
			virtual unsigned int size(SIMPLEX_TYPE i) const =0;	
			
			/// This member function is used in order to synchronize the number of values in the current property.
		 	/**
		 	 * This member function is used in order to synchronize the number of values in the current property. This class describes a handle for a generic property (a normal, a field
			 * value, Euclidean coordinates,...) that can be assigned to simplices in a simplicial complex, described by the mangrove_tds::BaseSimplicialComplex class. A property is composed
			 * by a set of values, one for each simplex in the reference collection of simplices: this member function is used in order to synchronize the number of values in the current
			 * property with the number of simplices in the reference collection.<p>It is important to state that the reference collection of simplices could implement a garbage collector
			 * mechanism (as in the mangrove_tds::SimplicesContainer class), thus you should synchronize the number of property values with the number of all the simplices, including the
			 * deleted ones.<p>In the subclasses of the mangrove_tds::PropertyBase class some further conditions could be checked and verified: if they are not satisfied, then this member
			 * function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.<p><b>IMPORTANT:</b> this member function is pure virtual in the current class, thus all the subclasses of the mangrove_tds::PropertyBase class must provide a valid
			 * implementation for this member function.
		 	 * \param len the new number of allocated locations in the reference collection of simplices
		 	 * \see mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle,
		 	 * mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplicesContainer,
		 	 * mangrove_tds::BaseSimplicialComplex
		 	 */
		 	virtual void resize(unsigned int len) =0;
		 	
		 	/// This member function is used in order to synchronize the number of values for a particular class of simplices in the current property.
		 	/**
		 	 * This member function is used in order to synchronize the number of values for a particular class of simplices in the current property. This class describes a handle for a
			 * generic property (a normal, a field value, Euclidean coordinates,...) that can be assigned to simplices in a simplicial complex, described by the
			 * mangrove_tds::BaseSimplicialComplex class. A property is composed by a set of values, one for each simplex in the reference collection of simplices: this member function is
			 * used in order to synchronize the number of values in the current property with the number of a particular class of simplices in the reference collection, identified by their
			 * dimension.<p>It is important to state that the reference collection of simplices could implement a garbage collector mechanism (as in the mangrove_tds::SimplicesContainer
			 * class), thus you should synchronize the number of property values with the number of all the simplices, including the deleted ones.<p>In the subclasses of the
			 * mangrove_tds::PropertyBase class some further conditions could be checked and verified: if they are not satisfied, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> this member function is pure
			 * virtual in the current class, thus all the subclasses of the mangrove_tds::PropertyBase class must provide a valid implementation for this member function.
			 * \param i the dimension of the required class of simplices
		 	 * \param len the new number of allocated locations for a particular class of simplices
		 	 * \see mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle,
			 * mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplicesContainer,
		 	 * mangrove_tds::BaseSimplicialComplex, mangrove_tds::SIMPLEX_TYPE
		 	 */
		 	virtual void resize( SIMPLEX_TYPE i, unsigned int len) =0;
			
			protected:
			
			/// The unique name of the current property.
			string name;
		};
		
		/// A handle for a local property associated to a particular class of simplices in a simplicial complex.
		/**
		 * This class describes a handle for a <i>local</i> property (a normal, a field value, Euclidean coordinates, ...) associated to a particular class of simplices in a simplicial complex,
		 * described by the mangrove_tds::BaseSimplicialComplex class: we say that a property is <i>local</i> if and only if it is binded only for a particular collection of simplices
		 * (identified by their dimension) belonging to a simplicial complex. Such properties are supported by every type of data structure, including the <i>local</i> ones. In this case, they
		 * are associated to all simplices of the required dimension directly encoded in the input data structure. Each property is described by a set of templated T values, one for each simplex
		 * in the reference collection: the T template class is required to support the I/O operators.<p><b>IMPORTANT:</b> you should not directly use this class, since you should manage
		 * properties through the member functions offered by the mangrove_tds::BaseSimplicialComplex class.
		 * \see mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle,
		 * mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::BaseSimplicialComplex
		 */
		template <class T> class LocalPropertyHandle : public PropertyBase
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This class describes a handle for a <i>local</i> property (a normal, a field value, Euclidean coordinates, ...) associated to a particular class of simplices in a simplicial
			 * complex, described by the mangrove_tds::BaseSimplicialComplex class: we say that a property is <i>local</i> if and only if it is binded only for a particular collection of
			 * simplices (identified by their dimension) belonging to a simplicial complex. Such properties are supported by every type of data structure, including the <i>local</i> ones. In
			 * this case, they are associated to all simplices of the required dimension directly encoded in the input data structure. Each property is described by a set of T templated
			 * values, one for each simplex in the reference collection: the T template class is required to support the writing and reading operators.<p><b>IMPORTANT:</b> you should not
			 * directly use this member function, since you should manage local properties through the member functions offered by the mangrove_tds::BaseSimplicialComplex class.
		 	 * \param dim the dimension of the simplices to whom the new templated property is associated
		 	 * \param name the name of the new templated property
		 	 * \param orig the default value for the new templated property
		 	 * \see mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
		 	 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline LocalPropertyHandle( SIMPLEX_TYPE dim, string name, T orig = T()) : PropertyBase(name)
			{
				/* We must create a new templated value */
				this->dim = dim;
				this->v.clear();
				this->original = orig;
			}
			
			/// This member function destroys an instance of this class.
			inline virtual ~LocalPropertyHandle()
			{
				/* We remove the property */
				this->v.clear();
				this->name.clear();
			}
			
			/// This member function checks if the current property is local.
			/**
			 * This member function checks if the current property is local. We say that a property is <i>local</i> if and only if it is binded only for a particular collection of simplices
			 * (identified by their dimension) belonging to a simplicial complex: this class describes a local property, thus the result of this member function is trivial.
			 * \return <ul><li><i>true</i>, if the current property is local</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual bool isLocal() const { return true; }
			
			/// This member function checks if the current property is sparse.
			/**
			 * This member function checks if the current property is sparse. We say that a property is <i>sparse</i> if and only if it is binded to some simplices without a particular rule:
			 * this class describes a local property, thus the result of this member function is trivial.
			 * \return <ul><li><i>true</i>, if the current property is sparse</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual bool isSparse() const { return false; } 
			
			/// This member function checks if the current property is ghost.
			/**
			 * This member function checks if the current property is ghost. We say that a property is <i>ghost</i> if and only if it is binded to a subset of simplices not directly encoded
			 * in the input data structure: this class describes a local property, thus the result of this member function is trivial.
			 * \return <ul><li><i>true</i>, if the current property is ghost</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex, mangrove_tds::SimplexPointer,
			 * mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual bool isGhost() const { return false; }
			
			/// This member function returns the dimension of simplices to whom the current property is associated.
			/**
			 * This member function returns the dimension of simplices to whom the current property is associated. We say that a property is <i>local</i> if and only if it is binded only for
			 * a particular collection of simplices (identified by their dimension) belonging to a simplicial complex. Such properties are supported by every type of data structure,
			 * including the <i>local</i> ones. In this case, they are associated to all simplices of the required dimension directly encoded in the input data structure.
			 * \return the dimension of simplices to whom the current property is associated
			 * \see mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual SIMPLEX_TYPE getDimension() const { return this->dim; }
			
			/// This member function is used in order to remove all the values of the current property.
			/**
			 * This member function is used in order to remove all the values of the current property. We say that a property is <i>local</i> if and only if it is binded only for a
			 * particular collection of simplices (identified by their dimension) belonging to a simplicial complex. Such properties are supported by every type of data structure, including
			 * the <i>local</i> ones. In this case, they are associated to all simplices of the required dimension directly encoded in the input data structure.
			 * \see mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
		 	inline virtual void clearValues() { this->v.clear(); }
			
			/// This member function provides a debug representation of the current property on an output stream.
			/**
			 * This member function provides a debug representation of the current property on an output stream, intended only for debugging purposes.
			 * \param os the output stream where we can write a debug representation of the current property
			 * \see mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex, mangrove_tds::SimplexPointer,
			 * mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer
			 */
		 	inline virtual void debug(ostream &os = cout) const
			{
				/* We write information about the current property. */
				os<<"Local property name: "<<this->getName()<<endl;
				os<<"Local property associated to a collection of "<<this->getDimension()<<"-simplices"<<endl;
				os<<"The default value: "<<this->original<<endl;
				os<<"The number of values: "<<this->v.size()<<endl;
				os<<"The values: ";
				for(unsigned int k=0;k<v.size();k++) os<<this->v[k]<<" ";
				os<<endl;
				os.flush();
			}

			/// This member function returns the number of values in the current property.
			/**
			 * This member function returns the number of values in the current property. A <i>local</i> property (i.e. an auxiliary information binded only for a particular collection of
			 * simplices, identified by their dimension) belonging to a simplicial complex is composed by a set of templated values, one for each simplex in the reference collection: this
			 * member function returns the number of these values, i.e. the number of simplices associated to the current property. Such properties are supported by every type of data
			 * structure, including the <i>local</i> ones. In this case, they are associated to all simplices of the required dimension directly encoded in the input data structure.
		 	 * \return the number of values in the current property
		 	 * \see mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
		 	 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual unsigned int size() const { return this->v.size(); }

			/// This member function returns the number of values for a particular class of simplices associated to the current property.
			/**
			 * This member function returns the number of values for a particular class of simplices associated to the current property. A <i>local</i> property (i.e. an auxiliary
			 * information binded only for a particular collection of simplices, identified by their dimension) belonging to a simplicial complex is composed by a set of templated values,
			 * one for each simplex in the reference collection: this member function returns the number of values for a particular class of simplices, identified by their dimension. Such
			 * properties are supported by every type of data structure, including the <i>local</i> ones. In this case, they are associated to all simplices of the required dimension
			 * directly encoded in the input data structure.<p>This property is local and thus we must check if the required class of simplices coincides with the class of simplices in the
			 * current property: if this condition is not satisfied, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this
			 * case, each forbidden operation will result in a failed assert.
			 * \param i the dimension of the required class of simplices
			 * \return the number of values for simplices of a particular dimension in the current property
			 * \see mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex,
			 * mangrove_tds::idle()
			 */
			inline virtual unsigned int size(SIMPLEX_TYPE i) const
			{
				/* First, we check if the required class of simplices exists + a dummy statement for avoiding warnings! */
				assert(i == this->dim);
				idle(i);
		 		return this->v.size();
			}
			
			/// This member function is used in order to synchronize the number of values in the current property.
			/**
			 * This member function is used in order to synchronize the number of values in the current property. A <i>local</i> property (i.e. an auxiliary information binded only for a
			 * particular collection of simplices, identified by their dimension) is composed by a set of templated values, one for each simplex in the reference collection: this member
			 * function is used in order to synchronize the number of values in the current property with the number of simplices in the reference collection. Such properties are supported
			 * by every type of data structure, including the <i>local</i> ones. In this case, they are associated to all simplices of the required dimension directly encoded in the input
			 * data structure.<p><b>IMPORTANT:</b> the reference collection of simplices could implement a garbage collector mechanism (as in the mangrove_tds::SimplicesContainer class),
			 * thus you should synchronize the number of values with the number of all the simplices, including the deleted ones.
			 * \param len the new number of allocated locations for a particular class of simplices
			 * \see mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex,
			 * mangrove_tds::idle()
			 */
			inline virtual void resize(unsigned int len) { if(len!=this->v.size()) this->v.resize(len,this->original); }
			
			/// This member function is used in order to synchronize the number of values for a particular class of simplices in the current property.
		 	/**
		 	 * This member function is used in order to synchronize the number of values for a particular class of simplices in the current property. A <i>local</i> property (i.e. an
			 * auxiliary information binded only for a particular collection of simplices, identified by their dimension) is composed by a set of templated values, one for each simplex in
			 * the reference collection: this member function is used in order to synchronize the number of values in the current property with the number of a particular class of simplices
			 * in the reference collection, identified by their dimension. Such properties are supported by every type of data structure, including the <i>local</i> ones. In this case, they
			 * are associated to all simplices of the required dimension directly encoded in the input data structure.<p>This property is local and thus we must check if the required class
			 * of simplices coincides with the class of simplices in the current property: if this condition is not satisfied, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> the reference collection of
			 * simplices could implement a garbage collector mechanism (as in the mangrove_tds::SimplicesContainer class), thus you should synchronize the number of property values with the
			 * number of all the simplices, including the deleted ones.
		 	 * \param i the dimension of the required class of simplices
		 	 * \param len the new number of allocated locations for a particular class of simplices
		 	 * \see mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
		 	 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex,
		 	 * mangrove_tds::idle()
		 	 */
			inline virtual void resize(SIMPLEX_TYPE i, unsigned int len)
		 	{
		 		/* First, we check if the required class of simplices exists + a dummy statement for avoiding warnings! */
		 		assert(i==this->dim);
		 		idle(i);
		 		if(len!=this->v.size()) this->v.resize(len,this->original);
		 	}
			
			/// This member function returns a reference for the value associated to a simplex managed in the current property.
			/**
			 * This member function returns a reference for the value associated to a simplex managed in the current property. A <i>local</i> property (i.e. an auxiliary information binded
			 * only for a particular collection of simplices, identified by their dimension) is composed by a set of templated values, one for each simplex in the reference collection: this
			 * member function returns the value associated to one of these simplices. Such properties are supported by every type of data structure, including the <i>local</i> ones. In this
			 * case, they are associated to all simplices of the required dimension directly encoded in the input data structure.<p>If the required simplex does not exist, then this member
			 * function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param cp a pointer to the simplex associated to the required property value
			 * \return the reference to the required property value
			 * \see mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex, mangrove_tds::SimplexPointer,
			 * mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::LocalPropertyHandle::set()
			 */
			inline virtual T& get(const SimplexPointer &cp)
			{
				/* First, we check if the required simplex exists! */
				assert( cp.type() == this->dim);
				assert(cp.id()<this->v.size());
				return this->v[cp.id()];
			}
			
			/// This member function updates the value associated to a simplex managed in the current property.
		 	/**
		 	 * This member function updates the value associated to a simplex managed in the current property. A <i>local</i> property (i.e. an auxiliary information binded only for a
			 * particular collection of simplices, identified by their dimension) is composed by a set of templated values, one for each simplex in the reference collection: this member
			 * function updates the value associated to one of these simplices. Such properties are supported by every type of data structure, including the <i>local</i> ones. In this case,
			 * they are associated to all simplices of the required dimension directly encoded in the input data structure.<p>If the simplex to be updated does not exist, then this member
			 * function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param cp a pointer to the simplex associated to the property value to be updated
			 * \param value the new value for the simplex to be updated
			 * \see mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex, mangrove_tds::SimplexPointer,
			 * mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::LocalPropertyHandle::get()
		 	 */
		 	inline virtual void set(const SimplexPointer& cp,T value)
			{
				/* First, we check if the required simplex exists! */
				assert( cp.type() == this->dim);
				assert(cp.id()<this->v.size());
				this->v[cp.id()] = value;
			}
			
			protected:
			
			/// The values associated to particular class of simplices in a simplicial complex.
			deque<T> v;
			
			/// The dimension of the simplices to whom the current property is associated.
			/**
			 * \see mangrove_tds::SIMPLEX_TYPE
			 */
			SIMPLEX_TYPE dim;
			
			/// The default templated value for all the simplices corresponding to the current property.
			T original;
			
			/// This member function creates a new instance of this class.
			inline LocalPropertyHandle() : PropertyBase()
			{
				/* Dummy initialization */
				this->dim = 0;
				this->v.clear();
			}
		};
		
		/// A handle for a global property associated to all the simplices in a simplicial complex.
		/**
		 * This class describes a handle for a <i>global</i> property (a normal, a field value, Euclidean coordinates,...) associated to all the simplices in a simplicial complex, described by
		 * the mangrove_tds::BaseSimplicialComplex class. This property is described by a set of T templated values for each class of simplices in a simplicial complex and the T template is
		 * required to support the I/O operators. Such properties are supported by every type of data structure, including the <i>local</i> ones.<p>It is important to state that a simplicial
		 * complex can have a different number of simplices for each dimension.<p><b>IMPORTANT:</b> you should not directly use this class, since you should manage properties through the member
		 * functions offered by the mangrove_tds::BaseSimplicialComplex class.
		 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle,
		 * mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::BaseSimplicialComplex
		 */
		template <class T> class GlobalPropertyHandle : public PropertyBase
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a new instance of this class, by allocating all the internal state. A <i>global</i> property is described by a set of T templated values for each
			 * class of simplices in a simplicial complex and the T template is required to support the I/O operators. Such properties are supported by every type of data structure,
			 * including the <i>local</i> ones.<p><b>IMPORTANT:</b> you should not directly use this member function, since you should manage global properties through the member functions
			 * offered by the mangrove_tds::BaseSimplicialComplex class.
		 	 * \param dim the dimension of the simplicial complex to whom the new property will be associated
		 	 * \param name the name of the new templated property
		 	 * \param orig the default value for the new templated property
		 	 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
		 	 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline GlobalPropertyHandle(SIMPLEX_TYPE dim, string name, T orig = T()) : PropertyBase(name)
			{
				/* First, we must reset 'handles' and then we create a property for each dimension */
				this->values.clear();
				this->original = orig;
				for(SIMPLEX_TYPE k=0;k<=dim;k++) { this->values.push_back(deque<T>()); }
			}
			
			/// This member function destroys an instance of this class.
			/**
			 * This member function destroys an instance of this class, by deallocating the internal state.
			 * \see mangrove_tds::GlobalPropertyHandle::destroyInternalState()
			 */
			inline virtual ~GlobalPropertyHandle() { this->destroyInternalState(); }
			
			/// This member function checks if the current property is local.
			/**
			 * This member function checks if the current property is local. We say that a property is <i>local</i> if and only if it is binded only for a particular collection of simplices
			 * (identified by their dimension) belonging to a simplicial complex: this class describes a global property, thus the result of this member function is trivial.
			 * \return <ul><li><i>true</i>, if the current property is local</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual bool isLocal() const { return false; }

			/// This member function checks if the current property is sparse.
			/**
			 * This member function checks if the current property is sparse. We say that a property is <i>sparse</i> if and only if it is binded to some simplices without a particular rule:
			 * this class describes a global property, thus the result of this member function is trivial.
			 * \return <ul><li><i>true</i>, if the current property is sparse</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual bool isSparse() const { return false; }
			
			/// This member function checks if the current property is ghost.
			/**
			 * This member function checks if the current property is ghost. We say that a property is <i>ghost</i> if and only if it is binded to a subset of simplices not directly encoded
			 * in the input data structure: this class describes a global property, thus the result of this member function is trivial.
			 * \return <ul><li><i>true</i>, if the current property is ghost</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual bool isGhost() const { return false; }
			
			/// This member function returns the dimension of the simplicial complex to whom the current property is associated.
			/**
			 * This member function returns the dimension of the simplicial complex to whom the current property is associated. A <i>global</i> property is described by a set of T templated
			 * values for each class of simplices in a simplicial complex and the T template is required to support the I/O operators. Such properties are supported by every type of data
			 * structure, including the <i>local</i> ones.
			 * \return the dimension of the simplicial complex to whom the current property is associated
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual SIMPLEX_TYPE getDimension() const
			{
				/* Now, we return the result in accordance with the number of components in 'values' */ 
				if(this->values.size()==0) { return 0; }
				else { return (this->values.size()-1); }
			}
			
			/// This member function is used in order to remove all the values of the current property.
			/**
			 * This member function is used in order to remove all the values of the current property. A <i>global</i> property is described by a set of T templated values for each class of
			 * simplices in a simplicial complex and the T template is required to support the I/O operators. Such properties are supported by every type of data structure, including the
			 * <i>local</i> ones.
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
		 	inline virtual void clearValues() { for(unsigned int k=0;k<this->values.size(); k++) this->values[k].clear(); }
		 	
		 	/// This member function provides a debug representation of the current property on an output stream.
		 	/**
		 	 * This member function provides a debug representation of the current property on an output stream, intended only for debugging purposes.
		 	 * \param os the output stream where we can write a debug representation of the current property
		 	 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
		 	 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
		 	 */
		 	inline virtual void debug(ostream &os = cout) const
			{
				/* We write a debug representation! */
				os<<"Global property associated to a simplicial "<<this->getDimension()<<"-complex"<<endl;
				os<<"The property name: "<<this->getName()<<endl;
				os<<"The default value: "<<this->original<<endl;
				os.flush();
				for(unsigned int k=0;k<this->values.size();k++)
				{
					/* Now, we write values for simplices of dimension k */
					os<<endl<<"Values for the "<<this->values[k].size()<<" simplices of dimension "<<k<<" : ";
					for(unsigned int j=0;j<this->values[k].size();j++) { cout<<this->values[k].at(j)<<" "; }
				}
				
				/* If we arrive here, we have finished! */
				os<<endl;
				os.flush();
			}
			
			/// This member function returns the number of values in the current property.
			/**
			 * This member function returns the number of values in the current property. The current property is <i>global</i> (i.e. a property available for all the simplices) and thus this
			 * member function cannot be applied since we need to know the dimension of simplices to refer.<p>Thus, this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \return the number of values in the current property
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual unsigned int size() const
			{
				/* Dummy statement for the compiler! */
				assert(this->isLocal()==true);
				return 0; 
			}
			
			/// This member function returns the number of values for a particular class of simplices associated to the current property.
			/**
			 * This member function returns the number of values for a particular class of simplices associated to the current property. A <i>global</i> property associated to a simplicial
			 * k-complex is composed by a set of T templated values for each class of simplices in a simplicial complex and the T template is required to support the I/O operators. It is
			 * important to state that a simplicial complex can have a different number of simplices for each dimension: this member function returns the number of simplices for a particular
			 * class of simplices, identified by its dimension.<p><b>IMPORTANT:</b> if the required type of simplices is not valid for the reference simplicial complex, then this member
			 * function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param i the dimension of the required class of simplices
			 * \return the number of values for a particular class of simplices associated to the current property
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */ 
			inline virtual unsigned int size(SIMPLEX_TYPE i) const
			{ 
				/* First, we check if the required class of simplices exists and it is valid */
				assert( i<this->values.size());
				return this->values[i].size();
			}
			
			/// This member function is used in order to synchronize the number of values in the current property.
			/**
			 * This member function is used in order to synchronize the number of values in the current property. The current property is <i>global</i> (i.e. a property available for all the
			 * simplices) and thus this member function cannot be applied since we need to know the dimension of simplices to refer.<p>Thus, this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param len the new number of allocated locations in the reference collection of simplices 
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex,
			 * mangrove_tds::idle()
			 */
			inline virtual void resize(unsigned int len) 
		 	{
		 		/* Dummy statement for the compiler! */
		 		idle(len);
		 		assert(this->isLocal()==true);
		 	}
		 	
		 	/// This member function is used in order to synchronize the simplices number of a particular dimension in the reference simplicial complex.
			/**
			 * This member function is used in order to synchronize the simplices number of a particular dimension in the reference simplicial complex. A <i>global</i> property associated to
			 * a simplicial k-complex is composed by a set of T templated values for each class of simplices in a simplicial complex and the T template is required to support the I/O
			 * operators. It is important to state that a simplicial complex can have a different number of simplices for each dimension: this member function is used in order to synchronize
			 * the number of values for a particular property with the simplices number of a particular class in the reference simplicial complex.<p>If the required type of simplices is not
			 * valid for the reference simplicial complex, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each
			 * forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> the reference collection of simplices could implement a garbage collector mechanism (as in the
			 * mangrove_tds::SimplicesContainer class), thus you should synchronize the number of property values with the number of all the simplices (including the deleted ones).
			 * \param i the required dimension of simplices in the reference simplicial complex
			 * \param len the new number of values for the required property associated to a particular class of simplices in the reference simplicial complex
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex,
			 * mangrove_tds::idle()
			 */
			inline virtual void resize(SIMPLEX_TYPE i, unsigned int len)
			{
				/* First, we check if the required class of simplices exists and it is valid */
				assert(i<this->values.size());
				if( this->values[i].size()!=len) this->values[i].resize(len,this->original);
			}
			
			/// This member function returns a reference for the value associated to a simplex managed in the current property.
			/**
			* This member function returns a reference for the value associated to a simplex managed in the current property. A <i>global</i> property associated to a simplicial k-complex is
			* composed by a set of T templated values for each class of simplices in a simplicial complex and the T template is required to support the I/O operators. It is important to
			* state that a simplicial complex can have a different number of simplices for each dimension: this member function returns a reference to the value associated to a value in the
			* current property.<p>If the required simplex does not exist, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In
			* this case, each forbidden operation will result in a failed assert.
			* \param cp a pointer to the simplex associated to the required property value
			* \return the reference to the required property value
			* \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			* mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex,
			* mangrove_tds::GlobalPropertyHandle::set()
			*/
			inline virtual T& get(const SimplexPointer &cp)
			{
				/* First, we must check if the required simplex exists! */
				assert( cp.type() < this->values.size());
				assert( cp.id() < this->values[ cp.type() ].size());
				return (this->values[cp.type()].at(cp.id()));
			}
			
			/// This member function updates the value associated to a simplex managed in the current property.
			/**
			 * This member function updates the value associated to a simplex managed in the current property. A <i>global</i> property associated to a simplicial k-complex is composed by a
			 * set of T templated values for each class of simplices in a simplicial complex and the T template is required to support the I/O operators. It is important to state that a
			 * simplicial complex can have a different number of simplices for each dimension: this member function updates the value associated to a simplex in the current property.<p>If
			 * the required simplex does not exist, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.
			 * \param cp a pointer to the simplex associated to the property value to be updated
			 * \param value the new value for the simplex to be updated
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex,
			 * mangrove_tds::GlobalPropertyHandle::get()
			 */
			inline virtual void set(const SimplexPointer &cp, T value)
			{
				/* First, we must check if the required simplex exists! */
				assert( cp.type() < this->values.size());
				assert( cp.id() < this->values[cp.type()].size());
				(this->values[cp.type()])[cp.id()] = value;
			}
			
			protected:
			
			/// The default value for the templated property.
			T original;
			
			/// The set of values for all the classes of simplices in the reference simplicial complex.
			deque< deque<T> > values;
			
			/// This member function creates an instance of this class.
			inline GlobalPropertyHandle() { this->values.clear(); }
			
			/// This member function destroys the internal state of this class.
			/**
			 * This member function destroys the internal state of this class, by deallocating all the internal data structures.
			 * \see mangrove_tds::GlobalPropertyHandle::clearValues()
			 */
			inline void destroyInternalState()
			{
				/* We destroy the property for all the dimensions! */
				this->clearValues();
				this->values.clear();
			}
		};
		
		/// A handle for a sparse property associated to some simplices in a simplicial complex.
		/**
		 * This class describes a handle for a <i>sparse</i> property (a normal, a field value, Euclidean coordinates,...) associated to some simplices (without any rule) directly encoded in a
		 * data structure, described by the mangrove_tds::BaseSimplicialComplex class. The T template describes the templated value associated to a simplex and it must supports the I/O operators.
		 * <p><b>IMPORTANT:</b> you should not directly use this class, since you should manage properties through the member functions offered by the
		 * mangrove_tds::BaseSimplicialComplex class.
		 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
		 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::BaseSimplicialComplex
		 */
		template <class T> class SparsePropertyHandle : public PropertyBase
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a new instance of this class. This class describes a handle for a <i>sparse</i> property (a normal, a field value, Euclidean coordinates,...)
			 * associated to some simplices (without any rule) directly encoded in a data structure, described by the mangrove_tds::BaseSimplicialComplex class. The T template describes the
			 * templated value associated to a simplex and it must supports the I/O operators.<p><b>IMPORTANT:</b> you should not directly use this class, since you should manage properties
			 * through the member functions offered by the mangrove_tds::BaseSimplicialComplex class.
			 * \param dim the dimension of the simplicial complex to whom the new property will be associated
		 	 * \param name the name of the new templated property
		 	 * \param orig the default value for the new templated property
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
		 	 */
			inline SparsePropertyHandle(SIMPLEX_TYPE dim, string name, T orig = T()) : PropertyBase(name)
			{
				/* We initialize member fields! */
				this->original=orig;
				this->dim=dim;
				this->clearValues();
			}
			
			/// This member function destroys an instance of this class.
			/**
			 * This member function destroys an instance of this class.
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual ~SparsePropertyHandle() { this->clearValues(); }
			
			/// This member function checks if the current property is local.
			/**
			 * This member function checks if the current property is local. We say that a property is <i>local</i> if and only if it is binded only for a particular collection of simplices
			 * (identified by their dimension) belonging to a simplicial complex: this class describes a sparse property, thus the result of this member function is trivial.
			 * \return <ul><li><i>true</i>, if the current property is local</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual bool isLocal() const { return false; }
			
			/// This member function checks if the current property is sparse.
			/**
			 * This member function checks if the current property is sparse. We say that a property is <i>sparse</i> if and only if it is binded to some simplices without a particular rule:
			 * this class describes a sparse property, thus the result of this member function is trivial.
			 * \return <ul><li><i>true</i>, if the current property is sparse</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual bool isSparse() const { return true; }
			
			/// This member function checks if the current property is ghost.
			/**
			 * This member function checks if the current property is ghost. We say that a property is <i>ghost</i> if and only if it is binded to a subset of simplices not directly encoded
			 * in the input data structure: this class describes a sparse property, thus the result of this member function is trivial.		
			 * \return <ul><li><i>true</i>, if the current property is ghost</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual bool isGhost() const { return false; }

			/// This member function returns the dimension of the simplicial complex to whom the current property is associated.
			/**
			 * This member function returns the dimension of the simplicial complex to whom the current property is associated. This class describes a handle for a <i>sparse</i> property (a
			 * normal, a field value, Euclidean coordinates,...) associated to some simplices (without any rule) directly encoded in a data structure, described by the
			 * mangrove_tds::BaseSimplicialComplex class. The T template describes the templated value associated to a simplex and it must supports the I/O operators.
			 * \return the dimension of the simplicial complex to whom the current property is associated
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex 
			 */
			inline virtual SIMPLEX_TYPE getDimension() const { return this->dim; }

			/// This member function is used in order to remove all the values of the current property.
			/**
			 * This member function is used in order to remove all the values of the current property. This class describes a handle for a <i>sparse</i> property (a normal, a field value,
			 * Euclidean coordinates,...) associated to some simplices (without any rule) directly encoded in a data structure, described by the mangrove_tds::BaseSimplicialComplex class.
			 * The T template describes the templated value associated to a simplex and it must supports the I/O operators.
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex 
			 */
			inline virtual void clearValues()
		 	{
		 		this->keys.clear();
		 		this->values.clear();
		 	}
		 	
		 	/// This member function provides a debug representation of the current property on an output stream.
		 	/**
		 	 * This member function provides a debug representation of the current property on an output stream.
		 	 * \param os the output stream where we can write a debug representation of the current property
		 	 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::Simplex,
		 	 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
		 	 */
		 	inline virtual void debug(ostream &os = cout) const
			{
				map<SimplexPointer,unsigned int>::const_iterator it;
				
				/* We write a debug representation! */
				os<<"Sparse property associated to a simplicial "<<this->getDimension()<<"-complex"<<endl;
				os<<"The property name: "<<this->getName()<<endl;
				os<<"The default value: "<<this->original<<endl;
				os<<"There are "<<this->values.size()<<" associations among simplices and values";
				for(it=this->keys.begin();it!=this->keys.end();it++)
				{
					os<<endl;
					it->first.debug(os);
					os<<"ASSOCIATED TO VALUE "<<this->values[it->second]<<endl<<"========================"<<endl;
					os.flush();
				}
				
				/* If we arrive here, we have finished */
				os<<endl;
				os.flush();
			}
			
			/// This member function returns the number of values in the current property.
			/**
			 * This member function returns the number of values in the current property. This class describes a handle for a <i>sparse</i> property (a normal, a field value, Euclidean
			 * coordinates,...) associated to some simplices (without any rule) directly encoded in a data structure, described by the mangrove_tds::BaseSimplicialComplex class. The T
			 * template describes the templated value associated to a simplex and it must supports the I/O operators.
			 * \return the number of values in the current property
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual unsigned int size() const { return this->values.size(); }
			
			/// This member function returns the number of values for a particular class of simplices associated to the current property.
			/**
			 * This member function returns the number of values for a particular class of simplices associated to the current property. This class describes a handle for a <i>sparse</i>
			 * property (a normal, a field value, Euclidean coordinates,...) associated to some simplices (without any rule) directly encoded in a data structure, described by the
			 * mangrove_tds::BaseSimplicialComplex class. The T template describes the templated value associated to a simplex and it must supports the I/O operators.
			 * \return the number of values in the current property
			 * \param i the dimension of the required simplices
			 * \return the number of values for a particular class of simplices associated to the current property
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual unsigned int size(SIMPLEX_TYPE i) const
			{
				map<SimplexPointer,unsigned int>::const_iterator it;
				unsigned int ci;
				
				/* First, we check if all is ok and the game can start! */
				assert(i<=dim);
				ci= 0;
				for(it=this->keys.begin();it!=this->keys.end();it++) { if(it->first.type()==i) ci=ci+1; }
				return ci;
			}

			/// This member function is used in order to synchronize the number of values in the current property.
			/**
			 * This member function is used in order to synchronize the number of values in the current property. This class describes a handle for a <i>sparse</i> property (a normal, a field
			 * value, Euclidean coordinates,...) associated to some simplices (without any rule) directly encoded in a data structure, described by the mangrove_tds::BaseSimplicialComplex
			 * class. The T template describes the templated value associated to a simplex and it must supports the I/O operators.<p>Thus, this member function cannot be applied since we
			 * have not some space to reallocate.<p><b>IMPORTANT:</b> this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each
			 * forbidden operation will result in a failed assert.
			 * \param len the new number of allocated locations in the reference collection of simplices
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex,
			 * mangrove_tds::idle()
			 */
			inline virtual void resize(unsigned int len) 
			{
				/* Dummy statements! */
				idle(len);
				assert(false);
			}
			
			/// This member function is used in order to synchronize the ghost simplices number of a particular dimension in the reference simplicial complex.
			/**
			 * This member function is used in order to synchronize the ghost simplices number of a particular dimension in the reference simplicial complex. This class describes a handle
			 * for a <i>sparse</i> property (a normal, a field value, Euclidean coordinates,...) associated to some simplices (without any rule) directly encoded in a data structure,
			 * described by the mangrove_tds::BaseSimplicialComplex class. The T template describes the templated value associated to a simplex and it must supports the I/O
			 * operators.<p>Thus, this member function cannot be applied since we have not some space to reallocate.<p><b>IMPORTANT:</b> this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param i the required dimension of simplices in the reference simplicial complex
			 * \param len the new number of values for the required property associated to a particular class of simplices in the reference simplicial complex
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex,
			 * mangrove_tds::idle()
			 */
			inline virtual void resize(SIMPLEX_TYPE i, unsigned int len) 
			{
				/* Dummy statements! */
				idle(len);
				idle(i);
				assert(false);
			}
			
			/// This member function updates the value associated to a simplex managed in the current property.
			/**
			 * This member function updates the value associated to a simplex managed in the current property. This class describes a handle for a <i>sparse</i> property (a normal, a field
			 * value, Euclidean coordinates,...) associated to some simplices (without any rule) directly encoded in a data structure, described by the mangrove_tds::BaseSimplicialComplex
			 * class. The T template describes the templated value associated to a simplex and it must supports the I/O operators.<p>If the input simplex has not an association with a
			 * templated value, then a new association is created with the default value.<p><b>IMPORTANT:</b> if we cannot complete operation for any reason, then this member function will
			 * fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param s a pointer to the ghost simplex associated to the property value to be updated
			 * \param value the new value for the simplex to be updated
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual void set(const SimplexPointer &s, T value)
			{
				map<SimplexPointer,unsigned int>::iterator it;
				
				/* First, we check if the required simplex exists! */
				assert(s.type()<=this->dim);
				it=this->keys.find(s);
				if(it==this->keys.end())
				{
					this->values.push_back(value);
					this->keys[SimplexPointer(s)]=this->values.size()-1;
				}
				else { this->values[it->second]=value; }
			}
			
			/// This member function retrieves a reference to the value associated to a simplex managed in the current property.
			/**
			 * This member function retrieves a reference to the value associated to a simplex managed in the current property. This class describes a handle for a <i>sparse</i> property (a
			 * normal, a field value, Euclidean coordinates,...) associated to some simplices (without any rule) directly encoded in a data structure, described by the
			 * mangrove_tds::BaseSimplicialComplex class. The T template describes the templated value associated to a simplex and it must supports the I/O operators.<p>If the input simplex
			 * has not an association with a templated value, then a new association is created with the default value. Otherwise, we return a reference to the current location.<p>
			 * <b>IMPORTANT:</b> if we cannot complete operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug
			 * mode. In this case, each forbidden operation will result in a failed assert.
			 * \param s a pointer to the simplex associated to the property value to be updated
			 * \return a reference to the value associated to a simplex managed in the current property
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual T& get(const SimplexPointer& s)
			{
				map<SimplexPointer,unsigned int>::iterator it;
				
				/* First, we check if the required simplex exists! */
				assert(s.type()<=this->dim);
				it=this->keys.find(s);
				if(it==this->keys.end())
				{
					this->values.push_back(this->original);
					this->keys[SimplexPointer(s)]=this->values.size()-1;
					return this->values[this->values.size()-1];
				}
				else { return this->values[it->second]; }
			}
			
			/// This member function retrieves a pointer to the value associated to a simplex managed in the current property.
			/**
			 * This member function retrieves a pointer to the value associated to a simplex managed in the current property: in this way, we can update this value. This class describes a
			 * handle for a <i>sparse</i> property (a normal, a field value, Euclidean coordinates,...) associated to some simplices (without any rule) directly encoded in a data structure,
			 * described by the mangrove_tds::BaseSimplicialComplex class. The T template describes the templated value associated to a simplex and it must supports the I/O operators.<p>If
			 * the input simplex has not an association with a templated value, then a new association is created with the default value and this member function returns <i>false</i>.
			 * Otherwise, we use the current location and we return <i>true</i>.<p><b>IMPORTANT:</b> if we cannot complete operation for any reason, then this member function will fail if
			 * and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param s a pointer to the ghost simplex associated to the property value to be updated
			 * \param value a pointer to the value associated to a ghost simplex managed in the current property
			 * \return <ul><li><i>false</i>, if we create a new association for the input ghost simplex</li><li><i>true</i>, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			virtual bool get(const SimplexPointer &s,T** value)
			{
				map<SimplexPointer,unsigned int>::iterator it;
				
				/* First, we check if the required simplex exists! */
				assert(s.type()<=this->dim);
				it=this->keys.find(s);
				if(it==this->keys.end())
				{
					this->values.push_back(this->original);
					this->keys[SimplexPointer(s)]=this->values.size()-1;
					(*value)=&(this->values[this->values.size()-1]);
					return false;
				}
				else
				{
					(*value)=(T*)(&(this->values[it->second]));
					return true;
				}
			}

			/// This member function retrieves all the pairs stored in the current property.
			/**
			 * This member function retrieves all the pairs stored in the current property, that associates a <i>T</i> templated value to a simplex (arbitrarily choosen, without any rule)
			 * directly encoded in a data structure, described by the mangrove_tds::BaseSimplicialComplex class. The T template describes the templated value associated to a simplex and it
			 * must support the I/O operators.
			 * \param stored all the pairs stored in the current property
			 * \see mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::CompareSimplex
			 */
			inline void getStoredPairs(vector< pair<SimplexPointer,T> >& stored)
			{
				map<SimplexPointer,unsigned int>::const_iterator it;
				
				/* First, we clear 'stored' */
				stored.clear();
				for(it=this->keys.begin();it!=this->keys.end();it++) { stored.push_back( make_pair( SimplexPointer(it->first), T(this->values[it->second]))); }
			}
			
			protected:
			
			/// The default value for the current property.
			T original;
			
			/// The dimension of the current simplicial complex.
			/**
			 * \see mangrove_tds::SIMPLEX_TYPE
			 */
			SIMPLEX_TYPE dim;
			
			/// The map for accessing templated values.
			/**
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::CompareSimplex
			 */
			map<SimplexPointer,unsigned int,CompareSimplex> keys;
			
			/// The templated values for the simplices.
			deque<T> values;
			
			/// This member function creates a new instance of this class.
			inline SparsePropertyHandle() : PropertyBase() { this->clearValues(); }
		};
		
		/// A handle for a ghost property associated to ghost simplices in a simplicial complex.
		/**
		 * This class describes a handle for a <i>ghost</i> property (a normal, a field value, Euclidean coordinates,...) associated to some ghost simplices in a simplicial complex, described by
		 * the mangrove_tds::BaseSimplicialComplex class. Ghost simplices are not directly encoded in the current data structure, assumed to be <i>local</i>, i.e. it encodes only a limited set
		 * of simplices (e.g. the top ones). Such T templated property can be used only with <i>local</i> data structures. The T template describes the templated value associated to a ghost
		 * simplex and it must supports the I/O operators.<p><b>IMPORTANT:</b> you should not directly use this class, since you should manage properties through the member functions
		 * offered by the mangrove_tds::BaseSimplicialComplex class.
		 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::Simplex,
		 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::BaseSimplicialComplex
		 */
		template <class T> class GhostPropertyHandle : public PropertyBase
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a new instance of this class. This class describes a handle for a <i>ghost</i> property (a normal, a field value, Euclidean coordinates,...)
			 * associated to some ghost simplices in a simplicial complex, described by the mangrove_tds::BaseSimplicialComplex class. Ghost simplices are not directly encoded in the current
			 * data structure, assumed to be <i>local</i>, i.e. it encodes only a limited set of simplices (e.g. the top ones). Such T templated property can be used only with <i>local</i>
			 * data structures. The T template describes the templated value associated to a ghost simplex and it must supports the I/O operators.<p><b>IMPORTANT:</b> you should not directly
			 * use this class, since you should manage properties through the member functions offered by the mangrove_tds::BaseSimplicialComplex class.
			 * \param dim the dimension of the simplicial complex to whom the new property will be associated
		 	 * \param name the name of the new templated property
		 	 * \param orig the default value for the new templated property
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline GhostPropertyHandle(SIMPLEX_TYPE dim, string name, T orig = T()) : PropertyBase(name)
			{
				/* We initialize member fields! */
				this->original=orig;
				this->dim=dim;
				this->clearValues();
			}
			
			/// This member function destroys an instance of this class.
			/**
			 * This member function destroys an instance of this class.
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual ~GhostPropertyHandle() { this->clearValues(); }
			
			/// This member function checks if the current property is local.
			/**
			 * This member function checks if the current property is local. We say that a property is <i>local</i> if and only if it is binded only for a particular collection of simplices
			 * (identified by their dimension) belonging to a simplicial complex: this class describes a ghost property, thus the result of this member function is trivial.
			 * \return <ul><li><i>true</i>, if the current property is local</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual bool isLocal() const { return false; }			

			/// This member function checks if the current property is sparse.
			/**
			 * This member function checks if the current property is sparse. We say that a property is <i>sparse</i> if and only if it is binded to some simplices without a particular rule:
			 * this class describes a ghost property, thus the result of this member function is trivial.
			 * \return <ul><li><i>true</i>, if the current property is sparse</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual bool isSparse() const { return false; }

			/// This member function checks if the current property is ghost.
			/**
			 * This member function checks if the current property is ghost. We say that a property is <i>ghost</i> if and only if it is binded to a subset of simplices not directly encoded
			 * in the input data structure: this class describes a ghost property, thus the result of this member function is trivial.
			 * \return <ul><li><i>true</i>, if the current property is ghost</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual bool isGhost() const { return true; }

			/// This member function returns the dimension of the simplicial complex to whom the current property is associated.
			/**
			 * This member function returns the dimension of the simplicial complex to whom the current property is associated. This class describes a handle for a <i>ghost</i> property (a
			 * normal, a field value, Euclidean coordinates,...) associated to some ghost simplices in a simplicial complex, described by the mangrove_tds::BaseSimplicialComplex class. Ghost
			 * simplices are not directly encoded in the current data structure, assumed to be <i>local</i>, i.e. it encodes only a limited set of simplices (e.g. the top ones). Such T
			 * templated property can be used only with <i>local</i> data structures. The T template describes the templated value associated to a ghost simplex and it must supports the I/O
			 * operators.
			 * \return the dimension of the simplicial complex to whom the current property is associated
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual SIMPLEX_TYPE getDimension() const { return this->dim; }
			
			/// This member function is used in order to remove all the values of the current property.
			/**
			 * This member function is used in order to remove all the values of the current property. This class describes a handle for a <i>ghost</i> property (a normal, a field value,
			 * Euclidean coordinates,...) associated to some ghost simplices in a simplicial complex, described by the mangrove_tds::BaseSimplicialComplex class. Ghost simplices are not
			 * directly encoded in the current data structure, assumed to be <i>local</i>, i.e. it encodes only a limited set of simplices (e.g. the top ones). Such T templated property can
			 * be used only with <i>local</i> data structures. The T template describes the templated value associated to a ghost simplex and it must supports the I/O operators.
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle,
		 	 * mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE,
		 	 *  mangrove_tds::BaseSimplicialComplex
			 */
		 	inline virtual void clearValues()
		 	{
		 		this->keys.clear();
		 		this->values.clear();
		 	}
			
			/// This member function provides a debug representation of the current property on an output stream.
		 	/**
		 	 * This member function provides a debug representation of the current property on an output stream
		 	 * \param os the output stream where we can write a debug representation of the current property
		 	 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::Simplex,
		 	 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
		 	 */
		 	inline virtual void debug(ostream &os = cout) const
			{
				map<GhostSimplex,unsigned int>::const_iterator it;
				
				/* We write a debug representation! */
				os<<"Ghost property associated to a simplicial "<<this->getDimension()<<"-complex"<<endl;
				os<<"The property name: "<<this->getName()<<endl;
				os<<"The default value: "<<this->original<<endl;
				os<<"There are "<<this->values.size()<<" associations among ghost simplices and values";
				for(it=this->keys.begin();it!=this->keys.end();it++)
				{
					os<<endl;
					it->first.debug(os);
					os<<"ASSOCIATED TO VALUE "<<this->values[it->second]<<endl<<"========================"<<endl;
					os.flush();
				}

				/* If we arrive here, we have finished */
				os<<endl;
				os.flush();
			}
			
			/// This member function returns the number of values in the current property.
			/**
			 * This member function returns the number of values in the current property. This class describes a handle for a <i>ghost</i> property (a normal, a field value, Euclidean
			 * coordinates,...) associated to some ghost simplices in a simplicial complex, described by the mangrove_tds::BaseSimplicialComplex class. Ghost simplices are not directly
			 * encoded in the current data structure, assumed to be <i>local</i>, i.e. it encodes only a limited set of simplices (e.g. the top ones). Such T templated property can be used
			 * only with <i>local</i> data structures. The T template describes the templated value associated to a ghost simplex and it must supports the I/O operators.
			 * \return the number of values in the current property
		 	 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::Simplex,
		 	 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual unsigned int size() const { return this->values.size(); }
			
			/// This member function returns the number of values for a particular class of ghost simplices associated to the current property.
			/**
			 * This member function returns the number of values for a particular class of simplices associated to the current property. This class describes a handle for a <i>ghost</i>
			 * property (a normal, a field value, Euclidean coordinates,...) associated to some ghost simplices in a simplicial complex, described by the mangrove_tds::BaseSimplicialComplex
			 * class. Ghost simplices are not directly encoded in the current data structure, assumed to be <i>local</i>, i.e. it encodes only a limited set of simplices (e.g. the top ones).
			 * Such T templated property can be used only with <i>local</i> data structures. The T template describes the templated value associated to a ghost simplex and it must supports
			 * the I/O operators.<p><b>IMPORTANT:</b> if we cannot complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i>
			 * is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param i the dimension of the required simplices
			 * \return the number of values for a particular class of ghost simplices associated to the current property
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual unsigned int size(SIMPLEX_TYPE i) const
			{
				map<GhostSimplex,unsigned int>::const_iterator it;
				unsigned int ci;
				
				/* First, we check if all is ok and the game can start! */
				assert(i<=dim);
				ci= 0;
				for(it=this->keys.begin();it!=this->keys.end();it++) { if(it->first.getCGhostSimplexPointer().getChildType()==i) ci=ci+1; }
				return ci;
			}
			
			/// This member function is used in order to synchronize the number of values in the current property.
			/**
			 * This member function is used in order to synchronize the number of values in the current property. This class describes a handle for a <i>ghost</i> property (a normal, a field
			 * value, Euclidean coordinates,...) associated to some ghost simplices in a simplicial complex, described by the mangrove_tds::BaseSimplicialComplex class. Ghost simplices are
			 * not directly encoded in the current data structure, assumed to be <i>local</i>, i.e. it encodes only a limited set of simplices (e.g. the top ones). Such T templated property
			 * can be used only with <i>local</i> data structures. The T template describes the templated value associated to a ghost simplex and it must supports the I/O operators.<p>Thus,
			 * this member function cannot be applied since we have not some space to reallocate.<p><b>IMPORTANT:</b> this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param len the new number of allocated locations in the reference collection of simplices 
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex,
			 * mangrove_tds::idle()
			 */
			inline virtual void resize(unsigned int len) 
			{
				idle(len);
				assert(false);
			}
			
			/// This member function is used in order to synchronize the ghost simplices number of a particular dimension in the reference simplicial complex.
			/**
			 * This member function is used in order to synchronize the simplices number of a particular dimension in the reference simplicial complex. This class describes a handle for a
			 * <i>ghost</i> property (a normal, a field value, Euclidean coordinates,...) associated to some ghost simplices in a simplicial complex, described by the
			 * mangrove_tds::BaseSimplicialComplex class. Ghost simplices are not directly encoded in the current data structure, assumed to be <i>local</i>, i.e. it encodes only a limited
			 * set of simplices (e.g. the top ones). Such T templated property can be used only with <i>local</i> data structures. The T template describes the templated value associated to
			 * a ghost simplex and it must supports the I/O operators.<p>Thus, this member function cannot be applied since we have not some space to reallocate.<p><b>IMPORTANT:</b> this
			 * member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param i the required dimension of simplices in the reference simplicial complex
			 * \param len the new number of values for the required property associated to a particular class of simplices in the reference simplicial complex
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex,
			 * mangrove_tds::idle()
			 */
			inline virtual void resize(SIMPLEX_TYPE i, unsigned int len) 
			{ 
				idle(len);
				idle(i);
				assert(false);
			}
			
			/// This member function updates the value associated to a ghost simplex managed in the current property.
			/**
			 * This member function updates the value associated to a ghost simplex managed in the current property. This class describes a handle for a <i>ghost</i> property (a normal, a
			 * field value, Euclidean coordinates,...) associated to some ghost simplices in a simplicial complex, described by the mangrove_tds::BaseSimplicialComplex class. Ghost simplices
			 * are not directly encoded in the current data structure, assumed to be <i>local</i>, i.e. it encodes only a limited set of simplices (e.g. the top ones). Such T templated
			 * property can be used only with <i>local</i> data structures. The T template describes the templated value associated to a ghost simplex and it must supports the I/O
			 * operators.<p>If the input ghost simplex has not an association with a templated value, then a new association is created with the default value.<p><b>IMPORTANT:</b> if we
			 * cannot complete operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each
			 * forbidden operation will result in a failed assert.
			 * \param s a pointer to the ghost simplex associated to the property value to be updated
			 * \param value the new value for the simplex to be updated
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual void set(const GhostSimplex &s, T value)
			{
				map<GhostSimplex,unsigned int>::iterator it;
				
				/* First, we check if the required simplex exists! */
				assert(s.getCGhostSimplexPointer().getChildType()<=this->dim);
				it=this->keys.find(s);
				if(it==this->keys.end())
				{
					this->values.push_back(value);
					this->keys[GhostSimplex(s)]=this->values.size()-1;
				}
				else { this->values[it->second]=value; }
			}

			/// This member function retrieves a reference to the value associated to a ghost simplex managed in the current property.
			/**
			 * This member function retrieves a reference to the value associated to a ghost simplex managed in the current property: in this way, we can update this value. This class
			 * describes a handle for a <i>ghost</i> property (a normal, a field value, Euclidean coordinates,...) associated to some ghost simplices in a simplicial complex, described by the
			 * mangrove_tds::BaseSimplicialComplex class. Ghost simplices are not directly encoded in the current data structure, assumed to be <i>local</i>, i.e. it encodes only a limited
			 * set of simplices (e.g. the top ones). Such T templated property can be used only with <i>local</i> data structures. The T template describes the templated value associated to
			 * a ghost simplex and it must supports the I/O operators.<p>If the input ghost simplex has not an association with a templated value, then a new association is created with the
			 * default value. Otherwise, we return a reference to the current location.<p><b>IMPORTANT:</b> if we cannot complete operation for any reason, then this member function will
			 * fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param s a pointer to the ghost simplex associated to the property value to be updated
			 * \return a reference to the value associated to a ghost simplex managed in the current property
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex 
			 */
			inline virtual T& get(const GhostSimplex &s)
			{
				map<GhostSimplex,unsigned int>::iterator it;
				
				/* First, we check if the required simplex exists! */
				assert(s.getCGhostSimplexPointer().getChildType()<=this->dim);
				it=this->keys.find(s);
				if(it==this->keys.end())
				{
					this->values.push_back(this->original);
					this->keys[GhostSimplex(s)]=this->values.size()-1;
					return this->values[this->values.size()-1];
				}
				else { return this->values[it->second]; }
			}

			/// This member function retrieves a pointer to the value associated to a ghost simplex managed in the current property.
			/**
			 * This member function retrieves a pointer to the value associated to a ghost simplex managed in the current property: in this way, we can update this value. This class
			 * describes a handle for a <i>ghost</i> property (a normal, a field value, Euclidean coordinates,...) associated to some ghost simplices in a simplicial complex, described by the
			 * mangrove_tds::BaseSimplicialComplex class. Ghost simplices are not directly encoded in the current data structure, assumed to be <i>local</i>, i.e. it encodes only a limited
			 * set of simplices (e.g. the top ones). Such T templated property can be used only with <i>local</i> data structures. The T template describes the templated value associated to
			 * a ghost simplex and it must supports the I/O operators.<p>If the input ghost simplex has not an association with a templated value, then a new association is created with the
			 * default value and this member function returns <i>false</i>. Otherwise, we use the current location and we return <i>true</i>.<p><b>IMPORTANT:</b> if we cannot complete
			 * operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation
			 * will result in a failed assert.
			 * \param s a pointer to the ghost simplex associated to the property value to be updated
			 * \param value a pointer to the value associated to a ghost simplex managed in the current property
			 * \return <ul><li><i>false</i>, if we create a new association for the input ghost simplex</li><li><i>true</i>, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::SparsePropertyHandle, mangrove_tds::Simplex,
			 * mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
			 */
			inline virtual bool get(const GhostSimplex &s,T** value)
			{
				map<GhostSimplex,unsigned int>::iterator it;
				
				/* First, we check if the required simplex exists! */
				assert(s.getCGhostSimplexPointer().getChildType()<=this->dim);
				it=this->keys.find(s);
				if(it==this->keys.end())
				{
					this->values.push_back(this->original);
					this->keys[GhostSimplex(s)]=this->values.size()-1;
					(*value)=&(this->values[this->values.size()-1]);
					return false;
				}
				else
				{
					(*value)=(T*)(&(this->values[it->second]));
					return true;
				}
			}

			/// This member function retrieves all the pairs stored in the current property.
			/**
			 * This member function retrieves all the pairs stored in the current property, that associates a <i>T</i> templated value to a ghost simplex, i.e. to a simplex not directly
			 * encoded in a data structure (namely an instance of the mangrove_tds::BaseSimplicialComplex class). Ghost simplices are not directly encoded in the current data structure,
			 * assumed to be <i>local</i>, i.e. it encodes only a limited set of simplices (e.g. the top ones). Such T templated property can be used only with <i>local</i> data structures.
			 * The T template describes the templated value associated to a ghost simplex and it must supports the I/O operators.
			 * \param stored all the pairs stored in the current property
			 * \see mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::CompareSimplex
			 */
			inline void getStoredPairs(vector< pair<GhostSimplex,T> >& stored)
			{
				map<GhostSimplex,unsigned int>::const_iterator it;
				
				/* First, we clear 'stored' */
				stored.clear();
				for(it=this->keys.begin();it!=this->keys.end();it++) { stored.push_back( make_pair( GhostSimplex(it->first), T(this->values[it->second]))); }
			}

			protected:
			
			/// The default value for the current property.
			T original;
			
			/// The dimension of the current simplicial complex
			/**
			 * \see mangrove_tds::SIMPLEX_TYPE
			 */
			SIMPLEX_TYPE dim;
			
			/// The map for accessing templated values.
			/**
			 * \see mangrove_tds::GhostSimplex, mangrove_tds::CompareSimplex
			 */
			map<GhostSimplex,unsigned int,CompareSimplex> keys;
			
			/// The templated values for the ghost simplices.
			deque<T> values;
			
			/// This member function creates a new instance of this class.
			inline GhostPropertyHandle() : PropertyBase() { this->clearValues(); }
		};
	}
	
#endif

