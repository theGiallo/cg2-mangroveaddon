/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library                                    
 *                                                                           				    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * November 2011 (Revised on May 2012)
 *                                                                    
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                      
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * IO.h - components for reading/writing a simplicial complex from/on files.
 ***********************************************************************************************/
 
/* Should we include this header file? */
#ifndef IO_H

	#define IO_H
	
	#include "SimplicesContainer.h"
	#include "Point.h"
	#include "Miscellanea.h"
	#include "BaseSimplicialComplex.h"
	#include <iostream>
	#include <fstream>
	#include <vector>
	#include <string>
	#include <assert.h>
	#include <iomanip>
	using namespace mangrove_tds;
	using namespace std;
	
	/// Components for I/O between simplicial complexes and files.
	/**
	 * The classes defined in this file describe some components for I/O between simplicial complexes and files: in other words, we can build a simplicial complex (described by the
	 * mangrove_tds::BaseSimplicialComplex class) by reading it from a file, that usually contains a soup of top simplices. Moreover, we can write a simplicial complex, by writing its
	 * representation in accordance with many standard I/O formats.
	 * \file IO.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	
	/* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		/// Container for mesh components.
		/**
	 	 * This class describes a component in a simplicial mesh, composed by a local list of vertices (described through an instance of the mangrove_tds::Point class, i.e. their Euclidean
	 	 * coordinates, or by their scalar values) and by a local list of top simplices, described through local indices of their vertices. We can use instances of this component for creating a
	 	 * complete list of vertices and top simplices in the input simplicial mesh.
	 	 * \see mangrove_tds::DataManager, mangrove_tds::Point, mangrove_tds::RawFace
	 	 */
	 	class MeshComponent
		{
			public:
			
			/// This member function creates an instance of this class.
			/**
			 * This member function creates a new instance of this class. It is not initialized and it must be initialized through the mangrove_tds::MeshComponent::addPoint(),
			 * mangrove_tds::MeshComponent::addSimplex() and mangrove_tds::MeshComponent::addFieldValue() member functions.
			 * \see mangrove_tds::Point, mangrove_tds::MeshComponent::addPoint(), mangrove_tds::MeshComponent::addFieldValue(), mangrove_tds::MeshComponent::addSimplex()
			 */
			inline MeshComponent()
			{
				/* We initialize the internal state of the current class. */
				this->points.clear();
				this->simplices.clear();
				this->fields.clear();
			}
			
			/// This member function destroys an instance of this class.
			inline virtual ~MeshComponent()
			{
				/* We destroy the internal state! */
				this->points.clear();
				for(unsigned int k=0;k<this->simplices.size();k++) this->simplices[k].clear();
				this->simplices.clear();
				this->fields.clear();
			}
			
			/// This member function returns the local list of vertices inside the current component.
			/**
			 * This member function returns the local list of vertices inside the current component: the vertices are stored in the same order they appear in the input file through the
			 * mangrove_tds::MeshComponent::addPoint() member function.
			 * \return the local list of vertices inside the current component
			 * \see mangrove_tds::Point, mangrove_tds::RawFace, mangrove_tds::MeshComponent::addPoint()
			 */
			inline vector< Point<double> > & getPoints() { return this->points; }
			
			/// This member function adds a new vertex to the local list of vertices inside the current component.
			/**
			 * This member function adds a new vertex to the local list of vertices inside the current component: the vertices are stored in the same order they appear in the input file and
			 * they can be retrieved through the mangrove_tds::MeshComponent::getPoints() member function.
			 * \param x the Euclidean x coordinate of the new point to be added
			 * \param y the Euclidean y coordinate of the new point to be added
			 * \param z the Euclidean z coordinate of the new point to be added
			 * \see mangrove_tds::Point, mangrove_tds::RawFace, mangrove_tds::MeshComponent::getPoints()
			 */
			inline void addPoint( double x, double y, double z) { this->points.push_back( Point<double>(x,y,z)); }
			
			/// This member function returns the local list of simplices inside the current component.
			/**
			 * This member function returns the local list of simplices inside the current components: the simplices are stored in the same order they appear in the input file through the
			 * mangrove_tds::MeshComponent::addSimplex() member function and they are described by a tuple of indices over the local list of vertices. Moreover, they can be used for
			 * generating  the set of raw faces, described by the mangrove_tds::RawFace class.
			 * \return the local list of simplices inside the current component.
			 * \see mangrove_tds::RawFace, mangrove_tds::MeshComponent::addSimplex()
			 */
			inline vector< vector<unsigned int> > & getSimplices() { return this->simplices; }
			
			/// This member function adds a new simplex to the local list of simplices inside the current component.
			/**
			 * This member function adds a new simplex to the local list of simplices inside the current component: the simplices are stored in the same order they appear in the input file
			 * and they are described by a tuple of indices over the local list of vertices. Moreover, they can be used for generating the set of raw faces, described by the
			 * mangrove_tds::RawFace class and they can be retrieved through the mangrove_tds::MeshComponent::getSimplices() member function.
			 * \param s the new tuple of local indices describing the simplex to be added
			 * \see mangrove_tds::RawFace, mangrove_tds::MeshComponent::getSimplices()
			 */
			inline void addSimplex(vector<unsigned int> &s) { this->simplices.push_back(vector<unsigned int>(s)); }
			
			/// This member function adds a new field value associated to a vertex in the local list of vertices inside the current component.
			/**
			 * This member function adds a new field value associated to a vertex in the local list of vertices inside the current component.<p>Vertices are stored in the same order they
			 * appear in the input file and they can be retrieved through the mangrove_tds::MeshComponent::getPoints() member function. The association between vertex and field value is by
			 * position.
			 * \param f the new field value to be associated to a vertex in the local list of vertices inside the current component
			 * \see mangrove_tds::RawFace, mangrove_tds::MeshComponent::getFieldValues(), mangrove_tds::MeshComponent::getPoints()
			 */
			inline void addFieldValue(double f) { this->fields.push_back(f); }
			
			/// This member function returns field values associated to vertices in the local list of vertices inside the current component.
			/**
			 * This member function returns field values associated to vertices in the local list of vertices inside the current component.<p>Vertices are stored in the same order they appear
			 * in the input file and they can be retrieved through the mangrove_tds::MeshComponent::getPoints() member function. The association between vertex and field value is by position.
			 * \return a reference to field values associated to vertices in the local list of vertices inside the current component.
			 * \see mangrove_tds::RawFace, mangrove_tds::MeshComponent::addFieldValue()
			 */
			inline vector<double> & getFieldValues() { return this->fields; }
			
			/// This operator provides a representation of a simplicial mesh component on an output stream.
			/**
			 * This operator provides a representation of a simplicial mesh component on an output stream. We provide information about the vertices and simplices belonging to the input
			 * component.<p>This component contains a local list of vertices inside the current component: vertices are stored in the same order they appear in the input file. Moreover, it
			 * contains a local list of simplices, stored in the same order they appear in the input file and they are described by a tuple of indices over the local list of vertices.
			 * \param os the output stream where we can write the required representation of the input component
			 * \param comp the simplicial mesh component to be written
			 * \return the output stream after having written the representation of the input component
			 * \see mangrove_tds::RawFace, mangrove_tds::Point
			 */
			friend ostream& operator<<(ostream& os,MeshComponent& comp)
			{
				double x,y,z;
				
				/* We write a representation of the current component! */
				os<<"\t Simplicial mesh component"<<endl<<endl;
				os<<"\t Number of vertices: "<<comp.points.size()<<endl<<endl;
				os<<"\t Vertices: "<<endl<<endl;
				for(unsigned int k=0;k<comp.points.size();k++)
				{
					/* Now, we write the coordinates of the k-th point! */
					x = comp.points[k].x();
					y = comp.points[k].y();
					z = comp.points[k].z();
					os<<"\t\t";
					os<<setprecision(9)<<x;
					os<<" ";
					os<<setprecision(9)<<y;
					os<<" ";
					os<<setprecision(9)<<z;
					os<<endl;				
				}
				
				/* Now, we can write all field values! */
				os<<"\t Field values: "<<endl<<endl;
				for(unsigned int k=0;k<comp.fields.size();k++)
				{
					os<<"\t\t";
					os<<setprecision(9)<<comp.fields[k];
					os<<endl;
				}

				/* Now, we can write all the simplices! */
				os<<endl<<"\t Number of simplices: "<<comp.simplices.size()<<endl<<endl;
				os<<"\t Simplices: "<<endl<<endl;
				for(unsigned int k=0;k<comp.simplices.size();k++)
				{
					/* Now, we can write the k-th simplex! */
					os<<"\t\t";
					for(unsigned int j=0;j<comp.simplices[k].size();j++) os<<comp.simplices[k].at(j)<<" ";
					os<<endl;
				} 

				/* If we arrive here, we have finished! */
				os<<endl;
				os.flush();
				return os;
			}
			
			protected:
			
			/// The local list vertices inside the current component.
			/**
			 * \see mangrove_tds::Point
			 */
			vector< Point<double> > points;

			/// The local list of simplices inside the current component.
			vector< vector<unsigned int> > simplices;
			
			/// Field values associated to the local list of vertices inside the current component.
			vector<double> fields;
		};
		
		/// A generic manager for the content of a file.
		/**
		 * This class describes a parser for the content of a generic file, written in accordance with a specific format. This class extracts the main information stored in the input file and it
		 * must
		 * be used inside a mangrove_tds::IO class (and its subclasses) in order to create a new instance of the mangrove_tds::BaseSimplicialComplex (in particular, some specializations like
		 * those described by the mangrove_tds::IG, the mangrove_tds::IS, the mangrove_tds::SIG, and the mangrove_tds::GIA, the mangrove_tds::TriangleSegment and the
		 * mangrove_tds::NMIA classes). Moreover, it exports the content of a simplicial complex in accordance witht the specific format managed by the current component.<p>The design of
		 * this class is based on the <A href=http://en.wikipedia.org/wiki/Curiously_recurring_template_pattern>Curiously Recurring Template Pattern</A> in order to implement
		 * polymorphism: in this way, we can reuse some parts and provide only the member functions specialized for a particular file format.<p><b>IMPORTANT:</b> in particular, this data
		 * structure works if we associate directly to the new input simplicial complex only the Euclidean coordinates and the field values, if they are supported by the specific file format.
		 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::SimplicesContainer, mangrove_tds::RawFace, mangrove_tds::IO, mangrove_tds::IG, mangrove_tds::IS,
		 * mangrove_tds::SIG, mangrove_tds::GIA, mangrove_tds::TriangleSegment, mangrove_tds::NMIA
		 */
		template <class Derived> class DataManager
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a new instance of this class, without initializing it. You should invoke the mangrove_tds::DataManager::loadData() for loading data written in
			 * accordance with the format managed by this specific component.
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::RawFace, mangrove_tds::DataManager::loadData()
			 */
			inline DataManager()
			{
				/* We initialize the internal state */
				this->raw_faces.clear();
				this->components.clear();
				this->simplices.clear();
				this->raw_faces.resize(1);
			}
			
			/// This member function destroys an instance of this class.
			/**
			 * This member function destroys an instance of this class, deallocating its internal state.
			 * \see mangrove_tds::SimplicesContainer::clearCollection(), mangrove_tds::SimplexPointer, mangrove_tds::RawFace
			 */
			inline virtual ~DataManager()
			{
				/* We release space! */
				if(this->in.is_open()) this->in.close();
				if(this->out.is_open()) this->out.close();
				for(unsigned int k=0;k<this->raw_faces.size();k++) { this->raw_faces[k].clear(); }
				this->raw_faces.clear();
				this->components.clear();
				for(unsigned int k=0;k<this->simplices.size();k++) { this->simplices[k].clearCollection(); }
				this->simplices.clear();
			}
			
			/// This member function returns the dimension of the simplicial complex described in the current file.
			/**
			 * This member function returns the dimension of the simplicial complex described in the current file.
			 * \return the dimension of the simplicial complex described in the current file
			 * \see mangrove_tds::SIMPLEX_TYPE
			 */
			inline unsigned int getDimension()
			{
				/* We extract the dimension from the 'simplices' vector! */
				if(this->simplices.size()==0) return 0;
				else return (this->simplices.size()-1);
			}
			
			/// This member function returns a reference to a collection of simplices.
			/**
			 * This member function returns a reference to a simplices collection in the simplicial complex, identified by their dimension.<p>If the required collection of simplices does not
			 * exist, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a
			 * failed assert.
			 * \param t the dimension of the required simplices collection
			 * \return a reference to the required collection of simplices.
			 * \see mangrove_tds::DataManager::loadData(), mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplicesContainer
			 */
			inline SimplicesContainer& getCollection(SIMPLEX_TYPE t)
			{
				/* We extract the t-th collection, if it exists! */
				assert(t<this->simplices.size());
				return this->simplices[t];
			}
			
			/// This member function returns a reference to all the raw descriptions for all the faces of top simplices.
			/**
			 * This member function returns a reference to all the raw descriptions for all the top simplices subfaces: such subsimplices are described by an instance of the
			 * mangrove_tds::RawFace class. Such particular descriptions could be very useful when we must generate all the simplices during the construction of a simplicial complex in the
			 * mangrove_tds::IO::createComplex() member function.
			 * \return a reference to the raw descriptions for all the raw simplices
			 * \see mangrove_tds::RawFace, mangrove_tds::IO::createComplex(), mangrove_tds::DataManager::generateRawSimplices()
			 */
			inline vector< vector<RawFace> >& getRawFaces() { return this->raw_faces; }
			
			/// This member function allows to customize the property name containing the Euclidean coordinates in the simplicial complex to be built.
			/**
			 * This member function allows to customize the property name containing the Euclidean coordinates for all vertices in the simplicial complex to be built.<p>Each subclass of the
			 * mangrove_tds::DataManager class could support the Euclidean coordinates for all the vertices: if this condition is not satisfied, then this member function will fail if and
			 * only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \return the property name containing the Euclidean coordinates in the simplicial complex to be built
			 * \see mangrove_tds::PropertyBase, mangrove_tds::Point, mangrove_tds::DataManager::supportsEuclideanCoordinates(), mangrove_tds::DataManager::getCoordinates()
			 */
			inline string getPropertyName4Coordinates() { return static_cast<Derived*>(this)->getPropertyName4Coordinates(); }
			
			/// This member function checks if the loading of the Euclidean coordinates is supported and enabled in the current component.
			/**
			 * This member function checks if the loading of the Euclidean coordinates is supported by the current component, since each subclass of the mangrove_tds::DataManager class could
			 * store the Euclidean coordinates all the vertices.
			 * \return <ul><li><i>true</i>, if the current component supports the loading of the Euclidean coordinates for all the vertices.</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::Point, mangrove_tds::PropertyBase, mangrove_tds::DataManager::getPropertyName4Coordinates(), mangrove_tds::DataManager::getCoordinates()
			 */
			inline bool supportsEuclideanCoordinates() { return static_cast<Derived*>(this)->supportsEuclideanCoordinates(); }
			
			/// This member function returns the Euclidean coordinates associated to a vertex in the simplicial complex to be built.
			/**
			 * This member function returns the Euclidean coordinates associated to a vertex in the simplicial complex to be built.<p>If the required vertex does not exist or if the Euclidean
			 * coordinates are not supported in the current file format, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this
			 * case, each forbidden operation will result in a failed assert.
			 * \param i the identifier of the required vertex
			 * \param p the Euclidean coordinates of the required vertex
			 * \see mangrove_tds::Point, mangrove_tds::PropertyBase, mangrove_tds::DataManager::getPropertyName4Coordinates(),
			 * mangrove_tds::DataManager::supportsEuclideanCoordinates(), mangrove_tds::SIMPLEX_ID
			 */
			inline void getCoordinates(SIMPLEX_ID i,Point<double> & p) { static_cast<Derived*>(this)->getCoordinates(i,p); }
			
			/// This member function allows to customize the property name containing the field values in the simplicial complex to be built.
			/**
			 * This member function allows to customize the property name containing the field values for all vertices in the simplicial complex to be built.<p>Each subclass of the
			 * mangrove_tds::DataManager class could support the field values for all the vertices: if this condition is not satisfied, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \return the property name containing the field values in the simplicial complex to be built
			 * \see mangrove_tds::PropertyBase, mangrove_tds::DataManager::supportsFieldValues(), mangrove_tds::DataManager::getFieldValue()
			 */
			inline string getPropertyName4FieldValue() { return static_cast<Derived*>(this)->getPropertyName4FieldValue(); }
			
			/// This member function checks if the loading of the field values is supported by the current component.
			/**
			 * This member function checks if the loading of the field values is supported by the current component, since each subclass of the mangrove_tds::DataManager class could store a
			 * field value for all the vertices.
			 * \return <ul><li><i>true</i>, if the current component supports the loading of the field values for all the vertices.</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::PropertyBase, mangrove_tds::DataManager::getPropertyName4FieldValue(), mangrove_tds::DataManager::getFieldValue()
			 */
			inline bool supportsFieldValues() { return static_cast<Derived*>(this)->supportsFieldValues(); }
			
			/// This member function returns the field value associated to a vertex in the current simplicial complex.
			/**
			 * This member function returns the field value associated to a vertex in the simplicial complex to be built.<p>If the required vertex does not exist or if the field values are not
			 * supported in the current file format, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.
			 * \param i the identifier of the required vertex
			 * \param f the field value of the required vertex
			 * \see mangrove_tds::DataManager::getPropertyName4FieldValue(), mangrove_tds::DataManager::supportsFieldValues(), mangrove_tds::SIMPLEX_ID
			 */
			inline void getFieldValue(SIMPLEX_ID i, double &f) { static_cast<Derived*>(this)->getFieldValue(i,f); }
			
			/// This member function parses the content of a file.
			/**
			 * This member function parses the content of a file: the mangrove_tds::DataManager class describes a parser for the content of a generic file, written in accordance with a
			 * specific format. The input file usually contains a soup of simplices, thus generating all the intermediate simplices could be mandatory.<p>In the subclasses of the
			 * mangrove_tds::DataManager class some further conditions could be checked and verified: if they are not satisfied, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param fname the path of the input file
			 * \param reqParams this boolean flag identfies if the loading of the auxiliary parameters is required
			 * \param gd this boolean flag indicates if the effective generation of raw simplices must be performed
			 * \param checksOn this boolean flag indicates if the effective checks on the input data must be performed
			 * \see mangrove_tds::Simplex, mangrove_tds::SimplicesContainer, mangrove_tds::DataManager::generateRawSimplices()
			 */
			inline void loadData(string fname,bool reqParams,bool gd,bool checksOn) { static_cast<Derived*>(this)->loadData(fname,reqParams,gd,checksOn); }
			
			/// This member function exports a simplicial complex in accordance with a certain format.
			/**
			 * This member function exports a simplicial complex, by writing its representation in accordance with the specific file format managed by the current component.<p>In the
			 * subclasses of the mangrove_tds::DataManager class some further conditions could be checked and verified: if they are not satisfied, then this member function will fail if and
			 * only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param ccw the input simplicial complex to be written
			 * \param fname the path of the output file
			 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::SimplicesContainer
			 */
			template <class T> void writeData(BaseSimplicialComplex<T> *ccw,string fname) { static_cast<Derived*>(this)->writeData(ccw,fname); }
			
			/// This member function exports a subcomplex in accordance with a certain format.
			/**
			 * This member function exports a subcomplex, by writing its representation in accordance with the specific file format managed by the input component. Here, we consider a subset
			 * of simplices directly encoded in the input data structure.<p><b>IMPORTANT:</b> in the subclasses of the mangrove_tds::DataManager class some further conditions could be
			 * checked and verified: if they are not satisfied, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each
			 * forbidden operation will result in a failed assert.
			 * \param tops the list of simplices to be considered top in the new subcomplex to be written
			 * \param ccw the reference simplicial complex
			 * \param fname the path of the output file
			 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices()
			 */
			template <class T> void writeSubcomplex( vector<SimplexPointer> &tops, BaseSimplicialComplex<T> *ccw, string fname)
				{ static_cast<Derived*>(this)->writeSubcomplex(tops,ccw,fname); }
			
			/// This member function exports a subcomplex in accordance with a certain format.
			/**
			 * This member function exports a subcomplex, by writing its representation in accordance with the specific file format managed by the input component. Here, we consider a subset
			 * of ghost simplices not directly encoded in the input data structure.<p><b>IMPORTANT:</b> in the subclasses of the mangrove_tds::DataManager class some further conditions could
			 * be checked and verified: if they are not satisfied, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case,
			 * each forbidden operation will result in a failed assert.
			 * \param tops the list of ghost simplices to be considered top in the new subcomplex to be written
			 * \param ccw the reference simplicial complex
			 * \param fname the path of the output file
			 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices()
			 */
			template <class T> void writeSubcomplex( vector<GhostSimplexPointer> &tops, BaseSimplicialComplex<T> *ccw, string fname)
				{ static_cast<Derived*>(this)->writeSubcomplex(tops,ccw,fname); }
			
			/// This member function writes a debug representation of the simplices directly stored in the input file.
			/**
			 * This member function writes a debug representation of the simplices directly stored in the input file, plus information Euclidean coordinates and field values, if they are
			 * supported.
			 * \param os the output stream where we must write a debug representation of the simplices directly stored in the input file
			 * \see mangrove_tds::Point, mangrove_tds::SimplexIterator, mangrove_tds::SimplicesContainer::debug(), mangrove_tds::DataManager::supportsFieldValues(),
			 * mangrove_tds::DataManager::getFieldValue(), mangrove_tds::DataManager::supportsEuclideanCoordinates(), mangrove_tds::DataManager::getCoordinates()
			 */
			inline void debug(ostream& os=cout)
			{
				unsigned int k;
				SimplexIterator it;
				double d;
				Point<double> p;
				
				/* We write some debug information about the simplices! */
				if( this->simplices.size()==0) { os<<"\t We are managing a not initialized I/O component"<<endl; }
				else
				{
					/* First, we write something about the vertices! */
					k = 0;
					os<<"\tSimplices of dimension 0"<<endl;
					os<<"\t=================================="<<endl<<endl;
					os.flush();
					for(it=this->simplices[0].begin();it!=this->simplices[0].end();it++)
					{
						/* Now, we write information about the k-th vertex and the Euclidean coordinates and the field value, if supported! */
						(*it).debug();
						os<<"\tEuclidean coordinates: ";
						if(this->supportsEuclideanCoordinates()==true)
						{
							this->getCoordinates(k,p);
							for(unsigned int j=0;j<p.size();j++) os<<p.at(j)<<" ";
						}
						else { os<<"not supported"; }
						os<<endl<<endl;
						os.flush();
						os<<"\tField value: ";
						if(this->supportsFieldValues()==true)
						{ 
							this->getFieldValue(k,d);
							os<<d;
						}
						else { os<<"not supported"; }
						os<<endl<<endl;
						os.flush();
						k=k+1;
					}
					
					/* Now, we write information about the other simplices! */
					for(k=1;k<this->simplices.size();k++)
					{
						os<<"\tSimplices of dimension "<<k<<endl;
						os<<"================================="<<endl<<endl;
						os.flush();
						for(it=this->simplices[k].begin();it!=this->simplices[k].end();it++) { (*it).debug(); }
					}
				}
				
				/* If we arrive here, we have finished! */
				os<<endl;
				os.flush();
			}
			
			protected:
			
			/// The input file from which we must read the input simplicial complex.
			ifstream in;
			
			/// The output file where we must write a representation of the input simplicial complex.
			ofstream out;
			
			/// The components in the current simplicial complex.
			/**
			 * \see mangrove_tds::MeshComponent
			 */
			vector<MeshComponent> components;
			
			/// The raw descriptions of the subfaces of the top-simplices.
			/**
			 * \see mangrove_tds::RawFace
			 */
			vector< vector<RawFace> > raw_faces;
			
			/// The top simplices of the simplicial complex stored in the input file.
			/**
			 * \see mangrove_tds::SimplicesContainer
			 */
			vector<SimplicesContainer> simplices;
			
			/// This member function generates all the raw subsimplices, starting by a top simplex boundary.
			/**
			 * This member function generates all the raw subsimplices of a top simplex: such subsimplices are described through the mangrove_tds::RawFace class. They could be very useful
			 * when we must generate all simplices during the construction of a simplicial complex in the mangrove_tds::IO::createComplex() member function.
			 * \param ids the indices of the simplices on the required top simplex boundary
			 * \param lt the unique index of the top simplex
			 * \param flag this boolean flag identifies if the new face is a direct face of a top simplex
			 * \see mangrove_tds::RawFace, mangrove_tds::IO::createComplex(), mangrove_tds::SIMPLEX_ID, mangrove_tds::DataManager::getRawFaces()
			 */
			inline void generateRawSimplices( vector<unsigned int> &ids, SIMPLEX_ID lt,bool flag)
			{
				vector<unsigned int> f;
				unsigned int lg;

				/* Now, we generate the subsimplices and exit! */
				if(ids.size()>2)
				{
					for(unsigned int j=0;j<ids.size();j++)
					{
						/* Now, we generate a new subsimplex and then we update the space! */
						findProjection(j,ids,f);
						if(f.size()>this->raw_faces.size())
						{
							lg = this->raw_faces.size();
							this->raw_faces.resize(f.size());
						}		
						
						/* Now, we generate a new raw face! */
						this->raw_faces[f.size()-1].push_back(RawFace());
						for(unsigned int i=0;i<f.size();i++) this->raw_faces[f.size()-1].rbegin()->getVertices().push_back(f[i]);
						this->raw_faces[f.size()-1].rbegin()->getReverseIndex() = j;
						this->raw_faces[f.size()-1].rbegin()->getIndex() = lt;
						this->raw_faces[f.size()-1].rbegin()->isTopSimplexChild() = flag;
						this->generateRawSimplices(f,this->raw_faces[f.size()-1].size()-1,false);
						if(flag==false) { this->raw_faces[ids.size()-1][lt].getBoundary().push_back(this->raw_faces[f.size()-1].size()-1); }
					}
				}
			}
		};
		
		/// The main component for executing all the I/O operations on a simplicial complex.
		/**
		 * This class describes the main component for executing all the I/O operations on a simplicial complex, for example we can generate a data structure for encoding a simplicial complex
		 * from a file that contains a possible representation or we can write a representation of a simplicial complex as an output in accordance with a certain format for meshes. The effective
		 * I/O operations are delegated to a particular instance of the mangrove_tds::DataManager class in order to support a wide range of file formats for reading and writing. The design of
		 * the mangrove_tds::IO class is based on the <A href=http://en.wikipedia.org/wiki/Curiously_recurring_template_pattern>Curiously Recurring Template Pattern</A> in order to
		 * implement the static polymorphism and to discard the virtual member functions. In this way, we can reuse some parts and provide only the member functions specialized for particular
		 * data structures like the <i>Incidence Graph</i> (described in the mangrove_tds::IG class), the <i>Incidence Simplicial</i> data structure (described in the mangrove_tds::IS class),
		 * the <i>Simplified Incidence Graph</i> (described in the mangrove_tds::SIG class), the <i>IA*</i> data structure (described in the mangrove_tds::GIA class), the
		 * <i>Triangle-Segment</i> data structure (described in the mangrove_tds::TriangleSegment class) and in the <i>Non-Manifold IA</i> data structure (described in the
		 * mangrove_tds::NMIA class).
		 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::DataManager, mangrove_tds::IGManager, mangrove_tds::ISManager, mangrove_tds::SIGManager,
		 * mangrove_tds::GIAManager, mangrove_tds::TriangleSegmentManager, mangrove_tds::NMIAManager, mangrove_tds::IG, mangrove_tds::IS, mangrove_tds::SIG, mangrove_tds::GIA,
		 * mangrove_tds::TriangleSegment, mangrove_tds::NMIA
		 */
		template <class DS> class IO
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a not initialized instance of this class: you should apply the mangrove_tds::IO::createComplex() member function in order to initialize it.
			 * \see mangrove_tds::IO::createComplex(), mangrove_tds::IO::close()
			 */
			inline IO() { ; }
			
			/// This member function destroys an instance of this class.
			/**
			 * The specialized data structure for the simplicial complex is created in the mangrove_tds::IO::createComplex() member function, thus you must not manually destroy it: you should
			 * apply this destructor or the mangrove_tds::IO::close() member function in order to destroy all the internal state.
			 * \see mangrove_tds::IO::createComplex(), mangrove_tds::IO::close()
			 */
			inline virtual ~IO() { ; }
			
			/// This member function destroys all the internal state in this component.
			/**
			 * This member function destroys all the internal state in this component.<p>The specialized data structure for the simplicial complex is created in the
			 * mangrove_tds::IO::createComplex() member function, thus you must not manually destroy it: you should apply the destructor or this member function in order to destroy the
			 * internal state.
			 * \see mangrove_tds::IO::createComplex()
			 */
			inline void close() { static_cast<DS*>(this)->close(); }
			
			/// This member function returns the simplicial complex created by the current component.
			/**
			 * This member function returns the simplicial complex created by the current component.<p>The specialized data structure for the simplicial complex is created in the
			 * mangrove_tds::IO::createComplex() member function, thus you must not manually destroy it: you should apply the destructor or the mangrove_tds::IO::close() member function in
			 * order to destroy all the internal state.
			 * \param cs the simplicial complex created by this component
			 * \see mangrove_tds::IO::close(), mangrove_tds::IO::createComplex()
			 */
			template <class D> void getComplex(D** cs) { static_cast<DS*>(this)->getComplex(cs); }
			
			/// This member function returns a string that describes the data structure created by the current I/O component.
 			/**
 			 * This member function returns a string that describes the data structure created by the current I/O component, and used for encoding the current simplicial complex.
			 * \return a string that describes the data structure created by the current I/O component
 			 * \see mangrove_tds::BaseSimplicialComplex::getDataStructureName()
 			 */
 			inline string getDataStructureName() { return static_cast<DS*>(this)->getDataStructureName(); }
 			
 			/// This member function creates a simplicial complex by reading it from a file.
			/**
			 * This member function creates a particular data structure for representing a simplicial complex starting from a file that contains a possible representation: the most common
			 * exchange format for a simplicial complex consists of a collection of <i>top</i> simplices described by their vertices, but it is not the unique possibility.<p>The design of this
			 * member function is based on the <A href=http://en.wikipedia.org/wiki/Curiously_recurring_template_pattern>Curiously Recurring Template Pattern</A> in order to implement the
			 * static polymorphism: in this way, we can reuse some parts and provide only the member functions specialized for particular data structures like the <i>Incidence Graph</i>
			 * (described in the mangrove_tds::IG class), the <i>Incidence Simplicial</i> data structure (described in the mangrove_tds::IS class), the <i>Simplified Incidence Graph</i>
			 * (described in the mangrove_tds::SIG class), the <i>IA*</i> data structure (described in the mangrove_tds::GIA class), the <i>Triangle-Segment</i> data structure (described in
			 * the mangrove_tds::TriangleSegment class) and in the <i>Non-Manifold IA</i> data structure (described in the 	mangrove_tds::NMIA class).<p>In this member function we allocate a
			 * new data structure representing the required simplicial complex: the specialized data structure for the simplicial complex is created in this member function, thus you must not
			 * manually destroy it. You should apply the destructor or mangrove_tds::IO::close() member function in order to destroy all the internal state.<p>If we cannot complete this
			 * operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation
			 * will result in a failed assert.
	 	 	 * \param loader a component for the effective data reading
	 	 	 * \param fname the path of the input file
			 * \param reqParams this boolean flag identfies if the loading of the auxiliary parameters encoded in the input file is enabled
			 * \param ld this boolean flag indicates if the effective reading of the input data must be performed
			 * \param checksOn this boolean flag indicates if the effective checks on the input data must be performed (valid only if the effective reading must be performed)
			 * \see mangrove_tds::DataManager, mangrove_tds::IO::close(), mangrove_tds::BaseSimplicialComplex, mangrove_tds::LocalPropertyHandle, mangrove_tds::RawFace,
			 * mangrove_tds::IGManager, mangrove_tds::ISManager, mangrove_tds::SIGManager, mangrove_tds::GIAManager, mangrove_tds::TriangleSegmentManager,
			 * mangrove_tds::NMIAManager, mangrove_tds::IG, mangrove_tds::IS, mangrove_tds::SIG, mangrove_tds::GIA, mangrove_tds::TriangleSegment, mangrove_tds::NMIA
			 */
			template <class T> void createComplex(DataManager<T> *loader, string fname,bool reqParams,bool ld,bool checksOn)
			{ static_cast<DS*>(this)->createComplex(loader,fname,reqParams,ld,checksOn); }
			
 			/// This member function exports a simplicial complex in accordance with a certain format.
			/**
			 * This member function exports a simplicial complex, by writing its representation in accordance with the specific file format managed by the current component. The output format
			 * could usually contain a soup of top simplices, but also a complete description of a simplicial complex.<p>The effective writing is delegated to a subclass of the
			 * mangrove_tds::DataManager class and some further conditions could be checked and verified: if they are not satisfied, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param writer a component for the effective data writing (we write the data in this member function)
			 * \param fname the path of the output file
			 * \see mangrove_tds::DataManager, mangrove_tds::IO::close(), mangrove_tds::BaseSimplicialComplex
			 */
			template <class T> void writeComplex(DataManager<T> *writer,string fname) { static_cast<DS*>(this)->writeComplex(writer,fname); }
		};
	}
	
#endif

