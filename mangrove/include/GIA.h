/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library                                
 *                                                                           				    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * September 2011 (Revised on May 2012)
 *                                                                 
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * GIA.h -  implementation of the IA* data structure
 ***********************************************************************************************/

/* Should we include this header file? */
#ifndef GIA_H_

	#define GIA_H_

	#include "BaseSimplicialComplex.h"
	#include "Miscellanea.h"
	#include <algorithm>
	#include <assert.h>
	#include <cstdlib>
	#include <queue>
	#include <stack>
	using namespace mangrove_tds;
	using namespace std;
	
	/// Implementation of the <i>IA*</i> data structure.
	/**
	 * The class defined in this file implements the <i>IA*</i> data structure, a dimension-independent data structure for representing simplificial complexes with an arbitrary domain. It
	 * extends the <i>IA</i> data structure and it is scalable to manifold complexes, and supports efficient navigation and topological modifications. The <i>IA*</I> data structure encodes
	 * only top simplices plus a suitable subset of the adjacency relations, thus it is a <i>local</i> data structure.<p>This class must be used in conjunction with the
	 * mangrove_tds::BaseSimplicialComplex class in accordance with the
	 * <A href=http://en.wikipedia.org/wiki/Curiously_recurring_template_pattern>Curiously Recurring Template Pattern</A> rules in order to implement the static polymorphism: in this way
	 * we can reuse some parts of the mangrove_tds::BaseSimplicialComplex class and we can achieve more efficient implementations, by discarding virtual member functions.
	 * \file GIA.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	 
	 /* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		/// Implementation of the <i>IA*</i> data structure.
		/**
		 * The class defined in this file implements the <i>IA*</i> data structure, a dimension-independent data structure for representing simplificial complexes with an arbitrary domain. It
		 * extends the <i>IA</i> data structure and it is scalable to manifold complexes, and supports efficient navigation and topological modifications. The <i>IA*</I> data structure encodes
		 * only top simplices plus a suitable subset of the adjacency relations, thus it is a <i>local</i> data structure.<p>This class must be used in conjunction with the
		 * mangrove_tds::BaseSimplicialComplex class in accordance with the <A href=http://en.wikipedia.org/wiki/Curiously_recurring_template_pattern>Curiously Recurring Template Pattern</A>
		 * rules in order to implement the static polymorphism: in this way we can reuse some parts of the mangrove_tds::BaseSimplicialComplex class and we can achieve more efficient
		 * implementations, by discarding virtual member functions.
		 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplexPointer, mangrove_tds::IG, mangrove_tds::IS, mangrove_tds::SIG,
		 * mangrove_tds::NMIA, mangrove_tds::TriangleSegment, mangrove_tds::GhostPropertyHandle
		 */
		class GIA : public BaseSimplicialComplex<GIA>
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates all the internal data structures required for encoding a simplicial complex through a <i>IA*</i> data structure. In this member function, we 
			 * allocate all the internal data structures without any simplices or properties, i.e. auxiliary information, like Euclidean coordinates and field values, associated to simplices
			 * directly encoded in the <i>IA*</i> data structure. Thus, you should apply the member functions offered by the mangrove_tds::GIA class in order to add simplices and properties
			 * to the current data structure encoding a new simplicial complex.
			 * \param t the dimension of the simplicial complex to be encoded in the current data structure
			 * \see mangrove_tds::BaseSimplicialComplex::init(), mangrove_tds::BaseSimplicialComplex::clear(), mangrove_tds::PropertyBase, mangrove_tds::SIMPLEX_TYPE, 
			 * mangrove_tds::BaseSimplicialComplex::addGlobalProperty(), mangrove_tds::BaseSimplicialComplex::addLocalProperty(),
			 * mangrove_tds::BaseSimplicialComplex::addSparseProperty(), mangrove_tds::BaseSimplicialComplex::addGhostProperty()
			 */
			inline GIA(SIMPLEX_TYPE t) : BaseSimplicialComplex<GIA>(t) { ; }
			
			/// This member function destroys an instance of this class.
			inline virtual ~GIA() { ; }
			
			/// This member function returns a string that describes the current data structure.
			/**
 			 * This member function returns a string that describes the current data structure, usually its name.
 			 * \return a string that describes the current data structure
 			 * \see mangrove_tds::BaseSimplicialComplex::debug()
 			 */
 			inline string getDataStructureName() { return string("IA* (IA*)"); }
 			
 			/// This member function checks if the current data structure encodes all simplices in the simplicial complex to be represented.
 			/**
 			 * This member function checks if the current data structure encodes all simplices in the current simplicial complex: in this, case it is called <i>global</i> data structure. 
			 * Alternatively, a data structure can encode a subset of all simplices, for instance all top simplices: in this case, it is called <i>local</i> data structure.<p>The <i>IA*</i>
			 * data structure encodes only top simplices and it is a <i>local</i> data structure.
 			 * \return <ul><li><i>true</i>, if the current data structure encodes all simplices</li><li><i>false</i>, otherwise</li></ul>
 			 * \see mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::isTop(), mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
 			 */
 			inline bool encodeAllSimplices() { return false; }
			
			/// This member function computes the storage cost of the current data structure.
			/**
			 * This member function computes the storage cost of the current data structure, expressed as number of pointers to the simplices directly encoded in the current data structure.
			 * In other words, we compute the number of instances of the mangrove_tds::SimplexPointer class required for encoding the current data structure.
			 * \return the storage cost of the current data structure
			 * \see mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::SimplexIterator, mangrove_tds::BaseSimplicialComplex::type(), mangrove_tds::SIMPLEX_TYPE
			 */
			inline unsigned int getStorageCost()
 			{
 				unsigned int c;
 				SIMPLEX_TYPE t,d;
 				SimplexIterator it;

 				/* We must iterate on all the simplices in the current data structure! */
				c=0;
				t=this->type();
				for(d=0;d<=t;d++)
				{
					/* Now, we iterate over all the d-simplices! */
					for(it=this->begin(d);it!=this->end(d);it++)
					{
						/* Now, we consider the type of the current d-simplex! */
						if(it->type()==0) c=c+it->getCoboundarySize();
						else
						{
							/* Now, we have a contribution to the boundary relation and the adjacency! */
							if(this->isTop(SimplexPointer(it.getPointer()))) c=c+it->getBoundarySize();
							else c=c+it->getCoboundarySize();
							if(it->type()>1) c=c+it->getBoundarySize();
						}
					}
				}
				
				/* If we arrive here, we can return the storage cost! */
				return c;
 			}
 			
 			/// This member function identifies all simplices belonging to the boundary of a simplex directly encoded in the current data structure.
 			/**
 			 * This member function identifies all simplices belonging to the boundary of a simplex directly encoded in the current data structure.<p>The reference simplex and the boundary
 			 * simplices are required to be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector
 			 * mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function
 			 * only with a <i>global</i> data structure.<p>The <i>IA*</i> data structure is a <i>local</i> data structure, thus we cannot apply this member function for computing the
 			 * boundary of a simplex: you should apply instances of the mangrove_tds::GhostSimplexPointer class for accessing descriptions of simplices not directly encoded in the current
			 * data structure. Consequently, this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation
			 * will result in a failed assert.
	 		 * \param cp the reference simplex directly encoded in the current data structure
	 		 * \param ll a list containing all simplices belonging to the boundary of a simplex directly encoded in the current data structure.
	 		 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::PropertyBase
	 		 */
	 		inline void boundary(const SimplexPointer& cp, list<SimplexPointer>* ll)
	 		{
	 			/* You cannot apply this member function - idle avoids compiler warnings! */
	 			idle(ll);
	 			idle(cp);
	 			assert(this->encodeAllSimplices()==true);
	 		}

	 		/// This member function identifies all simplices of a certain dimension belonging to the boundary of a simplex directly encoded in the current data structure.
 			/**
 			 * This member function identifies all simplices of a certain dimension belonging to the boundary of a simplex directly encoded in the current data structure: this member function
 			 * filters boundary simplices against their dimension.<p>The reference simplex and the boundary simplices are required to be <i>valid</i>, i.e. directly stored in the current data
 			 * structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class
 			 * and thus by their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>The <i>IA*</i> data structure is a
 			 * <i>local</i> data structure, thus we cannot apply this member function for computing the boundary of a simplex: you should apply instances of the
 			 * mangrove_tds::GhostSimplexPointer class for accessing descriptions of simplices not directly encoded in the current data structure. Consequently, this member function will 
			 * fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
 			 * \param cp the reference simplex directly encoded in the current data structure
 			 * \param k the dimension of the required simplices belonging to the input simplex boundary
	 		 * \param ll a list containing all simplices belonging to the boundary of a simplex directly encoded in the current data structure
			 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::PropertyBase
	 		 */
	 		inline void boundaryk(const SimplexPointer& cp,SIMPLEX_TYPE k,list<SimplexPointer> *ll)
	 		{
	 			/* You cannot apply this member function - idle avoids compiler warnings! */
	 			idle(ll);
	 			idle(cp);
	 			idle(k);
	 			assert(this->encodeAllSimplices()==true);
	 		}	
	 			
	 		/// This member function identifies all simplices belonging to the boundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the boundary of a simplex not directly encoded in the current data structure.<p>The reference simplex and the
			 * boundary simplices can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can
			 * invoke this member function only with a <i>local</i> data structure.<p>The <i>IA*</i> data structure is a <i>local</i> data structure.<p><b>IMPORTANT:</b>if we cannot complete
			 * this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will
			 * result in a failed assert.
 			 * \param cp the reference simplex not directly encoded in the current data structure
	 		 * \param ll a list containing all simplices belonging to the boundary of a simplex not directly encoded in the current data structure.
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::Simplex,
	 		 * mangrove_tds::COMPLEX
	 		 */
	 		inline void boundary(const GhostSimplexPointer& cp, list<GhostSimplexPointer>* ll)
	 		{
	 			stack<SimplexPointer> s;
				COMPLEX c;
				SimplexPointer curr;
				vector<SIMPLEX_ID> fs;
				unsigned int t,ct,ci,i,j,lg;

	 			/* First, we check if all is ok and then if 'cp' is a real ghost simplex! */
	 			assert(ll!=NULL);
	 			assert(this->isValid(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			assert(this->isTop(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			ll->clear();
	 			t=cp.getParentType();
				ct=cp.getChildType();
				ci=cp.getChildId();
				if(cp.isEncoded()==false)
				{
					/* The parent is a valid top: we must have a valid child of a valid top simplex! */
					assert(ct<t);
					assert(ci<this->pascal_triangle[t+1][ct+1]);
					if(ct!=0)
					{
						/* Now, we can extract the boundary of a not encoded simplex (not vertex) */
						s.push(SimplexPointer(ct,ci));
						while(s.empty()==false)
						{
							curr=s.top();
							s.pop();
							if(curr.type()!=0)
							{
								fs=vector<SIMPLEX_ID>(this->fposes[t].getHierarchy()[curr.type()][curr.id()].getCBoundary());
								for(unsigned int z=0;z<fs.size();z++)
								{
									lg=c.size();
									c.insert(SimplexPointer(curr.type()-1,fs[z]));
									if(c.size()!=lg)
									{
										s.push(SimplexPointer(curr.type()-1,fs[z]));
										ll->push_back(GhostSimplexPointer(t,cp.getParentId(),curr.type()-1,fs[z]));	
									}
								}
							}
						}
					}
				}
				else if(ct!=0) { for(j=0;j<t;j++) for(i=0;i<pascal_triangle[t+1][j+1];i++) ll->push_back(GhostSimplexPointer(t,cp.getParentId(),j,i)); }
	 		}
 			
 			/// This member function identifies all simplices of a certain dimension belonging to the boundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices of a certain dimension belonging to the boundary of a simplex not directly encoded in the current data structure: this member
			 * function filters boundary simplices against their dimension.<p>The reference simplex and the boundary simplices can also be not directly encoded in the current data structure
			 * and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>The
			 * <i>IA*</i> data structure is a <i>local</i> data structure.<p><b>IMPORTANT:</b>if we cannot complete this operation, then this member function will fail if and only if the
	 		 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
			 * \param k the dimension of the required simplices belonging to the input simplex boundary
			 * \param ll a list of the required simplices belonging to the input simplex boundary
	 		 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, 
	 		 * mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isValid() 
			 */
	 		inline void boundaryk(const GhostSimplexPointer& cp,SIMPLEX_TYPE k,list<GhostSimplexPointer> *ll)
	 		{
	 			unsigned int t,ct,ci,i,lg;
				vector<SIMPLEX_ID> fs;
				SimplexPointer curr;
				stack<SimplexPointer> s;
				COMPLEX c;

	 			/* First, we check if all is ok and then if 'cp' is a real ghost simplex! */
	 			assert(ll!=NULL);
	 			assert(this->isValid(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			assert(this->isTop(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			ll->clear();
	 			t=cp.getParentType();
				ct=cp.getChildType();
				ci=cp.getChildId();
				if(cp.isEncoded()==false)
				{
					/* The parent is a valid top: we must have a valid child of a valid top simplex! */
					assert(ct<t);
					assert(ci<this->pascal_triangle[t+1][ct+1]);
					assert(k<ct);
					if(ct!=0)
					{
						/* Now, we can extract the boundary of a not encoded simplex (not vertex) */
						s.push(SimplexPointer(ct,ci));
						while(s.empty()==false)
						{
							curr=s.top();
							s.pop();
							if(curr.type()>k)
							{
								fs=vector<SIMPLEX_ID>(this->fposes[t].getHierarchy()[curr.type()][curr.id()].getCBoundary());
								for(unsigned int z=0;z<fs.size();z++)
								{
									lg=c.size();
									c.insert(SimplexPointer(curr.type()-1,fs[z]));
									if(c.size()!=lg) { s.push(SimplexPointer(curr.type()-1,fs[z])); }
								}
							}
							else if(curr.type()==k) { ll->push_back(GhostSimplexPointer(t,cp.getParentId(),k,curr.id())); }
						}
					}
				}
				else if(ct!=0) { for(i=0;i<pascal_triangle[t+1][k+1];i++) ll->push_back(GhostSimplexPointer(t,cp.getParentId(),k,i)); }
	 		}

 			/// This member function identifies all simplices belonging to the coboundary of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the coboundary of a simplex directly encoded in the current data structure.<p>The reference simplex and the star
	 		 * simplices are required to be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector
	 		 * mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function
	 		 * only with a <i>global</i> data structure.<p>The <i>IA*</i> data structure is a <i>local</i> data structure, thus we cannot apply this member function for computing the
	 		 * coboundary of a simplex: you should apply instances of the mangrove_tds::GhostSimplexPointer class for accessing descriptions of simplices not directly encoded in the current
			 * data structure. Consequently, this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation
			 * will result in a failed assert.
 			 * \param cp the reference simplex directly encoded in the current data structure
			 * \param ll a list of the required simplices belonging to the input simplex coboundary
			 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase
			 */
			inline void coboundary(const SimplexPointer& cp,list<SimplexPointer> *ll)
	 		{
	 			/* You cannot apply this member function - idle avoids compiler warnings! */
	 			idle(ll);
	 			idle(cp);
	 			assert(this->encodeAllSimplices()==true);
	 		}
	 		
	 		/// This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex directly encoded in the current data structure: this member
			 * function filters boundary simplices against their dimension.<p>The reference simplex and the star simplices are required to be <i>valid</i>, i.e. directly stored in the
			 * current data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They are identified by instances of the
			 * mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>The
			 * <i>IA*</i> data structure is a <i>local</i> data structure, thus we cannot apply this member function for computing the coboundary of a simplex: you should apply instances of
			 * the mangrove_tds::GhostSimplexPointer class for accessing descriptions of simplices not directly encoded in the current data structure. Consequently, this member function will
			 * fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
 			 * \param cp the reference simplex directly encoded in the current data structure
 			 * \param k the dimension of the required simplices belonging to the input simplex coboundary
			 * \param ll a list of the required simplices belonging to the input simplex coboundary
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::GhostSimplex,
			 * mangrove_tds::GhostSimplexPointer, mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::LocalPropertyHandle,
			 * mangrove_tds::BaseSimplicialComplex::addGlobalProperty(), mangrove_tds::BaseSimplicialComplex::addLocalProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty()
			 */
	 		inline void coboundaryk(const SimplexPointer& cp,SIMPLEX_TYPE k,list<SimplexPointer> *ll)
	 		{
	 			/* You cannot apply this member function - idle avoids compiler warnings! */
	 			idle(ll);
	 			idle(cp);
	 			idle(k);
	 			assert(this->encodeAllSimplices()==true);
	 		}
	 		
	 		/// This member function identifies all simplices belonging to the coboundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the coboundary of a simplex not directly encoded in the current data structure.<p>The reference simplex and the star
	 		 * simplices can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke
			 * this member function only with a <i>local</i> data structure.<p>The <i>IA*</i> data structure is a <i>local</i> data structure.<p>If we cannot complete this operation, then
			 * this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.<p><b>IMPORTANT:</b> when you try to apply this member function, you should be careful about the use of auxiliary information (i.e. subclasses of the
			 * mangrove_tds::PropertyBase class) associated to the current simplicial complex. In other words, in this member function, we use a ghost property, called <i>cobs</i> and
			 * described by an instance of the mangrove_tds::GhostPropertyHandle, for marking ghost simplices. We remove this property, if it already belongs to the current simplicial
			 * complex. In any case, we remove it at the end of this member function.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
	 		 * \param ll a list of the required simplices belonging to the input simplex coboundary
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::PropertyBase,
	 		 * mangrove_tds::GhostPropertyHandle
	 		 */
	 		inline void coboundary(const GhostSimplexPointer& cp, list<GhostSimplexPointer> *ll)
	 		{
	 			bool *b;
	 			unsigned int t,ct,ci,pos,lg,z;
				GhostSimplex s,s1;
				GhostPropertyHandle<bool> *gprop;
				vector< list<SimplexPointer> > inc;
				list<SimplexPointer>::iterator it;
				SimplexPointer v,ni;
				queue<SimplexPointer> q;
				SIMPLEX_TYPE nit;

	 			/* First, we check if all is ok! */
	 			assert(ll!=NULL);
	 			assert(this->isValid(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			assert(this->isTop(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			ll->clear();
	 			t=cp.getParentType();
				ct=cp.getChildType();
				ci=cp.getChildId();
				if(cp.isEncoded()==false)
				{
					/* We must have a valid child of a valid top simplex: first, we retrieve all top simplices incident in 'cp' and then we expand them */
					assert(ct<t);
					assert(ci<this->pascal_triangle[t+1][ct+1]);
					for(SIMPLEX_TYPE d=0;d<this->type();d++) { inc.push_back(list<SimplexPointer>()); }
					this->ghostSimplex(cp,&s);
					gprop=this->addGhostProperty(string("cobs"),false);
					if(ct==0)
					{
						/* Here, the input ghost simplex is a vertex! */
						v=SimplexPointer(s.getBoundary()[0]);
						this->retrieveIncidentTopSimplices(SimplexPointer(v),&inc);
						for(it=inc[0].begin();it!=inc[0].end();it++) ll->push_back(GhostSimplexPointer(it->type(),it->id()));
					}
					else { this->retrieveIncidentTopSimplices(GhostSimplexPointer(cp),&inc); }
					for(SIMPLEX_TYPE d=1;d<inc.size();d++)
					{
						/* Now, we expand all the top d-simplices incident to 'cp' */
						for(it=inc[d].begin();it!=inc[d].end();it++)
						{
							/* Now, we check if 'it' can be considered! */
							ll->push_back(GhostSimplexPointer(it->type(),it->id()));
							if(ct!=d)
							{
								/* Now, we check if 'it' can be directly stored and then we can expand it! */
								if(ct==0) { this->simplex(SimplexPointer(*it)).positionInBoundary(v,pos); }
								else { pos=this->getFace(GhostSimplex(s),SimplexPointer(*it)); }
								lg=this->fposes[it->type()].getHierarchy()[ct].at(pos).getCCoboundary().size();
								for(z=0;z<lg;z++) { q.push(SimplexPointer(ct+1,this->fposes[it->type()].getHierarchy()[ct].at(pos).getCCoboundary().at(z))); }
								while(q.empty()==false)
								{
									/* Now, we analyze the current simplex in the queue */
									ni=SimplexPointer(q.front());
									q.pop();
									nit=ni.type();
									b=NULL;
									this->ghostSimplex(GhostSimplexPointer(it->type(),it->id(),nit,ni.id()),&s1);
									gprop->get(s1,&b);
									if((*b)==false)
									{
										/* The simplex is new! */
										(*b)=true;
										ll->push_back(GhostSimplexPointer(it->type(),it->id(),nit,ni.id()));
									}
									
									/* Now, we proceed on the other subfaces incident at 'cp' */
									if(nit<it->type()-1)
									{
										lg=this->fposes[it->type()].getHierarchy()[nit].at(ni.id()).getCCoboundary().size();
										for(z=0;z<lg;z++) q.push(SimplexPointer(nit+1,this->fposes[it->type()].getHierarchy()[nit].at(ni.id()).getCCoboundary().at(z)));
									}
								}
							}
						}
					}
					
					/* If we arrive here, we can sort the required list of simplices! */
					this->deleteProperty(string("cobs"));
					ll->sort(compare_ghost_simplices_pointers);
				}
	 		}
	 		
	 		/// This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex not directly encoded in the current data structure: this member
	 		 * function filters coboundary simplices against their dimension.<p>The reference simplex and the star simplices can also be not directly encoded in the current data structure and
	 		 * they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>The <i>IA*</i> 
	 		 * data structure is a <i>local</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is
	 		 * compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> when you try to apply this member function, you should be
	 		 * careful about the use of auxiliary information (i.e. subclasses of the mangrove_tds::PropertyBase class) associated to the current simplicial complex. In other words, in this
			 * member function, we use a ghost property, called <i>cobs</i> and described by an instance of the mangrove_tds::GhostPropertyHandle, for marking ghost simplices. We remove this
	 		 * property, if it already belongs to the current simplicial complex. In any case, we remove it at the end of this member function.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
			 * \param k the dimension of the required simplices belonging to the input simplex coboundary
			 * \param ll a list of the required simplices belonging to the input simplex coboundary
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase,
	 		 * mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isValid()
			 */
	 		inline void coboundaryk(const GhostSimplexPointer& cp,SIMPLEX_TYPE k,list<GhostSimplexPointer> *ll)
	 		{
	 			unsigned int t,ct,ci,pos,lg,z;
	 			vector< list<SimplexPointer> > inc;
				list<SimplexPointer>::iterator it;
				GhostPropertyHandle<bool> *gprop;
				SimplexPointer v,ni;
				GhostSimplex s,s1;
				queue<SimplexPointer> q;
				SIMPLEX_TYPE nit;
				bool *b;

	 			/* First, we check if all is ok! */
	 			assert(ll!=NULL);
	 			assert(this->isValid(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			assert(this->isTop(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			ll->clear();
	 			t=cp.getParentType();
				ct=cp.getChildType();
				ci=cp.getChildId();
				if(cp.isEncoded()==false)
				{
					/* We must have a valid child of a valid top simplex: first, we retrieve all top simplices incident in 'cp' and then we expand them */
					assert(ct<t);
					assert(ci<this->pascal_triangle[t+1][ct+1]);
					assert(k<=this->type());
					assert(k>ct);
					for(SIMPLEX_TYPE d=0;d<this->type();d++) { inc.push_back(list<SimplexPointer>()); }
					this->ghostSimplex(cp,&s);
					gprop=this->addGhostProperty(string("cobs"),false);
					if(ct==0)
					{
						/* Here, the input ghost simplex is a vertex! */
						v=SimplexPointer(s.getBoundary()[0]);
						this->retrieveIncidentTopSimplices(SimplexPointer(v),&inc);
						if(k==1) { for(it=inc[0].begin();it!=inc[0].end();it++) ll->push_back(GhostSimplexPointer(it->type(),it->id())); }
					}
					else { this->retrieveIncidentTopSimplices(GhostSimplexPointer(cp),&inc); }
					for(SIMPLEX_TYPE d=1;d<inc.size();d++)
					{
						/* Now, we expand all the top d-simplices incident to 'cp' */
						for(it=inc[d].begin();it!=inc[d].end();it++)
						{
							/* Now, we check if 'it' can be considered! */
							if(it->type()==k) ll->push_back(GhostSimplexPointer(it->type(),it->id()));
							else if( (ct!=d) && (it->type()>k))
							{
								/* Now, we check if 'it' can be directly stored and then we can expand it! */
								if(ct==0) { this->simplex(SimplexPointer(*it)).positionInBoundary(v,pos); }
								else { pos=this->getFace(GhostSimplex(s),SimplexPointer(*it)); }
								lg=this->fposes[it->type()].getHierarchy()[ct].at(pos).getCCoboundary().size();
								for(z=0;z<lg;z++) { q.push(SimplexPointer(ct+1,this->fposes[it->type()].getHierarchy()[ct].at(pos).getCCoboundary().at(z))); }
								while(q.empty()==false)
								{
									/* Now, we analyze the current simplex in the queue */
									ni=SimplexPointer(q.front());
									q.pop();
									nit=ni.type();
									if(nit==k)
									{
										b=NULL;
										this->ghostSimplex(GhostSimplexPointer(it->type(),it->id(),nit,ni.id()),&s1);
										gprop->get(s1,&b);
										if((*b)==false)
										{
											/* The simplex is new! */
											(*b)=true;
											ll->push_back(GhostSimplexPointer(it->type(),it->id(),nit,ni.id()));
										}
									}
									
									/* Now, we proceed on the other subfaces incident at 'cp' */
									if( (nit<k) && (nit<it->type()-1))
									{
										lg=this->fposes[it->type()].getHierarchy()[nit].at(ni.id()).getCCoboundary().size();
										for(z=0;z<lg;z++) q.push(SimplexPointer(nit+1,this->fposes[it->type()].getHierarchy()[nit].at(ni.id()).getCCoboundary().at(z)));
									}
								}
							}
						}
					}

					/* If we arrive here, we can sort the required list of simplices! */
					this->deleteProperty(string("cobs"));
					ll->sort(compare_ghost_simplices_pointers);
				}
	 		}

	 		/// This member function identifies all simplices adjacent to a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices adjacent to a simplex directly encoded in the current data structure: two 0-simplices are <i>adjacent</i> if they are connected
			 * by a common edge, while two k-simplices (with k>0) are <i>adjacent</i> if they share a simplex of dimension k-1.<p>The reference simplex and the adjacent simplices are
			 * required to be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They
			 * are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function only with a 
			 * <i>global</i> data structure.<p>The <i>IA*</i> data structure is a <i>local</i> data structure, thus we cannot apply this member function for computing the adjacency of a
			 * simplex: you should apply instances of the mangrove_tds::GhostSimplexPointer class for accessing descriptions of simplices not directly encoded in the current data structure.
			 * Consequently, this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a
			 * failed assert.
	 		 * \param cp the reference simplex directly encoded in the current data structure
			 * \param ll a list of the required simplices belonging to the input simplex adjacency
			 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
			 * mangrove_tds::PropertyBase
			 */
			inline void adjacency(const SimplexPointer &cp,list<SimplexPointer> *ll)
			{
				/* You cannot apply this member function - idle avoids compiler warnings! */
	 			idle(ll);
	 			idle(cp);
	 			assert(this->encodeAllSimplices()==true);
			}
			
			/// This member function identifies all simplices adjacent to a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices adjacent to a simplex not directly encoded in the current data structure: two 0-simplices are <i>adjacent</i> if they are
			 * connected by a common edge, while two k-simplices (with k>0) are <i>adjacent</i> if they share a simplex of dimension k-1.<p>The reference simplex and the adjacent simplices
			 * can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member
			 * function only with a <i>local</i> data structure.<p>The <i>IA*</i> data structure is a <i>local</i> data structure.<p>If we cannot complete this operation, then this member
			 * function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.<p><b>IMPORTANT:</b> when you try to apply this member function, you should be careful about the use of auxiliary information (i.e. subclasses of the
			 * mangrove_tds::PropertyBase class) associated to the current simplicial complex. In other words, in this member function, we use a local property (i.e. an instance of the
			 * mangrove_tds::LocalPropertyHandle class) with boolean values for all the d-simplices, called <i>assigned</i> and a ghost property (i.e. an instance of the
			 * mangrove_tds::GhostPropertyHandle class) with boolean values, called <i>ad</i>. We remove these properties, if they already belong to the current simplicial complex. In any
			 * case, we remove them at the end of this member function.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
			 * \param ll a list of the required simplices belonging to the input simplex adjacency and not directly encoded in the current data structure
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase,
	 		 * mangrove_tds::GIA::retrieveImmediateIncidents()
	 		 */
	 		inline void adjacency(const GhostSimplexPointer &cp,list<GhostSimplexPointer> *ll)
	 		{
	 			unsigned int t,ct,ci,pos,z;
	 			SIMPLEX_TYPE d;
	 			SimplexPointer v;
	 			vector< list<SimplexPointer> > inc;
	 			GhostSimplex s,s1;
	 			list<SimplexPointer>::iterator it;
	 			list<GhostSimplexPointer>::iterator tops;
				vector<SIMPLEX_ID> subfaces;
				list<GhostSimplexPointer> cob;
				list<GhostSimplexPointer>::iterator cob_it;
				GhostPropertyHandle<bool> *gprop;
				bool *b;

	 			/* First, we check if all is ok! */
	 			assert(ll!=NULL);
	 			assert(this->isValid(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			assert(this->isTop(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			ll->clear();
	 			t=cp.getParentType();
				ct=cp.getChildType();
				ci=cp.getChildId();
				if(cp.isEncoded()==false)
				{
					/* The input simplex is a real ghost simplex: we must have a valid child of a valid top simplex! */
					assert(ct<t);
					assert(ci<this->pascal_triangle[t+1][ct+1]);
					this->ghostSimplex(cp,&s);
					for(d=0;d<this->type();d++) { inc.push_back(list<SimplexPointer>()); }
					if(ct==0)
					{
						/* We must compute the adjacency of a vertex: we can retrieve all the edges incident here and then we can extract their vertices! */
						v=SimplexPointer(s.getBoundary()[0]);
						this->retrieveIncidentTopSimplices(SimplexPointer(v),&inc);
						for(d=0;d<this->type();d++)
						{
							/* If we arrive here, we consider the current top d-simplex and then we extract the other vertices */
							for(it=inc[d].begin();it!=inc[d].end();it++)
							{
								this->simplex(SimplexPointer(*it)).positionInBoundary(v,pos);
								for(unsigned int k=0;k<=it->type();k++)
								{
									if( (k!=pos) && (this->isVisited(this->simplex(SimplexPointer(*it)).bc(k))==false))
									{
										this->visit(this->simplex(SimplexPointer(*it)).bc(k));
										ll->push_back(GhostSimplexPointer(it->type(),it->id(),0,k));
									}
								}
							}
						}
						
						/* Now, we unmark all visited vertices! */
						this->unmarkAllVisited();
					}
					else
					{
						/* We must compute the adjacency of ghost simplex (non top and not a vertex) */
						if(this->hasProperty(string("ad"))) this->deleteProperty(string("ad"));
						gprop=this->addGhostProperty(string("ad"),false);
						subfaces=vector<SIMPLEX_ID>(this->fposes[t].getHierarchy()[ct].at(ci).getCBoundary());
						for(z=0;z<subfaces.size();z++)
						{
							this->retrieveImmediateIncidents(GhostSimplexPointer(t,cp.getParentId(),ct-1,subfaces[z]),&cob);
							for(cob_it=cob.begin();cob_it!=cob.end();cob_it++)
							{
								this->ghostSimplex(GhostSimplexPointer(*cob_it),&s1);
								if(s.theSame(s1)==false)
								{
									/* Is it new? */
									b=NULL;
									gprop->get(s1,&b);
									if( (*b)==false)
									{
										(*b)=true;
										ll->push_back(GhostSimplexPointer(*cob_it)); 
									}
								}
							}
						}
						
						/* Now, we remove 'ad' */
						this->deleteProperty(string("ad"));
					}
				}
				else
				{
					/* The input simplex is a fake ghost simplex, i.e. it is a top simplex! Is it maximal or not? */
					if(ct==this->type()) { this->retrieveAdjacentTopSimplices(SimplexPointer(cp.getParentType(),cp.getParentId()),ll); }
					else if(ct==0) { ; }
					else
					{
						/* We must compute the adjacency of a top simplex, but not maximal */
						if(this->hasProperty(string("ad"))) this->deleteProperty(string("ad"));
						gprop=this->addGhostProperty(string("ad"),false);
						this->ghostSimplex(cp,&s);
						for(z=0;z<=t;z++)
						{
							this->retrieveImmediateIncidents(GhostSimplexPointer(t,cp.getParentId(),t-1,z),&cob);
							for(cob_it=cob.begin();cob_it!=cob.end();cob_it++)
							{
								this->ghostSimplex(GhostSimplexPointer(*cob_it),&s1);
								if(s.theSame(s1)==false)
								{
									/* Is it new? */
									b=NULL;
									gprop->get(s1,&b);
									if( (*b)==false)
									{
										(*b)=true;
										ll->push_back(GhostSimplexPointer(*cob_it)); 
									}
								}
							}
						}
						
						/* Now, we remove 'ad' */
						this->deleteProperty(string("ad"));
					}
				}
				
				/* If we arrive here, we can sort the required list of simplices! */
				ll->sort(compare_ghost_simplices_pointers);
	 		}
	 		
	 		/// This member function identifies all simplices belonging to the link of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the link of a simplex directly encoded in the current data structure: let <i>c</i> a simplex in the current
			 * simplicial complex, then the <i>link</i> of <i>c</i> is the set of all the subsimplices of the simplices in the star of <i>c</i> that are not incident to <i>c</i>.<p>The
			 * reference simplex and the required simplices must be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by
			 * the garbage collector mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can
			 * invoke this member function only with a <i>global</i> data structure.<p>The <i>IA*</i> data structure is a <i>local</i> data structure, thus we cannot apply this member
			 * function for computing the adjacency of a simplex: you should apply instances of the mangrove_tds::GhostSimplexPointer class for accessing descriptions of simplices not
			 * directly encoded in the current data structure. Consequently, this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this
			 * case, each forbidden operation will result in a failed assert.
	 		 * \param cp the reference simplex directly encoded in the current data structure
	 		 * \param ll a list of the required simplices belonging to the input simplex link
	 		 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplexPointer, mangrove_tds::BaseSimplicialComplex::type(), mangrove_tds::PropertyBase
	 		 */
	 		inline void link(const SimplexPointer& cp,list<SimplexPointer> *ll)
	 		{
	 			/* Dummy statements for avoiding warnings! */
	 			idle(cp);
	 			idle(ll);
	 			assert(this->encodeAllSimplices()==true);
	 		}
	 		
	 		/// This member function identifies all simplices belonging to the link of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the link of a simplex not directly encoded in the current data structure: let <i>c</i> a simplex in the current 
			 * simplicial complex, then the <i>link</i> of <i>c</i> is the set of all the subsimplices of the simplices in the star of <i>c</i> that are not incident to <i>c</i>.<p>The
			 * reference simplex and the required simplices can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer
			 * class. Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>The <i>IA*</i> data structure is a <i>local</i> data structure.<p>If we
			 * cannot complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each
			 * forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> when you try to apply this member function, you should be careful about the use of auxiliary
			 * information (i.e. subclasses of the mangrove_tds::PropertyBase class) associated to the current simplicial complex. In other words, in this member function, we use a global
			 * property, called <i>assigned</i>, (namely an instance of the mangrove_tds::GlobalPropertyHandle class), and a ghost property, called <i>vis_lnk</i> (namely an instance of the
			 * mangrove_tds::GhostPropertyHandle class), both with boolean values for marking simplices. We remove such properties, if they already belong to the current simplicial complex.
			 * In any case, we remove them at the end of this member function.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
	 		 * \param ll a list of the required simplices belonging to the input simplex link and not directly encoded in the current data structure
	 		 * \see mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::SIMPLEX_TYPE,
	 		 * mangrove_tds::SIMPLEX_ID, mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle
	 		 */
	 		inline void link(const GhostSimplexPointer& cp,list<GhostSimplexPointer> *ll)
	 		{
				unsigned int t,ct,ci,pos,ot;
	 			GhostSimplex s,s1;
				SimplexPointer v;
				vector< list<SimplexPointer> > inc;
				list<SimplexPointer>::iterator it;
				list<GhostSimplexPointer> bnd;
				list<GhostSimplexPointer>::iterator bnd_it;
				GhostPropertyHandle<bool> *gprop;
				bool *b;

	 			/* First, we check if all is ok! */
	 			assert(ll!=NULL);
	 			assert(this->isValid(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			assert(this->isTop(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			ll->clear();
	 			t=cp.getParentType();
				ct=cp.getChildType();
				ci=cp.getChildId();
				if(cp.isEncoded()==false)
				{
					/* We must have a valid child of a valid top simplex! */
					assert(ct<t);
					assert(ci<this->pascal_triangle[t+1][ct+1]);
					if(this->hasProperty(string("vis_lnk"))) this->deleteProperty(string("vis_lnk"));
					gprop=this->addGhostProperty(string("vis_lnk"),false);
					this->ghostSimplex(cp,&s);
					if(ct==0)
					{
						/* The input simplex is a vertex! */
						v=SimplexPointer(s.getBoundary()[0]);
						for(SIMPLEX_TYPE d=0;d<this->type();d++) { inc.push_back(list<SimplexPointer>()); }
						this->retrieveIncidentTopSimplices(SimplexPointer(v),&inc);
						for(SIMPLEX_TYPE d=0;d<this->type();d++)
						{
							for(it=inc[d].begin();it!=inc[d].end();it++)
							{
								/* Now, we must understand in which position is 'v' */
								this->simplex(SimplexPointer(*it)).positionInBoundary(v,pos);
								if(it->type()==1)
								{
									/* We have a wire-edge, thus we can take only the other vertex! */
									if(pos==0) { ot=1; }
									else { ot=0; }
									this->ghostSimplex(GhostSimplexPointer(it->type(),it->id(),0,ot),&s1);
									gprop->get(GhostSimplex(s1),&b);
									if((*b)==false)
									{
										(*b)=true;
										ll->push_back(GhostSimplexPointer(it->type(),it->id(),0,ot));
									}
								}
								else
								{
									/* We have a top simplex, not wire-edge. Thus, we must consider the boundary of the face opposite to 'v' */
									ll->push_back(GhostSimplexPointer(it->type(),it->id(),it->type()-1,pos));
									this->boundary(GhostSimplexPointer(it->type(),it->id(),it->type()-1,pos),&bnd);
									for(bnd_it=bnd.begin();bnd_it!=bnd.end();bnd_it++)
									{
										this->ghostSimplex(GhostSimplexPointer(*bnd_it),&s1);
										gprop->get(GhostSimplex(s1),&b);
										if((*b)==false)
										{
											(*b)=true;
											ll->push_back(GhostSimplexPointer(*bnd_it));
										}
									}
								}
							}
						}
					}
					else
					{
						/* The ghost simplex to be analyzed is not a vertex! */
						for(SIMPLEX_TYPE d=0;d<this->type();d++) { inc.push_back(list<SimplexPointer>()); }
						this->retrieveIncidentTopSimplices(GhostSimplexPointer(cp),&inc);
						for(SIMPLEX_TYPE d=0;d<this->type();d++)
						{
							for(it=inc[d].begin();it!=inc[d].end();it++)
							{
								/* Now, we must understand how proceed! */
								pos=this->getFace(GhostSimplex(s),SimplexPointer(*it));
								if(ct==it->type()-1)
								{
									/* If we arrive here, the input simplex is the last simplex before maximal! */
									this->ghostSimplex(GhostSimplexPointer(it->type(),it->id(),0,pos),&s1);
									gprop->get(GhostSimplex(s1),&b);
									if((*b)==false)
									{
										(*b)=true;
										ll->push_back(GhostSimplexPointer(it->type(),it->id(),0,pos));
									}
								}
								else
								{
									/* Now, we consider the opposite simplex in 'it' and then we insert it in 'll' */
									ot=this->pascal_triangle[it->type()+1][ct+1]-1-pos;
									this->ghostSimplex(GhostSimplexPointer(it->type(),it->id(),ct,ot),&s1);
									gprop->get(GhostSimplex(s1),&b);
									if((*b)==false)
									{
										(*b)=true;
										ll->push_back(GhostSimplexPointer(it->type(),it->id(),ct,ot));
									}
									
									/* Now, we insert its boundary! */
									this->boundary(GhostSimplexPointer(it->type(),it->id(),ct,ot),&bnd);
									for(bnd_it=bnd.begin();bnd_it!=bnd.end();bnd_it++)
									{
										this->ghostSimplex(GhostSimplexPointer(*bnd_it),&s1);
										gprop->get(GhostSimplex(s1),&b);
										if((*b)==false)
										{
											(*b)=true;
											ll->push_back(GhostSimplexPointer(*bnd_it));
										}
									}
								}
							}
						}
					}
					
					/* Now, we remove 'ad' */
					this->deleteProperty(string("vis_lnk"));
					ll->sort(compare_ghost_simplices_pointers);
				}
	 		}
	 		
	 		/// This member function returns the number of components in the link of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function returns the number of components in the link of a simplex directly encoded in the current data structure: let <i>c</i> a simplex in the current simplicial
	 		 * complex, then the <i>link</i> of <i>c</i> is the set of all the subsimplices of the simplices in the star of <i>c</i> that are not incident to <i>c</i>.<p>The reference simplex
	 		 * and the required simplices must be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage
			 * collector mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this
			 * member function only with a <i>global</i> data structure.<p>The <i>IA*</i> data structure is a <i>local</i> data structure, thus we cannot apply this member function for
			 * computing the number of components in the link of a simplex: you should apply instances of the mangrove_tds::GhostSimplexPointer class for accessing descriptions of simplices
			 * not directly encoded in the  current data structure. Consequently, this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In
			 * this case, each forbidden operation will result in a failed assert.
	 		 * \param cc the reference simplex directly encoded in the current data structure
	 		 * \param lk a list of the required simplices belonging to the input simplex link
	 		 * \return the number of components in the link of a simplex directly encoded in the current data structure
	 		 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::PropertyBase
	 		 */
	 		inline unsigned int getLinkComponentsNumber(const SimplexPointer &cc,list<SimplexPointer> *lk)
			{
				/* You cannot apply this member function - idle avoids compiler warnings! */
	 			idle(lk);
	 			idle(cc);
	 			assert(this->encodeAllSimplices()==true);
	 			return 0;
			}
			
			/// This member function returns the number of components in the link of a simplex not directly encoded in the current data structure.
			/**
			 * This member function returns the number of components in the link of a simplex not directly encoded in the current data structure: let <i>c</i> a simplex in the current
			 * simplicial complex, then the <i>link</i> of <i>c</i> is the set of all the subsimplices of the simplices in the star of <i>c</i> that are not incident in <i>c</i>.<p>The
			 * reference simplex and the required simplices can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer
			 * class. Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>The <i>IA*</i> data structure is a <i>local</i> data structure.<p>If we
			 * cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.<p><b>IMPORTANT:</b> when you try to apply this member function, you should be careful about the use of auxiliary information (i.e.
			 * subclasses of the mangrove_tds::PropertyBase class) associated to the current simplicial complex. In other words, in this member function, we use a global property, called
			 * <i>assigned</i>, (namely an instance of the mangrove_tds::GlobalPropertyHandle class), and a ghost properties, respectively called <i>vis_lnk</i> and <i>inLink</i> (namely
			 * instances of the mangrove_tds::GhostPropertyHandle class), both with boolean values for marking simplices. We remove such properties, if they already belong to the current
			 * simplicial complex. In any case, we remove them at the end of this member function.
	 		 * \param cc the reference simplex not directly encoded in the current data structure
	 		 * \param lk a list of the required simplices belonging to the input simplex link
	 		 * \return the number of components in the link of a simplex not directly encoded in the current data structure
	 		 * \see mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::SIMPLEX_TYPE,
	 		 * mangrove_tds::SIMPLEX_ID, mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle
			 */
			inline unsigned int getLinkComponentsNumber(const GhostSimplexPointer &cc,list<GhostSimplexPointer> *lk)
			{
				unsigned int n,t,ct,ci;
				SimplexPointer v;
				GhostSimplex s;
				GhostPropertyHandle<bool> *vlp,*lkp;
				list<GhostSimplexPointer>::iterator cp,temp;
				list<GhostSimplexPointer> bl,cl;
				bool *b;
				GhostSimplexPointer current;

				/* First, we check if all is ok! */
				assert(lk!=NULL);
				assert(this->isValid(SimplexPointer(cc.getParentType(),cc.getParentId())));
	 			assert(this->isTop(SimplexPointer(cc.getParentType(),cc.getParentId())));
	 			lk->clear();
	 			n=0;
	 			t=cc.getParentType();
				ct=cc.getChildType();
				ci=cc.getChildId();
				if(cc.isEncoded()==false)
				{
					/* We have an internal simplex to be analyzed: first, we check if the input simplex is a vertex! */
					assert(ct<t);
					assert(ci<this->pascal_triangle[t+1][ct+1]);
					this->link(GhostSimplexPointer(cc),lk);
					if(lk->size()<=1) { n=1; }
					else if(ct==0)
					{
						this->ghostSimplex(GhostSimplexPointer(cc),&s);
						v=SimplexPointer(s.getCBoundary()[0]);
						n=this->simplex(SimplexPointer(v)).getCoboundarySize();
					}
					else
					{
						/* If we arrive here, the input simplex is not a vertex. First, we mark simplices in the link of 'cc' */
						if(this->hasProperty(string("vis_lnk"))) this->deleteProperty(string("vis_lnk"));
						if(this->hasProperty(string("inLink"))) this->deleteProperty(string("inLink"));
						vlp=this->addGhostProperty(string("vis_lnk"),false);
						lkp=this->addGhostProperty(string("inLink"),false);
						for(cp=lk->begin();cp!=lk->end();cp++)
						{
							this->ghostSimplex(*cp,&s);
							lkp->set(GhostSimplex(s),true);
						}
						
						/* Now, we identify connected components in the link of 'cc' */
						for(cp=lk->begin();cp!=lk->end();cp++)
						{
							this->ghostSimplex(*cp,&s);
							vlp->get(GhostSimplex(s),&b);
							if((*b)==false)
							{
								queue<GhostSimplexPointer> q;
							
								/* We can start the analysis of a new cluster! */
								n=n+1;
								q.push(GhostSimplexPointer(*cp));
								while(!q.empty())
								{
									current=q.front();
									q.pop();
									this->ghostSimplex(current,&s);
									vlp->get(GhostSimplex(s),&b);
									if((*b)==false)
									{
										/* The current ghost simplex is not visited, then we proceed on its boundary! */
										(*b)=true;
										this->boundary(GhostSimplexPointer(current),&bl);
										for(temp=bl.begin();temp!=bl.end();temp++)
										{
											this->ghostSimplex(*temp,&s);
											lkp->get(GhostSimplex(s),&b);
											if((*b)==true) q.push(GhostSimplexPointer(*temp));
										}
																			
										/* And then, on its coboundary! */
										this->star(GhostSimplexPointer(current),&cl);
										for(temp=cl.begin();temp!=cl.end();temp++)
										{
											this->ghostSimplex(*temp,&s);
											lkp->get(GhostSimplex(s),&b);
											if((*b)==true) q.push(GhostSimplexPointer(*temp));
										}
									}
								}
							}
						}
						
						/* If we arrive here, we can finalize this member function! */
						this->deleteProperty(string("vis_lnk"));
						this->deleteProperty(string("inLink"));
					}
					
					/* If we arrive here, we can return 'n' */
					return n;
				}
				else { return 0; }
			}
			
			/// This member function checks if a simplex directly encoded in the current data structure is manifold.
			/**
			 * This member function checks if a simplex directly encoded in the current data structure is manifold: we say that a simplex is <i>manifold</i> if and only if its neighborhood is
			 * homeomorphic to a connected component of a sphere, otherwise it is called <i>non-manifold</i>.<p>The reference simplex must be <i>valid</i>, i.e. directly stored in the
			 * current data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. It is identified by instances of the
			 * mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>The
			 * <i>IA*</i> data structure is a <i>local</i> data structure, thus we cannot apply this member function for checking if a simplex is manifold: you should apply instances of the
			 * mangrove_tds::GhostSimplexPointer class for accessing descriptions of simplices not directly encoded in the current data structure. Consequently, this member function will
			 * fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param cp the reference simplex directly encoded in the current data structure
			 * \return <ul><li><i>true</i>, if the required simplex is manifold</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
			 * mangrove_tds::PropertyBase
			 */
			inline bool isManifold(const SimplexPointer& cp)
	 		{
	 			/* You cannot apply this member function - idle avoids compiler warnings! */
	 			idle(cp);
	 			assert(this->encodeAllSimplices()==true);
	 			return false;
	 		}
	 		
	 		/// This member function checks if a simplex not directly encoded in the current data structure is manifold.
			/**
			 * This member function checks if a simplex not directly encoded in the current data structure is manifold: we say that a simplex is <i>manifold</i> if and only if its
			 * neighborhood is homeomorphic to a connected component of a sphere, otherwise it is called <i>non-manifold</i>.<p>The reference simplex and the required simplices can also be
			 * not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member function
			 * only with a <i>local</i> data structure.<p>The <i>IA*</i> data structure is a <i>local</i> data structure.<p>If we cannot complete this operation, then this member function
			 * will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b>
			 * when you try to apply this member function, you should be careful about the use of auxiliary information (i.e. subclasses of the mangrove_tds::PropertyBase class) associated
			 * to the current simplicial complex. In other words, in this member function, we use a global property (i.e. an instance of the mangrove_tds::GlobalPropertyHandle class) with
			 * boolean values for expanding clusters of top simplices. We remove such property, if it already belongs to the current simplicial complex. In any case, we remove it at the end
			 * of this member function.
			 * \param cp the reference simplex not directly encoded in the current data structure
			 * \return <ul><li><i>true</i>, if the required simplex is manifold</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase,
			 * mangrove_tds::GlobalPropertyHandle, mangrove_tds::GhostPropertyHandle, mangrove_tds::LocalPropertyHandle
	 		 */
	 		inline bool isManifold(const GhostSimplexPointer& cp)
			{
				unsigned int t,ct,ci,n,n0,n1;
				GhostSimplex s;
				SimplexPointer c,c1;
				list<GhostSimplexPointer> cob;
				list<GhostSimplexPointer>::iterator it;
				vector< list<SimplexPointer> > inc;
				vector<SIMPLEX_TYPE> tops;
				
				/* First, we check if all is ok! */
				assert(this->isValid(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			assert(this->isTop(SimplexPointer(cp.getParentType(),cp.getParentId())));
	 			t=cp.getParentType();
				ct=cp.getChildType();
				ci=cp.getChildId();
				if(cp.isEncoded()==false)
				{
					/* We must have a valid child of a valid top simplex! */
					assert(ct<t);
					assert(ci<this->pascal_triangle[t+1][ct+1]);
					switch(ct)
					{
						case 0:
						
							/* The current ghost simplex is a vertex! */
							this->ghostSimplex(cp,&s);
							c=SimplexPointer(0,s.getBoundary().at(0).id());
							n=this->simplex(SimplexPointer(c)).getCoboundarySize();
							if(n>2) { return false; }
							else if(n==2)
							{
								/* If the two connected components are 1-dimensional, then this vertex is manifold! */
								if(this->simplex(SimplexPointer(c)).cc(0).type()!=1) return false;
								if(this->simplex(SimplexPointer(c)).cc(1).type()!=1) return false;
								return true;
							}
							else if(n==1)
							{
								/* First, we check if the unique cluster is 1-dimensional! */
								if(this->simplex(SimplexPointer(c)).cc(0).type()==1) return true;
								else
								{
									/* Now, we must check if all the incident edges are manifold! */
									this->retrieveImmediateIncidents(GhostSimplexPointer(cp),&cob);
									for(it=cob.begin();it!=cob.end();it++) { if(this->isManifold(GhostSimplexPointer(*it))==false) return false; }
									return true;
								}
							}
							else { return true; }

						case 1:
						
							/* The current ghost simplex is an edge, first we check if it is directly expressed a child of a top triangle, i.e. directly marked in the IA* */
							if(t==2) { return this->simplex(SimplexPointer(t,cp.getParentId())).isManifoldAdjacency(ci); }
							else
							{
								/* The current ghost simplex is an edge, not directly expressed as child of a top triangle! */
								this->ghostSimplex(GhostSimplexPointer(cp),&s);
	 							c=SimplexPointer(s.getCBoundary()[0]);
	 							c1=SimplexPointer(s.getCBoundary()[1]);
	 							n0=this->getCanBeIncidentsAtEdge(SimplexPointer(c));
	 							n1=this->getCanBeIncidentsAtEdge(SimplexPointer(c1));
	 							n=min(n0,n1);
								if(n==1) { return true; }
								else
								{
									/* We should investigate again! */
	 								n=this->getLinkComponentsNumber(GhostSimplexPointer(cp),&cob);
	 								if(n<2) return true;
	 								else if(n==2)
	 								{
	 									if(cob.size()!=2) return false;
	 									if(cob.front().getChildType()!=0) return false;
	 									if(cob.back().getChildType()!=0) return false;
	 									return true;
	 								}
	 								else { return false; }
								}
	 						}

						default:
						
							/* In the default case, we have a manifold simplex */
							return true;
					};
				}
				else { return true; }
			}

			protected:
			
			/// This member function returns the position of a ghost simplex in the boundary of a top simplex.
	 		/**
	 		 * This member function returns the position of a ghost simplex in the boundary of a top simplex: the position is local against the list of subfaces of the same dimension for the
			 * input top simplex.
	 		 * \param s a raw description of the input ghost simplex
	 		 * \param cc a pointer to the input top simplex
	 		 * \return the position of the input ghost simplex in the boundary of the input top simplex.
	 		 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::SimplexPointer, mangrove_tds::Simplex
	 		 */
	 		inline unsigned int getFace(const GhostSimplex &s, const SimplexPointer &cc)
	 		{
	 			unsigned int k,p;
	 			vector<SIMPLEX_ID> pos;
	 			
	 			/* First, we express the input ghost simplex as canonical vertices (against cc) */
	 			for(k=0;k<s.getCBoundary().size();k++)
	 			{
	 				this->simplex(SimplexPointer(cc)).positionInBoundary(SimplexPointer(s.getCBoundary().at(k)),p);
	 				pos.push_back(p);
	 			}

	 			/* Now, we check the input face between the subfaces of 'cc', plus a dummy return! */
	 			for(k=0;k<this->fposes[cc.type()].getHierarchy()[s.getCBoundary().size()-1].size();k++) 
	 				{ if(pos==this->fposes[cc.type()].getHierarchy()[s.getCBoundary().size()-1].at(k).getCVertices()) return k; }
	 			return 0;
	 		}
	 		
	 		/// This member function retrieves all top simplices incident to a vertex directly encoded in the current data structure.
	 		/**
	 		 * This member function retrieves all top simplices incident to a vertex directly encoded in the current data structure: we must expand the partial coboundary relation for the
			 * input vertex <i>v</i>, where we store a top simplex for each cluster incident to <i>v</i>.<p><b>IMPORTANT:</b> when you try to apply this member function, you should be careful
	 		 * about the use of auxiliary information (i.e. subclasses of the mangrove_tds::PropertyBase class) associated to the current simplicial complex. In other words, in this member
	 		 * function, we use a global property (i.e. an instance of the mangrove_tds::GlobalPropertyHandle class) with boolean values for expanding clusters of top simplices. We remove
			 * such property, if it already belongs to the current simplicial complex. In any case, we remove it at the end of this member function.
	 		 * \param cp a pointer to the input vertex to be analyzed
	 		 * \param ll the required top simplices incident to the input vertex
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::PropertyBase,
	 		 * mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle
	 		 */
	 		inline void retrieveIncidentTopSimplices(const SimplexPointer& cp,vector< list<SimplexPointer> > *ll)
	 		{
	 			GlobalPropertyHandle<bool> *asp;
	 			SimplexPointer ni,aux,aux1;
				unsigned int n,aaa;
				SIMPLEX_TYPE nit;

				/* We must expand the partial coboundary relation for the input vertex 'cp', where we store a top simplex for each cluster incident to 'cp' */
	 			if(this->hasProperty(string("assigned"))) this->deleteProperty(string("assigned"));
	 			asp=this->addGlobalProperty(string("assigned"),false);
	 			n=this->simplex(cp).getCoboundarySize();
	 			for(unsigned int i=0;i<n;i++)
				{
					queue<SimplexPointer> q;

					/* Now, we start from an element in its coboundary! */
					q.push(SimplexPointer(this->simplex(cp).cc(i)));
					while(!q.empty())
					{
						/* Now, we extract the first element from q */
						ni=q.front();
						q.pop();
						nit=ni.type();
						if(asp->get(SimplexPointer(ni))==false)
						{
							asp->set(SimplexPointer(ni),true);
							(*ll)[nit-1].push_back(SimplexPointer(ni));
							if(nit>cp.type()+1)
							{
								/* The current simplex 'ni' is not a wire-edge, thus we can visit its adjacency! */
								this->simplex(ni).positionInBoundary(cp,aaa);
								for(unsigned int k=0;k<=nit;k++)
								{
									/* Now, we check if we must forward on the k-th adjacency! */
									if((k!=aaa) && (this->simplex(ni).hasAdjacency(k)==true))
									{
										/* Now, the adjacency along the k-th face exists: we check if it is manifold! */
										if(this->simplex(ni).isManifoldAdjacency(k)==true) { q.push(SimplexPointer(this->simplex(ni).a(k))); }
										else
										{
											/* Now, we take the non-manifold simplex, i.e. the k-th face (non-manifold). We can consider its co-boundary */
											aux=SimplexPointer(this->simplex(ni).a(k));
											for(unsigned int i=0;i<this->simplex(aux).getCoboundarySize();i++)
											{
												aux1=SimplexPointer(this->simplex(aux).c(i));
												if(aux1==cp) { ; }
												else { q.push(SimplexPointer(aux1)); }
											}
										}
									}
								}
							}
						}
					}
				}
	 			
	 			/* If we arrive here, we can finalize this member function! */
	 			this->deleteProperty(string("assigned"));
	 		}
	 		
	 		/// This member function retrieves all top simplices incident to a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function retrieves all top simplices incident to a simplex not directly encoded in the current data structure: we must expand the partial coboundary relation for
			 * the first vertex <i>v</i> in the input simplex, where we store a top simplex for each cluster incident to <i>v</i>.<p><b>IMPORTANT:</b> when you try to apply this member
			 * function, you should be careful about the use of auxiliary information (i.e. subclasses of the mangrove_tds::PropertyBase class) associated to the current simplicial complex.
			 * In other words, in this member function, we use a global property (i.e. an instance of the mangrove_tds::GlobalPropertyHandle class) with boolean values for expanding clusters
			 * of top simplices. We remove such property, if it already belongs to the current simplicial complex. In any case, we remove it at the end of this member function.
	 		 * \param gp a pointer to the input simplex to be analyzed
	 		 * \param ll the required top simplices incident to the input vertex
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::GhostSimplexPointer, mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::GC_COMPLEX, mangrove_tds::PropertyBase,
	 		 * mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::BaseSimplicialComplex::addLocalProperty(), mangrove_tds::addGlobalProperty(),
	 		 * mangrove_tds::BaseSimplicialComplex::deleteProperty()
	 		 */
	 		inline void retrieveIncidentTopSimplices(const GhostSimplexPointer& gp,vector< list<SimplexPointer> > *ll)
	 		{
	 			GlobalPropertyHandle<bool> *asp;
	 			GhostSimplex s;
				unsigned int n,aaa;
				bool a;
				SimplexPointer cp,ni,aux,aux1;
				vector<SimplexPointer> vs;
				SIMPLEX_TYPE nit;
				
	 			/* Now, we must expand the partial coboundary relation of the first vertex in 'cp' */
				if(this->hasProperty(string("assigned"))) this->deleteProperty(string("assigned"));
	 			asp=this->addGlobalProperty(string("assigned"),false);
	 			this->ghostSimplex(gp,&s);
				cp=SimplexPointer(s.getCBoundary().at(0));
				for(unsigned int k=0;k<=gp.getChildType();k++) vs.push_back(SimplexPointer(s.getCBoundary().at(k)));
				n=this->simplex(cp).getCoboundarySize();
				for(unsigned int i=0;i<n;i++)
				{
					queue<SimplexPointer> q;

					/* Now, we start from an element in its coboundary! */
					q.push(SimplexPointer(this->simplex(cp).cc(i)));
					while(!q.empty())
					{
						/* Now, we extract the first element from q */
						ni=q.front();
						q.pop();
						nit=ni.type();
						if(asp->get(SimplexPointer(ni))==false)
						{
							asp->set(SimplexPointer(ni),true);
							if(this->isIncidentInVertices(GhostSimplexPointer(nit,ni.id()),vs)==true) (*ll)[nit-1].push_back(SimplexPointer(ni));
							if(nit>cp.type()+1)
							{
								/* The current simplex 'ni' is not a wire-edge, thus we can visit its adjacency! */
								a=this->simplex(ni).positionInBoundary(cp,aaa);
								for(unsigned int k=0;k<=nit;k++)
								{
									/* Now, we check if we must forward on the k-th adjacency! */
									if( (k!=aaa) && (this->simplex(ni).hasAdjacency(k)==true))
									{
										/* Now, the adjacency along the k-th face exists: we check if it is manifold! */
										if(this->simplex(ni).isManifoldAdjacency(k)==true) { q.push(SimplexPointer(this->simplex(ni).a(k))); }
										else
										{
											/* Now, we take the non-manifold simplex, i.e. the k-th face (non-manifold). We can consider its co-boundary */
											aux=SimplexPointer(this->simplex(ni).a(k));
											for(unsigned int i=0;i<this->simplex(aux).getCoboundarySize();i++)
											{
												aux1=SimplexPointer(this->simplex(aux).c(i));
												if(aux1==cp) { ; }
												else { q.push(SimplexPointer(aux1)); }
											}
										}
									}
								}
							}
						}
					}
				}
		
				/* If we arrive here, we can finalize this member function! */
	 			this->deleteProperty(string("assigned"));
	 		}
	 		
	 		/// This member function checks if a ghost simplex is incident to some vertices.
	 		/**
	 		 * This member function checks if a ghost simplex is incident to some vertices, i.e. it checks if a list of vertices belongs to its boundary.<p><b>IMPORTANT:</b> all the input
			 * vertices must belong to the boundary of the input ghost simplex.
	 		 * \param gs a pointer to the input ghost simplex to be checked
	 		 * \param vs the input list of vertices to be checked
	 		 * \return <ul><li><i>true</i>, if the input ghost simplex is incident to all the input vertices</li><li><i>false</i>, otherwise</li></ul>
	 		 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove::GIA::atLeastOneVertexIncident()
	 		 */
	 		inline bool isIncidentInVertices(const GhostSimplexPointer& gs, vector<SimplexPointer> &vs)
	 		{
	 			GhostSimplex s;
	 			
	 			/* First, we extract a raw definition and then we loop over 'vs' */
	 			this->ghostSimplex(GhostSimplexPointer(gs),&s);
	 			for(vector<SimplexPointer>::iterator it=vs.begin();it!=vs.end();it++) if(s.isIncident(SimplexPointer(*it))==false) return false;
	 			return true;
	 		}
	 		
	 		/// This member function retrieves all the top simplices adjacent to a top simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function retrieves all the top simplices adjacent to a top simplex directly encoded in the current data structure: two 0-simplices are <i>adjacent</i> if they are
	 		 * connected by a common edge, while two k-simplices (with k>0) are <i>adjacent</i> if they share a simplex of dimension k-1. Simplices identified by this member function are top
	 		 * simplices and thus they are directly encoded in the current data structure.<p><b>IMPORTANT:</b> you cannot apply this member function for identifying adjacent edges to a wire
	 		 * edge.
	 		 * \param cp a pointer to the required top simplex directly encoded in the current data structure
	 		 * \param ll the required top simplices adjacent to the input one
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
	 		 */
	 		inline void retrieveAdjacentTopSimplices(const SimplexPointer& cp, list<SimplexPointer> *ll)
	 		{
	 			SimplexPointer aux,aux1;

	 			/* First, we clear 'll' and then we extract all the top simplices adjacent to 'cp' */
	 			ll->clear();
	 			for(unsigned int k=0;k<=cp.type();k++)
				{
					/* Now, we check if the adjacency along the k-th face exists! */
					if(this->simplex(cp).hasAdjacency(k)==true)
					{
						/* Now, the adjacency along the k-th face exists: we check if it is manifold! */
						if(this->simplex(cp).isManifoldAdjacency(k)==true) { ll->push_back(SimplexPointer(cp.type(),this->simplex(cp).a(k).id())); }
						else
						{
							/* Now, we take the non-manifold simplex, i.e. the k-th face (non-manifold) */
							aux=SimplexPointer(this->simplex(cp).a(k));
							for(unsigned int i=0;i<this->simplex(aux).getCoboundarySize();i++)
							{
								aux1=SimplexPointer(this->simplex(aux).c(i));
								if(aux1.id()!=cp.id()) { ll->push_back(SimplexPointer(aux1)); }
							}
						}
					}
				}
	 		}
	 		 
	 		/// This member function retrieves all the top simplices adjacent to a top simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function retrieves all the top simplices adjacent to a top simplex directly encoded in the current data structure: two 0-simplices are <i>adjacent</i> if they are
	 		 * connected by a common edge, while two k-simplices (with k>0) are <i>adjacent</i> if they share a simplex of dimension k-1. Simplices identified by this member function are top
	 		 * simplices and thus they are directly encoded in the current data structure: however, they are also accessed through an instance of the mangrove_tds::GhostSimplexPointer
	 		 * class.<p><b>IMPORTANT:</b> you cannot apply this member function for identifying edges adjacent to a wire edge.
	 		 * \param cp a pointer to the required top simplex directly encoded in the current data structure
	 		 * \param ll the required top simplices adjacent to the input one
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
	 		 */
	 		inline void retrieveAdjacentTopSimplices(const SimplexPointer& cp, list<GhostSimplexPointer> *ll)
	 		{
	 			SimplexPointer aux,aux1;

	 			/* First, we clear 'll' and then we extract all the top simplices adjacent to 'cp' */
	 			ll->clear();
	 			for(unsigned int k=0;k<=cp.type();k++)
				{
					/* Now, we check if the adjacency along the k-th face exists! */
					if(this->simplex(cp).hasAdjacency(k)==true)
					{
						/* Now, the adjacency along the k-th face exists: we check if it is manifold! */
						if(this->simplex(cp).isManifoldAdjacency(k)==true) { ll->push_back(GhostSimplexPointer(cp.type(),this->simplex(cp).a(k).id())); }
						else
						{
							/* Now, we take the non-manifold simplex, i.e. the k-th face (non-manifold) */
							aux=SimplexPointer(this->simplex(cp).a(k));
							for(unsigned int i=0;i<this->simplex(aux).getCoboundarySize();i++)
							{
								aux1=SimplexPointer(this->simplex(aux).c(i));
								if(aux1.id()!=cp.id()) { ll->push_back(GhostSimplexPointer(aux1.type(),aux1.id())); }
							}
						}
					}
				}
	 		}
	 		
	 		/// This member function retrieves all simplices immediate incident in a ghost simplex in the current data structure.
	 		/**
	 		 * This member function retrieves all simplices immediate incident in a ghost simplex in the current data structure, i.e. simplices with dimension immediately superior to the
			 * input one. In other words, we have an input ghost 2-simplex, then we require all 3-simplices incident to the input one.<p><b>IMPORTANT:</b> when you try to apply this member
			 * function, you should be careful about the use of auxiliary information (i.e. subclasses of the mangrove_tds::PropertyBase class) associated to the current simplicial complex.
			 * In other words, in this member function, we use a global property (i.e. an instance of the mangrove_tds::GlobalPropertyHandle class) with boolean values for expanding clusters
			 * of top simplices. We remove such property, if it already belongs to the current simplicial complex. In any case, we remove it at the end of this member function.
	 		 * \param cp the input ghost simplex to be analyzed
	 		 * \param ll the required simplices
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::GhostSimplexPointer, mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::GC_COMPLEX, mangrove_tds::PropertyBase,
	 		 * mangrove_tds::LocalPropertyHandle, mangrove_tds::GlobalPropertyHandle, mangrove_tds::BaseSimplicialComplex::addLocalProperty(), mangrove_tds::addGlobalProperty(),
	 		 * mangrove_tds::BaseSimplicialComplex::deleteProperty() 
	 		 */
	 		inline void retrieveImmediateIncidents(const GhostSimplexPointer &cp, list<GhostSimplexPointer> *ll)
	 		{
	 			unsigned int t,ct,ci,pos;
	 			vector< list<SimplexPointer> > inc;
				GhostSimplex s;
				SimplexPointer v;
				SIMPLEX_TYPE d;
				list<SimplexPointer>::iterator it;
				vector<SIMPLEX_ID> cob;

	 			/* First, we check if all is ok! */
	 			ll->clear();
	 			t=cp.getParentType();
				ct=cp.getChildType();
				ci=cp.getChildId();
				if(ct==this->type()) { return; }
				if(cp.isEncoded()==true) { return; }
				for(d=0;d<=this->type();d++) inc.push_back( list<SimplexPointer>() );
				this->ghostSimplex(GhostSimplexPointer(cp),&s);
				if(ct==0)
				{
					v=SimplexPointer(s.getBoundary()[0]);
					this->retrieveIncidentTopSimplices(SimplexPointer(v),&inc);
				}
				else { this->retrieveIncidentTopSimplices(GhostSimplexPointer(cp),&inc); }
				for(d=0;d<=this->type();d++)
				{
					for(it=inc[d].begin();it!=inc[d].end();it++)
					{
						if(it->type()==1) { ll->push_back(GhostSimplexPointer(it->type(),it->id())); }
						else
						{
							pos=this->getFace(GhostSimplex(s),SimplexPointer(*it));
							cob=vector<SIMPLEX_ID>(this->fposes[it->type()].getHierarchy()[ct].at(pos).getCCoboundary());
							for(vector<SIMPLEX_ID>::iterator a=cob.begin();a!=cob.end();a++) { ll->push_back(GhostSimplexPointer(it->type(),it->id(),ct+1,(*a))); }
						}
					}
				}		
	 		}
	 		
	 		/// This member function checks if at least one vertex of a ghost simplex is a reference vertex.
	 		/**
	 		 * This member function checks if at least one vertex of a ghost simplex is a reference vertex, i.e. a vertex marked through the local property (an instance of the
	 		 * mangrove_tds::LocalPropertyHandle class) for vertices, called <i>refVertex</i>.
	 		 * \param s the input ghost simplex to be checked
	 		 * \param rvp a component for accessing the <i>refVertex</i> local property
	 		 * \return <ul><li><i>true</i>, at least one vertex of the input ghost simplex is a reference vertex</li><li><i>false</i>, otherwise</li></ul>
	 		 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::LocalPropertyHandle,
	 		 * mangrove::GIA::isIncidentInVertices()
	 		 */
	 		inline bool atLeastOneVertexIncident(const GhostSimplex &s,LocalPropertyHandle<bool> *rvp)
	 		{
	 			unsigned int k;

	 			/* We must check if at least ONE vertex of 's' is marked as 'refVertex' */
	 			for(k=0;k<s.getCBoundary().size();k++) { if(rvp->get(SimplexPointer(s.getCBoundary().at(k)))==true) return true; }
	 			return false;
	 		}
	 		
	 		/// This member function extracts all top simplices in the partial co-boundary relation of a simplex that can be incident in an edge.
	 		/**
	 		 * This member function extracts all top simplices in the partial co-boundary relation of a simplex that can be incident in an edge.
	 		 * \param cp a pointer to the required simplex
	 		 * \return the number of top simplices in the partial co-boundary relation of a simplex that can be incident in an edge
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex
	 		 */
	 		inline unsigned int getCanBeIncidentsAtEdge(const SimplexPointer &cp)
	 		{
	 			unsigned int k,n,l;
	 			
	 			/* First, we should extract all the top simplices of dimension greater than '1' in the partial co-boundary relation of 'cp' */
	 			n=0;
	 			l=this->simplex(SimplexPointer(cp)).getCoboundarySize();
	 			for(k=0;k<l;k++) { if(this->simplex(SimplexPointer(cp)).cc(k).type()>1) n=n+1; }
	 			return n;
	 		}
	 	};
	}

#endif

