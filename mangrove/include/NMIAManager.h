/**********************************************************************************************************************
 * This source code belongs to the Mangrove TDS Library                                   
 *  		    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * October 2011 (Revised on May 2012)
 *                                                               
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * NMIAManager.h - the specialized component for executing I/O operations on the Non-Manifold IA data structure
 ********************************************************************************************************************/

/* Should we include this header file? */
#ifndef NMIA_MANAGER_H

	#define NMIA_MANAGER_H

	#include "NMIA.h"
	#include <list>
	#include <map>
	#include <queue>
	#include <algorithm>
	#include <cstdlib>
	using namespace std;
	using namespace mangrove_tds;
	
	/// The specialized component for executing I/O operations on the <i>Non-Manifold IA</i> data structure.
	/**
	 * The class defined in this file allows to execute I/O operations on the <i>Non-Manifold IA</i> data struture.<p>The <i>Non-Manifold IA</i> data structure, described by the mangrove_tds::NMIIA
	 * class, is a dimension-specific data structure for representing simplificial 3-complexes with an arbitrary domain. It extends the <i>IA</i> data structure and it is scalable to manifold
	 * complexes, and supports efficient navigation and topological modifications. The <i>Non-Manifold IA</I> data structure encodes only top simplices plus a suitable subset of the adjacency
	 * relations, thus it is a <i>local</i> data structure.<p>The effective I/O operations on the <i>Non-Manifold IA</i> data structure are delegated to a particular instance of the
	 * mangrove_tds::DataManager class in order to support a wide range of I/O formats.
	 * \file NMIAManager.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	
	/* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		/// The specialized component for executing I/O operations on the <i>Non-Manifold IA</i> data structure.
		/**
		 * The class defined in this file allows to execute I/O operations on the <i>Non-Manifold IA</i> data struture.<p>The <i>Non-Manifold IA</i> data structure, described by the
		 * mangrove_tds::NMIIA class, is a dimension-specific data structure for representing simplificial 3-complexes with an arbitrary domain. It extends the <i>IA</i> data structure and it is
		 * scalable to manifold complexes, and supports efficient navigation and topological modifications. The <i>Non-Manifold IA</I> data structure encodes only top simplices plus a suitable
		 * subset of the adjacency relations, thus it is a <i>local</i> data structure.<p>The effective I/O operations on the <i>Non-Manifold IA</i> data structure are delegated to a particular
		 * instance of the mangrove_tds::DataManager class in order to support a wide range of I/O formats.
	 	 * \see mangrove_tds::DataManager, mangrove_tds::IO, mangrove_tds::NMIA, mangrove_tds::BaseSimplicialComplex
	 	 */
		class NMIAManager : public IO<NMIAManager>
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a not initialized instance of this class: you should apply the mangrove_tds::NMIAManager::createComplex() member functions in order to initialize 				 * it.
			 * \see mangrove_tds::NMIAManager::createComplex(), mangrove_tds::NMIA, mangrove_tds::BaseSimplicialComplex
			 */
			inline NMIAManager() : IO<NMIAManager>() { this->ccw = NULL; }
			
			/// This member function destroys an instance of this class.
			/**
			 * This member function destroys an instance of this class. The specialized data structure for the simplicial complex is created in the mangrove_tds::NMIAManager::createComplex()
			 * member function, thus you must not manually destroy it: you should apply this destructor or the mangrove_tds::NMIAManager::close() member function in order to destroy all the
			 * internal state.
			 * \see mangrove_tds::NMIAManager::createComplex(), mangrove_tds::NMIAManager::close()
			 */
			inline virtual ~NMIAManager() { this->close(); }
			
			/// This member function destroys all the internal state in this component.
			/**
			 * This member function destroys all the internal state in this component. The specialized data structure for the simplicial complex is created in the
			 * mangrove_tds::NMIAManager::createComplex() member function, thus you must not manually destroy it: you should apply the destructor or this member function in order to destroy
			 * all the internal state.
			 * \see mangrove_tds::NMIAManager::createComplex(), mangrove_tds::NMIA, mangrove_tds::BaseSimplicialComplex
			 */
			inline void close()
			{
				/* Now, we reset the current state! */
				if(this->ccw!=NULL) delete this->ccw;
				this->ccw=NULL;
			}
			
			/// This member function returns the simplicial complex created by the current component.
			/**
			 * This member function returns the simplicial complex created in the mangrove_tds::NMIAManager::createComplex() member function. The current instance of the
			 * <i>Non-Manifold IA</I> data structure has been created in the mangrove_tds::NMIAManager::createComplex() member function, thus you must not manually destroy it. You should 
			 * apply the destructor or the mangrove_tds::NMIAManager::close() member function in order to destroy all the internal state. Thus, the input pointer must be a C++ pointer to an
			 * instance of the mangrove_tds::NMIA class.<p>If such conditions are not satisfied, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled 
			 * in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param cs the simplicial complex created by this component
			 * \see mangrove_tds::NMIAManager::close(), mangrove_tds::NMIAManager::createComplex(), mangrove_tds::NMIA, mangrove_tds::BaseSimplicialComplex
			 */
			template <class D> void getComplex(D** cs)
			{
				D *aux;
				
				/* First, we check if all is ok! */
				assert(this->ccw!=NULL);
				aux = dynamic_cast<D*>(this->ccw);
				assert(aux!=NULL);
				(*cs) = this->ccw;
			}
			
			/// This member function returns a string that describes the data structure created by the current I/O component.
 			/**
 			 * This member function returns a string that describes the data structure created by the current I/O component, and used for encoding the current simplicial complex.
			 * \return a string that describes the data structure created by the current I/O component
 			 * \see mangrove_tds::BaseSimplicialComplex::getDataStructureName()
 			 */
 			inline string getDataStructureName() { return string("Non-Manifold IA (NMIA)"); }
 			
 			/// This member function creates a simplicial complex by reading it from a file.
			/**
			 * This member function generates a <i>Non-Manifold IA</I> data structure from a file that contains a possible representation: the most common exchange format for a simplicial
			 * complex consists of a collection of <i>top</i> simplices described by their vertices, but it is not the unique possibility.<p>The <i>Non-Manifold IA</i> data structure,
			 * described by the mangrove_tds::NMIIA class, is a dimension-specific data structure for representing simplificial 3-complexes with an arbitrary domain. It extends the <i>IA</i>
			 * data structure and it is scalable to manifold complexes, and supports efficient navigation and topological modifications. The <i>Non-Manifold IA</I> data structure encodes only
			 * top simplices plus a suitable subset of the adjacency relations, thus it is a <i>local</i> data structure.<p>In this member function we can customize the required simplicial
			 * complex: for instance, we can attach the Euclidean coordinates and the field value for each vertex in the simplicial complex to be built (if supported in the I/O component).
			 * Such information is stored as local properties (i.e. as subclasses of the mangrove_tds::LocalPropertyHandle class) associated to vertices. You can retrieve them through their
			 * names, respectively provided by the mangrove_tds::DataManager::getPropertyName4Coordinates() and mangrove_tds::DataManager::getPropertyName4FieldValue() member function.
			 * Moreover, you can check what properties are supported through the mangrove_tds::DataManager::supportsEuclideanCoordinates() and mangrove_tds::DataManager::supportsFieldValues()
			 * member functions.<p>If we cannot complete these operations for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in
			 * debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> the specialized data structure for the simplicial complex is created in
			 * this member function, thus you must not manually destroy it. You should apply the destructor or mangrove_tds::NMIAManager::close() member function in order to destroy all the
			 * internal state.
			 * \param loader a component for the effective data reading
	 	 	 * \param fname the path of the input file
			 * \param reqParams this boolean flag identfies if the loading of the auxiliary parameters encoded in the input file is enabled
			 * \param ld this boolean flag indicates if the effective reading of the input data must be performed
			 * \param checksOn this boolean flag indicates if the effective checks on the input data must be performed (valid only if the effective reading must be performed)
			 * \see mangrove_tds::DataManager, mangrove_tds::NMIA, mangrove_tds::RawFace, mangrove_tds::RawGhostSimplex, mangrove_tds::PropertyBase,
			 * mangrove_tds::LocalPropertyHandle, mangrove_tds::BaseSimplicialComplex, mangrove_tds::NMIAManager::close(), mangrove_tds::NMIAManager::identifyClustersElements(),
			 * mangrove_tds::NMIAManager::computeAdjacency(), mangrove_tds::NMIAManager::fixEdgeBasedClusters()
			 */
			template <class T> void createComplex(DataManager<T> *loader, string fname,bool reqParams,bool ld,bool checksOn)
			{
				bool b;
				double aaa;
				LocalPropertyHandle< Point<double> > *pnt_prop;
				LocalPropertyHandle<double> *fld_prop;
				Point<double> p;
				SimplexPointer cp;
				SimplexIterator it;
				unsigned int k,i;
				list<GhostSimplexPointer> lk;
				GhostSimplex s;
				vector<RawIncidence> incidents;
 				vector<RawFace> raw_faces;
 				vector<RawGhostSimplex> raw_edges,edges;
 				
				/* First, we check if all is ok and then we run the effective construction! */
				assert(loader!=NULL);
				assert(reqParams);
				this->close();
				if(reqParams==false) { return; }
				if(ld) loader->loadData(fname,true,false,checksOn);			
				b=(loader->supportsEuclideanCoordinates()==true) || (loader->supportsFieldValues()==true);
				assert(b==true);
				if(b==false) { return; }
				assert(loader->getDimension()==3);
				if(loader->getDimension()!=3) { return; }
				try { this->ccw = new NMIA(3); }
				catch(bad_alloc ba) { this->ccw = NULL; }
				assert(this->ccw!=NULL);
				aaa=0.0;
				pnt_prop=NULL;
				fld_prop=NULL;
				if(loader->supportsEuclideanCoordinates())
				{
					pnt_prop=this->ccw->addLocalProperty(0,string(loader->getPropertyName4Coordinates()), Point<double>());
					this->ccw->getPropertyName4Coordinates()=string(loader->getPropertyName4Coordinates());
				}
				
				/* Now, we check the field values! */
				if(loader->supportsFieldValues())
				{
					fld_prop=this->ccw->addLocalProperty(0,string(loader->getPropertyName4FieldValue()),aaa); 
					this->ccw->getPropertyName4FieldValue()=string(loader->getPropertyName4FieldValue());
				}
				
				/* Now, we can add all the vertices and their properties! */
				it = loader->getCollection(0).begin();
				k=0;
				while(it!=loader->getCollection(0).end())
				{
					/* Now, we insert the k-vertex in 'ccw' and its Euclidean coordinates, if supported! */
					this->ccw->addSimplex(cp);
					incidents.push_back(RawIncidence());
					if(loader->supportsEuclideanCoordinates())
					{
						loader->getCoordinates(k,p);
						pnt_prop->set(cp,Point<double>(p));
					}
						
					/* Now, we retrieve the field values, if supported! */
					if(loader->supportsFieldValues())
					{
						loader->getFieldValue(k,aaa);
						fld_prop->set(cp,aaa);
					}
	
					/* Now, we move on the next vertex! */
					k=k+1;
					it++;
				}
				
				/* Now, we can add the other top simplices and initialize auxiliary data structures */
				for(SIMPLEX_TYPE d=1;d<=3;d++)
				{
					/* Now, we add all the top d-simplices! */
					for(it=loader->getCollection(d).begin();it!=loader->getCollection(d).end();it++)
					{
						/* Now, we add a new top simplex and then we update its boundary relation! */
						this->ccw->addSimplex(d,cp);
						for(k=0;k<=d;k++)
						{
							i=loader->getCollection(d).simplex(cp.id()).b(k).id();
							this->ccw->simplex(cp).add2Boundary(SimplexPointer(0,i),k);
							if(cp.type()==1) { this->ccw->simplex(SimplexPointer(0,i)).add2Coboundary(SimplexPointer(cp)); }
							else { incidents[i].addSimplex(SimplexPointer(cp)); }
							if(cp.type()==3)
							{
								raw_faces.push_back(RawFace());
								raw_faces.back().isTopSimplexChild()=true;
								raw_faces.back().getIndex()=cp.id();
								raw_faces.back().getReverseIndex()=k;
								for(i=0;i<k;i++) { raw_faces.back().getVertices().push_back(loader->getCollection(d).simplex(cp.id()).b(i).id()); }
								for(i=k+1;i<=d;i++) { raw_faces.back().getVertices().push_back(loader->getCollection(d).simplex(cp.id()).b(i).id()); }
							}
						}	
							
						/* Now, we check if its dimension cp.type>1! */
						if(cp.type()>1)
						{
							/* Now, we extract all the edges of the current top simplex, and then we extract auxiliary descriptions of 'edges' */
							this->ccw->boundaryk(GhostSimplexPointer(cp.type(),cp.id()),1,&lk);
							for(list<GhostSimplexPointer>::iterator e=lk.begin();e!=lk.end();e++)
							{
								raw_edges.push_back(RawGhostSimplex());
								raw_edges.back().getAliases().push_back(GhostSimplexPointer(*e));
								this->ccw->ghostSimplex(GhostSimplexPointer(*e),&s);
								raw_edges.back().getVertices().push_back(s.getCBoundary().at(0).id());
								raw_edges.back().getVertices().push_back(s.getCBoundary().at(1).id());
							}
						}
					}
				}
				
				/* Now, we complete the NMIA data structure! */
				this->identifyClustersElements(raw_edges,edges);
				raw_edges.clear();
				this->computeAdjacency(raw_faces);
				raw_faces.clear();
				this->fixCoboundary(incidents,edges);
				incidents.clear();
				this->fixEdgeBasedClusters(edges);
				edges.clear();
			}

			/// This member function exports the current simplicial complex in accordance with a certain format.
			/**
			 * This member function exports the current simplicial complex, by writing its representation in accordance with a specific file format.<p>The effective writing is delegated to a
			 * subclass of the mangrove_tds::DataManager class and some further conditions could be checked and verified: for example, the current simplicial complex must support the
			 * extraction of particular auxiliary information (like the Euclidean coordinates and/or scalar field values, described by subclasses of the mangrove_tds::PropertyBase class). If
			 * such properties are not satisfied, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.
			 * \param writer a component for the effective data writing (we write the data in this member function)
			 * \param fname the path of the output file
			 * \see mangrove_tds::DataManager, mangrove_tds::IO::close(), mangrove_tds::BaseSimplicialComplex, mangrove_tds::PropertyBase
			 */
			template <class T> void writeComplex(DataManager<T> *writer,string fname)
			{
				/* First, we check if all is ok! */
				assert(writer!=NULL);
				writer->writeData(this->ccw,fname);
			}
			
			protected:
			
			/// The simplicial complex created by this component.
			/**
			 * \see mangrove_tds::NMIA
			 */
			NMIA* ccw;
			
			/// This member function identifies top simplices sharing an edge in the input simplicial shape.
			/**
			 * This member function identifies top simplices sharing an edge in the input simplicial shape: here, given an edge, we can say what top simplices share this edge through
			 * instances of the mangrove_tds::RawGhostSimplex class.
			 * \param raw_edges the list of all edges in the input simplicial shape (with replicants)
			 * \param edges the unique list of edge in the input simplicial shape (without replicants)
			 * \see mangrove_tds::RawGhostSimplex
			 */
			inline void identifyClustersElements(vector<RawGhostSimplex> &raw_edges, vector<RawGhostSimplex> &edges)
			{
				unsigned int r,c;
				vector<unsigned int> cs;
				vector<unsigned int>::iterator it;

				/* First, we clear 'edges' and then we sort 'raw_edges' */
				edges.clear();
				sort(raw_edges.begin(),raw_edges.end(),compare_ghost_simplices);
				r=0;
				c=1;
				cs.clear();
				while( (r<raw_edges.size()) && (c<raw_edges.size()))
				{
					/* Now, we must identify 'connected portions' of simplices incident in the current edge. */
					if(same_ghost_simplices(raw_edges[r],raw_edges[c]))
					{
						/* We accumulate replicants! */
						cs.push_back(c);
						c=c+1;
					}
					else
					{
						/* We must create a new edge with top simplices and the extended adjacency for top triangles along an edge */
						cs.push_back(r);
						edges.push_back(RawGhostSimplex());
						edges.back().getVertices()=vector<unsigned int>(raw_edges[r].getVertices());
						for(it=cs.begin();it!=cs.end();it++) { edges.back().getAliases().push_back(GhostSimplexPointer(raw_edges[*it].getAliases()[0])); }
						r=c;
						c=r+1;
						cs.clear();	
					}
				}
				
				/* Now, we must complete the construction, finalizing the last cluster! */
				if(r<raw_edges.size())
				{
					/* We must create a new edge with top simplices */
					cs.push_back(r);
					edges.push_back(RawGhostSimplex());
					edges.back().getVertices()=vector<unsigned int>(raw_edges[r].getVertices());
					for(it=cs.begin();it!=cs.end();it++) { edges.back().getAliases().push_back(GhostSimplexPointer(raw_edges[*it].getAliases()[0])); }
				}
			}
			
			/// This member function retrieves the complete adjacency relation for all tetrahedra in the input simplicial complex.
			/**
			 * This member function retrieves the complete adjacency relation for all tetrahedra in the input simplicial complex, starting from a raw description of their faces of dimension
			 * 2.<p><b>IMPORTANT:</b> here, we directly update tetrahedra, and in particular, we update their adjacency relation.
			 * \param raw_faces a raw description of tetrahedra faces of dimension 2
			 * \see mangrove_tds::RawFace, mangrove_tds::RawAdjacency, mangrove_tds::RawGhostSimplex, mangrove_tds::Simplex
			 */
			inline void computeAdjacency(vector<RawFace> &raw_faces)
			{
				vector<unsigned int> cs;
				unsigned int r,c,fi,ri,fj;

				/* First, we sort 'raw_faces' in order to identify adjacent 2-faces */
				cs.clear();
				sort(raw_faces.begin(),raw_faces.end(),compare_faces);
				r=0;
				c=1;
				while( (r<raw_faces.size()) && (c<raw_faces.size()))
				{
					/* Now, we must identify connected components of faces! */
					if(same_faces(raw_faces[r],raw_faces[c]))
					{
						/* We accumulate replicants! */
						cs.push_back(c);
						c=c+1;
					}
					else
					{
						/* We mus adjust adjancencies! */
						if(cs.size()!=0)
						{
							/* We must store adjacent faces (i.e. adjacent tetrahedra) */
							cs.push_back(r);
							for(unsigned int i=0;i<cs.size();i++)
							{
								/* Now, we analyze adjacencies before 'i' */
								fi=raw_faces[cs[i]].getIndex();
								ri=raw_faces[cs[i]].getReverseIndex();
								for(unsigned int j=0;j<i;j++)
								{
									fj=raw_faces[cs[j]].getIndex();
									this->ccw->simplex(SimplexPointer(3,fi)).add2Adjacency(SimplexPointer(3,fj),ri);
								}

								/* And the, we proceed after 'i' */
								for(unsigned j=i+1;j<cs.size();j++)
								{
									fj=raw_faces[cs[j]].getIndex();
									this->ccw->simplex(SimplexPointer(3,fi)).add2Adjacency(SimplexPointer(3,fj),ri);
								}
							}
						}
						
						/* We reset auxiliary data structures for continuing...." */
						r=c;
						c=r+1;
						cs.clear();
					}
				}
				
				/* Now, we must complete the construction, finalizing the last cluster! */
				if(r<raw_faces.size())
				{
					/* We mus adjust adjancencies! */
					if(cs.size()!=0)
					{
						/* We must store adjacent faces (i.e. adjacent tetrahedra) */
						cs.push_back(r);
						for(unsigned int i=0;i<cs.size();i++)
						{
							/* Now, we analyze adjacencies before 'i' */
							fi=raw_faces[cs[i]].getIndex();
							ri=raw_faces[cs[i]].getReverseIndex();
							for(unsigned int j=0;j<i;j++)
							{
								fj=raw_faces[cs[j]].getIndex();
								this->ccw->simplex(SimplexPointer(3,fi)).add2Adjacency(SimplexPointer(3,fj),ri);
							}

							/* And the, we proceed after 'i' */
							for(unsigned int j=i+1;j<cs.size();j++)
							{
								fj=raw_faces[cs[j]].getIndex();
								this->ccw->simplex(SimplexPointer(3,fi)).add2Adjacency(SimplexPointer(3,fj),ri);
							}
						}
					}
				}
			}
			
			/// This member function retrieves the partial co-boundary relation for each vertex in the input simplicial complex.
			/**
			 * This member function retrieves the partial co-boundary relation for each vertex in the input simplicial complex, by starting from the auxiliary information of the top simplices
			 * incident in a vertex, and from the extended adjacency relation for top 2-simplices. Adjacency relation for tetrahedra is already encoded in the related instance of the
			 * mangrove_tds::Simplex class.
			 * \param incidents the auxiliary information of the top simplices incident in a vertex
			 * \param edges the unique list of edge in the input simplicial shape (without replicants)
			 * \see mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::RawIncidence,
			 * mangrove_tds::RawGhostSimplex
			 */
			inline void fixCoboundary(vector<RawIncidence> &incidents,vector<RawGhostSimplex> &edges)
			{
				GhostPropertyHandle< vector<SimplexPointer> > *prop;
				GlobalPropertyHandle<bool> *vp;
				GhostSimplex s;
				unsigned int v,e,k,pos;
				vector<SimplexPointer> *aaa;
				SimplexPointer cp,current;
				COMPLEX c;

				/* First, we convert 'edges', only if it is needed! */
				prop=this->ccw->addGhostProperty(string("inc"),vector<SimplexPointer>());
				for(e=0;e<edges.size();e++)
				{
					/* Now, we consider how many aliases we have */
					if(edges[e].getAliases().size()>0)
					{
						this->ccw->ghostSimplex(GhostSimplexPointer(edges[e].getAliases().at(0)),&s);
						for(unsigned int k=0;k<edges[e].getAliases().size();k++)
							{ prop->get(GhostSimplex(s)).push_back(SimplexPointer(edges[e].getAliases().at(k).getParentType(),edges[e].getAliases().at(k).getParentId())); }
					}
					else { prop->get(GhostSimplex(s)); }
				}

				/* Now, we loop over each record in 'incidents' */
				for(v=0;v<incidents.size();v++)
				{
					/* Now, we check how many top simplices we have */
					if(incidents[v].getSimplices().size()==0) { ; }
					else if(incidents[v].getSimplices().size()==1) { this->ccw->simplex(SimplexPointer(0,v)).add2Coboundary(SimplexPointer(incidents[v].getSimplices()[0])); }
					else
					{
						/* Now, we add properties, and then we can identify vertex-based clusters! */
						vp=this->ccw->addGlobalProperty(string("visited"),false);
						for(k=0;k<incidents[v].getSimplices().size();k++)
						{
							/* Now, we check if the current simplex 'cp' has been marked, otherwise a new connected component can start */
							cp=SimplexPointer(incidents[v].getSimplices().at(k));
							if(vp->get(SimplexPointer(cp))==false)
			 				{
								queue<SimplexPointer> q;
			 					
			 					/* The analysis of a new connected component can start! */
			 					c.clear();
			 					q.push(SimplexPointer(cp));
								while(!q.empty())
								{
									current = q.front();
									q.pop();
									if(vp->get(SimplexPointer(current))==false)
									{
										/* A new simplex to be added to 'c' */
										c.insert(SimplexPointer(current));
										vp->set(SimplexPointer(current),true);
										
										if(current.type()==2)
										{
											/* The 'current' simplex is a dangling face: we can proceed along edges incident at 'v' */
											this->ccw->simplex(SimplexPointer(current)).positionInBoundary(SimplexPointer(0,v),pos);
											for(unsigned int z=0;z<3;z++)
											{
												/* We require only edges incident at 'v' */
												if(z!=pos)
												{
													this->ccw->ghostSimplex(GhostSimplexPointer(current.type(),current.id(),1,z),&s);
													prop->get(GhostSimplex(s),&aaa);
													for(unsigned int n=0;n<aaa->size();n++) { q.push(SimplexPointer(aaa->at(n))); }
												}
											}
										}
										else if(current.type()==3)
										{
											/* The 'current' simplex is a tetrahedron: we can proceed along edges incident at 'v' */
											for(unsigned int z=0;z<6;z++)
											{
												this->ccw->ghostSimplex(GhostSimplexPointer(current.type(),current.id(),1,z),&s);
												if( (s.getCBoundary().at(0).id()==v) || (s.getCBoundary().at(1).id()==v))
												{
													prop->get(GhostSimplex(s),&aaa);
													for(unsigned int n=0;n<aaa->size();n++) { q.push(SimplexPointer(aaa->at(n))); }
												}
											}
										}
									}
								}
								
								/* If we arrive here, we have found a connected component incident in 'v' */
								if(c.size()!=0) { this->ccw->simplex(SimplexPointer(0,v)).add2Coboundary(SimplexPointer(*(c.rbegin()))); }
								c.clear();
			 				}
						}

						/* If we arrive here, we have finished! */
						this->ccw->deleteProperty(string("visited"));
					}
				}
				
				/* Now, we remove 'inc' */
				this->ccw->deleteProperty(string("inc"));
			}
			
			/// This member function retrieves edge-based cluster incident at edges in the current simplicial complex.
			/**
			 * This member function retrieves edge-based cluster incident at edges in the current simplicial complex, starting from the unique list of edges in the input simplicial shape
			 * (without replicants).<p>For each edge <i>e</i>, we encode the edge-based clusters preceeding and following the current simplex around <i>e</i>.
			 * \param edges the unique list of edge in the input simplicial shape (without replicants)
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawGhostSimplex, mangrove_tds::Simplex, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer,
			 * mangrove_tds::NMIAManager::retrieveEdgeClusters(), mangrove_tds::NMIAManager::createTriangles()
			 */
			inline void fixEdgeBasedClusters(vector<RawGhostSimplex> &edges)
			{
				LocalPropertyHandle<SimplexPointer> *rp;
				vector<GhostSimplexPointer> cls;
				vector<RawTriangle> triangles;
				bool b;
				SimplexPointer cp,ref_cp,aux;
				SIMPLEX_ID cid;
				unsigned int it_trg;
				
				/* Now, we must iterate on all edges */
				rp=this->ccw->addLocalProperty(3,string("representative"),SimplexPointer());
				for(unsigned int k=0;k<edges.size();k++)
				{
					/* Now, we check which type of edge we are analyzing */
					if(edges[k].getAliases().size()<2) { ; }
					else
					{
						/* Now, we must understand what edge-based clusters are incident in the current edge */
						this->retrieveEdgeClusters(edges[k].getAliases(),edges[k].getVertices(),cls,&b);
						if(cls.size()<2) { ; }
						else if(cls.size()==2)
						{
							/* Now, we have two clusters! We must understand what we have! */
							if(b)
							{
								/* We have two top triangles incident in an edge. The current edge is manifold! */
								this->ccw->simplex(SimplexPointer(2,cls[0].getParentId())).add2Adjacency(SimplexPointer(2,cls[1].getParentId()),cls[0].getChildId());
								this->ccw->simplex(SimplexPointer(2,cls[1].getParentId())).add2Adjacency(SimplexPointer(2,cls[0].getParentId()),cls[1].getChildId());
							}
							else
							{
								/* We have at least a cluster of tetrahedra incident in the current edge. It is is non-manifold! */
								this->createTriangles(cls,edges[k].getVertices().at(0),edges[k].getVertices().at(1),triangles);
								sort(triangles.begin(),triangles.end(),compare_raw_triangles);
								for(unsigned int j=0;j<edges[k].getAliases().size();j++)
								{
									/* Now, we analyze the j-th alias */
									this->ccw->addSimplex(1,cp);
									cid=edges[k].getAliases().at(j).getParentId();
									if(edges[k].getAliases().at(j).getParentType()==2)
									{
										/* Now, the current edge (belonging to a top triangle) is non-manifold */
										this->ccw->simplex(SimplexPointer(2,cid)).add2Adjacency(SimplexPointer(cp),edges[k].getAliases().at(j).getChildId());
										ref_cp=SimplexPointer(2,cid);
									}
									else
									{
										/* Now, the current edge (belonging to a tetrahedron) is non-manifold */
										this->ccw->simplex(SimplexPointer(3,cid)).add2AuxiliaryBoundary(SimplexPointer(cp),edges[k].getAliases().at(j).getChildId());
										ref_cp=SimplexPointer(this->ccw->getPropertyValue(string("representative"),SimplexPointer(3,cid),aux));
									}
									
									/* Now, we adjust the simplices incident in 'cp' */
									it_trg=positionOfTriangle(SimplexPointer(ref_cp),triangles);
									assert(it_trg!=triangles.size());
									if(it_trg==0)
									{
										/* The current triangle (cluster representative) is the first! */
										this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(triangles.back().getRealSimplex()));
										this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(triangles[it_trg+1].getRealSimplex()));
									}
									else if(it_trg==triangles.size()-1)
									{
										/* The triangle (cluster representative) is the last! */
										this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(triangles[it_trg-1].getRealSimplex()));
										this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(triangles[0].getRealSimplex()));
									}
									else
									{
										/* The triangle (cluster representative) is in the middle! */
										this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(triangles[it_trg-1].getRealSimplex()));
										this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(triangles[it_trg+1].getRealSimplex()));
									}
								}
							}
						}
						else
						{
							/* We have several clusters incident in the current edge. It is non-manifold! */
							this->createTriangles(cls,edges[k].getVertices().at(0),edges[k].getVertices().at(1),triangles);
							sort(triangles.begin(),triangles.end(),compare_raw_triangles);
							for(unsigned int j=0;j<edges[k].getAliases().size();j++)
							{
								/* Now, we analyze the j-th alias */
								this->ccw->addSimplex(1,cp);
								cid=edges[k].getAliases().at(j).getParentId();
								if(edges[k].getAliases().at(j).getParentType()==2)
								{
									/* Now, the current edge (belonging to a top triangle) is non-manifold */
									this->ccw->simplex(SimplexPointer(2,cid)).add2Adjacency(SimplexPointer(cp),edges[k].getAliases().at(j).getChildId());
									ref_cp=SimplexPointer(2,cid);
								}
								else
								{
									/* Now, the current edge (belonging to a tetrahedron) is non-manifold */
									this->ccw->simplex(SimplexPointer(3,cid)).add2AuxiliaryBoundary(SimplexPointer(cp),edges[k].getAliases().at(j).getChildId());
									ref_cp=SimplexPointer(this->ccw->getPropertyValue(string("representative"),SimplexPointer(3,cid),aux));
								}
								
								/* Now, we adjust the simplices incident in 'cp' */
								it_trg=positionOfTriangle(SimplexPointer(ref_cp),triangles);
								assert(it_trg!=triangles.size());
								if(it_trg==0)
								{
									/* The current triangle (cluster representative) is the first! */
									this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(triangles.back().getRealSimplex()));
									this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(triangles[it_trg+1].getRealSimplex()));
								}
								else if(it_trg==triangles.size()-1)
								{
									/* The triangle (cluster representative) is the last! */
									this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(triangles[it_trg-1].getRealSimplex()));
									this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(triangles[0].getRealSimplex()));
								}
								else
								{
									/* The triangle (cluster representative) is in the middle! */
									this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(triangles[it_trg-1].getRealSimplex()));
									this->ccw->simplex(SimplexPointer(cp)).add2Coboundary(SimplexPointer(triangles[it_trg+1].getRealSimplex()));
								}
							}
						}
					}
				}
				
				/* If we arrive here, we can remove the property 'representative' */
				this->ccw->deleteProperty(string("representative"));	
			}
			
			/// This member function creates a raw description of triangles for a set of edge-based clusters incident at an edge.
			/**
			 * This member function creates a raw description of triangles for a set of edge-based clusters incident at an edge. We should generate a triangle for each cluster incident at the
			 * input edge.
			 * \param cls the list of all ghost simplex pointers (possible aliases) for the input edge
			 * \param v0 the first vertex of the input edge
			 * \param v1 the second vertex of the input edge
			 * \param triangles the raw descriptions of the required triangles
			 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_ID, mangrove_tds::RawTriangle
			 */	
			void createTriangles(vector<GhostSimplexPointer> &cls, SIMPLEX_ID v0, SIMPLEX_ID v1, vector<RawTriangle> &triangles)
			{
				vector<GhostSimplexPointer>::iterator it;
				vector<unsigned int> pos,face;
				unsigned int z,p0,p1;
				Point<double> p,aux;
				SimplexPointer bcp;

				/* First, we check 'triangles' */
				triangles.clear();
				for(it=cls.begin();it!=cls.end();it++)
				{
					/* Now, we add a new RawTriangle! */
					face.clear();
					pos.clear();
					triangles.push_back(RawTriangle());
					triangles.back().getRealSimplex()=SimplexPointer(it->getParentType(),it->getParentId());
					if(it->getParentType()==2)
					{
						/* We must prepare the output triangle */
						if(it->getChildId()==0)
						{
							/* If the current edge is 0, then the last vertex is 0 */
							pos.push_back(1);
							pos.push_back(2);
							pos.push_back(0);
						}
						else if(it->getChildId()==1)
						{
							/* If the current edge is 1, then the last vertex is 1 */
							pos.push_back(0);
							pos.push_back(2);
							pos.push_back(1);
						}
						else
						{
							/* If the current edge is 2, then the last vertex is 2 */
							pos.push_back(0);
							pos.push_back(1);
							pos.push_back(2);
						}
					}
					else if(it->getParentType()==3)
					{
						/* Now, we should take a face incident in the input edge */
						this->ccw->simplex(SimplexPointer(it->getParentType(),it->getParentId())).positionInBoundary(SimplexPointer(0,v0),p0);
						this->ccw->simplex(SimplexPointer(it->getParentType(),it->getParentId())).positionInBoundary(SimplexPointer(0,v1),p1);
						for(z=0;z<4;z++) { if( (z!=p0) && (z!=p1)) face.push_back(z); }
						pos.push_back(p0);
						pos.push_back(p1);
						pos.push_back(face.at(0));
						face.clear();
					}
					
					/* Now, we finalize vertices */
					for(z=0;z<pos.size();z++)
					{
						bcp=SimplexPointer(this->ccw->simplex(SimplexPointer(it->getParentType(),it->getParentId())).bc(pos[z]));
						p=Point<double>(this->ccw->getPropertyValue(string(this->ccw->getPropertyName4Coordinates()),bcp,aux));
						triangles.back().getRealTriangleVertices().push_back(Point<double>(p));
					}
				}
			}
			
			/// This member function retrieves edge-based cluster incident at an edge in the current simplicial complex.
			/**
			 * This member function retrieves edge-based cluster incident at an edge in the current simplicial complex, starting from a list of all possible aliases, i.e. top simplices
			 * incident at the input edge.
			 * \param els possible aliases of the input edge
			 * \param vts vertices of the input edge
			 * \param cls a representative simplex for each simplex incident in the input edge
			 * \param b this boolean flag identifies if all cluster are 2-dimensional
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawGhostSimplex, mangrove_tds::Simplex, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer
			 * mangrove_tds::NMIAManager::fixEdgeBasedClusters()
			 */
			inline void retrieveEdgeClusters(vector<GhostSimplexPointer> & els, vector<SIMPLEX_ID> &vts, vector<GhostSimplexPointer> &cls,bool *b)
			{
				GlobalPropertyHandle<bool> *vp;
				SimplexPointer curr,aux;
				unsigned int pos0,pos1;

				/* First, we clear and then we can proceed! */
				cls.clear();
				(*b)=true;
				vp=this->ccw->addGlobalProperty(string("visited"),false);
				for(unsigned int i=0;i<els.size();i++)
				{
					if(els[i].getParentType()==2) { cls.push_back(GhostSimplexPointer(els[i])); }
					else if(vp->get(SimplexPointer(SimplexPointer(els[i].getParentType(),els[i].getParentId())))==false)
					{
						queue<SimplexPointer> q;
						
						/* Now, we must identify clusters of adjacent tetrahedra */	
						cls.push_back(GhostSimplexPointer(els[i]));
						(*b)=false;
						q.push(SimplexPointer(els[i].getParentType(),els[i].getParentId()));
						while(q.empty()==false)
						{
							curr=q.front();
							q.pop();
							if(vp->get(SimplexPointer(curr))==false)
							{
								vp->set(SimplexPointer(curr),true);
								this->ccw->getPropertyValue(string("representative"),SimplexPointer(curr),aux)=SimplexPointer(els[i].getParentType(),els[i].getParentId());
								this->ccw->simplex(SimplexPointer(curr)).positionInBoundary(SimplexPointer(0,vts[0]),pos0);
								this->ccw->simplex(SimplexPointer(curr)).positionInBoundary(SimplexPointer(0,vts[1]),pos1);
								for(unsigned int z=0;z<4;z++)
								{
									/* Do we proceed along this adjacency? */
									if( (z!=pos0) && (z!=pos1))
									{
										/* Now, we take the adiacency along the z-th face of 'curr' */
										if(this->ccw->simplex(SimplexPointer(curr)).hasAdjacency(z))
										{
											aux=SimplexPointer(this->ccw->simplex(SimplexPointer(curr)).a(z));
											q.push(SimplexPointer(aux));
										}	
									}
								}
							}
						}
					}
				}
				
				/* If we arrive here, we have finished! */
				this->ccw->deleteProperty(string("visited"));	
			}
		};
	}

#endif

