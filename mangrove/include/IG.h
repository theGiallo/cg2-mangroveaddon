/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library
 *                                                                        				    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * September 2011 (Revised on May 2012)
 *                                                                    
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * IG.h - Implementation of the Incidence Graph                                           
 ***********************************************************************************************/

/* Should we include this header file? */
#ifndef IG_H_

	#define IG_H_
	
	#include "BaseSimplicialComplex.h"
	#include "Miscellanea.h"
	#include <assert.h>
	#include <cstdlib>
	using namespace mangrove_tds;
	using namespace std;
	
	/// Implementation of the <i>Incidence Graph</i>.
	/**
	 * The class defined in this file implements the <i>Incidence Graph</i>, a dimension-independent data structure for representing simplicial complexes with an arbitrary domain. This data structure
	 * encodes all simplices in a simplicial complex, thus it is a <i>global</i> data structure. For each k-simplex it encodes:<ul><li>all k+1 simplices of dimension k-1 belonging to its boundary;
	 * </li><li>all simplices of dimension k+1 belonging to its coboundary.</li></ul><p>This class must be used in conjunction with the mangrove_tds::BaseSimplicialComplex class in accordance with
	 * the <A href=http://en.wikipedia.org/wiki/Curiously_recurring_template_pattern>Curiously Recurring Template Pattern</A> rules in order to implement the static polymorphism: in this way we can
	 * reuse some parts of the mangrove_tds::BaseSimplicialComplex class and we can achieve more efficient implementations, by discarding virtual member functions in the current implementation.
	 * \file IG.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	
	/* all following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		/// The implementation of the <i>Incidence Graph</i>.
		/**
		 * This class implements the <i>Incidence Graph</i>, a dimension-independent data structure for representing simplicial complexes with an arbitrary domain. This data structure encodes all
 		 * simplices in a simplicial complex, thus it is a <i>global</i> data structure. For each k-simplex it encodes:<ul><li>all k+1 simplices of dimension k-1 belonging to its boundary;</li>
		 * <li>all simplices of dimension k+1 belonging to its coboundary.</li></ul><p>This class must be used in conjunction with the mangrove_tds::BaseSimplicialComplex class in accordance with
		 * the <A href=http://en.wikipedia.org/wiki/Curiously_recurring_template_pattern>Curiously Recurring Template Pattern</A> rules in order to implement the static polymorphism: in this way
		 * we can reuse some parts of the mangrove_tds::BaseSimplicialComplex class and we can achieve more efficient implementations, by discarding virtual member functions in the current
		 * implementation.
	 	 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplexPointer, mangrove_tds::IS, mangrove_tds::SIG, mangrove_tds::GIA
		 */
		class IG : public BaseSimplicialComplex<IG>
		{
			public:

			/// This member function creates a new instance of this class.
			/**
			 * This member function creates all internal data structures required for encoding a simplicial complex through an <i>Incidence Graph</i>. In this member function, we allocate all
			 * internal data structures without any simplices or properties, i.e. auxiliary information, like Euclidean coordinates and field values, associated to simplices directly encoded 
			 * in the <i>Incidence Graph</i>. Thus, you should apply the member functions offered by the mangrove_tds::IG class in order to add simplices and properties to the current data
			 * structure encoding a new simplicial complex.
			 * \param t the dimension of the simplicial complex to be encoded in the current data structure
			 * \see mangrove_tds::BaseSimplicialComplex::init(), mangrove_tds::BaseSimplicialComplex::clear(), mangrove_tds::PropertyBase, mangrove_tds::SIMPLEX_TYPE,
			 * mangrove_tds::BaseSimplicialComplex::addGlobalProperty(), mangrove_tds::BaseSimplicialComplex::addLocalProperty(),
			 * mangrove_tds::BaseSimplicialComplex::addSparseProperty()
			 */
			inline IG(SIMPLEX_TYPE t) : BaseSimplicialComplex<IG>(t) { ; }

			/// This member function destroys an instance of this class.
			inline virtual ~IG() { ; }
			
			/// This member function returns a string that describes the current data structure.
			/**
 			 * This member function returns a string that describes the current data structure, usually its name.
 			 * \return a string that describes the current data structure
 			 * \see mangrove_tds::BaseSimplicialComplex::debug()
 			 */
 			inline string getDataStructureName() { return string("Incidence Graph (IG)"); }
 			
 			/// This member function checks if the current data structure encodes all simplices in the simplicial complex to be represented.
 			/**
 			 * This member function checks if the current data structure encodes all simplices in the current simplicial complex: in this, case it is called <i>global</i> data structure.
			 * Alternatively, a data structure can encode a subset of all simplices, for instance all top simplices: in this case, it is called <i>local</i> data structure.<p>The
			 * <i>Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure.
			 * \return <ul><li><i>true</i>, if the current data structure encodes all simplices</li><li><i>false</i>, otherwise</li></ul>
 			 * \see mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::isTop(), mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer
 			 */
 			inline bool encodeAllSimplices() { return true; }
 			
 			/// This member function computes the storage cost of the current data structure.
			/**
			 * This member function computes the storage cost of the current data structure, expressed as number of pointers to the simplices directly encoded in the current data structure. In
			 * ther words, we compute the number of instances of the mangrove_tds::SimplexPointer class required for encoding the current data structure.
			 * \return the storage cost of the current data structure
			 * \see mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::SimplexIterator, mangrove_tds::BaseSimplicialComplex::type(), mangrove_tds::SIMPLEX_TYPE
			 */
 			inline unsigned int getStorageCost()
 			{
 				unsigned int c;
				SIMPLEX_TYPE t,d;
				SimplexIterator it;

				/* We must iterate on all simplices in the current data structure! */
				c=0;
				t=this->type();
				for(d=0;d<=t;d++)
				{
					/* Now, we iterate over all d-simplices! */
					for(it=this->begin(d);it!=this->end(d);it++)
					{
						c=c+it->getBoundarySize();
						c=c+it->getCoboundarySize();
					}
				}
				
				/* If we arrive here, we can return the storage cost! */
				return c;
 			}
 			
 			/// This member function identifies all simplices belonging to the boundary of a simplex directly encoded in the current data structure.
 			/**
 			 * This member function identifies all simplices belonging to the boundary of a simplex directly encoded in the current data structure.<p>The reference simplex and the boundary
 			 * simplices are required to be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector
 			 * mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function
 			 * only with a <i>global</i> data structure.<p>The <i>Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure.<p>If we cannot complete this operation,
 			 * then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.<p><b>IMPORTANT:</b> in this member function, we apply all built-in facilities offered by the current data structure for marking simplices. You must apply a specific
			 * property (i.e. an instance of the mangrove_tds::PropertyBase class) for other application-dependent marking.
 			 * \param cp the reference simplex directly encoded in the current data structure
	 		 * \param ll a list containing all simplices belonging to the boundary of a simplex directly encoded in the current data structure
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::visit(), 
	 		 * mangrove_tds::BaseSimplicialComplex::isVisited(), mangrove_tds::BaseSimplicialComplex::unmarkAllVisited()
	 		 */
 			inline void boundary(const SimplexPointer& cp,list<SimplexPointer>* ll)
	 		{
	 			queue<SimplexPointer> q;
	 			SimplexPointer curr;

	 			/* First, we check if the input parameters are ok! */
	 			assert(this->isValid(cp));
	 			assert(ll!=NULL);
	 			ll->clear();
	 			if(cp.type()==0) { ; }
	 			else if(cp.type()==1)
	 			{
	 				/* The input simplex is an edge! */
	 				ll->push_back(SimplexPointer(this->simplex(cp).bc(0)));
	 				ll->push_back(SimplexPointer(this->simplex(cp).bc(1)));
	 			}
	 			else
	 			{
	 				/* The input simplex is not a vertex or an edge, thus we have a triangle, at least */
	 				for(unsigned int j=0;j<=cp.type();j++) q.push(SimplexPointer(this->simplex(cp).bc(j)));
	 				while(q.empty()==false)
	 				{
	 					/* Now, we visit the first simplex in queue! */
	 					curr=q.front();
	 					q.pop();
	 					if(this->isVisited(curr)==false)
	 					{
	 						/* Now, we visit the current simplex 'curr' */
	 						this->visit(curr);
	 						ll->push_back(SimplexPointer(curr));
	 						if(curr.type()!=0) { for(unsigned int j=0;j<=curr.type();j++) q.push(SimplexPointer(this->simplex(curr).bc(j))); }
	 					}
	 				}
	 				
	 				/* If we arrive here, we visited all simplices in the boundary of 'cp' */
	 				this->unmarkAllVisited();
	 				ll->sort(compare_simplices_pointers);
	 			}
	 		}
	 		
	 		/// This member function identifies all simplices belonging to the boundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the boundary of a simplex not directly encoded in the current data structure.<p>The reference simplex and the boundary
	 		 * simplices can also not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this
	 		 * member function only with a <i>local</i> data structure.<p>The <i>Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure: consequently, this
	 		 * member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
	 		 * \param ll a list containing all simplices belonging to the boundary of a simplex not directly encoded in the current data structure.
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase,
	 		 * mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::idle()
	 		 */
	 		inline void boundary(const GhostSimplexPointer& cp, list<GhostSimplexPointer>* ll)
	 		{
	 			/* Dummy statetement for avoiding compiler warnings! */
	 			idle(cp);
	 			idle(ll);
	 			assert(this->encodeAllSimplices()==false);
	 		}

			/// This member function identifies all simplices belonging to the boundary of a simplex directly encoded in the current data structure.
 			/**
 			 * This member function identifies all simplices of a certain dimension belonging to the boundary of a simplex directly encoded in the current data structure: this member function
 			 * filters boundary simplices against their dimension.<p>The reference simplex and the boundary simplices are required to be <i>valid</i>, i.e. directly stored in the current data
 			 * structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class
 			 * and thus by their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>If we cannot complete this operation, then
 			 * this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.<p><b>IMPORTANT:</b> in this member function, we apply all built-in facilities offered by the current data structure for marking simplices. You must apply a specific
			 * property (i.e. an instance of the mangrove_tds::PropertyBase class) for other application-dependent marking.
 			 * \param cp the reference simplex directly encoded in the current data structure
 			 * \param k the dimension of the required simplices belonging to the input simplex boundary
	 		 * \param ll a list containing all simplices belonging to the boundary of a simplex directly encoded in the current data structure
 			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase,
 			 * mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isValid()
	 		 */
	 		inline void boundaryk(const SimplexPointer& cp,SIMPLEX_TYPE k,list<SimplexPointer> *ll)
	 		{
	 			unsigned int j;
	 			SimplexPointer curr;
	 			queue<SimplexPointer> q;

	 			/* First, we check if the input parameters are ok! */
	 			assert(this->isValid(cp));
	 			assert(ll!=NULL);
	 			ll->clear();
	 			assert(cp.type()!=0);
	 			assert(k<cp.type());
	 			if(cp.type()==0) { ; }
	 			else if(k==cp.type()-1)
	 			{
	 				/* Now, we consider the immediate boundary! */
	 				for(j=0;j<=cp.type();j++) { ll->push_back(SimplexPointer(this->simplex(cp).bc(j))); }
	 				if(cp.type()!=1) ll->sort(compare_simplices_pointers);
	 			}
	 			else
	 			{
	 				/* Now, we can extract all required simplices */
	 				for(j=0;j<=cp.type();j++) q.push(SimplexPointer(this->simplex(cp).bc(j)));
	 				while(q.empty()==false)
	 				{
	 					/* Now, we visit the first simplex in queue! */
	 					curr=q.front();
	 					q.pop();
	 					if(this->isVisited(curr)==false)
	 					{
	 						/* Now, we visit the current simplex 'curr' */
	 						this->visit(curr);
	 						if(curr.type()==k) ll->push_back(SimplexPointer(curr));
	 						else if(curr.type()>k) { for(j=0;j<=curr.type();j++) q.push(SimplexPointer(this->simplex(curr).bc(j))); }
	 					}
	 				}
	 				
	 				/* If we arrive here, we visited all k-simplices in the boundary of 'cp' */
	 				this->unmarkAllVisited();
	 				ll->sort(compare_simplices_pointers);		
	 			}
	 		}

	 		/// This member function identifies all simplices of a certain dimension belonging to the boundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices of a certain dimension belonging to the boundary of a simplex not directly encoded in the current data structure: this member
			 * function filters boundary simplices against their dimension.<p>The reference simplex and the boundary simplices can also not directly encoded in the current data structure and
			 * they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>The
	 		 * <i>Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure: consequently, this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
			 * \param k the dimension of the required simplices belonging to the input simplex boundary
			 * \param ll a list of the required simplices belonging to the input simplex boundary
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase,
	 		 * mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::idle()
			 */
	 		inline void boundaryk(const GhostSimplexPointer& cp,SIMPLEX_TYPE k,list<GhostSimplexPointer> *ll)
	 		{
	 			/* Dummy statetement for avoiding compiler warnings! */
	 			idle(cp);
	 			idle(ll);
	 			idle(k);
	 			assert(this->encodeAllSimplices()==false);
	 		}

	 		/// This member function identifies all simplices belonging to the coboundary of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the coboundary of a simplex directly encoded in the current data structure.<p>The reference simplex and the star
	 		 * simplices are required to be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector
	 		 * mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function
	 		 * only with a <i>global</i> data structure.<p>The <i>Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure.<p>If we cannot complete this operation,
	 		 * then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.<p><b>IMPORTANT:</b> when you try to apply this member function, you should be careful about the use of auxiliary information (i.e. subclasses of the 
	 		 * mangrove_tds::PropertyBase class) associated to the current simplicial complex. In other words, in this member function, we use a global property (i.e. an instance of the
	 		 * mangrove_tds::GlobalPropertyHandle class) with boolean values, called <i>visited_cob</i>, and a local property (i.e. an instance of the mangrove_tds::LocalPropertyHandle class)
	 		 * for all d-simplices, called <i>refSimplex</i>. We remove such properties, if they already belong to the current simplicial complex. In any case, we remove them at the end of
			 * this member function.
 			 * \param cp the reference simplex directly encoded in the current data structure
			 * \param ll a list of the required simplices belonging to the input simplex coboundary
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::Simplex, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(),
			 * mangrove_tds::GhostSimplexPointer, mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::LocalPropertyHandle,
			 * mangrove_tds::BaseSimplicialComplex::addGlobalProperty(), mangrove_tds::BaseSimplicialComplex::addLocalProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty()
			 */
	 		inline void coboundary(const SimplexPointer& cp, list<SimplexPointer> *ll)
	 		{
	 			LocalPropertyHandle<bool> *rsp;
	 			GlobalPropertyHandle<bool> *vcp;
				unsigned int lg,j;
				queue<SimplexPointer> q;
				SimplexPointer curr;

	 			/* First, we check if the input parameters are ok! */
	 			assert(this->isValid(cp));
	 			assert(ll!=NULL);
	 			ll->clear();
	 			if(this->hasProperty(string("visited_cob"))) this->deleteProperty(string("visited_cob"));
	 			if(this->hasProperty(string("refSimplex"))) this->deleteProperty(string("refSimplex"));
				if(this->isTop(SimplexPointer(cp))==false)
				{
					rsp=this->addLocalProperty(cp.type(),string("refSimplex"),false);
					rsp->set(SimplexPointer(cp),true);
					vcp=this->addGlobalProperty(string("visited_cob"),false);
					lg=this->simplex(cp).getCoboundarySize();
					for(j=0;j<lg;j++) { q.push(SimplexPointer(this->simplex(cp).cc(j))); }
					while(q.empty()==false)
					{
						curr=q.front();
						q.pop();
						if(vcp->get(SimplexPointer(curr))==false)
						{
							vcp->get(SimplexPointer(curr))=true;
							if(this->intersect(SimplexPointer(curr),cp.type(),rsp)==true)
							{
								ll->push_back(SimplexPointer(curr));
								lg=this->simplex(curr).getCoboundarySize();
								for(j=0;j<lg;j++) { q.push(SimplexPointer(this->simplex(curr).cc(j))); }
							}	
						}
					}
					
					/* If we arrive here, we can finalize this member function */
	 				this->deleteProperty(string("visited_cob"));
	 				this->deleteProperty(string("refSimplex"));
	 				ll->sort(compare_simplices_pointers);
				}
	 		}

	 		/// This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex directly encoded in the current data structure: this member
			 * function filters boundary simplices against their dimension.<p>The reference simplex and the star simplices are required to be <i>valid</i>, i.e. directly stored in the current
			 * data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They are identified by instances of the mangrove_tds::SimplexPointer
			 * class and thus by their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>The <i>Incidence Graph</i> encodes
			 * all simplices and it is a <i>global</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> when you try to apply this
			 * member function, you should be careful about the use of auxiliary information (i.e. subclasses of the mangrove_tds::PropertyBase class) associated to the current simplicial
			 * complex. In other words, in this member function, we use a global property (i.e. an instance of the mangrove_tds::GlobalPropertyHandle class) with boolean values, called
			 * <i>visited_cob</i>, and a local property (i.e. an instance of the mangrove_tds::LocalPropertyHandle class) for all d-simplices, called <i>refSimplex</i>. We remove such
			 * properties, if they already belong to the current simplicial complex. In any case, we remove them at the end of this member function.
 			 * \param cp the reference simplex directly encoded in the current data structure
 			 * \param k the dimension of the required simplices belonging to the input simplex coboundary
			 * \param ll a list of the required simplices belonging to the input simplex coboundary
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::GhostSimplex,
			 * mangrove_tds::GhostSimplexPointer, mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::LocalPropertyHandle,
			 * mangrove_tds::BaseSimplicialComplex::addGlobalProperty(), mangrove_tds::BaseSimplicialComplex::addLocalProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty()
			 */
	 		inline void coboundaryk(const SimplexPointer& cp,SIMPLEX_TYPE k,list<SimplexPointer> *ll)
	 		{
	 			unsigned int j,lg;
				LocalPropertyHandle<bool> *rsp;
	 			GlobalPropertyHandle<bool> *vcp;
				queue<SimplexPointer> q;
				SimplexPointer curr;
				
	 			/* First, we check if the input parameters are ok! */
	 			assert(this->isValid(cp));
	 			assert(ll!=NULL);
	 			ll->clear();
	 			assert(k>cp.type());
	 			if(this->hasProperty(string("visited_cob"))) this->deleteProperty(string("visited_cob"));
	 			if(this->hasProperty(string("refSimplex"))) this->deleteProperty(string("refSimplex"));
	 			if(this->isTop(SimplexPointer(cp))==false)
				{
					if(k==cp.type()+1)
					{
						/* Co-boundary already encoded! */
						lg=this->simplex(cp).getCoboundarySize();
						for(j=0;j<lg;j++) { ll->push_back(SimplexPointer(this->simplex(cp).cc(j))); }
					}
					else
					{
						/* Now, we should visit co-boundary */
						rsp=this->addLocalProperty(cp.type(),string("refSimplex"),false);
						rsp->set(SimplexPointer(cp),true);
						vcp=this->addGlobalProperty(string("visited_cob"),false);
						lg=this->simplex(cp).getCoboundarySize();
						for(j=0;j<lg;j++) { q.push(SimplexPointer(this->simplex(cp).cc(j))); }
						while(q.empty()==false)
						{
							curr=q.front();
							q.pop();
							if(vcp->get(SimplexPointer(curr))==false)
							{
								vcp->get(SimplexPointer(curr))=true;
								if(curr.type()==k) { if(this->intersect(SimplexPointer(curr),cp.type(),rsp)==true) ll->push_back(SimplexPointer(curr)); }
								else if(curr.type()<k)
								{
									if(this->intersect(SimplexPointer(curr),cp.type(),rsp)==true)
									{
										lg=this->simplex(curr).getCoboundarySize();
										for(j=0;j<lg;j++) { q.push(SimplexPointer(this->simplex(curr).cc(j))); }
									}
								}
							}	
						}
						
						/* If we arrive here, we can finalize this member function */
	 					this->deleteProperty(string("visited_cob"));
	 					this->deleteProperty(string("refSimplex"));
	 					ll->sort(compare_simplices_pointers);
					}
				}
	 		}

	 		/// This member function identifies all simplices belonging to the coboundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the coboundary of a simplex not directly encoded in the current data structure.<p>The reference simplex and the star
	 		 * simplices can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this
	 		 * member function only with a <i>local</i> data structure.<p>The <i>Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure: consequently, this
	 		 * member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
	 		 * \param ll a list of the required simplices belonging to the input simplex coboundary
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::idle(),
			 * mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::isValid()
			 */
	 		inline void coboundary(const GhostSimplexPointer& cp, list<GhostSimplexPointer> *ll)
	 		{
	 			/* Dummy statements for avoiding compiler warnings! */
	 			idle(cp);
	 			idle(ll);
	 			assert(this->encodeAllSimplices()==false);
	 		}
	 		
	 		/// This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex not directly encoded in the current data structure: this member
	 		 * function filters coboundary simplices against their dimension.<p>The reference simplex and the coboundary simplices can also not directly encoded in the current data structure
			 * and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>The
	 		 * <i>Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure: consequently, this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
			 * \param k the dimension of the required simplices belonging to the input simplex coboundary
			 * \param ll a list of the required simplices belonging to the input simplex coboundary
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase,
	 		 * mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::idle()
			 */
	 		inline void coboundaryk(const GhostSimplexPointer& cp,SIMPLEX_TYPE k,list<GhostSimplexPointer> *ll)
	 		{
	 			/* Dummy statetement for avoiding compiler warnings! */
	 			idle(cp);
	 			idle(ll);
	 			idle(k);
	 			assert(this->encodeAllSimplices()==false);
	 		}
	 		
	 		/// This member function identifies all simplices adjacent to a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices adjacent to a simplex directly encoded in the current data structure: two 0-simplices are <i>adjacent</i> if they are connected by
			 * a common edge, while two k-simplices (with k>0) are <i>adjacent</i> if they share a simplex of dimension k-1.<p>The reference simplex and the adjacent simplices are required to
			 * be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They are
			 * identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function only with a
			 * <i>global</i> data structure.<p>The <i>Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure.<p>If we cannot complete this operation, then this
			 * member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \param cp the reference simplex directly encoded in the current data structure
			 * \param ll a list of the required simplices belonging to the input simplex adjacency
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices()
	 		 */
	 		inline void adjacency(const SimplexPointer &cp,list<SimplexPointer> *ll)
	 		{
	 			unsigned int lg,lg1;
	 			SimplexPointer current;

	 			/* First, we must check the input parameters! */
	 			assert( this->isValid(cp));
	 			assert(ll!=NULL);
	 			ll->clear();
	 			if(cp.type()==0)
	 			{
	 				/* We must find all 0-simplices adjacent to 'cp': we must enumerate all edges incident in 'cp' and extract their vertices! */
	 				lg = this->simplex(cp).getCoboundarySize();
	 				for(unsigned int k=0;k<lg;k++)
	 				{
	 					/* Now, we check which vertex we must add! */
	 					for(unsigned int j=0;j<2;j++)
	 					{
	 						current = SimplexPointer(this->simplex(this->simplex(cp).cc(k)).bc(j));
	 						if(cp.id()!=current.id()) { ll->push_back(SimplexPointer(current)); }
	 					}
	 				}
	 			}
	 			else
	 			{
	 				/* We must find all k-simplices that share a (k-1)-simplex with 'cp' */
	 				lg = this->simplex(cp).getBoundarySize();
					for(unsigned int k=0;k<lg;k++)
	 				{
	 					/* Now, we compute the coboundary of the k-simplex in the boundary of 'cp' */
	 					lg1 = this->simplex(this->simplex(cp).bc(k)).getCoboundarySize();
	 					for(unsigned int j=0;j<lg1;j++)
						{
							current = SimplexPointer(this->simplex(this->simplex(cp).bc(k)).cc(j));
							if(cp.id()!=current.id()) { ll->push_back(SimplexPointer(current)); }
						}
	 				}
	 			}
	 			
				/* If we arrive here, we have finished! */
	 			ll->sort(compare_simplices_pointers);
	 		}
	 		
	 		/// This member function identifies all simplices adjacent to a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices adjacent to a simplex not directly encoded in the current data structure: two 0-simplices are <i>adjacent</i> if they are
			 * connected by a common edge, while two k-simplices (with k>0) are <i>adjacent</i> if they share a simplex of dimension k-1.<p>The reference simplex and the adjacent simplices
			 * can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member
			 * function only with a <i>local</i> data structure.<p>The <i>Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure: consequently then this member
			 * function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
			 * \param ll a list of the required simplices belonging to the input simplex adjacency and not directly encoded in the current data structure
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(),
	 		 * mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::isValid(),
	 		 * mangrove_tds::idle()
	 		 */
	 		inline void adjacency(const GhostSimplexPointer &cp,list<GhostSimplexPointer> *ll)
	 		{
	 			/* Dummy statetement for avoiding compiler warnings! */
	 			idle(cp);
	 			idle(ll);
	 			assert(this->encodeAllSimplices()==false);
	 		}
	 		
	 		/// This member function identifies all simplices belonging to the link of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the link of a simplex directly encoded in the current data structure: let <i>c</i> a simplex in the current simplicial
	 		 * complex, then the <i>link</i> of <i>c</i> is the set of all subsimplices of the simplices in the star of <i>c</i> that are not incident in <i>c</i>.<p>The reference simplex and
	 		 * the required simplices must be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector
	 		 * mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function
	 		 * only with a <i>global</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled
			 * in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function we use a global property (i.e. an instance of
			 * the mangrove_tds::GlobalPropertyHandle class) with boolean values, called <i>visited</i>, and a local property (i.e. an instance of the mangrove_tds::LocalPropertyHandle class)
			 * for vertices with boolean values, called <i>refVertex</i>. We remove them, if they already belong to the current simplicial complex. In any case, we remove them at the end of
			 * this member function.
	 		 * \param cp the reference simplex directly encoded in the current data structure
	 		 * \param ll a list of the required simplices belonging to the input simplex link
	 		 * \see  mangrove_tds::SimplexPointer, mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::LocalPropertyHandle,
	 		 * mangrove_tds::BaseSimplicialComplex::addGlobalProperty(), mangrove_tds::BaseSimplicialComplex::addLocalProperty()
	 		 */
	 		inline void link(const SimplexPointer& cp,list<SimplexPointer> *ll)
	 		{
	 			LocalPropertyHandle<bool> *rvp;
	 			GlobalPropertyHandle<bool> *vp;
				list<SimplexPointer> cob,verts_cp,bnd;
				list<SimplexPointer>::iterator it,cob_it,bnd_it;

	 			/* First, we check the input parameters and then we can start our research! */
	 			assert(ll!=NULL);
	 			ll->clear();
	 			if(this->hasProperty(string("visited"))) this->deleteProperty(string("visited"));
	 			if(this->hasProperty(string("refVertex"))) this->deleteProperty(string("refVertex"));
	 			if(this->isTop(SimplexPointer(cp))==true) return;
	 			vp=this->addGlobalProperty( string("visited"),false);
	 			rvp=this->addLocalProperty(0,string("refVertex"),false);
	 			this->star(SimplexPointer(cp),&cob);
	 			if(cp.type()==0) verts_cp.push_back(SimplexPointer(cp));
	 			else this->boundaryk(SimplexPointer(cp),0,&verts_cp);
	 			for(it=verts_cp.begin();it!=verts_cp.end();it++) rvp->set(SimplexPointer(*it),true);
	 			for(cob_it=cob.begin();cob_it!=cob.end();cob_it++)
	 			{
	 				/* Now, we consider the current simplex incident in 'cp', if it has not been visited! */
	 				if(vp->get(SimplexPointer(*cob_it))==false)
	 				{
	 					/* Now, we visit the current 'cob_it' simplex, by extracting its boundary */
	 					vp->set(SimplexPointer(*cob_it),true);
	 					this->boundary(SimplexPointer(*cob_it),&bnd);
	 					for(bnd_it=bnd.begin();bnd_it!=bnd.end();bnd_it++)
	 					{
	 						/* Now, we check if we must analyze 'bnd_it' */
	 						if(vp->get(SimplexPointer(*bnd_it))==false)	
	 						{
	 							vp->set(SimplexPointer(*bnd_it),true);
	 							if(this->intersect(SimplexPointer(*bnd_it),rvp)==false) { ll->push_back(SimplexPointer(*bnd_it)); }
	 						}
	 					}
	 				}
	 			}

	 			/* If we arrive here, we can finalize this member function! */
	 			this->deleteProperty(string("visited"));
	 			this->deleteProperty(string("refVertex"));
	 			ll->sort(compare_simplices_pointers);
	 		}	

	 		/// This member function identifies all simplices belonging to the link of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the link of a simplex not directly encoded in the current data structure: let <i>c</i> a simplex in the current
			 * simplicial complex, then the <i>link</i> of <i>c</i> is the set of all subsimplices of the simplices in the star of <i>c</i> that are not incident in <i>c</i>.<p>The reference
			 * simplex and the required simplices can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class.
			 * Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>The <i>Incidence Graph</i> encodes all simplices and it is a <i>global</i> data
			 * structure: consequently, this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will
			 * result in a failed assert.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
	 		 * \param ll a list of the required simplices belonging to the input simplex link and not directly encoded in the current data structure
	 		 * \see mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::SIMPLEX_TYPE,
	 		 * mangrove_tds::SIMPLEX_ID, mangrove_tds::GS_COMPLEX, mangrove_tds::PropertyBase, mangrove_tds::idle()
	 		 */
	 		inline void link(const GhostSimplexPointer& cp,list<GhostSimplexPointer> *ll)
	 		{
	 			/* Dummy statetement for avoiding compiler warnings! */
	 			idle(cp);
	 			idle(ll);
	 			assert(this->encodeAllSimplices()==false);
	 		}

	 		/// This member function returns the number of components in the link of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function returns the number of components in the link of a simplex directly encoded in the current data structure: let <i>c</i> a simplex in the current simplicial
	 		 * complex, then the <i>link</i> of <i>c</i> is the set of all subsimplices of the simplices in the star of <i>c</i> that are not incident in <i>c</i>.<p>The reference simplex and
	 		 * the required simplices must be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector
	 		 * mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function
	 		 * only with a <i>global</i> data structure.<p>The <i>Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure.<p>If we cannot complete this operation,
	 		 * then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.<p><b>IMPORTANT:</b> in this member function we use two global properties (i.e. instances of the mangrove_tds::GlobalPropertyHandle class) with boolean values,
			 * respectively called <i>visited</i> and <i>inLink</i>. We remove such properties, if they already belong to the current simplicial complex. In any case, we remove them at the
			 * end of this member function.
	 		 * \param cc the reference simplex directly encoded in the current data structure
	 		 * \param lk a list of the required simplices belonging to the input simplex link
	 		 * \return the number of components in the link of a simplex directly encoded in the current data structure
	 		 * \see mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase,
	 		 * mangrove_tds::GlobalPropertyHandle, mangrove_tds::BaseSimplicialComplex::addGlobalProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty(),
	 		 * mangrove_tds::BaseSimplicialComplex::encodeAllSimplices()
	 		 */
	 		inline unsigned int getLinkComponentsNumber(const SimplexPointer &cc,list<SimplexPointer> *lk)
	 		{
	 			unsigned int n;
				GlobalPropertyHandle<bool> *ilp,*vp;
				list<SimplexPointer>::iterator cp,temp;
				list<SimplexPointer> b,c;
				SimplexPointer current;
				
	 			/* First, we compute the link of 'cc' and then we can identify its components! */
				this->link(SimplexPointer(cc),lk);
			 	n=0;
				if(this->hasProperty(string("visited"))) this->deleteProperty(string("visited"));
			 	if(this->hasProperty(string("inLink"))) this->deleteProperty(string("inLink"));
			 	ilp=this->addGlobalProperty( string("inLink"),false);
				vp=this->addGlobalProperty( string("visited"),false);
			 	for(cp=lk->begin();cp!=lk->end();cp++) { ilp->set(SimplexPointer(*cp),true); }
				for(cp=lk->begin();cp!=lk->end();cp++)
			 	{
			 		/* Now, we check if the current simplex 'cp' has been marked, otherwise a new connected component can start */
			 		if(vp->get(SimplexPointer(*cp))==false)
			 		{
			 			queue<SimplexPointer> q;
			 			
			 			/* The analysis of a new connected component can start! */
			 			n=n+1;
						q.push(SimplexPointer(*cp));
						while(!q.empty())
						{
							current = q.front();
							q.pop();
							if(vp->get(SimplexPointer(current))==false)
							{
								/* Now, we visit 'current' and then we proceed over its adjacents! */
								vp->set(SimplexPointer(current),true);
								this->boundary(current,&b);
								for(temp=b.begin();temp!=b.end();temp++) { if(ilp->get(SimplexPointer(*temp))==true) q.push(*temp); }
								this->star(current,&c);
								for(temp=c.begin();temp!=c.end();temp++) { if(ilp->get(SimplexPointer(*temp))==true) q.push(*temp); }
							}
						}
			 		}
			 	}

			 	/* If we arrive here, we can finalize this member function! */
				this->deleteProperty(string("visited"));
			 	this->deleteProperty(string("inLink"));
			 	return n;
	 		}	

	 		/// This member function returns the number of components in the link of a simplex not directly encoded in the current data structure.
			/**
			 * This member function returns the number of components in the link of a simplex not directly encoded in the current data structure: let <i>c</i> a simplex in the current
			 * simplicial complex, then the <i>link</i> of <i>c</i> is the set of all subsimplices of the simplices in the star of <i>c</i> that are not incident in <i>c</i>.<p>The reference
			 * simplex and the required simplices can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class.
			 * Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>The <i>Incidence Graph</i> encodes all simplices and it is a <i>global</i> data
			 * structure: consequently, this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will
			 * result in a failed assert.
	 		 * \param cc the reference simplex not directly encoded in the current data structure
	 		 * \param lk a list of the required simplices belonging to the input simplex link
	 		 * \return the number of components in the link of a simplex not directly encoded in the current data structure
	 		 * \see mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::SIMPLEX_TYPE,
	 		 * mangrove_tds::SIMPLEX_ID, mangrove_tds::GS_COMPLEX, mangrove_tds::PropertyBase, mangrove_tds::idle()
			 */
			inline unsigned int getLinkComponentsNumber(const GhostSimplexPointer &cc,list<GhostSimplexPointer> *lk)
			{
				/* Dummy statements for avoiding compiler warnings */
				idle(cc);
				idle(lk);
				assert(this->encodeAllSimplices()==false);
				return 0;
			}
			
			/// This member function checks if a simplex directly encoded in the current data structure is manifold.
			/**
			 * This member function checks if a simplex directly encoded in the current data structure is manifold: we say that a simplex is <i>manifold</i> if and only if its neighborhood is
			 * homeomorphic to a connected component of a sphere, otherwise it is called <i>non-manifold</i>.<p>The reference simplex must be <i>valid</i>, i.e. directly stored in the current 
			 * data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. It is identified by instances of the mangrove_tds::SimplexPointer class
			 * and thus by their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>The <i>Incidence Graph</i> encodes all
			 * simplices and it is a <i>global</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is
			 * compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> this member function works only if the input simplicial
			 * complex is embedded in the Euclidean 3-space. Moreover, in this member function, we invoke the mangrove_tds::IG::getLinkComponentsNumber() member function, thus we use all
			 * properties (i.e. instances of the mangrove_tds::PropertyBase class), introduced by this member function. We remove such properties, if they already belong to the current
			 * simplicial complex. In any case, we remove them at the end of this member function.
			 * \param cp the reference simplex directly encoded in the current data structure
			 * \return <ul><li><i>true</i>, if the required simplex is manifold</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplexPointer, mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(),
			 * mangrove_tds::GlobalPropertyHandle, mangrove_tds::LocalPropertyHandle, mangrove_tds::BaseSimplicialComplex::isTop()
			 */
			inline bool isManifold(const SimplexPointer& cp)
			{
				list<SimplexPointer> l;
				unsigned int n,lg;
				
				/* First, we check if all is ok */
				assert(this->type()<=3);
				assert(this->isValid(cp));
				if(this->isTop(SimplexPointer(cp))) return true;
				switch(cp.type())
				{
					case 0:
					
						/* We have a vertex, thus we must analyze its link! */
						n=this->getLinkComponentsNumber(SimplexPointer(cp),&l);
						if(n>2) { return false; }
					    else if(n==2)
					   	{
							/* Now, we must check if its link is composed only by 2 vertices! */
						 	if( (l.size()==2) && (l.front().type()==0) && (l.back().type()==0)) { return true; }
						  	else { return false; }
					   	}
					   	else if(n==1)
					   	{
					   		/* Now, we must check if all incident edges are manifold! */
					   		lg=this->simplex(SimplexPointer(cp)).getCoboundarySize();
					   		for(unsigned int k=0;k<lg;k++) { if(this->isManifold(SimplexPointer(this->simplex(SimplexPointer(cp)).cc(k)))==false) return false; }
					   		return true;
					   	}
					   	else { return true; }
					   	
					case 1:
					
						/* We have an edge, thus it is manifold only if its link is composed by two vertices! */
						n = this->getLinkComponentsNumber(SimplexPointer(cp),&l);
						if(n>2) return false;
						else if(n<2) return true;
						else if(this->type()==2) return true;
						else
						{
							/* Now, we must check if its link is composed only by 2 vertices! */
						  	if( (l.size()==2) && (l.front().type()==0) && (l.back().type()==0)) { return true; }
						  	else { return false; }
						}
						
					default:
					
						/* In the default case, we have a manifold simplex! */
					    return true;
				};
			}

			/// This member function checks if a simplex not directly encoded in the current data structure is manifold.
			/**
			 * This member function checks if a simplex not directly encoded in the current data structure is manifold: we say that a simplex is <i>manifold</i> if and only if its
			 * neighborhood is homeomorphic to a connected component of a sphere, otherwise it is called <i>non-manifold</i>.<p>The reference simplex and the required simplices can also be
			 * not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member function only
			 * with a <i>local</i> data structure.<p>The <i>Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure: consequently, this member function will fail if
			 * and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param cp the reference simplex not directly encoded in the current data structure
			 * \return <ul><li><i>true</i>, if the required simplex is manifold</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GS_COMPLEX,
			 * mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::isValid(),
			 * mangrove_tds::idle()
			 */
			inline bool isManifold(const GhostSimplexPointer& cp)
			{
				/* Dummy statements for avoiding warnings! */
				idle(cp);
				assert(this->encodeAllSimplices()==false);
				return false;
			}
	
	 		/// This member function updates the coboundary relations for all simplices directly stored in the current data structure.
			/**
			 * This member function updates the coboundary relations for all simplices directly stored in the current data structure, starting from the boundary relations. Such relations must
			 * be already encoded in order to successfully complete this operation.
			 * \see mangrove_tds::Simplex::clearCoboundary(), mangrove_tds::Simplex::add2Coboundary(), mangrove_tds::SimplicesContainer
			 */
			inline void buildCoboundaryFromBoundary()
			{
				SIMPLEX_TYPE t;
				SimplexIterator it;
				unsigned int lg;

				/* First, we clear the current coboundary relations and then we update them! */
				t = this->type();
				for(SIMPLEX_TYPE k=0;k<=t;k++) { for(it=this->simplices[k].begin();it!=this->simplices[k].end();it++) it->clearCoboundary(); }
				for(SIMPLEX_TYPE k=1;k<=t;k++)
				{
					/* Now, we visit all simplices in the k-th collection of simplices, i.e. the k-simplices! */
					for(it=this->simplices[k].begin();it!=this->simplices[k].end();it++)
					{
						/* Now, we update the coboundary of all simplices in b! */
						lg = it->b().size();
						for(unsigned int j=0;j<lg;j++) { this->simplex(it->b(j)).add2Coboundary(it.getPointer()); }
					}
				}
			}

	 		protected:
	 		
	 		/// This member function creates a new instance of this class.
 			/**
 			 * <b>IMPORTANT:</b> you should not invoke this member function. Instead, you should invoke the builder with a list of input parameters.
 			 */
			inline IG() : BaseSimplicialComplex<IG>() { ; }
		};
	}

#endif

