/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library
 *                                                                                                              				    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * November 2011 (Revised on May 2012)
 *                                                                    
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * ts_manager.h - component for I/O operations on TS files
 ***********************************************************************************************/

/* Should we include this header file? */
#ifndef TS_MANAGER_H

	#define TS_MANAGER_H

	#include "IO.h"
	#include "SimplicesMapper.h"
	#include "Simplex.h"
	#include "Point.h"
	#include <climits>
	#include <cfloat>
	#include <iomanip>
	using namespace mangrove_tds;
	using namespace std;
	
	/// Components for I/O between simplicial complexes and TS files.
	/**
	 * The class defined in this file describes a component for I/O between simplicial complexes and files written in <i>TS</i> format, that contains a description of a field value with a domain 
	 * approximated by a tetrahedral grid. However, we can also describe a non-regular grid, i.e. composed by elements of different dimensionalities. <p>The TS format is defined as follows:
	 * <p><i>[ number of vertices ] [ number of tetrahedra ]<br>for each vertex: [ Euclidean coordinates ] [ field value ]<br>for each tetrahedron: [ 4 indices for its vertices ].</i><p>We can
	 * describe elements different from a tetrahedron, replicating indices in its description.<p>In this class, we extract the main information stored in the input file and then we use this component
	 * for creating a new simplicial complex. Moreover, we can write a representation of a simplicial complex formatted in accordance with the <i>TS</i> format. In this component, the Euclidean
	 * coordinates and field values are supported for each vertex.
	 * \file ts_manager.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	
	/* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		/// A manager for the content of a file written in the <i>TS</i> format.
		/**
		 * The class defined in this file describes a component for I/O between simplicial complexes and files written in <i>TS</i> format, that contains a description of a field value with a
		 * domain approximated by a tetrahedral grid. However, we can also describe a non-regular grid, i.e. composed by elements of different dimensionalities.<p>The TS format is defined as
		 * follows:<p><i>[ number of vertices ] [ number of tetrahedra ]<br>for each vertex: [ Euclidean coordinates ] [ field value ]<br>for each tetrahedron: [ 4 indices for its vertices ].</i>
		 * <p>We can describe elements different from a tetrahedron, replicating indices in its description.<p>In this class, we extract the main information stored in the input file and then we
		 * use this component for creating a new simplicial complex. Moreover, we can write a representation of a simplicial complex formatted in accordance with the <i>TS</i> format. In this
		 * component the Euclidean coordinates and field values are supported for each vertex.
	 	 * \see mangrove_tds::DataManager, mangrove_tds::IO, mangrove_tds::Point, mangrove_tds::MeshComponent
		 */
		class TSManager : public DataManager<TSManager>
		{
			public:
			
			/// This member function creates an instance of this class.
			/**
			 * This member function creates a subclass of the mangrove_tds::DataManager class, optimized for managing a file written in <i>TS</i> format,  that contains a description of a
		 	 * field value with a domain approximated by a tetrahedral grid. However, we can also describe a non-regular grid, i.e. composed by elements of different dimensionalities.<p>The TS
			 * format is defined as follows:<p><i>[ number of vertices ] [ number of tetrahedra ]<br>for each vertex: [ Euclidean coordinates ] [ field value ]<br>for each tetrahedron:
			 * [ 4 indices for its vertices ].</i><p>We can describe elements different from a tetrahedron, replicating indices in its description.<p><b>IMPORTANT:</b> the new instance 
			 * created by this member function is not initialized, we must load new data through the mangrove_tds::TSManager::loadData() member function.
			 * \see mangrove_tds::DataManager, mangrove_tds::IO, ::mangrove_tds::Point, mangrove_tds::TSManager::loadData()
			 */
			inline TSManager() : DataManager<TSManager>() 
			{
				/* We reset the coordinates! */
				this->xs.clear();
				this->ys.clear();
				this->zs.clear();
				this->fvalues.clear();
			}
			
			/// This member function destroys an instance of this class.
			/**
			 * This member function destroys an instance of this class.
			 * \see mangrove_tds::TSManager::resetState()
			 */
			inline virtual ~TSManager() { this->resetState(); }
			
			/// This member function allows to customize the property name containing the Euclidean coordinates in the simplicial complex to be built.
			/**
			 * This member function allows to customize the property name containing the Euclidean coordinates for all vertices in the simplicial complex to be built.<p>In this component, the
			 * Euclidean coordinates and field values are stored for each vertex: the string to be used for the Euclidean coordinates is <i>coordinates</i>.
			 * \return the property name containing the Euclidean coordinates in the simplicial complex to be built
			 * \see mangrove_tds::PropertyBase, mangrove_tds::Point, mangrove_tds::TSManager::supportsEuclideanCoordinates(), mangrove_tds::TSManager::getCoordinates()
			 */
			inline string getPropertyName4Coordinates() { return string("coordinates"); }
			
			/// This member function checks if the loading of the Euclidean coordinates is currently supported and enabled in the current component.
			/**
			 * This member function checks if the loading of the Euclidean coordinates is currently supported by the current component.<p>In this component, the Euclidean coordinates and field
			 * values are stored for each vertex: the string to be used for the Euclidean coordinates is <i>coordinates</i>.
			 * \return <ul><li><i>true</i>, if the current component supports the loading of the Euclidean coordinates for all the vertices.</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::Point, mangrove_tds::PropertyBase, mangrove_tds::TSManager::getPropertyName4Coordinates(), mangrove_tds::TSManager::getCoordinates()
			 */
			inline bool supportsEuclideanCoordinates() 
			{
				/* In this component we could load the Euclidean coordinates, now we must check if we have the same number of elements in xs, ys, zs, fvalues! */
				if(this->simplices.size()==0) return false;
				if(this->xs.size()!=this->simplices[0].vectorSize()) return false;
				if(this->ys.size()!=this->simplices[0].vectorSize()) return false;
				if(this->zs.size()!=this->simplices[0].vectorSize()) return false;
				if(this->fvalues.size()!=this->simplices[0].vectorSize()) return false;
				return true;
			}
			
			/// This member function returns the Euclidean coordinates associated to a vertex in the simplicial complex to be built.
			/**
			 * This member function returns the Euclidean coordinates associated to a vertex in the simplicial complex to be built.<p>If we cannot complete this operation for any reason, then
			 * this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param i the identifier of the required vertex
			 * \param p the Euclidean coordinates of the required vertex
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::Point, mangrove_tds::PropertyBase, mangrove_tds::Simplex, mangrove_tds::TSManager::getPropertyName4Coordinates(),
			 * mangrove_tds::TSManager::supportsEuclideanCoordinates()
			 */
			inline void getCoordinates(SIMPLEX_ID i,Point<double> & p)
			{
				/* First, we check if all is ok and then we extract the i-th point from 'coordinates' */
				assert(this->supportsEuclideanCoordinates());
				assert(i<this->xs.size());
				p.updateSize(3);
				p.x()=this->xs[i];
				p.y()=this->ys[i];
				p.z()=this->zs[i];
			}
			
			/// This member function allows to customize the property name containing the field values in the simplicial complex to be built.
			/**
			 * This member function allows to customize the property name containing the field values for all vertices in the simplicial complex to be built.<p>In this component, the Euclidean
			 * coordinates and field values are stored for each vertex: the string to be used for the field values is <i>field_value</i>.
			 * \return the property name containing the field values in the simplicial complex to be built
			 * \see mangrove_tds::PropertyBase, mangrove_tds::TSManager::supportsFieldValues(), mangrove_tds::TSManager::getFieldValue()
			 */
			inline string getPropertyName4FieldValue() { return string("field_value"); }
			
			/// This member function checks if the loading of the field values is currently supported by the current component.
			/**
			 * This member function checks if the loading of the field values is supported by the current component.<p>In this component, the Euclidean coordinates and field values are stored
			 * for each vertex: the string to be used for the field values is <i>field_value</i>.
			 * \return <ul><li><i>true</i>, if the current component supports the loading of the field values for all the vertices.</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::TSManager::getPropertyName4FieldValue(), mangrove_tds::TSManager::getFieldValue()
			 */
			inline bool supportsFieldValues() 
			{
				/* In this component we could load the field values, now we must check if we have the same number of elements in xs, ys, zs, fvalues! */
				if(this->simplices.size()==0) return false;
				if(this->xs.size()!=this->simplices[0].vectorSize()) return false;
				if(this->ys.size()!=this->simplices[0].vectorSize()) return false;
				if(this->zs.size()!=this->simplices[0].vectorSize()) return false;
				if(this->fvalues.size()!=this->simplices[0].vectorSize()) return false;
				return true;
			}
			
			/// This member function returns the field value associated to a vertex in the current simplicial complex.
			/**
			 * This member function returns the field value associated to a vertex in the simplicial complex to be built.<p>If we cannot complete this operation for any reason, then this
			 * member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param i the identifier of the required vertex
			 * \param f the field value of the required vertex
			 * \see mangrove_tds::TSManager::getPropertyName4FieldValue(), mangrove_tds::TSManager::supportsFieldValues(), mangrove_tds::SIMPLEX_ID
			 */
			inline void getFieldValue(SIMPLEX_ID i, double &f)
			{
				/* First, we check if all is ok and then we extract the i-th point from 'fvalues' */
				assert(this->supportsFieldValues());
				assert(i<this->xs.size());
				f=this->fvalues[i];
			}
			
			/// This member function parses the content of a file written in <i>TS</i> format.
			/**
			 * This member function parses the content of a file written in the <i>TS</i> format, that contains that contains a description of a field value with a domain approximated by a
			 * tetrahedral grid. However, we can also describe a non-regular grid, i.e. composed by elements of different dimensionalities.<p>The TS format is defined as follows:<p>
			 * <i>[ number of vertices ] [ number of tetrahedra ]<br>for each vertex:[ Euclidean coordinates ] [ field value ]<br>for each tetrahedron:[ 4 indices for its vertices ].</i><p>We
			 * can describe elements different from a tetrahedron, replicating indices in its description.<p>In this component, the Euclidean coordinates and field values are supported for
			 * each vertex. The string to be used for the Euclidean coordinates is <i>coordinates</i>, returned by the mangrove_tds::TSManager::getPropertyName4Coordinates() member function:
			 * you can extract coordinates for a vertex through the mangrove_tds::TSManager::getCoordinates() member function. The string to be used for the field value is <i>field_value</i>,
			 * returned by the mangrove_tds::TSManager::getPropertyName4FieldValue(): you can extract a field value for a vertex through the mangrove_tds::TSManager::getFieldValue() member
			 * function.<p>If the input file does not exist or its content does not satisfy the <i>TS</i> format guidelines, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param fname the path of the input file
			 * \param reqParams this boolean flag identfies if the loading of the auxiliary parameters is required
		 	 * \param gd this boolean flag indicates if the effective generation of raw simplices must be performed
		 	 * \param checksOn this boolean flag indicates if the effective checks on the input data must be performed
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::Simplex, mangrove_tds::SimplicesContainer, mangrove_tds::SimplexPointer, mangrove_tds::MeshComponent,
			 * mangrove_tds::TSManager::resetState(), mangrove_tds::TSManager::getCoordinates(), mangrove_tds::TSManager::getPropertyName4Coordinates(),
			 * mangrove_tds::TSManager::getPropertyName4FieldValue(), mangrove_tds::TSManager::getFieldValue()
			 */
			inline void loadData(string fname,bool reqParams,bool gd,bool checksOn)
			{
				vector<unsigned int> ids;
				unsigned int nv,nt;
				vector<double> v;
				double f;

				/* ================================================
			 	 * Repetita iuvant:
				 *
				 * < number of vertices > < number of elements >
				 * for each vertex: < coordinates > < field value >
				 * for each element: < 4 indices >
				 * ================================================ */
				this->resetState();
				this->in.open(fname.c_str());
				assert(this->in.is_open());
				this->in>>nv;
				this->in>>nt;
				if(nv==0) return;
				this->simplices.push_back(SimplicesContainer(0));
				this->components.push_back(MeshComponent());
				v.resize(3);
				for(unsigned int k=0;k<nv;k++)
				{
					/* Now, we read all the coordinates and we store them if and only if it is required! */
					this->in>>setprecision(9)>>v;
					this->in>>setprecision(9)>>f;
					this->components.back().addPoint(v[0],v[1],v[2]);
					this->components.back().addFieldValue(f);
				}
				
				/* Now, we read the top simplices! */
				ids.resize(4);
				for(unsigned int k=0;k<nt;k++)
				{
					this->in>>ids;
					this->components.back().addSimplex(ids);
				}
				
				/* If we arrive here, we have finished the effective reading from the input file! */
				this->in.close();
				if(checksOn) this->mergeComponents(reqParams,gd);
				else { this->copyComponents(reqParams,gd); }
			}
			
			/// This member function exports a simplicial complex in a file written in <i>TS</i> format.
			/**
			 * This member function exports a simplicial complex, by writing its representation in accordance with the <i>TS</i> format, that contains a description of a field value with a
			 * domain approximated by a tetrahedral grid. However, we can also describe a non-regular grid, i.e. composed by elements of different dimensionalities.<p>The TS format is defined
			 * as follows:<p><i>[ number of vertices ] [ number of tetrahedra ]<br>for each vertex: [ Euclidean coordinates ] [ field value ]<br>for each tetrahedron:
			 * [ 4 indices for its vertices ].</i><p>We can describe elements different from a tetrahedron, replicating indices in its description.<p>We assume Euclidean coordinates in the
			 * input simplicial complex are stored in a local property named as the string returned by the mangrove_tds::TSManager::getPropertyName4Coordinates() member function. We assume
			 * field values in the input simplicial complex are stored in a local property named as the string returned by the mangrove_tds::TSManager::getPropertyName4FieldValue(): you can
			 * extract a field value for a vertex through the mangrove_tds::TSManager::getFieldValue() member function.<p>Moreover, we write only the valid simplices, i.e. simplices with real
			 * locations and not marked as <i>deleted</i> by the garbage collector mechanism.<p>If we cannot complete this operation for any reason, then this member function will fail if and
			 * only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
		 	 * \param ccw the input simplicial complex to be written
			 * \param fname the path for the output file
			 * \see mangrove_tds::SimplexPointerMap, mangrove_tds::BaseSimplicialComplex, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::LocalPropertyHandle,
			 * mangrove_tds::TSManager::getPropertyName4Coordinates(), mangrove_tds::TSManager::supportsEuclideanCoordinates(), mangrove_tds::TSManager::writeGlobalComplex(),
			 * mangrove_tds::TSManager::writeLocalComplex()
			 */
			template <class T> void writeData(BaseSimplicialComplex<T> *ccw,string fname)
			{
				/* ================================================
			 	 * Repetita iuvant:
				 *
				 * < number of vertices > < number of elements >
				 * for each vertex: < coordinates > < field value >
				 * for each element: < 4 indices >
				 * ================================================ */
				assert(ccw!=NULL);
				if(ccw->encodeAllSimplices()==true) this->writeGlobalComplex(ccw,fname);
				else this->writeLocalComplex(ccw,fname);
			}
			
			/// This member function exports a subcomplex in a file written in <i>TS</i> format.
			/**
			 * This member function exports a subcomplex in a file written in <i>TS</i> format, that contains a description of a field value with a domain approximated by a tetrahedral grid.
			 * However, we can also describe a non-regular grid, i.e. composed by elements of different dimensionalities.<p>The TS format is defined as follows:<p><i>[ number of vertices ]
			 * [ number of tetrahedra ]<br>for each vertex: [ Euclidean coordinates ] [ field value ]<br>for each tetrahedron: [ 4 indices for its vertices ].</i><p>We can describe elements
			 * different from a tetrahedron, replicating indices in its description.<p>We write only the valid simplices, i.e. simplices with real locations and not marked as <i>deleted</i>
			 * by the garbage collector mechanism.<p>Here, we consider a subset of simplices directly encoded in the input data structure, that must be valid simplices, i.e. simplices with
			 * real locations and not marked as <i>deleted</i> by the garbage collector mechanism.<p>We assume Euclidean coordinates in the input simplicial complex are stored in a local
			 * property named as the string returned by the mangrove_tds::TSManager::getPropertyName4Coordinates() member function. Again, we assume field values in the input simplicial
			 * complex are stored in a local property named as the string returned by the mangrove_tds::TSManager::getPropertyName4FieldValue(): you can extract a field value for a vertex
			 * through the mangrove_tds::TSManager::getFieldValue() member function.<p><b>IMPORTANT:</b> if we cannot complete this operation for any reason, then this member function will
			 * fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param tops the simplices to be considered top in the new subcomplex to be written
			 * \param ccw the reference simplicial complex
			 * \param fname the path of the output file
			 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::SimplexPointer, mangrove_tds::Point, mangrove_tds::SIMPLEX_ID, mangrove_tds::RawFace,
			 * mangrove_tds::LocalPropertyHandle, mangrove_tds::SimplexPointerMap, mangrove_tds::TSManager::getPropertyName4Coordinates(),
			 * mangrove_tds::TSManager::supportsEuclideanCoordinates(), mangrove_tds::TSManager::getCoordinates(), mangrove_tds::TSManager::getPropertyName4FieldValue(),
			 * mangrove_tds::TSManager::supportsFieldValues(), mangrove_tds::TSManager::getFieldValue()
			 */
			template <class T> void writeSubcomplex( vector<SimplexPointer> &tops, BaseSimplicialComplex<T> *ccw, string fname)
			{
				string ec_name,fv_name;
				unsigned int n;
				SIMPLEX_ID vind;
				Point<double> p;
				double aaa;
				list<GhostSimplexPointer> bk_loc;
				set<SIMPLEX_ID> vs;
				map<SIMPLEX_ID,SIMPLEX_ID> vett;
				vector< vector<RawFace> > simplexes;
				GhostSimplex s;
				list<SimplexPointer> bk;
				
				/* ================================================
			 	 * Repetita iuvant:
				 *
				 * < number of vertices > < number of tetrahedra >
				 * for each vertex: < Euclidean coordinates > < field value >
				 * for each tetrahedron: < 4 indices for its vertices >
				 * ================================================ */
				ec_name=string(this->getPropertyName4Coordinates());
				fv_name=string(this->getPropertyName4FieldValue());
				assert(ccw!=NULL);
				assert(ccw->hasProperty(string(ec_name)));
				assert(ccw->hasProperty(string(fv_name)));
				assert(ccw->type()<=3);
				if(this->out.is_open()) this->out.close();
				this->out.open(fname.c_str());
				assert(this->out.is_open());
				assert(tops.size()!=0);
				n=0;
				for(SIMPLEX_ID k=0;k<=ccw->type();k++) { simplexes.push_back(vector<RawFace>()); }
				for(vector<SimplexPointer>::iterator cp=tops.begin();cp!=tops.end();cp++)
				{
					/* Now, we consider if the current data structure is global or local! */
					if(ccw->encodeAllSimplices()==true)
					{
						/* Now, we extract the vertices of the current new top simplex (from a global data structure) */
						if(cp->type()!=0)
						{
							/* Now, we extract all the vertices of the current top simplex! */
							n=n+1;
							ccw->boundaryk( SimplexPointer(*cp),0,&bk);
							simplexes[cp->type()].push_back( RawFace());
							for(list<SimplexPointer>::iterator current=bk.begin();current!=bk.end();current++)
							{
								vs.insert(current->id());
								simplexes[cp->type()].back().getVertices().push_back(current->id());
							}
						}
					}
					else
					{
						/* Now, we extract the vertices of the current new top simplex (from a local data structure) */
						if(cp->type()!=0)
						{
							/* Now, we extract all the vertices of the current top simplex! */
							n=n+1;
							ccw->boundaryk(GhostSimplexPointer(cp->type(),cp->id()),0,&bk_loc);
							simplexes[cp->type()].push_back( RawFace());
							for(list<GhostSimplexPointer>::iterator current=bk_loc.begin();current!=bk_loc.end();current++)
							{
								ccw->ghostSimplex(GhostSimplexPointer(*current),&s);
								vs.insert(s.getCBoundary().at(0).id());
								simplexes[cp->type()].back().getVertices().push_back(s.getCBoundary().at(0).id());
							}
						} 
					}	
				}
				
				/* If we arrive here, we can write information about the vertices! */
				this->out<<vs.size()<<" "<<n<<endl;
				this->out.flush();
				vind=0;
				for(set<SIMPLEX_ID>::iterator it=vs.begin();it!=vs.end();it++)
				{
					/* Now, we write the Euclidean coordinates of the current vertex!  */
					this->out<<setprecision(9)<<ccw->getPropertyValue(string(ec_name),SimplexPointer(0,(*it)),p).x()<<" ";
					this->out<<setprecision(9)<<ccw->getPropertyValue(string(ec_name),SimplexPointer(0,(*it)),p).y()<<" ";
					this->out<<setprecision(9)<<ccw->getPropertyValue(string(ec_name),SimplexPointer(0,(*it)),p).z()<<" ";
					this->out<<setprecision(9)<<ccw->getPropertyValue(string(fv_name),SimplexPointer(0,(*it)),aaa)<<endl;
					this->out.flush();
					vett[ (*it) ] = vind;
					vind = vind+1;
				}
				
				/* Now, we can write the indexed representation of 'simplexes' */
				for(unsigned int k=0;k<simplexes.size();k++)
				{
					/* Now, we write the description of all k-simplices! */
					for(vector<RawFace>::iterator it=simplexes[k].begin();it!=simplexes[k].end();it++)
					{
						for(vector<SIMPLEX_ID>::iterator vv=it->getVertices().begin();vv!=it->getVertices().end();vv++) this->out<<vett[(*vv)]<<" ";
						for(unsigned int j=it->getVertices().size();j<4;j++) this->out<<vett[it->getVertices().back()]<<" ";
						this->out<<endl;
						this->out.flush();
					}
				}
				
				/* If we arrive here, we have finished! */
				this->out.flush();
				this->out.close();
				bk.clear();
				vs.clear();
				simplexes.clear();
				vett.clear();
				bk_loc.clear();
			}
			
			/// This member function exports a subcomplex in a file written in <i>TS</i> format.
			/**
			 * This member function exports a subcomplex in a file written in <i>TS</i> format, that contains a description of a field value with a domain approximated by a tetrahedral grid.
			 * However, we can also describe a non-regular grid, i.e. composed by elements of different dimensionalities.<p>The TS format is defined as follows:<p><i>[ number of vertices ]
			 * [ number of tetrahedra ]<br>for each vertex: [ Euclidean coordinates ] [ field value ]<br>for each tetrahedron: [ 4 indices for its vertices ].</i><p>We can describe elements
			 * different from a tetrahedron, replicating indices in its description.<p>Here, we receive as input a list of ghost simplices, i.e. simplices not directly encoded in the input
			 * data structure, that must be a <i>local</i> mangrove, i.e. encodes only a subset of simplices.<p>We assume Euclidean coordinates in the input simplicial complex are stored in a
			 * local property named as the string returned by the mangrove_tds::TSManager::getPropertyName4Coordinates() member function. Again, we assume field values in the input simplicial
			 * complex are stored in a local property named as the string returned by the mangrove_tds::TSManager::getPropertyName4FieldValue(): you can extract a field value for a vertex
			 * through the mangrove_tds::TSManager::getFieldValue() member function.<p><b>IMPORTANT:</b> if we cannot complete this operation for any reason, then this member
			 * function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param tops the simplices to be considered top in the new subcomplex to be written
			 * \param ccw the reference simplicial complex
			 * \param fname the path of the output file
			 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::Point, mangrove_tds::SIMPLEX_ID, mangrove_tds::RawFace,
			 * mangrove_tds::LocalPropertyHandle, mangrove_tds::SimplexPointerMap, mangrove_tds::TSManager::getPropertyName4Coordinates(),
			 * mangrove_tds::TSManager::supportsEuclideanCoordinates(), mangrove_tds::TSManager::getCoordinates(), mangrove_tds::TSManager::getPropertyName4FieldValue(),
			 * mangrove_tds::TSManager::supportsFieldValues(), mangrove_tds::TSManager::getFieldValue()
			 */
			template <class T> void writeSubcomplex( vector<GhostSimplexPointer> &tops, BaseSimplicialComplex<T> *ccw, string fname)
			{
				string ec_name,fv_name;
				unsigned int n;
				list<GhostSimplexPointer> bk_loc;
				vector< vector<RawFace> > simplexes;
				GhostSimplex s;
				set<SIMPLEX_ID> vs;
				SIMPLEX_ID vind;
				Point<double> p;
				double aaa;
				map<SIMPLEX_ID,SIMPLEX_ID> vett;
				
				/* ================================================
			 	 * Repetita iuvant:
				 *
				 * < number of vertices > < number of tetrahedra >
				 * for each vertex: < Euclidean coordinates > < field value >
				 * for each tetrahedron: < 4 indices for its vertices >
				 * ================================================ */
				ec_name=string(this->getPropertyName4Coordinates());
				fv_name=string(this->getPropertyName4FieldValue());
				assert(ccw!=NULL);
				assert(ccw->encodeAllSimplices()==false);
				assert(ccw->hasProperty(string(ec_name)));
				assert(ccw->hasProperty(string(fv_name)));
				assert(ccw->type()<=3);
				if(this->out.is_open()) this->out.close();
				this->out.open(fname.c_str());
				assert(this->out.is_open());
				assert(tops.size()!=0);
				n=0;
				for(SIMPLEX_ID k=0;k<=ccw->type();k++) { simplexes.push_back(vector<RawFace>()); }
				for(vector<GhostSimplexPointer>::iterator cp=tops.begin();cp!=tops.end();cp++)
				{
					/* Now, we extract the vertices of the current new ghost simplex (from a local data structure) */
					if(cp->type()!=0)
					{
						/* Now, we extract all the vertices of the current top simplex! */
						n=n+1;
						ccw->boundaryk(GhostSimplexPointer(*cp),0,&bk_loc);
						simplexes[cp->type()].push_back( RawFace());	
						for(list<GhostSimplexPointer>::iterator current=bk_loc.begin();current!=bk_loc.end();current++)
						{
							ccw->ghostSimplex(GhostSimplexPointer(*current),&s);
							vs.insert(s.getCBoundary().at(0).id());
							simplexes[cp->type()].back().getVertices().push_back(s.getCBoundary().at(0).id());
						}
					}
				}
				
				/* If we arrive here, we can write information about the vertices! */
				this->out<<vs.size()<<" "<<n<<endl;
				this->out.flush();
				vind=0;
				for(set<SIMPLEX_ID>::iterator it=vs.begin();it!=vs.end();it++)
				{
					/* Now, we write the Euclidean coordinates of the current vertex!  */
					this->out<<setprecision(9)<<ccw->getPropertyValue(string(ec_name),SimplexPointer(0,(*it)),p).x()<<" ";
					this->out<<setprecision(9)<<ccw->getPropertyValue(string(ec_name),SimplexPointer(0,(*it)),p).y()<<" ";
					this->out<<setprecision(9)<<ccw->getPropertyValue(string(ec_name),SimplexPointer(0,(*it)),p).z()<<" ";
					this->out<<setprecision(9)<<ccw->getPropertyValue(string(fv_name),SimplexPointer(0,(*it)),aaa)<<endl;
					this->out.flush();
					vett[ (*it) ] = vind;
					vind = vind+1;
				}
				
				/* Now, we can write the indexed representation of 'simplexes' */
				for(unsigned int k=0;k<simplexes.size();k++)
				{
					/* Now, we write the description of all k-simplices! */
					for(vector<RawFace>::iterator it=simplexes[k].begin();it!=simplexes[k].end();it++)
					{
						for(vector<SIMPLEX_ID>::iterator vv=it->getVertices().begin();vv!=it->getVertices().end();vv++) this->out<<vett[(*vv)]<<" ";
						for(unsigned int j=it->getVertices().size();j<4;j++) this->out<<vett[it->getVertices().back()]<<" ";
						this->out<<endl;
						this->out.flush();
					}
				}
				
				/* If we arrive here, we have finished! */
				this->out.flush();
				this->out.close();
				vs.clear();
				simplexes.clear();
				vett.clear();
				bk_loc.clear();
			}
			
			protected:
			
			/// The x-value coordinates for each vertex.
			vector<double> xs;
			
			/// The y-value coordinates for each vertex.
			vector<double> ys;
			
			/// The z-value coordinates for each vertex.
			vector<double> zs;
			
			/// The field values for each vertex.
			vector<double> fvalues;
			
			/// This member function clears the internal state.
			/**
			 * This member function clears the internal state, by deallocating its member fields.
			 * \see mangrove_tds::RawFace, mangrove_tds::SimplicesContainer, mangrove_tds::MeshComponent
			 */
			inline void resetState()
			{
				/* We deallocate the internal state of the current instance */
				this->xs.clear();
				this->ys.clear();
				this->zs.clear();
				if(this->in.is_open()) this->in.close();
				if(this->out.is_open()) this->out.close();
				for(unsigned int k=0;k<this->raw_faces.size();k++) { this->raw_faces[k].clear(); }
				this->raw_faces.clear();
				this->components.clear();
				for(unsigned int k=0;k<this->simplices.size();k++) { this->simplices[k].clearCollection(); }
				this->simplices.clear();
				this->fvalues.clear();
			}

			/// This member function copies the content of components in the current simplicial complex.
			/**
			 * This member function copies the content of components in the current simplicial complex into a unique component: each component is described through an instance of the
			 * mangrove_tds::MeshComponent. In this way, we can generate an unique set of raw simplices (described by the mangrove_tds::RawFace class): this operation is mandatory for
			 * creating a new simplicial complex, described by a subclass of the mangrove_tds::BaseSimplicialComplex class.
			 * \param reqCoords this boolean flag identfies if the Euclidean coordinates (the unique auxiliary parameters in this component) loading is required
			 * \param gd this boolean flag indicates if the effective generation of raw simplices must be performed
			 * \see mangrove_tds::RawFace, mangrove_tds::BaseSimplicialComplex, mangrove_tds::MeshComponent, mangrove_tds::DataManager::generateRawSimplices(),
			 * mangrove_tds::DataManager::loadData()
			 */
			inline void copyComponents(bool reqCoords,bool gd)
			{
				SimplexPointer cp;
				map< SimplexPointer,SIMPLEX_ID, CompareSimplex> mapper;
				vector<unsigned int> orig,aux;
				unsigned int ne,n;
				list<SIMPLEX_ID> dest;
				list<SIMPLEX_ID>::iterator it;
				SIMPLEX_ID idt;				

				/* First, we map each vertex in every component! */
				for(unsigned int k=0;k<this->components.size();k++) 
				{
					/* We add all the vertices in the k-th component in order to create a unique set of vertices! */
					for(unsigned int j=0;j<this->components[k].getPoints().size();j++)
					{
						this->simplices[0].addSimplex(cp);
						mapper[SimplexPointer(k,j)]=cp.id();
						if(reqCoords)
						{
							this->xs.push_back(this->components[k].getPoints().at(j).cx());
							this->ys.push_back(this->components[k].getPoints().at(j).cy());
							this->zs.push_back(this->components[k].getPoints().at(j).cz());
							this->fvalues.push_back(this->components[k].getFieldValues().at(j));
						}
					}
				}
				
				/* Now, we create a unique list of top simplices, starting from the list of simplices in each component! */
				for(unsigned int k=0;k<this->components.size();k++) 
				{
					/* We analyze the list of simplices  in the current component! */
					for(unsigned int j=0;j<this->components[k].getSimplices().size();j++)
					{
						/* Now, we extract the current simplex, and we must map it over the global list of vertices */
						orig=vector<unsigned int>( this->components[k].getSimplices().at(j));
						dest.clear();
						for(unsigned int i=0;i<orig.size();i++)
						{
							idt=mapper[SimplexPointer(k,orig[i])];
							dest.push_back(idt);
						}

						/* Now, we sort 'dest', and try to add this new face! */
						dest.sort();
						ne=dest.size();
						if(ne>=2)
						{
							/* In this case, we have at least two vertices and thus we can add a new top simplex! */
							if(ne-1>=this->simplices.size())
							{
								/* Now, we must create a new container! */
								n = this->simplices.size();
								this->simplices.resize(ne);
								for(unsigned int z=n;z<ne;z++) this->simplices[z].updateType(z);
							}
							
							/* Now, we can create the new simplex and then we add the boundary vertices! */
							this->simplices[ne-1].addSimplex(cp);
							it=dest.begin();
							for(unsigned int z=0;z<dest.size();z++,it++) this->simplices[ne-1].simplex(cp.id()).add2Boundary(SimplexPointer(0,*it),z);
							if( (ne>2) && (gd==true))
							{
								aux.clear();
								for(it=dest.begin();it!=dest.end();it++) aux.push_back(*it);
								this->generateRawSimplices(aux,this->simplices[ne-1].size()-1,true);
							}
						}
					}
				}
			}
			
			/// This member function merges components in the current simplicial complex.
			/**
			 * This member function merges components in the current simplicial complex into a unique component: each component is described through an instance of the
			 * mangrove_tds::MeshComponent. In this way, we can generate an unique set of raw simplices (described by the mangrove_tds::RawFace class): this operation is mandatory for
			 * creating a new simplicial complex, described by a subclass of the mangrove_tds::BaseSimplicialComplex class.
			 * \param reqParams this boolean flag identfies if the Euclidean coordinates and the field values loading is required
			 * \param gd this boolean flag indicates if the effective generation of raw simplices must be performed
			 * \see mangrove_tds::RawFace, mangrove_tds::BaseSimplicialComplex, mangrove_tds::MeshComponent, mangrove_tds::DataManager::generateRawSimplices(),
			 * mangrove_tds::DataManager::loadData()
			 */
			inline void mergeComponents(bool reqParams,bool gd)
			{
				VERTICES_SET vs;
				VERTICES_FIELDS fs;
				SimplexPointer cp;
				VERTICES_MAPPER mapper;
				unsigned int ne,n;
				list<unsigned int> dest;
				vector<unsigned int> orig,aux;
				list<unsigned int>::iterator it;
				
				/* First, we must build a unique set of vertices starting from all the components! */
				for(unsigned int k=0;k<this->components.size();k++) 
				{
					/* We add all the vertices in the k-th component in order to create a unique set of vertices! */
					for(unsigned int j=0;j<this->components[k].getPoints().size();j++)
					{
						vs.insert( this->components[k].getPoints().at(j));
						fs[this->components[k].getPoints().at(j)]=this->components[k].getFieldValues().at(j);
					}
				}
				
				/* Now, we map the unique set of vertices, by adding all the vertices! */
				for(VERTICES_SET::iterator it=vs.begin();it!=vs.end();it++)
				{
					/* Now, we add all the vertices and we store their coordinates! */
					this->simplices[0].addSimplex(cp);
					mapper[ (*it) ] = cp.id();
					if(reqParams)
					{
						this->xs.push_back(it->cx());
						this->ys.push_back(it->cy());
						this->zs.push_back(it->cz());
						this->fvalues.push_back(fs[*it]);
					}
				}
				
				/* Now, we must build a unique set of simplices: first, we map them into the global list of vertices! */
				for(unsigned int k=0;k<this->components.size();k++)
				{
					/* Now, we must map all the simplices in the k-th component over the global list of vertices */
					for(unsigned int j=0;j<this->components[k].getSimplices().size();j++)
					{
						/* Now, we extract the current simplex and we must map it over the global list of vertices */
						orig.clear();
						dest.clear();
						orig = vector<unsigned int>( this->components[k].getSimplices().at(j));
						for(unsigned int i=0;i<orig.size();i++) { dest.push_back(mapper[this->components[k].getPoints().at(orig[i])]); }
						dest.sort();
						dest.unique();
						ne = dest.size();
						if(ne>=2)
						{
							/* In this case, we have at least two vertices and thus we can add a new top simplex */
							if(ne-1>=this->simplices.size())
							{
								/* Now, we must create a new container! */
								n = this->simplices.size();
								this->simplices.resize(ne);
								for(unsigned int z=n;z<ne;z++) this->simplices[z].updateType(z);
							}
							
							/* Now, we can create the new simplex and then we add the boundary vertices! */
							this->simplices[ne-1].addSimplex(cp);
							it=dest.begin();
							for(unsigned int z=0;z<dest.size();z++,it++) this->simplices[ne-1].simplex(cp.id()).add2Boundary(SimplexPointer(0,*it),z);
							if( (ne>2) && (gd==true))
							{
								aux.clear();
								for(it=dest.begin();it!=dest.end();it++) aux.push_back(*it);
								this->generateRawSimplices(aux,this->simplices[ne-1].size()-1,true);
							}
						}
					}
				}
			}

			/// This member function exports a simplicial complex in a file written in the <i>TS</i> format.
			/**
			 * This member function exports a simplicial complex described by a data structure encoding all simplices (namely a <i>global</i> data structure). This member function writes a
			 * representation of the input simplicial complex in accordance with the <i>TS</i> format, that contains a description of a field value with a domain approximated by a tetrahedral
			 * grid. However, we can also describe a non-regular grid, i.e. composed by elements of different dimensionalities.<p>The TS format is defined as follows:<p>
			 * <i>[ number of vertices ] [ number of tetrahedra ]<br>for each vertex: [ Euclidean coordinates ] [ field value ]<br>for each tetrahedron: [ 4 indices for its vertices ].</i><p>
			 * We can describe elements different from a tetrahedron, replicating indices in its description.<p>We assume Euclidean coordinates in the input simplicial complex are stored in a
			 * local property named as the string returned by the mangrove_tds::TSManager::getPropertyName4Coordinates() member function. We assume field values in the input simplicial 
			 * complex are stored in a local property named as the string returned by the mangrove_tds::TSManager::getPropertyName4FieldValue(): you can extract a field value for a vertex
			 * through the mangrove_tds::TSManager::getFieldValue() member function.<p>Moreover, we write only the valid simplices, i.e. simplices with real locations and not marked as
			 * <i>deleted</i> by the garbage collector mechanism.<p>If we cannot complete this operation for any reason, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param ccw the input global simplicial complex to be represented
			 * \param fname the path of the output file
			 * \see mangrove_tds::SimplexPointerMap, mangrove_tds::BaseSimplicialComplex, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::LocalPropertyHandle,
			 * mangrove_tds::TSManager::getPropertyName4Coordinates(), mangrove_tds::TSManager::supportsEuclideanCoordinates(), mangrove_tds::TSManager::getCoordinates(),
			 * mangrove_tds::TSManager::getPropertyName4FieldValue(), mangrove_tds::TSManager::supportsFieldValues(), mangrove_tds::TSManager::getFieldValue(),
			 * mangrove_tds::TSManager::writeGlobalComplex()
			 */
			template <class T> void writeGlobalComplex(BaseSimplicialComplex<T> *ccw,string fname)
			{
				string ec_name,fv_name;
				SimplexPointerMap<T> map;
				list<SimplexPointer> tops,bk;
				SimplexIterator it;
				Point<double> p;
				unsigned int vt;
				double f;

				/* ================================================
			 	 * Repetita iuvant:
				 *
				 * < number of vertices > < number of tetrahedra >
				 * for each vertex: < Euclidean coordinates > < field value >
				 * for each tetrahedron: < 4 indices for its vertices >
				 * ================================================ */
				ec_name=string(this->getPropertyName4Coordinates());
				fv_name=string(this->getPropertyName4FieldValue());
				assert(ccw->hasProperty(string(ec_name)));
				assert(ccw->hasProperty(string(fv_name)));
				assert(ccw->type()<=3);
				if(this->out.is_open()) this->out.close();
				this->out.open(fname.c_str());
				assert(this->out.is_open());
				map.setComplex(ccw);
				map.forwardMapping();
				this->out<<ccw->size(0)<<" ";
				ccw->getTopSimplices(tops);
				vt=ccw->getTopSimplicesNumber(0);
				this->out<<(tops.size()-vt)<<endl;
				for(it=ccw->begin(0);it!=ccw->end(0);it++)
				{
					/* Now, we write the Euclidean coordinates of the current vertex!  */
					this->out<<setprecision(9)<<ccw->getPropertyValue(string(ec_name),SimplexPointer(it.getPointer()),p).x()<<" ";
					this->out<<setprecision(9)<<ccw->getPropertyValue(string(ec_name),SimplexPointer(it.getPointer()),p).y()<<" ";
					this->out<<setprecision(9)<<ccw->getPropertyValue(string(ec_name),SimplexPointer(it.getPointer()),p).z()<<" ";
					this->out<<setprecision(9)<<ccw->getPropertyValue(string(fv_name),SimplexPointer(it.getPointer()),f)<<endl;
					this->out.flush();
				}
				
				/* Now, we write all the top simplices! */
				for(list<SimplexPointer>::iterator cp=tops.begin();cp!=tops.end();cp++)
				{
					/* Now, we write a new top simplex! */
					if(cp->type()>=1)
					{
						/* We haven't a top simplex! */
						ccw->boundaryk( SimplexPointer(*cp),0,&bk);
						for(list<SimplexPointer>::iterator current=bk.begin();current!=bk.end();current++) this->out<<map.getId(SimplexPointer(*current))<<" ";
						for(unsigned int k=bk.size();k<4;k++) this->out<<map.getId(SimplexPointer(bk.back()))<<" ";
						this->out<<endl;
					}
				}
				
				/* If we arrive here, we have finished! */
				this->out.flush();
				this->out.close();
			}

			/// This member function exports a simplicial complex in a file written in the <i>TS</i> format.
			/**
			 * This member function exports a simplicial complex described by a data structure encoding only a subset of all simplices (namely a <i>local</i> data structure). This member
			 * function writes a representation of the input simplicial complex in accordance with the <i>TS</i> format, that contains a description of a field value  with a domain
			 * approximated by a tetrahedral grid. However, we can also describe a non-regular grid, i.e. composed by elements of different dimensionalities.<p>The TS format is defined as
			 * follows:<p><i>[ number of vertices ] [ number of tetrahedra ]<br>for each vertex: [ Euclidean coordinates ] [ field value ]<br>for each tetrahedron:
			 * [ 4 indices for its vertices ].</i><p>We can describe elements different from a tetrahedron, replicating indices in its description.<p>We assume Euclidean coordinates in the
			 * input simplicial complex are stored in a local property named as the string returned by the mangrove_tds::TSManager::getPropertyName4Coordinates() member function. We assume
			 * field values in the input simplicial complex are stored in a local property named as the string returned by the mangrove_tds::TSManager::getPropertyName4FieldValue(): you can
			 * extract a field value for a vertex through the mangrove_tds::TSManager::getFieldValue() member function.<p>Moreover, we write only the valid simplices, i.e. simplices with real
			 * locations and not marked as <i>deleted</i> by the garbage collector mechanism.<p>If we cannot complete this operation for any reason, then this member function will fail if and
			 * only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param ccw the input global simplicial complex to be represented
			 * \param fname the path of the output file
			 * \see mangrove_tds::SimplexPointerMap, mangrove_tds::BaseSimplicialComplex, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::LocalPropertyHandle,
			 * mangrove_tds::TSManager::getPropertyName4Coordinates(), mangrove_tds::TSManager::supportsEuclideanCoordinates(), mangrove_tds::TSManager::getCoordinates(),
			 * mangrove_tds::TSManager::getPropertyName4FieldValue(), mangrove_tds::TSManager::supportsFieldValues(), mangrove_tds::TSManager::getFieldValue(),
			 * mangrove_tds::TSManager::writeGlobalComplex()
			 */
			template <class T> void writeLocalComplex(BaseSimplicialComplex<T> *ccw,string fname)
			{
				string ec_name,fv_name;
				SimplexPointerMap<T> map;
				list<SimplexPointer> tops;
				unsigned int vt;
				SimplexIterator it;
				Point<double> p;
				double f;
				GhostSimplex s;
				
				/* ================================================
			 	 * Repetita iuvant:
				 *
				 * < number of vertices > < number of tetrahedra >
				 * for each vertex: < Euclidean coordinates > < field value >
				 * for each tetrahedron: < 4 indices for its vertices >
				 * ================================================ */
				ec_name=string(this->getPropertyName4Coordinates());
				fv_name=string(this->getPropertyName4FieldValue());
				assert(ccw->hasProperty(string(ec_name)));
				assert(ccw->hasProperty(string(fv_name)));
				assert(ccw->type()<=3);
				if(this->out.is_open()) this->out.close();
				this->out.open(fname.c_str());
				assert(this->out.is_open());
				map.setComplex(ccw);
				map.forwardMapping();
				this->out<<ccw->size(0)<<" ";
				ccw->getTopSimplices(tops);
				vt=ccw->getTopSimplicesNumber(0);
				this->out<<(tops.size()-vt)<<endl;
				for(it=ccw->begin(0);it!=ccw->end(0);it++)
				{
					/* Now, we write the Euclidean coordinates of the current vertex!  */
					this->out<<setprecision(9)<<ccw->getPropertyValue(string(ec_name),SimplexPointer(it.getPointer()),p).x()<<" ";
					this->out<<setprecision(9)<<ccw->getPropertyValue(string(ec_name),SimplexPointer(it.getPointer()),p).y()<<" ";
					this->out<<setprecision(9)<<ccw->getPropertyValue(string(ec_name),SimplexPointer(it.getPointer()),p).z()<<" ";
					this->out<<setprecision(9)<<ccw->getPropertyValue(string(fv_name),SimplexPointer(it.getPointer()),f)<<endl;
					this->out.flush();
				}
				
				/* Now, we write all the top simplices! */
				for(list<SimplexPointer>::iterator cp=tops.begin();cp!=tops.end();cp++)
				{
					/* Now, we write a new top simplex! */
					if(cp->type()>=1)
					{
						/* We haven't a top 0-simplex! */
						ccw->ghostSimplex(GhostSimplexPointer(cp->type(),cp->id()),&s);
						for(vt=0;vt<=cp->type();vt++) this->out<<map.getId(SimplexPointer(s.getBoundary()[vt]))<<" ";
						for(vt=cp->type()+1;vt<4;vt++) this->out<<map.getId(SimplexPointer(s.getBoundary().back()))<<" ";
						this->out<<endl;
					}
				}
				
				/* If we arrive here, we have finished! */
				this->out.flush();
				this->out.close();
			}
		};
	}

#endif

