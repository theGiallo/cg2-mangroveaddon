/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library                                
 *                                                                       				    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * September 2011 (Revised on May 2012)    
 *                                                                 
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * SIG.h -  implementation of the Simplified Incidence Graph (SIG)
 ***********************************************************************************************/

/* Should we include this header file? */
#ifndef SIG_H_

	#define SIG_H_

	#include "BaseSimplicialComplex.h"
	#include "Miscellanea.h"
	#include <assert.h>
	#include <cstdlib>
	#include <queue>
	using namespace mangrove_tds;
	using namespace std;
	
	/// Implementation of the <i>Simplified Incidence Graph</i>.
	/**
	 * The class defined in this file implements the <i>Simplified Incidence Graph</i>, a dimension-independent and incidence-based data structure for representing simplicial complexes with an
	 * arbitrary domain. This data structure encodes all the simplices in a simplicial complex, thus it is a <i>global</i> data structure. For each k-simplex, it encodes:<ul><li>all k+1 simplices of
	 * dimension k-1 belonging to its boundary;</li><li>one top j-simplex (with j>k) for each j-cluster in its star.</li></ul><p>This class must be used in conjunction with the
	 * mangrove_tds::BaseSimplicialComplex class in accordance with the <A href=http://en.wikipedia.org/wiki/Curiously_recurring_template_pattern>Curiously Recurring Template Pattern</A> rules in
	 * order to implement the static polymorphism: in this way we can reuse some parts of the mangrove_tds::BaseSimplicialComplex class and we can achieve more efficient implementations, by
	 * discarding the virtual member functions.
	 * \file SIG.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	
	/* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		/// Implementation of the <i>Simplified Incidence Graph</i>.
		/**	
	 	 * The class defined in this file implements the <i>Simplified Incidence Graph</i>, a dimension-independent and incidence-based data structure for representing simplicial complexes with
		 * an arbitrary domain. This data structure encodes all the simplices in a simplicial complex, thus it is a <i>global</i> data structure. For each k-simplex, it encodes:<ul><li>all k+1
		 * simplices of dimension k-1 belonging to its boundary;</li><li>one top j-simplex (with j>k) for each j-cluster in its star.</li></ul><p>This class must be used in conjunction with the
		 * mangrove_tds::BaseSimplicialComplex class in accordance with the <A href=http://en.wikipedia.org/wiki/Curiously_recurring_template_pattern>Curiously Recurring Template Pattern</A>
		 * rules in order to implement the static polymorphism: in this way we can reuse some parts of the mangrove_tds::BaseSimplicialComplex class and we can achieve more efficient
		 * implementations, by discarding the virtual member functions.
	 	 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplexPointer, mangrove_tds::IG, mangrove_tds::IS, mangrove_tds::GIA,
	 	 * mangrove_tds::TriangleSegment, mangrove_tds::NMIA
	 	 */
	 	class SIG : public BaseSimplicialComplex<SIG>
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates all the internal data structures required for encoding a simplicial complex through a <i>Simplified Incidence Graph</i>. In this member function, we
			 * allocate all the internal data structures without any simplices or properties, i.e. auxiliary information, like Euclidean coordinates and field values, associated to simplices
			 * directly encoded in the <i>Simplified Incidence Graph</i>. Thus, you should apply the member functions offered by the mangrove_tds::SIG class in order to add simplices and
			 * properties to the current data structure encoding a new simplicial complex.
			 * \param t the dimension of the simplicial complex to be encoded in the current data structure
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::init(),
			 * mangrove_tds::BaseSimplicialComplex::clear(), mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::addGlobalProperty(),
			 * mangrove_tds::BaseSimplicialComplex::addLocalProperty(), mangrove_tds::BaseSimplicialComplex::addSparseProperty()
			 */
			inline SIG(SIMPLEX_TYPE t) : BaseSimplicialComplex<SIG>(t) { ; }
			
			/// This member function destroys an instance of this class.
			inline virtual ~SIG() { ; }

			/// This member function returns a string that describes the current data structure.
			/**
 			 * This member function returns a string that describes the current data structure, usually its name.
 			 * \return a string that describes the current data structure
 			 * \see mangrove_tds::BaseSimplicialComplex::debug()
 			 */
 			inline string getDataStructureName() { return string("Simplified Incidence Graph (SIG)"); }
 			
 			/// This member function checks if the current data structure encodes all simplices in the simplicial complex to be represented.
 			/**
 			 * This member function checks if the current data structure encodes all simplices in the current simplicial complex: in this case, it is called <i>global</i> data structure.
			 * Alternatively, a data structure can encode a subset of all simplices, for instance all top simplices: in this case, it is called <i>local</i> data structure.<p>The
			 * <i>Simplified Incidence Graph</i> encodes all the simplices and it is a <i>global</i> data structure.
 			 * \return <ul><li><i>true</i>, if the current data structure encodes all simplices</li><li><i>false</i>, otherwise</li></ul>
 			 * \see mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex::isTop(), mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer
 			 */
 			inline bool encodeAllSimplices() { return true; }
 			
 			/// This member function computes the storage cost of the current data structure.
			/**
			 * This member function computes the storage cost of the current data structure, expressed as number of pointers to the simplices directly encoded in the current data structure. In
			 * ther words, we compute the number of instances of the mangrove_tds::SimplexPointer class required for encoding the current data structure.
			 * \return the storage cost of the current data structure
			 * \see mangrove_tds::Simplex, mangrove_tds::SimplexPointer, mangrove_tds::SimplexIterator, mangrove_tds::BaseSimplicialComplex::type(), mangrove_tds::SIMPLEX_TYPE
			 */
 			inline unsigned int getStorageCost()
 			{
 				unsigned int c;
				SIMPLEX_TYPE t,d;
				SimplexIterator it;

				/* We must iterate on all the simplices in the current data structure! */
				c=0;
				t=this->type();
				for(d=0;d<=t;d++)
				{
					/* Now, we iterate over all the d-simplices! */
					for(it=this->begin(d);it!=this->end(d);it++)
					{
						c=c+it->getBoundarySize();
						c=c+it->getCoboundarySize();
					}
				}
				
				/* If we arrive here, we can return the storage cost! */
				return c;
 			}
 			
 			/// This member function identifies all simplices belonging to the boundary of a simplex directly encoded in the current data structure.
 			/**
 			 * This member function identifies all simplices belonging to the boundary of a simplex directly encoded in the current data structure.<p>The reference simplex and the boundary
 			 * simplices are required to be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector
 			 * mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function
 			 * only with a <i>global</i> data structure.<p>The <i>Simplified Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure.<p>If we cannot complete this
 			 * operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a
			 * failed assert.<p><b>IMPORTANT:</b> in this member function, we apply all the built-in facilities offered by the current data structure for marking simplices. You must apply a
			 * specific property (i.e. an instance of the mangrove_tds::PropertyBase class) for other application-dependent marking.
	 		 * \param cp the reference simplex directly encoded in the current data structure
	 		 * \param ll a list containing all simplices belonging to the boundary of a simplex directly encoded in the current data structure
	 		 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, ,mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::BaseSimplicialComplex::visit(), mangrove_tds::BaseSimplicialComplex::isVisited(), mangrove_tds::BaseSimplicialComplex::unmarkAllVisited(),
	 		 * mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::PropertyBase
	 		 */
 			inline void boundary(const SimplexPointer& cp, list<SimplexPointer>* ll)
	 		{
	 			queue<SimplexPointer> q;
	 			SimplexPointer curr;

	 			/* First, we check if the input parameters are ok! */
	 			assert(this->isValid(cp));
	 			assert(ll!=NULL);
	 			ll->clear();
	 			if(cp.type()==0) { ; }
	 			else if(cp.type()==1)
	 			{
	 				/* The input simplex is an edge! */
	 				ll->push_back(SimplexPointer(this->simplex(cp).bc(0)));
	 				ll->push_back(SimplexPointer(this->simplex(cp).bc(1)));
	 			}
	 			else
	 			{
	 				/* The input simplex is not a vertex or an edge, thus we have a triangle, at least */
	 				for(unsigned int j=0;j<=cp.type();j++) q.push(SimplexPointer(this->simplex(cp).bc(j)));
	 				while(q.empty()==false)
	 				{
	 					/* Now, we visit the first simplex in queue! */
	 					curr=q.front();
	 					q.pop();
	 					if(this->isVisited(curr)==false)
	 					{
	 						/* Now, we visit the current simplex 'curr' */
	 						this->visit(curr);
	 						ll->push_back(SimplexPointer(curr));
	 						if(curr.type()!=0) { for(unsigned int j=0;j<=curr.type();j++) q.push(SimplexPointer(this->simplex(curr).bc(j))); }
	 					}
	 				}
	 				
	 				/* If we arrive here, we visited all simplices in the boundary of 'cp' */
	 				this->unmarkAllVisited();
	 				ll->sort(compare_simplices_pointers);
	 			}
	 		}
 			
 			/// This member function identifies all simplices belonging to the boundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the boundary of a simplex not directly encoded in the current data structure.<p>The reference simplex and the boundary
	 		 * simplices can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this
	 		 * member function only with a <i>local</i> data structure.<p>The <i>Simplified Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure: this member
	 		 * function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
	 		 * \param ll a list containing all simplices belonging to the boundary of a simplex not directly encoded in the current data structure.
	 		 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::BaseSimplicialComplex::encodeAllSimplices()
	 		 */
	 		inline void boundary(const GhostSimplexPointer& cp, list<GhostSimplexPointer>* ll)
	 		{
	 			/* Dummy statetement for avoiding compiler warnings! */
	 			idle(cp);
	 			idle(ll);
	 			assert(this->encodeAllSimplices()==false);
	 		}
	 		
	 		/// This member function identifies all simplices belonging to the boundary of a simplex directly encoded in the current data structure.
 			/**
 			 * This member function identifies all simplices of a certain dimension belonging to the boundary of a simplex directly encoded in the current data structure: this member function
 			 * filters boundary simplices against their dimension.<p>The reference simplex and the boundary simplices are required to be <i>valid</i>, i.e. directly stored in the current data
 			 * structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class
 			 * and thus by their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>If we cannot complete this operation, then
 			 * this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.<p><b>IMPORTANT:</b> in this member function, we apply all the built-in facilities offered by the current data structure for marking simplices. You must apply a specific
			 * property (i.e. an instance of the mangrove_tds::PropertyBase class) for other application-dependent marking.
 			 * \param cp the reference simplex directly encoded in the current data structure
 			 * \param k the dimension of the required simplices belonging to the input simplex boundary
	 		 * \param ll a list containing all simplices belonging to the boundary of a simplex directly encoded in the current data structure
 			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase,
 			 * mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isValid()
	 		 */
	 		inline void boundaryk(const SimplexPointer& cp,SIMPLEX_TYPE k,list<SimplexPointer> *ll)
	 		{
	 			unsigned int j;
	 			SimplexPointer curr;
	 			queue<SimplexPointer> q;

	 			/* First, we check if the input parameters are ok! */
	 			assert(this->isValid(cp));
	 			assert(ll!=NULL);
	 			ll->clear();
	 			assert(cp.type()!=0);
	 			assert(k<cp.type());
	 			if(cp.type()==0) { ; }
	 			else if(k==cp.type()-1)
	 			{
	 				/* Now, we consider the immediate boundary! */
	 				for(j=0;j<=cp.type();j++) { ll->push_back(SimplexPointer(this->simplex(cp).bc(j))); }
	 				if(cp.type()!=1) ll->sort(compare_simplices_pointers);
	 			}
	 			else
	 			{
	 				/* Now, we can extract all the required simplices */
	 				for(j=0;j<=cp.type();j++) q.push(SimplexPointer(this->simplex(cp).bc(j)));
	 				while(q.empty()==false)
	 				{
	 					/* Now, we visit the first simplex in queue! */
	 					curr=q.front();
	 					q.pop();
	 					if(this->isVisited(curr)==false)
	 					{
	 						/* Now, we visit the current simplex 'curr' */
	 						this->visit(curr);
	 						if(curr.type()==k) ll->push_back(SimplexPointer(curr));
	 						else if(curr.type()>k) { for(j=0;j<=curr.type();j++) q.push(SimplexPointer(this->simplex(curr).bc(j))); }
	 					}
	 				}
	 				
	 				/* If we arrive here, we visited all k-simplices in the boundary of 'cp' */
	 				this->unmarkAllVisited();
	 				ll->sort(compare_simplices_pointers);		
	 			}
	 		}

	 		/// This member function identifies all simplices of a certain dimension belonging to the boundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices of a certain dimension belonging to the boundary of a simplex not directly encoded in the current data structure: this member
			 * function filters boundary simplices against their dimension.<p>The reference simplex and the boundary simplices can also not directly encoded in the current data structure and
			 * they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>The
	 		 * <i>Simplified Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure: consequently, this member function will fail if and only if the
	 		 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
			 * \param k the dimension of the required simplices belonging to the input simplex boundary
			 * \param ll a list of the required simplices belonging to the input simplex boundary
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase,
	 		 * mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::idle()
			 */
	 		inline void boundaryk(const GhostSimplexPointer& cp,SIMPLEX_TYPE k,list<GhostSimplexPointer> *ll)
	 		{
	 			/* Dummy statetement for avoiding compiler warnings! */
	 			idle(cp);
	 			idle(ll);
	 			idle(k);
	 			assert(this->encodeAllSimplices()==false);
	 		}
	 		
	 		/// This member function identifies all simplices belonging to the coboundary of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the coboundary of a simplex directly encoded in the current data structure.<p>The reference simplex and the star
	 		 * simplices are required to be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector
	 		 * mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function
	 		 * only with a <i>global</i> data structure.<p>The <i>Simplified Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure.<p>If we cannot complete this
	 		 * operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a
			 * failed assert.<p><b>IMPORTANT:</b> when you try to apply this member function, you should be careful about what auxiliary information (i.e. subclasses of the
	 		 * mangrove_tds::PropertyBase class) is associated to the current simplicial complex. In other words, in this member function, we use two global properties (i.e. two instances of
			 * the mangrove_tds::GlobalPropertyHandle class) with boolean values, respectively called <i>visited_cob</i> and <i>assigned</i>, and a local property (i.e. an instance of the
	 		 * mangrove_tds::LocalPropertyHandle class) for all the d-simplices, called <i>refSimplex</i>. We remove such properties, if they already belong to the current simplicial complex.
			 * In any case, we remove them at the end of this member function.
 			 * \param cp the reference simplex directly encoded in the current data structure
			 * \param ll a list of the required simplices belonging to the input simplex coboundary
			 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::LocalPropertyHandle,
	 		 * mangrove_tds::BaseSimplicialComplex::addGlobalProperty(), mangrove_tds::BaseSimplicialComplex::addLocalProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty()
	 		 */
			inline void coboundary(const SimplexPointer& cp,list<SimplexPointer> *ll)
	 		{
	 			LocalPropertyHandle<bool> *rsp;
	 			GlobalPropertyHandle<bool> *asp;
	 			GlobalPropertyHandle<bool> *vcp;
				unsigned int n,z;
				queue<SimplexPointer> q;
				list<SimplexPointer> bnd;
				SimplexPointer ni;

	 			/* First, we check if the input parameters are ok! */
	 			assert(this->isValid(cp));
	 			assert(ll!=NULL);
	 			ll->clear();
	 			if(this->hasProperty(string("refSimplex"))) this->deleteProperty(string("refSimplex"));
	 			if(this->hasProperty(string("assigned"))) this->deleteProperty(string("assigned"));
	 			if(this->hasProperty(string("visited_cob"))) this->deleteProperty(string("visited_cob"));
	 			if(this->isTop(SimplexPointer(cp))==false)
	 			{
	 				/* We start our visit from 'cp' and from all top simplices incident at 'cp' */
	 				rsp=this->addLocalProperty(cp.type(),string("refSimplex"),false);
	 				asp=this->addGlobalProperty(string("assigned"),false);
	 				vcp=this->addGlobalProperty(string("visited_cob"),false);
	 				rsp->set(SimplexPointer(cp),true);
					n=this->simplex(cp).getCoboundarySize();
					for(unsigned int i=0;i<n;i++)
					{
						/* Now, we start from an element in its coboundary! */
						q.push(SimplexPointer(this->simplex(cp).cc(i)));
						while(!q.empty())
						{
							/* Now, we extract the first element from q */
							ni=q.front();
							q.pop();
							if(asp->get(SimplexPointer(ni))==false)
							{
								/* The first simplex has not been visited: can we add it as new simplex in the star of 'cp' ? */
								asp->set(SimplexPointer(ni),true);
								if(vcp->get(SimplexPointer(ni))==false)
								{
									vcp->set(SimplexPointer(ni),true);
									ll->push_back(SimplexPointer(ni));
								}

								/* If we arrive here, we check if we can continue our analysis. */
								if(ni.type()>cp.type()+1)
								{
									/* Now, we consider the boundary of 'ni' (it is a top simplex incident in cp) */
									this->boundary(SimplexPointer(ni),&bnd);
									for(list<SimplexPointer>::iterator it1=bnd.begin();it1!=bnd.end();it1++)
									{
										/* Now, we check if it is possible to add the current simplex */
										if(vcp->get(SimplexPointer(*it1))==false)
										{
											vcp->set(SimplexPointer(*it1),true);
											if(this->intersect(SimplexPointer(*it1),cp.type(),rsp)==true) if(it1->type()!=cp.type()) ll->push_back(SimplexPointer(*it1));
										}
										
										/* Now, we check if we can proceed on an other top simplex! */
										if(it1->type()==ni.type()-1)
										{
											if(this->intersect(SimplexPointer(*it1),cp.type(),rsp)==true)
											{
												/* Now, we visit the partial coboundary of 'it1' */
												z=this->simplex(SimplexPointer(*it1)).getCoboundarySize();
												for(unsigned int m=0;m<z;m++) { q.push(SimplexPointer(this->simplex(SimplexPointer(*it1)).cc(m))); }
											}
										}
									}
								}
							}
						}
					}

	 				/* If we arrive here, we can finalize this member function! */
	 				ll->sort(compare_simplices_pointers);
	 				this->deleteProperty(string("refSimplex"));
	 				this->deleteProperty(string("assigned"));
	 				this->deleteProperty(string("visited_cob"));
	 			}
	 		}
	 		
	 		/// This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex directly encoded in the current data structure: this member
			 * function filters boundary simplices against their dimension.<p>The reference simplex and the star simplices are required to be <i>valid</i>, i.e. directly stored in the current
			 * data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They are identified by instances of the mangrove_tds::SimplexPointer
			 * class and thus by 	their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>The
			 * <i>Simplified Incidence Graph</i> data structure encodes all simplices and it is a <i>global</i> data structure.<p>If we cannot complete this operation, then this member
			 * function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.<p><b>IMPORTANT:</b> when you try to apply this member function, you should be careful about the use of auxiliary information (i.e. subclasses of the
			 * mangrove_tds::PropertyBase class) associated to the current simplicial complex. In other words, in this member function, we use a global property (i.e. an instance of the
			 * mangrove_tds::GlobalPropertyHandle class) with boolean values, called <i>visited_cob</i>, and a local property (i.e. an instance of the mangrove_tds::LocalPropertyHandle class)
			 * for all the d-simplices, called <i>refSimplex</i>. We remove such properties, if they already belong to the current simplicial complex. In any case, we remove them at the end
			 * of this member function.
 			 * \param cp the reference simplex directly encoded in the current data structure
 			 * \param k the dimension of the required simplices belonging to the input simplex coboundary
			 * \param ll a list of the required simplices belonging to the input simplex coboundary
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::GhostSimplex,
			 * mangrove_tds::GhostSimplexPointer, mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::LocalPropertyHandle,
			 * mangrove_tds::BaseSimplicialComplex::addGlobalProperty(), mangrove_tds::BaseSimplicialComplex::addLocalProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty()
			 */
			inline void coboundaryk(const SimplexPointer& cp,SIMPLEX_TYPE k,list<SimplexPointer> *ll)
	 		{
	 			LocalPropertyHandle<bool> *rsp;
	 			GlobalPropertyHandle<bool> *asp;
	 			GlobalPropertyHandle<bool> *vcp;
				unsigned int n,z;
				queue<SimplexPointer> q;
				list<SimplexPointer> bnd;
				SimplexPointer ni;

	 			/* First, we check if the input parameters are ok! */
	 			assert(this->isValid(cp));
	 			assert(ll!=NULL);
	 			ll->clear();
	 			if(this->hasProperty(string("refSimplex"))) this->deleteProperty(string("refSimplex"));
	 			if(this->hasProperty(string("assigned"))) this->deleteProperty(string("assigned"));
	 			if(this->hasProperty(string("visited_cob"))) this->deleteProperty(string("visited_cob"));
	 			if(this->isTop(SimplexPointer(cp))==false)
	 			{
	 				/* We start our visit from 'cp' and from all top simplices incident at 'cp' */
	 				rsp=this->addLocalProperty(cp.type(),string("refSimplex"),false);
	 				asp=this->addGlobalProperty(string("assigned"),false);
	 				vcp=this->addGlobalProperty(string("visited_cob"),false);
	 				rsp->set(SimplexPointer(cp),true);
					n=this->simplex(cp).getCoboundarySize();
					for(unsigned int i=0;i<n;i++)
					{
						/* Now, we start from an element in its coboundary! */
						q.push(SimplexPointer(this->simplex(cp).cc(i)));
						while(!q.empty())
						{
							/* Now, we extract the first element from q */
							ni=q.front();
							q.pop();
							if(asp->get(SimplexPointer(ni))==false)
							{
								/* The first simplex has not been visited: can we add it as new simplex in the star of 'cp' ? */
								asp->set(SimplexPointer(ni),true);
								if(vcp->get(SimplexPointer(ni))==false)
								{
									vcp->set(SimplexPointer(ni),true);
									if(ni.type()==k) ll->push_back(SimplexPointer(ni));
								}

								/* If we arrive here, we check if we can continue our analysis. */
								if(ni.type()>cp.type()+1)
								{
									/* Now, we consider the boundary of 'ni' (it is a top simplex incident in cp) */
									this->boundary(SimplexPointer(ni),&bnd);
									for(list<SimplexPointer>::iterator it1=bnd.begin();it1!=bnd.end();it1++)
									{
										/* Now, we check if it is possible to add the current simplex */
										if(vcp->get(SimplexPointer(*it1))==false)
										{
											vcp->set(SimplexPointer(*it1),true);
											if(this->intersect(SimplexPointer(*it1),cp.type(),rsp)==true) if(it1->type()==k) ll->push_back(SimplexPointer(*it1));
										}
										
										/* Now, we check if we can proceed on an other top simplex! */
										if(it1->type()==ni.type()-1)
										{
											if(this->intersect(SimplexPointer(*it1),cp.type(),rsp)==true)
											{
												/* Now, we visit the partial coboundary of 'it1' */
												z=this->simplex(SimplexPointer(*it1)).getCoboundarySize();
												for(unsigned int m=0;m<z;m++) { q.push(SimplexPointer(this->simplex(SimplexPointer(*it1)).cc(m))); }
											}
										}
									}
								}
							}
						}
					}

	 				/* If we arrive here, we can finalize this member function! */
	 				ll->sort(compare_simplices_pointers);
	 				this->deleteProperty(string("refSimplex"));
	 				this->deleteProperty(string("assigned"));
	 				this->deleteProperty(string("visited_cob"));
	 			}
	 		}
			
	 		/// This member function identifies all simplices belonging to the coboundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the coboundary of a simplex not directly encoded in the current data structure.<p>The reference simplex and the star
	 		 * simplices can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this
	 		 * member function only with a <i>local</i> data structure.<p>The <i>Simplified Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure: then this
	 		 * member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
	 		 * \param ll a list of the required simplices belonging to the input simplex coboundary
			 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, 
			 * mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::BaseSimplicialComplex::encodeAllSimplices()
			 */
	 		inline void coboundary(const GhostSimplexPointer& cp, list<GhostSimplexPointer> *ll)
	 		{
	 			/* Dummy statements for avoiding compiler warnings! */
	 			idle(cp);
	 			idle(ll);
	 			assert(this->encodeAllSimplices()==false);
	 		}
	 		
	 		/// This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices of a certain dimension belonging to the coboundary of a simplex not directly encoded in the current data structure: this member
	 		 * function filters coboundary simplices against their dimension.<p>The reference simplex and the coboundary simplices can also not directly encoded in the current data structure
			 * and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>The
	 		 * <i>Simplified Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure: consequently, this member function will fail if and only if the
	 		 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
			 * \param k the dimension of the required simplices belonging to the input simplex coboundary
			 * \param ll a list of the required simplices belonging to the input simplex coboundary
	 		 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::BaseSimplicialComplex::isValid()
			 */
	 		inline void coboundaryk(const GhostSimplexPointer& cp,SIMPLEX_TYPE k,list<GhostSimplexPointer> *ll)
	 		{
	 			/* Dummy statetement for avoiding compiler warnings! */
	 			idle(cp);
	 			idle(ll);
	 			idle(k);
	 			assert(this->encodeAllSimplices()==false);
	 		}
	 		
	 		/// This member function identifies all simplices adjacent to a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices adjacent to a simplex directly encoded in the current data structure: two 0-simplices are <i>adjacent</i> if they are connected by
			 * a common edge, while two k-simplices (with k>0) are <i>adjacent</i> if they share a simplex of dimension k-1.<p>The reference simplex and the adjacent simplices are required to
	 		 * be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. They are
			 * identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member function only with a
			 * <i>global</i> data structure.<p>The <i>Simplified Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure.<p>If we cannot complete this operation,
			 * then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed
			 * assert.<p><b>IMPORTANT:</b> when you try to apply this member function, you should be careful about the use of auxiliary information (i.e. subclasses of the
			 * mangrove_tds::PropertyBase class) associated to the current simplicial complex. In other words, in this member function, we use a local property (i.e. an instance of the
			 * mangrove_tds::LocalPropertyHandle class) for all the d-simplices, called <i>visited_adj</i>, where d is the dimension of the reference simplex. We remove such property, if it
			 * already belongs to the current simplicial complex. In any case, we remove it at the end of this member function.
	 		 * \param cp the reference simplex directly encoded in the current data structure
			 * \param ll a list of the required simplices belonging to the input simplex adjacency
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::PropertyBase,
			 * mangrove_tds::LocalPropertyHandle, mangrove_tds::BaseSimplicialComplex::addLocalProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty()
			 */
	 		inline void adjacency(const SimplexPointer &cp,list<SimplexPointer> *ll)
	 		{
	 			LocalPropertyHandle<bool> *vap;
	 			list<SimplexPointer> aux,aux1;
				list<SimplexPointer>::iterator aux_it,aux1_it;
				
	 			/* First, we must check the input parameters! */
	 			assert( this->isValid(cp));
	 			assert(ll!=NULL);
	 			ll->clear();
	 			if(this->hasProperty(string("visited_adj"))) this->deleteProperty(string("visited_adj"));
	 			vap=this->addLocalProperty(cp.type(),string("visited_adj"),false);
	 			vap->set(SimplexPointer(cp),true);
	 			if(cp.type()==0)
	 			{
	 				/* We must find all the 1-simplices incident in 'cp' and then we extract their extremes */
	 				this->coboundaryk(cp,1,&aux);
	 				for(aux_it=aux.begin();aux_it!=aux.end();aux_it++)
	 				{
	 					/* Now, we analyze the two extremes for each edge incident in 'cp' */
	 					for(unsigned int k=0;k<2;k++)
	 					{
	 						if(vap->get(SimplexPointer(this->simplex(*aux_it).b(k)))==false)
	 						{
	 							ll->push_back(SimplexPointer(this->simplex(*aux_it).b(k)));
	 							vap->set(SimplexPointer(this->simplex(*aux_it).b(k)),true);
	 						}
	 					}
	 				}		
	 			}
	 			else
	 			{
	 				/* We must find all the k-simplices that share a (k-1)-simplex with 'cp' */
	 				for(unsigned int k=0;k<=cp.type();k++)
	 				{
	 					/* Now, we compute the coboundary of a simplex sharing a (k-1)-simplex with 'cp' */
						this->coboundaryk(SimplexPointer(this->simplex(cp).bc(k)),cp.type(),&aux1);
						for(aux1_it=aux1.begin();aux1_it!=aux1.end();aux1_it++)
						{
							if(vap->get(SimplexPointer(*aux1_it))==false)
							{
								ll->push_back(SimplexPointer(*aux1_it));
								vap->set(SimplexPointer(*aux1_it),true);
							}
						}
	 				}
	 			}
	 			
	 			/* If we arrive here, we can finalize this member function! */
	 			ll->sort(compare_simplices_pointers);
	 			this->deleteProperty(string("visited_adj"));
	 		}
	 		
	 		/// This member function identifies all simplices adjacent to a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices adjacent to a simplex not directly encoded in the current data structure: two 0-simplices are <i>adjacent</i> if they are
			 * connected by a common edge, while two k-simplices (with k>0) are <i>adjacent</i> if they share a simplex of dimension k-1.<p>The reference simplex and the adjacent simplices
			 * can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member
			 * function only with a <i>local</i> data structure.<p>The <i>Simplified Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure: this member function
			 * will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
			 * \param ll a list of the required simplices belonging to the input simplex adjacency and not directly encoded in the current data structure
	 		 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::BaseSimplicialComplex::encodeAllSimplices()
	 		 */
	 		inline void adjacency(const GhostSimplexPointer &cp,list<GhostSimplexPointer> *ll)
	 		{
	 			/* Dummy statetement for avoiding compiler warnings! */
	 			idle(cp);
	 			idle(ll);
	 			assert(this->encodeAllSimplices()==false);
	 		}
	 		
	 		/// This member function identifies all simplices belonging to the link of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the link of a simplex directly encoded in the current data structure: let <i>c</i> a simplex in the current simplicial
	 		 * complex, then the <i>link</i> of <i>c</i> is the set of all the subsimplices of the simplices in the star of <i>c</i> that are not incident in <i>c</i>.<p>The reference simplex
	 		 * and the required simplices must be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage
			 * collector mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member
			 * function only with a <i>global</i> data structure.<p>If we cannot complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is
			 * compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function we use a global property (i.e. an
			 * instance of the mangrove_tds::GlobalPropertyHandle class) with boolean values, called <i>visited</i>, and a local property (i.e. an instance of the
			 * mangrove_tds::LocalPropertyHandle class) for vertices with boolean values, called <i>refVertex</i>. We remove them, if they already belong to the current simplicial complex. In
			 * any case, we remove them at the end of this member function.
	 		 * \param cp the reference simplex directly encoded in the current data structure
	 		 * \param ll a list of the required simplices belonging to the input simplex link
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::SIMPLEX_TYPE, 
	 		 * mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::BaseSimplicialComplex::addGlobalProperty(), mangrove_tds::LocalPropertyHandle,
	 		 * mangrove_tds::BaseSimplicialComplex::addLocalProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty(), mangrove_tds::BaseSimplicialComplex::type(),
	 		 * mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::BaseSimplicialComplex::encodeAllSimplices()
	 		 */
	 		inline void link(const SimplexPointer& cp,list<SimplexPointer> *ll)
	 		{
	 			GlobalPropertyHandle<bool> *vp;
	 			LocalPropertyHandle<bool> *rvp;
	 			list<SimplexPointer> cob,verts_cp,bnd;
	 			list<SimplexPointer>::iterator it,cob_it,bnd_it;

	 			/* First, we check the input parameters and then we can start our research! */
	 			assert(ll!=NULL);
	 			ll->clear();
	 			if(this->hasProperty(string("visited"))) this->deleteProperty(string("visited"));
	 			if(this->hasProperty(string("refVertex"))) this->deleteProperty(string("refVertex"));
	 			if(this->isTop(SimplexPointer(cp))==true) return;
	 			vp=this->addGlobalProperty( string("visited"),false);
	 			rvp=this->addLocalProperty(0,string("refVertex"),false);
	 			this->star(SimplexPointer(cp),&cob);
	 			if(cp.type()==0) verts_cp.push_back(SimplexPointer(cp));
	 			else this->boundaryk(SimplexPointer(cp),0,&verts_cp);
	 			for(it=verts_cp.begin();it!=verts_cp.end();it++) rvp->set(SimplexPointer(*it),true);
	 			for(cob_it=cob.begin();cob_it!=cob.end();cob_it++)
	 			{
	 				/* Now, we consider the current simplex incident in 'cp' and then we can extract its boundary (expressed as vertices) */
	 				if(vp->get(SimplexPointer(*cob_it))==false)
	 				{
	 					/* Now, we visit the current 'cob_it' simplex */
	 					vp->set(SimplexPointer(*cob_it),true);
	 					this->boundary(SimplexPointer(*cob_it),&bnd);
	 					for(bnd_it=bnd.begin();bnd_it!=bnd.end();bnd_it++)
	 					{
	 						/* Now, we check if we must analyze 'bnd_it' */
	 						if(vp->get(SimplexPointer(*bnd_it))==false)
	 						{
	 							/* Now, we visit the current 'cob_it' simplex */
	 							vp->set(SimplexPointer(*bnd_it),true);
	 							if(this->intersect(SimplexPointer(*bnd_it),rvp)==false) { ll->push_back(SimplexPointer(*bnd_it)); }
	 						}
	 					}
	 				}
	 			}

	 			/* If we arrive here, we can finalize this member function! */
	 			this->deleteProperty(string("visited"));
	 			this->deleteProperty(string("refVertex"));
	 			ll->sort(compare_simplices_pointers);
	 		}

			/// This member function identifies all simplices belonging to the link of a simplex not directly encoded in the current data structure.
	 		/**
	 		 * This member function identifies all simplices belonging to the link of a simplex not directly encoded in the current data structure: let <i>c</i> a simplex in the current
			 * simplicial complex, then the <i>link</i> of <i>c</i> is the set of all the subsimplices of the simplices in the star of <i>c</i> that are not incident in <i>c</i>.<p>The
			 * reference simplex and the required simplices can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer
			 * class. Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>The <i>Simplified Incidence Graph</i> encodes all simplices and it is a
			 * <i>global</i> data structure: this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation
			 * will result in a failed assert.
	 		 * \param cp the reference simplex not directly encoded in the current data structure
	 		 * \param ll a list of the required simplices belonging to the input simplex link and not directly encoded in the current data structure
	 		 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::PropertyBase,
			 * mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::BaseSimplicialComplex::encodeAllSimplices()
	 		 */
	 		inline void link(const GhostSimplexPointer& cp,list<GhostSimplexPointer> *ll)
	 		{
	 			/* Dummy statetement for avoiding compiler warnings! */
	 			idle(cp);
	 			idle(ll);
	 			assert(this->encodeAllSimplices()==false);
	 		}
	 		
	 		/// This member function returns the number of components in the link of a simplex directly encoded in the current data structure.
	 		/**
	 		 * This member function returns the number of components in the link of a simplex directly encoded in the current data structure: let <i>c</i> a simplex in the current simplicial
	 		 * complex, then the <i>link</i> of <i>c</i> is the set of all the subsimplices of the simplices in the star of <i>c</i> that are not incident in <i>c</i>.<p>The reference simplex
	 		 * and the required simplices must be <i>valid</i>, i.e. directly stored in the current data structure and related to locations not marked as <i>deleted</i> by the garbage 
			 * collector mechanism. They are identified by instances of the mangrove_tds::SimplexPointer class and thus by their types and identifiers. Consequently, we can invoke this member
			 * function only with a <i>global</i> data structure.<p>The <i>Simplified Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure.<p>If we cannot
			 * complete this operation, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation
			 * will result in a failed assert.<p><b>IMPORTANT:</b> in this member function we use a global property (i.e. an instance of the mangrove_tds::GlobalPropertyHandle class) with
			 * boolean values, called <i>visited</i>, and a local property (i.e. an instance of the mangrove_tds::LocalPropertyHandle class) for vertices with boolean values, called
			 * <i>refVertex</i>. We remove them, if they already belong to the current simplicial complex. In any case, we remove them at the end of this member function.
	 		 * \param cc the reference simplex directly encoded in the current data structure
	 		 * \param lk a list of the required simplices belonging to the input simplex link
	 		 * \return the number of components in the link of a simplex directly encoded in the current data structure
	 		 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::SIMPLEX_TYPE,
	 		 * mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::BaseSimplicialComplex::addGlobalProperty(), mangrove_tds::LocalPropertyHandle,
	 		 * mangrove_tds::BaseSimplicialComplex::addLocalProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty(), mangrove_tds::BaseSimplicialComplex::type(),
	 		 * mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::BaseSimplicialComplex::encodeAllSimplices()
	 		 */
	 		inline unsigned int getLinkComponentsNumber(const SimplexPointer &cc,list<SimplexPointer> *lk)
			{
				/* First, we compute the link of the required component */
				this->link(SimplexPointer(cc),lk);
				return this->simplex(SimplexPointer(cc)).getCoboundarySize();
			}
			
			/// This member function returns the number of components in the link of a simplex not directly encoded in the current data structure.
			/**
			 * This member function returns the number of components in the link of a simplex not directly encoded in the current data structure: let <i>c</i> a simplex in the current
			 * simplicial complex, then the <i>link</i> of <i>c</i> is the set of all the subsimplices of the simplices in the star of <i>c</i> that are not incident in <i>c</i>.<p>The
			 * reference simplex and the required simplices can also be not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer
			 * class. Consequently, we can invoke this member function only with a <i>local</i> data structure.<p>The <i>Simplified Incidence Graph</i> encodes all simplices and it is a
			 * <i>global</i> data structure: this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation
			 * will result in a failed assert.
	 		 * \param cc the reference simplex not directly encoded in the current data structure
	 		 * \param lk a list of the required simplices belonging to the input simplex link
	 		 * \return the number of components in the link of a simplex not directly encoded in the current data structure
	 		 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
	 		 * mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::BaseSimplicialComplex::encodeAllSimplices()
			 */
			inline unsigned int getLinkComponentsNumber(const GhostSimplexPointer &cc,list<GhostSimplexPointer> *lk)
			{
				/* Dummy statements for avoiding compiler warnings */
				idle(cc);
				idle(lk);
				assert(this->encodeAllSimplices()==false);
				return 0;
			}
			
			/// This member function checks if a simplex directly encoded in the current data structure is manifold.
			/**
			 * This member function checks if a simplex directly encoded in the current data structure is manifold: we say that a simplex is <i>manifold</i> if and only if its neighborhood is
			 * homeomorphic to a connected component of a sphere, otherwise it is called <i>non-manifold</i>.<p>The reference simplex must be <i>valid</i>, i.e. directly stored in the current 
			 * data structure and related to locations not marked as <i>deleted</i> by the garbage collector mechanism. It is identified by instances of the mangrove_tds::SimplexPointer class
			 * and thus by their types and identifiers. Consequently, we can invoke this member function only with a <i>global</i> data structure.<p>The <i>Simplified Incidence Graph</i>
			 * encodes all simplices and it is a <i>global</i> data structure.<p>If we cannot complete this operation,then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> in this member function we
			 * use a global property (i.e. an instance of the mangrove_tds::GlobalPropertyHandle class) with boolean values, called <i>visited</i>, and a local property (i.e. an instance of
			 * the mangrove_tds::LocalPropertyHandle class) for vertices with boolean values, called <i>refVertex</i>. We remove them, if they already belong to the current simplicial
			 * complex. In any case, we remove them at the end of this member function.
			 * \param cp the reference simplex directly encoded in the current data structure
			 * \return <ul><li><i>true</i>, if the required simplex is manifold</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::SIMPLEX_TYPE,
	 		 * mangrove_tds::PropertyBase, mangrove_tds::GlobalPropertyHandle, mangrove_tds::BaseSimplicialComplex::addGlobalProperty(), mangrove_tds::LocalPropertyHandle,
	 		 * mangrove_tds::BaseSimplicialComplex::addLocalProperty(), mangrove_tds::BaseSimplicialComplex::deleteProperty(), mangrove_tds::BaseSimplicialComplex::type(),
	 		 * mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::BaseSimplicialComplex::encodeAllSimplices()
			 */
			inline bool isManifold(const SimplexPointer& cp)
	 		{
	 			unsigned long n;
	 			list<SimplexPointer> l;
	 			list<SimplexPointer>::iterator it;

	 			/* First, we check if all is ok */
				assert(this->type()<=3);
				assert(this->isValid(cp));
				if(this->isTop(SimplexPointer(cp))) return true;
				switch(cp.type())
				{
					case 1:
					
						/* We have an edge, thus it is manifold if and only if its link is composed by two vertices! */
						n=this->simplex(SimplexPointer(cp)).getCoboundarySize();
						if(n>2) return false;
						else if(n<2) return true;
						else if(this->type()==2) return true;
						else
						{
							/* Now, we must check if its link is composed only by 2 vertices! */
							this->link( SimplexPointer(cp),&l);							
							if( (l.size()==2) && (l.front().type()==0) && (l.back().type()==0)) { return true; }
							else { return false; }
						}
					
					case 0:

						/* We have a vertex, thus we must analyze its link! */
						n = this->simplex(SimplexPointer(cp)).getCoboundarySize();
						if(n>2) { return false; }
						else if(n==2)
						{
							/* If the two connected components are 1-dimensional, then this vertex is manifold! */
							if(this->simplex(SimplexPointer(cp)).cc(0).type()!=1) return false;
							if(this->simplex(SimplexPointer(cp)).cc(1).type()!=1) return false;
							return true;
						}
						else if	(n==1)
						{
							/* Now, we must check if all the incident edges are manifold! */
							if(this->simplex(SimplexPointer(cp)).cc(0).type()==1) return true;
							else
							{
						 		this->coboundaryk(SimplexPointer(cp),1,&l);
						 		for(it=l.begin();it!=l.end();it++) { if(this->isManifold(SimplexPointer(*it))==false) return false; }
								return true;
							}
						}
						else { return true; }
		
					default:
					
						/* In the default case, we have a manifold simplex! */
					    return true;
				};
	 		}
	 		
	 		/// This member function checks if a simplex not directly encoded in the current data structure is manifold.
			/**
			 * This member function checks if a simplex not directly encoded in the current data structure is manifold: we say that a simplex is <i>manifold</i> if and only if its
			 * neighborhood is homeomorphic to a connected component of a sphere, otherwise it is called <i>non-manifold</i>.<p>The reference simplex and the required simplices can also be
			 * not directly encoded in the current data structure and they are identified by the mangrove_tds::GhostSimplexPointer class. Consequently, we can invoke this member function only
			 * with a <i>local</i> data structure.<p>The <i>Simplified Incidence Graph</i> encodes all simplices and it is a <i>global</i> data structure: then this member function will fail
			 * if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param cp the reference simplex not directly encoded in the current data structure
			 * \return <ul><li><i>true</i>, if the required simplex is manifold</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::idle(), mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex,
			 * mangrove_tds::PropertyBase, mangrove_tds::BaseSimplicialComplex::isValid(), mangrove_tds::BaseSimplicialComplex::encodeAllSimplices()
			 */
			inline bool isManifold(const GhostSimplexPointer& cp)
			{
				/* Dummy statements for avoiding warnings! */
				idle(cp);
				assert(this->encodeAllSimplices()==false);
				return false;
			}
			
			protected:
			
			/// This member function creates a new instance of this class.
 			/**
 			 * This member function creates a new instance of this class.<p><b>IMPORTANT:</b> you should not invoke this member function. Instead, you should invoke the builder with a list
 			 * of input parameters.
 			 */
			inline SIG() : BaseSimplicialComplex<SIG>() { ; }
		};
	}

#endif

