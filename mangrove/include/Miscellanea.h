/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library                                     
 *                                                                           				    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * October 2011 (Revised on May 2012)
 *                                                       
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * Miscellanea.h - Some auxiliary and useful components
 ***********************************************************************************************/
 
/* Should we include this header file? */
#ifndef MISCELLANEA_H

	#define MISCELLANEA_H
	
	#include "Simplex.h"
	#include "Point.h"
	#include <cstdlib>
	#include <QString>
	#include <vector>
	#include <iostream>
	#include <set>
	#include <list>
	#include <assert.h>
	#include <algorithm>
	#include <string>
	#include <deque>
	#include <map>
	using namespace std;
	using namespace mangrove_tds;
	
	/// Some auxiliary and useful components.
	/**
	 * The classes and the functions defined in this file describe some auxiliary components, not linked to a particular topic or aspect, that can be very useful in the <i>Mangrove TDS Library</i>.
	 * The most part of them is implemented through templated functions and classes.
	 * \file Miscellanea.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	
	/* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		/// This function toggles the boolean values of a list of values.
		/**
		 * This function toggles the boolean values belonging to a source list of values: new values are stored in a destination list of boolean values.
		 * \param src the source list of boolean values
		 * \param dst the destination list of boolean values
		 */
		void toggle(deque<bool> &src,deque<bool> &dst);

		/// This operator writes a representation of a templated vector on an output stream.
		/**
		 * This operator writes a representation of a templated vector on an output stream: the templated class T is required to support the writing operator.
		 * \param os the output stream where we must write the input templated vector
		 * \param v the input templated vector to be written
		 * \return the output stream after having written a representation of the input templated vector
		 */
		template<class T> ostream& operator<<(ostream& os,vector<T> v)
		{
			/* We write the input vector! */
			for(unsigned int k=0;k<v.size();k++) { os<<v[k]<<" "; }
			os<<endl;
			os.flush();
			return os;
		}
		
		/// This operator rebuilds a templated vector by reading it from an input stream.
		/**
		 * This operator rebuilds a templated vector by reading it from an input stream: the templated class T is required to support the reading operator.<p><b>IMPORTANT:</b> the number
		 * of templated values to be read is determined by the size of the input vector.
		 * \param in the input stream from which we must read the templated values
		 * \param v the templated vector to be rebuilt
		 * \return the input stream after having read a representation of the input templated vector
		 */
		template<class T> istream& operator>>(istream& in,vector<T> &v)
		{
			unsigned int l;
			T el;
			
			/* Now, we must read some templated values */
			l = v.size();
			v.clear();
			for(unsigned int k=0;k<l;k++)
			{
				in>>el;
				v.push_back(el);
			}
			
			/* If we arrive here, we have finished! */
			return in;
		}
		
		/// This operator writes a representation of a templated list on an output stream.
		/**
		 * This operator writes a representation of a templated list on an output stream: the templated class T is required to support the writing operator.
		 * \param os the output stream where we must write the input templated list
		 * \param ll the input templated list to be written
		 * \return the output stream after having written a representation of the input templated list
		 */
		template <class T> ostream& operator<<(ostream& os,list<T> ll)
		{
			/* Fakes definitions */
			typedef T value_type;
			typedef list<value_type> list_type;
			typedef typename list_type::iterator list_iterator_type;
			list_iterator_type it;
        	
			/* Now, we must write the input list! */
			for( it = ll.begin(); it!=ll.end(); it++) { os<<(*it)<<" "; }
			os<<endl;
			os.flush();
			return os;
		}
		
		/// This operator rebuilds a templated list by reading it from an input stream.
		/**
		 * This operator rebuilds a templated list by reading it from an input stream: the templated class T is required to support the reading operator.<p><b>IMPORTANT:</b> the number of
		 * templated values to be read is determined by the size of the input list.
		 * \param in the input stream from which we must read the templated values
		 * \param ll the templated list to be rebuilt
		 * \return the input stream after having read a representation of the input templated list
		 */
		template <class T> istream& operator>>(istream& in,list<T> &ll)
		{
			unsigned n;
			T el;
        	
			/* First, we read the required templated values */
			n = ll.size();
			ll.clear();
			for(unsigned int k=0;k<n;k++)
			{
			      in>>el;
			      ll.push_back(el);
			}
        	
			/* If we arrive here, we have finished! */
			return in;
		}
		
		/// This operator compares two vectors of templated values.
		/**
		 * This operator compares two vectors of templated values, by implementing a <i>"strictly less than"</i> comparator for vectors of templated values: this component is based on the
		 * lexicographic order and gives priority to the vectors size.<p><b>IMPORTANT:</b> the template class T must support the comparison operator.
		 * \param first the first vector to be compared
		 * \param second the second vector to be compared
		 * \return <ul><li><i>true</i>, if the first vector is <i>"strictly less than"</i> the second one</li><li><i>false</i>, otherwise</li></ul>
		 */
		template <class T> bool operator<(vector<T>& first, vector<T>& second)
		{
			/* First, we give priority to the length size and then to the lexicographic ordering. */
		 	if(first.size() < second.size()) return true;
		 	if(first.size() > second.size()) return false;
		 	for(unsigned int i=0;i<first.size();i++)
		 	{
		 		/* Now, we apply the lexicographic order. */
		 		if(first[i]<second[i]) return true;
		 		if(first[i]>second[i]) return false;
		 	}
		 		
		 	/* If we arrive here, the first vector is not 'strictly less than' the second one. */
		 	return false;
		}
		
		/// This operator writes a representation of a templated set on an output stream.
		/**
		 * This operator writes a representation of a templated set on an output stream: the templated class T is required to support the writing operator.
		 * \param os the output stream where we must write the input templated set
		 * \param s the input templated set to be written
		 * \return the output stream after having written a representation of the input templated set
		 */
		template<class T> ostream& operator<<(ostream& os,set<T> s)
		{
			/* Fake definitions */
			typedef T value_type;
			typedef set<value_type> set_type;
			typedef typename set_type::iterator set_iterator_type;
			set_iterator_type it;
        	
			/* Now, we must write the input set! */
			for( it = s.begin(); it!=s.end(); it++) { os<<(*it)<<" "; }
			os<<endl;
			os.flush();
			return os;
		}
		
		/// This operator rebuilds a templated set by reading it from an input stream.
		/**
		 * This operator rebuilds a templated set by reading it from an input stream: the templated class T is required to support a reading operator.<p><b>IMPORTANT:</b> the number of
		 * templated values to be read is determined by the size of the input set.
		 * \param s the templated set to be rebuilt
		 * \param in the input stream from which we must read the templated values
		 * \return the input stream after having read a representation of the input templated set
		 */
		template <class T> istream& operator>>(istream& in,set<T> &s)
		{
			unsigned int l;
			T el;
       
			/* First, we read the required templated values */
			l = s.size();
			s.clear();
			for(unsigned int k=0;k<l;k++)
			{
				in>>el;
				s.insert(el);
			}
        	
			/* If we arrive here, we have finished! */
			return in;
		}
		
		/// This function does nothing and it is useful for avoiding compiler warnings.
		/**
		 * This function does nothing and it is useful for avoiding compiler warnings, i.e. it returns the same input value.
		 * \param a the fake parameter
		 * \return the fake parameter
		 */
		template <class T> T& idle(T& a) { return a; }
		
		/// This function checks if a set and a vector of templated values contain the same values.
		/**
		 * This function checks if a set and a vector of templated values contain the same values: in other words, we check if the vector is a copy of the input set, i.e. if it contains all the 			 * values of the set in the same order.<p><b>IMPORTANT:</b> the template class T must support the comparison operator.
		 * \param a the set of templated values to be compared
		 * \param b the vector of templated values to be compared
		 * \return <ul><li><i>true</i>, if the input set and vector of templated values contain the same set of values</li><li><i>false</i>, otherwise</li></ul>
		 */
		template <class T> bool sameValues( set<T> &a, vector<T> &b)
		{
			/* Fake definitions */
			typedef T value_type;
			typedef set<value_type> set_type;
			typedef vector<value_type> vector_type;
			typedef typename set_type::iterator set_iterator_type;
			typedef typename vector_type::iterator vector_iterator_type;
			set_iterator_type ita;
			vector_iterator_type itb;
        	
			/* Now, we check if 'a' and 'b' contain the same values! */
			if(a.size()!=b.size()) return false;
			itb = b.begin();
			for(ita=a.begin();ita!=a.end();ita++)
			{
				if((*ita)!=(*itb)) return false;
				itb++;
			}
			
			/* If we arrive here, we have the same simplices identifiers! */
			return true; 
		}
		
		/// This function computes a canonical projection of an ordered set of templated values.
		/**
		 * This function computes a canonical projection of an ordered set of templated values: it can be obtained by discarding a templated value (identified by its position) from the input
		 * set.<p>If the position to be discarded does not exist, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each
		 * forbidden operation will result in a failed assert.
		 * \param i the position of the value to be discarded
		 * \param s the input set of templated values
		 * \param c the required projection of the input set
		 */
		template <class T> void findProjection( unsigned int i, set<T> &s, vector<T> &c)
		{
			/* Fake definitions */
			typedef T value_type;
			typedef set<value_type> set_type;
			typedef typename set_type::iterator set_iterator_type;
			set_iterator_type it;
        	
			/* First, we check if the input parameters are valid! */
			assert(s.size()!=0);
			assert(i<s.size());
			c.clear();
			it = s.begin();
			for(unsigned int k=0;k<s.size();k++)
			{
				/* Do we insert it? */
				if(k!=i) c.push_back(*it);
				it++;
			}
		}
		
		/// This function computes a canonical projection of a vector of templated values.
		/**
		 * This function computes a canonical projection of a vector of templated values: it can be obtained by discarding a templated value (identified by its position) from the input vector.<p>
		 * If the position to be discarded does not exist, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each
		 * forbidden operation will result in a failed assert.
		 * \param i the position of the value to be discarded
		 * \param s the input vector of templated values
		 * \param c the required projection of the input vector
		 */
		template <class T> void findProjection( unsigned int i, vector<T> &s, vector<T> &c)
		{
			/* Fake definitions! */
		 	typedef T value_type;
		 	typedef vector<value_type> vector_type;
		 	typedef typename vector_type::iterator vector_iterator_type;	
		 	vector_iterator_type it;
		 	
		 	/* First, we check if the input parameters are valid! */
			assert(s.size()!=0);
			assert(i<s.size());
			c.clear();
			it = s.begin();
			for(unsigned int k=0;k<s.size();k++)
			{
				/* Do we insert it? */
				if(k!=i) c.push_back(*it);
				it++;
			}
		 }
		
		/// This function checks if a vector is a permutation of an other vector.
		/**
		 * This function checks if a vector is a permutation of an other vector, i.e. if it contains all the values in any order.
		 * \param a the first vector to be compared
		 * \param b the second vector to be compared
		 * \return <ul><li><i>true</i>, if the first vector is a permutation of the second vector</li><li><i>false</i>, otherwise</li></ul>
		 */
		template <class T> bool isPermutation(vector<T>& a,vector<T>& b)
		{
			/* Fake definitions! */
		 	typedef T value_type;
		 	typedef vector<value_type> vector_type;
		 	typedef typename vector_type::iterator vector_iterator_type;
		 	vector_iterator_type it;
		 	
		 	/* Now, we check if a is a permutation of b */
		 	if(a.size()!=b.size()) return false;
		 	if(a.size()==0) return true;
		 	for(it=b.begin();it!=b.end();it++) { if( find(a.begin(),a.end(),*it)==a.end()) return false; }
		 	return true;
		}
		
		/// A comparator for simplices in a simplicial complex.
		/**
		 * This class describes a comparator for simplices in a simplicial complex. For us, a simplex can be described as:<ul><li>a vector of simplices identifiers, i.e. values of
		 * mangrove_tds::SIMPLEX_ID type. In other words, these indices refers all the faces on its boundary.</li><li>instances of the mangrove_tds::SimplexPointer class, in particular when a
		 * simplex is directly encoded in a data structure.</li><li>instances of the mangrove_tds::GhostSimplexPointer class, in particular when a simplex is not directly encoded in a data
		 * structure.</li><li>instances of the mangrove_tds::GhostSimplex class, in particular when a simplex is not directly encoded in a data structure.</li></ul>This component is based on
		 * the <i>"strictly less than"</i> comparisons by using the lexicographic order and by giving priority to vectors size (if the input parameters are vectors or instances of the
		 * mangrove_tds::GhostSimplex class) or to the simplex dimension (if the input parameters are instances of the mangrove_tds::SimplexPointer class) or to the top simplex dimension (if
		 * the input parameters are instances of the mangrove_tds::GhostSimplexPointer class).
		 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointer, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
		 */
		class CompareSimplex
		{
			public:
			
			/// This operator compares two simplices.
			/**
			 * This operator compares two simplices, described by vector of simplices identifiers, i.e. values of the mangrove_tds::SIMPLEX_ID type: they are the identifiers of all the faces
			 * on its boundary.<p>This operator implements a <i>"strictly less than"</i> comparator for simplices identifiers, by giving priority to the vectors size.
			 * \param first the first vector to be compared
		 	 * \param second the second vector to be compared
		 	 * \return <ul><li><i>true</i>, if the first vector is <i>"strictly less than"</i> the second one</li><li><i>false</i>, otherwise</li></ul>
		 	 * \see mangrove_tds::SIMPLEX_ID
		 	 */
			bool operator() (const vector<SIMPLEX_ID> &first, const vector<SIMPLEX_ID> &second) const;
			
			/// This operator compares two simplices.
			/**
			 * This operator compares two simplices, referred through two instances of the mangrove_tds::SimplexPointer class.<p>This operator implements a <i>"strictly less than"</i>
			 * comparator for simplices pointers, by giving priority to the referred simplices dimension.
			 * \param a the first simplex pointer to be compared
		 	 * \param b the second simplex pointer to be compared
		 	 * \return <ul><li><i>true</i>, if the first simplex pointer is less than the second one</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::compare_simplices_pointers()
			 */
			bool operator()(const SimplexPointer& a,const SimplexPointer& b) const;
			
			/// This operator compares two simplices.
			/**
			 * This operator compares two ghost simplices, referred through two instances of the mangrove_tds::GhostSimplexPointer class.<p>This operator implements a
			 * <i>"strictly less than"</i> comparator for ghost simplices pointers, by giving priority to the referred top simplices dimension.
			 * \param a the first ghost simplex pointer to be compared
		 	 * \param b the second ghost simplex pointer to be compared
		 	 * \return <ul><li><i>true</i>, if the first ghost simplex pointer is less than the second one</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::compare_ghost_simplices_pointers()
			 */
			bool operator()(const GhostSimplexPointer& a, const GhostSimplexPointer& b) const;
			
			/// This operator compares two ghost simplices.
			/**
			 * This operator compares two ghost simplices, referred through two instances of the mangrove_tds::GhostSimplexPointer class.<p>This operator implements a
			 * <i>"strictly less than"</i> comparator for ghost simplices, expressed through their vertices.
			 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
			 */
			bool operator()(const GhostSimplex& a,const GhostSimplex& b) const;
		};
		
		/// A raw and auxiliary description of a face.
		/**
		 * This class provides a raw and auxiliary description of a face: in this context a face is described by its vertices. This particular description could be very useful when we must
		 * generate all the simplices during the construction of a simplicial complex in the mangrove_tds::IO::createComplex() member function.
		 * \see mangrove_tds::Simplex, mangrove_tds::IO::createComplex(), mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex
	     	 */
	    	class RawFace
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a new instance of this class, but it does not initialize it: you must apply member functions provided by this class for initializing internal
			 * fields.
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawFace::isTopSimplexChild(), mangrove_tds::RawFace::getVertices(), mangrove_tds::RawFace::getOriginalPosition(),
			 * mangrove_tds::RawFace::getUniqueIndex(), mangrove_tds::RawFace::getIndex(), mangrove_tds::RawFace::getReverseIndex(), mangrove_tds::RawFace::getBoundary()
			 */
			RawFace();
			
			/// This member function creates a new instance of this class.
			/**
			 * This member functions creates a new instance of this class as a deep copy of the input face.
			 * \param f the face to be copied (deep copy)
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawFace::isTopSimplexChild(), mangrove_tds::RawFace::getVertices(),
			 * mangrove_tds::RawFace::getOriginalPosition(), mangrove_tds::RawFace::getUniqueIndex(), mangrove_tds::RawFace::getIndex(), mangrove_tds::RawFace::getReverseIndex(),
			 * mangrove_tds::RawFace::getBoundary()
			 */
			RawFace(const RawFace& f);
        	
			/// This member function destroys an instance of this class.
			virtual ~RawFace();
			
			/// This member function checks if the current face is a direct subface of a top simplex.
			/**
			 * This member function checks if the current face is a direct subface of a top simplex: you can check if a simplex is top through the mangrove_tds::BaseSimplicialComplex::isTop()
			 * member function.
			 * \return <ul><li><i>true</i>, if the current face is a direct subface of a top face</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::BaseSimplicialComplex::isTop()
			 */
			bool& isTopSimplexChild();
			
			/// This member function returns a reference to the vertices belonging to the current face boundary.
			/**
			 * This member function returns a reference to the vertices belonging to the current face boundary. If you have a k-face, you can obtain all the (k-1)-faces on its boundary
			 * through the mangrove_tds::RawFace::getBoundary() member function.
			 * \return a reference to the vertices belonging to the current face boundary
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawFace::getBoundary()
			 */
			vector<SIMPLEX_ID>& getVertices();
			
			/// This member function returns a reference to the original position of the current face in the input list.
			/**
			 * This member function returns a reference to the original position of the current face in the input list: this member function is useful when you sort the input list of faces
			 * and you need to refer the original list of faces, as in the mangrove_tds::IO::createComplex() member function.
			 * \return a reference to the original position of the current face
			 * \see mangrove_tds::IO::createComplex()
			 */
			unsigned int& getOriginalPosition();
			
			/// This member function returns a reference to the index assigned to the current face, when it is visited and stored in a simplicial complex.
			/**
			 * This member function returns a reference to the index assigned to the current face, when it is visited and stored in a simplicial complex through the
			 * mangrove_tds::IO::createComplex().
			 * \return a reference to the index assigned to the current face, when it is visited and stored in a simplicial complex.
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::IO::createComplex()
			 */
			SIMPLEX_ID& getUniqueIndex();
			
			/// This member function returns a reference to the index of the parent face that contains the current face.
			/**
			 * This member function returns a reference to the index of the parent face that contains the current face: this property is useful for retrieving the total hierarchy of
			 * generated faces when you build a simplicial complex, as in the mangrove_tds::IO::createComplex(). You can retrieve the position of the current face in the boundary of the
			 * parent face through the mangrove_tds::RawFace::getReverseIndex() member function.
			 * \return a reference to the index of the parent face that contains the current face
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::IO::createComplex(), mangrove_tds::RawFace::getReverseIndex()
			 */
			SIMPLEX_ID& getIndex();
			
			/// This member function returns a reference to the position of the current face in the boundary of the parent face.
			/**
			 * This member function returns a reference to the position of the current face in the boundary of the parent face: this property is useful for retrieving the total hierarchy of
			 * generated faces when you build a simplicial complex, as in the mangrove_tds::IO::createComplex(). You can retrieve the index of the parent face through the
			 * mangrove_tds::RawFace::getIndex() member function.
			 * \return a reference to the position of the current face in the boundary of the parent face.
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::IO::createComplex(), mangrove_tds::RawFace::getIndex()
			 */
			SIMPLEX_ID& getReverseIndex();
			
			/// This member function returns a reference to the faces in the current face boundary.
			/**
			 * This member function returns a reference to the (k-1)-faces in the current k-face boundary. If you have a k-face, you can obtain all its vertices through the
			 * mangrove_tds::RawFace::getVertices() member function.
			 * \return a reference to the faces in the current face boundary
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::IO::createComplex(), mangrove_tds::RawFace::getVertices()
			 */
			vector<SIMPLEX_ID>& getBoundary();
			
			/// This member function returns a reference to the pointer used for accessing this face.
			/**
			 * This member function returns a reference to the pointer used for accessing this face, when it is visited and used in the simplicial complex.
			 * \return a reference to the pointer used for accessing this face
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex, mangrove_tds::Simplex
			 */
			SimplexPointer& getSharedFace();

			/// This operator writes a representation of the current face.
			/**
			 * This operator writes a representation of the current face, only for debugging purposes.
			 * \param os the output stream where we can write the face
			 * \param f the input face to be written
			 * \return the output stream after having written the input face
			 * \see mangrove_tds::SIMPLEX_ID
			 */
			inline friend ostream& operator<<(ostream& os,RawFace &f)
			{
				/* We write a representation of the input RawFace! */
				os<<endl;
				os<<"Vertices: "<<f.vertices;
				os<<"Boundary: "<<f.children;
				os<<"Original position: "<<f.pos<<endl;
				os<<"Index: "<<f.index<<endl;
				os<<"Reverse index: "<<f.reverse_index<<endl;
				os<<"Unique index: "<<f.unique_index<<endl;
				os<<"Direct child of a top simplex: "<<f.flag<<endl;
				os<<"Pointer: "<<f.sf<<endl;
				os.flush();
				return os;
			}
			
			protected:
			
			/// This boolean flag identifies if the current face is a direct subface of a top simplex.
			bool flag;
			
			/// The identifiers of the vertices belonging to the current face boundary.
			/**
			 * \see mangrove_tds::SIMPLEX_ID
			 */
			vector<SIMPLEX_ID> vertices;
			
			/// The original position of the current face.
			unsigned int pos;
			 
			/// The index assigned to the current face, when it is visited and stored in a simplicial complex.
			/**
			 * \see mangrove_tds::SIMPLEX_ID
			 */
			SIMPLEX_ID unique_index;
			 
			/// The index of the parent face that contains the current face.
			/**
			 * \see mangrove_tds::SIMPLEX_ID
			 */
			SIMPLEX_ID index;
			 
			/// The position of the current face in the boundary of the parent face.
			/**
			 * \see mangrove_tds::SIMPLEX_ID
			 */
			SIMPLEX_ID reverse_index;
			 
			/// The identifiers of the faces on the current face boundary.
			/**
			 * \see mangrove_tds::SIMPLEX_ID
			 */
			vector<SIMPLEX_ID> children;
			
			/// A pointer used for accessing this face, when it is visited and stored in a simplicial complex.
			/**
			 * \see mangrove_tds::SimplexPointer
			 */
			SimplexPointer sf;
		};
		
		/// This function compares two faces descriptions.
		/**
		 * This function compares two faces descriptions. This function implements a <i>"strictly less than"</i> comparator for two faces descriptions, by using the two faces vertices: this
		 * component is based on the lexicographic order and gives priority to the vectors size.
		 * \param rfa the first face to be compared
		 * \param rfb the second face to be compared
		 * \return <ul><li><i>true</i>, if the first face is lesser than the second</li><li><i>false</i>, otherwise</li></ul>
		 * \see mangrove_tds::RawFace, mangrove_tds::SIMPLEX_ID
		 */
		bool compare_faces( RawFace rfa, RawFace rfb);
		
		/// This function compares two faces descriptions.
		/**
		 * This function compares two faces descriptions. This function implements a <i>"strictly less than"</i> comparator for two faces descriptions, by using the two pointers assigned to
		 * each face, when they are used in the simplicial complex.
		 * \param rfa the first face to be compared
		 * \param rfb the second face to be compared
		 * \return <ul><li><i>true</i>, if the first face is lesser than the second</li><li><i>false</i>, otherwise</li></ul>
		 * \see mangrove_tds::RawFace, mangrove_tds::SimplexPointer
		 */
		bool compare_shared_faces(RawFace rfa,RawFace rfb);
		
		/// This function checks if two faces descriptions are the same.
		/**
		 * This function checks if two faces descriptions are the same, by using the two faces vertices.
		 * \param rfa the first face to be compared
		 * \param rfb the second face to be compared
		 * \return <ul><li><i>true</i>, if the input faces descriptions are the same</li><li><i>false</i>, otherwise</li></ul>
		 * \see mangrove_tds::RawFace, mangrove_tds::SIMPLEX_ID
		 */
		bool same_faces(RawFace rfa,RawFace rfb);
		
		/// This function checks if two faces descriptions are the same.
		/**
		 * This function checks if two faces descriptions are the same, by using the two pointers assigned to each face, when they are used in the simplicial complex.
		 * \param rfa the first face to be compared
		 * \param rfb the second face to be compared
		 * \return <ul><li><i>true</i>, if the input faces descriptions are the same</li><li><i>false</i>, otherwise</li></ul>
		 * \see mangrove_tds::RawFace, mangrove_tds::SimplexPointer
		 */
		bool same_shared_faces(RawFace rfa,RawFace rfb);
		
		/// An alias for a complex, considered as a set of mangrove_tds::SimplexPointer instances.
		/**
		 * \see mangrove_tds::SimplexPointer, mangrove_tds::CompareSimplex
		 */
		typedef set<SimplexPointer,CompareSimplex> COMPLEX;
		
		/// An alias for a complex formed by ghost simplices, considered as a set of mangrove_tds::GhostSimplexPointer instances.
		/**
		 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::CompareSimplex
		 */
		typedef set<GhostSimplexPointer,CompareSimplex> GHOST_COMPLEX;
		
		/// An alias for a complex directly expressed as ghost simplices, considered as a set of mangrove_tds::GhostSimplex instances.
		/**
		 * \see mangrove_tds::GhostSimplex, mangrove_tds::CompareSimplex
		 */
		typedef set<GhostSimplex,CompareSimplex> GS_COMPLEX;

		/// This operator writes a representation of a complex on an output stream.
		/**
		 * This operator writes a representation of a complex on an output stream: a complex is a set of mangrove_tds::SimplexPointer instances, described through the
		 * mangrove_tds::COMPLEX.
		 * \param os the output stream where we must write the input complex
		 * \param s the input complex to be written
		 * \return the output stream after having written a representation of the input complex
		 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::COMPLEX
		 */
		ostream& operator<<(ostream& os,COMPLEX &s);

		/// This operator writes a representation of a complex of ghost simplices on an output stream.
		/**
		 * This operator writes a representation of a complex of ghost simplices on an output stream: a complex of ghost simplices is a set of mangrove_tds::GhostSimplexPointer instances,
		 * described through the mangrove_tds::GHOST_COMPLEX.
		 * \param os the output stream where we must write the input complex of ghost simplices
		 * \param s the input complex of ghost simplices to be written
		 * \return the output stream after having written a representation of the input complex of ghost simplices
		 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::GHOST_COMPLEX
		 */
		ostream& operator<<(ostream& os,GHOST_COMPLEX &s);

		/// This operator writes a representation of a complex, directly expressed as ghost simplices on an output stream.
		/**
		 * This operator writes a representation of a complex, directly expressed as ghost simplices on an output stream: a complex of ghost simplices is a set of any
		 * mangrove_tds::GhostSimplex instances, described through the mangrove_tds::GS_COMPLEX.
		 * \param os the output stream where we must write the input complex of ghost simplices
		 * \param s the input complex of ghost simplices to be written
		 * \return the output stream after having written a representation of the input complex of ghost simplices
		 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GS_COMPLEX
		 */
		ostream& operator<<(ostream& os,GS_COMPLEX &s);
		
		/// This operator rebuilds a complex by reading it from an input stream.
		/**
		 * This operator rebuilds a complex by reading it from an input stream: a complex is a set of mangrove_tds::SimplexPointer instances, described through the mangrove_tds::COMPLEX.
		 * \param s the complex to be rebuilt
		 * \param in the input stream from which we must read a complex
		 * \return the input stream after having read a representation of the input complex
		 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::COMPLEX
		 */
		istream& operator>>(istream& in,COMPLEX &s);
		
		/// This operator rebuilds a complex of ghost simplices by reading it from an input stream.
		/**
		 * This operator rebuilds a complex of ghost simplices by reading it from an input stream: a complex of ghost simplices is a set of mangrove_tds::GhostSimplexPointer instances,
		 * described through the mangrove_tds::GHOST_COMPLEX.
		 * \param s the complex of ghost simplices to be rebuilt
		 * \param in the input stream from which we must read a complex of ghost simplices
		 * \return the input stream after having read a representation of the input complex of ghost simplices
		 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::GHOST_COMPLEX
		 */
		istream& operator>>(istream& in,GHOST_COMPLEX &s);
		
		/// This operator rebuilds a complex, directly expressed as ghost simplices on an output stream.
		/**
		 * This operator rebuilds a complex, directly expressed as ghost simplices on an output stream: a complex of ghost simplices is a set of any mangrove_tds::GhostSimplex instances,
		 * described through the mangrove_tds::GS_COMPLEX.
		 * \param s the complex of ghost simplices to be rebuilt
		 * \param in the input stream from which we must read a complex of ghost simplices
		 * \return the output stream after having written a representation of the input complex of ghost simplices
		 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::GS_COMPLEX
		 */
		istream& operator>>(istream& in,GS_COMPLEX &s);

		/// This function deallocates a vector of vectors of simplices pointers.
		/**
		 * This function deallocates a vector of vectors of simplices pointers, described by the mangrove_tds::SimplexPointer class. In this way, each location is deallocated, and we clear all
		 * the allocated space for the input vector.
		 * \param vv the vector of vectors of simplices pointers to be deallocated
		 * \see mangrove_tds::SimplexPointer
		 */
		void destroy( vector< vector<SimplexPointer> > &vv);
		
		/// This function deallocates a vector of vectors of ghost simplices pointers.
		/**
		 * This function deallocates a vector of vectors of ghost simplices pointers, described by the mangrove_tds::GhostSimplexPointer class. In this way, each location is deallocated. and we
		 * clear all the allocated space for the input vector.
		 * \param vv the vector of vectors of simplices pointers to be deallocated
		 * \see mangrove_tds::GhostSimplexPointer
		 */
		void destroy( vector< vector<GhostSimplexPointer> > &vv);
		
		/// This function deallocates a vector of complexes.
		/**
		 * This function deallocates a vector of complexes, described by the mangrove_tds::COMPLEX class. In this way, each location is deallocated and we clear all the allocated space for the
		 * input vector.
		 * \param sc the vector of complexes to be deallocated
		 * \see mangrove_tds::COMPLEX, mangrove_tds::SimplexPointer
		 */
		void destroy(vector< COMPLEX > &sc);
		
		/// This function deallocates a vector of complexes of ghost simplices.
		/**
		 * This function deallocates a vector of complexes of ghost simplices, described by the mangrove_tds::GHOST_COMPLEX class. In this way, each location is deallocated, and we clear all
		 * the allocated space for the input vector.
		 * \param sc the vector of complexes of ghost simplices to be deallocated
		 * \see mangrove_tds::GHOST_COMPLEX, mangrove_tds::GhostSimplexPointer
		 */
		void destroy(vector< GHOST_COMPLEX > &sc);
		
		/// This function checks if a complex and a vector of simplices pointers contain the same values.
		/**
		 * This function checks if a complex (i.e. a set of simplices pointers) and a vector of simplices pointers contain the same values: in other words, we check if the input vector contains
		 * all the values (i.e. instances of the mangrove_tds::SimplexPointer class) in the same order.
		 * \param a the complex to be compared
	     	 * \param b the vector of simplices pointers to be compared
		 * \return <ul><li><i>true</i>, if the input complex and the vector of simplices pointers contain the same values</li><li><i>false</i>, otherwise</li></ul>
		 * \see mangrove_tds::SimplexPointer, mangrove_tds::COMPLEX
		 */
		bool sameValues(COMPLEX &a, vector<SimplexPointer> &b);
		
		/// This function checks if a complex and a vector of ghost simplices pointers contain the same values.
		/**
		 * This function checks if a ghost complex (i.e. a set of ghost simplices pointers) and a vector of ghost simplices pointers contain the same values: in other words, we check if the input
		 * vector contains all the values (i.e. instances of the mangrove_tds::GhostSimplexPointer class) in the same order.
		 * \param a the ghost complex to be compared
	     	 * \param b the vector of ghost simplices pointers to be compared
		 * \return <ul><li><i>true</i>, if the input ghost complex and the vector of ghost simplices pointers contain the same values</li><li><i>false</i>, otherwise</li></ul>
		 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::GHOST_COMPLEX
		 */
		bool sameValues(GHOST_COMPLEX &a, vector<GhostSimplexPointer> &b);
		
		/// This function returns the dimension of a complex.
		/**
		 * This function returns the dimension of a complex, i.e. the maximum dimension of its simplices.
		 * \param s the input complex to be analyzed
		 * \return the dimension of the input complex
		 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::COMPLEX, mangrove_tds::SimplexPointer
		 */
		SIMPLEX_TYPE dimension(COMPLEX s);
		
		/// This function returns the dimension of ghost complex.
		/**
		 * This function returns the dimension of a ghost complex, i.e. the maximum dimension of ghost simplices.
		 * \param s the input ghost complex to be analyzed
		 * \return the dimension of the input ghost complex
		 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::GHOST_COMPLEX, mangrove_tds::GhostSimplexPointer
		 */
		SIMPLEX_TYPE dimension(GHOST_COMPLEX s);
		
		/// This function computes a canonical projection of a complex.
		/**
		 * This function computes a canonical projection of a complex, i.e. a set of simplices pointers described through mangrove_tds::COMPLEX: it can be obtained by discarding a simplex
		 * pointer (identified by its position) from the input complex, described as a set of mangrove_tds::SimplexPointer class instances.<p>If the position to be discarded does not exist, 
		 * \param i the position of the value to be discarded
		 * \param s the input complex, i.e. a set of simplices pointers
		 * \param c the required projection of the input complex
		 * \see mangrove_tds::SimplexPointer, mangrove_tds::COMPLEX
		 */
		void findProjection(unsigned int i,COMPLEX &s,vector<SimplexPointer> &c);
		
		/// This function computes a canonical projection of a ghost complex.
		/**
		 * This function computes a canonical projection of a ghost complex, i.e. a set of ghost simplices pointers described through mangrove_tds::GHOST_COMPLEX: it can be obtained by
		 * discarding a ghost simplex pointer (identified by its position) from the input ghost complex, described as a set of mangrove_tds::GhostSimplexPointer class instances.<p>If the
		 * position to be discarded does not exist, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
		 * operation will result in a failed assert.
		 * \param i the position of the value to be discarded
		 * \param s the input ghost complex, i.e. a set of ghost simplices pointers
		 * \param c the required projection of the input ghost complex
		 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::GHOST_COMPLEX
		 */
		void findProjection( unsigned int i,GHOST_COMPLEX &s,vector<GhostSimplexPointer> &c);

		/// This function filters the values contained in a list of simplices pointers against the values contained in a complex.
		/**
		 * This function filters the values contained in a list of simplices pointers against the values contained in a complex, i.e. a set of simplices pointers (described by the
		 * mangrove_tds::COMPLEX class): in other words, this function removes from the input list those values that do not belong to the reference complex.
		 * \param l the list of simplices pointers to be filtered
		 * \param s the reference complex
		 * \see mangrove_tds::SimplexPointer, mangrove_tds::COMPLEX
		 */
		void filterValues( list<SimplexPointer> &l, const COMPLEX &s);
		
		/// This function filters the values contained in a list of ghost simplices pointers against the values contained in a ghost complex.
		/**
		 * This function filters the values contained in a list of ghost simplices pointers against the values contained in a ghost complex, i.e. a set of ghost simplices pointers (described by
		 * the mangrove_tds::GHOST_COMPLEX class): in other words, this function removes from the input list those values that do not belong to the reference ghost complex.
		 * \param l the list of ghost simplices pointers to be filtered
		 * \param s the reference ghost complex
		 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::GHOST_COMPLEX
		 */
		void filterValues( list<GhostSimplexPointer> &l, const GHOST_COMPLEX &s);
		
		/// This function converts a vector of complexes to a vector of vectors of simplices pointers.
		/**
		 * This function converts a vector of complexes, described by the mangrove_tds::COMPLEX, into a vector of vectors of simplices pointers, described by the mangrove_tds::SimplexPointer
		 * class. The two data structures will contain the same elements.
		 * \param src the source vector of complexes to be copied
		 * \param dst the destination vector of vectors of simplices pointers
		 * \see mangrove_tds::COMPLEX, mangrove_tds::SimplexPointer
		 */
		void convert( vector< COMPLEX > &src, vector< vector<SimplexPointer> > &dst);
		
		/// This function converts a vector of ghost complexes to a vector of vectors of ghost simplices pointers.
		/**
		 * This function converts a vector of ghost complexes, described by the mangrove_tds::GHOST_COMPLEX, into a vector of vectors of ghost simplices pointers, described by the
		 * mangrove_tds::GhostSimplexPointer class. The two data structures will contain the same elements.
		 * \param src the source vector of ghost complexes to be copied
		 * \param dst the destination vector of vectors of ghost simplices pointers
		 * \see mangrove_tds::GHOST_COMPLEX, mangrove_tds::GhostSimplexPointer
		 */
		void convert( vector< GHOST_COMPLEX > &src, vector< vector<GhostSimplexPointer> > &dst);
		 
		/// This function converts a vector of simplices pointers to a vector of complexes.
		/**
		 * This function converts a vector of vectors of simplices pointers, described by the mangrove_tds::SimplexPointer class, into a vector of complexes, described by the
		 * mangrove_tds::COMPLEX. The two data structures will contain the same elements, unless the input vector contains some duplicated simplices.
		 * \param src the source vector of vectors of simplices pointers to be copied
		 * \param dst the destination vector of complexes
		 * \see mangrove_tds::COMPLEX, mangrove_tds::SimplexPointer
		 */
		void convert( vector< vector<SimplexPointer> > &src, vector< COMPLEX > &dst);
		
		/// This function converts a vector of ghost simplices pointers to a vector of ghost complexes.
		/**
		 * This function converts a vector of vectors of ghost simplices pointers, described by the mangrove_tds::GhostSimplexPointer class, into a vector of ghost complexes, described by the
		 * mangrove_tds::GHOST_COMPLEX. The two data structures will contain the same elements, unless the input vector contains some duplicated ghost simplices.
		 * \param src the source vector of vectors of ghost simplices pointers to be copied
		 * \param dst the destination vector of ghost complexes
		 * \see mangrove_tds::GHOST_COMPLEX, mangrove_tds::GhostSimplexPointer
		 */
		void convert( vector< vector<GhostSimplexPointer> > &src, vector< GHOST_COMPLEX > &dst);
		
		/// This function compares two simplices pointers.
		/**
		 * In this function two simplices pointers are compared and the first pointer is lesser than the second one by giving priority to the referred simplices dimension in accordance to the
		 * lexicographic order.
		 * \param a the first simplex pointer to be compared
		 * \param b the second simplex pointer to be compared
		 * \return <ul><li><i>true</i>, if the first simplex pointer is less than the second one</li><li><i>false</i>, otherwise</li></ul>
		 * \see mangrove_tds::SimplexPointer::id(), mangrove_tds::SimplexPointer::type(), mangrove_tds::compare_ghost_simplices_pointers(), mangrove_tds::CompareSimplex
		 */
		bool compare_simplices_pointers(SimplexPointer &a,SimplexPointer& b);
		
		/// This function checks if a complex contains a simplex.
		/**
		 * This function checks if a complex contains a simplex in accordance to the lexicographic order.
		 * \param c the set of simplices to be analyzed
		 * \param sp the simplex pointer to be found
		 * \return <ul><li><i>true</i>, if the input simplex belongs to the input complex</li><li><i>false</i>, otherwise</li></ul>
		 * \see mangrove_tds::COMPLEX, mangrove_tds::SimplexPointer
		 */
		bool COMPLEX_contains(COMPLEX &c, const SimplexPointer &sp);

		/// This function compares two ghost simplices pointers.
		/**
		 * In this function, two ghost simplices pointers are compared and the first ghost pointer is lesser than the second one by giving priority to to the dimension of the referred top
		 * simplex, in accordance with the lexicographic order.
		 * \param a the first ghost simplex pointer to be compared
		 * \param b the second ghost simplex pointer to be compared
		 * \return <ul><li><i>true</i>, if the first ghost simplex pointer is less than the second one</li><li><i>false</i>, otherwise</li></ul>
		 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::compare_simplices_pointers(), mangrove_tds::CompareSimplex
		 */
		bool compare_ghost_simplices_pointers(GhostSimplexPointer& a,GhostSimplexPointer& b);
		
		/// This function extracts all the elements having a certain dimension from a vector of simplices pointers.
		/**
		 * This function extracts all the elements having a certain dimension from a vector of simplices pointers, described by the mangrove_tds::SimplexPointer class: the input vector is not
		 * modified.
		 * \param t the dimension of the required elements
		 * \param src the input vector of simplices pointers
		 * \param dst the output vector of simplices pointers
		 * \see mangrove_tds::SimplexPointer, mangrove_tds::SIMPLEX_TYPE
		 */
		void extractSimplices( SIMPLEX_TYPE t, vector<SimplexPointer> &src, vector<SimplexPointer> &dst);
		
		/// This function extracts all the elements having a certain dimension from a vector of ghost simplices pointers.
		/**
		 * This function extracts all the elements having a certain dimension from a vector of ghost simplices pointers, described by the mangrove_tds::GhostSimplexPointer class: the input
		 * vector is not modified.
		 * \param t the dimension of the required elements
		 * \param src the input vector of ghost simplices pointers
		 * \param dst the output vector of ghost simplices pointers
		 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::SIMPLEX_TYPE
		 */
		void extractSimplices( SIMPLEX_TYPE t, vector<GhostSimplexPointer> &src, vector<GhostSimplexPointer> &dst);

		/// This function converts a <i><A href="http://trolltech.com/">Qt Toolkit</A></i> string in a <i>C++-like</i> string.
	    	/**
	     	 * This function converts a <i><A href="http://trolltech.com/">Qt Toolkit</A></i> string in a <i>C++-like</i> string.
	     	 * \param str the input string
	     	 * \return the <i>C++-like</i> version of the input string
	     	 */
	    	string fromQt2Cplustring(QString str);
	    
		/// This function computes the average value by using the double floating point precision.
		/**
		 * This function computes the average value by using the double floating point precision, by starting from two integer values.
		 * \param i0 the first integer value to be analyzed
		 * \param i1 the second integer value to be analyzed
		 * \return the average value by using the double floating point precision
		 */
		double computeAverage(unsigned int i0,unsigned int i1);
		
		/// An alias for the Pascal's Triangle.
		/**
		 * In mathematics, Pascal's triangle is a triangular array, whose elements are the binomial coefficients: the Pascal's Triangle has usually d+1 rows, where d is the <i>order</i> of such
		 * triangle.
		 * \see mangrove_tds::SIMPLEX_TYPE, mangrove_tds::BaseSimplicialComplex
		 */
		typedef vector< vector<unsigned int> > PASCAL_TRIANGLE;
	
		/// Information about simplices adjacent to a simplex.
		/**
		 * This class provides information about simplices adjacent to an other simplex: an instance of this class corresponds to a simplex directly encoded in a data structure, and stores all
		 * simplices adjacent to the reference simplex. Such simplices are encoded through instances of the mangrove_tds::SimplexPointer class.
		 * \see mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex
		 */
		class RawAdjacency
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates an instance of this class, without initializing the internal state.
			 * \param t the dimension of the input simplex
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::SIMPLEX_TYPE
			 */
			RawAdjacency(SIMPLEX_TYPE t);

			/// This member function destroys an instance of this class.
			/**
			 * This member function destroys an instance of this class, deallocating internal state.
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
			 */
			virtual ~RawAdjacency();
			
			/// This member function adds a new simplex adjacent to the reference simplex.
			/**
			 * This member function adds a new simplex adjacent to the reference simplex, sharing a subface of this one.
			 * \param cp a pointer to the new simplex to be added
			 * \param k the index of the shared face between the reference simplex and the new simplex to be added
			 * \param ri the reverse index of the current simplex adjacent to the reference simplex
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
			 */
			void addSimplex(const SimplexPointer& cp,unsigned int k,unsigned int ri);
			
			/// This member function returns a reference to simplices adjacent to the reference simplex.
			/**
			 * This member function returns a reference to simplices adjacent to the reference simplex.
			 * \return a reference to simplices adjacent to the reference simplex
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
			 */
			inline vector< vector<SimplexPointer> >& getSimplices() { return this->simplices; }

			/// This member function returns a reference to the reverse indices for simplices adjacent to the reference simplex.
			/**
			 * This member function returns a reference to the reverse indices for simplices adjacent to the reference simplex.
			 * \return a reference to the reverse indices for simplices adjacent to the reference simplex
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
			 */
			inline vector< vector<unsigned int> >& getReverseIndices() { return this->reverse_indices; }

			/// This member function checks if an subface of the reference simplex is not manifold.
			/**
			 * This member function checks if an subface of the reference simplex is not manifold.
			 * \param k the position of the required subface
			 * \return <ul><li><i>true</i>, if the required subface of the reference simplex is not manifold</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
			 */
			inline bool isSingularity(unsigned int k) { return this->bflags[k]; }
		
			/// This member function adds a simplex describing a non-manifold subface of the reference simplex.
			/**
			 * This member function adds a simplex describing a non-manifold subface of the reference simplex.
			 * \param cp a pointer to the non-manifold subface of the reference simplex
			 * \param k the position of the required subface
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
			 */
			void addSingularity(const SimplexPointer& cp, unsigned int k);
			
			/// This member function retrieves a non-manifold subface of the reference simplex.
			/**
			 * This member function retrieves a non-manifold subface of the reference simplex: we assume the required simplex is non-manifold.
			 * \param k the position of the required subface
			 * \param cp a pointer to the required non-manifold subface of the reference simplex
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
			 */
			void getSingularity(unsigned int k, SimplexPointer& cp);
			
			/// This operator writes a representation of the information about simplices incident in a 0-simplex.
			/**
			 * This operator writes a representation of the information about simplices incident in a 0-simplex, namely a vertex.
			 * \param os the output stream where we can write information about incident simplices of a 0-simplex.
			 * \param r the input incidence information to be written
			 * \return the output stream after having written the input incidence information
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
			 */
			inline friend ostream& operator<<(ostream& os,RawAdjacency &r)
			{
				/* Now, we write all incident simplices */
				for(unsigned int k=0;k<r.simplices.size();k++)
				{
					os<<"\tsimplices adjacent along the subface "<<k<<endl;
					for(unsigned int j=0;j<r.simplices[k].size();j++)
					{
						os<<"\t\tSimplex: "<<r.simplices[k][j];
						os<<"\t\tReverse index: "<<r.reverse_indices[k][j]<<endl<<endl;
					}
				}
				
				/* If we arrive here, we finish! */
				os<<endl;
				os.flush();
				return os;
			}					

			protected:
			
			/// This member function creates an instance of this class.
			/**
			 * This member function creates an instance of this class, without initializing the internal state.
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
			 */
			RawAdjacency() { this->simplices.clear(); }
			
			/// Simplices adjacent to the reference simplex.
			/**
			 * \see mangrove_tds::SimplexPointer
			 */
			vector< vector<SimplexPointer> > simplices;
			
			/// Reverse indices for simplices adjacent to the reference simplex.
			vector< vector<unsigned int> > reverse_indices;
			
			/// Boolean flags for marking the visiting of each subface.
			deque<bool> bflags;
			
			/// Non-manifold singularities for each subface.
			/**
			 * The k-th location of this vector contains a singularity if and only if the k-th location of mangrove_tds::RawAdjacency::bflags is true.
			 * \see mangrove_tds::SimplexPointer
			 */
			vector<SimplexPointer> singularities;
		};	

		/// Information about simplices incident in a simplex.
		/**
		 * This class provides information about simplices incident in a simplex: an instance of this class corresponds to a simplex directly encoded in a data structure, and stores all simplices
		 * incident in the reference simplex. Such simplices are encoded through instances of the mangrove_tds::SimplexPointer class.
		 * \see mangrove_tds::SimplexPointer, mangrove_tds::BaseSimplicialComplex, mangrove_tds::IG, mangrove_tds::IS, mangrove_tds::SIG, mangrove_tds::GIA
		 */
		class RawIncidence
		{
			public:
			
			/// This member function creates an instance of this class.
			/**
			 * This member function creates an instance of this class, without initializing the internal state.
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
			 */
			RawIncidence() { this->simplices.clear(); }

			/// This member function destroys an instance of this class.
			/**
			 * This member function destroys an instance of this class, deallocating internal state.
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
			 */
			inline virtual ~RawIncidence() { this->simplices.clear(); }

			/// This member function returns a reference to simplices incident in the reference simplex.
			/**
			 * This member function returns a reference to simplices incident in the reference simplex.
			 * \return a reference to simplices incident in the reference simplex
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
			 */
			inline vector<SimplexPointer>& getSimplices() { return this->simplices; }

			/// This member function adds a new simplex incident in the reference simplex.
			/**
			 * This member function adds a new simplex incident in the reference simplex.
			 * \param cp a pointer to the new simplex to be added
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
			 */
			inline void addSimplex(const SimplexPointer& cp) { this->simplices.push_back(SimplexPointer(cp)); }
			
			/// This member function filters simplices incident in the reference simplex against their dimension.
			/**
			 * This member function filters simplices incident in the reference simplex against their dimension, giving an inferior extreme.
			 * \param d the smallest dimension of the required simplices
			 * \param l the required simplices
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex, mangrove_tds::SIMPLEX_TYPE
			 */
			void filterSimplices(SIMPLEX_TYPE d,vector<SimplexPointer> &l);
			
			/// This operator writes a representation of the information about simplices incident in a simplex.
			/**
			 * This operator writes a representation of the information about simplices incident in a simplex.
			 * \param os the output stream where we can write information about incident simplices of a simplex.
			 * \param r the input incidence information to be written
			 * \return the output stream after having written the input incidence information
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
			 */
			inline friend ostream& operator<<(ostream& os,RawIncidence &r)
			{
				/* Now, we write all incident simplices */
				for(unsigned int k=0;k<r.simplices.size();k++) { os<<"\t"<<r.simplices[k]; }
				os<<endl;
				os.flush();
				return os;
			}

			protected:
			
			/// Simplices incident in the reference 0-simplex.
			/**
			 * \see mangrove_tds::SimplexPointer
			 */
			vector<SimplexPointer> simplices;
		};
		
		/// A basic description of a face.
		/**
		 * This class provides a description of a face: a face is fully determined by positions of its vertices (i.e. instances of mangrove_tds::SIMPLEX_ID). In other words, this class can be
		 * used as a map for retrieving what vertices we must consider for describing a specific face.<p>The mangrove_tds::RawFace class provides a more complete description of a face.
		 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawFace, mangrove_tds::GIAManager, mangrove_tds::GIA
		 */
		class Face
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * A face is fully determined by positions of its vertices (i.e. instances of mangrove_tds::SIMPLEX_ID). In other words, this class can be used as a map for retrieving what
			 * vertices we must consider for describing a specific face.<p>The mangrove_tds::RawFace class provides a more complete description of a face.<p><b>IMPORTANT:</b> the new face is
			 * not initialized.
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawFace 
			 */
			Face();
			
			/// This member function creates a new instance of this class.
			/**
			 * A face is fully determined by positions of its vertices (i.e. instances of mangrove_tds::SIMPLEX_ID). In other words, this class can be used as a map for retrieving what
			 * vertices we must consider for describing a specific face.<p>The mangrove_tds::RawFace class provides a more complete description of a face.
			 * \param pos the positions of vertices for the current face
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawFace 
			 */
			Face(const vector<SIMPLEX_ID> & pos);
			
			/// This member destroys an instance of this class.
			virtual ~Face();
						
			/// This member function returns a reference to vertices positions of the current face.
			/**
			 * This member function returns a reference to vertices positions of the current face. A face is fully determined by positions of its vertices (i.e. instances of
			 * mangrove_tds::SIMPLEX_ID). In other words, this class can be used as a map for retrieving what vertices we must consider for describing a specific face.<p>The
			 * mangrove_tds::RawFace class provides a more complete description of a face.
			 * \return a reference to vertices positions of the current face
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawFace
			 */
			inline vector<SIMPLEX_ID>& getVertices() { return this->pos; }
			
			/// This member function returns a constant reference to vertices positions of the current face.
			/**
			 * This member function returns a constant reference to vertices positions of the current face. A face is fully determined by positions of its vertices (i.e. instances of
			 * mangrove_tds::SIMPLEX_ID). In other words, this class can be used as a map for retrieving what vertices we must consider for describing a specific face.<p>The
			 * mangrove_tds::RawFace class provides a more complete description of a face.
			 * \return a constant reference to vertices positions of the current face
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawFace
			 */
			inline const vector<SIMPLEX_ID> & getCVertices() const { return this->pos; }
			
			/// This member function returns a reference to subfaces indices of the current face.
			/**
			 * This member function returns a reference to subfaces indices of the current face.
			 * \return a reference to subfaces indices of the current face
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawFace
			 */
			inline vector<SIMPLEX_ID>& getBoundary() { return this->bnd; }

			/// This member function returns a constant reference to subfaces indices of the current face.
			/**
			 * This member function returns a constant reference to subfaces indices of the current face.
			 * \return a constant reference to subfaces indices of the current face
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawFace
			 */
			inline const vector<SIMPLEX_ID>& getCBoundary() const { return this->bnd; }

			/// This member function returns a reference to coboundary faces indices of the current face.
			/**
			 * This member function returns a reference to coboundary faces indices of the current face.
			 * \return a reference to coboundary faces indices of the current face
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawFace
			 */
			inline vector<SIMPLEX_ID>& getCoboundary() { return this->cbnd; }
			
			/// This member function returns a constant reference to coboundary faces indices of the current face.
			/**
			 * This member function returns a constant reference to coboundary faces indices of the current face.
			 * \return a constant reference to coboundary faces indices of the current face
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawFace
			 */
			inline const vector<SIMPLEX_ID>& getCCoboundary() const { return this->cbnd; }

			/// This operator writes a representation of a face.
			/**
			 * This operator writes a representation of a face, i.e. described by its vertices, by its immediate boundary subfaces and coboundary faces.
			 * \param os the output stream where we can write information about the current face
			 * \param f the input face to be described
			 * \return the output stream after having written the input face to be described
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawFace
			 */
			inline friend ostream& operator<<(ostream& os,Face &f)
			{
				/* We write positions */
				os<<"\tIndices of vertices: "<<f.pos;
				os<<"\tIndices of boundary subfaces: "<<f.bnd;
				os<<"\tIndices of coboundary faces: "<<f.cbnd<<endl;
				os.flush();
				return os;
			}			

			protected:
			
			/// Vertices positions of the current face
			/**
			 * A face is fully determined by indices of its vertices (i.e. instances of mangrove_tds::SIMPLEX_ID). The mangrove_tds::RawFace class provides a more complete description of a
			 * face.
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawFace
			 */
			vector<SIMPLEX_ID> pos;
			
			/// Indices of boundary faces.
			/**
			 * \see mangrove_tds::SIMPLEX_ID
			 */
			vector<SIMPLEX_ID> bnd;
			
			/// Indices of coboundary faces.
			/**
			 * \see mangrove_tds::SIMPLEX_ID
			 */
			vector<SIMPLEX_ID> cbnd;
		};
		
		/// A hierarchy of subfaces for a simplex.
		/**
		 * This class describes a hierarchy of subfaces for a simplex belonging to a simplicial complex. Each face is completely described by its vertices positions (see the mangrove_tds::Face
		 * class). In this way, we can extract all the subfaces of a simplex. The generation of a subface is guided by an orientation, that can be also used for extracting a chain-complex. In
		 * other words, if a k-face is described by a list of vertices l, then the j-th subface can be obtained by discarding the j-th vertex from l. The vertices positions for a single face can
		 * be described through an instance of the mangrove_tds::Face class.
		 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::RawFace, mangrove_tds::Face
		 */
		class FaceHierarchy
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a new instance of this class.<p><b>IMPORTANT:</b> the new hierarchy is not initialized. You should not apply this member function for building an
			 * hierarchy of faces.
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawFace, mangrove_tds::Face
			 */
			inline FaceHierarchy() { this->fposes.clear(); }
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a new instance of this class. With this member function, you can build an hierarchy of faces, expressed by vertices positions (see the
			 * mangrove_tds::Face class) for a simplex of a certain dimension.<p><b>IMPORTANT:</b> you cannot modify this hierarchy. You can apply this class as a map for extracting vertices
			 * of subfaces.
			 * \param d the dimension of the reference simplex
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::RawFace, mangrove_tds::Face
			 */
			FaceHierarchy(SIMPLEX_TYPE d);
			
			/// This member function destroys an instance of this class.
			/**
			 * This member function destroys an instance of this class.
			 * \see mangrove_tds::FaceHierarchy::clear()
			 */
			inline virtual ~FaceHierarchy() { this->clear(); }
			
			/// This member function deallocates the internal state of this class.
			/**
			 * This member function deallocates the internal state of this class, destroying the entire hierarchy of faces. 
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::RawFace, mangrove_tds::Face
			 */ 
			void clear();
			
			/// This member function retrieves the current hierarchy of faces for the reference simplex.
			/**
			 * This member function retrieves the current hierarchy of faces for the reference simplex. Each face is completely described by its vertices positions (see the mangrove_tds::Face
			 * class).<p><b>IMPORTANT:</b> you cannot modify this hierarchy. You can apply this class as a map for extracting vertices of subfaces.
			 * \return the current hierarchy of faces for the reference simplex, organized by dimension
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::RawFace, mangrove_tds::Face
			 */
			inline const vector< vector<Face> >& getHierarchy() const { return this->fposes; }
			
			/// This member function returns the dimension of the reference simplex for the current hierarchy of faces.
			/**
			 * This member function returns the dimension of the reference simplex for the current hierarchy of faces. Each face is completely described by its vertices positions (see the
			 * mangrove_tds::Face class).
			 * \return the dimension of the reference simplex for the current hierarchy of faces
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SIMPLEX_TYPE, mangrove_tds::RawFace, mangrove_tds::Face
			 */
			SIMPLEX_TYPE getDimension() const;
			
			/// This operator provides a representation of a hierarchy of faces.
			/**
			 * This operator provides a representation of a hierarchy of faces.
			 * \param os the output stream where we can write information about the input hierarchy of faces: each face is completely described by its vertices positions (see the
			 * mangrove_tds::Face class).
			 * \param fh the input hierarchy of faces
			 * \return the output stream after having written the input hierarchy of faces
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawFace
			 */
			inline friend ostream& operator<<(ostream& os,FaceHierarchy &fh)
			{
				/* We write the hierarchy! */
				os<<endl<<endl;
				os<<"\tHierarchy of faces for a "<<fh.getDimension()<<"-simplex"<<endl;
				for(unsigned int k=0;k<fh.fposes.size();k++)
				{
					os<<"Hierarchy of "<<k<<"-faces: "<<endl;
					os<<fh.fposes[k]<<endl;
				}
				
				/* We finalize! */
				os.flush();
				return os;
			}
			
			protected:
			
			/// The complete hierarchy of subfaces for a simplex.
			/**
			 * \see mangrove_tds::Face, mangrove_tds::SIMPLEX_ID, mangrove_tds::SIMPLEX_TYPE
			 */
			vector< vector<Face> > fposes;
		};
		
		/// A raw description of a triangle.
		/**
		 * This class provides a raw description of a triangle, useful for sorting triangles in counter-clockwise order around a common edge. This raw description is composed by a pointer (see
		 * mangrove_tds::SimplexPointer class) and of vertices (see mangrove_tds::Point class) for the triangle to be represented.
		 * \see mangrove_tds::SimplexPointer, mangrove_tds::Simplex, mangrove_tds::Point, mangrove_tds::TriangleSegmentManager, mangrove_tds::TriangleSegment
		 */
		class RawTriangle
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a new instance of this class: you should complete this new instance through the member functions offerred by this class.
			 * \see mangrove_tds::RawTriangle::initState()
			 */
			inline RawTriangle() { this->initState(); }

			/// This member function destroys an instance of this class.
			/**
			 * \see mangrove_tds::RawTriangle::initState()
			 */
			inline virtual ~RawTriangle() { this->initState(); }
			
			/// This member function returns a reference to a pointer related to the real triangle.
			/**
			 * This member function returns a reference to a pointer related to the real triangle, to be compared with the reference one.
			 * \return a reference to a pointer related to the real triangle
			 * \see mangrove_tds::SimplexPointer
			 */
			inline SimplexPointer & getRealSimplex() { return this->effPnt; }
			
			/// This member function returns a reference to the vertices of the current triangle.
			/**
			 * This member function returns a reference to the vertices of the current triangle.
			 * \return a reference to the vertices of the current triangle
			 * \see mangrove_tds::Point, mangrove_tds::RawTriangle::getRealSimplex()
			 */
			inline vector< Point<double> > & getRealTriangleVertices() { return this->realVerts; }
			
			/// This operator provides a debug representation of the current triangle.
			/**
			 * This operator provides a debug representation of the current triangle.
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Point
			 */
			friend ostream& operator<<(ostream& out, RawTriangle &rt)
			{
				/* We write a representation of the internal state! */
				out<<"\t Current triangle: "<<rt.effPnt;
				out<<"\t Vertices of the current triangle: "<<rt.realVerts;
				out.flush();
				return out;
			}
			
			/// This operator checks if two triangles are the the same triangle.
			/**
			 * This operator checks if two triangles are the the same triangle, i.e. they refer the same effective triangle.
			 * \param rt the input triangle to be compared with the current one
			 * \return <ul><li><i>true</i>, if the input and current triangles are the same ones</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Point
			 */
			inline bool operator==(RawTriangle& rt)
			{
				/* We check the effective triangle! */
				if(this->effPnt.type()!=rt.effPnt.type()) return false;
				if(this->effPnt.id()!=rt.effPnt.id()) return false;
				return true;
			}

			protected:
			
			/// A pointer to the effective triangle.
			/**
			 * \see mangrove_tds::SimplexPointer
			 */
			SimplexPointer effPnt;
			
			/// Euclidean vertices of the effective triangle.
			/**
			 * \see mangrove_tds::Point
			 */
			vector< Point<double> > realVerts;
			
			/// This member function initializes the internal state of this class.
			/**
			 * This member function initializes the internal state of this class.
			 * \see mangrove_tds::SimplexPointer, mangrove_tds::Point
			 */
			inline void initState() { this->realVerts.clear(); }
		};
		
		/// This function compares two raw descriptions of triangles.
		/**
		 * This function compares two raw descriptions of triangles. This function implements a <i>"strictly less than"</i> comparator for two raw triangles descriptions, by using the ordering of
		 * triangles around an edge belonging to a reference triangle. In other words, the first triangle is <i>"strictly less than"</i> the second, if it is located before in radial order
		 * around an edge of the reference triangle.
		 * \param rta a raw description of the first triangle to be compared
		 * \param rtb a raw description of the second triangle to be compared
		 * \return <ul><li><i>true</i>, if the first triangle is lesser than the second</li><li><i>false</i>, otherwise</li></ul>
		 * \see mangrove_tds::RawTriangle, mangrove_tds::Point
		 */
		bool compare_raw_triangles(RawTriangle rta,RawTriangle rtb);
		
		/// This function looks for a raw simplex in a vector of raw triangles.
		/**
		 * This function looks for a raw triangle in a vector of raw triangles, returning the position of the input simplex, if it exists.
		 * \param cp a pointer to the input simplex
		 * \param triangles the input list of triangles to be checked
		 * \return a reference to the required simplex in the input vector of triangles
		 * \see mangrove_tds::RawTriangle, mangrove_tds::Point, mangrove_tds::SimplexPointer, mangrove_tds::positionOfTriangle(), mangrove_tds::RawTriangle
		 */
		vector<RawTriangle>::iterator findRawTriangle(const SimplexPointer& cp,vector<RawTriangle>& triangles);

		/// This function looks for a raw simplex in a vector of raw triangles.
		/**
		 * This function looks for a raw triangle in a vector of raw triangles, returning the position of the input simplex, if it exists.
		 * \param cp a pointer to the input simplex
		 * \param triangles the input list of triangles to be checked
		 * \return the position of to the required simplex in the input vector of triangles 
		 * \see mangrove_tds::RawTriangle, mangrove_tds::Point, mangrove_tds::SimplexPointer, mangrove_tds::RawTriangle, mangrove_tds::findRawTriangle()
		 */
		unsigned int positionOfTriangle(const SimplexPointer& cp,vector<RawTriangle> &triangles);
		
		/// A raw and auxiliary description of a ghost simplex.
		/**
		 * This class provides a raw and auxiliary description of a ghost simplex: in this context, a ghost simplex is not directly encoded in the input data structure, and it is completely
		 * described by its vertices. Moreover, we are interested to its possible aliases, considered as child of several ghost simplices. We encode these aliases through instances of the
		 * mangrove_tds::GhostSimplexPointer instances.
		 * \see mangrove_tds::GhostSimplex, mangrove_tds::NMIA, mangrove_tds::GhostSimplexPointer
		 */
		class RawGhostSimplex
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a new instance of this class, but it does not initialize it: you must apply member functions provided by this class for initializing internal
			 * fields.
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawGhostSimplex::getAliases(), mangrove_tds::RawGhostSimplex::getVertex()
			 */
			RawGhostSimplex();
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a new instance of this class, as a deep copy of the input raw simplex internal fields.
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::RawGhostSimplex::getAliases(), mangrove_tds::RawGhostSimplex::getVertices()
			 */
			RawGhostSimplex(const RawGhostSimplex& s);

			/// This member function destroys an instance of this class.
			virtual ~RawGhostSimplex();
			
			/// This member function returns a reference to the vertices belonging to the current raw ghost simplex.
			/**
			 * This member function returns a reference to the vertices belonging to the current raw ghost simplex.
			 * \return a reference to the vertices belonging to the current raw ghost simplex
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::SimplexPointer
			 */
			vector<SIMPLEX_ID>& getVertices();
			
			/// This member function returns a reference to the aliases of the current raw ghost simplex.
			/**
			 * This member function returns a reference to the aliases of the current raw ghost simplex.
			 * \return a reference to the aliases of the current raw ghost simplex
			 * \see mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
			 */
			vector<GhostSimplexPointer>& getAliases();

			/// This operator writes a representation of the current raw ghost simplex.
			/**
			 * This operator writes a representation of the current raw ghost simplex, only for debugging purposes.
			 * \param os the output stream where we can write the raw ghost simplex
			 * \param f the input raw ghost simplex to be written
			 * \return the output stream after having written the input raw ghost simplex
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::GhostSimplexPointer, mangrove_tds::GhostSimplex
			 */
			inline friend ostream& operator<<(ostream& os,RawGhostSimplex &f)
			{
				/* We write a representation of the input RawGhostSimplex! */
				os<<"\t Vertices: "<<f.vertices;
				os<<"\t Aliases: "<<endl;
				for(unsigned int k=0;k<f.aliases.size();k++) os<<"\t\t"<<f.aliases[k];
				os<<endl;
				os.flush();
				return os;
			}
		
			protected:
			
			/// The identifiers of the vertices belonging to the boundary of the current raw ghost simplex.
			/**
			 * \see mangrove_tds::SIMPLEX_ID
			 */
			vector<SIMPLEX_ID> vertices;
			
			/// Several aliases of the current raw ghost simplex.
			/**
			 * \see mangrove_tds::GhostSimplexPointer
			 */
			vector<GhostSimplexPointer> aliases;
		};
		
		/// This function compares two descriptions of ghost simplices.
		/**
		 * This function compares two descriptions of ghost simplices. This function implements a <i>"strictly less than"</i> comparator for two descriptions of ghost simplices, by using their
		 * vertices: this component is based on the lexicographic order and gives priority to the vectors size.
		 * \param rfa the first ghost simplex to be compared
		 * \param rfb the second ghost simplex to be compared
		 * \return <ul><li><i>true</i>, if the first ghost simplex is lesser than the second</li><li><i>false</i>, otherwise</li></ul>
		 * \see mangrove_tds::RawGhostSimplex, mangrove_tds::SIMPLEX_ID
		 */
		bool compare_ghost_simplices(RawGhostSimplex rfa, RawGhostSimplex rfb);
		
		/// This function checks if two descriptions of ghost simplices are the same.
		/**
		 * This function checks if two descriptions of ghost simplices are the same, by using their vertices.
		 * \param rfa the first ghost simplex to be compared
		 * \param rfb the second ghost simplex to be compared
		 * \return <ul><li><i>true</i>, if the input descriptions of ghost simplices are the same</li><li><i>false</i>, otherwise</li></ul>
		 * \see mangrove_tds::RawFace, mangrove_tds::SIMPLEX_ID
		 */
		bool same_ghost_simplices(RawGhostSimplex rfa, RawGhostSimplex rfb);
	}

#endif

