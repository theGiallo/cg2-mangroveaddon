/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library
 *                                                                        				    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * November 2011 (Revised on May 2012)
 *                                                                    
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * gmv_manager.h - component for I/O operations on GMV files
 ***********************************************************************************************/
 
/* Should we include this header file? */
#ifndef GMV_MANAGER_H

	#define GMV_MANAGER_H

	#include "IO.h"
	#include "SimplicesMapper.h"
	#include "Simplex.h"
	#include "Point.h"
	#include <climits>
	#include <cfloat>
	#include <iomanip>
	using namespace mangrove_tds;
	using namespace std;
	
	/// Components for I/O between simplicial complexes and GMV files.
	/**
	 * The class defined in this file describes a component for I/O between simplicial complexes and files designed for the
	 * <i><A href="http://www.generalmeshviewer.com/">General Mesh Viewer</A></i>, a commercial software package for finite element analysis, developed at the <i>Los Alamos Laboratories</i>, Los
	 * Alamos, USA.<p>This format is very complex and this class offers a limited support. For instance, supported files must satisfy these guidelines:<p> <i>gmvinput ascii<br>[ comments ]
	 * endcomm<br>nodes [ number of coordinates ]<br>[ list of the x coordinates ]<br>[ list of the y coordinates ]<br>[ list of the z coordinates ]<br> cells [ number of top simplices ]
	 * [ simplices description ]<br>endgmv</i><p>This format encodes a soup of top simplices, describing a simplicial 3-complex embedded in the 3D Euclidean space. Accepted descriptions
	 * are:<ul><li><i>line 2 [ 2 indices for the vertices ]</i> for describing an edge;</li><li><i>tri 3 [ 3 indices for the vertices ]</i> for describing a triangle;</li><li>
	 * <i>tet 4 [ 4 indices for the vertices ]</i> for describing a tetrahedron.</li></ul><p>In this class we extract the main information stored in the input file and then we use this component for
	 * creating a new simplicial complex. Moreover, we can write a representation of a simplicial complex formatted in accordance with the <i>GMV</i> format. In this component only the Euclidean
	 * coordinates are supported, while we do not store any field value for each vertex.
	 * \file gmv_manager.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	
	/* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		/// A manager for the content of a file written in the <i>GMV</i> format.
		/**
	 	 * The class defined in this file describes a component for I/O between simplicial complexes and files designed for the
	 	 * <i><A href="http://www.generalmeshviewer.com/">General Mesh Viewer</A></i>, a commercial software package for finite element analysis developed at the <i>Los Alamos Laboratories</i>,
		 * Los Alamos, USA.<p>This format is very complex and this class offers a limited support. For instance, supported files must satisfy these guidelines:
	 	 * <p><i>gmvinput ascii<br>[ comments ] endcomm<br>nodes [ number of coordinates ]<br>[ list of the x coordinates ]<br>[ list of the y coordinates ]<br>[ list of the z coordinates ]<br>
		 * cells [ number of top simplices ] [ simplices description ]<br>endgmv</i><p>This format encodes a soup of top simplices, describing a simplicial 3-complex embedded in the 3D Euclidean
		 * space. Accepted descriptions are:<ul><li><i>line 2 [ 2 indices for the vertices ]</i> for describing an edge;</li><li><i>tri 3 [ 3 indices for the vertices ]</i> for describing a
		 * triangle;</li><li><i>tet 4 [ 4 indices for the vertices ]</i> for describing a tetrahedron.</li></ul><p>In this class we extract the main information stored in the input file and then
		 * we use this component for creating a new simplicial complex. Moreover, we can write a representation of a simplicial complex formatted in accordance with the <i>GMV</i> format. In
		 * this component only the Euclidean coordinates are supported, while we do not store any field value for each vertex.
	 	 * \see mangrove_tds::DataManager, mangrove_tds::IO, mangrove_tds::Point, mangrove_tds::MeshComponent
	 	 */
	 	class GMVManager : public DataManager<GMVManager>
		{
			public:
			
			/// This member function creates an instance of this class.
			/**
			 * This member function creates a subclass of the mangrove_tds::DataManager class, optimized for managing a file written in <i>GMV</i> format, that contains a soup of top
			 * simplices.<p>Such format is designed for the <i><A href="http://www.generalmeshviewer.com/">General Mesh Viewer</A></i>, a commercial software package for finite
			 * element analysis developed at the <i>Los Alamos Laboratories</i>, Los Alamos, USA.<p>This format is very complex and this class offers a limited support. For instance,
			 * supported files must satisfy these guidelines:<p><i>gmvinput ascii<br>[ comments ] endcomm<br>nodes [ number of coordinates ]<br>[ list of the x coordinates ]<br>
			 * [ list of the y coordinates ]<br>[ list of the z coordinates ]<br>cells [ number of top simplices ] [ simplices description ]<br>endgmv</i><p>This format encodes a soup of top
			 * simplices, describing a simplicial 3-complex embedded in the 3D Euclidean space. Accepted descriptions are:<ul><li><i>line 2 [ 2 indices for the vertices ]</i> for describing
			 * an edge;</li><li><i>tri 3 [ 3 indices for the vertices ]</i> for describing a triangle;</li><li><i>tet 4 [ 4 indices for the vertices ]</i> for describing a tetrahedron.</li>
			 * </ul><b>IMPORTANT:</b> the new instance created by this member function is not initialized, consequently we must load new data through the mangrove_tds::GMVManager::loadData()
			 * member function.
			 * \see mangrove_tds::DataManager, mangrove_tds::IO, mangrove_tds::Point, mangrove_tds::GMVManager::loadData()
			 */
			inline GMVManager() : DataManager<GMVManager>() 
			{
				/* We reset the coordinates! */
				this->xs.clear();
				this->ys.clear();
				this->zs.clear();
			}
			
			/// This member function destroys an instance of this class.
			/**
			 * This member function destroys an instance of this class.
			 * \see mangrove_tds::GMVManager::resetState()
			 */
			inline virtual ~GMVManager() { this->resetState(); }
			
			/// This member function allows to customize the property name containing the Euclidean coordinates in the simplicial complex to be built.
			/**
			 * This member function allows to customize the property name containing the Euclidean coordinates for all vertices in the simplicial complex to be built.<p>In this component,
			 * only the Euclidean coordinates are supported, while we do not store any field value for each vertex: the string to be used for the Euclidean coordinates is <i>coordinates</i>.
			 * \return the property name containing the Euclidean coordinates in the simplicial complex to be built
			 * \see mangrove_tds::PropertyBase, mangrove_tds::Point, mangrove_tds::GMVManager::supportsEuclideanCoordinates(), mangrove_tds::GMVManager::getCoordinates()
			 */
			inline string getPropertyName4Coordinates() { return string("coordinates"); }
			
			/// This member function checks if the loading of the Euclidean coordinates is supported and enabled in the current component.
			/**
			 * This member function checks if the loading of the Euclidean coordinates is supported by the current component. In this component only the Euclidean coordinates are supported,
			 * while we do not store any field value for each vertex.
			 * \return <ul><li><i>true</i>, if the current component supports the loading of the Euclidean coordinates for all the vertices.</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::Point, mangrove_tds::PropertyBase, mangrove_tds::GMVManager::getPropertyName4Coordinates(), mangrove_tds::GMVManager::getCoordinates()
			 */
			inline bool supportsEuclideanCoordinates() 
			{
				/* In this component we could load the Euclidean coordinates, now we must check if we have the same number of elements in xs, ys, zs! */
				if(this->simplices.size()==0) return false;
				if(xs.size()!=simplices[0].vectorSize()) return false;
				if(ys.size()!=simplices[0].vectorSize()) return false;
				if(zs.size()!=simplices[0].vectorSize()) return false;
				return true;
			}
			
			/// This member function returns the Euclidean coordinates associated to a vertex in the simplicial complex to be built.
			/**
			 * This member function returns the Euclidean coordinates associated to a vertex in the simplicial complex to be built.<p>If we cannot complete this operation for any reason, then
			 * this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param i the identifier of the required vertex
			 * \param p the Euclidean coordinates of the required vertex
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::Point, mangrove_tds::PropertyBase, mangrove_tds::Simplex, mangrove_tds::GMVManager::getPropertyName4Coordinates(),
			 * mangrove_tds::GMVManager::supportsEuclideanCoordinates()
			 */
			inline void getCoordinates(SIMPLEX_ID i,Point<double> & p)
			{
				/* First, we check if all is ok and then we extract the i-th point from 'coordinates' */
				assert(this->supportsEuclideanCoordinates());
				assert(i<this->xs.size());
				p.updateSize(3);
				p.x()=this->xs[i];
				p.y()=this->ys[i];
				p.z()=this->zs[i];
			}
			
			/// This member function allows to customize the property name containing the field values in the simplicial complex to be built.
			/**
			 * This member function allows to customize the property name containing the field values for all vertices in the simplicial complex to be built.<p>In this component only the
			 * Euclidean coordinates are supported, while we do not store any field value for each vertex. Consequently, this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \return the property name containing the field values in the simplicial complex to be built
			 * \see mangrove_tds::PropertyBase, mangrove_tds::GMVManager::supportsFieldValues(), mangrove_tds::GMVManager::getFieldValue()
			 */
			inline string getPropertyName4FieldValue()
			{
				/* Field values are not supported! */
				assert(false);
				return string(); 
			}
			
			/// This member function checks if the loading of the field values is supported by the current component.
			/**
			 * This member function checks if the loading of the field values is supported by the current component: in this component only the Euclidean coordinates are supported, while we
			 * do not store any field value for each vertex.
			 * \return <ul><li><i>true</i>, if the current component supports the loading of the field values for all the vertices.</li><li><i>false</i>, otherwise</li></ul>
			 * \see mangrove_tds::GMVManager::getPropertyName4FieldValue(), mangrove_tds::GMVManager::getFieldValue()
			 */
			inline bool supportsFieldValues() { return false; }
			
			/// This member function returns the field value associated to a vertex in the current simplicial complex.
			/**
			 * This member function returns the field value associated to a vertex in the simplicial complex to be built. In this component only the Euclidean coordinates are supported,
			 * while we do not store any field value for each vertex. Consequently, this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode.
			 * In this case, each forbidden operation will result in a failed assert.
			 * \param i the identifier of the required vertex
			 * \param f the field value of the required vertex
			 * \see mangrove_tds::GMVManager::getPropertyName4FieldValue(), mangrove_tds::GMVManager::supportsFieldValues(), mangrove_tds::SIMPLEX_ID, mangrove_tds::idle()
			 */
			inline void getFieldValue(SIMPLEX_ID i, double &f)
			{
				/* Dummy statetements for avoiding warnings! */
				idle(i);
				idle(f);
				assert(false);
			}
			
			/// This member function parses the content of a file written in <i>GMV</i> format.
			/**
			 * This member function parses the content of a file written in the <i>GMV</i> format: it is designed for the
			 * <i><A href="http://www.generalmeshviewer.com/">General Mesh Viewer</A></i>, a commercial software package for finite element analysis developed at the
			 * <i>Los Alamos Laboratories</i>, Los Alamos, USA.<p>This format is very complex and this class offers a limited support. For instance, supported files must satisfy these
			 * guidelines:<p><i>gmvinput ascii<br>[ comments ] endcomm<br>nodes [ number of coordinates ]<br>[ list of the x coordinates ]<br>[ list of the y coordinates ]<br>
			 * [ list of the z coordinates ]<br>cells [ number of top simplices ] [ simplices description ]<br>endgmv</i><p>This format encodes a soup of top simplices, describing a
			 * simplicial 3-complex embedded in the 3D Euclidean space. Accepted descriptions are:<ul><li><i>line 2 [ 2 indices for the vertices ]</i> for describing an edge;</li>
			 * <li><i>tri 3 [ 3 indices for the vertices ]</i> for describing a triangle;</li><li><i>tet 4 [ 4 indices for the vertices ]</i> for describing a tetrahedron.</li></ul>In this
			 * component, only the Euclidean coordinates are supported, while we do not store any field value for each vertex. The string to be used for the Euclidean coordinates is
			 * <i>coordinates</i>, returned by the mangrove_tds::GMVManager::getPropertyName4Coordinates() member function: you can extract coordinates for a vertex through the
			 * mangrove_tds::GMVManager::getCoordinates() member function.<p>If the input file does not exist or its content does not satisfy the <i>GMV</i> format guidelines, then this
			 * member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param fname the path of the input file
			 * \param reqParams this boolean flag identfies if the loading of the auxiliary parameters is required
		 	 * \param gd this boolean flag indicates if the effective generation of raw simplices must be performed
		 	 * \param checksOn this boolean flag indicates if the effective checks on the input data must be performed
			 * \see mangrove_tds::Simplex, mangrove_tds::SimplicesContainer, mangrove_tds::SimplexPointer, mangrove_tds::GMVManager::resetState(),
			 * mangrove_tds::GMVManager::getCoordinates(), mangrove_tds::GMVManager::getPropertyName4Coordinates(), mangrove_tds::GMVManager::mergeComponents()
			 */
			inline void loadData(string fname,bool reqParams,bool gd,bool checksOn)
			{
				string aux,lab;
				unsigned int n;
				vector<unsigned int> ids;
				
				/* ====================================================================
				 * First, we reset the current state and then we open the input file!. Repetita iuvant - supported GMV files:
				 *
				 * gmvinput ascii
				 * <comments> endcomm
				 * nodes <number of coordinates>
				 * [ list of the x coordinates ] [ list of the y coordinates ] [ list of the z coordinates ]
				 * cells <number of top simplices>
				 * line 2 <indices> (optional)
				 * tri 3 <indices> (optional)
				 * tet 4 <indices> (optional)
				 * endgmv 
				 * ==================================================================== */
				this->resetState();
				this->in.open(fname.c_str());
				assert(this->in.is_open());
				this->in>>aux;
				assert( aux.compare("gmvinput")==0);
				this->in>>aux;
				assert( aux.compare("ascii")==0);
				do { this->in>>aux; }
				while( aux.compare("endcomm")!=0);
				this->in>>aux;
				assert(aux.compare("nodes")==0);
				this->in>>n;
				if(n==0) return;
				this->simplices.push_back(SimplicesContainer(0));
				this->components.push_back(MeshComponent());
				this->xs.resize(n);
				this->ys.resize(n);
				this->zs.resize(n);
				this->in>>setprecision(9)>>(this->xs);
				this->in>>setprecision(9)>>(this->ys);
				this->in>>setprecision(9)>>(this->zs);
				for(unsigned int k=0;k<this->xs.size();k++) this->components.back().addPoint(this->xs[k],this->ys[k],this->zs[k]);
				this->xs.clear();
				this->ys.clear();
				this->zs.clear();
				this->in>>aux;
				assert(aux.compare("cells")==0);
				this->in>>n;
				do
				{
					/* Now, we read the next keyword! */
					this->in>>aux;
					ids.clear();
					if(aux.compare("line")==0)
					{
						/* We must read a description of an edge! */
						this->in>>lab;
						assert(lab.compare("2")==0);
						ids.resize(2);
						this->in>>ids;
						for(unsigned int k=0;k<ids.size();k++) ids[k]=ids[k]-1;
						this->components.back().addSimplex(ids);
					}
					else if(aux.compare("tri")==0)
					{
						/* We must read a description of a triangle! */
						this->in>>lab;
						assert(lab.compare("3")==0);
						ids.resize(3);
						this->in>>ids;
						for(unsigned int k=0;k<ids.size();k++) ids[k]=ids[k]-1;
						this->components.back().addSimplex(ids);
					}
					else if(aux.compare("tet")==0)
					{
						/* We must read a description of a tetrahedron! */
						this->in>>lab;
						assert(lab.compare("4")==0);
					    ids.resize(4);
					    this->in>>ids;
						for(unsigned int k=0;k<ids.size();k++) ids[k]=ids[k]-1;
						this->components.back().addSimplex(ids);
					}
				}
				while( (this->in.eof()==false) && (aux.compare("endgmv")!=0 ));
				this->in.close();
				if(checksOn) this->mergeComponents(reqParams,gd);
				else { this->copyComponents(reqParams,gd); }
			}
			
			/// This member function exports a simplicial complex in a file written in <i>GMV</i> format.
			/**
			 * This member function exports a simplicial complex, by writing its representation in accordance with the <i>GMV</i> format. Such format is designed for the 
			 * <i><A href="http://www.generalmeshviewer.com/">General Mesh Viewer</A></i>, a commercial software package for finite element analysis developed at the
			 * <i>Los Alamos Laboratories</i>, Los Alamos, USA.<p>This format is very complex and this class offers a limited support. For instance, supported files must satisfy these
			 * guidelines:<p><i>gmvinput ascii<br>[ comments ] endcomm<br>nodes [ number of coordinates ]<br>[ list of the x coordinates ]<br>[ list of the y coordinates ]<br>
			 * [ list of the z coordinates ]<br>cells [ number of top simplices ] [ simplices description ]<br>endgmv</i><p>This format encodes a soup of top simplices, describing a
			 * simplicial 3-complex embedded in the 3D Euclidean space. Accepted descriptions are:<ul><li><i>line 2 [ 2 indices for the vertices ]</i> for describing an edge;</li>
			 * <li><i>tri 3 [ 3 indices for the vertices ]</i> for describing a triangle;</li><li><i>tet 4 [ 4 indices for the vertices ]</i> for describing a tetrahedron.</li></ul>We assume
			 * Euclidean coordinates in the input simplicial complex are stored in a local property named as the string returned by the mangrove_tds::GMVManager::getPropertyName4Coordinates()
			 * member function. Moreover, we write only the valid simplices, i.e. simplices with real locations and not marked as <i>deleted</i> by the garbage collector mechanism.<p>If we
			 * cannot complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each
			 * forbidden operation will result in a failed assert.
		 	 * \param ccw the input simplicial complex to be written
			 * \param fname the path for the output file
			 * \see mangrove_tds::SimplexPointerMap, mangrove_tds::BaseSimplicialComplex, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::LocalPropertyHandle,
			 * mangrove_tds::GMVManager::getPropertyName4Coordinates(), mangrove_tds::GMVManager::supportsEuclideanCoordinates(), mangrove_tds::GMVManager::writeGlobalComplex(),
			 * mangrove_tds::GMVManager::writeLocalComplex()
			 */
			template <class T> void writeData(BaseSimplicialComplex<T> *ccw,string fname)
			{
				/* ===================================================================
				 * Repetita iuvant - supported GMV files:
			 	 *
		 		 * gmvinput ascii
		 		 * <comments> endcomm
		 		 * nodes <number of coordinates>
		 		 * [ list of the x coordinates ] [ list of the y coordinates ] [ list of the z coordinates ]
		 		 * cells <number of top simplicess>
				 * line 2 <indices> (optional)
		 		 * tri 3 <indices> (optional)
		 		 * tet 4 <indices> (optional)
		 		 * endgmv
		 		 * =================================================================== */
				assert(ccw!=NULL);
				assert(ccw->type()<=3);
				if(ccw->encodeAllSimplices()==true) this->writeGlobalComplex(ccw,fname);
				else this->writeLocalComplex(ccw,fname);
			}

			/// This member function exports a subcomplex in a file written in <i>GMV</i> format.
			/**
			 * This member function exports a subcomplex in a file written in <i>GMV</i> format. Such format is designed for the
			 * <i><A href="http://www.generalmeshviewer.com/">General Mesh Viewer</A></i>, a commercial software package for finite element analysis developed at the
			 * <i>Los Alamos Laboratories</i>, Los Alamos, USA.<p>This format is very complex and this class offers a limited support. For instance, supported files must satisfy these
			 * guidelines:<p><i>gmvinput ascii<br>[ comments ] endcomm<br>nodes [ number of coordinates ]<br>[ list of the x coordinates ]<br>[ list of the y coordinates ]<br>
			 * [ list of the z coordinates ]<br>cells [ number of top simplices ] [ simplices description ]<br>endgmv</i><p>This format encodes a soup of top simplices, describing a
			 * simplicial 3-complex embedded in the 3D Euclidean space. Accepted descriptions are:<ul><li><i>line 2 [ 2 indices for the vertices ]</i> for describing an edge;</li>
			 * <li><i>tri 3 [ 3 indices for the vertices ]</i> for describing a triangle;</li><li><i>tet 4 [ 4 indices for the vertices ]</i> for describing a tetrahedron.</li></ul><p>We can
			 * manage only a simplicial 3-complex composed by vertices, lines, triangles, and tetrahedra. Here, we consider a subset of simplices directly encoded in the input data
			 * structure, that must be valid simplices, i.e. simplices with real locations and not marked as <i>deleted</i> by the garbage collector mechanism.<p>We assume Euclidean
			 * coordinates in the input simplicial complex are stored in a local property named as the string returned by the mangrove_tds::GMVManager::getPropertyName4Coordinates() member
			 * function.<p><b>IMPORTANT:</b> if we cannot complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is
			 * compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param tops the simplices to be considered top in the new subcomplex to be written
			 * \param ccw the reference simplicial complex
			 * \param fname the path of the output file
			 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::SimplexPointer, mangrove_tds::LocalPropertyHandle, mangrove_tds::RawFace, mangrove_tds::Point,
			 * mangrove_tds::SimplexPointerMap, mangrove_tds::SIMPLEX_ID,  mangrove_tds::GMVManager::getPropertyName4Coordinates(),
			 * mangrove_tds::GMVManager::supportsEuclideanCoordinates()
			 */
			template <class T> void writeSubcomplex( vector<SimplexPointer> &tops, BaseSimplicialComplex<T> *ccw, string fname)
			{
				string ec_name;
				vector< vector<RawFace> > simplexes;
				list<SimplexPointer> bk;
				set<SIMPLEX_ID> vs;
				GhostSimplex s;
				list<GhostSimplexPointer> bk_loc;
				vector<double> aux_xs,aux_ys,aux_zs;
				map<SIMPLEX_ID,SIMPLEX_ID> vett;
				SIMPLEX_ID vind;
				Point<double> p;
				unsigned int n;

				/* ===================================================================
				 * Repetita iuvant - supported GMV files:
			 	 *
		 		 * gmvinput ascii
		 		 * <comments> endcomm
		 		 * nodes <number of coordinates>
		 		 * [ list of the x coordinates ] [ list of the y coordinates ] [ list of the z coordinates ]
		 		 * cells <number of top simplicess>
				 * line 2 <indices> (optional)
		 		 * tri 3 <indices> (optional)
		 		 * tet 4 <indices> (optional)
		 		 * endgmv
		 		 * =================================================================== */
		 		assert(ccw!=NULL);
				assert(ccw->type()<=3);
				ec_name=string(this->getPropertyName4Coordinates());
				assert(ccw->hasProperty(string(ec_name)));
				assert(tops.size()!=0);
				if(this->out.is_open()) this->out.close();
				this->out.open(fname.c_str());
				assert(this->out.is_open());
				this->out<<string("gmvinput ascii")<<endl;
				this->out<<string(fname)<<" "<<string("- generated by the Mangrove TDS Library ");
				this->out<<string("endcomm")<<endl;
				for(SIMPLEX_ID k=0;k<=ccw->type();k++) { simplexes.push_back(vector<RawFace>()); }
				n=0;
				for(vector<SimplexPointer>::iterator cp=tops.begin();cp!=tops.end();cp++)
				{
					/* Now, we consider if the current data structure is global or local! */
					if(ccw->encodeAllSimplices()==true)
					{
						/* Now, we extract the vertices of the current new top simplex (from a global data structure) */
						if(cp->type()!=0)
						{
							/* Now, we extract all the vertices of the current top simplex! */
							n=n+1;
							ccw->boundaryk( SimplexPointer(*cp),0,&bk);
							simplexes[cp->type()].push_back( RawFace());
							for(list<SimplexPointer>::iterator current=bk.begin();current!=bk.end();current++)
							{
								vs.insert(current->id());
								simplexes[cp->type()].back().getVertices().push_back(current->id());
							}
						}
					}
					else
					{
						/* Now, we extract the vertices of the current new top simplex (from a local data structure) */
						if(cp->type()!=0)
						{
							/* Now, we extract all the vertices of the current top simplex! */
							n=n+1;
							ccw->boundaryk(GhostSimplexPointer(cp->type(),cp->id()),0,&bk_loc);
							simplexes[cp->type()].push_back( RawFace());
							for(list<GhostSimplexPointer>::iterator current=bk_loc.begin();current!=bk_loc.end();current++)
							{
								ccw->ghostSimplex(GhostSimplexPointer(*current),&s);
								vs.insert(s.getCBoundary().at(0).id());
								simplexes[cp->type()].back().getVertices().push_back(s.getCBoundary().at(0).id());
							}
						} 
					}	
				}
				
				/* If we arrive here, we can write information about the vertices! */
				this->out<<string("nodes ")<<vs.size()<<endl;
				this->out.flush();
				vind=0;
				for(set<SIMPLEX_ID>::iterator it=vs.begin();it!=vs.end();it++)
				{
					/* Now, we collect the Euclidean coordinates for all the vertices! */				
					aux_xs.push_back(ccw->getPropertyValue( string(ec_name),SimplexPointer(0,(*it)),p).x());
					aux_ys.push_back(ccw->getPropertyValue( string(ec_name),SimplexPointer(0,(*it)),p).y());
					aux_zs.push_back(ccw->getPropertyValue( string(ec_name),SimplexPointer(0,(*it)),p).z());
					vett[ (*it) ] = vind;
					vind=vind+1;
				}
				
				/* Now, we write the Euclidean coordinates and then we proceed with all the top simplices! */
				this->out<<setprecision(9)<<aux_xs;
				this->out<<setprecision(9)<<aux_ys;
				this->out<<setprecision(9)<<aux_zs;
				this->out<<string("cells ")<<n<<endl;
				this->out.flush();
				for(unsigned int k=0;k<simplexes.size();k++)
				{
					/* Now, we write the description of all k-simplices! */
					for(vector<RawFace>::iterator it=simplexes[k].begin();it!=simplexes[k].end();it++)
					{
						if(it->getVertices().size()==2) { this->out<<string("line 2"); }
						if(it->getVertices().size()==3) { this->out<<string("tri 3"); }
						if(it->getVertices().size()==4) { this->out<<string("tet 4"); }
						for(vector<SIMPLEX_ID>::iterator vv=it->getVertices().begin();vv!=it->getVertices().end();vv++) this->out<<" "<<(vett[(*vv)]+1);
						this->out<<endl;
						this->out.flush();
					}
				}
				
				/* If we arrive here, we have finished! */
				this->out<<string("endgmv")<<endl;
				this->out.close();	
				bk.clear();
				bk_loc.clear();
				simplexes.clear();
				vs.clear();
				aux_xs.clear();
				aux_ys.clear();
				aux_zs.clear();
				vett.clear();
			}

			/// This member function exports a subcomplex in a file written in <i>GMV</i> format.
			/**
			 * This member function exports a subcomplex in a file written in <i>GMV</i> format. Such format is designed for the
			 * <i><A href="http://www.generalmeshviewer.com/">General Mesh Viewer</A></i>, a commercial software package for finite element analysis developed at the
			 * <i>Los Alamos Laboratories</i>, Los Alamos, USA.<p>This format is very complex and this class offers a limited support. For instance, supported files must satisfy these
			 *  guidelines:<p><i>gmvinput ascii<br>[ comments ] endcomm<br>nodes [ number of coordinates ]<br>[ list of the x coordinates ]<br>[ list of the y coordinates ]<br>
			 * [ list of the z coordinates ]<br>cells [ number of top simplices ] [ simplices description ]<br>endgmv</i><p>This format encodes a soup of top simplices, describing a
			 * simplicial 3-complex embedded in the 3D Euclidean space. Accepted descriptions are:<ul><li><i>line 2 [ 2 indices for the vertices ]</i> for describing an edge;</li>
			 * <li><i>tri 3 [ 3 indices for the vertices ]</i> for describing a triangle;</li><li><i>tet 4 [ 4 indices for the vertices ]</i> for describing a tetrahedron.</li></ul><p>We can
			 * manage only a simplicial 3-complex composed by vertices, lines, triangles, and tetrahedra.<p>Here, we receive as input a list of ghost simplices, i.e. simplices not directly
			 * encoded in the input data structure, that must be a <i>local</i> mangrove, i.e. encodes only a subset of simplices.<p>We assume Euclidean coordinates in the input simplicial
			 * complex are stored in a local property named as the string returned by the mangrove_tds::GMVManager::getPropertyName4Coordinates() member function.<p><b>IMPORTANT:</b> if we
			 * cannot complete this operation for any reason, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each
			 * forbidden operation will result in a failed assert.
			 * \param tops the simplices to be considered top in the new subcomplex to be written
			 * \param ccw the reference simplicial complex
			 * \param fname the path of the output file
			 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::GhostSimplexPointer, mangrove_tds::LocalPropertyHandle, mangrove_tds::RawFace, mangrove_tds::Point,
			 * mangrove_tds::SimplexPointerMap, mangrove_tds::SIMPLEX_ID,  mangrove_tds::GMVManager::getPropertyName4Coordinates(),
			 * mangrove_tds::GMVManager::supportsEuclideanCoordinates()
			 */
			template <class T> void writeSubcomplex( vector<GhostSimplexPointer> &tops, BaseSimplicialComplex<T> *ccw, string fname)
			{
				string ec_name;
				vector< vector<RawFace> > simplexes;
				unsigned int n;
				list<GhostSimplexPointer> bk_loc;
				GhostSimplex s;
				set<SIMPLEX_ID> vs;
				vector<double> aux_xs,aux_ys,aux_zs;
				SIMPLEX_ID vind;
				Point<double> p;
				map<SIMPLEX_ID,SIMPLEX_ID> vett;

				/* ===================================================================
				 * Repetita iuvant - supported GMV files:
			 	 *
		 		 * gmvinput ascii
		 		 * <comments> endcomm
		 		 * nodes <number of coordinates>
		 		 * [ list of the x coordinates ] [ list of the y coordinates ] [ list of the z coordinates ]
		 		 * cells <number of top simplicess>
				 * line 2 <indices> (optional)
		 		 * tri 3 <indices> (optional)
		 		 * tet 4 <indices> (optional)
		 		 * endgmv
		 		 * =================================================================== */
		 		assert(ccw!=NULL);
				assert(ccw->type()<=3);
				assert(ccw->encodeAllSimplices()==false);
				ec_name=string(this->getPropertyName4Coordinates());
				assert(ccw->hasProperty(string(ec_name)));
				assert(tops.size()!=0);
				if(this->out.is_open()) this->out.close();
				this->out.open(fname.c_str());
				assert(this->out.is_open());
				this->out<<string("gmvinput ascii")<<endl;
				this->out<<string(fname)<<" "<<string("- generated by the Mangrove TDS Library ");
				this->out<<string("endcomm")<<endl;
				for(SIMPLEX_ID k=0;k<=ccw->type();k++) { simplexes.push_back(vector<RawFace>()); }
				n=0;
				for(vector<GhostSimplexPointer>::iterator cp=tops.begin();cp!=tops.end();cp++)
				{
					/* Now, we extract the vertices of the current new ghost simplex (from a local data structure) */
					if(cp->type()!=0)
					{
						/* Now, we extract all the vertices of the current top simplex! */
						n=n+1;
						ccw->boundaryk(GhostSimplexPointer(*cp),0,&bk_loc);
						simplexes[cp->type()].push_back( RawFace());
						for(list<GhostSimplexPointer>::iterator current=bk_loc.begin();current!=bk_loc.end();current++)
						{
							ccw->ghostSimplex(GhostSimplexPointer(*current),&s);
							vs.insert(s.getCBoundary().at(0).id());
							simplexes[cp->type()].back().getVertices().push_back(s.getCBoundary().at(0).id());
						}
					}
				}
				
				/* If we arrive here, we can write information about the vertices! */
				this->out<<string("nodes ")<<vs.size()<<endl;
				this->out.flush();
				vind=0;
				for(set<SIMPLEX_ID>::iterator it=vs.begin();it!=vs.end();it++)
				{
					/* Now, we collect the Euclidean coordinates for all the vertices! */				
					aux_xs.push_back(ccw->getPropertyValue( string(ec_name),SimplexPointer(0,(*it)),p).x());
					aux_ys.push_back(ccw->getPropertyValue( string(ec_name),SimplexPointer(0,(*it)),p).y());
					aux_zs.push_back(ccw->getPropertyValue( string(ec_name),SimplexPointer(0,(*it)),p).z());
					vett[ (*it) ] = vind;
					vind=vind+1;
				}
				
				/* Now, we write the Euclidean coordinates and then we proceed with all the top simplices! */
				this->out<<setprecision(9)<<aux_xs;
				this->out<<setprecision(9)<<aux_ys;
				this->out<<setprecision(9)<<aux_zs;
				this->out<<string("cells ")<<n<<endl;
				this->out.flush();
				for(unsigned int k=0;k<simplexes.size();k++)
				{
					/* Now, we write the description of all k-simplices! */
					for(vector<RawFace>::iterator it=simplexes[k].begin();it!=simplexes[k].end();it++)
					{
						if(it->getVertices().size()==2) { this->out<<string("line 2"); }
						if(it->getVertices().size()==3) { this->out<<string("tri 3"); }
						if(it->getVertices().size()==4) { this->out<<string("tet 4"); }
						for(vector<SIMPLEX_ID>::iterator vv=it->getVertices().begin();vv!=it->getVertices().end();vv++) this->out<<" "<<(vett[(*vv)]+1);
						this->out<<endl;
						this->out.flush();
					}
				}

				/* If we arrive here, we have finished! */
				this->out<<string("endgmv")<<endl;
				this->out.close();
				bk_loc.clear();
				simplexes.clear();
				vs.clear();
				aux_xs.clear();
				aux_ys.clear();
				aux_zs.clear();
				vett.clear();
			}

			protected:
			
			/// The x-value coordinates for each vertex.
			vector<double> xs;
			
			/// The y-value coordinates for each vertex.
			vector<double> ys;
			
			/// The z-value coordinates for each vertex.
			vector<double> zs;
			
			/// This member function clears the internal state.
			/**
			 * This member function clears the internal state, by deallocating its member fields.
			 * \see mangrove_tds::RawFace, mangrove_tds::SimplicesContainer, mangrove_tds::MeshComponent
			 */
			inline void resetState()
			{
				/* We deallocate the internal state of the current instance */
				this->xs.clear();
				this->ys.clear();
				this->zs.clear();
				if(this->in.is_open()) this->in.close();
				if(this->out.is_open()) this->out.close();
				for(unsigned int k=0;k<this->raw_faces.size();k++) { this->raw_faces[k].clear(); }
				this->raw_faces.clear();
				this->components.clear();
				for(unsigned int k=0;k<this->simplices.size();k++) { this->simplices[k].clearCollection(); }
				this->simplices.clear();
			}
			
			/// This member function copies the content of components in the current simplicial complex.
			/**
			 * This member function copies the content of components in the current simplicial complex into a unique component: each component is described through an instance of the
			 * mangrove_tds::MeshComponent. In this way, we can generate an unique set of raw simplices (described by the mangrove_tds::RawFace class): this operation is mandatory for
			 * creating a new simplicial complex, described by a subclass of the mangrove_tds::BaseSimplicialComplex class.
			 * \param reqCoords this boolean flag identfies if the Euclidean coordinates (the unique auxiliary parameters in this component) loading is required
			 * \param gd this boolean flag indicates if the effective generation of raw simplices must be performed
			 * \see mangrove_tds::RawFace, mangrove_tds::BaseSimplicialComplex, mangrove_tds::MeshComponent, mangrove_tds::DataManager::generateRawSimplices(),
			 * mangrove_tds::DataManager::loadData()
			 */
			inline void copyComponents(bool reqCoords,bool gd)
			{
				SimplexPointer cp;
				map< SimplexPointer,SIMPLEX_ID, CompareSimplex> mapper;
				vector<unsigned int> orig,aux;
				unsigned int ne,n;
				list<SIMPLEX_ID> dest;
				list<SIMPLEX_ID>::iterator it;
				SIMPLEX_ID idt;				

				/* First, we map each vertex in every component! */
				for(unsigned int k=0;k<this->components.size();k++) 
				{
					/* We add all the vertices in the k-th component in order to create a unique set of vertices! */
					for(unsigned int j=0;j<this->components[k].getPoints().size();j++)
					{
						this->simplices[0].addSimplex(cp);
						mapper[SimplexPointer(k,j)]=cp.id();
						if(reqCoords)
						{
							this->xs.push_back(this->components[k].getPoints().at(j).cx());
							this->ys.push_back(this->components[k].getPoints().at(j).cy());
							this->zs.push_back(this->components[k].getPoints().at(j).cz());
						}
					}
				}
				
				/* Now, we create a unique list of top simplices, starting from the list of simplices in each component! */
				for(unsigned int k=0;k<this->components.size();k++) 
				{
					/* We analyze the list of simplices  in the current component! */
					for(unsigned int j=0;j<this->components[k].getSimplices().size();j++)
					{
						/* Now, we extract the current simplex, and we must map it over the global list of vertices */
						orig=vector<unsigned int>( this->components[k].getSimplices().at(j));
						dest.clear();
						for(unsigned int i=0;i<orig.size();i++)
						{
							idt=mapper[SimplexPointer(k,orig[i])];
							dest.push_back(idt);
						}

						/* Now, we sort 'dest', and try to add this new face! */
						dest.sort();
						ne=dest.size();
						if(ne>=2)
						{
							/* In this case, we have at least two vertices and thus we can add a new top simplex! */
							if(ne-1>=this->simplices.size())
							{
								/* Now, we must create a new container! */
								n = this->simplices.size();
								this->simplices.resize(ne);
								for(unsigned int z=n;z<ne;z++) this->simplices[z].updateType(z);
							}
							
							/* Now, we can create the new simplex and then we add the boundary vertices! */
							this->simplices[ne-1].addSimplex(cp);
							it=dest.begin();
							for(unsigned int z=0;z<dest.size();z++,it++) this->simplices[ne-1].simplex(cp.id()).add2Boundary(SimplexPointer(0,*it),z);
							if( (ne>2) && (gd==true))
							{
								aux.clear();
								for(it=dest.begin();it!=dest.end();it++) aux.push_back(*it);
								this->generateRawSimplices(aux,this->simplices[ne-1].size()-1,true);
							}
						}
					}
				}
			}
			
			/// This member function merges components in the current simplicial complex.
			/**
			 * This member function merges components in the current simplicial complex into a unique component: each component is described through an instance of the
			 * mangrove_tds::MeshComponent. In this way, we can generate an unique set of raw simplices (described by the mangrove_tds::RawFace class): this operation is mandatory for
			 * creating a new simplicial complex, described by a subclass of the mangrove_tds::BaseSimplicialComplex class.
			 * \param reqCoords this boolean flag identfies if the Euclidean coordinates (the unique auxiliary parameters in this component) loading is required
			 * \param gd this boolean flag indicates if the effective generation of raw simplices must be performed
			 * \see mangrove_tds::RawFace, mangrove_tds::BaseSimplicialComplex, mangrove_tds::MeshComponent, mangrove_tds::DataManager::generateRawSimplices(),
			 * mangrove_tds::DataManager::loadData()
			 */
			inline void mergeComponents(bool reqCoords,bool gd)
			{
				VERTICES_SET vs;
				SimplexPointer cp;
				VERTICES_MAPPER mapper;
				unsigned int ne,n;
				list<unsigned int> dest;
				vector<unsigned int> orig,aux;
				list<unsigned int>::iterator it;

				/* First, we must build a unique set of vertices starting from all the components! */
				for(unsigned int k=0;k<this->components.size();k++) { for(unsigned int j=0;j<this->components[k].getPoints().size();j++) vs.insert( this->components[k].getPoints().at(j)); }
				for(VERTICES_SET::iterator it=vs.begin();it!=vs.end();it++)
				{
					/* Now, we add all the vertices and we store their coordinates! */
					this->simplices[0].addSimplex(cp);
					mapper[ (*it) ] = cp.id();
					if(reqCoords)
					{
						this->xs.push_back(it->cx());
						this->ys.push_back(it->cy());
						this->zs.push_back(it->cz());
					}
				}
				
				/* Now, we must build a unique set of simplices: first, we map them into the global list of vertices! */
				for(unsigned int k=0;k<this->components.size();k++)
				{
					/* Now, we must map all the simplices in the k-th component over the global list of vertices */
					for(unsigned int j=0;j<this->components[k].getSimplices().size();j++)
					{
						/* Now, we extract the current simplex and we must map it over the global list of vertices */
						orig.clear();
						dest.clear();
						orig = vector<unsigned int>( this->components[k].getSimplices().at(j));
						for(unsigned int i=0;i<orig.size();i++) { dest.push_back(mapper[this->components[k].getPoints().at(orig[i])]); }
						dest.sort();
						dest.unique();
						ne = dest.size();
						if(ne>=2)
						{
							/* In this case, we have at least two vertices and thus we can add a new top simplex */
							if(ne-1>=this->simplices.size())
							{
								/* Now, we must create a new container! */
								n = this->simplices.size();
								this->simplices.resize(ne);
								for(unsigned int z=n;z<ne;z++) this->simplices[z].updateType(z);
							}
							
							/* Now, we can create the new simplex and then we add the boundary vertices! */
							this->simplices[ne-1].addSimplex(cp);
							it=dest.begin();
							for(unsigned int z=0;z<dest.size();z++,it++) this->simplices[ne-1].simplex(cp.id()).add2Boundary(SimplexPointer(0,*it),z);
							if( (ne>2) && (gd==true))
							{
								aux.clear();
								for(it=dest.begin();it!=dest.end();it++) aux.push_back(*it);
								this->generateRawSimplices(aux,this->simplices[ne-1].size()-1,true);
							}
						}
					}
				}
			}
			
			/// This member function exports a simplicial complex in a file written in the <i>GMV</i> format.
			/**
			 * This member function exports a simplicial 3-complex described by a data structure encoding all simplices (namely a <i>global</i> data structure). This member function writes a
			 * representation of the input simplicial 3-complex in accordance with the <i>GMV</i> format: it is designed for the
			 * <i><A href="http://www.generalmeshviewer.com/">General Mesh Viewer</A></i>, a commercial software package for finite element analysis developed at the
			 * <i>Los Alamos Laboratories</i>, Los Alamos, USA.<p>This format is very complex and this class offers a limited support. For instance, supported files must satisfy these
			 * guidelines:<p><i>gmvinput ascii<br>[ comments ] endcomm<br>nodes [ number of coordinates ]<br>[ list of the x coordinates ]<br>[ list of the y coordinates ]<br>
			 * [ list of the z coordinates ]<br>cells [ number of top simplices ] [ simplices description ]<br>endgmv</i><p>This format encodes a soup of top simplices, describing a
			 * simplicial 3-complex embedded in the 3D Euclidean space. Accepted descriptions are:<ul><li><i>line 2 [ 2 indices for the vertices ]</i> for describing an edge;</li>
			 * <li><i>tri 3 [ 3 indices for the vertices ]</i> for describing a triangle;</li><li><i>tet 4 [ 4 indices for the vertices ]</i> for describing a tetrahedron.</li></ul>We assume
			 * Euclidean coordinates in the input simplicial complex are stored in a local property named as the string returned by the
			 * mangrove_tds::GMVManager::getPropertyName4Coordinates() member function. Moreover, we write only the valid simplices, i.e. simplices with real locations and not marked as
			 * <i>deleted</i> by the garbage collector mechanism.<p>If we cannot complete this operation for any reason, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param ccw the input global simplicial 3-complex to be represented
			 * \param fname the path of the output file
			 * \see mangrove_tds::SimplexPointerMap, mangrove_tds::BaseSimplicialComplex, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::LocalPropertyHandle,
			 * mangrove_tds::GMVManager::getPropertyName4Coordinates(), mangrove_tds::GMVManager::supportsEuclideanCoordinates(), mangrove_tds::GMVManager::writeLocalComplex()
			 */
			template <class T> void writeGlobalComplex(BaseSimplicialComplex<T>*ccw,string fname)
			{
				string ec_name;
				SimplexIterator it;
				Point<double> p;
				SimplexPointerMap<T> map;
				list<SimplexPointer> tops1,tops2,tops3,bk;
				vector<double> aux_xs,aux_ys,aux_zs;
				
				/* ===================================================================
				 * Repetita iuvant - supported GMV files:
			 	 *
		 		 * gmvinput ascii
		 		 * <comments> endcomm
		 		 * nodes <number of coordinates>
		 		 * [ list of the x coordinates ] [ list of the y coordinates ] [ list of the z coordinates ]
		 		 * cells <number of top simplicess>
				 * line 2 <indices> (optional)
		 		 * tri 3 <indices> (optional)
		 		 * tet 4 <indices> (optional)
		 		 * endgmv
		 		 * =================================================================== */
		 		ec_name=string(this->getPropertyName4Coordinates());
				assert(ccw->hasProperty(string(ec_name)));
				map.setComplex(ccw);
				map.forwardMapping();
				if(this->out.is_open()) this->out.close();
				this->out.open(fname.c_str());
				assert(this->out.is_open());
				this->out<<string("gmvinput ascii")<<endl;
				this->out<<string(fname)<<" "<<string("- generated by the Mangrove TDS Library ");
				this->out<<string("endcomm")<<endl;
				this->out<<string("nodes ")<<ccw->size(0)<<endl;	
				for(it=ccw->begin(0);it!=ccw->end(0);it++)
				{
					aux_xs.push_back(ccw->getPropertyValue( string(ec_name),SimplexPointer(it.getPointer()),p).x());
					aux_ys.push_back(ccw->getPropertyValue( string(ec_name),SimplexPointer(it.getPointer()),p).y());
					aux_zs.push_back(ccw->getPropertyValue( string(ec_name),SimplexPointer(it.getPointer()),p).z());
				}
				
				/* Now, we write the Euclidean coordinates and then we proceed with all the top simplices! */
				this->out<<setprecision(9)<<aux_xs;
				this->out<<setprecision(9)<<aux_ys;
				this->out<<setprecision(9)<<aux_zs;
				tops1.clear();
				tops2.clear();
				tops3.clear();
				if(ccw->type()>=1) ccw->getTopSimplices(1,tops1); 
				if(ccw->type()>=2) ccw->getTopSimplices(2,tops2);
				if(ccw->type()==3) ccw->getTopSimplices(3,tops3);
				this->out<<string("cells ")<<(tops1.size()+tops2.size()+tops3.size())<<endl;
				this->out.flush();
				for(list<SimplexPointer>::iterator cp=tops1.begin();cp!=tops1.end();cp++)
				{
					/* Now, we write a new top 1-simplex! */
					ccw->boundaryk( SimplexPointer(*cp),0,&bk);
					this->out<<string("line 2");
					for(list<SimplexPointer>::iterator current=bk.begin();current!=bk.end();current++) this->out<<" "<<(map.getId(SimplexPointer(*current))+1);
					this->out<<endl;
					this->out.flush();
				}
				
				/* Now, we write the top 2-simplices, if they exist! */
				for(list<SimplexPointer>::iterator cp=tops2.begin();cp!=tops2.end();cp++)
				{
					/* Now, we write a new top 2-simplex! */
					ccw->boundaryk( SimplexPointer(*cp),0,&bk);
					this->out<<string("tri 3");
					for(list<SimplexPointer>::iterator current=bk.begin();current!=bk.end();current++) this->out<<" "<<(map.getId(SimplexPointer(*current))+1);
					this->out<<endl;
					this->out.flush();
				}
				
				/* Now, we write the top 3-simplices, if they exist! */
				for(list<SimplexPointer>::iterator cp=tops3.begin();cp!=tops3.end();cp++)
				{
					/* Now, we write a new top 3-simplex! */
					ccw->boundaryk( SimplexPointer(*cp),0,&bk);
					this->out<<string("tet 4");
					for(list<SimplexPointer>::iterator current=bk.begin();current!=bk.end();current++) this->out<<" "<<(map.getId(SimplexPointer(*current))+1);
					this->out<<endl;
					this->out.flush();
				}
				
				/* If we arrive here, we have finished! */
				this->out<<string("endgmv")<<endl;
				this->out.close();	
				aux_xs.clear();
				aux_ys.clear();
				aux_zs.clear();	
				tops1.clear();
				tops2.clear();
				tops3.clear();
				bk.clear();
			}
			
			/// This member function exports a simplicial complex in a file written in the <i>GMV</i> format.
			/**
			 * This member function exports a simplicial 3-complex described by a data structure encoding only a subset of all simplices (namely a <i>local</i> data structure). This member
			 * function writes a representation of the input simplicial 3-complex in accordance with the <i>GMV</i> format: it is designed for the
			 * <i><A href="http://www.generalmeshviewer.com/">General Mesh Viewer</A></i>, a commercial software package for finite element analysis developed at the
			 * <i>Los Alamos Laboratories</i>, Los Alamos, USA.<p>This format is very complex and this class offers a limited support. For instance, supported files must satisfy these
			 * guidelines:<p><i>gmvinput ascii<br>[ comments ] endcomm<br>nodes [ number of coordinates ]<br>[ list of the x coordinates ]<br>[ list of the y coordinates ]<br>
			 * [ list of the z coordinates ]<br>cells [ number of top simplices ] [ simplices description ]<br>endgmv</i><p>This format encodes a soup of top simplices, describing a
			 * simplicial 3-complex embedded in the 3D Euclidean space. Accepted descriptions are:<ul><li><i>line 2 [ 2 indices for the vertices ]</i> for describing an edge;</li>
			 * <li><i>tri 3 [ 3 indices for the vertices ]</i> for describing a triangle;</li><li><i>tet 4 [ 4 indices for the vertices ]</i> for describing a tetrahedron.</li></ul>We assume
			 * Euclidean coordinates in the input simplicial complex are stored in a local property named as the string returned by the
			 * mangrove_tds::GMVManager::getPropertyName4Coordinates() member function. Moreover, we write only the valid simplices, i.e. simplices with real locations and not marked as
			 * <i>deleted</i> by the garbage collector mechanism.<p>If we cannot complete this operation for any reason, then this member function will fail if and only if the
			 * <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.
			 * \param ccw the input global simplicial 3-complex to be represented
			 * \param fname the path of the output file
			 * \see mangrove_tds::SimplexPointerMap, mangrove_tds::BaseSimplicialComplex, mangrove_tds::BaseSimplicialComplex::encodeAllSimplices(), mangrove_tds::LocalPropertyHandle,
			 * mangrove_tds::GMVManager::getPropertyName4Coordinates(), mangrove_tds::GMVManager::supportsEuclideanCoordinates(), mangrove_tds::GMVManager::writeGlobalComplex(),
			 * mangrove_tds::GhostSimplex, mangrove_tds::GhostSimplexPointer
			 */
			template <class T> void writeLocalComplex(BaseSimplicialComplex<T>*ccw,string fname)
			{
				GhostSimplex s;
				unsigned int k;
				SimplexIterator it;
				string ec_name;
				Point<double> p;
				SimplexPointerMap<T> map;
				list<SimplexPointer> tops1,tops2,tops3;
				vector<double> aux_xs,aux_ys,aux_zs;
				
				/* ===================================================================
				 * Repetita iuvant - supported GMV files:
			 	 *
		 		 * gmvinput ascii
		 		 * <comments> endcomm
		 		 * nodes <number of coordinates>
		 		 * [ list of the x coordinates ] [ list of the y coordinates ] [ list of the z coordinates ]
		 		 * cells <number of top simplicess>
				 * line 2 <indices> (optional)
		 		 * tri 3 <indices> (optional)
		 		 * tet 4 <indices> (optional)
		 		 * endgmv
		 		 * =================================================================== */
		 		ec_name=string(this->getPropertyName4Coordinates());
		 		assert(ccw->hasProperty(string(ec_name)));
				map.setComplex(ccw);
				map.forwardMapping();
				if(this->out.is_open()) this->out.close();
				this->out.open(fname.c_str());
				assert(this->out.is_open());
				this->out<<string("gmvinput ascii")<<endl;
				this->out<<string(fname)<<" "<<string("- generated by the Mangrove TDS Library ");
				this->out<<string("endcomm")<<endl;
				this->out<<string("nodes ")<<ccw->size(0)<<endl;	
				for(it=ccw->begin(0);it!=ccw->end(0);it++)
				{
					aux_xs.push_back(ccw->getPropertyValue( string(ec_name),SimplexPointer(it.getPointer()),p).x());
					aux_ys.push_back(ccw->getPropertyValue( string(ec_name),SimplexPointer(it.getPointer()),p).y());
					aux_zs.push_back(ccw->getPropertyValue( string(ec_name),SimplexPointer(it.getPointer()),p).z());
				}
				
				/* Now, we write the Euclidean coordinates and then we proceed with all the top simplices! */
				this->out<<setprecision(9)<<aux_xs;
				this->out<<setprecision(9)<<aux_ys;
				this->out<<setprecision(9)<<aux_zs;
				if(ccw->type()>=1) ccw->getTopSimplices(1,tops1); 
				if(ccw->type()>=2) ccw->getTopSimplices(2,tops2);
				if(ccw->type()==3) ccw->getTopSimplices(3,tops3);
				this->out<<string("cells ")<<(tops1.size()+tops2.size()+tops3.size())<<endl;
				this->out.flush();
				for(list<SimplexPointer>::iterator cp=tops1.begin();cp!=tops1.end();cp++)
				{
					/* Now, we write a new top 1-simplex! */
					this->out<<string("line 2");
					ccw->ghostSimplex(GhostSimplexPointer(cp->type(),cp->id()),&s);
					for(k=0;k<=cp->type();k++) this->out<<" "<<(map.getId(SimplexPointer(s.getBoundary()[k]))+1);
					this->out<<endl;
					this->out.flush();
				}
				
				/* Now, we write the top 2-simplices, if they exist! */
				for(list<SimplexPointer>::iterator cp=tops2.begin();cp!=tops2.end();cp++)
				{
					/* Now, we write a new top 2-simplex! */
					this->out<<string("tri 3");
					ccw->ghostSimplex(GhostSimplexPointer(cp->type(),cp->id()),&s);
					for(k=0;k<=cp->type();k++) this->out<<" "<<(map.getId(SimplexPointer(s.getBoundary()[k]))+1);
					this->out<<endl;
					this->out.flush();
				}
				
				/* Now, we write the top 3-simplices, if they exist! */
				for(list<SimplexPointer>::iterator cp=tops3.begin();cp!=tops3.end();cp++)
				{
					/* Now, we write a new top 3-simplex! */
					this->out<<string("tet 4");
					ccw->ghostSimplex(GhostSimplexPointer(cp->type(),cp->id()),&s);
					for(k=0;k<=cp->type();k++) this->out<<" "<<(map.getId(SimplexPointer(s.getBoundary()[k]))+1);
					this->out<<endl;
					this->out.flush();
				}
				
				/* If we arrive here, we have finished! */
				this->out<<string("endgmv")<<endl;
				this->out.close();	
				aux_xs.clear();
				aux_ys.clear();
				aux_zs.clear();	
				tops1.clear();
				tops2.clear();
				tops3.clear();
			}
		};
	}

#endif

