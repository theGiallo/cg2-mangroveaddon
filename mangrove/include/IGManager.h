/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library
 *                                                                           				    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * October 2011 (Revised on May 2012)
 *                                                                    
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * IGManager.h -  the specialized component for executing I/O operations on the Incidence Graph.
 ***********************************************************************************************/

/* Should we include this header file? */
#ifndef IG_MANAGER_H

	#define IG_MANAGER_H
	
	#include "IO.h"
	#include "IG.h"
	#include "Miscellanea.h"
	#include <cstdlib>
	#include <algorithm>
	#include <vector>
	using namespace std;
	using namespace mangrove_tds;
	
	/// The specialized component for executing I/O operations on the <i>Incidence Graph</i>.
	/**
	 * The class defined in this file allows to execute I/O operations on the <i>Incidence Graph</i>.<p>The <i>Incidence Graph</i>, described by the mangrove_tds::IG class, is a dimension-independent
	 * and incidence-based data structure for representing simplicial complexes with an arbitrary domain. It encodes all the simplices in a simplicial complex, thus it is a <i>global</i> data
	 * structure. For each k-simplex it encodes:<ul><li>all the k+1 simplices of dimension k-1 belonging to its boundary;</li><li>all the simplices of dimension k+1 belonging to its
	 * coboundary.</li></ul>The effective I/O operations on the <i>Incidence Graph</i> are delegated to a particular instance of the mangrove_tds::DataManager class in order to support a
	 * wide range of I/O formats.
	 * \file IGManager.h
	 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
	 */
	
	/* All the following source belongs to the 'mangrove_tds' namespace */
	namespace mangrove_tds
	{
		/// The specialized component for executing I/O operations on the <i>Incidence Graph</i>.
		/**
	 	 * This class allows to execute I/O operations on the <i>Incidence Graph</i>.<p>The <i>Incidence Graph</i> data structure, described by the mangrove_tds::IG class, is a
		 * dimension-independent and incidence-based data structure for representing simplicial complexes with an arbitrary domain. It encodes all the simplices in a simplicial complex. Thus, it
		 * is a <i>global</i> data structure. For each k-simplex it encodes:<ul><li>all the k+1 simplices of dimension k-1 belonging to its boundary;</li><li>all the simplices of dimension k+1
	 	 * belonging to its coboundary.</li></ul>The effective I/O operations on the <i>Incidence Graph</i> are delegated to a particular instance of the mangrove_tds::DataManager class in order
		 * to support a wide range of I/O formats.
	 	 * \see mangrove_tds::DataManager, mangrove_tds::IO, mangrove_tds::IG
	 	 */
	 	class IGManager : public IO<IGManager>
		{
			public:
			
			/// This member function creates a new instance of this class.
			/**
			 * This member function creates a not initialized instance of this class: you should apply the mangrove_tds::IGManager::createComplex() member functions in order to initialize it.
			 * \see mangrove_tds::IGManager::createComplex(), mangrove_tds::IG
			 */
			inline IGManager() : IO<IGManager>() { 	this->ccw = NULL; }
			
			/// This member function destroys an instance of this class.
			/**
			 * The specialized data structure for the simplicial complex is created in the mangrove_tds::IGManager::createComplex() member function, thus you must not manually destroy it: you
			 * should apply this destructor or the mangrove_tds::IGManager::close() member function in order to destroy all the internal state.
			 * \see mangrove_tds::IGManager::createComplex(), mangrove_tds::IGManager::close()
			 */
			inline virtual ~IGManager() { this->close(); }
			
			/// This member function destroys all the internal state in this component.
			/**
			 * The specialized data structure for the simplicial complex is created in the mangrove_tds::IGManager::createComplex() member function, thus you must not manually destroy it: you
			 * should apply the destructor or this member function in order to destroy all the internal state.
			 * \see mangrove_tds::IGManager::createComplex(), mangrove_tds::IG, mangrove_tds::BaseSimplicialComplex
			 */
			inline void close()
			{
				/* Now, we reset the current state! */
				if(this->ccw!=NULL) delete this->ccw;
				this->ccw=NULL;
			}
			
			/// This member function returns the simplicial complex created by the current component.
			/**
			 * This member function returns the simplicial complex created in the mangrove_tds::IGManager::createComplex() member function. The current <i>Incidence Graph</i> has been
			 * created in the mangrove_tds::IGManager::createComplex() member function, thus you must not manually destroy it. You should apply the destructor or the
			 * mangrove_tds::IGManager::close() member function in order to destroy all the internal state. Thus, the input pointer must be a C++ pointer to an instance of the mangrove_tds::IG
			 * class.<p>If such conditions are not satisfied, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each
			 * forbidden operation will result in a failed assert.
			 * \param cs the simplicial complex created by this component
			 * \see mangrove_tds::IGManager::close(), mangrove_tds::IGManager::createComplex(), mangrove_tds::IG, mangrove_tds::BaseSimplicialComplex
			 */
			template <class D> void getComplex(D** cs)
			{
				D *aux;
				
				/* First, we check if all is ok! */
				assert(this->ccw!=NULL);
				aux = dynamic_cast<D*>(this->ccw);
				assert(aux!=NULL);
				(*cs) = this->ccw;
			}
			
			/// This member function returns a string that describes the data structure created by the current I/O component.
 			/**
 			 * This member function returns a string that describes the data structure created by the current I/O component, and used for encoding the current simplicial complex.
			 * \return a string that describes the data structure created by the current I/O component
 			 * \see mangrove_tds::BaseSimplicialComplex::getDataStructureName()
 			 */
 			inline string getDataStructureName() { return string("Incidence Graph (IG)"); }
 			
 			/// This member function creates a simplicial complex by reading it from a file.
			/**
			 * This member function generates an <i>Incidence Graph</i> from a file that contains a possible representation: the most common exchange format for a simplicial complex consists
			 * of a collection of <i>top</i> simplices described by their vertices, but it is not the unique possibility.<p>The <i>Incidence Graph</i>, described by the mangrove_tds::IG
			 * class, is a dimension-independent and incidence-based data structure for representing simplicial complexes with an arbitrary domain and it encodes all the simplices in a
			 * simplicial complex. Thus, it is a <i>global</i> data structure. For each k-simplex it encodes:<ul><li>all the k+1 simplices of dimension k-1 belonging to its boundary;</li>
			 * <li>all the simplices of dimension k+1 belonging to its coboundary.</li></ul>The effective reading is delegated to a particular instance of the mangrove_tds::DataManager in
			 * order to support a wide range of file formats.<p>Thus, this member function starts from the raw data and constructs the complete <i>Incidence Graph</i>: if the input contains a
			 * soup of top simplices, then we need to generate all the simplices and then to establish all the topological relations among them.<p>In this member function we can customize the
			 * required simplicial complex: for instance, we can attach the Euclidean coordinates and the field value for each vertex in the simplicial complex to be built (if supported in
			 * the I/O component). Such information is stored as local properties (i.e. as subclasses of the mangrove_tds::LocalPropertyHandle class) associated to vertices. You can retrieve
			 * them through their names, respectively provided by the mangrove_tds::DataManager::getPropertyName4Coordinates() and the mangrove_tds::DataManager::getPropertyName4FieldValue()
			 * member functions. Moreover, you can check what properties are supported through the mangrove_tds::DataManager::supportsEuclideanCoordinates() and
			 * mangrove_tds::DataManager::supportsFieldValues() member functions.<p>If we cannot complete these operation for any reason, then this member function will fail if and only if
			 * the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden operation will result in a failed assert.<p><b>IMPORTANT:</b> the specialized data
			 * structure for the simplicial complex is created in this member function, thus you must not manually destroy it. You should apply the destructor or
			 * mangrove_tds::IGManager::close() member function in order to destroy all the internal state.
	 	 	 * \param loader a component for the effective data reading
	 	 	 * \param fname the path of the input file
			 * \param reqParams this boolean flag identfies if the loading of the auxiliary parameters encoded in the input file is enabled
			 * \param ld this boolean flag indicates if the effective reading of the input data must be performed
			 * \param checksOn this boolean flag indicates if the effective checks on the input data must be performed (valid only if the effective reading must be performed)
			 * \see mangrove_tds::DataManager, mangrove_tds::IGManager::close(), mangrove_tds::BaseSimplicialComplex, mangrove_tds::LocalPropertyHandle, mangrove_tds::RawFace,
			 * mangrove_tds::IG
			 */
			template <class T> void createComplex(DataManager<T> *loader, string fname,bool reqParams,bool ld,bool checksOn)
			{
				bool b;
				double aaa;
				LocalPropertyHandle< Point<double> > *pnts_prop;
				LocalPropertyHandle<double> *fvals_prop;
				SimplexIterator it;
				SimplexPointer cp;
				unsigned int k,uik,rik,ik;
				Point<double> p;

				/* First, we check if all is ok and then we run the effective construction! */
				assert(loader!=NULL);
				this->close();
				if(ld) loader->loadData(fname,reqParams,true,checksOn);
				b=(loader->supportsEuclideanCoordinates()==true) || (loader->supportsFieldValues()==true);
				if(reqParams) assert(b==true);
				try { this->ccw = new IG(loader->getDimension()); }
				catch(bad_alloc ba) { this->ccw = NULL; }
				assert(this->ccw!=NULL);
				aaa=0.0;
				pnts_prop=NULL;
				fvals_prop=NULL;
				if(reqParams)
				{
					/* What properties must be added? First, we check the Euclidean coordinates */
					if(loader->supportsEuclideanCoordinates())
					{
						pnts_prop=this->ccw->addLocalProperty(0,string(loader->getPropertyName4Coordinates()), Point<double>());
						this->ccw->getPropertyName4Coordinates()=string(loader->getPropertyName4Coordinates());
					}
					
					/* Now, we check the field values! */
					if(loader->supportsFieldValues())
					{
						fvals_prop=this->ccw->addLocalProperty(0,string(loader->getPropertyName4FieldValue()),aaa); 
						this->ccw->getPropertyName4FieldValue()=string(loader->getPropertyName4FieldValue());
					}
				}
				
				/* Now, we can add all vertices and their properties! */
				it = loader->getCollection(0).begin();
				k=0;
				while(it!=loader->getCollection(0).end())
				{
					/* Now, we insert the k-th vertex in 'ccw' */
					this->ccw->addSimplex(cp);
					if(reqParams)
					{
						/* Now, we retrieve the Euclidean coordinates, if supported! */						
						if(loader->supportsEuclideanCoordinates())
						{
							loader->getCoordinates(k,p);
							pnts_prop->set(SimplexPointer(cp),Point<double>(p));
						}
						
						/* Now, we retrieve the field values, if supported! */
						if(loader->supportsFieldValues())
						{
							loader->getFieldValue(k,aaa);
							fvals_prop->set(SimplexPointer(cp),aaa);
						}
					}
					
					/* Now, we move on the next vertex! */
					k=k+1;
					it++;
				}
				
				/* Now, we must eliminate and create the subsimplices of the top simplices. Then, we must understand how connecting the top simplices! */
				if(loader->getDimension()==0) return;
				for(SIMPLEX_TYPE d=1;d<loader->getRawFaces().size();d++) { this->getUnique(d,loader); }
				for(it=loader->getCollection(1).begin(); it!=loader->getCollection(1).end();it++) { this->addEdge((*it).bc(0).id(),(*it).bc(1).id()); }
				for(SIMPLEX_TYPE d=loader->getDimension();d>=2;d--)
				{
					/* First, we check if there are any top simplices of dimension d */
					if( loader->getCollection(d).size()!=0)
					{
						/* If we arrive here, there are certainly simplices of dimension d-1 that are on these simplices boundary! */
						for(k=0;k<loader->getRawFaces()[d-1].size();k++)
						{
							/* Now, we check if the k-th simplex is a child of a top simplex! */
							if(loader->getRawFaces()[d-1].at(k).isTopSimplexChild())
							{
								uik = loader->getRawFaces()[d-1].at(k).getUniqueIndex();
								ik = loader->getRawFaces()[d-1].at(k).getIndex();
								rik = loader->getRawFaces()[d-1].at(k).getReverseIndex();
								loader->getCollection(d).simplex(ik).b(rik).setId(uik);
								loader->getCollection(d).simplex(ik).b(rik).setType(d-1);								
							}
						}
						
						/* Now, we add all the top simplices of dimension d to ccw */
						for(it=loader->getCollection(d).begin();it!=loader->getCollection(d).end();it++)
						{
							/* Now, we create a new simplex with its boundary! */
							this->ccw->addSimplex(d,cp);
							for(unsigned int l=0;l<=d;l++) 
							{ 
								this->ccw->simplex(cp).b(l).setId((*it).b(l).id());
								this->ccw->simplex(cp).b(l).setType((*it).b(l).type());
							}
						}
					}
				}
				
				/* If we arrive here, we can complete the coboundary! */
				this->ccw->buildCoboundaryFromBoundary();
			}

			/// This member function exports the current simplicial complex in accordance with a certain format.
			/**
			 * This member function exports the current simplicial complex, by writing its representation in accordance with a specific file format.<p>The effective writing is delegated to a
			 * subclass of the mangrove_tds::DataManager class and some further conditions could be checked and verified: for example, the current simplicial complex must support the
			 * extraction of particular auxiliary information (like the Euclidean coordinates and/or scalar field values, described by subclasses of the mangrove_tds::PropertyBase class). If
			 * such properties are not satisfied, then this member function will fail if and only if the <i>Mangrove TDS Library</i> is compiled in debug mode. In this case, each forbidden
			 * operation will result in a failed assert.
			 * \param writer a component for the effective data writing (we write the data in this member function)
			 * \param fname the path of the output file
			 * \see mangrove_tds::DataManager, mangrove_tds::IO::close(), mangrove_tds::BaseSimplicialComplex, mangrove_tds::PropertyBase
			 */
			template <class T> void writeComplex(DataManager<T> *writer,string fname)
			{
				/* First, we check if all is ok! */
				assert(writer!=NULL);
				writer->writeData(this->ccw,fname);
			}
			
			protected:

			/// The simplicial complex created by this component.
			/**
			 * \see mangrove_tds::IG
			 */
			IG* ccw;

			/// This member functions add a new edge to the current simplicial complex.
			/**
			 * This member functions add a new edge to the current data structure: an edge is completely defined by its two vertices, that we assume already stored in the current data
			 * structure.
			 * \param v0 the identifier of the first vertex for the new edge to be added to the current simplicial complex
			 * \param v1 the identifier of the second vertex for the new edge to be added to the current simplicial complex
			 * \return the identifier of the new edge added to the current simplicial complex
			 * \see mangrove_tds::SIMPLEX_ID, mangrove_tds::IGManager::getUnique(), mangrove_tds::SimplexPointer, mangrove_tds::IG, mangrove_tds::BaseSimplicialComplex
			 */
			inline SIMPLEX_ID addEdge(SIMPLEX_ID v0,SIMPLEX_ID v1)
			{
				SimplexPointer cp;
			
				/* First, we add a new edge and then return its new identifier! */
				this->ccw->addSimplex(SimplexPointer(0,v0),SimplexPointer(0,v1),cp);
				return cp.id();
			}
			
			/// This member function generates all the unique simplices in the current simplicial complex.
			/**
			 * This member function generates all the unique simplices in the current simplicial complex: we start our analysis from the raw subsimplices of all the top simplices in the I/O
			 * component. We assume to retrieve all top simplices through the mangrove_tds::DataManager::getCollection() member function and to retrieve all the raw simplices (described
			 * through the mangrove_tds::RawFace class) through the mangrove_tds::DataManager::getRawFaces() member function.<p>In this algorithm, we generate all the unique simplices,
			 * by sorting all the raw simplices: in this way, we can also rebuild the boundary hierarchy between them.
			 * \param d the dimension of the unique simplices to be generated
			 * \param loader the component that contains the raw and top simplices describing the input simplicial complex
			 * \see mangrove_tds::IGManager::addEdge(), mangrove_tds::SIMPLEX_TYPE, mangrove_tds::RawFace, mangrove_tds::DataManager::getCollection(),
			 * mangrove_tds::SimplicesContainer, mangrove_tds::DataManager::getRawFaces(), mangrove_tds::Simplex
			 */
			template <class T> void getUnique(SIMPLEX_TYPE d,DataManager<T> *loader)
			{
				vector<RawFace> auxd;
				unsigned int r,c;
				SIMPLEX_ID v0,v1,indr,t;
				vector<unsigned int> cs;
				vector<SimplexPointer> f;
				SimplexPointer cp;
				
				/* First, we copy all the raw d-simplices in order to mantain the correspondence among them */
				for(unsigned int k=0;k<loader->getRawFaces()[d].size();k++)
				{
					loader->getRawFaces()[d][k].getOriginalPosition() = k;
					auxd.push_back(RawFace(loader->getRawFaces()[d].at(k)));	
				}
				
				/* Now, we sort the copy 'auxd' in order to remove duplicates (replicants). */				
				sort( auxd.begin(),auxd.end(),compare_faces);
				r=0;
				c=1;
				while( (r<auxd.size()) && (c<auxd.size()))
				{
					/* Now, we must identify connected components of faces! */
					if(same_faces(auxd[r],auxd[c]))
					{
						/* We accumulate replicants! */
						cs.push_back(c);
						c=c+1;
					}
					else
					{
						/* Now, we must finalize the identification of a new simplex */
						if(d==1)
						{
							/* Now, we must add an edge! */
							v0 = auxd[r].getVertices().at(0);
							v1 = auxd[r].getVertices().at(1);
							indr = this->addEdge(v0,v1);
						}
						else
						{
							/* Now, we must rebuild the boundary of the new simplex to be added */
							f.clear();
							for(unsigned int j=0;j<=d;j++)
							{
								t=auxd[r].getBoundary()[j];
								f.push_back( SimplexPointer(d-1, loader->getRawFaces()[d-1][t].getUniqueIndex()));
							}
							
							/* Now, we add the new simplex. */
							this->ccw->addSimplex(f,cp);
							indr = cp.id();
						}
						
						/* Now, we must complete the construction of the new simplex and then we prepare the creation of a new cluster of elements! */
						loader->getRawFaces()[d][auxd[r].getOriginalPosition()].getUniqueIndex() = indr;
						auxd[r].getUniqueIndex() = indr;
						for(unsigned int l=0;l<cs.size();l++) { loader->getRawFaces()[d][auxd[cs[l]].getOriginalPosition()].getUniqueIndex() = indr; }
						r=c;
						c=r+1;
						cs.clear();
					}
				}
				
				/* Now, we must complete the construction, finalizing the last cluster! */
				if(r<auxd.size())
				{
					/* Now, we must finalize the identification of a new simplex! */
					if(d==1)
					{
						/* Now, we must add an edge! */
						v0 = auxd[r].getVertices().at(0);
						v1 = auxd[r].getVertices().at(1);
						indr = this->addEdge(v0,v1);
					}
					else
					{
						/* Now, we must rebuild the boundary of the new simplex to be added */
						f.clear();
						for(unsigned int j=0;j<=d;j++)
						{
							t=auxd[r].getBoundary()[j];
							f.push_back( SimplexPointer(d-1, loader->getRawFaces()[d-1][t].getUniqueIndex()));
						}
							
						/* Now, we add the new simplex. */
						this->ccw->addSimplex(f,cp);
						indr = cp.id();
					}
					
					/* Now, we must complete the construction of the new simplex! */
					auxd[r].getUniqueIndex() = indr;
					loader->getRawFaces()[d][auxd[r].getOriginalPosition()].getUniqueIndex() = indr;
					for(unsigned int l=0;l<cs.size();l++)
					{
						auxd[cs[l]].getUniqueIndex() = indr;
						loader->getRawFaces()[d][auxd[cs[l]].getOriginalPosition()].getUniqueIndex() = indr;
					}
				}
			}
		};
	}

#endif

