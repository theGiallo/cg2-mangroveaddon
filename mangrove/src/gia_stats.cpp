/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library
 * 
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * October 2011 (Revised on May 2012)		    
 *                                                                    
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * gia_stats.cpp - a program for testing the IA* data structure
 ***********************************************************************************************/

#include <QString>
#include <QTime>
#include <QFileInfo> 
#include <cstdlib>
#include <iostream>
#include <string>
#include "IO_files.h"
#include "GIAManager.h"
using namespace std;
using namespace mangrove_tds;

/// The main component for the <i>gia_stats</i> program.
/**
 * The functions defined in this file compose the main component for the <i>gia_stats</i> program, a program for testing the <i>IA*</i> data structure. The <i>IA*</i> data structure, described by the
 * mangrove_tds::GIA class, is a dimension-independent data structure for representing simplificial complexes with an arbitrary domain. It extends the <i>IA</i> data structure and it is scalable to
 * manifold complexes, and supports efficient navigation and topological modifications. The <i>IA*</I> data structure encodes only top simplices plus a suitable subset of the adjacency relations, thus it
 * is a <i>local</i> data structure.<p>The <i>gia_stats</i> program is a simple shell program that must be invoked as:<p><center><i>gia_stats [-d|-n] input_file</i></center><p> where: <ul><li>the flag
 * <i>-d</i> indicates wheter deep checks are required</li><li>the flag <i>-n</i> indicates whether none checks are required</li><li><i>input_file</i> contains the path for the file from which we can
 * read the simplicial complex to be analyzed. This file must be supported by the <i>Mangrove TDS Library</i>.</li></ul><p>This program also requires the
 * <i><A href="http://trolltech.com/">Qt Toolkit</A></i> - this toolkit is a cross-platform application development framework, widely used for the development of GUI programs (in which case it is known
 * as <i>widget toolkit</i>) and also used for developing non-GUI programs such as console tools and servers. It is produced by the Norwegian company <i><A href="http://trolltech.com/">Trolltech</A></i>,
 * a wholly owned subsidiary of <i><A href="http://www.nokia.com/">Nokia Corporation</A></i> since June 17, 2008.
 * \file gia_stats.cpp
 * \author <A href="mailto:canino@disi.unige.it">David Canino</A>
 */

/// This function writes an error message on the standard error and closes the program.
/**
 * This function writes an error message on the standard error and closes the program.
 * \param exe_name the name of the current executable
 * \param err_msg the error message to be written
 * \return the built-in <i>EXIT_FAILURE</i> constant
 */
void writeErrorMessage( string exe_name, string err_msg)
{
	/* Now, we write! */
	cout<<endl;
	cout.flush();
	cerr<<"\t Error: "<<err_msg<<endl;
	cerr<<endl<<"\t Use: "<<exe_name<<" [-d|-n] <input file>"<<endl<<endl;
	cerr.flush();
	exit(EXIT_FAILURE);
}

/// The main function for the <i>gia_stats</i> program
/**
 * This function is the main one for the <i>gia_stats</i> program, a program for testing the <i>IA*</i> data structure. The <i>IA*</i> data structure, described by the mangrove_tds::GIA class, is a
 * dimension-independent data structure for representing simplificial complexes with an arbitrary domain. It extends the <i>IA</i> data structure and it is scalable to manifold complexes, and supports
 * efficient navigation and topological modifications. The <i>IA*</I> data structure encodes only top simplices plus a suitable subset of the adjacency relations, thus it is a <i>local</i> data structure.
 * \param argc the number of shell parameters
 * \param argv the shell parameters
 * \return <ul><li>the built-in <i>EXIT_SUCCESS</i> constant, if all is ok</li><li>the built-in <i>EXIT_FAILURE</i> constant, otherwise</li></ul>
 * \see mangrove_tds::BaseSimplicialComplex, mangrove_tds::OFFManager, mangrove_tds::GMVManager, mangrove_tds::AbaqusManager, mangrove_tds::TSManager, mangrove_tds::GIAManager, mangrove_tds::GIA
 */
int main(int argc,char **argv)
{
	OFFManager *off;
  	GMVManager *gmv;
  	AbaqusManager *inp;
	TSManager *ts;
	string full_path_in;
	QString ext_in;
	QTime t;
	GIA *ccw;
	GIAManager ccw_man;
	vector< vector<GhostSimplexPointer> > clusters;
	bool checksOn;
	
	/* First, we check the input parameters! */
	if(argc!=3) writeErrorMessage( string(argv[0]), string("wrong number of parameters"));
	checksOn=false;
	if( string(argv[1]).compare(string("-d"))==0) checksOn=true;
	else if( string(argv[1]).compare(string("-n"))==0) checksOn=false;	
	else writeErrorMessage( string(argv[0]), string("wrong parameters"));
	full_path_in = fromQt2Cplustring( QFileInfo(argv[2]).absoluteFilePath() );
  	ext_in = QFileInfo( QString(full_path_in.c_str())).suffix();
  	if( isSupportedFormat(ext_in)==false) { writeErrorMessage(string(argv[0]),string("'"+fromQt2Cplustring(ext_in)+"' is a not supported format")); }
	off =NULL;
 	gmv = NULL;
  	inp = NULL;
  	ts = NULL;
  	try
  	{
  		/* Now, we create all the readers! */
    		off = new OFFManager();
    		gmv = new GMVManager();
    		inp = new AbaqusManager();
    		ts = new TSManager();
  	}
  	catch(bad_alloc ba)
  	{
    		if(off!=NULL) delete off;
    		if(gmv!=NULL) delete gmv;
    		if(inp!=NULL) delete inp;
    		if(ts!=NULL) delete ts;
    		off = NULL;
    		gmv = NULL;
    		inp = NULL;
    		ts = NULL;
    		writeErrorMessage( string(argv[0]), string("allocation error")); 
  	}
  	
  	/*Now, we load the input simplicial complex and then we write its statistics! */
  	cout<<endl<<"\tReading data and creating a new "<<ccw_man.getDataStructureName()<<" ... ";
  	cout.flush();
  	t.start();
  	if(ext_in == QString("off")) { ccw_man.createComplex(off,full_path_in,true,true,checksOn); }
  	else if(ext_in == QString("inp")) { ccw_man.createComplex(inp,full_path_in,true,true,checksOn); }
  	else if(ext_in == QString("ts")) { ccw_man.createComplex(ts,full_path_in,true,true,checksOn); }
  	else { ccw_man.createComplex(gmv,full_path_in,true,true,checksOn); }
  	cout<<"ok [ in "<<t.elapsed()<<" ms ]"<<endl<<endl;
  	cout.flush();
  	ccw=NULL;
 	ccw_man.getComplex(&ccw);
 	cout<<"\tSimplicial "<<ccw->type()<<"-complex encoded through the "<<ccw->getDataStructureName()<<endl;
  	cout<<"\tStorage cost: "<<ccw->getStorageCost()<<endl<<endl;
  	cout.flush();
  	for(SIMPLEX_TYPE d=0;d<=ccw->type();d++)
  	{
    		cout<<"\tNumber of encoded "<<d<<"-simplices: "<<ccw->size(d)<<endl;
    		if(d!=ccw->type()) cout<<"\tNumber of top "<<d<<"-simplices: "<<ccw->getTopSimplicesNumber(d)<<endl;
    		cout.flush();
    		if(d<=1) cout<<"\tNumber of non-manifold "<<d<<"-simplices: "<<ccw->getNonManifoldSimplicesNumber(d)<<endl;
    		cout.flush();
    		ccw->getClusters(d,false,clusters);
    		if(clusters.size()!=0) ccw->componentsForViewer(clusters,string(fromQt2Cplustring(QString::number(d))+string("clusters.ccv")),string("connected-components"));
		cout<<"\tNumber of clusters formed by "<<d<<"-simplices: "<<clusters.size()<<" - saved"<<endl;
		cout.flush();
		ccw->getClusters(d,true,clusters);
		if(d<ccw->type()) cout<<"\tNumber of clusters formed by top "<<d<<"-simplices: "<<clusters.size()<<endl;
		cout<<endl;
		cout.flush();
  	}
	
	
	for(SIMPLEX_TYPE d=0;d<ccw->type();d++)
  	{
		if(ccw->isKConnected(d))
		  cout << "\tThe shape is "<<d<<"-connected"<<endl;
		else
		  cout << "\tThe shape is not "<<d<<"-connected"<<endl;
		cout.flush();
  	}
	cout<<(ccw->isPseudoManifold()?"\tThe shape is pseudomanifold":"\tThe shape is not pseudomanifold")<<endl;
	cout<<(ccw->isManifoldShape()?"\tThe shape is manifold":"\tThe shape is not manifold")<<endl;
	cout<<(ccw->isRegular()?"\tThe shape is regular":"\tThe shape is not regular")<<endl;
	
	
	/* If we arrive here, we can exit! */
	cout<<"\tWriting a representation of the input model ... ";
	ccw->write4Viewer(string(fromQt2Cplustring(QFileInfo(QString(full_path_in.c_str())).baseName()))+string(".ccv"),string("model"));
	cout<<"ok"<<endl<<endl;
	cout.flush();
 	ccw_man.close();
  	if(off!=NULL) delete off;
  	if(gmv!=NULL) delete gmv;
  	if(inp!=NULL) delete inp;
  	if(ts!=NULL) delete ts;
 	off=NULL;
  	gmv=NULL;
  	inp=NULL;
  	ts=NULL;
  	ccw=NULL;
  	return EXIT_SUCCESS;
}