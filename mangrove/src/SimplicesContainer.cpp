/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library                                    
 *                                                                           				    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * January 2011 (Revised May 2012)
 *
 * SimplicesContainer.cpp - containers for collections of simplices in a simplicial complex (implementation)
 *                                                                    
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.                                                    
 ***********************************************************************************************/

#include "SimplicesContainer.h"
#include "Property.h"
#include <algorithm>
#include <assert.h>
using namespace std;
using namespace mangrove_tds;

/* All the following source belongs to the 'mangrove_tds' namespace */
namespace mangrove_tds
{
	/* ============================== IMPLEMENTATION of SimplexIterator CLASS  ================== */
	
	SimplexIterator::SimplexIterator()
	{
		/* This method creates a not initialized iterator. */
		this->container = NULL;
		this->p = 0;
		this->eof = false;
		this->reof = false;
	}
	
	SimplexIterator::SimplexIterator(const SimplexIterator &it)
	{
		/* We should copy the input iterator */
		this->container = it.container;
		this->p = it.p;
		this->eof = it.eof;
		this->reof = it.reof;
	}
	
	SimplexIterator::SimplexIterator(SimplicesContainer*cc,bool eofp,bool reofp,SIMPLEX_ID index)
	{
		/* First, we must check the input container and then we must understand which type of iterator we must create. */
		this->setCollection(cc);
		if(reofp)
		{
			/* We have reofp = true, thus the iterator could:
			 * - refer the last simplex in the reverse direction (the element before the first!) [ eofp = false ]
			 * - refer an empty collections of valid simplices [ eofp = true ] */
			this->p = 0;
			if(eofp) { assert( this->container->isEmpty()); }
			else { assert(this->container->isEmpty()==false); }
		}
		else
		{
			/* We have reofp = false, thus the iterator could:
			 * - refer the last simplex in the direct direction (the element after the last!) [ eofp = true ] 
			 * - refer an internal simplex, if it is valid [ eofp = false ] */
			assert(this->container->size()!=0);
			if(!eofp)
			{
				/* The iterator MUST refer a valid simplex */
				assert(index < this->container->vectorSize());
				assert(this->container->isDeleted(index)==false);
				this->p = index;
			}
			else { this->p = this->container->vectorSize(); }
		}
		
		/* If we arrive here, we can update 'eof' and 'reof' */
		this->eof = eofp;
		this->reof = reofp;
	}
	
	void SimplexIterator::setCollection(SimplicesContainer *container)
	{
		/* First, we check 'container' and we can continue */
		assert(container!=NULL);
		this->container = container;
	}
	
	SimplexPointer SimplexIterator::getPointer() const 
	{ 
		/* First, we check 'container' and then we build a SimplexPointer, if the iterator points to a valid simplex! */
		assert(this->haveCollection());
		assert((this->reof==false) && (this->eof==false));
		assert(this->container->isDeleted(this->p)==false);
		return SimplexPointer(this->container->type(),this->p);
	}
	
	void SimplexIterator::debug(ostream& os) const
	{
		/* We write a debug representation of the current iterator! */
		if(this->haveCollection()==false) 
		{ 
			os<<"\tThis iterator has not been associated to a simplices collection"<<endl;
			os.flush();
			return;
		}
		
		/* Iterator associated to a simplices collection */
		os<<"\tThis iterator has been associated to a collection of "<<this->container->type()<<"-simplices"<<endl;
		os<<"\tThis collection contains "<<this->container->size()<<" valid simplices over ";
		os<<this->container->vectorSize()<<" allocated locations"<<endl;
		if((this->reof==false) && (this->eof==false))
		{
			/* The current iterator points to a simplex in the current collection */
			os<<"\tThe current iterator points to the "<<this->p<<" simplex in the associated collection"<<endl;
			if(this->container->isValid(this->p)==false) { os<<"\tThe referred simplex is not valid"<<endl; }
			else
			{
				/* The referred simplex is valid! */
				os<<"\tThe referred simplex is valid:"<<endl<<endl;
				this->container->simplexc(this->p).debug(os);
			}
		}
		else if( (this->reof==false) && (this->eof)) { os<<"\tThe current iterator points to the associated collection end"<<endl; }
		else if( (this->reof) && (this->eof==false)) { os<<"\tThe current iterator points to the associated collection head"<<endl; }
		else { os<<"\tThe current iterator points to an empty collection of simplices"<<endl; }
		os.flush();
	}
	
	bool SimplexIterator::operator==(const SimplexIterator &ci)
	{
		  /* First, we check if the iterators have the same collection of simplices and then if they are valid! */
		  if( this->haveCollection()==false) { return !(ci.haveCollection()); }
		  if(this->getCollection()!=ci.getCollection()) return false;
		  if(this->reof!=ci.reof) return false;
		  if(this->eof!=ci.eof) return false;
		  if((this->reof==false) && (this->eof==false)) { return (this->p==ci.p); }
		  else { return true; }
 	}
 	
 	bool SimplexIterator::operator!=(const SimplexIterator& ci) { return !(*this == ci); }
 	
 	Simplex& SimplexIterator::operator*()
	{
		/* First, we check if 'container' and the current simplex are valid: the game can start! */
		assert(this->haveCollection());
		assert((this->reof==false) && (this->eof==false));
		return this->container->simplex(this->p);
	}
	
	Simplex* SimplexIterator::operator->()
	{
		/* First, we check if 'container' and the current simplex are valid: the game can start! */
		assert(this->haveCollection());
		assert((this->reof==false) && (this->eof==false));
		return &(this->container->simplex(this->p));
	}
	
	SimplexIterator SimplexIterator::operator++()
	{
		SimplexIterator temp;

		/* We move to the next simplex using the other operator ++ */
		temp = (*this);
		(*this)++;
		return temp;
	}
	
	SimplexIterator& SimplexIterator::operator++(int aux)
	{
		bool stop;
		
		/* First, we check if 'container' is valid and then we can understand if it is possible to find the next valid simplex. */
		assert(this->haveCollection());
		assert(this->eof==false);
		assert(this->container->size()!=0);
		idle(aux);
		if(this->reof)
		{
			/* We are on the reverse end, i.e. the element before the first one: we move forward, thus we are not more here. */
			this->p = 0;
			this->reof = false;
		}
		else
		{
			/* We are in the middle of container: the current simplex must be valid */
			assert(this->container->isValid(this->p));
			this->p = this->p + 1;
		}
		
		/* Now, we must find the next valid simplex on which we must move. */
		stop = false;
		while(!stop)
		{
			/* Now, we check the current simplex. */
			if(this->p >= this->container->vectorSize()) { stop = true; }
			else if(this->container->isDeleted(this->p)) { this->p = this->p + 1; }
			else { stop = true; }
		}

		/* Now, we must understand where we have arrived. */
		if( this->p >= this->container->vectorSize()) { this->eof = true; }
		else { this->eof = false; }
		return (*this);
	}
	
	SimplexIterator SimplexIterator::operator--()
	{
		SimplexIterator temp;

		/* We move to the previous simplex using the other operator -- */
		temp = (*this);
		(*this)--;
		return temp;
	}
	
	SimplexIterator& SimplexIterator::operator--(int aux)
	{
		bool stop;
		
		/* First, we check if 'container' is valid and then we can understand if it is possible to find the previous valid simplex. */
		assert(this->haveCollection());
		assert(this->reof==false);
		assert(this->container->size()!=0);
		idle(aux);
		if(this->eof)
		{
			/* We are on the element after the last one in the associated collection (forward end) thus we return on the last element */
			this->p = this->container->vectorSize()-1;
			this->eof = false;
		}
		else
		{
			/* We are on an internal element thus we can move backward */
			assert(this->container->isValid(this->p));
			if(this->p==0)
			{
				this->reof = true;
				return (*this);
			}
			else { this->p = this->p-1; }
		}
		
		/* Now, we must find the previous valid simplex. */
		stop = false;
		while(!stop)
		{
			if(this->p==0) { stop = true; }
			else
			{
				/* Now, we are on an internal simplex ... */
				if(this->container->isDeleted(this->p)) { this->p = this->p - 1; }
				else { stop = true; }
			}
		}
		
		/* Now, we check where we have arrived. */
		if(this->container->isDeleted(this->p)) { this->reof = true; }
		else { this->reof = false; }
		return (*this);
	}
	
	/* ==================================== IMPLEMENTATION of SimplicesContainer CLASS ====================== */
	
	SimplicesContainer::SimplicesContainer(const SimplicesContainer &c)
	{
		/* Now, we copy the simplices container 'c' */
		this->ctype = c.ctype;
		this->simplices = vector<Simplex>();
		for(unsigned int i=0;i<c.simplices.size();i++) { this->simplices.push_back(Simplex(c.simplices[i])); }
		this->next_loc = c.next_loc;
		this->valid_locs = c.valid_locs;
	}
	
	void SimplicesContainer::updateType(SIMPLEX_TYPE ctype)
	{
		/* We clear the old simplices and update 'ctype' */
		this->ctype = ctype;
		this->simplices.clear();
		this->next_loc = 0;
		this->valid_locs = 0;
	}
	
	bool SimplicesContainer::isValid(const SIMPLEX_ID i) const
	{
		/* We check if the required simplex is valid. */
		if(i>=this->simplices.size()) { return false; }
		else { return !(this->simplices[i].isDeleted()); }
	}
	
	bool SimplicesContainer::isDeleted(const SIMPLEX_ID i) const
	{
		/* We check if the required simplex is valid. */
		if(i>=this->simplices.size()) { return false; }
		else { return this->simplices[i].isDeleted(); }
	}
	
	Simplex& SimplicesContainer::simplex(const SIMPLEX_ID i)
	{
		/* The required location must be valid! */
		assert(this->isValid(i));
		return this->simplices[i];
	}
	
	const Simplex& SimplicesContainer::simplexc(SIMPLEX_ID i) const
	{
		/* The required location must be valid! */
		assert(this->isValid(i));
		return this->simplices[i];
	}
	
	bool SimplicesContainer::isEmpty() const
	{
		/* We check if we have 0 simplices or 0 valid simplices! */
		if(this->vectorSize()==0) { return true; }
		if(this->size()==0) { return true; }
		return false;
	}
	
	void SimplicesContainer::clearCollection()
	{
		/* First, we clear all the simplices in the current container. */
		for(vector<Simplex>::iterator it = this->simplices.begin();it!=this->simplices.end();it++)
		{
			it->clearBoundary();
			it->clearCoboundary();
			it->clearAdjacency();
			it->clearAuxiliaryBoundary();
		}

		/* Now, we clear the old simplices and update the type */
		this->updateType(0);
	}
	
	void SimplicesContainer::addSimplex(SimplexPointer &cp)
	{
		unsigned int aux;
		
		/* First, we check if we can reuse an old location. */
		aux = this->next_loc;
		if(this->next_loc == this->vectorSize())
		{
			/* We must create a new location to the bottom of 'simplices' */
			this->simplices.push_back( Simplex(this->type()));
			this->valid_locs = this->valid_locs+1;
			this->next_loc = this->vectorSize();
		}
		else
		{
			/* We cau reuse an old location */
			this->simplices[this->next_loc].setDeleted(false);
			this->simplices[this->next_loc].setVisited(false);
			this->simplices[this->next_loc].setType(this->type());
			this->valid_locs = this->valid_locs+1;
			this->updateNextFreeLocation(this->next_loc+1);
		}
		
		/* Now, we can update the output simplex pointer 'cp' */
		cp.setType(this->type());
		cp.setId(aux);
	}
	
	void SimplicesContainer::newSimplex(vector<PropertyBase *> &properties, SimplexPointer &cp)
	{
		unsigned int aux;
		vector<PropertyBase *>::iterator it;
		
		/* First, we check if we can reuse an old location. */
		aux = this->next_loc;
		if(this->next_loc == this->vectorSize())
		{
			/* We must create a new location to the bottom of 'simplices' */
			this->simplices.push_back( Simplex(this->type()));
			this->valid_locs = this->valid_locs+1;
			this->next_loc = this->vectorSize();
			for( it = properties.begin(); it!= properties.end(); it++) 
			{
				/* Now, we must understand which type of property we have */
				if( (*it)->isLocal()) { (*it)->resize( this->vectorSize()); }
				else if( (*it)->isSparse()) { ; }
				else if( (*it)->isGhost()) { ; }
				else { (*it)->resize( this->type(), this->vectorSize()); }
			}
		}
		else
		{
			/* We cau reuse an old location */
			this->simplices[this->next_loc].setDeleted(false);
			this->simplices[this->next_loc].setVisited(false);
			this->simplices[this->next_loc].setType(this->type());
			this->valid_locs = this->valid_locs+1;
			this->updateNextFreeLocation(this->next_loc+1);
		}
		
		/* Now, we can update the output simplex pointer 'cp' */
		cp.setType( this->type());
		cp.setId(aux);
	}
	
	void SimplicesContainer::deleteSimplex(const SIMPLEX_ID i)
	{
		/* First, we check if the required simplex exists! */
		assert(i<this->vectorSize());
		assert(this->simplices[i].isDeleted()==false);
		this->simplices[i].setDeleted(true);
		this->simplices[i].clearBoundary();
		this->simplices[i].clearCoboundary();
		this->simplices[i].clearAdjacency();
		this->simplices[i].clearAuxiliaryBoundary();
		this->valid_locs = this->valid_locs-1;
		this->updateNextFreeLocation(0);
	}
	
	SimplexIterator SimplicesContainer::begin() 
	{
		/* First, we check if the associated collection is empty, otherwise we must look for the first valid simplex! */
		if(this->isEmpty()) { return SimplexIterator(this,true,true,0); }
		for(unsigned int k=0;k<this->vectorSize();k++) { if(this->simplices[k].isDeleted()==false) return SimplexIterator(this,false,false,k); }
		return SimplexIterator(this,true,true,0);
	}
	
	SimplexIterator SimplicesContainer::end()
	{
		/* First, we check if the associated collection is empty */
		if(this->isEmpty()) { return SimplexIterator(this,true,true,0); }
		else { return SimplexIterator(this,true,false,0); }
	}
	
	SimplexIterator SimplicesContainer::rbegin()
	{
		/* First, we check if the associated collection is empty, otherwise the must look for the last valid simplex! */
		if(this->isEmpty()) { return SimplexIterator(this,true,true,0); }
		for(long int k=(long)(this->vectorSize()-1);k>=0;k--) { if(this->simplices[k].isDeleted()==false) return SimplexIterator(this,false,false,k); }
		return SimplexIterator(this,true,true,0);
	}
	
	SimplexIterator SimplicesContainer::rend()
	{
		/* First, we check if the associated collection is empty */
		if(this->isEmpty()) { return SimplexIterator(this,true,true,0); }
		else { return SimplexIterator(this,false,true,0); }
	}
	
	void SimplicesContainer::debug(ostream& os) const
	{
		/* First, we write the global number of locations and then information about the remaining part of collection */
		os<<"Simplices Dimension: "<<this->type()<<endl;
		os<<"Global number of locations: "<<this->vectorSize()<<endl;
		os<<"Number of deleted locations: "<<this->deletedSize()<<endl;
		os<<"Number of valid locations: "<<this->size()<<endl;
		os<<"First free location: "<<this->next_loc<<endl<<endl;
		os.flush();
		for(unsigned long k=0;k<this->vectorSize();k++)  { this->simplices[k].debug(os); }
	}
	
	void SimplicesContainer::updateNextFreeLocation(unsigned int pos)
	{
		/* We must look for a valid location, starting from 'pos' */
		for(unsigned int i=pos;i<this->vectorSize();i++) 
		{
			/* Now, we check if the i-th location has been deleted */ 
			if(this->simplices[i].isDeleted())
			{ 
				this->next_loc = i;
				return;
			}
		}
		
		/* If we arrive here, there is not a valid location ... */
		this->next_loc = this->vectorSize();
	}
}

