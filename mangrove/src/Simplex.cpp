/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library
 *                                                                          				    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * June 2011 (Revised May 2012)
 *
 * Simplex.cpp - representations of simplices contained in a simplicial complex (implementation)
 *                                                                    
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.                                                    
 ***********************************************************************************************/

#include "Simplex.h"
#include <climits>
using namespace mangrove_tds;

/* All the following source belongs to the 'mangrove_tds' namespace */
namespace mangrove_tds
{
	/* =================== IMPLEMENTATION of CLASS SimplexPointer =========================== */
	
	SimplexPointer::SimplexPointer()
	{
		/* Dummy initialization for this simplex! */
		this->setType(0);
		this->setId(0);
	}
	
	SimplexPointer::SimplexPointer(SIMPLEX_TYPE typep,SIMPLEX_ID idp)
	{
		/* We update the current pointer */
		this->setType(typep);
		this->setId(idp);
	}
	
	SimplexPointer::SimplexPointer(const SimplexPointer& cp)
	{
		/* We copy 'cp' */
		this->setType(cp.type());
		this->setId(cp.id());
	}
	
	void SimplexPointer::debug(ostream& os) const
	{
		/* In this representation we write dimension and identifier! */
		os << "SimplexPointer to a " << this->type() << "-simplex, identifier: " << this->id()<<endl;
		os.flush();
	}
	
	bool SimplexPointer::theSame(const SimplexPointer& a) const
	{
		/* We check if the dimensions and identifiers are the same ones! */
		if(a.type()!=this->type()) return false;
		if(a.id()!=this->id()) return false;
		return true;
	}

	bool SimplexPointer::operator==(const SimplexPointer& a)
	{
		/* We check if the dimensions and identifiers are the same ones! */
		if(a.type()!=this->type()) return false;
		if(a.id()!=this->id()) return false;
		return true;
	}
	
	bool SimplexPointer::operator<(const SimplexPointer& a)
	{
		/* We compare the current pointer with 'a', by giving priority to simplex dimension. */
		if(this->type()<a.type()) { return true; }
		else if( this->type()==a.type())
		{
			if(this->id()<a.id()) { return true; }
			else { return false; }
		}
		else { return false; }
	}
	
	/* =================== IMPLEMENTATION of CLASS Simplex ==================================== */
	
	Simplex::Simplex()
	{
		/* We must create a not initialized simplex! */
		this->deleted=true;
		this->visited=false;
		this->clearBoundary();
		this->clearCoboundary();
		this->clearAdjacency();
		this->aux_boundary.clear();
	}
	
	Simplex::Simplex(SIMPLEX_TYPE type)
	{
		/* We initialize the new simplex of dimension 'type' */
		this->deleted = false;
		this->visited = false;
		this->setType(type);
	}
	
	Simplex::Simplex(const Simplex& c)
	{
		/* We copy the input simplex 'c' */
		this->setDeleted(c.isDeleted());
		this->setVisited(c.isVisited());
		this->clearBoundary();
		this->clearCoboundary();
		this->clearAdjacency();
		this->clearAuxiliaryBoundary();
		for(unsigned int k=0;k<c.boundary.size();k++) this->boundary.push_back(SimplexPointer(c.bc(k)));
		for(unsigned int k=0;k<c.coboundary.size();k++) this->coboundary.push_back(SimplexPointer(c.cc(k)));
		for(unsigned int k=0;k<c.adjacency.size();k++)
		{
			this->adjacency.push_back(vector<SimplexPointer>());
			for(unsigned int j=0;j<c.adjacency[k].size();k++) this->adjacency[k].push_back(SimplexPointer(c.adjacency[k].at(j)));
		}
		
		/* Now, we copy 'auxiliary boundary' */
		if(c.type()>1) { for(unsigned int k=0;k<c.type();k++) this->aux_boundary.push_back(map<SIMPLEX_ID,SIMPLEX_ID>(c.aux_boundary[k])); }
	}

	Simplex::~Simplex()
	{
		/* We clear topological relations! */
		this->clearBoundary();
		this->clearCoboundary();
		this->clearAdjacency();
		this->clearAuxiliaryBoundary();
	}
	
	SIMPLEX_TYPE Simplex::type() const
	{
		/* A k-simplex has k+1 simplicess in its boundary: if it is a 0-simplex, then it's empty! */
		if(this->boundary.size()>0) return (this->boundary.size()-1);
		else return 0;
	}
	
	void Simplex::setType(SIMPLEX_TYPE t)
	{
		/* First, we check if the type is 0! */
		this->clearCoboundary();
		this->clearAdjacency();
		this->clearBoundary();
		this->clearAuxiliaryBoundary();
		if(t>1) { for(unsigned int k=0;k<t;k++) this->aux_boundary.push_back(map<SIMPLEX_ID,SIMPLEX_ID>()); }
		for(unsigned int k=0;k<=t;k++)
		{
			if(t!=0) this->boundary.push_back(SimplexPointer(0,0));
			this->adjacency.push_back(vector<SimplexPointer>());
			this->adjacency.back().clear();
		}
	}
	
	unsigned int Simplex::auxiliaryBoundarySize() const
	{
		unsigned int n;

		/* First, we check if all is ok */
		assert(this->type()>1);
		n=0;
		for(unsigned int j=0;j<this->aux_boundary.size();j++) { n=n+this->aux_boundary[j].size(); }
		return n;
	}
				
	unsigned int Simplex::auxiliaryBoundarySize(SIMPLEX_TYPE t) const
	{
		/* First, we check if all is ok */
		assert(this->type()>1);
		assert(t<this->aux_boundary.size());
		return this->aux_boundary.at(t).size();
	}

	bool Simplex::isAuxiliary(const SimplexPointer &cp) const
	{
		map<SIMPLEX_ID,SIMPLEX_ID>::const_iterator it;
		
		/* First, we check if all is ok */
		assert(this->type()>1);
		assert(cp.type()<this->aux_boundary.size());
		for(it=this->aux_boundary.at(cp.type()).begin();it!=this->aux_boundary.at(cp.type()).end();it++) { if(it->second==cp.id()) return true; }
		return false;
	}

	void Simplex::clearAdjacency()
	{
		/* We deallocate adjacency! */
		for(unsigned int k=0;k<this->adjacency.size();k++) this->adjacency[k].clear();
		this->adjacency.clear();
	}
	
	void Simplex::clearAuxiliaryBoundary()
	{
		/* We deallocate 'aux_boundary' */
		for(unsigned int k=0;k<this->aux_boundary.size();k++) this->aux_boundary[k].clear();
		this->aux_boundary.clear();
	}
	
	SimplexPointer& Simplex::b(unsigned int elem)
	{
		/* The required location must exist, thus we can return it. */
		assert(elem < this->boundary.size());
		return this->boundary[elem];
	}
	
	SimplexPointer& Simplex::c(unsigned int elem)
	{
		/* The required location must exist, thus we can return it. */
		assert(elem<this->coboundary.size());
		return this->coboundary[elem];
	}
	
	SimplexPointer& Simplex::a(unsigned int elem)
	{
		/* The required location must exist and we must have an adjacency! */
		assert(elem<this->adjacency.size());
		assert(this->adjacency[elem].size()==1);
		return this->adjacency[elem][0];
	}
	
	void Simplex::getAuxiliaryBoundarySimplex(SIMPLEX_TYPE ct,SIMPLEX_ID ci,SimplexPointer &cp)
	{
		map<SIMPLEX_ID,SIMPLEX_ID>::const_iterator it;
		
		/* First, we check if all is ok */
		assert(this->type()>1);
		assert(ct<this->aux_boundary.size());
		it=this->aux_boundary.at(ct).find(ci);
		assert(it!=this->aux_boundary.at(ct).end());
		cp.setType(ct);
		cp.setId(it->second);
	}
	
	bool Simplex::isSetUp(SIMPLEX_TYPE ct,SIMPLEX_ID ci)
	{
		map<SIMPLEX_ID,SIMPLEX_ID>::const_iterator it;
		
		/* First, we check if all is ok */
		assert(this->type()>1);
		assert(ct<this->aux_boundary.size());
		it=this->aux_boundary.at(ct).find(ci);
		if(it==this->aux_boundary.at(ct).end()) return false;
		else return true;
	}
	
	const SimplexPointer& Simplex::bc(unsigned int elem) const
	{
		/* First, we check if the required pointer exists. */
		assert(elem < this->boundary.size());
		return this->boundary[elem];	
	}
	
	const SimplexPointer& Simplex::cc(unsigned int elem) const
	{
		/* First, we check if the required pointer exists */
		assert(elem<this->coboundary.size());
		return this->coboundary[elem];
	}
	
	const SimplexPointer& Simplex::ac(unsigned int elem) const
	{
		/* The required location must exist and we must have an adjacency! */
		assert(elem<this->adjacency.size());
		assert(this->adjacency[elem].size()==1);
		return this->adjacency[elem][0];
	}
	
	void Simplex::add2Boundary(const SimplexPointer& cp,unsigned int elem)
	{
		/* First, we check the boundary condition and if the required location is valid. */
		assert(this->type()!=0);
		assert(cp.type()<this->type());
		assert(elem<this->boundary.size());
		this->boundary[elem].setId(cp.id());
		this->boundary[elem].setType(cp.type());
	}
	
	void Simplex::add2Coboundary(const SimplexPointer& cp)
	{
		/* First, we check the coboundary condition and then we add the new simplex. */
		assert(cp.type()>this->type());
		this->coboundary.push_back(SimplexPointer(cp));
	}
	
	void Simplex::add2Adjacency(const SimplexPointer& cp,unsigned int elem)
	{
		/* First, we check the adjacency condition and if the required location is valid. */
		assert(elem<this->adjacency.size());
		if(this->type()==0) { assert(cp.type()==0); }
		else 
		{
			assert(this->adjacency[elem].size()==0);
			assert( (cp.type()==this->type()) || (cp.type()==this->type()-1)); 
		}
		
		/* Now, we can add a new simplex! */
		this->adjacency[elem].push_back(SimplexPointer(cp));
	}
	
	void Simplex::add2AuxiliaryBoundary(const SimplexPointer& cp,SIMPLEX_ID ci)
	{
		/* First, we check if all is ok! */
		assert(this->type()>1);
		assert(cp.type()<this->aux_boundary.size());
		this->aux_boundary[cp.type()][ci]=cp.id();
	}
	
	void Simplex::removeFromBoundary(const SimplexPointer& cp)
	{
		vector<SimplexPointer>::iterator it;
		
		/* First, we check if the boundary condition is satisfied */
		assert(this->type()!=0);
		assert(cp.type()<this->type());
		it = find( this->boundary.begin(),this->boundary.end(),cp);
		assert(it!=this->boundary.end());
		this->boundary.erase(it);
		this->clearAdjacency();
		this->clearCoboundary();
		this->clearAuxiliaryBoundary();
		if(this->boundary.size()==1) this->clearBoundary();
		if(this->type()>1) { for(unsigned int k=0;k<this->type();k++) this->aux_boundary.push_back(map<SIMPLEX_ID,SIMPLEX_ID>()); }
		for(unsigned int k=0;k<=this->type();k++)
		{
			this->adjacency.push_back(vector<SimplexPointer>());
			this->adjacency.back().clear();
		}
	}
	
	void Simplex::removeFromCoboundary(const SimplexPointer& cp)
	{
		vector<SimplexPointer>::iterator it;
		
		/* First, we check if the coboundary condition is satisfied */
		assert(cp.type()>this->type());
		it = find( this->coboundary.begin(),this->coboundary.end(),cp);
		assert(it!=this->coboundary.end());
		this->coboundary.erase(it);
	}
	
	void Simplex::removeFromAdjacency(const SimplexPointer& cp)
	{
		vector<SimplexPointer>::iterator it;

		/* First, we check if the adjacency condition is satisfied! */
		assert((cp.type()==this->type()) || (cp.type()==this->type()-1));
		for(unsigned int k=0;k<this->adjacency.size();k++)
		{
			it=find(this->adjacency[k].begin(),this->adjacency[k].end(),cp);
			if(it!=this->adjacency[k].end())
			{
				this->adjacency[k].erase(it);
				return;
			}
		}
		
		/* If we arrive here, we have an error! */
		assert(false);
	}
	
	void Simplex::removeAuxiliaryBoundarySimplex(SIMPLEX_TYPE ct, SIMPLEX_ID ci)
	{
		map<SIMPLEX_ID,SIMPLEX_ID>::iterator it;
		
		/* First, we check if all is ok! */
		assert(this->type()>1);
		assert(ct<this->aux_boundary.size());
		it=this->aux_boundary[ct].find(ci);
		assert(it!=this->aux_boundary[ct].end());
		this->aux_boundary[ct].erase(it);
	}
	
	bool Simplex::hasAdjacency(unsigned int elem) const
	{
		/* The required location must exist! */
		assert(elem<this->adjacency.size());
		return (this->adjacency[elem].size()==1);
	}
	
	bool Simplex::isManifoldAdjacency(unsigned int elem) const
	{
		/* The required location must exist! */
		if(this->hasAdjacency(elem)==false) return true;
		else if(this->adjacency[elem].at(0).type()==this->type()) return true;
		else return false;
	}
	
	bool Simplex::positionInBoundary(const SimplexPointer& cp,unsigned int &pos)
	{
		unsigned int l;
		vector<SimplexPointer>::iterator it;
		
		/* We iterate over the boundary! */
		for(it = this->boundary.begin(),l=0; it!=this->boundary.end(); it++,l++) 
		{ 
			if((*it) == cp)
			{
				pos=l;
				return true;
			}
		}
		
		/* If we arrive here, we fail! */
		pos=UINT_MAX;
		return false;
	}
			
	bool Simplex::positionInCoboundary(const SimplexPointer& cp,unsigned int &pos)
	{
		unsigned int l;
		vector<SimplexPointer>::iterator it;
		
		/* We iterate over the coboundary! */
		for(it = this->coboundary.begin(),l=0; it!=this->coboundary.end(); it++,l++) 
		{ 
			if((*it) == cp)
			{
				pos=l;
				return true;
			}
		}
		
		/* If we arrive here, we fail! */
		pos=UINT_MAX;
		return false;
	}
			
	bool Simplex::positionInAdjacency(const SimplexPointer& cp,unsigned int &pos)
	{
		unsigned int l;
		
		/* We iterate over the adjacency! */
		for(l=0;l<this->adjacency.size();l++)
		{
			if(this->adjacency[l].size()==1)
			{
				if( SimplexPointer(this->adjacency[l].at(0))==cp)
				{
					pos=l;
					return true;
				}
			}
		}
		
		/* If we arrive here, we fail! */
		pos=UINT_MAX;
		return false;
	}
	
	void Simplex::debug(ostream& os) const
	{
		map< SIMPLEX_ID, SIMPLEX_ID >::const_iterator it;

		/* Now, we write informations about the current simplex. */
		os<<"\tDimension: "<<this->type()<<endl;
		os<<"\tGarbage status: ";
		if(this->isDeleted()) { os<<"deleted"<<endl; }
		else { os<<"not deleted"<<endl; }
		os<<"\tVisit status: ";
		if(this->isVisited()) { os<<"visited"<<endl; }
		else { os<<"not visited"<<endl; }
		os<<"\tBoundary:"<<endl;
		for(unsigned long k=0;k<this->getBoundarySize();k++) os<<"\t\t"<<this->bc(k);
		os<<"\tCoboundary:"<<endl;
		for(unsigned long k=0;k<this->getCoboundarySize();k++) os<<"\t\t"<<this->cc(k);
		os<<"\tAdjacency:"<<endl;
		for(unsigned int k=0;k<this->adjacency.size();k++)
		{
			os<<"\t\talong the subface "<<k<<": ";
			if(this->hasAdjacency(k)==true) os<<this->ac(k);
			else os<<"none"<<endl;
		}
		
		/* Auxiliary boundary! */
		os<<endl<<"\tAuxiliary boundary: ";
		if(this->supportsAuxiliaryBoundary()==false) { os<<"not supported"<<endl; }
		else
		{
			/* The auxiliary boundary is supported! */
			os<<endl<<endl;
			for(unsigned int k=0;k<this->aux_boundary.size();k++)
			{
				os<<"\t\tChildren of dimension "<<k<<" : ";
				if(this->aux_boundary.at(k).size()==0) { os<<"none"<<endl; }
				else
				{
					/* We have something to write! */
					os<<endl<<endl;
					for(it=this->aux_boundary.at(k).begin();it!=this->aux_boundary.at(k).end();it++)
					{
						os<<"\t\t\t"<<it->first<<"-th subface of dimension "<<k<<" : "<<it->second<<endl;
						os.flush();
					}
				}
			}
		}
		
		/* If we arrive here, we can finalize! */
		os<<endl;
		os.flush();
	}
	
	/* =================== IMPLEMENTATION of CLASS GhostSimplexPointer ==================================== */
	
	GhostSimplexPointer::GhostSimplexPointer()
	{
		/* Dummy initialization! */
		this->setParentType(0);
		this->setParentId(0);
		this->setChildType(0);
		this->setChildId(0);
	}
	
	GhostSimplexPointer::GhostSimplexPointer(SIMPLEX_TYPE pt,SIMPLEX_ID pi)
	{
		/* We create a top simplex! */
		this->setParentType(pt);
		this->setParentId(pi);
		this->setChildType(pt);
		this->setChildId(0);
	}

	GhostSimplexPointer::GhostSimplexPointer(SIMPLEX_TYPE pt,SIMPLEX_ID pi,SIMPLEX_TYPE ct,SIMPLEX_ID ci)
	{
		/* Initialization... */
		this->setParentType(pt);
		this->setParentId(pi);
		this->setChildType(ct);
		this->setChildId(ci);
	}
	
	GhostSimplexPointer::GhostSimplexPointer(const GhostSimplexPointer &g)
	{
		/* Deep copy! */
		this->setParentType(g.getParentType());
		this->setParentId(g.getParentId());
		this->setChildType(g.getChildType());
		this->setChildId(g.getChildId());
	}
	
	bool GhostSimplexPointer::isEncoded() const
	{
		/* We must have pt=ct and ci=0 */
		if(this->getParentType()!=this->getChildType()) return false;
		if(this->getChildId()!=0) return false;
		return true;
	}
	
	void GhostSimplexPointer::debug(ostream& os) const
	{
		/* Debug representation */
		os<<"Pointer to a ghost simplex: ";
		if(this->isEncoded()) { os<<"top simplex "<<this->getParentType()<<" "<<this->getParentId()<<endl; }
		else { os<<this->getChildId()<<"-th "<<this->getChildType()<<"-face of the top simplex "<<this->getParentType()<<" "<<this->getParentId()<<endl; }
		os.flush();
	}
	
	bool GhostSimplexPointer::theSame(const GhostSimplexPointer& g) const
	{
		/* We must have the same values! */
		if(this->getParentType()!=g.getParentType()) return false;
		if(this->getParentId()!=g.getParentId()) return false;
		if(this->getChildType()!=g.getChildType()) return false;
		if(this->getChildId()!=g.getChildId()) return false;
		return true;
	}
	
	bool GhostSimplexPointer::operator==(const GhostSimplexPointer& g)
	{
		/* We must have the same values! */
		if(this->getParentType()!=g.getParentType()) return false;
		if(this->getParentId()!=g.getParentId()) return false;
		if(this->getChildType()!=g.getChildType()) return false;
		if(this->getChildId()!=g.getChildId()) return false;
		return true;
	}
	
	bool GhostSimplexPointer::operator<(const GhostSimplexPointer& g)
	{
		/* We compare indices, by giving priority to dimension of the top simplices! */
		if(this->getParentType()<g.getParentType()) return true;
		if(this->getParentType()>g.getParentType()) return false;
		if(this->getParentId()<g.getParentId()) return true;
		if(this->getParentId()>g.getParentId()) return false;	
		if(this->getChildType()<g.getChildType()) return true;
		if(this->getChildType()>g.getChildType()) return false;
		if(this->getChildId()<g.getChildId()) return true;
		else return false;
	}
	
	/* =================== IMPLEMENTATION of CLASS GhostSimplex =============================================== */

	GhostSimplex::GhostSimplex()
	{
		this->gsp=GhostSimplexPointer();
		this->bnd.clear();
	}
	
	GhostSimplex::GhostSimplex(const GhostSimplex& s)
	{
		unsigned int k;

		/* Deep copy */
		this->gsp=GhostSimplexPointer(s.gsp);
		this->bnd.clear();
		for(k=0;k<s.bnd.size();k++) this->bnd.push_back(SimplexPointer(s.bnd[k]));
	}

	GhostSimplex::~GhostSimplex() { this->clear(); }
	
	void GhostSimplex::clear()
	{
		this->bnd.clear();
		this->bnd=vector<SimplexPointer>();
	}
			
	GhostSimplexPointer& GhostSimplex::getGhostSimplexPointer() { return this->gsp; }
	
	const GhostSimplexPointer& GhostSimplex::getCGhostSimplexPointer() const { return this->gsp; }
	
	vector<SimplexPointer>& GhostSimplex::getBoundary() { return this->bnd; }
	
	const vector<SimplexPointer>& GhostSimplex::getCBoundary() const { return this->bnd; }

	bool GhostSimplex::isIncident(const SimplexPointer& cp) const
	{
		/* First, we check if the 'cp' is a vertex */
		assert(cp.type()==0);
		for(unsigned int k=0;k<this->bnd.size();k++) { if(this->bnd.at(k).id()==cp.id()) return true; }
		return false;
	}
	
	bool GhostSimplex::getPositionOf(const SimplexPointer& cp,unsigned int &pos)
	{
		/* First, we check if the 'cp' is a vertex */
		assert(cp.type()==0);
		for(unsigned int k=0;k<this->bnd.size();k++)
		{
			if(this->bnd.at(k).id()==cp.id())
			{
				pos=k;
				return true;
			}
		}
		
		/* If we arrive here, we fail! */
		pos=UINT_MAX;
		return false;
	}

	bool GhostSimplex::theSame(const GhostSimplex& s) const
	{
		/* First, we check the number of vertices! */
		if(this->bnd.size()!=s.bnd.size()) return false;
		for(unsigned int k=0;k<s.bnd.size();k++) { if(this->bnd.at(k).id()!=s.bnd.at(k).id()) return false; }
		return true;
	}

	void GhostSimplex::debug(ostream& os) const
	{
		/* Debug representation! */
		this->gsp.debug(os);
		os<<"Boundary simplices: "<<endl;
		for(unsigned int k=0;k<this->bnd.size();k++)
		{
			os<<"\t";
			this->bnd[k].debug(os);
			os.flush();
		}
		
		/* Now, 'new line'! */
		os<<endl;
		os.flush();
	}
}

