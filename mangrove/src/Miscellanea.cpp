/***********************************************************************************************
 * This source code belongs to the Mangrove TDS Library                                
 *                                                                           				    
 * David Canino (canino@disi.unige.it)
 * PhD. Student at DISI - Dipartimento di Informatica e Scienze dell'Informazione
 * Via Dodecanneso 35, 16146, Genova(GE), Italia
 *
 * October 2011 (Revised May 2012) 
 *                                                                    
 * This program is Free Software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License as published by the Free Software Foundation; either 
 * version 3 of the License, or (at your option) any later version.                                       
 *                                                                           
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License (http://www.gnu.org/licenses/gpl.txt) for more details.
 *
 * Miscellanea.cpp - Some auxiliary and useful components (implementation)
 ***********************************************************************************************/
 
#include "Miscellanea.h"
#include <climits>
#include <cstdlib>
#include <algorithm>
#include <map>
#include <cmath>
using namespace std;
using namespace mangrove_tds;

/* All the following source belongs to the 'mangrove_tds' namespace */
namespace mangrove_tds
{
	void toggle(deque<bool> &src,deque<bool> &dst)
	{
		/* First, we clear 'dst' and then we toggle values! */
		dst.clear();
		for(unsigned int j=0;j<src.size();j++) { dst.push_back( (!src.at(j))); }		
	}

	/* ============= IMPLEMENTATION of CLASS CompareSimplex =============================================== */
		       
    	bool CompareSimplex::operator() (const vector<SIMPLEX_ID> &first, const vector<SIMPLEX_ID> &second) const { return first<second; }
        
    	bool CompareSimplex::operator()(const SimplexPointer& a,const SimplexPointer& b) const 
    	{
      		/* We compare 'a' with 'b' by giving priority to simplex dimension! */
       		if( a.type() < b.type()) { return true; }
       		else if(a.type()==b.type())
       		{
       			if( a.id()<b.id()) { return true; }
       			else { return false; }
       		}
       		else { return false; }
    	}
    
    	bool CompareSimplex::operator()(const GhostSimplexPointer& a, const GhostSimplexPointer& b) const
    	{
    		/* We compare indices, by giving priority to dimension of the top simplices! */
		if(a.getParentType()<b.getParentType()) return true;
		if(a.getParentType()>b.getParentType()) return false;
		if(a.getParentId()<b.getParentId()) return true;
		if(a.getParentId()>b.getParentId()) return false;	
		if(a.getChildType()<b.getChildType()) return true;
		if(a.getChildType()>b.getChildType()) return false;
		if(a.getChildId()<b.getChildId()) return true;
		else return false;
    	}
	
	bool CompareSimplex::operator()(const GhostSimplex& a,const GhostSimplex& b) const
	{
		/* Now, we must compare two ghost simplices: we compare their vertices! */
		if(a.getCBoundary().size()<b.getCBoundary().size()) return true;
		if(a.getCBoundary().size()>b.getCBoundary().size()) return false;
		for(unsigned int k=0;k<a.getCBoundary().size();k++)
		{
			if(a.getCBoundary().at(k).id()<b.getCBoundary().at(k).id()) return true;
			if(a.getCBoundary().at(k).id()>b.getCBoundary().at(k).id()) return false;
		}
		
		/* If we arrive here, all simplices are equal */
		return false;
	}

    	/* ============= IMPLEMENTATION of CLASS RawFace ================================================== */
	
	RawFace::RawFace()
	{
		/* We initialize the current face! */
		this->flag=false;
		this->vertices.clear();
		this->pos=UINT_MAX;
		this->unique_index = UINT_MAX;
		this->index = UINT_MAX;
		this->reverse_index = UINT_MAX;
		this->children.clear();
		this->sf=SimplexPointer();
	}
    
    	RawFace::RawFace(const RawFace& f)
	{
		/* We deeply copy the input face f */
		this->flag=f.flag;
		this->vertices=vector<SIMPLEX_ID>(f.vertices);
		this->pos=f.pos;
		this->unique_index=f.unique_index;
		this->index=f.index;
		this->reverse_index=f.reverse_index;
		this->children=vector<SIMPLEX_ID>(f.children);
		this->sf=SimplexPointer(f.sf);
	}
	
	RawFace::~RawFace() 
	{
		/* We clear the internal state! */
		this->children.clear(); 
		this->vertices.clear();
	}
	
	bool& RawFace::isTopSimplexChild() { return this->flag; }
	
	vector<SIMPLEX_ID>& RawFace::getVertices() { return this->vertices; }
	
	unsigned int& RawFace::getOriginalPosition() { return this->pos; }
	
	SIMPLEX_ID& RawFace::getUniqueIndex() { return this->unique_index; }
	
	SIMPLEX_ID& RawFace::getIndex() { return this->index; }
	
	SIMPLEX_ID& RawFace::getReverseIndex() { return this->reverse_index; }
	
	vector<SIMPLEX_ID>& RawFace::getBoundary() { return this->children; }
	
	SimplexPointer& RawFace::getSharedFace() { return this->sf; }

    	/* ================= MISCELLANEA ============================================================== */
	
	bool compare_faces( RawFace rfa, RawFace rfb)
    	{
    		/* We give priority to the number of vertices */
       		if(rfa.getVertices().size()<rfb.getVertices().size()) return true;
       		if(rfa.getVertices().size()>rfa.getVertices().size()) return false;
       		for(unsigned int i=0;i<rfa.getVertices().size();i++)
       		{
       			if(rfa.getVertices()[i]<rfb.getVertices()[i]) return true;
       			if(rfa.getVertices()[i]>rfb.getVertices()[i]) return false;
       		}
        	
       		/* If we arrive here, the first face is not 'strictly less than' the second one. */
	 	return false;
	}
	
	bool compare_shared_faces(RawFace rfa,RawFace rfb)
	{
		SimplexPointer a,b;
		
		/* First, we compare the two simplices pointers! */
		a=SimplexPointer(rfa.getSharedFace());
		b=SimplexPointer(rfb.getSharedFace());
		if( a.type() < b.type()) { return true; }
       		else if(a.type()==b.type())
       		{
       			if( a.id()<b.id()) { return true; }
       			else { return false; }
       		}
       		else { return false; }
	}

   	bool same_faces(RawFace rfa,RawFace rfb)
	{
       		/* We should have the same vertices! */
       		if(rfa.getVertices().size()!=rfb.getVertices().size()) return false;
       		for(unsigned int i=0;i<rfa.getVertices().size();i++) { if(rfa.getVertices()[i]!=rfb.getVertices()[i]) return false; }
       		return true;
	}

	bool same_shared_faces(RawFace rfa,RawFace rfb)
	{
		/* The two simplex pointers must be the same ones! */
		if(rfa.getSharedFace().type()!=rfb.getSharedFace().type()) return false;
		if(rfa.getSharedFace().id()!=rfb.getSharedFace().id()) return false;
		return true;
	}
	
	ostream& operator<<(ostream& os, COMPLEX &s)
	{
		COMPLEX::iterator it;
			
		/* Now, we must write the input set! */
		os<<endl;
		for(it=s.begin();it!=s.end();it++) { os<<(*it); }
		os<<endl;
		os.flush();
		return os;
	}
	
	ostream& operator<<(ostream& os,GHOST_COMPLEX &s)
	{
		GHOST_COMPLEX::iterator it;
		
		/* Now, we must write the input set! */
		os<<endl;
		for(it=s.begin();it!=s.end();it++) { os<<(*it); }
		os<<endl;
		os.flush();
		return os;
	}

	ostream& operator<<(ostream& os,GS_COMPLEX &s)
	{
		GS_COMPLEX::iterator it;

		/* Now, we must write the input set! */
		os<<endl;
		for(it=s.begin();it!=s.end();it++) { os<<(*it); }
		os<<endl;
		os.flush();
		return os;
	}

	istream& operator>>(istream& in,COMPLEX &s)
	{
		unsigned int l;
		SimplexPointer c;
			
		/* First, we must understand how many 'SimplexPointer' we must read! */
		l = s.size();
		s.clear();
		for(unsigned int k=0;k<l;k++)
		{
			in>>c;
			s.insert(SimplexPointer(c));
		}
			
		/* If we arrive here, we have finished! */
		return in;
	}
	
	istream& operator>>(istream& in,GHOST_COMPLEX &s)
	{
		unsigned int l;
		GhostSimplexPointer c;
			
		/* First, we must understand how many 'SimplexPointer' we must read! */
		l = s.size();
		s.clear();
		for(unsigned int k=0;k<l;k++)
		{
			in>>c;
			s.insert(GhostSimplexPointer(c));
		}
			
		/* If we arrive here, we have finished! */
		return in;
	}

	istream& operator>>(istream& in,GS_COMPLEX &s)
	{
		unsigned int l;
		GhostSimplex c;

		/* First, we must understand how many 'GhostSimplex' we must read! */
		l=s.size();
		s.clear();
		for(unsigned int k=0;k<l;k++)
		{
			in>>c;
			s.insert(GhostSimplex(c));
		}

		/* If we arrive here, we have finished! */
		return in;
	}

	void destroy( vector< vector<SimplexPointer> > &vv)
	{
		/* We destroy all the locations! */
		for(unsigned int k=0;k<vv.size();k++) vv[k].clear();
		vv.clear();
	}
	
	void destroy( vector< vector<GhostSimplexPointer> > &vv)
	{
		/* We destroy all the locations! */
		for(unsigned int k=0;k<vv.size();k++) vv[k].clear();
		vv.clear();
	}
	
	void destroy( vector< COMPLEX > &sc)
	{
		/* We destroy all the locations! */
		for(unsigned int k=0;k<sc.size();k++) sc[k].clear();
		sc.clear();
	}
	
	void destroy(vector< GHOST_COMPLEX > &sc)
	{
		/* We destroy all the locations! */
		for(unsigned int k=0;k<sc.size();k++) sc[k].clear();
		sc.clear();
	}

	bool sameValues(COMPLEX &a, vector<SimplexPointer> &b)
	{
		COMPLEX::iterator ita;
		vector<SimplexPointer>::iterator itb;
			
		/* Now, we check if 'a' and 'b' contain the same values! */
		if(a.size()!=b.size()) return false;
		itb = b.begin();        	
		for(ita=a.begin();ita!=a.end();ita++)
		{
			if(SimplexPointer(*ita)==SimplexPointer(*itb)) itb++;
			else return false;
		}
			
		/* If we arrive here, we have the same simplices */
		return true; 
    	}
    
    	bool sameValues(GHOST_COMPLEX &a, vector<GhostSimplexPointer> &b)
    	{
    		GHOST_COMPLEX::iterator ita;
		vector<GhostSimplexPointer>::iterator itb;
			
		/* Now, we check if 'a' and 'b' contain the same values! */
		if(a.size()!=b.size()) return false;
		itb = b.begin();        	
		for(ita=a.begin();ita!=a.end();ita++)
		{
			if(GhostSimplexPointer(*ita)==GhostSimplexPointer(*itb)) itb++;
			else return false;
		}
			
		/* If we arrive here, we have the same simplices identifiers! */
		return true; 
    	}

    	SIMPLEX_TYPE dimension(COMPLEX s)
	{
		SIMPLEX_TYPE t;
		
		/* We must iterate over s and take the maximum dimension! */
		t=0;
		for(COMPLEX::iterator it=s.begin();it!=s.end();it++) { t=max(t,it->type()); }
		return t;
	}
	
	SIMPLEX_TYPE dimension(GHOST_COMPLEX s)
	{
		SIMPLEX_TYPE t;
		
		/* We must iterate over s and take the maximum dimension! */
		t=0;
		for(GHOST_COMPLEX::iterator it=s.begin();it!=s.end();it++) { t=max(t,it->getChildType()); }
		return t;
	}
	
	void findProjection( unsigned int i, COMPLEX &s, vector<SimplexPointer> &c)
	{
		COMPLEX::iterator it;
			
		/* First, we check if the input parameters ared valid! */
		assert(s.size()!=0);
		assert(i<s.size());
		assert(s.size()!=0);
		c.clear();
		it = s.begin();
		for(unsigned int k=0;k<s.size();k++)
		{
			/* Do we insert it? */
			if(k!=i) c.push_back(*it);
			it++;
		}
	}
	
	void findProjection( unsigned int i, GHOST_COMPLEX &s, vector<GhostSimplexPointer> &c)
	{
		GHOST_COMPLEX::iterator it;
			
		/* First, we check if the input parameters ared valid! */
		assert(s.size()!=0);
		assert(i<s.size());
		assert(s.size()!=0);
		c.clear();
		it = s.begin();
		for(unsigned int k=0;k<s.size();k++)
		{
			/* Do we insert it? */
			if(k!=i) c.push_back(*it);
			it++;
		}
	}
	
	void filterValues(list<SimplexPointer> &l,const COMPLEX &s)
    	{
      		list<SimplexPointer>::iterator itl;
        	
      		/* Now, we iterator over l and check if 's' contain some elements! */
       		while(itl!=l.end())
       		{
       			if(s.find(*itl)==s.end()) itl = l.erase(itl);
       			else itl++;
       		}
    	}
    
    	void filterValues(list<GhostSimplexPointer> &l,const GHOST_COMPLEX &s)
    	{
      		list<GhostSimplexPointer>::iterator itl;
        	
      		/* Now, we iterator over l and check if 's' contain some elements! */
       		while(itl!=l.end())
       		{
       			if(s.find(*itl)==s.end()) itl = l.erase(itl);
       			else itl++;
       		}
    	}
    
    	void convert( vector< COMPLEX > &src, vector< vector<SimplexPointer> > &dst)
	{
		/* First, we clear 'dst' and then we can convert 'src' in 'dst' */
		destroy(dst);
		for(unsigned int k=0;k<src.size();k++)
		{
			/* We copy the k-th COMPLEX! */
			dst.push_back(vector<SimplexPointer>());
			for(COMPLEX::iterator it=src[k].begin();it!=src[k].end();it++) { dst[k].push_back(SimplexPointer(*it)); }
		}
	}
	
	void convert( vector< GHOST_COMPLEX > &src, vector< vector<GhostSimplexPointer> > &dst)
	{
		/* First, we clear 'dst' and then we can convert 'src' in 'dst' */
		destroy(dst);
		for(unsigned int k=0;k<src.size();k++)
		{
			/* We copy the k-th COMPLEX! */
			dst.push_back(vector<GhostSimplexPointer>());
			for(GHOST_COMPLEX::iterator it=src[k].begin();it!=src[k].end();it++) { dst[k].push_back(GhostSimplexPointer(*it)); }
		}
	}
 
 	void convert( vector< vector<SimplexPointer> > &src, vector< COMPLEX > &dst)
	{
		/* First, we clear 'dst' and then we can convert 'src' in 'dst' */
		destroy(dst);
		for(unsigned int k=0;k<src.size();k++)
		{
			/* We copy the k-th vector<SimplexPointer>! */
			dst.push_back( COMPLEX() );
			for(vector<SimplexPointer>::iterator it=src[k].begin();it!=src[k].end();it++) { dst[k].insert( SimplexPointer(*it)); }
		}
	}
	
	void convert( vector< vector<GhostSimplexPointer> > &src, vector< GHOST_COMPLEX > &dst)
	{
		/* First, we clear 'dst' and then we can convert 'src' in 'dst' */
		destroy(dst);
		for(unsigned int k=0;k<src.size();k++)
		{
			/* We copy the k-th vector<SimplexPointer>! */
			dst.push_back( GHOST_COMPLEX() );
			for(vector<GhostSimplexPointer>::iterator it=src[k].begin();it!=src[k].end();it++) { dst[k].insert(GhostSimplexPointer(*it)); }
		}
	}

	bool compare_simplices_pointers(SimplexPointer& a,SimplexPointer& b) { return a<b; }
	
	bool COMPLEX_contains(COMPLEX &c, const SimplexPointer &sp)
	{
		/* Now, we iterate on 'c' and look for 'sp' */
		for(COMPLEX::iterator cit=c.begin();cit!=c.end();cit++) { if( (cit->type()==sp.type()) && (cit->id()==sp.id()) ) return true; }
		return false;
	}
	
	bool compare_ghost_simplices_pointers(GhostSimplexPointer& a,GhostSimplexPointer& b) { return a<b; }
	
	void extractSimplices( SIMPLEX_TYPE t, vector<SimplexPointer> &src, vector<SimplexPointer> &dst)
    	{
      		/* First, we remove dst! */
       		dst.clear();
       		for(vector<SimplexPointer>::iterator it=src.begin();it!=src.end();it++) { if(it->type()==t) dst.push_back( SimplexPointer(*it)); }
    	}
    
    	void extractSimplices( SIMPLEX_TYPE t, vector<GhostSimplexPointer> &src, vector<GhostSimplexPointer> &dst)
    	{
    		/* First, we remove dst! */
       		dst.clear();
       		for(vector<GhostSimplexPointer>::iterator it=src.begin();it!=src.end();it++) { if(it->getChildType()==t) dst.push_back(GhostSimplexPointer(*it)); }
    	}
    
    	string fromQt2Cplustring(QString str) { return string(str.toAscii().data()); }
    
    	double computeAverage(unsigned int i0,unsigned int i1)
	{
		double a,b;
			
		/* First, we copy the input values and then we compute average! */
		if(i1==0) return 0.0;
		else
		{
			/*  We can compute ratio! */
			a=i0;
			b=i1;
			return (a/b);
		}
	}
	
	/* ======================= RawAdjacency CLASS =================================================== */
	
	RawAdjacency::RawAdjacency(SIMPLEX_TYPE t)
	{
		/* We allocate internal state! */
		this->simplices.clear();
		this->bflags.clear();
		this->reverse_indices.clear();
		this->singularities.clear();
		for(SIMPLEX_TYPE d=0;d<=t;d++) 
		{
			this->simplices.push_back(vector<SimplexPointer>());
			this->reverse_indices.push_back(vector<unsigned int>());
			this->bflags.push_back(false);
			this->singularities.push_back(SimplexPointer(0,0));
		}
	}
	
	RawAdjacency::~RawAdjacency() 
	{
		/* We destroy internal state! */
		destroy(this->simplices);
		this->bflags.clear();
		this->singularities.clear();
		for(unsigned int k=0;k<this->reverse_indices.size();k++) this->reverse_indices[k].clear();
		this->reverse_indices.clear();
	}
	
	void RawAdjacency::addSimplex(const SimplexPointer& cp,unsigned int k,unsigned int ri)
	{
		this->simplices[k].push_back(SimplexPointer(cp));
		this->reverse_indices[k].push_back(ri);
	}
	
	void RawAdjacency::addSingularity(const SimplexPointer& cp, unsigned int k)
	{
		/* Now, we add a new singularity! */
		this->bflags[k]=true;
		this->singularities[k].setType(cp.type());
		this->singularities[k].setId(cp.id());
	}
	
	void RawAdjacency::getSingularity(unsigned int k, SimplexPointer& cp)
	{
		cp.setType(this->singularities[k].type());
		cp.setId(this->singularities[k].id());
	}
	
	/* ============================================= RawIncidence CLASS ============================= */
	
	void RawIncidence::filterSimplices(SIMPLEX_TYPE d,vector<SimplexPointer> &l)
	{
		vector<SimplexPointer>::iterator it;

		/* Now, we extract all k-simplices, with k>=d */
		l.clear();
		for(it=this->simplices.begin();it!=this->simplices.end();it++) { if(it->type()>=d) l.push_back(SimplexPointer(*it)); }
	}
	
	/* ============================ Face CLASS =================================================== */
	
	Face::Face()
	{
		this->pos.clear();
		this->bnd.clear();
		this->cbnd.clear();
	}
	
	Face::Face(const vector<SIMPLEX_ID> & pos)
	{
		this->pos=vector<SIMPLEX_ID>(pos);
		this->bnd.clear();
		this->cbnd.clear();
	}
	
	Face::~Face()
	{
		this->pos.clear();
		this->bnd.clear();
		this->cbnd.clear();
	}

	/* ============================ FaceHierarchy CLASS ============================================== */
	
	FaceHierarchy::FaceHierarchy(SIMPLEX_TYPE d)
	{
		vector<SIMPLEX_ID> src,dst;
		vector<SIMPLEX_ID>::iterator it;
		set< vector<SIMPLEX_ID> , CompareSimplex > s;
		unsigned int lg,a;
		map< vector<SIMPLEX_ID> , SIMPLEX_ID, CompareSimplex> fmapper;

		/* We create a hierarchy of faces: first, we initialize 'fposes' and the original positions! */
		this->fposes.clear();
		for(unsigned int k=0;k<=d;k++)
		{
			this->fposes.push_back(vector<Face>());
			src.push_back(k);
			dst.clear();
			dst.push_back(k);
			this->fposes[0].push_back(Face(dst));
		}

		/* Now, we generate the hierarchy of subfaces */
		this->fposes[d].push_back(Face(src));
		for(int k=d-1;k>=1;k--)
		{
			/* Now, we generate new faces! */
			s.clear();
			for(unsigned int i=0;i<fposes[k+1].size();i++)
			{
				src=vector<SIMPLEX_ID>(this->fposes[k+1][i].getCVertices());
				for(unsigned int j=0;j<src.size();j++)
				{
					/* Now, we create the j-th subface of 'src' */
					findProjection(j,src,dst);
					lg=s.size();
					s.insert(vector<SIMPLEX_ID>(dst));
					if(s.size()!=lg)
					{
						this->fposes[k].push_back(Face(dst));
						if(k==1)
						{
							for(it=dst.begin();it!=dst.end();it++)
							{
								this->fposes[k].back().getBoundary().push_back(*it);
								this->fposes[k-1][*it].getCoboundary().push_back(this->fposes[k].size()-1);
							}
						}
						this->fposes[k+1][i].getBoundary().push_back(s.size()-1);
						this->fposes[k][s.size()-1].getCoboundary().push_back(i);
						fmapper[vector<SIMPLEX_ID>(dst)]=s.size()-1;
					}
					else
					{
						a=fmapper[vector<SIMPLEX_ID>(dst)];
						this->fposes[k+1][i].getBoundary().push_back(a);
						this->fposes[k][a].getCoboundary().push_back(i);
					}
				}
			}	
		}
	}

	void FaceHierarchy::clear()
	{
		/* We remove all! */
		for(unsigned int k=0;k<this->fposes.size();k++) this->fposes[k].clear();
		this->fposes.clear();
	}
			
	SIMPLEX_TYPE FaceHierarchy::getDimension() const
	{
		/* We return the dimension of the reference simplex! */
		if(this->fposes.size()!=0) return (this->fposes.size()-1);
		else return 0;
	}
	
	bool compare_raw_triangles(RawTriangle rta,RawTriangle rtb)
	{
		int t;

		/* Now, we compute the turn from 'rta' to 'rtb' */		
		t=turn(rta.getRealTriangleVertices()[0],rta.getRealTriangleVertices()[1],rta.getRealTriangleVertices()[2],rtb.getRealTriangleVertices()[2]);
		if(t==CCW) return true;
		else return false;
	}
	
	vector<RawTriangle>::iterator findRawTriangle(const SimplexPointer& cp,vector<RawTriangle>& triangles)
	{
		vector<RawTriangle>::iterator it;
		
		/* Now, we look for 'cp' */
		for(it=triangles.begin();it!=triangles.end();it++)
		{
			if(it->getRealSimplex().type()!=cp.type()) { ; }
			else if(it->getRealSimplex().id()!=cp.id()) { ; }
			else { return it; }
		}
		
		/* We haven't found it! */
		return triangles.end();
	}
	
	unsigned int positionOfTriangle(const SimplexPointer& cp,vector<RawTriangle> &triangles)
	{
		unsigned int k;
		
		/* Now, we look for 'cp' */
		for(k=0;k<triangles.size();k++)
		{
			if(triangles[k].getRealSimplex().type()!=cp.type()) { ; }
			else if(triangles[k].getRealSimplex().id()!=cp.id()) { ; }
			else { return k; }
		}
		
		/* We haven't found it! */
		return triangles.size();
	}
	
	/*************** CLASS RawGhostSimplex ******************************************************************************/
	
	RawGhostSimplex::RawGhostSimplex()
	{
		/* Dummy initialization */
		this->vertices.clear();
		this->aliases.clear();
	}

	RawGhostSimplex::RawGhostSimplex(const RawGhostSimplex& s)
	{	
		/* Dummy initialization */
		this->vertices.clear();
		this->aliases.clear();
		this->vertices=vector<SIMPLEX_ID>(s.vertices);
		for(unsigned int k=0;k<s.aliases.size();k++) this->aliases.push_back(GhostSimplexPointer(s.aliases.at(k)));
	}
	
	RawGhostSimplex::~RawGhostSimplex()
	{
		/* Dummy initialization */
		this->vertices.clear();
		this->aliases.clear();
	}

	vector<SIMPLEX_ID>& RawGhostSimplex::getVertices() { return this->vertices; }
			
	vector<GhostSimplexPointer>& RawGhostSimplex::getAliases() { return this->aliases; }

	bool compare_ghost_simplices(RawGhostSimplex rfa, RawGhostSimplex rfb)
	{
    	/* We give priority to the number of vertices */
       	if(rfa.getVertices().size()<rfb.getVertices().size()) return true;
       	if(rfa.getVertices().size()>rfa.getVertices().size()) return false;
       	for(unsigned int i=0;i<rfa.getVertices().size();i++)
       	{
       		if(rfa.getVertices()[i]<rfb.getVertices()[i]) return true;
       		if(rfa.getVertices()[i]>rfb.getVertices()[i]) return false;
       	}
        	
       	/* If we arrive here, the first ghost simplex is not 'strictly less than' the second one. */
	 	return false;
	}
		
	bool same_ghost_simplices(RawGhostSimplex rfa, RawGhostSimplex rfb)
	{
		/* Now, we require the same vertices! */
		if(rfa.getVertices().size()!=rfb.getVertices().size()) return false;
       	for(unsigned int i=0;i<rfa.getVertices().size();i++) { if(rfa.getVertices()[i]!=rfb.getVertices()[i]) return false; }
		return true;
	}
}

